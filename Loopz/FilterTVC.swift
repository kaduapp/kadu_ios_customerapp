//
//  FilterTVC.swift
//  DelivX
//
//  Created by 3Embed on 13/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class FilterTVC: UITableViewCell {

    @IBOutlet weak var selectedLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var selectedFilterImage : UIImageView!
    
  
    @IBOutlet weak var radioButtonImage: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Fonts.setPrimaryRegular(titleLabel)
        titleLabel.textColor = Colors.PrimaryText
        if selectedLabel != nil {
            Fonts.setPrimaryRegular(selectedLabel)
            selectedLabel.textColor = Colors.AppBaseColor
        }
        self.tintColor = Colors.AppBaseColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setSelectedImage(type:String,isSelected:Bool){
        
        if (type == APIRequestParams.Sort || type == StringConstants.Sort())  {
            radioButtonImage.setImage(#imageLiteral(resourceName: "CheckOn"), for: .selected)
            radioButtonImage.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
        }else{
            radioButtonImage.setImage(#imageLiteral(resourceName: "CheckBoxOn"), for: .selected)
            radioButtonImage.setImage(#imageLiteral(resourceName: "CheckBoxOff"), for: .normal)
        }
        
        if isSelected {
            radioButtonImage.isSelected = true
        }else{
            radioButtonImage.isSelected = false
        }
        
    }

}
