//
//  Store.swift
//  UFly
//
//  Created by 3Embed on 30/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
class StoreSubCatsModel:NSObject{
    var Id:String = ""
    var SubCategoryName:String = ""
}
class Store: NSObject {
    var Name                    = "Store"
    var Id                      = ""
    var Image                   = ""
    var DistanceKm              = "0"
    var DistanceMiles:Float     = 0
    var EstimatedTime           = "0 Mins"
    var Address                 = "Address"
    var StoreRating:Float       = 0
    var StoreLat:Double         = 0
    var StoreLong:Double        = 0
    var MinimumOrder:Float      = 0
    var FreeDelivery:Float      = 0
    var Products:[Item]         = []
    var CostForTwo:Float        = 0
    var dataObject:[String:Any] = [:]
    var TypeStore:AppConstants.TypeOfStore = .Grocery
    var TypeStoreMsg: String    = ""
    var StoreDesc: String       = ""//
    var BannerImage: String     = ""//
    var FoodTypeName            = ""
    var SubcategoryId           = ""
    var SubcategoryName         = ""
    var FranchiseId             = ""
    var FranchiseName           = ""
    var OfferId                 = ""
    var OfferBanner             = ""
    var OfferTitle              = ""
    var StoreSubCats:[StoreSubCatsModel]    = []
    var Fav                     = false
    var StoreIsOpen             = false
    var todayIsOpen             = 0 // if 0 then today and 1 for tomorrow
    var nextOpenTime            = ""
    var storeArea               = ""
    
    // Cart Allowed 1 :  Single Cart Single Store
    // Cart Allowed 2 :  Single Cart Multiple store
    // Cart Allowed 3 :  Single Cart Multiple Store Category
    var cartType:AppConstants.TypeOfCart = .SingleCartSingleStore
    
    
    init(data:[String:Any]) {
        super.init()
        
        if data.count > 0{
        
        if let storeAddrTemp = data[APIResponceParams.StoreAddr] as? String{
            Address = storeAddrTemp
        }
        
        if let storeBillingAddrTemp = data[APIResponceParams.StoreBillingAddr] as? String{
            Address = storeBillingAddrTemp
        }
        
        if let minimumOrderTemp = data[APIResponceParams.RestuarantsMinimumOrder] as? String{
            MinimumOrder = minimumOrderTemp.floatValue!
        }

        if let titleTemp = data[APIResponceParams.Fav] as? Bool{
            Fav             = titleTemp
        }
        
        if let titleTemp = data["areaName"] as? String{
            storeArea             = titleTemp
        }
        
        if let storeTypeTemp = data[APIResponceParams.RestuarantsStoreType] as? Double{
            
            switch Int(storeTypeTemp) {
            case 1:
                TypeStore = .Restaurant
                break
            case 2:
                TypeStore = .Grocery
                break
            case 3:
                TypeStore = .Shopping
                break
            case 4:
                TypeStore = .Marijuana
                break
            case 5:
                TypeStore = .Launder
                break
            default:
                TypeStore = .Grocery
                break
            }
        }
        
        if let cartTypeTemp = data[APIResponceParams.StoreCartType] as? Int{
            
            switch Int(cartTypeTemp) {
            case 1:
                cartType = .SingleCartSingleStore
                break
            case 3:
                cartType = .SingleCartMulStoreCat
                break
            default:
               cartType = .SingleCartMulStore
                break
            }
        }
        
        
        if let freeDeliveryAboveTemp = data[APIResponceParams.RestuarantsFreeDeliveryAbove] as? String{
            if freeDeliveryAboveTemp.length > 0 && Float(freeDeliveryAboveTemp) != nil {
                FreeDelivery =  Float(freeDeliveryAboveTemp)!
            }
        }
        if let storeTypeMsgTemp = data[APIResponceParams.StoreTypeMsg] as? String{
            TypeStoreMsg = storeTypeMsgTemp
        }
        
        if let distanceTemp = data[APIResponceParams.Distance] as? Double{
            DistanceMiles = Float(distanceTemp)
        }
        
        if let storeNameTemp = data[APIResponceParams.RestuarantsStoreName] as? String{
            Name = storeNameTemp
        }
        
        if let storeDescriptionTemp = data[APIResponceParams.StoreDescription] as? String{
            StoreDesc = storeDescriptionTemp
        }
        
        if let bannerImageTemp = data[APIResponceParams.RestuarantsBannerImage] as? String{
            BannerImage = bannerImageTemp
        }
        
        if let logoImageTemp = data[APIResponceParams.RestuarantsLogoImage] as? String{
            Image = logoImageTemp
        }
        
        if let storeIdTemp = data[APIResponceParams.RestuarantsStoreId] as? String{
            Id = storeIdTemp
        }
        
        if let averageRatingTemp = data[APIResponceParams.AverageRating] as? Double{
            StoreRating = Float(averageRatingTemp)
        }
        if let averageRatingTemp = data[APIResponceParams.CostForTwo] as? Double{
            CostForTwo = Float(averageRatingTemp)
        }
        if let averageRatingTemp = data[APIResponceParams.AverageRating] as? Float{
            StoreRating = averageRatingTemp
        }
        if let averageRatingTemp = data[APIResponceParams.CostForTwo] as? Float{
            CostForTwo = averageRatingTemp
        }
        if let averageRatingTemp = data[APIResponceParams.AverageRating] as? String{
            StoreRating = averageRatingTemp.floatValue!
        }
        if let averageRatingTemp = data[APIResponceParams.CostForTwo] as? String{
            CostForTwo = averageRatingTemp.floatValue!
        }
        if let averageRatingTemp = data[APIResponceParams.FoodTypeName] as? String{
            FoodTypeName = averageRatingTemp
        }
        
        if let averageRatingTemp = data[APIResponceParams.FranchiseId] as? String{
            FranchiseId = averageRatingTemp
            
        }
        if let averageRatingTemp = data[APIResponceParams.FranchiseName] as? String{
            FranchiseName = averageRatingTemp
        }
        if let offerId = data[APIResponceParams.OfferId] as? String{
            OfferId = offerId
        }
        if let offerTitle = data[APIResponceParams.OfferTitle] as? String{
            OfferTitle = offerTitle
            
        }
        if let offerBanner = data[APIResponceParams.OfferBanner] as? [String:Any]{
            if let mobile = offerBanner[APIResponceParams.Phone] as? String{
                OfferBanner = mobile
                
            }
        }
        
        if let StoreSubCat = data[APIResponceParams.StoreSubCats] as? [[String:Any]]{
            StoreSubCats.removeAll()
            
            for subCat in StoreSubCat{
                let cat = StoreSubCatsModel()
                if let id = subCat[APIResponceParams.StoreSubCatsId] as? String{
                    cat.Id = id
                }
                if let id = subCat[APIResponceParams.SubCategoryName] as? String{
                    cat.SubCategoryName = id
                }
                StoreSubCats.append(cat)
            }
            
        }
        if let averageRatingTemp = data[APIResponceParams.subcategoryId] as? String{
            SubcategoryId = averageRatingTemp
            
        }
        if let averageRatingTemp = data[APIResponceParams.subcategoryName] as? String{
            SubcategoryName = averageRatingTemp
        }
        if let averageRatingTemp = data[APIResponceParams.SubCategoryName2] as? String{
            SubcategoryName = averageRatingTemp
        }
        if StoreSubCats.count > 0 {
            SubcategoryId = StoreSubCats[0].Id
            SubcategoryName = StoreSubCats[0].SubCategoryName
        }
        
//        if let titleTemp = data[APIResponceParams.Products] as? [Any] {
//            Products.removeAll()
//
//            for item in titleTemp {
//                 Products.append(Item.i)
//            }
//        }
        if let StoreSubCat = data[APIResponceParams.StoreSubCats] as? [[String:Any]]{
            StoreSubCats.removeAll()
            
            for subCat in StoreSubCat{
                let cat = StoreSubCatsModel()
                if let id = subCat[APIResponceParams.StoreSubCatsId] as? String{
                    cat.Id = id
                }
                if let id = subCat[APIResponceParams.SubCategoryName] as? String{
                    cat.SubCategoryName = id
                }
                StoreSubCats.append(cat)
            }
            
        }
        if let averageRatingTemp = data[APIResponceParams.subcategoryId] as? String{
            SubcategoryId = averageRatingTemp
            
        }
        if let averageRatingTemp = data[APIResponceParams.subcategoryName] as? String{
            SubcategoryName = averageRatingTemp
        }
        if StoreSubCats.count > 0 {
            SubcategoryId = StoreSubCats[0].Id
            SubcategoryName = StoreSubCats[0].SubCategoryName
        }
        dataObject = data
        if let titleTemp = data[APIResponceParams.StoreId] as? String{
            Id           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.StoreName] as? String{
            Name         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.BusinessImage] as? String{
            Image        = titleTemp
        }
            if let titleTemp = data[APIResponceParams.DistanceKm] {
                DistanceKm    = "\(titleTemp)"
        }
        if let titleTemp = data[APIResponceParams.DistanceMiles] as? Float{
            DistanceMiles  =  titleTemp
        }
        if let titleTemp = data[APIResponceParams.DistanceMiles] as? Double{
            DistanceMiles  =  Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.EstimatedTime] as? String{
            EstimatedTime      = titleTemp
        }
        if let titleTemp = data[APIResponceParams.StoreRating] as? Float{
            StoreRating        = titleTemp
        }
        if let titleTemp = data[APIResponceParams.StoreRating] as? Double{
            StoreRating        = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.StoreAddress] as? String{
            Address      = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.StoreAddress2] as? String{
            Address      = titleTemp
        }

        if let titleTemp = data[APIResponceParams.StoreIsOpen] as? Bool{
            StoreIsOpen = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.StoreIsOpen] as? Int{
            if titleTemp == 0{
                StoreIsOpen = false
            }else{
                StoreIsOpen = true
            }
        }

        if let titleTemp = data[APIResponceParams.Today] as? Bool{
            if titleTemp{
                todayIsOpen = 1
            }else{
                todayIsOpen = 0
            }
        }
        
        if let titleTemp = data[APIResponceParams.Today] as? Int{
            todayIsOpen = titleTemp
        }
        
          if let titleTemp = data[APIResponceParams.NextOpenTime] as? Int{
            if titleTemp == 0
            {
                nextOpenTime = ""
                return
            }
            let startTimestamp = NSNumber.init(value:titleTemp )
            let timeStampStart =  Date(timeIntervalSince1970: TimeInterval(truncating: startTimestamp))
            let dateStr = Helper.getDateString(value: timeStampStart, format: DateFormat.DateAndTimeFormatServer, zone: true)
            nextOpenTime = Helper.getDateString12hrsFormat(dateStr: dateStr)
            
//       if let titleTemp = data[APIResponceParams.NextOpenTime] as? String{
//           nextOpenTime = Helper.getDateString(value: Helper.getStringToDate(value: titleTemp, format: DateFormat.TimeFormatServer, zone: true)!, format: DateFormat.TimeFormatToDisplay, zone: true)
       }
        
        if let titleTemp = data[APIResponceParams.StoreLat] as? Double {
            StoreLat = titleTemp
        }
        if let titleTemp = data[APIResponceParams.StoreLong] as? Double {
            StoreLong = titleTemp
        }
        if let titleTemp = data[APIResponceParams.StoreType] as? Int {
            switch titleTemp {
            case 1:
                TypeStore = .Restaurant
                break
            case 2:
                TypeStore = .Grocery
                break
            case 3:
                TypeStore = .Shopping
                break
            case 4:
                TypeStore = .Marijuana
                break
            default:
                TypeStore = .Grocery
                break
            }
        }
        
        if let titleTemp = data[APIResponceParams.MinimumOrder] as? String {
            if titleTemp.length > 0{
                MinimumOrder = Float(titleTemp)!
            }
            
        }
        if let titleTemp = data[APIResponceParams.FreeDeliveryAbove] as? String {
            if titleTemp.length > 0 && Float(titleTemp) != nil {
                FreeDelivery =  Float(titleTemp)!
            }
        }
        if let titleTemp = data[APIResponceParams.Products] as? [Any] {
            Products.removeAll()
            for item in titleTemp {
                Products.append(Item.init(data: item as! [String : Any], store: self))
            }
        }
        
    }
  }
}
