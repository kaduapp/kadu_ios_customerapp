//
//  HistoryTVC.swift
//  History
//
//  Created by 3Embed on 06/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class HistoryTVC: UITableViewController
{
    var dataModel = DataModel()
    var inTheSameStore = false
    var historyVM = HistoryVM()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.contentOffset = CGPoint.init(x: 0, y: 0)
        self.tableView.separatorColor = UIColor(red: 133/255.0, green: 138/255.0, blue: 168/255.0, alpha: 1)
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return dataModel.dataToDisplay.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if let count = dataModel.dataToDisplay[section].orders?.count
        {
            inTheSameStore = false
            return count + 1
        }
        
        return 0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.row == dataModel.dataToDisplay[indexPath.section].orders?.count
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HistoryFooterTVCell.self), for: indexPath) as! HistoryFooterTVCell
            
            cell.configureCell(data: dataModel.dataToDisplay[indexPath.section])
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HistoryTVCell.self), for: indexPath) as! HistoryTVCell
            
            if indexPath.row >= 1
            {
                if dataModel.dataToDisplay[indexPath.section].orders![indexPath.row].storeType == dataModel.dataToDisplay[indexPath.section].orders![indexPath.row - 1].storeType
                {
                    inTheSameStore = true
                }
                else
                {
                    inTheSameStore = false
                }
            }
            
            cell.configureCell(data: dataModel.dataToDisplay[indexPath.section].orders![indexPath.row], indexPath:indexPath,inTheSameStore:inTheSameStore)
            
            return cell
            
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let headerView = UIView()
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HistoryHeaderTVCell.self)) as! HistoryHeaderTVCell
        headerView.addSubview(cell)
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == dataModel.dataToDisplay.count - 1{
            if let tab = self.tabBarController?.tabBar{
            return tab.bounds.height + 5
            }
        }
        
            return 1
        
    }
}
