//
//  EditProfileUIE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension EditVC {
    
    /// initial setup
    func initialSetup() {
        editView.layer.cornerRadius = 5
        editProfileVM.pickedImage = nil
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.editView.phoneNumTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
         self.editView.emailTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        editProfileVM.pickerObj?.delegate = self
        editProfileVM.userData = Utility.getProfileData()
        editProfileVM.authentication = Authentication()
        editProfileVM.authentication.CountryCode = (editProfileVM.userData?.CountryCode)!
        editProfileVM.authentication.Name = (editProfileVM.userData?.FirstName)!
        editProfileVM.authentication.Phone = (editProfileVM.userData?.Phone)!
        editProfileVM.authentication.Email = (editProfileVM.userData?.Email)!
        editProfileVM.authentication.ProfilePic = (editProfileVM.userData?.userProfilePic)!
        didValidatePhone()
        
        self.editView.updateView(data: editProfileVM.userData!)
    }
    
    /// set navigation bar transperant and remove the shadow
    func setNavigationBar() {
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        Helper.transparentNavigation(controller: self.navigationController!)
    }
    
}
