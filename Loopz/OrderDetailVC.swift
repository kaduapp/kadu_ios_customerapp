//
//  OrderDetailVC.swift
//  UFly
//
//  Created by 3 Embed on 07/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class OrderDetailVC: UIViewController {
    //outlets
    @IBOutlet weak var navigationWidth: NSLayoutConstraint!
    @IBOutlet weak var reviewButton: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var navSubTitle: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    var Response_Picker = PublishSubject<Int>()
    var disposeBag = DisposeBag()
    var orderDetailVM = OrderDetailVM()
    var help = false
    var isTrackOrderVC = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        updateOrderID()
        didGetOrderDetail()
        orderDetailVM.getOrderDetail()
         cancelBtn.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Helper.finalController().tabBarController != nil{
            Helper.finalController().tabBarController?.tabBar.isHidden = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let con = Helper.finalController()
        if con.navigationController != nil {
            if con.navigationController?.viewControllers.count == 1 {
                if con.tabBarController != nil{
                    con.tabBarController?.tabBar.isHidden = false
                }
            }
        }
    }
    
    
    @IBAction func closeBtnAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        if (orderDetailVM.selectedOrder?.Status)! < 5 || (orderDetailVM.selectedOrder?.Status)! == 40 {
            self.performSegue(withIdentifier: UIConstants.SegueIds.OrderToCancel, sender: self)
        }else{
            self.performSegue(withIdentifier: String(describing: TrackingVC.self), sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func reviewAction(_ sender: Any) {
        
        if self.orderDetailVM.selectedOrder?.Status == 15 || self.orderDetailVM.selectedOrder?.Status == 7 {
            Helper.openReview(controller: self, order: self.orderDetailVM.selectedOrder!)
        }else if (self.orderDetailVM.selectedOrder?.Status)! >= 10 {
            self.performSegue(withIdentifier: String(describing: ChatVC.self), sender: self)
        }
        else{
             self.performSegue(withIdentifier: String(describing: TrackingVC.self), sender: self)
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.OrderToCancel {
            
            if let viewController: PopupVC = segue.destination as? PopupVC {
                viewController.popupVM.type = 1
                
                viewController.popupVM.popupVM_response.subscribe(onNext: {[weak self]success in
                    self?.orderDetailVM.cancelOrder(reason: success[0] as! String)
                }, onError: {error in
                    print(error)
                }).disposed(by: viewController.dispose)
            }
        } else if segue.identifier == String(describing: "ChatVC") {
            let nav = segue.destination as! UINavigationController
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                if let bookingIdValue = self.orderDetailVM.selectedOrder?.BookingId{
                    viewController.bookingID = String(describing: bookingIdValue)
                    }
                viewController.custName = (self.orderDetailVM.selectedOrder?.DriverName)!
                viewController.customerID = (self.orderDetailVM.selectedOrder?.DriverId)!
                viewController.custImage = (self.orderDetailVM.selectedOrder?.DriverImage)!
            }
        } else if segue.identifier == String(describing: TrackingVC.self) {
            let nav = segue.destination as! UINavigationController
            if let viewController: TrackingVC = nav.viewControllers.first as! TrackingVC? {
                viewController.dataOrder = self.orderDetailVM.selectedOrder!
            }
        } else if segue.identifier == String(describing: ZendeskVC.self) {
            let nav = segue.destination as! UINavigationController
            if let nextScene: ZendeskVC = nav.viewControllers.first as! ZendeskVC?{
                nextScene.subJuctGiven = "[\(String(describing: self.orderDetailVM.selectedOrder!.BookingId))] " + (sender as! String)
            }
        }
    }
}

extension OrderDetailVC {
    
    /// Observe for view model
    func didGetOrderDetail(){
        orderDetailVM.orderDetailVM_response.subscribe(onNext: { success in
            
            self.mainTableView.reloadData()
            self.updateOrderID()
        }).disposed(by: self.orderDetailVM.dispose)
        
        MQTTDelegate.message_response.subscribe(onNext: {success in
            if self.orderDetailVM.selectedOrder?.BookingId == success.Id && success.Action != 12 && success.Action != 14 {
                if success.Status == 10 || success.Status == 8 {
                    self.orderDetailVM.selectedOrder?.DriverId = success.DriverId
                    self.orderDetailVM.selectedOrder?.DriverName = success.DriverName
                    self.orderDetailVM.selectedOrder?.DriverImage = success.DriverImage
                    self.orderDetailVM.selectedOrder?.DriverEmail = success.DriverEmail
                    self.orderDetailVM.selectedOrder?.DriverMobile = success.DriverMobile
                }
                if success.Status != 0 {
                    self.orderDetailVM.selectedOrder?.Status = success.Status
                    self.orderDetailVM.selectedOrder?.StatusMessage = success.Message
                }
                if success.Status != 1{
                    self.orderDetailVM.selectedOrder?.statusCode = success.Status
                }
                self.mainTableView.reloadRows(at: [IndexPath.init(item: 0, section: 0)], with: .fade)
                self.updateOrderID()
                }
            
        }, onError: {error in
            print(error)
        }).disposed(by: disposeBag)
        
        HistoryAPICalls.reviewDone.subscribe(onNext: { [weak self]data in
    
            self?.dismiss(animated: false, completion: nil)
        }).disposed(by: disposeBag)
        
    }
}
extension OrderDetailVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableView {
            if (self.navigationController != nil) {
            if scrollView.contentOffset.y > 0 {
                Helper.addShadowToNavigationBar(controller: self.navigationController!)
            }else{
                Helper.removeShadowToNavigationBar(controller: self.navigationController!)
            }
            }
        }
    }
  
}




