//
//  ReviewVM.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ReviewVM: NSObject {
    let disposeBag = DisposeBag()
    var orderRecieved:Order? = nil
    var selectTip:Float = 0
    var RatingArray:[Rating] = []
    let ratingVM_response = PublishSubject<Bool>()
    
    func getRating(_ type: Int) {
        HistoryAPICalls.getRating(type).subscribe(onNext: { [weak self]data in
            self?.RatingArray = data
            self?.ratingVM_response.onNext(true)
        }).disposed(by: self.disposeBag)
    }
    
    
    
    func submitRating() {
        HistoryAPICalls.updateRating(rating: RatingArray, order: orderRecieved!,tip:selectTip).subscribe(onNext: { [weak self]data in
            self?.ratingVM_response.onNext(false)
        }).disposed(by: self.disposeBag)
    }
}
