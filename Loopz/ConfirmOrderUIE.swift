//
//  ConfirmOrderUIE.swift
//  DelivX
//
//  Created by 3Embed on 22/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension ConfirmOrderVC {
    
    func setData() {
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        Fonts.setPrimaryBold(placeOrderButton)
        placeOrderView.backgroundColor =  Colors.AppBaseColor
        Helper.setButtonTitle(normal: StringConstants.PlaceTheOrder(), highlighted: StringConstants.PlaceTheOrder(), selected: StringConstants.PlaceTheOrder(), button: placeOrderButton)
        Helper.setButton(button: placeOrderButton, view: placeOrderView, primaryColour:  Colors.AppBaseColor, seconderyColor:  Colors.SecondBaseColor, shadow: true)
        Helper.removeNavigationSeparator(controller: self.navigationController!,image: true)
        updateBtn()
        Helper.setShadow(sender: totalView)
        total.textColor =  Colors.PrimaryText
        amount.textColor =  Colors.PrimaryText
        
        Fonts.setPrimaryMedium(total)
        Fonts.setPrimaryMedium(amount)
        
        total.text = StringConstants.ToPay()
        amount.text = Helper.df2so(Double( AppConstants.TotalCartPriceWithTax + AppConstants.TotalDeliveryPrice - checkoutVM.discount))
        
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.ConfirmOrder())
        }
        
        if  checkoutVM.paymentHandler.paymentType == 0{
        if Utility.getWallet().Amount > AppConstants.TotalCartPriceWithTax{
       checkoutVM.isPaymentMethodSelected = true
        }
        else{
            checkoutVM.isPaymentMethodSelected = false
         }
        }
        
    }
    
    
    func updateBtn(){
        if checkoutVM.isPaymentMethodSelected {
            
            if Utility.getSelectedSuperStores().type == .Launder{
                
                if checkoutVM.pickupAddress != nil &&  checkoutVM.deliveryAddress != nil{
                    Helper.setButton(button: placeOrderButton, view: placeOrderView, primaryColour:  Colors.DeliveryBtn, seconderyColor:  Colors.SecondBaseColor, shadow: true)
                    placeOrderButton.isUserInteractionEnabled = true
                    return
                }
                
            }
            else{
            if checkoutVM.pickup == 1{
                
                if checkoutVM.pickupAddress != nil ||  checkoutVM.selectedAddress != nil{
                    Helper.setButton(button: placeOrderButton, view: placeOrderView, primaryColour:  Colors.DeliveryBtn, seconderyColor:  Colors.SecondBaseColor, shadow: true)
                    placeOrderButton.isUserInteractionEnabled = true
                    return
                }
            }else {
                
                Helper.setButton(button: placeOrderButton, view: placeOrderView, primaryColour:  Colors.PickupBtn, seconderyColor:  Colors.SecondBaseColor, shadow: true)
                placeOrderButton.isUserInteractionEnabled = true
                return
             }
            }
        }
        Helper.setButton(button: placeOrderButton, view: placeOrderView, primaryColour:  Colors.DoneBtnNormal, seconderyColor:  Colors.SecondBaseColor, shadow: true)
        placeOrderButton.isUserInteractionEnabled = false
        
    }
    

    func getPaymentType()
    {
        if (Utility.getWallet().Amount != 0) && Utility.getWallet().Amount >= AppConstants.TotalCartPriceWithTax
        {
            checkoutVM.paymentHandler.isPayByWallet = true
        } else
        {
            checkoutVM.paymentHandler.isPayByWallet = false //select payment
        }
    }
    func getWallet(){
        WalletAPICalls.getBalance().subscribe({ data in
          
            if self.checkoutVM.paymentHandler.paymentType == 0 {
            if Utility.getWallet().Amount > AppConstants.TotalCartPriceWithTax  {
                self.checkoutVM.isPaymentMethodSelected = true
            }
            else{
                self.checkoutVM.isPaymentMethodSelected = false
            }
            }
         self.mainTableVew.reloadData()
        }).disposed(by: APICalls.disposesBag)
    }
    
    func addDoneBtn(_ sender: Any) {
        //Add done button to numeric pad keyboard
        
        let toolbarDone = UIToolbar()
        toolbarDone.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneBtnAction(_:)))
        if let textField = sender as? UITextField {
            done.tag = textField.tag
            toolbarDone.items = [flexSpace, done] // You can even add cancel button too
            textField.inputAccessoryView = toolbarDone
        } else if let textView = sender as? UITextView {
            toolbarDone.items = [flexSpace, done] // You can even add cancel button too
            textView.inputAccessoryView = toolbarDone
        }
    }
    
    @objc func doneBtnAction(_ sender: UIButton) {
              // handle done button action for textview
                  self.view.endEditing(true)
        }
    
}

