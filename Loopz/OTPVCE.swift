//
//  OTPVCE.swift
//  UFly
//
//  Created by 3 Embed on 11/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

// MARK: - UIScrollViewDelegate
extension OTPVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        let movedOffset: CGFloat = sender.contentOffset.y
        if sender == otpView.scrollView {
            if movedOffset <= 0 {
                sender.contentOffset.y = 0
            }
            let pageWidth: CGFloat = 238 - 64             //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            if ratio >= 1 {
                navigationBackView.isHidden = false
                navigationBackView.backgroundColor = UIColor.white
                titleText.textColor = UIColor.black
            }else {
                navigationBackView.isHidden = true
            }
        }
    }
}

extension OTPVC {
    
    /// this method acts as a Handler for login ,get otp and verify otp Responses
    ///
    /// - Parameter data: data its of typ enum ResponseType
    func subscribe(data:OTPVM.ResponseType) {
        switch data {
        case .LoginDone:
            Helper.SaveCredsToKeyChain(controller:self,key:self.otpVM.authentication!){(flag) in
                Utility.UtilityUpdate.onNext(.Login)
                self.dismiss(animated: true, completion: nil)
            }
            break
        case .NewPassword:
            self.performSegue(withIdentifier: UIConstants.SegueIds.OtpToEnterNewPW, sender: self)
            break
        case .UpdateProfileData:
            if otpVM.isFromUpdateProfile {
                editProfileVM.authentication = otpVM.authentication!
                editProfileVM.isValidPhone = true
                editProfileVM.isValidEmail = true
                editProfileVM.userData = Utility.getProfileData()
                editProfileVM.updateProfile()
            }else{
                signupVM.signup(data: otpVM.authentication!)
            }
            break
        case .Failuar:
            self.otpView.clearOtp()
            break
        }
    }
    
    
    
}
