//
//  UploadIdentityUIE.swift
//  UFly
//
//  Created by 3 Embed on 08/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension UploadIdentityVC {
    
    
    /// idCard Image and MMj Card having same Actions
    ///each card buttons having different tags,based on tag image gets asigned
    /// - Parameter sender: sender is Button type
    func updateImage(sender: UIButton){
        
        uploadIdVM.btnTag = UploadIdentityVM.card(rawValue: sender.tag)!
        var imageChosen:UIImage? = nil
        if uploadIdVM.btnTag == .iDCard {
            imageChosen = uploadIDView.pickedIDImage
        }
        else{
            imageChosen = uploadIDView.pickedMMJImage
        }
        if imageChosen == nil {
            uploadIdVM.flag = 0
        }
        else {
            uploadIdVM.flag = 1
        }
        uploadIdVM.pickerObj?.setProfilePic(controller: self , tag: uploadIdVM.flag)
    }
}
