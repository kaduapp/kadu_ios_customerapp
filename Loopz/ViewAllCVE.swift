//
//  ViewAllCVE.swift
//  DelivX
//
//  Created by 3Embed on 02/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension ViewAllVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchVM.arrayOfExpandedStore.Products.count > 0 {
            return searchVM.arrayOfExpandedStore.Products.count
        }
        return viewAllVM.arrayOfItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ItemCommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self), for: indexPath) as! ItemCommonCollectionViewCell
        
        cell.cartVM = self.cartVM
        if searchVM.arrayOfExpandedStore.Products.count > 0 {
            cell.setValue(data: searchVM.arrayOfExpandedStore.Products[indexPath.row],isDelete: false)
        }else{
            cell.setValue(data: viewAllVM.arrayOfItems[indexPath.row],isDelete: false)
        }
        
        cell.addToCartBtn.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: CGFloat((UIScreen.main.bounds.width - 40)/2), height: 225)
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        var size = CGSize.zero
//        let cellWidth = (UIScreen.main.bounds.width-2)/2
//        let cellHight = (collectionView.frame.size.width)/2 + 40
//        size.height =  cellHight
//        size.width  =  cellWidth
//        return size
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selected = collectionView.cellForItem(at: indexPath)
        if searchVM.arrayOfExpandedStore.Products.count > 0 {
            performSegue(withIdentifier: String(describing: ItemDetailVC.self), sender: searchVM.arrayOfExpandedStore.Products[indexPath.row])
        } else {
            performSegue(withIdentifier: String(describing: ItemDetailVC.self), sender: viewAllVM.arrayOfItems[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if indexPath.row == viewAllVM.arrayOfItems.count-1 && viewAllVM.arrayOfItems.count > (viewAllVM.limit * viewAllVM.offset) + (viewAllVM.limit - 1) {
//            viewAllVM.offset = viewAllVM.offset + 1
//            viewAllVM.getData(id: offerId)
//        }
        if indexPath.row == viewAllVM.arrayOfItems.count-1 && searchVM.arrayOfExpandedStore.Products.count == 0 {
            viewAllVM.getData(id: offerId)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionView.elementKindSectionHeader){
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:String(describing: ViewAllHeaderView.self), for: indexPath) as! ViewAllHeaderView
            headerView.titleLB.text = viewAllVM.titleLabel
            return headerView
        }
        return UICollectionReusableView()
    }
}


extension ViewAllVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //        if scrollView == self.mainCollectionView {
        //            if scrollView.contentOffset.y > 0 {
        //                if (self.navigationController != nil) {
        //                Helper.addShadowToNavigationBar(controller: self.navigationController!)
        //                }
        //            }else{
        //                if (self.navigationController != nil) {
        //                    Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        //                }
        //            }
        //        }
        
        if let navBar = self.navigationController{
            var offset = scrollView.contentOffset.y / 35
            if scrollView.contentOffset.y > 1{
                Helper.addShadowToNavigationBar(controller: navBar)
            }
            else{
                Helper.removeShadowToNavigationBar(controller: navBar)
            }
            if offset > 30{
                offset = 1
                self.setTheAlphaToNavigationViewsOnScroll(offset)
            }
            else{
                self.setTheAlphaToNavigationViewsOnScroll(offset)
            }
        }
    }
}

