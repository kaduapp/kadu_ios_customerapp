//
//  WishListIPE.swift
//  DelivX
//
//  Created by 3Embed on 06/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
extension WishListVC: ImagePickerModelDelegate {
    
    
    
    /// its imagePicker model Delegate method
    /// when user pics the image this method gets called
    /// - Parameters:
    ///   - selectedImage: it is of type UIImage,if tag - 0 it will be having default image , tag - 1 means user selected image
    ///   - tag: if tag is zero means,user has not chosen any image,or removed earlier image
    func didPickImage(selectedImage: UIImage , tag:Int) {
        
        if tag == 0 {
            wishListVM.wishList.Image = ""
            self.wishListVM.updateWishList()
        }else{
            uploadImage(data:selectedImage)
        }
    }
    //upload selected images
    func uploadImage(data:UIImage){
        wishListVM.wishList.Image = Helper.uploadImage(data:data,path:AmazonKeys.WishList, page: .WishList)
    }
}
