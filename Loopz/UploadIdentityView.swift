//
//  UploadIdentityView.swift
//  UFly
//
//  Created by Rahul Sharma on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
class UploadIdentityView: UIView {
    
    @IBOutlet weak var navigationBackView: UIView!
    @IBOutlet weak var navTitleText: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var yourIdentityLabel: UILabel!
    @IBOutlet weak var uploadIdentityLabel: UILabel!
    @IBOutlet weak var subTittle: UILabel!
    @IBOutlet weak var idCardLabel: UILabel!
    @IBOutlet weak var idTitle: UILabel!
    @IBOutlet weak var mmjCardLabel: UILabel!
    @IBOutlet weak var mmjCardTitle: UILabel!
    @IBOutlet weak var dontHaveMMjLabel: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var doneBtnView: UIView!
    
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var idUploadedLabel: UILabel!
    @IBOutlet weak var mmjUploadedLabel: UILabel!
    
    @IBOutlet weak var mmjCheckMark: UIButton!
    @IBOutlet weak var idCheckMark: UIButton!
    
    @IBOutlet weak var mmjCardImage: UIImageView!
    @IBOutlet weak var idCardImage: UIImageView!
    var pickedIDImage:UIImage? = nil
    var pickedMMJImage:UIImage? = nil
    var isDissmis = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialViewSetup()
    }
    
    /// Initial setup
    func initialViewSetup() {
        
        mmjCheckMark.tintColor =  Colors.AppBaseColor
        idCheckMark.tintColor =  Colors.AppBaseColor
        
        navigationBackView.isHidden = true
        Fonts.setPrimaryMedium(navTitleText)
        navTitleText.text = StringConstants.YourIdentity()
        navTitleText.textColor =  Colors.PrimaryText
        
        Fonts.setPrimaryBlack(yourIdentityLabel)
        Fonts.setPrimaryMedium(uploadIdentityLabel)
        
        Fonts.setPrimaryMedium(idCardLabel)
        Fonts.setPrimaryMedium(mmjCardLabel)
        Fonts.setPrimaryMedium(doneBtn)
        
        Fonts.setPrimaryMedium(idUploadedLabel)
        Fonts.setPrimaryMedium(mmjUploadedLabel)
        
        Fonts.setPrimaryRegular(subTittle)
        Fonts.setPrimaryRegular(idTitle)
        Fonts.setPrimaryRegular(mmjCardTitle)
        Fonts.setPrimaryMedium(skipBtn)
        
        skipBtn.tintColor =  Colors.AppBaseColor
        skipBtn.setTitleColor( Colors.AppBaseColor, for: .normal)
        uploadIdentityLabel.textColor =  Colors.PrimaryText
        dontHaveMMjLabel.textColor =  Colors.PrimaryText
        
        idCardLabel.textColor =  Colors.SeconderyText
        mmjCardLabel.textColor =  Colors.SeconderyText
        
        Helper.setButtonTitle(normal: StringConstants.Done(), highlighted: StringConstants.Done(), selected: StringConstants.Done(), button: doneBtn)
        Helper.setButtonTitle(normal: StringConstants.Skip(), highlighted:StringConstants.Skip(), selected: StringConstants.Skip(), button: skipBtn)
        
        Helper.setButton(button: doneBtn,view:doneBtnView, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
        
        subTittle.textColor =  Colors.SecoundPrimaryText
        idTitle.textColor =  Colors.SecoundPrimaryText
        mmjCardTitle.textColor =  Colors.SecoundPrimaryText
        
        mmjUploadedLabel.textColor =  Colors.AppBaseColor
        idUploadedLabel.textColor =  Colors.AppBaseColor
        
        Helper.setUiElementBorderWithCorner(element: mmjCardImage, radius: 3.0, borderWidth: 0,color: Colors.SeparatorLight)
        Helper.setUiElementBorderWithCorner(element: idCardImage, radius: 3.0, borderWidth: 0, color:  Colors.SeparatorLight)
        
        idUploadedLabel.text = StringConstants.Uploaded()
        mmjUploadedLabel.text = StringConstants.Uploaded()
        
        mmjCardLabel.text = StringConstants.MMJCard()
        idCardLabel.text  = StringConstants.IDCard()
        
        yourIdentityLabel.text = StringConstants.YourIdentity()
        yourIdentityLabel.textColor = Colors.HeadColor
        uploadIdentityLabel.text = StringConstants.UploadId()
        subTittle.text = StringConstants.UploadIDMMJ()
        idTitle.text = StringConstants.UploadIDTitle()
        mmjCardTitle.text = StringConstants.UploadMMj()
        
    }
}


// MARK: - UIScrollViewDelegate
extension UploadIdentityView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
        let movedOffset: CGFloat = sender.contentOffset.y
        if sender == scrollView {
            let pageWidth: CGFloat = 238 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            print("\(movedOffset),\(ratio)")
            if ratio >= 1 {
                navigationBackView.isHidden = false
                navigationBackView.backgroundColor =  Colors.SecondBaseColor
                
                navTitleText.textColor =  Colors.PrimaryText
            }else {
                navigationBackView.isHidden = true
            }
        }
    }
}
