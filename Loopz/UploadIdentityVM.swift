//
//  IdentityVM.swift
//  UFly
//
//  Created by 3 Embed on 08/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class UploadIdentityVM: NSObject {
    
    enum card:Int{
        
        case iDCard  = 100
        case mmjCard = 110
    }
    var authentication = Authentication()
    var btnTag:card?
    var flag = 0
    let pickerObj:ImagePickerModel? = ImagePickerModel.sharedImagePicker
    let disposeBag = DisposeBag()
    let UploadIdVMRespose = PublishSubject<Bool>()
    
    /// this method subscribe for update profile Api defined in EditProfileAPICalls
    func updateProfile(){
        ProfileAPICalls.updateProfileData(user:authentication).subscribe(onNext: { [weak self]success in
                if success {
                    self?.UploadIdVMRespose.onNext(true)
                }
            } , onError: { error in
                print(error)
        }).disposed(by: disposeBag)
    }
}
