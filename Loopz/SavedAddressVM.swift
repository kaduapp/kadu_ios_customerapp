//
//  SavedAddressVM.swift
//  UFly
//
//  Created by 3Embed on 13/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SavedAddressVM: NSObject {
    var disposeBag = DisposeBag()
    var arrayOfddress = [Address]()
    enum ResponseType:Int {
        case Load       = 1
        case Delete     = 2
        case Update     = 3
        case Add        = 4
    }
    var loaded = false
    static let addressVM_response = PublishSubject<(ResponseType,Int,Address)>()
    
    func getAddress() {
        Helper.showPI(string: StringConstants.GettingAddress())
        AddressAPICalls.getAddress().debug().subscribe(onNext: {result in
            self.arrayOfddress = result
           self.loaded = true
            SavedAddressVM.addressVM_response.onNext((ResponseType.Load,0,Address.init(data: [:])))
        }, onError: {error in
            self.loaded = true
            SavedAddressVM.addressVM_response.onNext((ResponseType.Load,0,Address.init(data: [:])))
            print(error)
        }).disposed(by: disposeBag)
    }
    
    func deleteAddress(tag:Int) {
        Helper.showPI(string: StringConstants.DeletingAddress())
        if arrayOfddress.count < tag {
            return
        }
        AddressAPICalls.deleteAddress(address: (arrayOfddress[tag])).debug().subscribe(onNext: {result in
            if self.arrayOfddress.count >= tag + 1 {
            SavedAddressDBManager().deleteAddress(data: self.arrayOfddress[tag])
            }
            SavedAddressVM.addressVM_response.onNext((ResponseType.Delete,tag,Address.init(data: [:])))
        }, onError: {error in
            print(error)
        }).disposed(by: disposeBag)
    }
    
    func addAddress(location:Location,address:Address, finish:@escaping (Bool) -> Void) {
        AddressAPICalls.addAddress(location:location, address: address).debug().subscribe(onNext: {result in
            if result.Id.length > 0 {
                SavedAddressVM.addressVM_response.onNext((ResponseType.Add,0,result))
                finish(true)
            }else{
                finish(false)
            }
            
        }, onError: {error in
            print(error)
            finish(false)
        }).disposed(by: disposeBag)
    }
    
    func updateAddress(location:Location,address:Address, finish:@escaping (Bool) -> Void) {
        AddressAPICalls.updateAddress(location:location, address: address).debug().subscribe(onNext: {result in
            if result.Id.length > 0 { SavedAddressVM.addressVM_response.onNext((ResponseType.Update,0,result))
            finish(true)
            }else{
                finish(false)
            }
        }, onError: {error in
            print(error)
            finish(false)
        }).disposed(by: disposeBag)
    }
    
}
