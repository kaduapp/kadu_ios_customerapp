
//
//  OTPTFE.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//
import UIKit

extension OTPVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if oneTouchOTP.count == 6{
            
            return false
        }
        
        if textField == otpView.textFieldOtp0 && string.count == 1{
            
            oneTouchOTP =  oneTouchOTP + string
            if oneTouchOTP.count == 6{
                self.updateOtpTextfields(otpValue: oneTouchOTP)
                return false
            }
        }
        
        if range.length == 0 {
            
           
        }else {
            
            Helper.setButton(button: otpView.verifyButton,view: otpView.verifyButtonView, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
            
        }
        
        // This allows numeric text only, but also backspace for deletes
        if string.sorted().count > 0 && !Helper.isNumber(text: string) {
            return false
        }
        // Hide Keyboard when you enter last digit
        if textField.isEqual(otpView.textFieldOtp5) && !string.isEmpty {
            otpView.tfSeparator5.backgroundColor =  Colors.AppBaseColor
            textField.text = string
            textField.resignFirstResponder()
            Helper.setButton(button: otpView.verifyButton,view: otpView.verifyButtonView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
            if oneTouchOTP.count == 6{
                return false
            }
            self.didTapVerifyButton(UIButton())
        }
        
        oldLength = textField.text!.sorted().count
        replacementLength = string.sorted().count
        let rangeLength = range.length
        let newLength = oldLength - rangeLength + replacementLength
        
        // This 'tabs' to next field when entering digits
        if (newLength == 1)||(replacementLength == 1) {
            str = string
            if textField.isEqual(otpView.textFieldOtp0) {
                otpView.tfSeparator0.backgroundColor =  Colors.AppBaseColor
                if oneTouchOTP.count == 1{
            self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp1, afterDelay: 0.05)
                }
            }
            else if textField.isEqual(otpView.textFieldOtp1) {
                otpView.tfSeparator1.backgroundColor =  Colors.AppBaseColor
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp2, afterDelay: 0.05)
            }
            else if textField.isEqual(otpView.textFieldOtp2) {
                otpView.tfSeparator2.backgroundColor =  Colors.AppBaseColor
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp3, afterDelay: 0.05)
            }
            else if textField.isEqual(otpView.textFieldOtp3) {
                otpView.tfSeparator3.backgroundColor =  Colors.AppBaseColor
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp4, afterDelay: 0.05)
            }
            else if textField.isEqual(otpView.textFieldOtp4) {
                otpView.tfSeparator4.backgroundColor =  Colors.AppBaseColor
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp5, afterDelay: 0.05)
            }
        }
            //this goes to previous field as you backspace through them, so you don't have to tap into them individually
        else if (oldLength > 0 && newLength == 0)||(replacementLength == 0)  && oneTouchOTP.count != 6{
            str = ""
            if textField.isEqual(otpView.textFieldOtp5) {
                otpView.tfSeparator5.backgroundColor =  Colors.SeparatorLight
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp4, afterDelay: 0.05)
            }
            else if textField.isEqual(otpView.textFieldOtp4) {
                otpView.tfSeparator4.backgroundColor =  Colors.SeparatorLight
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp3, afterDelay: 0.05)
            }
            else if textField.isEqual(otpView.textFieldOtp3) {
                otpView.tfSeparator3.backgroundColor =  Colors.SeparatorLight
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp2, afterDelay: 0.05)
            }
                //
            else if textField.isEqual(otpView.textFieldOtp2) {
                otpView.tfSeparator2.backgroundColor =  Colors.SeparatorLight
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp1, afterDelay: 0.05)
            }
            else if textField.isEqual(otpView.textFieldOtp1) {
                otpView.tfSeparator1.backgroundColor =  Colors.SeparatorLight
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: otpView.textFieldOtp0, afterDelay: 0.05)
            }
        }
        return newLength <= 1
    }
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        Helper.addDoneButtonOnTextField(tf: textField, vc: self.otpView)

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
//        if textField.text?.count == 6
//        {
//        self.updateOtpTextfields(otpValue:textField.text!)
//        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    @objc func setNextResponder(nextResponder: UITextField) {
        
        if oneTouchOTP.count > 1{
            nextResponder.resignFirstResponder()
        }
        else{
            nextResponder.becomeFirstResponder()
            if oldLength == 1 && replacementLength != 0 {
                nextResponder.text = str
            }
            switch nextResponder {
            case otpView.textFieldOtp0:
                break
            case otpView.textFieldOtp1:
                break
            case otpView.textFieldOtp2:
                break
            case otpView.textFieldOtp3:
                break
            case otpView.textFieldOtp4:
                break
            case otpView.textFieldOtp5:
                otpView.tfSeparator5.backgroundColor =  Colors.AppBaseColor
                break
            default:
                break
            }
            
        }
        
      
    
    }
    func keyboardInputShouldDelete(_ textField: UITextField) -> Bool {
    
        switch textField {
        case otpView.textFieldOtp5:
            otpView.tfSeparator5.backgroundColor =  Colors.SeparatorLight
            otpView.textFieldOtp4.becomeFirstResponder()
         break
        case otpView.textFieldOtp4:
            otpView.tfSeparator4.backgroundColor =  Colors.SeparatorLight
            otpView.textFieldOtp3.becomeFirstResponder()
            break
        case otpView.textFieldOtp3:
            otpView.tfSeparator3.backgroundColor =  Colors.SeparatorLight
            otpView.textFieldOtp2.becomeFirstResponder()
            break
        case otpView.textFieldOtp2:
            otpView.tfSeparator2.backgroundColor =  Colors.SeparatorLight
            otpView.textFieldOtp1.becomeFirstResponder()
          break
        case otpView.textFieldOtp1:
            otpView.tfSeparator1.backgroundColor =  Colors.SeparatorLight
            otpView.textFieldOtp0.becomeFirstResponder()
         break
        default:
            break
        }
        return true
    }
    
   @objc func textFieldDidChange(_ sender : UITextField) {
        if sender.text?.sorted().count != 0 {
            if sender == otpView.textFieldOtp5 && oneTouchOTP.count != 6
            {
                sender.resignFirstResponder()
                self.didTapVerifyButton(UIButton())
            }
            
        }
        else{
            switch sender {
            case otpView.textFieldOtp5:
                otpView.tfSeparator5.backgroundColor =  Colors.SeparatorLight
                break
            case otpView.textFieldOtp4:
                otpView.tfSeparator4.backgroundColor =  Colors.SeparatorLight
                break
            case otpView.textFieldOtp3:
                otpView.tfSeparator3.backgroundColor =  Colors.SeparatorLight
                break
            case otpView.textFieldOtp2:
                otpView.tfSeparator2.backgroundColor =  Colors.SeparatorLight
                break
            case otpView.textFieldOtp1:
                otpView.tfSeparator1.backgroundColor =  Colors.SeparatorLight
                break
            case otpView.textFieldOtp0:
                otpView.tfSeparator0.backgroundColor =  Colors.SeparatorLight
                break
            default:
                break
            }
        }
    }
    
    
    /// Dismiss Keyboard
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    
    func updateOtpTextfields(otpValue:String){
         otpView.textFieldOtp0.text = ""
         otpView.textFieldOtp1.text = ""
         otpView.textFieldOtp2.text = ""
        let otpArray = Array(otpValue)
        otpView.textFieldOtp0.text = String(otpArray[0])
        otpView.tfSeparator0.backgroundColor =  Colors.AppBaseColor
       
        otpView.textFieldOtp1.text = String(otpArray[1])
        otpView.tfSeparator1.backgroundColor =  Colors.AppBaseColor
        
        otpView.textFieldOtp2.text = String(otpArray[2])
        otpView.tfSeparator2.backgroundColor =  Colors.AppBaseColor
        
        otpView.textFieldOtp3.text = String(otpArray[3])
        otpView.tfSeparator3.backgroundColor =  Colors.AppBaseColor
        
        otpView.textFieldOtp4.text = String(otpArray[4])
        otpView.tfSeparator4.backgroundColor =  Colors.AppBaseColor
        
        otpView.textFieldOtp5.text = String(otpArray[5])
        otpView.tfSeparator5.backgroundColor =  Colors.AppBaseColor
    
        Helper.setButton(button: otpView.verifyButton,view: otpView.verifyButtonView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        
    }
    
}


class OTPTextField: UITextField {

    override func deleteBackward() {
     self.text = ""
   // NotificationCenter.default.post(name: .deleteEmptyTextField, object: self)
    }
    
}
