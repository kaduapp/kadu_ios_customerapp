//
//  SignInTFE.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - UITextFieldDelegate
extension SignInVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.signInView.phoneNumTF {
            self.signInView.passwordTF.becomeFirstResponder()
            self.signInView.pwSeparator.backgroundColor =  Colors.AppBaseColor
        } else {
            self.signInView.pwSeparator.backgroundColor =  Colors.SeparatorLight
            self.signInView.passwordTF.resignFirstResponder()
            if textField.text?.sorted().count != 0 {
                checkForData()
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.signInView.phoneNumTF {
            
            self.signInView.phSeperartor.backgroundColor =  Colors.AppBaseColor
            self.signInView.pwSeparator.backgroundColor =  Colors.SeparatorLight
        } else {
            
            self.signInView.phSeperartor.backgroundColor =  Colors.SeparatorLight
            self.signInView.pwSeparator.backgroundColor =  Colors.AppBaseColor
        }
        
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.signInView.phoneNum.append(string)
        
        if textField == signInView.phoneNumTF  {
            if range.location+1 == self.signinVM.authentication.CountryCode.sorted().count{
                if range.location+1 == self.signinVM.authentication.CountryCode.sorted().count {
                    return false
                }
                return true
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if self.signInView.scrollView.frame.size.height < self.signInView.scrollView.contentSize.height {
            
            UIView.animate(withDuration: 0.5){
                self.signInView.scrollView.contentOffset.y = self.signInView.scrollView.frame.size.height - self.signInView.scrollView.contentSize.height
            }
        }
        if textField == self.signInView.phoneNumTF {
            signInView.countryCodeBtn.isEnabled = true
            Helper.addDoneButtonOnTextField(tf: textField, vc: self.signInView)
            self.signInView.phSeperartor.backgroundColor =  Colors.AppBaseColor
            self.signInView.pwSeparator.backgroundColor =  Colors.SeparatorLight
            
            if textField.text?.sorted().count == 0 {
                textField.text = signinVM.authentication.CountryCode
            }
        }else{
            
            self.signInView.phSeperartor.backgroundColor =  Colors.SeparatorLight
            self.signInView.pwSeparator.backgroundColor =  Colors.AppBaseColor
        }
        
        signInView.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == self.signInView.phoneNumTF {
            let count = self.signinVM.authentication.CountryCode.sorted().count
            let phoneNumber = signInView.phoneNumTF.text?.replacingOccurrences(of: signinVM.authentication.CountryCode, with: "")
            if Helper.isPhoneNumValid(phoneNumber: phoneNumber!, code: signinVM.authentication.CountryCode) && (textField.text!.sorted().count) > count {
                self.signInView.phNumTickMark.isHidden = false
            }else{
                self.signInView.phNumTickMark.isHidden = true
                
            }
            signinVM.authentication.Phone = phoneNumber!
            signinVM.showAlert = false
            
        }else{
            if (self.signInView.passwordTF.text?.sorted().count)! >= 6 {
                self.signInView.pwNumTickMark.isHidden = false
            }else{
                self.signInView.pwNumTickMark.isHidden = true
            }
            signinVM.authentication.Password = self.signInView.passwordTF.text!
            signinVM.showAlert = true
        }
        signInView.activeTextField = nil
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        if textField == self.signInView.phoneNumTF{
            
            let count = self.signinVM.authentication.CountryCode.sorted().count
            let phoneNumber = signInView.phoneNumTF.text?.replacingOccurrences(of: signinVM.authentication.CountryCode, with: "")
            if Helper.isPhoneNumValid(phoneNumber: phoneNumber!, code: self.signinVM.authentication.CountryCode) && (textField.text!.sorted().count) > count {
                self.signInView.phNumTickMark.isHidden = false
                Helper.setButton(button: signInView.continueBtn,view: signInView.continueBtnContainer, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
                signInView.continueBtn.isUserInteractionEnabled = true
            }else{
                self.signInView.phNumTickMark.isHidden = true
                Helper.setButton(button: signInView.continueBtn,view: signInView.continueBtnContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
                signInView.continueBtn.isUserInteractionEnabled = false
                
            }
            self.signInView.passwordTFHight.constant = 0
            self.signInView.updateConstraints()
            self.signInView.layoutIfNeeded()
            self.signinVM.authentication.ValidPhone = false
            
        }else if textField == self.signInView.passwordTF {
            
            if (self.signInView.passwordTF.text?.sorted().count)! >= 6 {
                self.signInView.pwNumTickMark.isHidden = false
                Helper.setButton(button: signInView.continueBtn,view: signInView.continueBtnContainer, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
                signInView.continueBtn.isUserInteractionEnabled = true
            }else{
                self.signInView.pwNumTickMark.isHidden = true
                Helper.setButton(button: signInView.continueBtn,view: signInView.continueBtnContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
                signInView.continueBtn.isUserInteractionEnabled = false
            }
        }
    }
}
