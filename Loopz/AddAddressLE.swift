//
//  AddAddressLE.swift
//  UFly
//
//  Created by 3Embed on 15/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

import GoogleMaps
import GooglePlaces
import CoreLocation

//MARK:- LocationManager Delegate
extension AddAddressVC : LocationManagerDelegate {
    func didFailToUpdateLocation() {
        
    }
    // func didUpdateLocation(location: Location, search: Bool) {
    func didUpdateLocation(location: Location, search:Bool,update:Bool) {
        let locationin = CLLocation(latitude: CLLocationDegrees(location.Latitude), longitude: CLLocationDegrees(location.Longitude))
        self.mapView.animate(to: GMSCameraPosition(target: locationin.coordinate, zoom: zoomLevelofMap, bearing: 0, viewingAngle: 0))
        self.addressTF.text = AddressUtility.getSortedAddressFromLocation(location)
        addressSelected = location
        self.view.endEditing(true)
        flagMapChange = true
    }
    
    func didChangeAuthorization(authorized: Bool) {
        self.mapView.settings.myLocationButton = true
    }
    
    func didUpdateSearch(locations: [Location]) {
        
        addressList = locations
        searchTableView.reloadData()
        
        
    }
}

extension AddAddressVC : GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        zoomLevelofMap = mapView.camera.zoom
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        zoomLevelofMap = mapView.camera.zoom
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        if flagMapChange {
            Helper.hidePI()
            self.addressTF.text = AddressUtility.getSortedAddressFromLocation(addressSelected)
            let latitude: CLLocationDegrees = position.target.latitude
            let longitude: CLLocationDegrees = position.target.longitude
            if addressSelected.Latitude != Float(latitude) || addressSelected.Longitude != Float(longitude) {
                addressSelected.LocationDescription = ""
                addressSelected.MainText = ""
                addressSelected.PlaceId = ""
                let location: CLLocation = CLLocation(latitude: latitude, longitude: longitude)
                locationManager?.updateLocationData(location: location, placeIn: addressSelected,flag: false)
            }
        }
        flagMapChange = true
        zoomLevelofMap = mapView.camera.zoom
    }
}
