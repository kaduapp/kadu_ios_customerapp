//
//  RestuarantsStoreVM.swift
//  DelivX
//
//  Created by 3Embed on 17/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class RestuarantsStoreVM: NSObject {
    
    let disposeBag = DisposeBag()
    let restuarantsStoreVM_response = PublishSubject<Bool>()
    let ProductFav_response = PublishSubject<Bool>()
    var store = Store.init(data: [:])
    var categories:[Category] = []
    var recomendedProducts:[Item] = []
    var favProducts : [Item] = []
    var skip:Int = 0
    var limit:Int = 100
    var List_Store = Store.init(data: [:])
    
    func getData() {
        HomeAPICalls.getRestuarantStore(skip:skip,limit:limit).subscribe(onNext: {[weak self]result in
            self?.store = result.0
            self?.categories = result.1
            self?.recomendedProducts = result.2
            self?.favProducts = result.3
            self?.restuarantsStoreVM_response.onNext(true)
            }, onError: {error in
        }).disposed(by: disposeBag)
    }
    
    
    func setFavProductSubscribe() {
        FavouriteAPICalls.favVMResponse.subscribe(onNext: { [weak self]data in
            if data.1 == 2 {
                self?.favProducts.removeAll()
                self?.favProducts.append(contentsOf: data.0)
              self?.ProductFav_response.onNext(true)
            } else {
                if self?.store.Id == data.0[0].StoreId {
                    if data.1 == 1 {
                        self?.favProducts.append(data.0[0])
              self?.ProductFav_response.onNext(true)
                    }else if data.1 == 0 {
                        for item in (self?.favProducts)! {
                            if item.Id == data.0[0].Id {
                                self?.favProducts.remove(at: (self?.favProducts.index(of: item))!)
                            }
                        }
              self?.ProductFav_response.onNext(true)
                    }
                }
            }
        }).disposed(by: self.disposeBag)
    }
        
}
