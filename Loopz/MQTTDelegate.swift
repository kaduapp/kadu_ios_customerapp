//
//  MessageNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Nabeel Gulzar on 31/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import RxCocoa
import RxSwift


class MQTTDelegate: NSObject, MQTTSessionManagerDelegate {
    
    static var mqttMessageHandler = MQTTChatResponseHandler()
    static let message_response = PublishSubject<MQTTMessageModel>()
    let disposeBag = DisposeBag()
    func handleMessage(_ data: Data!, onTopic topic: String!, retained: Bool) {
        // self.mqttHandler.gotResponeFromMqtt(responseData: data, topicChannel: topic)MQTT-
    
        if Utility.getProfileData().MQTTTopic != topic && topic != Utility.getProfileData().googleMapKeyMqtt {
            if topic == "message/\(Utility.getProfileData().SId)" {
                MQTTDelegate.mqttMessageHandler.gotChatResponeFromMqtt(responseData: data, topicChannel: topic)
                return
            }
            let mqtt = MQTT.sharedInstance
            mqtt.unsubscribeTopic(topic: topic)
            return
        }

        do {
            guard let mqttData = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            print("Received \(mqttData) on:\(String(describing: topic)) r\(retained)")
            
            // update the new google key here
            if topic == Utility.getProfileData().googleMapKeyMqtt,let status = mqttData["status"] as? Int, status == 100,let newKey = mqttData["googleMapKey"] as? String{
                
                if let data  = UserDefaults.standard.object(forKey: UserDefaultConstants.AppConfig) as? [String : Any] {
                    var userData = data
                  userData.updateValue(newKey, forKey:"googleMapKey" )
                  Utility.setAppConfig(data: userData)
                }
                
                return
            }
            
            
            var action = 0
            if let actionInt = mqttData["action"] as? Int
            {
                action = actionInt
            }
            if let actionString = mqttData["action"] as? String
            {
                action = Int(actionString)!
            }
            if action == 12 && (mqttData["deviceId"] as? String) != Utility.DeviceId && (mqttData["token"] as? String) != Utility.getSessionToken() {
                if let msg = mqttData[APIResponceParams.Message] as? String{
                    Utility.logout(type: 1,message: msg)
                }else{
                    Utility.logout(type: 1,message: "")
                }
                
                return
            } else if (mqttData["action"] as? Int) == 18 {
                ProfileAPICalls.getProfile().subscribe().disposed(by: self.disposeBag)
            }
            let data = MQTTMessageModel(data:mqttData)
            if data.Status == 15 || data.Status == 7 {
                let order = Order.init(data: mqttData)
                Helper.openReview(controller: Helper.finalController(), order: order)
            }else if data.Status == 8 || data.Status == 10 || data.Action == 14 {
                data.updateDriver(data: mqttData)
            }
            MQTTDelegate.message_response.onNext(data)
            
        } catch let jsonError {
            print("Error !!!\(jsonError)")
        }
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didDeliverMessage msgID: UInt16) {
    }
    
    func messageDelivered(_ session: MQTTSession!, msgID: UInt16, topic: String!, data: Data!, qos: MQTTQosLevel, retainFlag: Bool) {
        print( "\(msgID)Message delivered")
        
        let userID = ""
        session.persistence.deleteAllFlows(forClientId: userID)
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didChange newState: MQTTSessionManagerState) {
        switch newState {
        case .connected:
            print("connected")
            let mqtt = MQTT.sharedInstance
            let mqttTopicToken = Utility.getProfileData().MQTTTopic
            mqtt.isConnected = true
            
            if Utility.getProfileData().googleMapKeyMqtt.length > 0{
        mqtt.subscribeTopic(withTopicName:Utility.getProfileData().googleMapKeyMqtt, withDelivering: .exactlyOnce)
            }
            
            if mqttTopicToken.length > 0 && Utility.getLogin() {
                mqtt.subscribeTopic(withTopicName: mqttTopicToken, withDelivering: .exactlyOnce)
                mqtt.subscribeTopic(withTopicName: "message/" + Utility.getProfileData().SId, withDelivering: .exactlyOnce)
            }else{
                mqtt.unsubscribeTopic(topic: mqttTopicToken)
                mqtt.unsubscribeTopic(topic: "message/" + Utility.getProfileData().SId)
              mqtt.unsubscribeTopic(topic:Utility.getProfileData().googleMapKeyMqtt )
            }
            
        case .closed:
            print("disconnected")
        case .starting:
            print("Starting...")
            
            break
            
        case .connecting:
            print("Connecting...")
            
            break
            
        case .error:
            print("Error...")
            break
            
        case .closing:
            print("Closing...")
            break
            
        }
    }
}

