//
//  HistoryVCE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension HistoryVC {
    /// reloads the view
    ///
    /// - Parameter sender: is of type UIRefreshControl
    @objc func refresh(sender:UIRefreshControl) {
        
        if Utility.getLogin() {
            if (isFromProfile)
            {
                historyVM.historyIn(data:2, storeType: -1)
                historyVM.historyIn(data:1, storeType: -1)
                
            }else {
                
                if sender == historyVM.refreshControlActive {
                    historyVM.historyIn(data:2, storeType: Utility.getSelectedSuperStores().type.rawValue)
                }else{
                    historyVM.historyIn(data:1, storeType: Utility.getSelectedSuperStores().type.rawValue)
                }
                
                
                
            }
            
            self.historyVM.refreshControlActive.endRefreshing()
            self.historyVM.refreshControlPast.endRefreshing()
            
        }else {
            
            historyVM.arrayOfActiveOrder = []
            historyVM.arrayOfPastOrder = []
            pastOrderTableView.reloadData()
            activeOrderTableView.reloadData()
            self.historyVM.refreshControlActive.endRefreshing()
            self.historyVM.refreshControlPast.endRefreshing()
            
        }
    }
    
        
}
