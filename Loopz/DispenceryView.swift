//
//  DispenceryView.swift
//  DelivX
//
//  Created by 3Embed on 16/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class DispenceryView: UIView {

    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var StoreImage: UIImageView!
    @IBOutlet weak var StoreName: UILabel!
    @IBOutlet weak var StoreAddress: UILabel!
    @IBOutlet weak var MilesValue: UILabel!
    @IBOutlet weak var MilesHead: UILabel!
    @IBOutlet weak var RatingValue: UILabel!
    @IBOutlet weak var RatingHead: UILabel!
    @IBOutlet weak var ETAValue: UILabel!
    @IBOutlet weak var ETAHead: UILabel!
    
    @IBOutlet weak var changeButtonWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var changeButtonView: UIView!
    @IBOutlet weak var storeChangeButton: UIButton!
    
    var obj: DispenceryView? = nil
    var shared: DispenceryView {
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[7] as? DispenceryView
            obj?.frame = UIScreen.main.bounds
            /*
            Fonts.setPrimaryBold(obj?.MilesHead as Any)
            Fonts.setPrimaryBold(obj?.ETAHead as Any)
            Fonts.setPrimaryBold(obj?.RatingHead as Any)
            Fonts.setPrimaryBold(obj?.RatingValue as Any)
            Fonts.setPrimaryBold(obj?.MilesValue as Any)
            Fonts.setPrimaryBold(obj?.ETAValue as Any)
            Fonts.setPrimaryMedium(obj?.StoreName as Any)
            Fonts.setPrimaryRegular(obj?.StoreAddress as Any)
            */
            
            Fonts.setPrimaryRegular(obj?.MilesHead as Any, size: 10)
            Fonts.setPrimaryRegular(obj?.ETAHead as Any, size: 10)
            Fonts.setPrimaryRegular(obj?.RatingHead as Any, size: 10)
            Fonts.setPrimaryRegular(obj?.RatingValue as Any, size: 10)
            Fonts.setPrimaryRegular(obj?.MilesValue as Any, size: 10)
            Fonts.setPrimaryRegular(obj?.ETAValue as Any, size: 10)
            Fonts.setPrimaryBold(obj?.StoreName as Any, size: 16)
            Fonts.setPrimaryRegular(obj?.StoreAddress as Any, size: 12)
            Fonts.setPrimaryRegular(obj?.storeChangeButton as Any, size: 10)

            
            
            obj?.ETAHead.text = StringConstants.Reviews()
            obj?.ETAHead.textColor = Colors.SecoundPrimaryText
            obj?.RatingHead.text = StringConstants.Rating()
            obj?.RatingHead.textColor = Colors.SecoundPrimaryText
            obj?.MilesHead.text = StringConstants.MileAway()
            obj?.ETAHead.textColor = Colors.SecoundPrimaryText
            
            obj?.StoreName.textColor = Colors.PrimaryText
            obj?.StoreAddress.textColor = Colors.SecoundPrimaryText
            obj?.StoreName.text = StringConstants.StoreSelected().uppercased()

            if let button = obj?.storeChangeButton, let buttonView = obj?.changeButtonView
            {
                   Helper.setButton(button: button, view: buttonView, primaryColour: Colors.SecondBaseColor, seconderyColor: Colors.AppBaseColor, shadow: false)
                
                Helper.setButtonTitle(normal: StringConstants.ChangeLabel(), highlighted: StringConstants.ChangeLabel(), selected: StringConstants.ChangeLabel(), button: button)
            }
            
         
//            if let image = obj?.StoreImage
//            {
//                 Helper.setUiElementBorderWithCorner(element: image, radius: Helper.setRadius(element: image), borderWidth: 1, color: Colors.AppBaseColor)
//            }
         
           
        }
        return obj!
    }
    
    func setData(data:Store)
    {
        StoreName.text = data.Name
        StoreAddress.text = data.Address
        //MilesValue.text = data.DistanceKm
        MilesValue.text = Helper.clipDigit(valeu: data.DistanceMiles, digits: 2)
        ETAValue.text = data.EstimatedTime
        RatingValue.text  = String(data.StoreRating)
        Helper.setImage(imageView: StoreImage, url: data.Image , defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
    }
    
    
}
