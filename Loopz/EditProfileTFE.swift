//
//  EditProfileTFE.swift
//  UFly
//
//  Created by 3 Embed on 30/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - UITextFieldDelegate
extension EditProfileVC : UITextFieldDelegate {
 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        editProfileVIew.phoneNumber.append(string)
        
        if textField == editProfileVIew.phoneNum {
            if range.location+1 == self.editProfileVM.authentication.CountryCode.length{
                if range.location+1 == self.editProfileVM.authentication.CountryCode.length {
                    return false
                }
                return true
            }
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField ==  editProfileVIew.phoneNum  {
            Helper.addDoneButtonOnTextField(tf: textField, vc: self.editProfileVIew)
            if textField.text?.length == 0 {
                textField.text = editProfileVM.authentication.CountryCode
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
   
    switch textField {
        
        case editProfileVIew.nameTF:
            editProfileVM.authentication.Name = editProfileVIew.nameTF.text!
            break
        
        case editProfileVIew.phoneNum:
            let phoneNumber = editProfileVIew.phoneNum.text?.replacingOccurrences(of: editProfileVM.authentication.CountryCode, with: "")
            if (textField.text?.length)! > 0{
                
                guard Helper.isPhoneNumValid(phoneNumber: phoneNumber!, code: editProfileVM.authentication.CountryCode) == true else {
                    Helper.showAlert(message: StringConstants.ValidPhoneNumber() , head: StringConstants.Error(), type: 1)
                    return
                }
            }
            
            editProfileVM.authentication.Phone = phoneNumber!
            break
        
        default:
            guard Helper.isValidEmail(text: editProfileVIew.emailTF.text!) == true else {
                Helper.showAlert(message: StringConstants.ValidEmail() , head: StringConstants.Error(), type: 1)
                return
            }
            editProfileVM.authentication.Email = editProfileVIew.emailTF.text!
            break
        }
      
    }
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case editProfileVIew.nameTF:
            
            editProfileVIew.phoneNum.becomeFirstResponder()
            break
        case editProfileVIew.phoneNum:
           editProfileVIew.emailTF.becomeFirstResponder()
            break
        case editProfileVIew.emailTF:
           self.dismissKeyboard()
            break
        default: break
        }
        return true
    }
}
