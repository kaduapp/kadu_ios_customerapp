//
//  AddOnVC.swift
//  DelivX
//
//  Created by 3Embed on 01/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AddOnVC: UIViewController {
    
    @IBOutlet weak var tableListView: UIView!
    @IBOutlet weak var illchooseView: UIView!
    @IBOutlet weak var illcooseButton: UIButton!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var repeatButtonView: UIView!
    @IBOutlet weak var selectedAddOnsLabel: UILabel!
    @IBOutlet weak var repeatTitle: UILabel!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var selectedAddOns: UIView!
    @IBOutlet weak var confirmPrice: UILabel!
    @IBOutlet weak var confirmAddItem: UILabel!
    @IBOutlet weak var addOnsTitleView: UIView!
    @IBOutlet weak var addOnsTitlePrice: UILabel!
    @IBOutlet weak var AddOnsTitleLabel: UILabel!
    @IBOutlet weak var selectedAddOnsPreviewLabel: UILabel!
    
    var addOnVM = AddOnVM()
    var fromCart = false
    override func viewDidLoad() {
        super.viewDidLoad()
        addOnVM.getTheAddOnList()
        tableViewHeight.constant = 0
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        addObserverToAddOn()
        // Do any additional setup after loading the view.
        setUpUI()
    }
    
    @IBAction func repeatAction(_ sender: Any) {
       self.addOnVM.AddOnsVM_cartUpdateResponse.onNext(.Patch)
  self.addOnVM.AddOnsVM_cartUpdateResponseFromCart.onNext((.Patch,addOnVM.SelectedItem))
    }
    
    
    @IBAction func illchooseAction(_ sender: Any) {
        addOnVM.ArrayOfAddons.removeAll()
        addOnVM.ArrayOfAddons.append(contentsOf: addOnVM.SelectedItem.AddOnGroups)
        self.setMandatoryAddon()
        self.mainTableView.reloadData()
        tableListView.isHidden = false
        customView.isHidden = true
        var numOfcells:Int = 0
        for eachSection in addOnVM.ArrayOfAddons{
            numOfcells = numOfcells + eachSection.AddOns.count
        }
        tableViewHeight.constant = CGFloat(addOnVM.ArrayOfAddons.count * 50 + numOfcells * 50)
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
    @IBAction func confirmAction(_ sender: UIButton) {
        
        if addOnVM.isMandatoryAddOnsAdded(){
        if !fromCart {
            self.addOnVM.AddOnsVM_cartUpdateResponse.onNext(.Add)
        self.addOnVM.AddOnsVM_cartUpdateResponseFromCart.onNext((.Add,addOnVM.SelectedItem))
        }else{
             addOnVM.customize()
        }
        }
    }
    @IBAction func closeAction(_ sender: UIButton) {
        self.addOnVM.AddOnsVM_cartUpdateResponse.onNext(.Close)
        self.dismiss(animated: true, completion: nil)
    }
    func addObserverToAddOn(){
        addOnVM.AddOnVM_response.subscribe(onNext: {[weak self]success in
            if success == true {
                
                self?.setBasicFunctionality()
                if !self!.fromCart
                {
                self?.mainTableView.reloadData()
                }
            }else{
                self?.dismiss(animated: true, completion: nil)
            }
            }, onError: {error in
                print(error)
        }).disposed(by: self.addOnVM.disposeBag)
    }
}

extension AddOnVC{
    func setUpUI(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        Fonts.setPrimaryBold(AddOnsTitleLabel, size: 16)
        Fonts.setPrimaryBold(confirmPrice, size: 14)
        Fonts.setPrimaryBold(confirmAddItem, size: 14)
        Fonts.setPrimaryBold(addOnsTitlePrice, size: 14)
        AddOnsTitleLabel.textColor = Colors.DarkBlueBlack
        addOnsTitlePrice.textColor = Colors.DarkBlueBlack
        confirmPrice.textColor = UIColor.white
        confirmAddItem.textColor = UIColor.white
        
        AddOnsTitleLabel.text = self.addOnVM.SelectedItem.Name//StringConstants.Addons()
        addOnsTitlePrice.text = Helper.df2so(Double(self.addOnVM.SelectedItem.Price))
        
        confirmPrice.text = StringConstants.Item() + " " + StringConstants.total().lowercased() + " " + Helper.df2so(Double(self.addOnVM.SelectedItem.Price))
        
        if fromCart{
            confirmAddItem.text = StringConstants.Update().uppercased() + " " + StringConstants.Item().uppercased()
        }
        else{
        confirmAddItem.text = StringConstants.AddCart().uppercased() + " " + StringConstants.Item().uppercased()
        }
        
        addOnsTitleView.layer.cornerRadius = 10
        addOnsTitleView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
        addOnsTitleView.backgroundColor = Colors.lightScreenBackground
        confirmView.backgroundColor = Colors.AppBaseColor
        confirmView.layer.cornerRadius = CGFloat(UIConstants.ButtonCornerRadius)
        confirmView.layer.borderWidth = CGFloat(0)
        Helper.setShadow(sender: confirmView)
        Helper.setButton(button: illcooseButton, view: illchooseView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor, shadow: true)
        Helper.setButtonTitle(normal: StringConstants.IllChoose(), highlighted: StringConstants.IllChoose(), selected: StringConstants.IllChoose(),button: illcooseButton)
        
        Helper.setButton(button: repeatButton, view: repeatButtonView, primaryColour: Colors.SecondBaseColor, seconderyColor: Colors.AppBaseColor, shadow: true)
        Helper.setButtonTitle(normal: StringConstants.RepeatLast(), highlighted: StringConstants.RepeatLast(), selected: StringConstants.RepeatLast(),button: repeatButton)
        repeatTitle.textColor = Colors.PrimaryText
        selectedAddOnsPreviewLabel.textColor = Colors.SecoundPrimaryText
        selectedAddOnsLabel.textColor = Colors.SecoundPrimaryText
        self.selectedAddOnsLabel.text = ""

        Fonts.setPrimaryBold(repeatTitle)
        repeatTitle.text = StringConstants.Repeatcustomization()
        Fonts.setPrimaryRegular(selectedAddOnsLabel)
        if self.getQuantityOfItem() == 0{
            tableListView.isHidden = false
            customView.isHidden = true
        }else{
            tableListView.isHidden = true
            customView.isHidden = false
        }
    }
    
    func setBasicFunctionality(){
        if self.getQuantityOfItem() == 0 {
            addOnVM.ArrayOfAddons.removeAll()
            var row = 0,section = 0
            var arrayselectedIndex:[IndexPath] = []
            for (indx,each) in addOnVM.SelectedItem.AddOnGroups.enumerated() {
                var addonGroup: AddOnGroup = AddOnGroup.init(data: [:])
                addonGroup = each
               
                   section = indx
                    for (index, eachSub) in each.AddOns.enumerated() {
                       
                        if  addOnVM.SelectedItem.RecentAddOns.contains(eachSub.Id)
                        {
                            var addon: AddOn =  AddOn.init(data: [:])
                            addon = eachSub
                            addon.Selected = true
                            print(eachSub.Selected)
                           // previewText = previewText + eachSub.Name + ", "
                                addonGroup.AddOns.remove(at: index)
                             addonGroup.AddOns.insert(addon, at: index)
                            row = index
                            let indexPath = IndexPath(row: row, section: section)
                            arrayselectedIndex.append(indexPath)
                            print(arrayselectedIndex)
                        }else{
                        row = -1
                        }
                    }
                
                
                
                
                addOnVM.ArrayOfAddons.append(addonGroup)
                print(addOnVM.ArrayOfAddons)
            }
           // addOnVM.ArrayOfAddons.append(contentsOf: addOnVM.SelectedItem.AddOnGroups)
            if !fromCart{
                self.setMandatoryAddon()
            }
            else{
              self.setSelectedAddonsPreview()
            }
            self.mainTableView.reloadData()
            tableListView.isHidden = false
            customView.isHidden = true
            //selecting cell
            if fromCart
            {
                for id in arrayselectedIndex
                {
                     self.mainTableView.selectRow(at: id, animated: true, scrollPosition: .none)
                    tableView( self.mainTableView, didSelectRowAt: id)

                }
            }
        }else{
            addOnVM.ArrayOfAddons.removeAll()
            self.mainTableView.reloadData()
            var text = ""
            var previewText = ""
            for each in addOnVM.SelectedItem.AddOnGroups {
                for eachSub in each.AddOns {
                    if addOnVM.SelectedItem.RecentAddOns.contains(eachSub.Id) {
                        if text.length > 0 {
                            text = text + "\n"
                        }
                        
                        text = text + eachSub.Name.trimmingCharacters(in: .whitespaces)
                        print(text)
                    }
                    if eachSub.Selected{
                        previewText = previewText + eachSub.Name + ", "
                    }
                    
                }
            }
            if previewText.length > 0{
                let endIndex = previewText.index(previewText.endIndex, offsetBy: -2)
                previewText = String(previewText[..<endIndex])
            }
            selectedAddOnsPreviewLabel.text = previewText
            selectedAddOnsLabel.text = text
            tableListView.isHidden = true
            customView.isHidden = false
        }
        
        var numOfcells:Int = 0
        for eachSection in addOnVM.ArrayOfAddons{
            numOfcells = numOfcells + eachSection.AddOns.count
        }
        tableViewHeight.constant = CGFloat(addOnVM.ArrayOfAddons.count * 50 + numOfcells * 50)
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
    
    func getQuantityOfItem() -> Int{
        let totalSameCart = CartDBManager().getCartFullCount(data: addOnVM.SelectedItem)
        var tempCount = 0
        for data in totalSameCart {
            tempCount = tempCount + data.QTY
        }
        if fromCart {
            tempCount = 0
        }
        return tempCount
    }
    
    func setMandatoryAddon(){
       var previewText = ""
        for i in 0..<addOnVM.SelectedItem.AddOnGroups.count{
            if addOnVM.SelectedItem.AddOnGroups[i].Mandatory && addOnVM.SelectedItem.AddOnGroups[i].AddOns.count > 0 && !addOnVM.SelectedItem.AddOnGroups[i].multiple{
                addOnVM.SelectedItem.AddOnGroups[i].AddOns[0].Selected = true
                for j in 0..<addOnVM.SelectedItem.AddOnGroups[i].AddOns.count{
                    if  j == 0{
                    addOnVM.SelectedItem.AddOnGroups[i].AddOns[j].Selected = true
                    previewText = previewText + addOnVM.SelectedItem.AddOnGroups[i].AddOns[j].Name + ", "
                    }else{
                        addOnVM.SelectedItem.AddOnGroups[i].AddOns[j].Selected = false
                    }
                }
            }
        }
        if previewText.length > 0{
            let endIndex = previewText.index(previewText.endIndex, offsetBy: -2)
            previewText = String(previewText[..<endIndex])
        }
        selectedAddOnsPreviewLabel.text = previewText
    }
    
    
    func setSelectedAddonsPreview (){
        var previewText = ""
        for each in addOnVM.SelectedItem.AddOnGroups {
            for id in addOnVM.SelectedItem.RecentAddOns
            {
            for eachSub in each.AddOns {
                if  id == eachSub.Id
                {
                    previewText = previewText + eachSub.Name + ", "
                }
            }
                
            }
        }
        
        if previewText.length > 0{
        let endIndex = previewText.index(previewText.endIndex, offsetBy: -2)
         previewText = String(previewText[..<endIndex])
        }
        selectedAddOnsPreviewLabel.text = previewText
    }
}
