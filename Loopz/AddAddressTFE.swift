//
//  LSDataSourceAndDelegates.swift
//  UFly
//
//  Created by Rahul Sharma on 14/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

// MARK: - TextField Delegates

extension AddAddressVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case addressTF:
            textField.text = ""
        default:
            print(textField.text as Any)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == addressTF {
            searchTableView.isHidden = true
        }
    }
    @objc func textFieldDidChange(_ textField : UITextField) {
        if textField == addressTF{
            
            let language = Helper.getKeyboardLanguage(language:(textField.textInputMode?.primaryLanguage)!)
            locationManager?.delegate = self
            locationManager?.search(searchText: textField.text!,language: language)
    
        }
        
        if textField == flatTextField{
          
            self.flatNumber = textField.text!
        }
        if textField == landmarkTextField{
            self.landmark = textField.text!
        }
        
        
        
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
    
    
    
}

