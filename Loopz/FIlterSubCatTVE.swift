//
//  FIlterSubCatTVE.swift
//  DelivX
//
//  Created by 3Embed on 24/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension FilterSubCatVC: UITableViewDelegate,UITableViewDataSource {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfSubsubCat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FilterTVC.self), for: indexPath) as! FilterTVC
        let data = arrayOfSubsubCat[indexPath.row]
        if filterDetailVM.SubSubCatSelected == data.Name {
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        cell.titleLabel.text = data.Name
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if filterDetailVM.SubSubCatSelected == arrayOfSubsubCat[indexPath.row].Name {
            filterDetailVM.SubSubCatSelected = ""
        }else{
            filterDetailVM.SubSubCatSelected = arrayOfSubsubCat[indexPath.row].Name
        }
        searchVMReference.filterInfo.onNext((true,APIRequestParams.SubSubCategoryFilter,[filterDetailVM.SubSubCatSelected]))
        tableView.reloadData()
    }
}
