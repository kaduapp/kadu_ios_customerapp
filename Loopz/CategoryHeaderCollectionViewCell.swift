//
//  CategoryHeaderCollectionViewCell.swift
//  DelivX
//
//  Created by 3EMBED on 15/04/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CategoryHeaderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var Title_Label: UILabel!
    
    @IBOutlet weak var bottomlineView: UIView!
}
