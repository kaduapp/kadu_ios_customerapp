//
//  TouchIDAuthentication.swift
//  TouchMeIn
//
//  Created by Marin Benčević on 21/04/2017.
//  Copyright © 2017 iT Guy Technologies. All rights reserved.
//

import Foundation
import LocalAuthentication

class TouchIDAuth {
  
  static var context = LAContext()
  
  func canEvaluatePolicy() -> Bool {
    return TouchIDAuth.context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
  }
  
  func authenticateUser(completion: @escaping (Bool) -> Void) {
    TouchIDAuth.context = LAContext()
    guard canEvaluatePolicy() else {
//        completion(nil)
        return
    }
    TouchIDAuth.context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
      localizedReason: "Logging in with TouchID") {
        (success, evaluateError) in
        if success {
          DispatchQueue.main.async {
            // User authenticated successfully, take appropriate action
            completion(true)
          }
        } else {
        }
    }
  }
}
