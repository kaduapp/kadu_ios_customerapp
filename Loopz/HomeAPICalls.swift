//
//  HomeAPICalls.swift
//  UFly
//
//  Created by 3Embed on 16/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire

class HomeAPICalls: NSObject {
    static let disposeBag = DisposeBag()
    //Get Store Data
    class func categoryData() ->Observable<([Category],[Item],[Item],Store,[Item],[Offer],[Brand])> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        var storeId = Utility.getStore().Id
        if storeId.length == 0 {
            storeId = "0"
        }
        let address = Utility.getAddress()
        let superStore = Utility.getSelectedSuperStores()
        let url = APIEndTails.HomeList + address.ZoneId + "/\(superStore.type.rawValue)/\(superStore.Id)" + "/\(storeId)/\(address.Latitude)/\(address.Longitude)"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var dataFrom:[String:Any] = [:]
                    if let data = bodyIn[APIResponceParams.Data] as? [String:Any] {
                        dataFrom = data
                    }
                    var Items:[Item] = []
                    var ItemsFav:[Item] = []
                    var ItemsLow:[Item] = []
                    var Categories:[Category] = []
                    var Offers:[Offer] = []
                    var Brands:[Brand] = []
                    if let titleTemp = dataFrom[APIResponceParams.Categories] as? [Any]{
                        for data in titleTemp {
                            let cat = Category.init(data: data as! [String : Any])
                            Categories.append(cat)
                        }
                    }
                    
                    if let titleTemp = dataFrom[APIResponceParams.LowestPrice] as? [Any]{
                        for data in titleTemp {
                            let cat = Item.init(data: data as! [String : Any], store: Store.init(data: [:]))
                            ItemsLow.append(cat)
                        }
                    }
                    if let titleTemp = dataFrom[APIResponceParams.Products] as? [Any]{
                        for data in titleTemp {
                            let cat = Item.init(data: data as! [String : Any], store: Store.init(data: [:]))
                            Items.append(cat)
                        }
                    }
                    if let titleTemp = dataFrom[APIResponceParams.ProductsFav] as? [Any]{
                        for data in titleTemp {
                            let cat = Item.init(data: data as! [String : Any], store: Store.init(data: [:]))
                            ItemsFav.append(cat)
                        }
                    }
                    if let titleTemp = dataFrom[APIResponceParams.Offers] as? [Any]{
                        for data in titleTemp {
                            let cat = Offer.init(data: data as! [String : Any])
                            Offers.append(cat)
                        }
                    }
                    if let titleTemp = dataFrom[APIResponceParams.Brands] as? [Any]{
                        for data in titleTemp {
                            let cat = Brand.init(data: data as! [String : Any])
                            Brands.append(cat)
                        }
                    }
                    var storeIn:Store? = Store.init(data: [:])
                    if let titleTemp = dataFrom[APIResponceParams.Store] as? [String:Any]{
                        storeIn = Store.init(data: titleTemp)
                        Utility.saveStore(location: titleTemp)
                    }
                    if Utility.getLogin() {
                        if let titleTemp = dataFrom[APIResponceParams.Profile] as? [String:Any]{
                            let user = Utility.getProfileData()
                            user.updateData(data: titleTemp, tool: true)
                        }
                    }
                    observer.onNext((Categories,Items,ItemsFav,storeIn!,ItemsLow,Offers,Brands))
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //Get Store Data(suCategory Items)
    class func getItems(category:String,store:String,subcat:String) ->Observable<[Any]> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        var store = store
        if store.length == 0 {
            store = "0"
        }
         Helper.showPI(string: "")
        let header = Utility.getHeaderWithoutAuth()
     //   let location = Utility.getAddress()
        
//        /products/{categoryId}/{subCategoryId}/{storeId}/{skip}/{limit}
      //  let url = APIEndTails.GetProducts + Utility.getAddress().ZoneId + "/" + category + "/" + subcat + "/" + store + "/\(location.Latitude)/\(location.Longitude)" + "/\(0)" + "/\(3000)"
        
        let url = APIEndTails.GetProducts  + "/" + category + "/" + subcat + "/" + store  + "/\(0)" + "/\(100)"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    
                    let dataFrom = bodyIn[APIResponceParams.Data] as! [String:Any]
                    let store = Store.init(data: dataFrom)
                    var Items = [SubSubCategory]()
                    if let titleTemp = dataFrom[APIResponceParams.SubSubCategories] as? [Any], titleTemp.count > 0{
                        for data in titleTemp {
                            let cat = SubSubCategory.init(data: data as! [String : Any])
                            if cat.Products.count > 0 {
                                Items.append(cat)
                            }
                        }
                    }else if let titleTemp = dataFrom[APIResponceParams.SubCategoryName2] as? [Any], titleTemp.count > 0{
                        for data in titleTemp {
                            let cat = SubSubCategory.init(data: data as! [String : Any])
                            if cat.Products.count > 0 {
                                Items.append(cat)
                            }
                        }
                    }else if (dataFrom[APIResponceParams.Products] as? [Any]) != nil{
                        let cat = SubSubCategory.init(data: dataFrom)
                        Items.append(cat)
                    }
                    observer.onNext([Items,store])
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //get Dispensaries
    class func getDispensaries(category: String) ->Observable<[Store]>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Loading())
        let header = Utility.getHeaderWithoutAuth()
        let location = Utility.getAddress()
        let url = APIEndTails.GetDispensaries + location.ZoneId + "/\(category)/\(location.Latitude)/\(location.Longitude)"
        
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    
                    
                    var stores = [Store]()
                    
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [Any]
                    {
                        for data in titleTemp {
                            let storeData = Store.init(data: data as! [String : Any])
                            stores.append(storeData)
                        }
                    }
                    observer.onNext(stores)
                    observer.onCompleted()
                }
                
            },onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }

    
    // getItemDetail
    class func getItemDetail(selectedItem:Item) -> Observable<Item>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        // let header = Utility.getHeaderWithoutAuth()
        let header = Utility.getHeader()
        let url = APIEndTails.ProductDetails + selectedItem.Id + "/\(Utility.getAddress().Latitude)/\(Utility.getAddress().Longitude)"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? =  APICalls.ErrorCode(rawValue: head.statusCode)!
                let  item = selectedItem
                if errNum == .success {
                    
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [String:Any]{
                        DispatchQueue.global(qos: .background).async {
                            
                               item.updateItemDetailFromList(data: titleTemp ,selectedUnitID:selectedItem.UnitId)
                            
                            DispatchQueue.main.async {
                                observer.onNext(item)
                                observer.onCompleted()
                                return
                            }
                        }
                    }else{
                        observer.onNext(item)
                        observer.onCompleted()
                    }
                }else{
                    observer.onNext(item)
                    observer.onCompleted()
                }
            },onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    //Get Store Data(suCategory Items)
    class func getViewAllItems(type:Int, store:Store, index:Int,id:String) ->Observable<[Item]> {
        var idIn = id
        if id.length == 0 {
            idIn = "0"
        }
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        
        Helper.showPI(string: StringConstants.Loading())
        let header = Utility.getHeader()
        let location = Utility.getAddress()
        let url = APIEndTails.ViewAll + location.ZoneId + "/" + store.Id + "/\(index)/\(type)/\(idIn)"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    
                    var Items = [Item]()
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [Any]{
                        for data in titleTemp {
                            let cat = Item.init(data: data as! [String : Any],store: store)
                            Items.append(cat)
                        }
                    }
                    observer.onNext(Items)
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //get all offers
    class func getOffers() ->Observable<[Offer]> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        var url = APIEndTails.Offers + Utility.getAddress().ZoneId + "/"
        if Utility.getStore().Id.length > 0 {
            url = url + Utility.getStore().Id
        }else{
            url = url + "0"
        }
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get,url , parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                var array:[Offer] = []
                if errNum == .success {
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [Any] {
                        for data in titleTemp {
                            array.append(Offer.init(data: data as! [String:Any]))
                        }
                    }
                    observer.onNext(array)
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //Get Store Data(HomePage Stores)
    class func getStores(city:String) ->Observable<[SuperStore]>
    {
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        
        // Helper.showPI(string: "")
        
        let header = Utility.getHeader()
        let url = APIEndTails.SuperCategories + "/" + city
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = Helper.nullKeyRemoval(data:body as![String:Any])
                
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let stores = bodyIn[APIResponceParams.Data] as! [Any]
                    var arrayOfSuper:[SuperStore] = []
                    for item in stores {
                        let data = SuperStore.init(data: item as! [String:Any])     //
                        arrayOfSuper.append(data)
                    }
                    observer.onNext(arrayOfSuper)
                    observer.onCompleted()
                    Utility.saveSuperStores(stores: stores)
                }
                else{
                    observer.onNext([])
                    observer.onCompleted()
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                //Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //Get Restuarants Data(Restuarants Stores)
    
    class func getRestuarants(offset:Int,limit:Int) ->Observable<([Store],[Offer],[Store])>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        let address = Utility.getAddress()
        let selectedSuperStore = Utility.getSelectedSuperStores()
        let url = APIEndTails.Store + "/\(selectedSuperStore.type.rawValue)/\(selectedSuperStore.Id)/\(address.Latitude)/\(address.Longitude)/\(Utility.getAddress().ZoneId)/\(offset)/\(limit)"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var restuarantsDetails = [Store]()
                    if let restuarantsTemp = bodyIn[APIResponceParams.Data] as? [Any]{
                        for restuarant in restuarantsTemp {
                            let ind = Store.init(data:restuarant as! [String : Any])
                            restuarantsDetails.append(ind)
                        }
                    }
                    var offerDetails = [Offer]()
                    if let offerTemps = bodyIn[APIResponceParams.offerData] as? [Any]{
                        for offerTemp in offerTemps {
                            let ind = Offer.init(data: offerTemp as! [String : Any])
                            offerDetails.append(ind)
                        }
                    }
                    
                var favoriteStores = [Store]()
                    if let favoriteTemps = bodyIn[APIResponceParams.favStore] as? [Any]{
                        for favTemp in favoriteTemps {
                            let ind = Store.init(data: favTemp as! [String : Any])
                            favoriteStores.append(ind)
                        }
                    }
                    
            observer.onNext((restuarantsDetails,offerDetails,favoriteStores))
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    observer.onNext(([],[],[]))
                    observer.onCompleted()
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    class func viewAllFavouriteStore(offset:Int,limit:Int) ->Observable<([Store],[Offer],[Store])>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        let address = Utility.getAddress()
        let selectedSuperStore = Utility.getSelectedSuperStores()
        let url = APIEndTails.favStores + "/\(selectedSuperStore.type.rawValue)/\(selectedSuperStore.Id)/\(address.Latitude)/\(address.Longitude)/\(Utility.getAddress().ZoneId)/\(offset)/\(limit)"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var restuarantsDetails = [Store]()
                    if let restuarantsTemp = bodyIn[APIResponceParams.Data] as? [Any]{
                        for restuarant in restuarantsTemp {
                            let ind = Store.init(data:restuarant as! [String : Any])
                            restuarantsDetails.append(ind)
                        }
                    }
                    var offerDetails = [Offer]()
                    if let offerTemps = bodyIn[APIResponceParams.offerData] as? [Any]{
                        for offerTemp in offerTemps {
                            let ind = Offer.init(data: offerTemp as! [String : Any])
                            offerDetails.append(ind)
                        }
                    }
                    
                    var favoriteStores = [Store]()
                    if let favoriteTemps = bodyIn[APIResponceParams.favStore] as? [Any]{
                        for favTemp in favoriteTemps {
                            let ind = Store.init(data: favTemp as! [String : Any])
                            favoriteStores.append(ind)
                        }
                    }
                    
                    observer.onNext((restuarantsDetails,offerDetails,favoriteStores))
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    observer.onNext(([],[],[]))
                    observer.onCompleted()
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    
    class func getRestuarantStore(skip:Int,limit:Int) ->Observable<(Store,[Category],[Item],[Item])>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        let address = Utility.getAddress()
        let selectedStore = Utility.getStore()
        let currentTime = Helper.getDateString(value: Date(), format: DateFormat.DateAndTimeFormatServer, zone: false)
        var  url = APIEndTails.RestuarantStore + "\(Utility.getAddress().ZoneId)/\(selectedStore.Id)/\(currentTime)/\(address.Latitude)/\(address.Longitude)/\(skip)/\(limit)"
        if let escapeurl =  url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        {
            url = escapeurl
        }
        //
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var store = Store.init(data: [:])
                    var products:[Item] = []
                    var cats:[Category] = []
                    var recomendedProducts:[Item] = []
                    var ItemsFav:[Item] = []
                    
                    if let restuarantsTemp = bodyIn[APIResponceParams.Data] as? [String : Any]{
                        store = Store.init(data: restuarantsTemp)
                        if let cats = restuarantsTemp[APIResponceParams.Products] as? [Any]{
                            for each in cats{
                                if let eachCat = each as? [String:Any]{
                                    products.append(Item.init(data: eachCat, store: store))
                                }
                            }
                            
                        }
                        for each in products{
                            var catAdded = false
                            var subCatAdded = false
                            for cat in cats{
                                if cat.Id == each.FirstCategoryId{
                                    catAdded = true
                                    
                                    for subCat in cat.SubCategories{
                                        
                                        if subCat.Id == each.SecondCategoryId{
                                            subCatAdded = true
                                            subCat.Products.append(each)
                                        }
                                    }
                                    
                                    if !subCatAdded{
                                        let subCat = SubCategory.init(data: [:], store: store)
                                        subCat.Id = each.SecondCategoryId
                                        subCat.Name = each.SecondCategoryName
                                        subCat.Products.append(each)
                                        if each.SecondCategoryName.length == 0 {
                                            cat.SubCategories.insert(subCat, at: 0)
                                        }else{
                                            cat.SubCategories.append(subCat)
                                        }
                                    }
                                    
                                }
                            }
                            
                            if !catAdded{
                                let subCat = SubCategory.init(data: [:], store: store)
                                subCat.Id = each.SecondCategoryId
                                subCat.Name = each.SecondCategoryName
                                subCat.Products.append(each)
                                
                                let cat = Category.init(data: [:])
                                cat.Id = each.FirstCategoryId
                                cat.Name = each.FirstCategoryName
                                cat.SubCategories.append(subCat)
                                cats.append(cat)
                            }
                            
                        }
                        
                        if let recomended = restuarantsTemp[APIResponceParams.Recommended] as? [Any]{
                            for each in recomended{
                                if let eachCat = each as? [String:Any]{
                                    recomendedProducts.append(Item.init(data: eachCat, store: store))
                                }
                            }
                        }
                        
                     if let favProducts = restuarantsTemp[APIResponceParams.favRestaurantProduct] as? [Any]{
                            for each in favProducts{
                                if let eachCat = each as? [String:Any]{
                                    ItemsFav.append(Item.init(data: eachCat, store: store))
                                }
                            }
                            
                        }
                        
                    }
                    observer.onNext((store,cats,recomendedProducts,ItemsFav))
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
}



