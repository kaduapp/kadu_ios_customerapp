//
//  OrderDetailTVE.swift
//  UFly
//
//  Created by 3 Embed on 07/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

// MARK: - UITableViewDataSource
extension OrderDetailVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView != mainTableView{
            if let addOn = orderDetailVM.selectedOrder?.AddOnGroups{
                return addOn.count
            }
        }
        
        if help {
            return 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView != mainTableView{
            if let addOn = orderDetailVM.selectedOrder?.AddOnGroups{
                return addOn[section].AddOns.count
            }
        }
        switch section {
        case 0:
            return 1
        case 2:
            var cou = 4
            if (orderDetailVM.selectedOrder?.Taxes.count)! > 0 {
                cou = 5
            }
            return cou + (orderDetailVM.selectedOrder?.Taxes.count)! + 3
        default:
            if help {
                return AppConstants.orderHelp().count
            } else {
                return (orderDetailVM.selectedOrder?.ItemsData.count)!
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //if tableView == mainTableView{
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.AdressDetailCell) as! AdressDetailCell
            
            cell.isFromTrackOrder = isTrackOrderVC
            cell.helpBtn.addTarget(self, action:#selector(helpBtnTaped) , for: UIControl.Event.touchUpInside)
            cell.detailsBtn.addTarget(self, action:#selector(helpBtnTaped) , for: UIControl.Event.touchUpInside)
            cell.setData(data: orderDetailVM.selectedOrder!)
            if help == false {
                cell.updateHelpHeader()
            }else {
                cell.updateDetails()
            }
            return cell
        case 1:
            if help {
                let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.HelpTitleCell) as! HelpTitleCell
                cell.updateHelpOrdeQ(index: indexPath.row)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.TotalItemsCell) as! TotalItemsCell
                cell.updateItems(data:(orderDetailVM.selectedOrder?.ItemsData[indexPath.row])!)
                if let addOnsGroup = orderDetailVM.selectedOrder?.ItemsData[indexPath.row].ItemCartAddonGroups{
                    var numOfcells:Int = 0
                    for eachSection in addOnsGroup{
                        numOfcells = numOfcells + eachSection.AddOns.count
                    }
                    cell.addOnTableviewHeight.constant = CGFloat(numOfcells * 20)
                    cell.addOnTableview.delegate = cell
                    cell.addOnTableview.dataSource = cell
                    cell.addOnTableview.reloadData()
                    cell.itemsView.updateConstraints()
                    cell.itemsView.layoutIfNeeded()
                }
                
                if indexPath.row == ((orderDetailVM.selectedOrder?.ItemsData.count)! - 1) {
                    cell.seperator.isHidden = true
                }
                else {
                    cell.seperator.isHidden = false
                }
                
                return cell
            }
        case 2:
            var cou = 3
            if (orderDetailVM.selectedOrder?.Taxes.count)! > 0 {
                cou = 4
            }
            if indexPath.row == cou + (orderDetailVM.selectedOrder?.Taxes.count)! {
                let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.AmountDetailCell) as! AmountDetailCell
                cell.updateAmount(data: orderDetailVM.selectedOrder!)
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.AmountDetailCell + "2") as! AmountDetailCell
                var data = Tax.init(data: [:])
                if indexPath.row == 0 {
                    data.Name = StringConstants.SubTotal()
                    data.Price = (orderDetailVM.selectedOrder?.SubTotal)!
                }else if indexPath.row == 1 {
                    data.Name = StringConstants.DeliveryCharge()
                    data.Price = (orderDetailVM.selectedOrder?.DeliveryCharge)!
                }else if indexPath.row == 2 {
                    data.Name = StringConstants.Discount()
                    data.Price = (orderDetailVM.selectedOrder?.Discount)!
                }else if indexPath.row == cou + (orderDetailVM.selectedOrder?.Taxes.count)! + 1 {
                    data.Name = StringConstants.Payment()
                    data.Value = 0
                }else if indexPath.row == cou + (orderDetailVM.selectedOrder?.Taxes.count)! + 2 {
                    var paymentType = ""
                    var amount : Float = 0
                    
                    let dataIn = orderDetailVM.selectedOrder!
                    if dataIn.PaymentType == 0 {
                        paymentType = StringConstants.Wallet()
                        amount = dataIn.paidBy.wallet
                    }else if dataIn.PaymentType == 1 {
                        paymentType = StringConstants.Cart()
                        amount = dataIn.paidBy.card
                        if dataIn.paidBy.wallet != 0
                        {
                            paymentType = StringConstants.Cart() + "+" + StringConstants.Wallet()//"Card + Wallet"  StringConstants.Wallet()
                            amount = dataIn.paidBy.card + dataIn.paidBy.wallet
                        }

                    }else if dataIn.PaymentType == 2 {
                        paymentType = StringConstants.Cash()
                        amount = dataIn.paidBy.cash
                        if dataIn.paidBy.wallet != 0
                        {
                            paymentType = StringConstants.CashLower() + "+" + StringConstants.Wallet()
                            amount = dataIn.paidBy.cash + dataIn.paidBy.wallet
                        }
                    }
                    data.Name = paymentType
                    data.Price = amount
                  }else if indexPath.row == cou + (orderDetailVM.selectedOrder?.Taxes.count)! + 3 {
                    let dataIn = orderDetailVM.selectedOrder!
                    data.Name = StringConstants.Wallet()
                    data.Price = dataIn.paidBy.wallet
                  
                }
                else{
                    if (orderDetailVM.selectedOrder?.Taxes.count)! > 0 && indexPath.row == 3 {
                        data.Name = StringConstants.Taxes()
                        data.Value = 0
                    }else{
                        data = (orderDetailVM.selectedOrder?.Taxes[indexPath.row - 4])!
                    }
                }
                cell.updateAmount(data: data)
                return cell
            }
        default:
            return UITableViewCell()
        }
        //        }else{
        //            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddOnTableViewCell.self), for: indexPath) as! AddOnTableViewCell
        //            if let data = orderDetailVM.selectedOrder?.AddOnGroups[indexPath.section]{
        //                cell.setData(data: data, indexPath: indexPath)
        //            }
        //            return cell
        //        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView != mainTableView{
            if let data = orderDetailVM.selectedOrder?.AddOnGroups[section]{
                let view = Helper.showHeader(title:data.Name, showViewAll: false, showSeperator: false)
                return view
            }
            return UIView()
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView != mainTableView{
            return 50
        }
 
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView != mainTableView{

        return 30
        }
        
        var cou = 3
        if (orderDetailVM.selectedOrder?.Taxes.count)! > 0 {
            cou = 4
        }
      
        if indexPath.section == 2 && indexPath.row == cou + (orderDetailVM.selectedOrder?.Taxes.count)! + 3 {
            let dataIn = orderDetailVM.selectedOrder!
            if !dataIn.payByWallet || dataIn.PaymentType == 0{
                return 0
            }
        }
   
        
        return UITableView.automaticDimension
    }
    @objc func helpBtnTaped(sender:UIButton){
        let cell = mainTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! AdressDetailCell
        if sender.tag == 10 {
            help = false
            cell.detailsIndicator.isHidden = false
            cell.helpIndicator.isHidden = true
        }else {
            help = true
            cell.detailsIndicator.isHidden = true
            cell.helpIndicator.isHidden = false
        }
        //        var mySet: IndexSet = IndexSet.init()
        //        mySet.insert(1)
        self.mainTableView.reloadData()//Sections(mySet, with: .fade)
    }
}
extension OrderDetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == mainTableView{
            if help {
                tableView.deselectRow(at: indexPath, animated: true)
                if Changebles.LiveChatAvailable {
                    performSegue(withIdentifier: UIConstants.SegueIds.OrderDetailToWebVC, sender: self)
                }else if Changebles.Zendesc {
                    performSegue(withIdentifier: String(describing: ZendeskVC.self), sender: AppConstants.orderHelp()[indexPath.row])
                }
            }
        }
    }
    
}
