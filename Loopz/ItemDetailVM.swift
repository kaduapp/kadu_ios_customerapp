//
//  ItemDetailVM.swift
//  UFly
//
//  Created by 3 Embed on 27/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ItemDetailVM {
    
    let ItemDetailVM_response = PublishSubject<ResponseType>()

   var updatedCount = 0

    /// its an enum of type Int
    ///
    /// - UpdateCart: having integer value 0 for updating cart
    /// - GetAPIResponse: having value 1 for GetAPIResponse
    enum ResponseType:Int {
        case UpdateCart = 0
        case GetAPIResponse = 1
        case FavTrue = 2
        case FavFalse = 3
    }
    
    //variables
    var item:Item = Item.init(data: [:], store: Store.init(data: [:]))
    var didTapDownArrow:Bool = false
    var cartDb = CartDBManager()
    var cartVM = CartVM()
    let disposeBag = DisposeBag()
    /// having subscription to get item detai Api
    func getItemDetail(){
        
        HomeAPICalls.getItemDetail(selectedItem: item).subscribe(onNext: { result in
            self.item = result
            self.ItemDetailVM_response.onNext(ResponseType.GetAPIResponse)
        }, onError: { error in
            print(error)
        }).disposed(by: self.disposeBag)
    }
    
    // Calling Item Detail API
    func favourite(){
        FavouriteAPICalls.favourite(item: item).subscribe(onNext: { result in
            if result == true {
            self.ItemDetailVM_response.onNext(ResponseType.FavTrue)
            }else{
           self.ItemDetailVM_response.onNext(ResponseType.FavFalse)
            }
        }, onError: { error in
            print(error)
        }).disposed(by: self.disposeBag)
    }
    
   //update the Cart count
    func updateCart(){
        if item.CartQty == 0 && updatedCount > 0 {
                item.CartQty = updatedCount
                cartVM.addCart(cartItem: CartItem.init(item: item))
        } else if (item.CartQty) > 0 && updatedCount > 0 {
             item.CartQty = updatedCount
             cartVM.updateCart(cartItem: CartItem.init(item: item))
        } else if (item.CartQty) > 0 && updatedCount == 0 {
            item.CartQty = updatedCount
            cartVM.deleteCart(cartItem: CartItem.init(item: item))
        }
        self.ItemDetailVM_response.onNext(ResponseType.UpdateCart)
    }
    
}
