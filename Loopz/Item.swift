//
//  Item.swift
//  UFly
//
//  Created by 3Embed on 12/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

struct DictObject {
    var Name    = ""
    var Value   = ""
}

struct MobileImageModel {
    var Description     = ""
    var Image           = ""
    var ImageId         = ""
    var ImageText       = ""
    var Keyword         = ""
    var Mobile          = ""
    var Thumbnail       = ""
    var Title           = ""
    
}
struct Unit {
    var Title       = ""
    var Value:Float = 0
    var Id          = ""
    var Discount:Float = 0
    var FinalPrice:Float = -1
    var availableQuantity = 0
    var AddOnGroups:[AddOnGroup]    = []
    var AddOnAvailable:Bool         = false
    var RecentAddOns:[String]       = []
    
    
    
    init(data:[String:Any]) {
        if let titleTemp = data[APIResponceParams.Title] as? String {
            Title = titleTemp
        }
        if let titleTemp = data[APIResponceParams.UnitName] as? String {
            Title = titleTemp
        }
        if let titleTemp = data[APIResponceParams.UnitId] as? String {
            Id = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Value] as? Float{
            Value = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Value] as? Double{
            Value = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.UnitPrice] as? Float{
            Value = titleTemp
        }
        if let titleTemp = data[APIResponceParams.UnitPrice] as? String{
            
            Value = Float(titleTemp)!
            //Value = Float(titleTemp.trimmingCharacters(in: .whitespaces))!
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? String{
            Discount = Float(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? String{
            FinalPrice = Float(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? Double{
            Discount = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? Double{
            FinalPrice = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? Float{
            Discount = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? Float{
            FinalPrice = Float(titleTemp)
        }
        
        if let titleTemp = data[APIResponceParams.availableQuantity] as? Int{
            availableQuantity = Int(titleTemp)
        }
        
        if RecentAddOns.count == 0 {
            if let selectedAddOns = data[APIResponceParams.SelectedAddons] as? [String] {
                RecentAddOns = selectedAddOns
            }
        }
        if let titleTemp = data[APIResponceParams.AddOns] as? [[String:Any]] {
            AddOnGroups.removeAll()
            for temp in titleTemp{
                let newData = AddOnGroup.init(data: temp,selected: RecentAddOns)
                AddOnGroups.append(newData)
            }
        }
        
        if let titleTemp = data[APIResponceParams.AddOnsAvailable] as? Int{
            if titleTemp == 1 {
                AddOnAvailable           = true
            }else{
                AddOnAvailable           = false
            }
        }
        
    }
}

class Item: NSObject {
    
    var Images                      =   [String]()
    var Id                          =   ""
    var ParentId                    =   ""
    var Name                        =   ""
    var Fav                         =   false
    var FavCount                    =   0
    var THC:Float                   =   0
    var CBD:Float                   =   0
    var Price:Float                 =   0
    var ShortDescription            =   ""
    var Units:[Unit]                =   []
    var CartQty:Int                 =   0
    var UPC                         =   ""
    var SKU                         =   ""
    var StoreId                     =   ""
    var StoreName                   =   ""
    var StoreLogo                   =   ""
    var StoreLat:Double             =   0
    var StoreLong:Double            =   0
    var StoreAddress                =   ""
    var TypeStore:AppConstants.TypeOfStore = .Grocery
    var cartType:AppConstants.TypeOfCart = .SingleCartMulStore
    var Cart:[CartItem]             =   []
    var CartId                      =   ""
    var DetailedDescription         =   ""
    var Effects                     =   [([DictObject](),"")]
    var SubCategory                 =   ""
    var StoreDistanceKm             =   ""
    var StoreDistanceMiles:Float    =   0
    var WishList:[String]           =   []
    var UnitId                      =   ""
    var UnitName                    =   ""
    var Discount:Float              =   0
    var FinalPrice:Float            =   -1
    var OfferId                     =   ""
    var Ingredients                 =   ""
    var AddOnGroups:[AddOnGroup]    = []
    var AddOnAvailable:Bool         = false
    var RecentAddOns:[String]       = []
    var PackId                      = ""
    var outOfStock                  = false
    var availableQuantity           = 0
    
    //RestuarantStore
    
    var CategoryName:String         = ""
    var FirstCategoryId:String      = ""
    var FirstCategoryName:String    = ""
    var ProductName:String          = ""
    var SecondCategoryId:String     = ""
    var SecondCategoryName:String   = ""
    var ThirdCategoryId:String      = ""
    var ThirdCategoryName:String    = ""
    var MobileImage:[MobileImageModel] = []
    
    init(data:[String:Any],store:Store) {
        super.init()
        StoreId         = store.Id
        StoreLogo       = store.Image
        StoreName       = store.Name
        StoreLong       = store.StoreLong
        StoreLat        = store.StoreLat
        StoreAddress    = store.Address
        TypeStore       = store.TypeStore
        self.updateItemDetail(data: data)
    }
    
    func setCart() {
        let cartDbManager = CartDBManager()
        Cart = cartDbManager.getCartFullCount(data: self)
        CartQty = 0
        for each in Cart {
            CartQty = CartQty + each.QTY
            CartId = each.CartId
        }
        
    }
    func updateItemDetailFromList(data:[String:Any] ,selectedUnitID:String ){
        if let titleTemp = data[APIResponceParams.ItemId] as? String{
            Id           = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.FavCount] as? Int{
            FavCount     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemPCU] as? String{
            UPC          = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemSKU] as? String{
            SKU          = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DistanceKm] as? String{
            StoreDistanceKm    = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DistanceMiles] as? Float{
            StoreDistanceMiles  =  titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.UnitId] as? String{
            UnitId  =  titleTemp
        }
        if let titleTemp = data[APIResponceParams.UnitName] as? String{
            UnitName  =  titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.ItemPrice] as? Float{
            Price        = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemPrice] as? String{
            Price        = Float(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.ItemPrice] as? Double{
            Price        = Float(titleTemp)
        }
        
        if let titleTemp = data[APIResponceParams.Fav] as? Bool{
            Fav             = titleTemp
        }
        
        
        
        if let titleTemp = data[APIResponceParams.ItemImages] as? [Any]{
            Images.removeAll()
            for item in titleTemp {
                let tempData = item as! [String:Any]
                Images.append(tempData[APIResponceParams.Phone] as! String)
            }
        }
        if let titleTemp = data[APIResponceParams.ItemDescription] as? String {
            ShortDescription  = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DetailedDescription] as? String {
            DetailedDescription  = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SubCategoryName] as? String {
            SubCategory  = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemTCH] as? String {
            
            if titleTemp.length > 0 && Helper.isNumeric(text: titleTemp){
                self.THC          = Float(titleTemp)!
            }
            
        }
        if let titleTemp = data[APIResponceParams.ItemCBD] as? String{
            
            if titleTemp.length > 0 && Helper.isNumeric(text: titleTemp){
                CBD          = Float(titleTemp)!
            }
            
            
        }
        if let titleTemp = data[APIResponceParams.ItemName] as? String{
            Name         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemName2] as? String{
            Name         = titleTemp
        }
        Effects.removeAll()
        if let titleTemp1 = data[APIResponceParams.Effects] as? [Any] {
            for titleTemp in titleTemp1 {
                let dataTemp = titleTemp as! [String:Any]
                let data = dataTemp[APIResponceParams.Data] as! [String:Any]
                let name = dataTemp[APIResponceParams.Name] as! String
                let tempKeys = data.compactMap(){$0.0}
                var effectIn:[DictObject] = []
                for i in 0...tempKeys.count-1 {
                    var amount = ""
                    if let dataValue = data[tempKeys[i]] as? Float {
                        amount = String(describing: dataValue)
                    }
                    if let dataValue = data[tempKeys[i]] as? String {
                        amount = dataValue
                    }
                    if let dataValue = data[tempKeys[i]] as? String {
                        amount = dataValue
                    }
                    
                    if let dataCon = amount.floatValue {
                        if dataCon == 0 {
                            amount = ""
                        }
                    }
                    
                    if amount.length > 0 {
                        var dict = DictObject()
                        dict.Name = tempKeys[i]
                        if let dataValue = data[tempKeys[i]] as? Float {
                            dict.Value = String(describing: dataValue)
                        }
                        if let dataValue = data[tempKeys[i]] as? String {
                            dict.Value = dataValue
                        }
                        effectIn.append(dict)
                    }
                }
                if effectIn.count > 0 {
                    Effects.append((effectIn,name))
                }
            }
        }
        if let titleTemp = data[APIResponceParams.Units] as? [Any] {
            Units.removeAll()
            for eachUnit in titleTemp {
                let temp = eachUnit as! [String:Any]
                let unit = Unit.init(data: temp)
                Units.append(unit)
            }
        }
        
        
        if let titleTemp = data[APIResponceParams.availableQuantity] as? Int{
            availableQuantity = Int(titleTemp)
            if availableQuantity <= 0{
                outOfStock = true
            }
        }
        if let titleTemp = data[APIResponceParams.FinalPriceList] as? [Any] {
            Units.removeAll()
            if titleTemp.count > 0 {
                let temp = titleTemp[0] as! [String:Any]
                let unit = Unit.init(data: temp)
                FinalPrice = unit.FinalPrice
                Price = unit.Value
                UnitId = unit.Id
                availableQuantity = unit.availableQuantity
                if unit.availableQuantity <= 0{
                    outOfStock = true
                }
                
            }else{
                if Units.count > 0 {
                    let unit = Units[0]
                    FinalPrice = unit.FinalPrice
                    Price = unit.Value
                    UnitId = unit.Id
                    availableQuantity = unit.availableQuantity
                    if unit.availableQuantity <= 0{
                        outOfStock = true
                    }
                }
            }
        }else{
            if Units.count > 0 {
                let unit = Units[0]
                FinalPrice = unit.FinalPrice
                Price = unit.Value
                UnitId = unit.Id
                availableQuantity = unit.availableQuantity
                if unit.availableQuantity <= 0{
                    outOfStock = true
                }
            }
        }
        if let temp = data[APIResponceParams.WishList] as? [Any] {
            WishList.removeAll()
            for item in temp {
                let tempData = item as! [String:Any]
                if let tempId = tempData[APIResponceParams.ListId] as? String {
                    WishList.append(tempId)
                }
            }
        }
        if let titleTemp = data[APIResponceParams.CartStoreID] as? String {
            StoreId = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreLogo] as? String {
            StoreLogo = titleTemp
        }
        if let titleTemp = data[APIResponceParams.BusinessImage] as? String {
            StoreLogo = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreName] as? String {
            StoreName = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreLat] as? Double {
            StoreLat = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreLong] as? Double {
            StoreLong = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreAddress] as? String {
            StoreAddress = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.Ingredients] as? String {
            Ingredients = titleTemp
        }
        if UnitId.length == 0 {
            if Units.count > 0 {
                UnitId = Units[0].Id
                Price = Units[0].Value
                FinalPrice = Units[0].FinalPrice
            }
        }else{
            if Units.count > 0 {
                for itemIn in Units {
                    if UnitId == itemIn.Id {
                        Price = itemIn.Value
                        UnitName = itemIn.Title
                        FinalPrice = itemIn.FinalPrice
                        break
                    }
                }
            }
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? Float{
            Discount           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? Float{
            FinalPrice           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? Double{
            Discount           = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? Double{
            FinalPrice           = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? String{
            Discount           = Float(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? String{
            FinalPrice           = Float(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.OfferId] as? String{
            OfferId           = titleTemp
        }
        if OfferId.length == 0 && FinalPrice == -1 {
            FinalPrice = Price
        }
        if let titleTemp = data[APIResponceParams.AddOnsAvailable] as? Int{
            if titleTemp == 1 {
                AddOnAvailable           = true
            }else{
                AddOnAvailable           = false
            }
        }
        
        if let titleTemp = data[APIResponceParams.MobileImage] as? [Any]{
            MobileImage.removeAll()
            
            for each in titleTemp{
                var temp = MobileImageModel()
                if let mobileImage = each as? [String:String]{
                    if let titleTemp = (mobileImage[APIResponceParams.Image]){
                        temp.Image    = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Description]){
                        temp.Description    = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.ImageId]){
                        temp.ImageId        = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.ImageText]){
                        temp.ImageText      = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Keyword]){
                        temp.Keyword    = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Mobile]){
                        temp.Mobile         = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Thumbnail]){
                        temp.Thumbnail            = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Title]){
                        temp.Title           = titleTemp
                    }
                    MobileImage.append(temp)
                }
            }
        }
        
        if let titleTemp = data[APIResponceParams.CategoryName] as? String{
            CategoryName           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FirstCategoryId] as? String{
            FirstCategoryId           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FirstCategoryName] as? String{
            FirstCategoryName           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SecondCategoryId] as? String{
            SecondCategoryId           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SecondCategoryName] as? String{
            SecondCategoryName           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ThirdCategooryId] as? String{
            ThirdCategoryId           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ThirdCategoryName] as? String{
            ThirdCategoryName           = titleTemp
        }
        if RecentAddOns.count == 0 {
            if let selectedAddOns = data[APIResponceParams.SelectedAddons] as? [String] {
                RecentAddOns = selectedAddOns
            }
        }
        if let titleTemp  = data[APIResponceParams.Units]  as? [Any]
        {
            
            for temp in titleTemp
            {
                if let json: [String: Any] = temp as? [String : Any]
                {
                    if selectedUnitID == json["unitId"] as? String
                    {
                        if let array = json["addOns"] as? [[String:Any]]
                        {
                            AddOnGroups.removeAll()
                            for temp in array{
                                let newData = AddOnGroup.init(data: temp,selected: RecentAddOns)
                                AddOnGroups.append(newData)
                            }
                        }
                        break
                    }
                }
                
            }
            
        }
        if let titleTemp = data[APIResponceParams.AddOns] as? [[String:Any]] {
            AddOnGroups.removeAll()
            for temp in titleTemp{
                let newData = AddOnGroup.init(data: temp,selected: RecentAddOns)
                AddOnGroups.append(newData)
            }
        }
        
         if let titleTemp = data[APIResponceParams.AddOnsGrouped] as? [[String:Any]] {
            AddOnGroups.removeAll()
            for temp in titleTemp{
                let newData = AddOnGroup.init(data: temp,selected: RecentAddOns)
                AddOnGroups.append(newData)
            }
        }
        setCart()
        if Price - FinalPrice > 0
        {
            Discount = Price - FinalPrice
        }
    }
    func updateItemDetail(data:[String:Any]){
        if let titleTemp = data[APIResponceParams.ItemId] as? String{
            Id           = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.FavCount] as? Int{
            FavCount     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemPCU] as? String{
            UPC          = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemSKU] as? String{
            SKU          = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DistanceKm] as? String{
            StoreDistanceKm    = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DistanceMiles] as? Float{
            StoreDistanceMiles  =  titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.UnitId] as? String{
            UnitId  =  titleTemp
        }
        if let titleTemp = data[APIResponceParams.UnitName] as? String{
            UnitName  =  titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.ItemPrice] as? Float{
            Price        = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemPrice] as? String{
            Price        = Float(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.ItemPrice] as? Double{
            Price        = Float(titleTemp)
        }
        
        if let titleTemp = data[APIResponceParams.Fav] as? Bool{
            Fav             = titleTemp
        }
        
     
        
        if let titleTemp = data[APIResponceParams.ItemImages] as? [Any]{
            Images.removeAll()
            for item in titleTemp {
                let tempData = item as! [String:Any]
                Images.append(tempData[APIResponceParams.Phone] as! String)
            }
        }
        if let titleTemp = data[APIResponceParams.ItemDescription] as? String {
            ShortDescription  = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DetailedDescription] as? String {
            DetailedDescription  = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SubCategoryName] as? String {
            SubCategory  = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemTCH] as? String {
            
            if titleTemp.length > 0 && Helper.isNumeric(text: titleTemp){
                self.THC          = Float(titleTemp)!
            }
            
        }
        if let titleTemp = data[APIResponceParams.ItemCBD] as? String{
            
            if titleTemp.length > 0 && Helper.isNumeric(text: titleTemp){
                CBD          = Float(titleTemp)!
            }
            
            
        }
        if let titleTemp = data[APIResponceParams.ItemName] as? String{
            Name         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ItemName2] as? String{
            Name         = titleTemp
        }
        Effects.removeAll()
        if let titleTemp1 = data[APIResponceParams.Effects] as? [Any] {
            for titleTemp in titleTemp1 {
                let dataTemp = titleTemp as! [String:Any]
                let data = dataTemp[APIResponceParams.Data] as! [String:Any]
                let name = dataTemp[APIResponceParams.Name] as! String
                let tempKeys = data.compactMap(){$0.0}
                var effectIn:[DictObject] = []
                for i in 0...tempKeys.count-1 {
                    var amount = ""
                    if let dataValue = data[tempKeys[i]] as? Float {
                        amount = String(describing: dataValue)
                    }
                    if let dataValue = data[tempKeys[i]] as? String {
                        amount = dataValue
                    }
                    if let dataValue = data[tempKeys[i]] as? String {
                        amount = dataValue
                    }
                    
                    if let dataCon = amount.floatValue {
                        if dataCon == 0 {
                            amount = ""
                        }
                    }
                    
                    if amount.length > 0 {
                        var dict = DictObject()
                        dict.Name = tempKeys[i]
                        if let dataValue = data[tempKeys[i]] as? Float {
                            dict.Value = String(describing: dataValue)
                        }
                        if let dataValue = data[tempKeys[i]] as? String {
                            dict.Value = dataValue
                        }
                        effectIn.append(dict)
                    }
                }
                if effectIn.count > 0 {
                    Effects.append((effectIn,name))
                }
            }
        }
        if let titleTemp = data[APIResponceParams.Units] as? [Any] {
            Units.removeAll()
            for eachUnit in titleTemp {
                let temp = eachUnit as! [String:Any]
                let unit = Unit.init(data: temp)
                Units.append(unit)
            }
        }
        
        
        if let titleTemp = data[APIResponceParams.availableQuantity] as? Int{
            availableQuantity = Int(titleTemp)
            if availableQuantity <= 0{
                outOfStock = true
            }
        }
        if let titleTemp = data[APIResponceParams.FinalPriceList] as? [Any] {
            Units.removeAll()
            if titleTemp.count > 0 {
                let temp = titleTemp[0] as! [String:Any]
                let unit = Unit.init(data: temp)
                FinalPrice = unit.FinalPrice
                Price = unit.Value
                UnitId = unit.Id
                availableQuantity = unit.availableQuantity
                if unit.availableQuantity <= 0{
                outOfStock = true
                }
                
            }else{
                if Units.count > 0 {
                    let unit = Units[0]
                    FinalPrice = unit.FinalPrice
                    Price = unit.Value
                    UnitId = unit.Id
                    availableQuantity = unit.availableQuantity
                    if unit.availableQuantity <= 0{
                        outOfStock = true
                    }
                }
            }
        }else{
            if Units.count > 0 {
                let unit = Units[0]
                FinalPrice = unit.FinalPrice
                Price = unit.Value
                UnitId = unit.Id
                availableQuantity = unit.availableQuantity
                if unit.availableQuantity <= 0{
                    outOfStock = true
                }
            }
        }
        if let temp = data[APIResponceParams.WishList] as? [Any] {
            WishList.removeAll()
            for item in temp {
                let tempData = item as! [String:Any]
                if let tempId = tempData[APIResponceParams.ListId] as? String {
                    WishList.append(tempId)
                }
            }
        }
        if let titleTemp = data[APIResponceParams.CartStoreID] as? String {
            StoreId = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreLogo] as? String {
            StoreLogo = titleTemp
        }
        if let titleTemp = data[APIResponceParams.BusinessImage] as? String {
            StoreLogo = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreName] as? String {
            StoreName = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreLat] as? Double {
            StoreLat = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreLong] as? Double {
            StoreLong = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreAddress] as? String {
            StoreAddress = titleTemp
        }
        if let storeTypeTemp = data[APIResponceParams.RestuarantsStoreType] as? Double{
            
            switch Int(storeTypeTemp) {
            case 1:
                TypeStore = .Restaurant
                break
            case 2:
                TypeStore = .Grocery
                break
            case 3:
                TypeStore = .Shopping
                break
            case 4:
                TypeStore = .Marijuana
                break
            case 5:
                TypeStore = .Launder
                break
            default:
                TypeStore = .Grocery
                break
            }
        }
        
        if let cartTypeTemp = data[APIResponceParams.StoreCartType] as? Int{
            
            switch Int(cartTypeTemp) {
            case 1:
                cartType = .SingleCartSingleStore
                break
            case 3:
                cartType = .SingleCartMulStoreCat
                break
            default:
                cartType = .SingleCartMulStore
                break
            }
        }
        
        
        if let titleTemp = data[APIResponceParams.Ingredients] as? String {
            Ingredients = titleTemp
        }
        if UnitId.length == 0 {
            if Units.count > 0 {
                UnitId = Units[0].Id
                Price = Units[0].Value
                FinalPrice = Units[0].FinalPrice
                AddOnAvailable = Units[0].AddOnAvailable
                AddOnGroups = Units[0].AddOnGroups
                RecentAddOns = Units[0].RecentAddOns
            }
        }else{
            if Units.count > 0 {
                for itemIn in Units {
                    if UnitId == itemIn.Id {
                        Price = itemIn.Value
                        UnitName = itemIn.Title
                        FinalPrice = itemIn.FinalPrice
                        break
                    }
                }
            }
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? Float{
            Discount           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? Float{
            FinalPrice           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? Double{
            Discount           = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? Double{
            FinalPrice           = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? String{
            Discount           = Float(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? String{
            FinalPrice           = Float(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.OfferId] as? String{
            OfferId           = titleTemp
        }
        if OfferId.length == 0 && FinalPrice == -1 {
            FinalPrice = Price
        }
        if let titleTemp = data[APIResponceParams.AddOnsAvailable] as? Int{
            if titleTemp == 1 {
                AddOnAvailable           = true
            }else{
                AddOnAvailable           = false
            }
        }
        
        if let titleTemp = data[APIResponceParams.MobileImage] as? [Any]{
            MobileImage.removeAll()
            
            for each in titleTemp{
                var temp = MobileImageModel()
                if let mobileImage = each as? [String:String]{
                    if let titleTemp = (mobileImage[APIResponceParams.Image]){
                        temp.Image    = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Description]){
                        temp.Description    = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.ImageId]){
                        temp.ImageId        = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.ImageText]){
                        temp.ImageText      = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Keyword]){
                        temp.Keyword    = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Mobile]){
                       temp.Mobile         = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Thumbnail]){
                        temp.Thumbnail            = titleTemp
                    }
                    if let titleTemp = (mobileImage[APIResponceParams.Title]){
                        temp.Title           = titleTemp
                    }
                    MobileImage.append(temp)
                }
            }
        }
        
        if let titleTemp = data[APIResponceParams.CategoryName] as? String{
            CategoryName           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FirstCategoryId] as? String{
            FirstCategoryId           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FirstCategoryName] as? String{
            FirstCategoryName           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SecondCategoryId] as? String{
            SecondCategoryId           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SecondCategoryName] as? String{
            SecondCategoryName           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ThirdCategooryId] as? String{
            ThirdCategoryId           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ThirdCategoryName] as? String{
            ThirdCategoryName           = titleTemp
        }
        if RecentAddOns.count == 0 {
        if let selectedAddOns = data[APIResponceParams.SelectedAddons] as? [String] {
            RecentAddOns = selectedAddOns
        }
        }
//        if let titleTemp  = data[APIResponceParams.Units]  as? [Any]
//        {
//
//            for temp in titleTemp
//            {
//                if let json: [String: Any] = temp as? [String : Any]
//                {
//                    if let array = json["addOns"] as? [[String:Any]]
//                    {
//                        AddOnGroups.removeAll()
//                        for temp in array{
//                            let newData = AddOnGroup.init(data: temp,selected: RecentAddOns)
//                            AddOnGroups.append(newData)
//                        }
//                    }
//                }
//            }
//
//        }
        if let titleTemp = data[APIResponceParams.AddOns] as? [[String:Any]] {
            AddOnGroups.removeAll()
            for temp in titleTemp{
                let newData = AddOnGroup.init(data: temp,selected: RecentAddOns)
                AddOnGroups.append(newData)
            }
        }
        
        // added by phani on 29th April
        if let titleTemp = data[APIResponceParams.AddOnsGrouped] as? [[String:Any]] {
            AddOnGroups.removeAll()
            for temp in titleTemp{
                let newData = AddOnGroup.init(data: temp,selected: RecentAddOns)
                AddOnGroups.append(newData)
            }
        }
        setCart()
        if Price - FinalPrice > 0
               {
                   Discount = Price - FinalPrice
               }
    }
    init(data:CartItem){
        
        Name          = data.ItemName
        Id            = data.ChildProductId
        StoreName     = data.StoreName
        StoreAddress  = data.StoreAddress
        StoreLong     = data.StoreLong
        StoreLat      = data.StoreLat
        CartId        = data.CartId
        StoreId       = data.StoreId
        StoreAddress  = data.StoreAddress
        StoreLogo     = data.StoreImage
        StoreLong     = data.StoreLong
        CartQty       = data.QTY
        Price         = data.UnitPrice
        UnitId        = data.UnitId
        UnitName      = data.UnitName
        ParentId      = data.ParentProductId
        OfferId       = data.OfferId
        SKU           = data.Sku
        PackId          = data.PackId
        AddOnAvailable = data.AddOnAvailable
        RecentAddOns.removeAll()
        for each in data.Addons {
            RecentAddOns.append(each.Id)
        }
        
        Images.removeAll()
        Images.append(data.ItemImageURL)
    }
}
