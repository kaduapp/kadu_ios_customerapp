//
//  ProfileModel.swift
//  UFly
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

struct ProfileModel {
    var menuTitle = ""
    var menuIcon = UIImage()
}

/// its is having profile menu list and icons
class ProfileModelLibrary {
    
    static var menuList: ([[ProfileModel]]) {
        
        var menu1 = ProfileModel()
        menu1.menuTitle = AppConstants.menuTitles()[0] //manage Address
        menu1.menuIcon = #imageLiteral(resourceName: "Address")
        
        var menu2 = ProfileModel()
        menu2.menuTitle = AppConstants.menuTitles()[1] //payment
        menu2.menuIcon = #imageLiteral(resourceName: "Payment")
        
        var menu3 = ProfileModel()       //favourite
        menu3.menuTitle = AppConstants.menuTitles()[2]
        menu3.menuIcon = #imageLiteral(resourceName: "favourite")
        
        var menu4 = ProfileModel()         // wishlist
        menu4.menuTitle = AppConstants.menuTitles()[3]
        menu4.menuIcon = #imageLiteral(resourceName: "wish_list")
        
        var menu5 = ProfileModel()   //offers
        menu5.menuTitle = AppConstants.menuTitles()[4]
        menu5.menuIcon = #imageLiteral(resourceName: "Offer")
        
        var menu6 = ProfileModel()     //identity
        menu6.menuTitle = AppConstants.menuTitles()[5]
        menu6.menuIcon = #imageLiteral(resourceName: "id_card")
        
        var menu7 = ProfileModel()   // help
        menu7.menuTitle = AppConstants.menuTitles()[6]
        menu7.menuIcon = #imageLiteral(resourceName: "Help")
        
        var menu8 = ProfileModel()    //refer your friends
        menu8.menuTitle = AppConstants.menuTitles()[7]
        menu8.menuIcon = #imageLiteral(resourceName: "refer")
        
        var menu9 = ProfileModel()    //refer your friends
        menu9.menuTitle = AppConstants.menuTitles()[8]
        menu9.menuIcon = #imageLiteral(resourceName: "Support")
        
        var menu10 = ProfileModel()    //refer your friends
        menu10.menuTitle = AppConstants.menuTitles()[9]
        menu10.menuIcon = #imageLiteral(resourceName: "language")
        
        var menu11 = ProfileModel()    //refer your friends
        menu11.menuTitle = AppConstants.menuTitles()[10]
        menu11.menuIcon = #imageLiteral(resourceName: "Logout")
        
        var menu12 = ProfileModel()    //My Orders
        menu12.menuTitle = AppConstants.menuTitles()[11]
        menu12.menuIcon = #imageLiteral(resourceName: "Calendar")
        
        var menu13 = ProfileModel()   // help
        menu13.menuTitle = AppConstants.menuTitles()[12]
        menu13.menuIcon = #imageLiteral(resourceName: "Help")
        
        let menu14 = ProfileModel()   // Version
        
        let arrayOfProfiles:[[ProfileModel]] = [[menu11],[/*menu5,*/menu12,menu7,menu13],[menu2,menu1,menu3,menu4,menu8,menu10],[menu11,menu14]]
        //let list:[ProfileModel] = [menu6,menu1,menu2,menu3,menu4,menu5,menu7,menu8,menu9,menu10]
        return (arrayOfProfiles)
        
    }
}
