//
//  CheckOutVC.swift
//  UFly
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController,UIViewControllerTransitioningDelegate {
    //outlets
    @IBOutlet weak var profileTableView: UITableView!
    
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    //variables
    var mainProfileVM = MainProfileVM()
    var navView = NavigationView().shared
    override func viewDidLoad() {
        super.viewDidLoad()
        didGetProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //profileTableView.reloadData()
        UIApplication.shared.statusBarStyle = .lightContent
        setNavigation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !Changebles.isGrocerOnly{
        Helper.PullToClose(scrollView: self.profileTableView)
        Helper.pullToDismiss?.delegate = self
        }
        load()
    }
    
    //LoadView
    func load() {
        if Utility.getLogin() {
            mainProfileVM.getProfile()
        }else{
            self.mainProfileVM.profileModel = ProfileModelLibrary.menuList
            mainProfileVM.userData = User(data: [:])
            self.profileTableView.reloadData()
            self.profileTableView.scrollsToTop = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.title = ""
        if segue.identifier == UIConstants.SegueIds.profileToSavedCards {
            if let viewController: PaymentVC = segue.destination as? PaymentVC{
                if let btn = sender as? Int {
                    viewController.paymentMethod = btn
                }
            }
        }else if segue.identifier == UIConstants.SegueIds.profileToHistoryVC {
            if let viewController: HistoryVC = segue.destination as? HistoryVC{
                viewController.isFromProfile = true
                viewController.hidesBottomBarWhenPushed = true
            }
        }
        
//        if segue.identifier == String.init(describing: TicketsViewController.self){
//            if let viewController = segue.destination as? TicketsViewController{
//                viewController.titleForView = StringConstants.HelpAndSupport()
//            }
//        }
        
    }
    
    @objc func dismissTheView()
    {
        if Changebles.isGrocerOnly{
            Helper.changetTabTo(index:AppConstants.Tabs.Home)
        }
        else{
        self.dismiss(animated: true, completion: nil)
        }
    }
    
}
extension ProfileVC {
    
    /// this method Observe the mainProfile ViewModel
    func didGetProfile(){
        MainProfileVM.MainProfileVM_response.subscribe(onNext: { success in
            if success {
                if self.profileTableView.numberOfRows(inSection: 0) > 0 {
                    self.mainProfileVM.profileModel = ProfileModelLibrary.menuList
                    self.profileTableView.reloadData()
                }
            }
        }).disposed(by: self.mainProfileVM.disposeBag)
        
        APICalls.LogoutInfoTo.subscribe(onNext: { success in
            if success {
                self.load()
                self.profileTableView.reloadData()
            }
        }).disposed(by: self.mainProfileVM.disposeBag)
        
        
        EditProfileVM.EditProfileVM_response.subscribe(onNext: { success in
            if success == .updateProfile {
                self.load()
            }else if success == .Logout {
                self.profileTableView.reloadData()
            }
        }).disposed(by: self.mainProfileVM.disposeBag)
        
        CommonAlertView.AlertPopupResponse.subscribe(onNext: { data in
            if data == CommonAlertView.ResponceType.Logout {
                self.mainProfileVM.logout()
            }
        }, onError: {error in
        }).disposed(by: self.mainProfileVM.disposeBag)
        
    }
}
