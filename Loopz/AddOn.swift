//
//  AddOn.swift
//  DelivX
//
//  Created by 3Embed on 01/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

struct AddOnGroup{
    var Id                  = ""
    var Name                = ""
    var AddOns:[AddOn]      = []
    var Mandatory           = false
    var Limit:Int           = 0
    var Description         = ""
    var SelectedCount       = 0
    var PackId              = ""
    var multiple            = false
    var maximumLimit:Int        = 0
    var minimumLimit:Int       = 0
    
    init(data:[String:Any],selected:[String]) {
        
       if let titleTemp = data["name"] as? String{
            Name       = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.AddOnsId] as? String{
            Id       = titleTemp
        }
      
        if let titleTemp = data[APIResponceParams.AddOns] as? [[String:Any]] {
            for temp in titleTemp{
                var newData = AddOn.init(data: temp)
                if selected.contains(newData.Id) {
                    newData.Selected = true
                    SelectedCount = SelectedCount + 1
                }
                AddOns.append(newData)
            }
        }
        if let titleTemp = data[APIResponceParams.AddOnsMandatory] as? Int {
            if titleTemp == 1{
                Mandatory  = true
            }else{
                Mandatory = false
            }
        }
        
        if let titleTemp = data[APIResponceParams.AddOnsMultiple] as? Bool {
            multiple = titleTemp
        }
        
        
        if let titleTemp = data[APIResponceParams.AddOnsMultiple] as? Int {
            if titleTemp == 1{
                multiple  = true
            }else{
                multiple = false
            }
        }
      if let titleTemp = data["addOnGroup"] as?  [[String:Any]]  {
         for temp in titleTemp{
            let newData = AddOn.init(data: temp)
            AddOns.append(newData)
         }

      }
        if let titleTemp = data["addOnGroup"] as?  [[String:Any]]  {
            Limit  = titleTemp.count
        }
        
        if let titleTemp = data[APIResponceParams.AddOnsDescription] as? String{
            Description      = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.maximumLimit] as? Int {
            maximumLimit  = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.minimumLimit] as? Int {
            minimumLimit  = titleTemp
        }
      
    }
    
    
    init(data:[String:Any]) {
        if let titleTemp = data[APIResponceParams.AddOnsId] as? String{
            Id       = titleTemp
        }
        if let titleTemp = data[APIResponceParams.PackId] as? String{
            PackId       = titleTemp
        }
        if let titleTemp = data[APIResponceParams.AddOnsGrouped] as? [[String:Any]] {
            for temp in titleTemp{
                AddOns.append(AddOn.init(data: temp))
            }
        }
    }
}

struct AddOn{
    var Id                   = ""
    var Name                 = ""
    var Price:Float          = 0.0
    var Selected             = false
    var Dictionary:[String:Any] = [:]
    
   init(data:[String:Any]) {
      Dictionary = data
      if let titleTemp = data[APIResponceParams.AddOnsId] as? String{
         Id             = titleTemp
      }
      if let titleTemp = data[APIResponceParams.StoreId] as? String{
         Id             = titleTemp
      }
      if let titleTemp = data[APIResponceParams.AddOnName] as? String {
         Name       = titleTemp
      }
      if let NameDict: [String: String] = data[APIResponceParams.AddOnName] as? Dictionary {
         if let titleTemp = NameDict[APIResponceParams.en] {
            Name       = titleTemp
         }
      }
      
      if let titleTemp = data[APIResponceParams.AddOnsprice] as? Float {
         Price            = titleTemp
      }
      if let titleTemp = data[APIResponceParams.AddOnsprice] as? Double {
         Price            = Float(titleTemp)
      }
      if let titleTemp = data[APIResponceParams.AddOnsprice] as? String {
         if titleTemp != "0" && titleTemp != "null"{
            Price            =  titleTemp.floatValue!
         }
      }
   }
    
}
