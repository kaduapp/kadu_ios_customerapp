//
//  HeaderView.swift
//  DelivX
//
//  Created by 3 Embed on 22/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HeaderView: UIView {

    
    @IBOutlet weak var fullButton: UIButton!
    @IBOutlet weak var selectStoreButton: UIButton!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var viewAllView: UIView!
    @IBOutlet weak var viewAllText: UIButton!
    @IBOutlet weak var viewAllArrow: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var seperator1: UIView!
    @IBOutlet weak var detailText: UILabel!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var subTitle: UILabel!
    
    
    var obj: HeaderView? = nil
    var shared: HeaderView {
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[2] as? HeaderView
            obj?.frame = UIScreen.main.bounds
            Fonts.setPrimaryMedium(obj?.headerTitle as Any)
            Fonts.setPrimaryBold(obj?.viewAllText as Any)
           obj?.viewAllText.setTitleColor(Colors.AppBaseColor, for: .normal)
            obj?.viewAllArrow.tintColor = Colors.AppBaseColor
            obj?.viewAllText.setTitle(StringConstants.ViewAll(), for: .normal)
            obj?.backgroundColor = Colors.ScreenBackground
            obj?.headerTitle.textColor = Colors.HeaderTitle
            obj?.fullButton.isHidden = true
            obj?.seperator1.isHidden = true
            obj?.seperator1.backgroundColor = Colors.SeparatorLight
        }
        return obj!
    }
    
    var shared2: HeaderView {
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[6] as? HeaderView
            obj?.frame = UIScreen.main.bounds
            Fonts.setPrimaryRegular(obj?.headerTitle as Any)
            Fonts.setPrimaryRegular(obj?.detailText as Any)
            obj?.backgroundColor = Colors.SecondBaseColor
            obj?.headerTitle.textColor = Colors.SeparatorDark
            if obj?.detailText != nil {
            obj?.detailText.textColor = Colors.SeparatorDark
            obj?.separator.backgroundColor = Colors.SeparatorLight
            }
        }
        return obj!
    }
    
    
    var shared3: HeaderView {
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[10] as? HeaderView
            obj?.frame = UIScreen.main.bounds
            Fonts.setPrimaryMedium(obj?.headerTitle as Any)
            obj?.backgroundColor = Colors.ScreenBackground
            obj?.headerTitle.textColor = Colors.HeaderTitle
        }
        return obj!
    }

}
