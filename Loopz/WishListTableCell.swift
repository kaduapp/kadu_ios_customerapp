//
//  WishListTableCell.swift
//  UFly
//
//  Created by 3 Embed on 04/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class WishListTableCell: UITableViewCell {
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var addCartView: UIView!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    
    var dataItem:Item?  = nil
    var cartDb = CartDBManager()
    var cartVM = CartVM()
    let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    /// initial view setup
    func initialSetup() {
        Fonts.setPrimaryBold(removeBtn)
        Fonts.setPrimaryBold(addToCartBtn)
        
        Fonts.setPrimaryMedium(itemName)
        Fonts.setPrimaryRegular(discount)
        Fonts.setPrimaryRegular(amount)
        Fonts.setPrimaryRegular(weight)
        Fonts.setPrimaryRegular(quantity)
        
        discount.textColor =  Colors.AppBaseColor
        itemName.textColor =  Colors.PrimaryText
        amount.textColor =  Colors.SecoundPrimaryText
        weight.textColor =  Colors.SecoundPrimaryText
        
        Helper.setButtonTitle(normal: StringConstants.AddCart(), highlighted: StringConstants.AddCart(), selected: StringConstants.AddCart(), button: addToCartBtn)
        
        Helper.setButtonTitle(normal: StringConstants.Remove(), highlighted: StringConstants.Remove(), selected: StringConstants.Remove(), button: removeBtn )
        
        Helper.setUiElementBorderWithCorner(element: plusBtn, radius: Helper.setRadius(element: plusBtn), borderWidth: 1.0,color: Colors.Red)
        Helper.setUiElementBorderWithCorner(element: minusBtn, radius: Helper.setRadius(element: minusBtn), borderWidth: 1.0,color:  Colors.Red)
    }
    
    func updateCart() {
        let count = (self.dataItem?.CartQty)!
        if count > 0 {
            self.dataItem?.CartQty = count
            self.addToCartBtn.isHidden = true
            self.addCartView.isHidden = false
        }else {
            self.dataItem?.CartQty = 0
            self.addToCartBtn.isHidden = false
            self.addCartView.isHidden = true
        }
        self.quantity.text = String(describing: (self.dataItem?.CartQty)!)
        let cart = self.cartDb.getCartFullCount(data: self.dataItem!)
        for each in cart {
            self.dataItem?.CartQty = (self.dataItem?.CartQty)! + each.QTY
        }
        
        self.dataItem?.CartId = Utility.getCartId()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(item:Item) {
        
        dataItem = item
        if item.Images.count > 0{
            Helper.setImage(imageView: itemImage, url: item.Images[0], defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        }
        itemName.text = item.Name
        discount.text = ""
        amount.text = Helper.df2so(Double( item.Price))
        updateCart()
        CartVM.cartVM_response.subscribe(onNext: {success in
            self.updateCart()
        }, onError: {error in
        }).disposed(by: self.disposeBag)
        CartDBManager.cartDBManager_response.subscribe(onNext: { success in
            if success == false {
                self.dataItem?.setCart()
                self.updateCart()
            }
        }).disposed(by: self.disposeBag)
    }
    
    
    @IBAction func didTapAddToCart(_ sender: Any) {
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
        addToCartBtn.isHidden = true
        addCartView.isHidden = false
        dataItem?.CartQty = 1
        cartVM.addCart(cartItem: CartItem.init(item: dataItem!))
        updateCart()
    }
    
    @IBAction func plusBtnAction(_ sender: Any) {
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
        dataItem?.CartQty = (dataItem?.CartQty)! + 1
        cartVM.updateCart(cartItem: CartItem.init(item: dataItem!))
        updateCart()
    }
    
    @IBAction func minusBtnAction(_ sender: Any) {
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
        let count = (dataItem?.CartQty)! - 1
        if count > 0 {
            dataItem?.CartQty = count
            addToCartBtn.isHidden = true
            addCartView.isHidden = false
            cartVM.updateCart(cartItem: CartItem.init(item: dataItem!))
        }else {
            dataItem?.CartQty = 0
            addToCartBtn.isHidden = false
            addCartView.isHidden = true
            cartVM.deleteCart(cartItem: CartItem.init(item: dataItem!))
        }
    }
}
