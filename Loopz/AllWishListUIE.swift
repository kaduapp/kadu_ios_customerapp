//
//  AllWishListUIE.swift
//  DelivX
//
//  Created by 3 Embed on 08/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension AllWishListVC {

    // shows empty screen if data is not there and start shopping button
    func setEmptyScreen() {
        emptyView.setData(image: #imageLiteral(resourceName: "EmptyWishlist"), title: StringConstants.EmptyWish(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        emptyView.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
        addtoWishListButton.setTitle(StringConstants.ADDWISHLIST(), for: .normal)
         mainTableView.backgroundColor = Colors.ScreenBackground
        Helper.setButton(button: addtoWishListButton,view:addtoWishListView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: false)
        Fonts.setPrimaryMedium(addtoWishListButton)
    }
    
    
  // action for start shopping button take to home screen
    @objc func StartShopping() {
        self.dismiss(animated: true, completion: nil)
//        Helper.changetTabTo(index: AppConstants.Tabs.Home)
//        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setObserver(){
        FavouriteAPICalls.getWishList(id:"0",pi:true).subscribe({ [weak self]data in
            self?.mainTableView.reloadData()
        }).disposed(by: self.allWishListVM.disposeBag)
    }

}
