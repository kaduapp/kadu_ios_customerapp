//
//  SignInVC.swift
//  UFly
//
//  Created by Rahul Sharma on 14/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FacebookCore
import FacebookLogin

class SignInVC: UIViewController{
    
    @IBOutlet weak var signInView: SignInView!
    @IBOutlet weak var navigationBackView: UIView!
    @IBOutlet weak var navTitleText: UILabel!
    @IBOutlet weak var closeBtn: UIBarButtonItem!
    let disposeBag = DisposeBag()
    var signinVM = SignInVM()
    
    var continueFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
//        if FBSDKAccessToken.current() {
//            // User is logged in, do work such as go to next view controller.
//            print("Login")
//        }
        didGetResponse()
        updateView()
        signinVM.authentication.CountryCode = Helper.setCountryCode()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        signInView.phoneNumTF.becomeFirstResponder()
        self.continueFlag = false
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Hello")
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.addObserver()
        Helper.transparentNavigation(controller: self.navigationController!)
        if Utility.getLogin() {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.removeObserver()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        //Helper.nonTransparentNavigation(controller: self.navigationController!)
    }
    
    //Going to Signup
    func gotoSignup() {
        if !continueFlag{
            continueFlag = true
            performSegue(withIdentifier: UIConstants.SegueIds.SigninToSignup,sender: self)
        }
    }
    
    @IBAction func didTapForgotPassword(_ sender: Any) {
        signinVM.showAlert = false
        signInView.passwordTF.text = ""
        performSegue(withIdentifier: UIConstants.SegueIds.LoginToForgotPassword,sender: self)
    }
    
    //MARK: - UIButton Action
    @IBAction func didTapCloseButton(_ sender: Any) {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.signinVM.willClose = true
                        self.signInView.passwordTF.resignFirstResponder()
                        self.signInView.phoneNumTF.resignFirstResponder()
        }) { (Bool) in
            UIView.animate(withDuration: 0.3, animations: {
                self.dismiss(animated: true, completion: nil)
            }){ (Bool) in
                if AppConstants.SelectedIndex == AppConstants.Tabs.History || AppConstants.SelectedIndex == AppConstants.Tabs.Cart {
                    Helper.changetTabTo(index: AppConstants.Tabs.Home)
                }
                
            }
        }
    }
    
    @IBAction func didTapSignUp(_ sender: Any) {
        signInView.phoneNumTF.text = ""
        signInView.passwordTF.text = ""
        signinVM.authentication.Phone = ""
        signInView.phNumTickMark.isHidden = true
        signInView.pwNumTickMark.isHidden = true
        
        self.gotoSignup()
    }
    
    @IBAction func didTapNextButton(_ sender: Any) {
        //Continue Btn Action
        
        if self.signInView.passwordTFHight.constant != 0 {
            if signInView.passwordTF.text!.sorted().count == 0 {
                Helper.showAlert(message: StringConstants.PasswordAvailable(), head: StringConstants.Error(), type: 1)
                return
            }
        }
        signInView.activeTextField = nil
        dismissKeyboard()
        
    }
    
    
    @IBAction func didTapFACEBOOKBTN(_ sender: Any) {
//        let loginManager = LoginManager()
//        loginManager.logIn(readPermissions: [.publicProfile,.email], viewController: self) { (loginResult) in
//            switch loginResult {
//            case .failed(let error):
//                print(error)
//            case .cancelled:
//                print("User cancelled login.")
//            case .success(grantedPermissions: (let grantedPermissions, declinedPermissions: let declinedPermissions, token: let accessToken)):
//                print("Logged in!")
//            }
//        }
    }
    
    @IBAction func countryCodeAction(_ sender: Any) {
        
        let vnhStoryboard = UIStoryboard(name: UIConstants.ControllerIds.VNHCountryPicker, bundle: nil)
        if let nav: UINavigationController = vnhStoryboard.instantiateInitialViewController() as! UINavigationController? {
            let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker
            // delegate
            picker.delegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == UIConstants.SegueIds.SigninToSignup {
            //let nav = segue.destination as! UINavigationController
            let viewController: SignUpVC = segue.destination as! SignUpVC
            if signinVM.authentication.Phone != "" {
                viewController.signupVM.authentication = signinVM.authentication
            }
            
        }else if segue.identifier == UIConstants.SegueIds.LoginToForgotPassword {
            let forgotVC:ForgotPasswordVC = segue.destination as! ForgotPasswordVC
            forgotVC.forgotPasswordVM.authentication = signinVM.authentication
            
        }else if segue.identifier == UIConstants.SegueIds.SigninToOTP {
            let otpVC:OTPVC = segue.destination as! OTPVC
            otpVC.otpVM.authentication = signinVM.authentication
        }
    }
}
extension SignInVC {
    
    /// initial view setup
    func updateView(){
        navTitleText.text = ""
        Fonts.setPrimaryMedium(navTitleText)
        navTitleText.textColor = Colors.PrimaryText
        signInView.phoneNumTF.text = signinVM.authentication.CountryCode
        self.signInView.phoneNumTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.signInView.passwordTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        Helper.transparentNavigation(controller: self.navigationController!)
        //        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    /// observe for response of signinVM
    func didGetResponse(){
        signInView.countryCodeBtn.isEnabled = false
        signinVM.loginVM_response.subscribe(onNext: {[weak self]success in
            self?.subscribedData(data:success)
            }, onError: {error in
                print(error)
        }).disposed(by: signinVM.disposeBag)
        Utility.UtilityUpdate.subscribe(onNext: { [weak self]data in
            self?.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
}

