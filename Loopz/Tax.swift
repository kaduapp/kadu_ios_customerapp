//
//  Tax.swift
//  DelivX
//
//  Created by 3Embed on 10/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct Tax {
    var Name        = ""
    var ID          = ""
    var Price:Float = 0
    var Value:Float = 0
    
    init(data:[String:Any]) {
        if let temp = data[APIResponceParams.TaxName] as? String {
            Name = temp
        }
        if let temp = data[APIResponceParams.TaxId] as? String {
            ID = temp
        }
        if let temp = data[APIResponceParams.TaxValue] as? Float {
            Value = temp
        }
        if let temp = data[APIResponceParams.Price] as? Float {
            Price = temp
        }
        if let temp = data[APIResponceParams.TaxValue] as? String {
            Value = Float(temp)!
        }
        if let temp = data[APIResponceParams.Price] as? String {
            Price = Float(temp)!
        }
        if let temp = data[APIResponceParams.TaxValue] as? Double {
            Value = Float(temp)
        }
        if let temp = data[APIResponceParams.Price] as? Double {
            Price = Float(temp)
        }
    }
}

struct Tip {
    var ID          = ""
    var status:Int = 0
    var statusMsg:String = ""
    var tipValue:Float = 0
    
    init(data:[String:Any]) {
        
        if let temp = data[APIResponceParams.IdAddress] as? String {
            ID = temp
        }
        if let temp = data[APIResponceParams.statusMsg]
        {
            statusMsg = "\(temp)"
        }
        if let temp = data[APIResponceParams.tipValue] as? Double {
            tipValue = Float(temp)
        }
        if let temp = data[APIResponceParams.status]   {
            status = temp as! Int
        }
        
    }
}
