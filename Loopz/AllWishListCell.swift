//
//  AllWishListCell.swift
//  DelivX
//
//  Created by 3Embed on 04/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AllWishListCell: UITableViewCell {

    @IBOutlet weak var allBackgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       initialSetup()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    /// initialsetup
    func initialSetup(){
        Fonts.setPrimaryRegular(titleLabel)
        titleLabel.textColor =  Colors.PrimaryText
        Helper.setUiElementBorderWithCorner(element: allBackgroundView, radius: 3, borderWidth: 0, color:  Colors.PrimaryText)
        allBackgroundView.backgroundColor = Colors.SeparatorLight
    }
    
    
    /// updates the cell
    ///
    /// - Parameter data: is of type PopupModel object
    func setData(data:PopupModel) {
        titleLabel.text = data.String
    }
}
