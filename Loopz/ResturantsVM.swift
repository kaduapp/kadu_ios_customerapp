//
//  ResturantsVM.swift
//  DelivX
//
//  Created by 3Embed on 26/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class ResturantsVM: NSObject{
    let disposeBag = DisposeBag()
    var arrayOfResturants:[Store] = []
    var arrayOfFavorites :[Store] = []
    var arrayOfOffer:[Offer] = []
    var offset:Int = 0
    var limit:Int = 15
    var response_ResturantsVM  = PublishSubject<Bool>()
    var selectedRating = 0
    var selectedFoodType = ""
    var selectedCosine = ""
    var selectedStoreForDetail:Store = Store.init(data: [:])
    
    
    func getData()
    {
        HomeAPICalls.getRestuarants(offset:offset,limit:limit).subscribe(
            onNext:
            { [weak self]data in
                
                if self?.offset == 0{
                    self?.arrayOfResturants.removeAll()
                    self?.arrayOfOffer.removeAll()
                 self?.arrayOfFavorites.append(contentsOf: data.2)
                }
                self?.arrayOfResturants.append(contentsOf: data.0)
                self?.arrayOfOffer.append(contentsOf: data.1)
                self?.response_ResturantsVM.onNext(true)
        }).disposed(by: self.disposeBag)
    }
    
    func getAllFavoriteStore()
    {
        HomeAPICalls.viewAllFavouriteStore(offset:offset,limit:limit).subscribe(
            onNext:
            { [weak self]data in
                
                if self?.offset == 0{
                    self?.arrayOfResturants.removeAll()
                    self?.arrayOfOffer.removeAll()
                    self?.arrayOfFavorites.removeAll()
                }
                self?.arrayOfResturants.append(contentsOf: data.0)
                self?.response_ResturantsVM.onNext(true)
        }).disposed(by: self.disposeBag)
    }
    
    
}
