//
//  HeaderRating.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HeaderRating: UIView {
    
    @IBOutlet var driverImage: UIImageView!
    @IBOutlet var driverName: UILabel!
    @IBOutlet var driverType: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var tipHead: UILabel!
    @IBOutlet var tipCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialViewSetup()
    }
    
    func initialViewSetup() {
        Helper.setUiElementBorderWithCorner(element: driverImage, radius: Helper.setRadius(element: driverImage), borderWidth: 1, color: Colors.AppBaseColor)
        
        Fonts.setPrimaryMedium(driverName)
        Fonts.setPrimaryRegular(driverType)
        Fonts.setPrimaryBold(priceLabel)
        Fonts.setPrimaryMedium(tipHead)
        
        driverName.textColor = Colors.PrimaryText
        driverType.textColor = Colors.SecoundPrimaryText
        priceLabel.textColor = Colors.PrimaryText
        tipHead.textColor = Colors.SecoundPrimaryText
        self.backgroundColor = Colors.SeparatorLight
    }
    
    func setData(order:Order) {
        priceLabel.text = Helper.df2so(Double( order.TotalAmount))
        driverType.text = order.DriverType
        driverName.text = order.DriverName
        Helper.setImage(imageView: driverImage, url: order.DriverImage, defaultImage: #imageLiteral(resourceName: "UserDefault"))
        tipCollectionView.reloadData()
    }
    
}
