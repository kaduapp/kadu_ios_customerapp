//
//  ReviewUIE.swift
//  DelivX
//
//  Created by 3Embed on 21/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation


extension ReviewVC:UIScrollViewDelegate {
    func setUI() {
        Helper.PullToClose(scrollView: mainCollectionView)
        Helper.pullToDismiss?.delegate = self
        Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor, shadow: true)
        Helper.setButtonTitle(normal: StringConstants.Submit(), highlighted: StringConstants.Submit(), selected: StringConstants.Submit(), button: bottomButton)
        
        Fonts.setPrimaryMedium(titleLabel)
        Fonts.setPrimaryRegular(subTitleLabel)
        
        titleLabel.textColor = Colors.PrimaryText
        subTitleLabel.textColor = Colors.PrimaryText
        
        titleLabel.text = StringConstants.YourLastOrder()
        subTitleLabel.text = String(describing: reviewVM.orderRecieved!.BookingId)
        navWidth.constant = UIScreen.main.bounds.width
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        }else{
            Helper.addShadowToNavigationBar(controller: self.navigationController!)
        }
    }
}
