//
//  SearchStoreTVE.swift
//  DelivX
//
//  Created by 3EMBED on 14/11/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension SearchStoreVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchVM.arrayOfStores.count > 0{
            return searchVM.arrayOfStores.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:  String(describing: DispensariesTableCell.self)) as! DispensariesTableCell
        cell.setDataForSearchStorez(data: searchVM.arrayOfStores[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        searchTF.resignFirstResponder()
        var storeIsOpen = false
            storeIsOpen = self.searchVM.arrayOfStores[indexPath.row].StoreIsOpen
        
        if Changebles.isGrocerOnly && storeIsOpen {
            Utility.saveStore(location: searchVM.arrayOfStores[indexPath.row].dataObject)
            Utility.UtilityUpdate.onNext(Utility.TypeServe.ChangeStore)
            self.navigationController?.popViewController(animated: true)
        }
        else if storeIsOpen {
            Utility.saveStore(location: searchVM.arrayOfStores[indexPath.row].dataObject)
            if Utility.getSelectedSuperStores().type == .Restaurant{
                self.performSegue(withIdentifier: String(describing: RestHomeVC.self), sender: indexPath.row)
            }
            else{
                self.performSegue(withIdentifier: String(describing: HomeVC.self), sender: indexPath.row)
            }
        }
    }
}


