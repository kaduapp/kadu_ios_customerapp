//
//  StoresListTVCell.swift
//  DelivX
//
//  Created by 3Embed on 28/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class StoresListTVCell: UITableViewCell {
    
    @IBOutlet weak var closedDescriptionLbl: UILabel!
    @IBOutlet weak var openStore:UILabel!
    
    @IBOutlet weak var minOrderLabel: UILabel!
    @IBOutlet weak var distanceIMG: UIImageView!
    @IBOutlet weak var storeOuterView: UIView!
    @IBOutlet weak var freedeliveryHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBOutlet weak var deliveryLB: UILabel!
    @IBOutlet weak var distanceLB: UILabel!
    
    @IBOutlet weak var deliveryLBImage: UIImageView!
    @IBOutlet weak var ratingIMG: UIImageView!
    @IBOutlet weak var ratingLB: UILabel!
    //@IBOutlet weak var priceLB: UILabel!
    @IBOutlet weak var seperationView: UIView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    var constraintFlagSubtracted:Bool = false
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        setImageAttributes()
        setTheFontColors()
        setUpTheFontsSize()
    }
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func configureCell(data:Store){
        self.titleLabel.text = data.Name
        if data.SubcategoryName == "" {
            self.subTitle.text = data.storeArea
        }else{
            self.subTitle.text = data.SubcategoryName + "\n" + data.storeArea
        }
        //self.ratingLB.text = "\(data.StoreRating)"
        self.ratingLB.text = "\(Helper.clipDigit(valeu: data.StoreRating, digits: 1))"
        
        var distanceMetric = StringConstants.MileAway()
        if Int(Utility.getCurrency().2) == 1{
            distanceMetric = StringConstants.MileAway()
             self.distanceLB.text = "\(Helper.clipDigit(valeu: data.DistanceMiles, digits: 0))" + " " + "\(distanceMetric)"
        }else{
            distanceMetric = StringConstants.KMAway()
             self.distanceLB.text = "\(Helper.clipDigit(valeu: data.DistanceKm.floatValue ?? 0.0, digits: 2))" + " " + "\(distanceMetric)"
         }
       
        if data.TypeStore.rawValue == 1{
            var minText = Helper.df2so(Double(data.CostForTwo))
            minOrderLabel.text =  "\(minText) \(StringConstants.ForTwo())"

        }
        else{
            let minText = Helper.df2so(Double(data.MinimumOrder))
            minOrderLabel.text =  "Min \(minText)"
        }
        
        //priceLB.isHidden = true
        if data.Image.count > 0 {
            imageWidth.constant = self.mainImageView.frame.size.height
        }else{
            imageWidth.constant = 0
        }
        Helper.setImage(imageView: self.mainImageView, url: data.Image, defaultImage: UIImage())
        
        deliveryLB.isHidden = false
        deliveryLBImage.isHidden = false
        if data.OfferId.length > 0 {
            deliveryLB.text = data.OfferTitle
            deliveryLB.textColor = Colors.DeliveryBtn
        }else if data.FreeDelivery == 0 {
            deliveryLB.text = StringConstants.FreeDeliveryAvailable()
            deliveryLB.textColor = Colors.DeliveryBtn
        }else{
            deliveryLB.isHidden = true
            deliveryLB.text = ""
            deliveryLBImage.isHidden = true
        }
        self.closedDescriptionLbl.text = ""
        openStore.text = ""
        if data.StoreIsOpen{
            self.setTheFontColors()
            self.storeOuterView.isHidden = true
        }else{
                    self.setTheFonrColorForCloseStore()
                    self.storeOuterView.isHidden = false
                    if data.nextOpenTime != "" {
        //                if data.todayIsOpen == 1{
        //                    self.timeLbl.text = StringConstants.Opens() + " " +  StringConstants.Today() + " " + StringConstants.At() + " " + data.nextOpenTime
        //                }else{
                        
                       // let str = Helper.getDateString12hrsFormat(dateStr: data.nextOpenTime)
                        if data.nextOpenTime.contains(",")
                        {
                              let array = data.nextOpenTime.components(separatedBy: ",")
                            self.openStore.text = StringConstants.Opens() + " " + array[0] + " " + StringConstants.At() + " " + array[1] //+ StringConstants.Tomorrow() + " "

                        }else{
                             self.openStore.text = StringConstants.Opens() + " "  + StringConstants.At() + " " + data.nextOpenTime //data.nextOpenTime
                        }
                        
                           // self.timeLbl.text = StringConstants.Opens() + " "  + StringConstants.At() + " " + data.nextOpenTime //+ StringConstants.Tomorrow() + " "
                      //  }
                          self.closedDescriptionLbl.text = StringConstants.Closed()
                    }else{
                        self.closedDescriptionLbl.text = StringConstants.Closed()
                        self.openStore.text = ""
                    }
                }

    }
    fileprivate func setImageAttributes()
    {
        Helper.setShadow(sender: mainImageView)
    }
    fileprivate func setTheFontColors()
    {
        titleLabel.textColor = Colors.PrimaryText
        subTitle.textColor = Colors.SeconderyText
        distanceLB.textColor = Colors.SectionHeader
        ratingLB.textColor = Colors.SectionHeader
        minOrderLabel.textColor = Colors.SectionHeader
        seperationView.backgroundColor = Colors.SeparatorLarge
        deliveryLB.tintColor = Colors.DeliveryBtn
        closedDescriptionLbl.textColor = UIColor.red
        openStore.textColor = UIColor.red
        self.ratingIMG.setImageColor(color: Colors.PrimaryText)
        self.distanceIMG.setImageColor(color: Colors.PrimaryText)
    }
    
    fileprivate func setTheFonrColorForCloseStore(){
        titleLabel.textColor = Colors.ClosedStoreColor
        subTitle.textColor = Colors.ClosedStoreColor
        distanceLB.textColor = Colors.ClosedStoreColor
        ratingLB.textColor = Colors.ClosedStoreColor
        minOrderLabel.textColor = Colors.ClosedStoreColor
        seperationView.backgroundColor = Colors.SeparatorLarge
        deliveryLB.tintColor = Colors.ClosedStoreColor
        minOrderLabel.tintColor = Colors.ClosedStoreColor
        self.ratingIMG.setImageColor(color: Colors.ClosedStoreColor)
        self.distanceIMG.setImageColor(color: Colors.ClosedStoreColor)
        deliveryLB.tintColor = Colors.ClosedStoreColor
        self.storeOuterView.backgroundColor = Colors.ClosedStoreColor
        self.storeOuterView.alpha = 0.5
    }

    func setUpTheFontsSize(){
        Fonts.setPrimaryBold(titleLabel, size: 16)
        Fonts.setPrimaryRegular(distanceLB, size: 12)
        Fonts.setPrimaryRegular(closedDescriptionLbl, size: 11)
        Fonts.setPrimaryRegular(ratingLB, size: 12)
        //Fonts.setPrimaryRegular(priceLB, size: 12)
        Fonts.setPrimaryRegular(subTitle, size: 12)
        Fonts.setPrimaryRegular(distanceLB, size: 12)
        Fonts.setPrimaryRegular(deliveryLB, size: 12)
        Fonts.setPrimaryRegular(minOrderLabel, size: 12)
    }
}
