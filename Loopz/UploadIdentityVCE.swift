//
//  UploadIdentityVCE.swift
//  DelivX
//
//  Created by 3 Embed on 20/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension UploadIdentityVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
        let movedOffset: CGFloat = sender.contentOffset.y
        print(movedOffset)
        if sender == uploadIDView.scrollView {
            if movedOffset <= 0 {
                sender.contentOffset.y = 0
            }
            let pageWidth: CGFloat = 200 - 88             //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            if ratio >= 1 {
                uploadIDView.navigationBackView.isHidden = false
                uploadIDView.navigationBackView.backgroundColor = UIColor.white
                uploadIDView.navTitleText.textColor = UIColor.black
            }else {
                uploadIDView.navigationBackView.isHidden = true
            }
        }
    }
}
