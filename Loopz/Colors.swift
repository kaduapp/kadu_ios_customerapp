//
//  Colors.swift
//  UFly
//
//  Created by 3Embed on 26/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Colors: NSObject {
    
    static let AppBaseColor                                 = Helper.getUIColor(color:Changebles.AppColor)
    static let SecondBaseColor                              = Helper.getUIColor(color:"FFFFFF")//White Color
    
    static let SeparatorDark                                = Helper.getUIColor(color:"A3A3A3")
    static let SeparatorLight                               = Helper.getUIColor(color:"DFE4EF")
    static let SeparatorLarge                               = Helper.getUIColor(color:"F5F6F9")
    static let ClosedStoreColor                             = Helper.getUIColor(color: "ADABAB")
    
    static let PrimaryText                                  = Helper.getUIColor(color:"1A1945")//Black
    static let SectionHeader                                = Helper.getUIColor(color:"2F2E3A")
    static let SeconderyText                                = Helper.getUIColor(color:"7B8091")//Light Secound Head
    
    static let FieldHeader                                  = Helper.getUIColor(color:"A8AAB2")
    static let PlaceHolder                                  = Helper.getUIColor(color:"D8DCE5")
    
    
    static let PageBackground                               = Helper.getUIColor(color:"E8EBF3")
    static let PickupBtn                                    = Helper.getUIColor(color:"4F86EB")
    static let DeliveryBtn                                  = Helper.getUIColor(color:"48C16A")
    
    
    
    static let SecoundPrimaryText                           = Helper.getUIColor(color:"858B92")//Black
    // static let SecounderyText                               = "747474"//Gray
    static let HeadColor                                    = Helper.getUIColor(color:"C4C4C4")
    static let ScreenBackground                             = Helper.getUIColor(color:"F5F5F5")
    static let AddButton                                    = Helper.getUIColor(color:"4695E6")
    static let DoneBtnNormal                                = Helper.getUIColor(color:"DFE2E7")//
    static let Red                                          = Helper.getUIColor(color:"D95656") // red for cart button
    static let BlueColor                                    = Helper.getUIColor(color:"4695E6") //"4695E6" //payment ,orderDetail
    //static let SectionHeader                                = "4A4A4A" //OrderDetail
    static let Unselector                                   = Helper.getUIColor(color:"B9B9B9") // orderDetail
    static let gotItBtnColor                                = Helper.getUIColor(color:"C9A353") //ItemListinVC BotonBarText
    static let BtnBackground                                = Helper.getUIColor(color:"BBC0CE")
    static let grey                                         = Helper.getUIColor(color:"A8AAB2")
    static let Sender                                = Helper.getUIColor(color:"dcf8c6")
    static let Receiver                                         = Helper.getUIColor(color:"ece5dd")
    
    
    //Addons
    static let lightScreenBackground                        = Helper.getUIColor(color:"E9EBF8")
    static let lightGreen                                   = Helper.getUIColor(color:"63CB63")
    static let DarkBlueBlack                                = Helper.getUIColor(color:"242A4B")//addOnsTitleFontcolor
    static let lightBlueBlack                                = Helper.getUIColor(color:"858AA8")//addOnsFontcolor#858AA8
    
    
    static let SearchText                                  = Helper.getUIColor(color:"686B78")
    static let SearchHighlight                              = Helper.getUIColor(color:"282B3F")
    static let HeaderTitle                                  = Helper.getUIColor(color:"535766")
    static let HeaderBackground                             = Helper.getUIColor(color:"EFF0F7")
    static let ReviewBlack                                  = Helper.getUIColor(color:"606477")
    
    
    
    
    //Superstore
    static let Grocery                      = Helper.getUIColor(color: "FFD00A")
    static let Restaurant                   = Helper.getUIColor(color: "9ACC56")
    static let Marijuana                    = UIColor.init(named: "Marijuana")
    static let Fashion                      = Helper.getUIColor(color: "FF9090")
    
    // Confirm Order
    static let laundryUnselectCellColor     = Helper.getUIColor(color: "C1C1C1")
}

