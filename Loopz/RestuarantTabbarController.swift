//
//  RestuarantTabbarController.swift
//  DelivX
//
//  Created by 3Embed on 15/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class RestuarantTabbarController: UITabBarController {
    
    var tabBarIndex = 0
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        didCartUpdate()
        // Do any additional setup after loading the view.
        if let viewControllers = self.viewControllers{
            let home = viewControllers[0]
            let history = viewControllers[1]
            let cart = viewControllers[2]
            let search = viewControllers[3]
            self.viewControllers = [home,search,cart,history]
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTabBadge()
    }
}

extension RestuarantTabbarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        tabBarIndex = tabBarController.selectedIndex
        AppConstants.SelectedIndex = AppConstants.Tabs(rawValue: tabBarIndex)!
        switch tabBarIndex {
            case 2,3:
                if !Helper.isLoggedIn(isFromHist: true){
                    self.tabBarController?.selectedIndex = 0
                }
            default:
                return
            }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AppDelegate.tabChange != nil {
            let temp = AppDelegate.tabChange
            AppDelegate.tabChange = nil
            Helper.changetTabTo(index: temp!)
        }
    }
    
}

extension RestuarantTabbarController{
    
    /// Observe CartDBManager
    func didCartUpdate(){
        CartDBManager.cartDBManager_response.subscribe(onNext: { [weak self]success in
            self?.updateTabBadge()
            if success == false {
                self?.tabBarController?.selectedIndex = 0
            }
        }).disposed(by: disposeBag)
    }
    
    /// update the TabBadge which holds the cartCount
    func updateTabBadge() {
        if (AppConstants.CartCount > 0) {
            let tabItem = self.tabBar.items![2]
            tabItem.badgeValue = String(AppConstants.CartCount)
        }else{
            let tabItem = self.tabBar.items![2]
            tabItem.badgeValue = nil
        }
    }
}
