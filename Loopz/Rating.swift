//
//  Rating.swift
//  DelivX
//
//  Created by 3Embed on 22/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct Star {
    var Name = ""
    var Options:[(String,Bool)] = []
    var Value = 0
    
}


struct Rating {
    var Name = ""
    var Options:[Star] = []
    var Value:Int = 5
    var Id = ""
    var Associated = 0
    
    init(data:[String:Any]) {
        if let temp = data[APIResponceParams.Name] as? String {
            Name = temp
        }
        if let temp = data[APIResponceParams.IdAddress] as? Int {
            Id = "\(temp)"
        }
        if let temp = data[APIResponceParams.IdAddress] as? String {
            Id = temp
        }
        
        if let temp = data[APIResponceParams.Associated] as? Int {
            Associated = temp
        }
        
        if let temp = data[APIResponceParams.DriverRating] as? [String:Any] {
            let unSortedArray = temp.compactMap(){$0.0}
            var tempKeys = unSortedArray.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }

            for i in 0...tempKeys.count-1 {
                var dict = Star()
                dict.Value = Int(tempKeys[i])!
                if let dataValue = temp[tempKeys[i]] as? [String] {
                    for item in dataValue {
                        dict.Options.append((item,false))
                    }
                }
                Options.append(dict)
            }
        }
        Value = Options.count
        if Options.count == 0 {
            Value = 5
        }
    }
}
