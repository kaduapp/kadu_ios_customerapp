//
//  APICalls.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import ReachabilitySwift

class APICalls: NSObject {
    static let disposesBag = DisposeBag()
    enum ErrorCode: Int {
        case success                = 200
        case created                = 201
        case deleted                = 202 //Accepted forgotPW //Accepted for delete item in Laundry
        case updated                = 203
        case NoUser                 = 204
        case badRequest             = 400
        case Unauthorized           = 401
        case Required               = 402
        case Forbidden              = 403
        case NotFound               = 404
        case MethodNotAllowed       = 405
        case NotAcceptable          = 406
        case Banned1                = 408
        case Banned                 = 415
        case ExpiredOtp             = 410
        case StoredoesntAccept      = 411
        case PreconditionFailed     = 412
        case RequestEntityTooLarge  = 413
        case TooManyAttemt          = 429
        case ExpiredToken           = 440
        case InvalidToken           = 498
        case InternalServerError    = 500
        case InternalServerError2   = 502
        case InternalServerError3    = 420
        
    }
    static let LogoutInfoTo = PublishSubject<Bool>()
    //Refresh Token
    class func refreshToken() {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return
        }
        let header = Utility.getHeader()
        RxAlamofire.requestJSON(.get, APIEndTails.RefreshToken, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
            Helper.hidePI()
            let bodyIn = body as! [String:Any]
            let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
            if errNum == .success {
                let newToken = bodyIn[APIResponceParams.Data] as! String
                Utility.saveSession(token: newToken)
            }else{
                APICalls.basicParsing(data:bodyIn,status:head.statusCode)
            }
        }, onError: { (Error) in
            Helper.hidePI()
        }).disposed(by: self.disposesBag)
    }
    
    
    
    class func configure() ->Observable<Bool> {
        let header = Utility.getHeaderWithoutAuth()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, APIEndTails.Configure, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as! [String:Any]
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let data = bodyIn[APIResponceParams.Data] as! [String:Any]
                    Utility.setAppConfig(data: data)
                }
                observer.onNext(true)
                observer.onCompleted()
            }, onError: { (Error) in
                Helper.hidePI()
                observer.onNext(false)
                observer.onCompleted()
            }).disposed(by: self.disposesBag)
            return Disposables.create()
        }.share(replay: 1)
    }
    
    
    class func isUpdateAvailable() throws -> Bool {
        guard let url = URL(string: APIEndTails.VersionAppStore) else {
            throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            return version != Utility.AppVersion
        }
        throw VersionError.invalidResponse
    }
    
    
    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }
    
    
    
    class func appVersion() {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return
        }
        let header = Utility.getHeaderWithoutAuth()
        let params = [
            APIRequestParams.OrderType      :   22,
            APIRequestParams.Version        :   Utility.AppVersion
            ] as [String : Any]
        RxAlamofire.requestJSON(.post, APIEndTails.Version, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
            if errNum == .badRequest {
                if let data = bodyIn[APIResponceParams.Data] as? [String:Any] {
                    if let mandatory = data[APIRequestParams.Mandatory] as? Bool {
                        var message = bodyIn[APIResponceParams.Message] as! String
                        if let set = data[APIRequestParams.Version] as? String {
                            message = message + " " + set
                        }
                        var appleaccountVersion:Double = 0.0
                        appleaccountVersion = data[APIRequestParams.Version] as? Double ?? 1.0
                        let currentVersion:Double = Double(Utility.AppVersion) ?? 1.0
                        if appleaccountVersion > currentVersion {
                            Helper.showAlertReturn(message: message, head: StringConstants.Update(), type: StringConstants.UpdateLower(), closeHide: mandatory, responce: CommonAlertView.ResponceType.AppVersion)
                        }
                    }
                }
            }
        }, onError: { (Error) in
            Helper.hidePI()
        }).disposed(by: self.disposesBag)
    }
    
    
    class func languages() ->Observable<[Language]> {
        //        let header = Utility.getHeaderWithoutAuth()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, APIEndTails.Languages, parameters: [:], headers: [:]).debug().subscribe(onNext: { (head, body) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                var languages:[Language] = []
                if errNum == .success {
                    let bodyIn = body as! [String:Any]
                    let data = bodyIn[APIResponceParams.Data] as! [Any]
                    for each in data {
                        languages.append(Language.init(each as! [String:Any]))
                    }
                }
                observer.onNext(languages)
                observer.onCompleted()
            }, onError: { (Error) in
                Helper.hidePI()
                observer.onNext([])
                observer.onCompleted()
            }).disposed(by: self.disposesBag)
            return Disposables.create()
        }.share(replay: 1)
    }
    
    
    class func basicParsing(data:[String:Any],status: Int) {
        let dataIn = Helper.nullKeyRemoval(data: data)
        Helper.hidePI()
        let errNum = ErrorCode(rawValue: status)!
        var message = ""
        if let msg = dataIn[APIResponceParams.Message] as? String {
            message = msg
        }
        switch errNum {
        case .success,.created,.deleted,.updated:
            Helper.showAlert(message: message, head: StringConstants.Success(), type: 0)
            break
        case .ExpiredToken:
            
            if let temp = dataIn[APIResponceParams.Data] as? String {
                Utility.saveSession(token: temp)
                APICalls.refreshToken()
            }else {
                Utility.logout(type:1,message:StringConstants.SessionInvalid() )
                LaunchVM().callGuestLogin()
            }
            break
        case .InvalidToken,.NoUser,.Banned,.Banned1:
            Utility.logout(type:1,message: message)
            LaunchVM().callGuestLogin()
            break
        case .NotFound:
            break
        default:
            Helper.showAlert(message: message, head: StringConstants.Error(), type: 1)
            break
        }
    }
    
    
    //1 - place , 2 - direction , 3 - distance , 4 - others
    class func keyRotationImpression(type:keyRotationType) {
        let header = Utility.getHeaderWithoutAuth()
        
        let params = ["count":1,
                      "currentKey":GoogleKeys.GoogleMapKey,
                      "type" :type.rawValue ] as [String:Any]
        RxAlamofire.requestJSON(.post, APIEndTails.KeyRotation, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
            let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
            if errNum == .success {
                print("Success")
            }
            
        }, onError: { (Error) in
            
        }).disposed(by: self.disposesBag)
    }
    
    
    
    enum keyRotationType:Int {
        case place  = 1
        case direction = 2
        case distance  = 3
        case others = 4
    }
    
}
