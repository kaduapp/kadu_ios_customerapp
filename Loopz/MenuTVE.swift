//
//  MenuTVE.swift
//  UFly
//
//  Created by 3 Embed on 20/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - UITableViewDataSource
extension MenuVC: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controllerSubCategoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.MenuTableCell, for: indexPath) as! MenuTableCell
        
        cell.updateCell(data: controllerSubCategoryArray[indexPath.row], id: selectedId)
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension MenuVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelectSubCat(data:controllerSubCategoryArray[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
