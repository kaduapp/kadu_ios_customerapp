//
//  LaunchLocationE.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

//MARK:- LocationManager Delegate
extension LaunchVC : LocationManagerDelegate {
    
    func didFailToUpdateLocation() {
        
    }
    //static let disposeBag = DisposeBag()
    func didUpdateLocation(location: Location, search:Bool, update:Bool) {
        if Utility.getHeader()["authorization"]!.count > 0{
                
                            if isManualLocation {
                                isManualLocation = false
                                self.setManualAction(self.setManualButton)
                            }
                            else if isDetectLocation {
                                isDetectLocation = false
                            launchVM.getOperationZones(location: location)
                            }
                        }
    }
    
    func didChangeAuthorization(authorized: Bool) {
        
    }
    
    func didUpdateSearch(locations: [Location]) {
        
    }
}


