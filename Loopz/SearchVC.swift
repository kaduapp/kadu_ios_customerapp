//
//  SearchVC.swift
//  UFly
//
//  Created by 3 Embed on 28/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {
    //outlets
    var lang = ""

    @IBOutlet weak var dummyView: UIView!
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var NavigationWidth: NSLayoutConstraint!
    @IBOutlet weak var closeIconWidth: NSLayoutConstraint!
    @IBOutlet weak var searchTF: UITextField!
    
    @IBOutlet weak var searchButton: UIButton!
    var noProductsView = EmptyView().shared
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var cancelBtnTrailingConstaraint: NSLayoutConstraint!
    @IBOutlet weak var cancelBtnWidth: NSLayoutConstraint!
    
    @IBOutlet weak var productNameTable: UITableView!
    @IBOutlet weak var searchHeaderView: UIView!
    
    //variable
//    let transition = PopAnimator()
    var selected:UICollectionViewCell? = nil
    var searchVM = SearchVM()
    var productName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
        self.setNavigation()
            self.searchTF.Semantic()
            self.searchTF.placeholder = StringConstants.Search()
        self.didGet()
        self.title = "  "
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            
            self.setupUI()
           // self.showCloseIcon()
            self.productNameTable.reloadData()
            self.setBanner()
            self.searchVM.getPopular()
        }
        
        //searchVM.getProduct(text: searchTF.text!, data: 2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let count = self.searchTF.text?.count, count == 0{
        self.searchTF.becomeFirstResponder()
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    
    @IBAction func dispensariesBtnTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.3){
            self.scrollView.contentOffset.x = self.scrollView.frame.width*CGFloat(1)
//            self.updatePage(scrollView1: self.scrollView)
        }
    }
    
    @IBAction func productsBtnTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.3){
            self.scrollView.contentOffset.x = self.scrollView.frame.width*CGFloat(0)
//            self.updatePage(scrollView1: self.scrollView)
        }
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        self.hideCancelBtn()
        searchTF.text = ""
        searchTF.resignFirstResponder()
        searchVM.arrayOfItems.removeAll()
        productNameTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.SearchToItemDetails {
            if let itemDetailVC:ItemDetailVC = segue.destination as? ItemDetailVC {
               itemDetailVC.itemDetailVM.item = sender as! Item                          //searchVM.arrayOfItems[button.tag]
//                itemDetailVC.transitioningDelegate = self
            }
        }else if segue.identifier == UIConstants.SegueIds.SearchToStore {
            if let viewController: HomeVC = segue.destination as? HomeVC{
                viewController.homeVM.store = sender as! Store
            }
        }else if segue.identifier == String(describing: SearchDetailsVC.self) {
            if let viewController: SearchDetailsVC = segue.destination as? SearchDetailsVC{
                viewController.dataString = sender as! String
                viewController.searchVM.productName = sender as! String
                productName = ""
                viewController.searchVM.isFromItemList = self.searchVM.isFromItemList
                viewController.languageIn = lang

            }
        }
    }
    
    @IBAction func backACT(_ sender: UIBarButtonItem){
        Utility.saveStore(location: [:])
        self.tabBarController?.dismiss(animated: true, completion: nil)
    }
    
}

extension SearchVC {
    
    /// Observe for view model
    func didGet(){
        searchVM.searchVM_response.subscribe(onNext: { [weak self]success in
            switch success {
            case .ItemName:
                 self?.searchVM.loadPopular = true
                self?.productNameTable.reloadData()
                break
            case .Recent:
                self?.searchVM.loadPopular = true
                self?.productNameTable.reloadData()
                break
            default:
                break
            }
            self?.setBanner()
        }).disposed(by: self.searchVM.disposeBag)
        
    }
}

