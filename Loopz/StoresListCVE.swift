//
//  RestuarantCVE.swift
//  DelivX
//
//  Created by 3EMBED on 26/11/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

extension StoresListVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.restaurantVM.arrayOfOffer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RestHeaderImageCollectionViewCell.self), for: indexPath) as! RestHeaderImageCollectionViewCell
        Helper.setImage(imageView: cell.offerImageView, url: self.restaurantVM.arrayOfOffer[indexPath.row].ImageURL, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width/2 +  self.view.frame.width/4 , height: 150)
////        var cellWidth = collectionView.frame.size.width - 40
////        var cellHight = cellWidth*9/16
//        return CGSize(width: collectionView.frame.size.width - 40, height: (collectionView.frame.size.width - 40)*9/16)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: String(describing: ViewAllVC.self), sender: indexPath.row)
    }
    
}
