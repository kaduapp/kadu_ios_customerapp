//
//  UploadImageModel.swift
//  DayRunner
//
//  Created by Vasant Hugar on 27/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

struct UploadImage {
    var image = UIImage()
    var path = ""
    
    init(image: UIImage, path: String) {
        self.image = image
        self.path = path
    }
}
enum Page:Int{
    case UploadIdentityVC = 1
    case IdentityCardVC = 2
    case EditVC = 3
    case WishList = 4
    case Default = 0
}

class UploadImageModel: NSObject {
    
    private static var manager: UploadImageModel? = nil
    let UploadImageRespose = PublishSubject<Page>()
    
    
    static var shared: UploadImageModel {
        if manager == nil {
            manager = UploadImageModel()
        }
        return manager!
    }
    
    override init() {
        super.init()
    }
    
    var uploadImages: [UploadImage] = []
    private var amazon = AmazonWrapper.sharedInstance()
    var imageIndex = 0
    var fromVC:Page = .Default
    
    /// Start Uploading Image
    func start(page: Page) {
        Helper.showPI(string: StringConstants.Updating())
        if imageIndex < uploadImages.count {
            uploadImage(uploadImg: uploadImages[imageIndex] , page:page)
            imageIndex += 1
        }
        else {
            print("\n*****************************\nUploading Completed\n*****************************\n")
            UploadImageRespose.onNext(page)
            Helper.hidePI()
        }
    }
    
    /// Uplad Images in Background
    ///
    /// - Parameter uploadImg: Upload Image Object
    private func uploadImage(uploadImg: UploadImage,page: Page) {
        
        amazon.upload(withImage: uploadImg.image,
                      imgPath: uploadImg.path,
                      completion: {(success, url) in
                        
                        print("\n*****************************\nUploaded Image: \(url)\n*****************************\n")
                        self.start(page: page)
        })
    }
}
