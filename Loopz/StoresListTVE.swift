//
//  RestaurantsTVE.swift
//  DelivX
//
//  Created by 3Embed on 28/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

extension StoresListVC:UITableViewDataSource,UITableViewDelegate{
    
   func numberOfSections(in tableView: UITableView) -> Int {
      return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if section == 0{
        return restaurantVM.arrayOfFavorites.count
        }
        else{
        return restaurantVM.arrayOfResturants.count
      }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = mainTableView.dequeueReusableCell(withIdentifier: String(describing: StoresListTVCell.self)) as! StoresListTVCell
        
        if indexPath.section == 0{
            cell.configureCell(data: restaurantVM.arrayOfFavorites[indexPath.row])
            
        }
        else{
             cell.configureCell(data: restaurantVM.arrayOfResturants[indexPath.row])
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        var storeIsOpen = false
         if (indexPath.section == 0)
        {
       restaurantVM.selectedStoreForDetail = self.restaurantVM.arrayOfFavorites[indexPath.row]
        storeIsOpen = self.restaurantVM.arrayOfFavorites[indexPath.row].StoreIsOpen
        }
        else if(indexPath.section == 1)
        {
             restaurantVM.selectedStoreForDetail = self.restaurantVM.arrayOfResturants[indexPath.row]
            storeIsOpen = self.restaurantVM.arrayOfResturants[indexPath.row].StoreIsOpen
        }
        
        if Changebles.isGrocerOnly && storeIsOpen {
            Utility.saveStore(location: restaurantVM.selectedStoreForDetail.dataObject)
        Utility.UtilityUpdate.onNext(Utility.TypeServe.ChangeStore)
            self.navigationController?.popViewController(animated: true)
        }
        else if storeIsOpen {
            Utility.saveStore(location: restaurantVM.selectedStoreForDetail.dataObject)
            if Utility.getSelectedSuperStores().type == .Restaurant{
                self.performSegue(withIdentifier: String(describing: RestHomeVC.self), sender: indexPath.row)
            }
            else{
                self.performSegue(withIdentifier: String(describing: HomeVC.self), sender: indexPath.row)
            }
        }
    
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     let titleCell = mainTableView.dequeueReusableCell(withIdentifier: String(describing: TitleHeaderTVCell.self)) as! TitleHeaderTVCell
        
        if section == 0{
        titleCell.setHeaderTitle(title:"Favorites",showViewAll:true)
            
        }
        else{
          titleCell.setHeaderTitle(title:"Others",showViewAll:false)
        }
        
        titleCell.backgroundColor = UIColor.white
        return titleCell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        if restaurantVM.arrayOfFavorites.count == 0{
               return 0
            }
        return 60
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        var storeIsOpen = false
        
        if indexPath.section == 0 {
            
         storeIsOpen = self.restaurantVM.arrayOfFavorites[indexPath.row].StoreIsOpen
            
        }
        else{
            
           storeIsOpen = self.restaurantVM.arrayOfResturants[indexPath.row].StoreIsOpen
      }
        
        if storeIsOpen{
            return true
        }else{
            return false
        }
     
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section != 0 {
        if indexPath.row == restaurantVM.arrayOfResturants.count - 1 && restaurantVM.arrayOfResturants.count > (restaurantVM.limit * restaurantVM.offset) + (restaurantVM.limit - 1) {
                restaurantVM.offset = restaurantVM.offset + 1
                restaurantVM.getData()
        }
      }
    }
    
    
    
    func setTheAlphaToNavigationViewsOnScroll(_ offset:CGFloat){
        self.navTitleLabel.alpha = offset
        self.searchBarButton.alpha = offset
        self.filterBarButton.alpha = offset
        self.titleLBHeaderView.alpha = 1 - offset
        self.searchBTNHeaderView.alpha = 1 - offset
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        var offset = scrollView.contentOffset.y / 35
        if self.navigationController != nil {
            if scrollView.contentOffset.y > 1{
                Helper.addShadowToNavigationBar(controller: self.navigationController!)
            }
            else{
                Helper.removeShadowToNavigationBar(controller: self.navigationController!)
            }
        }
        if offset > 30{
            offset = 1
            setTheAlphaToNavigationViewsOnScroll(offset)
        }
        else{
            setTheAlphaToNavigationViewsOnScroll(offset)
        }
    }
    

    
}
