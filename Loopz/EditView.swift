//
//  EditView.swift
//  DelivX
//
//  Created by 3 Embed on 06/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class EditView: UIView {
    
    //    @IBOutlet weak var scrollView:UIScrollView!
    //    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var phoneNumView: UIView!
    @IBOutlet weak var phoneNumTF: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var updateBtnView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var phNumTickMark: UIButton!
    @IBOutlet weak var emailTickMark: UIButton!
    
    @IBOutlet weak var loginTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBOutlet weak var phSeperartor: UIView!
    @IBOutlet weak var emailSeparator: UIView!
    @IBOutlet weak var countryCodeBtn: UIButton!
    
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var uploadImageBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    var phoneNum:[String] = []
    var pickedProfileImage:UIImage? = nil
    var phoneNumber:[String] = []
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewSetup()
        phoneNumTF.Semantic()
        emailTF.Semantic()
    }
    
    /// initial setup
    func viewSetup(){
        //Upload new image.
        uploadImageBtn.setTitle(StringConstants.uploadImage(), for: .normal)
        Helper.setShadow(sender: profileImage)
        Helper.setUiElementBorderWithCorner(element: plusBtn, radius: Helper.setRadius(element: plusBtn), borderWidth: 0, color:  Colors.SeparatorLight)
        plusBtn.backgroundColor =  Colors.AppBaseColor
        
        Fonts.setPrimaryMedium(plusBtn)
        Fonts.setPrimaryMedium(loginTitle)
        Fonts.setPrimaryMedium(uploadImageBtn)
        Fonts.setPrimaryRegular(subTitle)
        
        Fonts.setPrimaryMedium(emailTF)
        Fonts.setPrimaryMedium(phoneNumTF)
        Fonts.setPrimaryMedium(updateBtn)
        
        phNumTickMark.tintColor =  Colors.AppBaseColor
        emailTickMark.tintColor =  Colors.AppBaseColor
        
        uploadImageBtn.tintColor =  Colors.AppBaseColor
        uploadImageBtn.setTitleColor( Colors.AppBaseColor, for: .normal)
        
        loginTitle.text = StringConstants.Update()
        subTitle.text = StringConstants.UpdateEmailPh()
        
        Helper.setButton(button: updateBtn,view:updateBtnView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.Update(), highlighted: StringConstants.Update(), selected: StringConstants.Update(), button: updateBtn)
        Helper.setUiElementBorderWithCorner(element: profileImage, radius: Helper.setRadius(element:profileImage), borderWidth: 0.5, color:  Colors.SeparatorLight)
        
        phSeperartor.backgroundColor =  Colors.SeparatorLight
        emailSeparator.backgroundColor =  Colors.SeparatorLight
        
        emailTF.textColor =  Colors.PrimaryText
        phoneNumTF.textColor =  Colors.PrimaryText
        loginTitle.textColor =  Colors.PrimaryText
        subTitle.textColor   =  Colors.SeconderyText
        phoneNumTF.placeholder = StringConstants.PhoneNumber()
        emailTF.placeholder = StringConstants.EmailAddress()
        emailTickMark.isHidden = true
        emailTickMark.isHidden = true
    }
    
    func updateView(data: User){
        
        emailTF.text = data.FirstName
        phoneNumTF.text =  "\(data.CountryCode)\(data.Phone)"
        emailTF.text = data.Email
        Helper.setImage(imageView: profileImage, url: (data.userProfilePic), defaultImage: #imageLiteral(resourceName: "UserDefault") )
    
        if Helper.isPhoneNumValid(phoneNumber: data.Phone, code: data.CountryCode){
            phNumTickMark.isHidden = false
        }else{
          phNumTickMark.isHidden = true
        }
        
        if Helper.isValidEmail(text: data.Email) {
            emailTickMark.isHidden = false
        }
        else{
          emailTickMark.isHidden = true
        }
     
    }
    
}
