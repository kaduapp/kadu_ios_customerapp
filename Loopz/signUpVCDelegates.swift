//
//  signUpVCDelegates.swift
//  UFly
//
//  Created by 3 Embed on 09/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - UIScrollViewDelegate
extension SignUpVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
        let movedOffset: CGFloat = sender.contentOffset.y
        if sender == signUpView.signUpScrollView {
            DispatchQueue.main.async {
            let pageWidth: CGFloat = 171 - 64            //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            if ratio >= 1 {
                self.navigationBackView.isHidden = false
                self.navigationBackView.backgroundColor = UIColor.white
                self.titleText.textColor = UIColor.black
            }else {
                self.navigationBackView.isHidden = true
            }
         }
        }
    }
}


// MARK: - VNHCountryPickerDelegate
extension SignUpVC: VNHCountryPickerDelegate {
    func didPick(country: VNHCounty) {
        signUpView.mobileNumberTF.text = country.dialCode
        signupVM.authentication.CountryCode = country.dialCode
    }
    
}
