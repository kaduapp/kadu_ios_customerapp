//
//  DatePickerVC.swift
//  DelivX
//
//  Created by 3Embed on 29/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DatePickerVC: UIViewController {

    var ModeDate = true
    var TitleString = ""
    var Response_Picker = PublishSubject<Date>()
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var selectedDate = Date()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = Colors.PrimaryText.withAlphaComponent(0.2)
            self.heightConstraint.constant = 260
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.view.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.3) {
            self.heightConstraint.constant = 0
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func doneAction(_ sender: Any) {
        Response_Picker.onNext(self.datePicker.date)
        self.dismiss(animated: true, completion: nil)
    }
    
}
