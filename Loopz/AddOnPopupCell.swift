//
//  AddOnPopupCell.swift
//  DelivX
//
//  Created by 3Embed on 01/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AddOnPopupCell: UITableViewCell {
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var selectImage: UIButton!
    @IBOutlet weak var selectLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        separatorView.backgroundColor = Colors.SeparatorLight
        selectLabel.textColor = Colors.DarkBlueBlack
        Fonts.setPrimaryRegular(selectLabel, size: 12)
        priceLabel.textColor = Colors.DarkBlueBlack
        Fonts.setPrimaryRegular(priceLabel, size: 12)
        
        
    }
    
    func setData(data:AddOnGroup,indexPath:IndexPath){
        let dataAddOn = data.AddOns[indexPath.row]
        //selectLabel.text = dataAddOn.Name
        var addOnNameAndPrice = dataAddOn.Name
        var price:String = ""
        if dataAddOn.Price > 0 && dataAddOn.Name.count > 0{
             price = Helper.df2so(Double( dataAddOn.Price))
            addOnNameAndPrice = addOnNameAndPrice + " - " + price
        }
        //priceLabel.text = Helper.setCurrency(displayAmount: dataAddOn.Price, price: dataAddOn.Price).string
        Helper.updateText(text: addOnNameAndPrice, subText: price, selectLabel, color: Colors.lightBlueBlack, link: "")
        
        if data.Mandatory && !data.multiple {
            selectImage.setImage(#imageLiteral(resourceName: "CheckOn"), for: .selected)
            selectImage.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
        }else{
            selectImage.setImage(#imageLiteral(resourceName: "CheckBoxOn"), for: .selected)
            selectImage.setImage(#imageLiteral(resourceName: "CheckBoxOff"), for: .normal)
        }
        
        if dataAddOn.Selected {
            selectImage.isSelected = true
        }else{
            selectImage.isSelected = false
        }
//
//            if data.addOnGroupd[indexPath.row].flag{
//                selectImage.setImage(UIImage.init(named: "CheckOn"), for: UIControlState.normal)
//            }else{
//                selectImage.setImage(UIImage.init(named: "Check_off"), for: UIControlState.normal)
//            }
//        }
    }
}
