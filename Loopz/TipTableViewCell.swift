//
//  TipTableViewCell.swift
//  DelivX
//
//  Created by 3EMBED on 06/04/20.
//  Copyright © 2020 Nabeel Gulzar. All rights reserved.
//

import UIKit

class TipTableViewCell: UITableViewCell {
    @IBOutlet var tipTextField: UITextField!
    
    @IBOutlet var tipcollectionView: UICollectionView!
    @IBOutlet var tipLabel: UILabel!
    var checkoutVM = CheckoutVM()
    var selectedIndex:Int = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let paddingView :UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 46))
        let lbl = UILabel.init(frame: paddingView.frame)
        lbl.text = Utility.getCurrency().1
        paddingView.addSubview(lbl)
        tipTextField.leftViewMode = .always
        tipTextField.leftView = paddingView
        tipTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setButtonselected(amount:String)
    {
//        let text = tipLabel.text!.replacingOccurrences(of: "  $", with: "", options: [])
//               let num = (text as NSString).integerValue
//               let oldbutton = self.viewWithTag(num - 1) as? UIButton
//                 oldbutton?.backgroundColor = .red
//        oldbutton?.titleLabel?.textColor = .white
    }
    @IBAction func tappedOnTipButton(_ sender: Any) {
        let text = tipLabel.text!.replacingOccurrences(of: "  $", with: "", options: [])
        let num = (text as NSString).integerValue
        let oldbutton = self.viewWithTag(num - 1) as? UIButton
        oldbutton?.backgroundColor = .white
        oldbutton?.titleLabel?.textColor = .black
        let button = sender as? UIButton
        button?.backgroundColor = .red
        switch (sender as AnyObject).tag {
        case 1:
            AppConstants.DriverTip = 2.00
            tipLabel.text = "  $2.00"
            button?.titleLabel?.textColor = .white
            break
        case 2:
            AppConstants.DriverTip = 3.00
            tipLabel.text = "  $3.00"
            button?.titleLabel?.textColor = .white
            break
        default:
            AppConstants.DriverTip = 4.00
            tipLabel.text = "  $4.00"
            button?.titleLabel?.textColor = .white
            
        }

        checkoutVM.checkoutVM_response.onNext(CheckoutVM.ResponseType(rawValue: 6)!)


    }
    @objc func textFieldDidChange(_ textField : UITextField) {
        AppConstants.DriverTip =  Float(textField.text?.doubleValue ?? 0.0)
          self.selectedIndex = -1
        checkoutVM.checkoutVM_response.onNext(CheckoutVM.ResponseType(rawValue: 6)!)
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 ) {
            self.tipcollectionView.reloadData()
        }

    }
    
}
extension TipTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return checkoutVM.tipArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        print( cell.subviews)
        let sbview = cell.subviews[0] as? UIView
               
                let lbl = sbview?.subviews[0] as? UILabel
        lbl?.text = Utility.getCurrency().1 + "\(Float(checkoutVM.tipArray[indexPath.row].tipValue))"
        lbl?.adjustsFontSizeToFitWidth = true
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.0 ) {
                if self.selectedIndex == indexPath.row
        {
            lbl?.textColor = .white
              lbl?.backgroundColor = .red
        }else
        {       lbl?.textColor = .black
              lbl?.backgroundColor = .white
        }
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        
        //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        let cell = collectionView.cellForItem(at: indexPath)
        let sbview = cell?.subviews[0]
        
        print(sbview?.subviews)
        let lbl = sbview?.subviews[0] as? UILabel
        lbl?.backgroundColor = .red
        lbl?.textColor = .white
       self.selectedIndex = indexPath.row
        
        AppConstants.DriverTip =  Float(checkoutVM.tipArray[indexPath.row].tipValue)
        tipTextField.text = "\(Float(checkoutVM.tipArray[indexPath.row].tipValue))"

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 ) {
            collectionView.reloadData()
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
        
        checkoutVM.checkoutVM_response.onNext(CheckoutVM.ResponseType(rawValue: 6)!)

                       }

 }


