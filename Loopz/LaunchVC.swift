 //
//  LaunchVC.swift
//  UFly
//
//  Created by 3Embed on 26/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class LaunchVC: UIViewController {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var setManualButton: UIButton_RotateButtonClass!
    @IBOutlet weak var detectLocationButton: UIButton_RotateButtonClass!
    @IBOutlet weak var buttonsHight: NSLayoutConstraint!
    
    let launchVM = LaunchVM()
    var layer: AVPlayerLayer = AVPlayerLayer.init()
    var isDetectLocation = false
    var isManualLocation = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isGuestLogin()
        validateUI()
        setView()
        Helper.getFonts()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        launchVM.goneHome = false
        checkForLogin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        layer.removeFromSuperlayer()
    }
    
    // MARK: - Button Actions
    @IBAction func detectLocationAction(_ sender: UIButton) {
        isDetectLocation = true
          if Utility.getGuestLogin() == true {
          //  Helper.showPI(string: StringConstants.ValidateLocation())
             launchVM.locationManager?.delegate = self
            launchVM.locationManager?.start()
        }else if LocationManager.shared.longitude != 0 && LocationManager.shared.latitute != 0{
            launchVM.callGuestLogin()
        }else{
            launchVM.callGuestLogin()
        }
        
    }
    
    @IBAction func setManualAction(_ sender: UIButton) {
  isManualLocation = true
        if Utility.getGuestLogin() == true {
            Helper.hidePI()
            self.performSegue(withIdentifier: UIConstants.SegueIds.SplashToLocation, sender: self)
        }else if LocationManager.shared.longitude != 0 && LocationManager.shared.latitute != 0{
            launchVM.callGuestLogin()
        }  else   {
            launchVM.callGuestLogin()
        }
    
    }
    
    /// Validating whether logged in or not
    func checkForLogin(){
        let installed = Utility.getGuestLogin()
        if installed == true {
            let when = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: when){
                self.moveToHome()
            }
        } else if LocationManager.shared.longitude != 0 && LocationManager.shared.latitute != 0 {
            
            launchVM.callGuestLogin()
        }
    }
}

extension LaunchVC {
    /// observing for view model
    func isGuestLogin(){
        launchVM.launchVMResponse.subscribe(onNext: { [weak self]success in
            if success {
                if Utility.getAddress().FullText.count > 0 {
                    self?.moveToHome()
                }
                else{
                    self?.launchVM.locationManager?.start()
                }
            }
            }, onError: {error in
                print(error)
        }).disposed(by: self.launchVM.disposeBag)
    }
    
    /// this method makes sure adress is selected or not and having check for guestlogin
    //goes from splash to Home screen
    func moveToHome() {
        setView()
        if Utility.getAddress().FullText.length > 0 && Utility.getGuestLogin() == true && launchVM.goneHome == false{
            launchVM.goneHome = true
            Helper.hidePI()
            
            
            if Changebles.isGrocerOnly
            {
                let installed = Utility.getGuestLogin()
                if installed == true {
                    Helper.setGrocerOnlyRootViewController()
                }else{
                    launchVM.callGuestLogin()
                }
            }
            else{
                 DispatchQueue.main.async{
                                      let mainStoryBoard = UIStoryboard(name: "SuperStore", bundle: nil)
                                      let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SuperStoreNav") as! UINavigationController
                                      let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                      appDelegate.window?.rootViewController = redViewController
                                  }
            }
        }
    }
}
