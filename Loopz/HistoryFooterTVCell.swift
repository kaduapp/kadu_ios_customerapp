//
//  HistoryFooterTVCell.swift
//  History
//
//  Created by 3Embed on 05/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class HistoryFooterTVCell: UITableViewCell
{
    @IBOutlet weak var backgroundViewForView1: UIView!
    @IBOutlet weak var backgroundViewForView2: UIView!
    @IBOutlet weak var backgroundViewForView3: UIView!
    @IBOutlet weak var backgroundViewForView4: UIView!
    
    @IBOutlet weak var actualChargeLB: UILabel!
    @IBOutlet weak var actualPriceLB: UILabel!
    @IBOutlet weak var deliveryPriceLB: UILabel!
    @IBOutlet weak var deliveryLB: UILabel!
    @IBOutlet weak var discountLB: UILabel!
    @IBOutlet weak var discountPriceLB: UILabel!
    @IBOutlet weak var toPayPriceLB: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var toPayLB: UILabel!
    
    @IBOutlet weak var heightForView1: NSLayoutConstraint! //25
    @IBOutlet weak var heightForView2: NSLayoutConstraint! //15
    @IBOutlet weak var heightForView3: NSLayoutConstraint! //25
    
    
    func configureCell(data:order){
        if let deliveryPrice = data.deliveryCharge{
            self.hideDeliveryView(false)
            self.deliveryPriceLB.text = deliveryPrice
        }
        else{
            self.hideDeliveryView(true)
        }
        
        if let discount = data.discount{
            self.hideDiscountView(false)
            self.discountPriceLB.text = discount
        }
        else{
            self.hideDiscountView(true)
        }
    
        if let actualPrice = data.actualcharge{
            self.actualPriceLB.text = actualPrice
        }
        if let toPay = data.toPay{
            self.toPayPriceLB.text = toPay
        }
    }
    
    func hideDeliveryView(_ boolean:Bool){
        self.deliveryPriceLB.isHidden = boolean
        self.deliveryLB.isHidden = boolean
        if boolean{
            self.heightForView2.constant = 0
        }
        else{
            self.heightForView2.constant = 15
        }
        self.updateConstraints()
        self.layoutIfNeeded()
    }
    
    func hideDiscountView(_ boolean:Bool){
        self.discountLB.isHidden = boolean
        self.discountPriceLB.isHidden = boolean
        
        if boolean{
            self.heightForView3.constant = 0
        }
        else{
            self.heightForView3.constant = 25

        }
        self.updateConstraints()
        self.layoutIfNeeded()
    }

}
