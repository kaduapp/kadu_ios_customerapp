//
//  CartAPICalls.swift
//  UFly
//
//  Created by 3Embed on 21/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class CartAPICalls: NSObject {
    static let disposeBag = DisposeBag()
    
    //Get Cart Data
    class func getCartData() ->Observable<([Cart],[Tax])> {
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        let url  = APIEndTails.Cart
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var Items = [Cart]()
                    var arrayOfTax:[Tax] = []
                    if Utility.getLogin() {
                        let tempData = bodyIn[APIResponceParams.Data] as? [String:Any]
                        if let data = tempData![APIResponceParams.TotalPrice] as? Double {
                            AppConstants.TotalCartPrice = Float(data)
                        }
                        if let data = tempData![APIResponceParams.TotalPrice] as? Float {
                            AppConstants.TotalCartPrice = data
                        }
                        
                        if let data = tempData![APIResponceParams.TotalPriceWithTax] as? Double {
                            AppConstants.TotalCartPriceWithTax = Float(data)
                        }
                        
                        if let data = tempData![APIResponceParams.cartDiscount] as? Double {
                            AppConstants.CartDiscount = Float(data)
                        }
                        
                        if let data = tempData![APIResponceParams.cartDiscount] as? Float {
                            AppConstants.CartDiscount = Float(data)
                        }
                        
                        if let tempData = tempData![APIResponceParams.CartId] as? String {
                            //AppConstants.CartId = tempData
                            Utility.saveCartId(data: tempData)
                        }
                        if let tempData = tempData!["laundryCartExists"] as? Int
                        {
                            AppConstants.laundrycartexists = NSString(string:"\(tempData)").boolValue
                        }
                        if let titleTemp = tempData![APIResponceParams.Cart] as? [Any]{
                            for data in titleTemp {
                                let cat = Cart.init(data: data as! [String : Any])
                                Items.append(cat)
                            }
                        }
                        if let titleTemp = tempData![APIResponceParams.ExclusiveTaxes] as? [Any]{
                            for data in titleTemp {
                                let cat = Tax.init(data: data as! [String : Any])
                                arrayOfTax.append(cat)
                            }
                        }
                        CartDBManager().insertCartDocument(data: Items)
                    }
                    observer.onNext((Items,arrayOfTax))
                    observer.onCompleted()
                }
                else if(errNum == .NotFound){
                    AppConstants.laundrycartexists = false
                    CartDBManager().insertCartDocument(data: [])
                    observer.onNext(([],[]))
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //Update Cart Data
    class func updateCartData(item:CartItem) ->Observable<Bool> {
        
        if !AppConstants.Reachable || !Helper.isLoggedIn(isFromHist: false) {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        
        if Utility.getCartId().length > 0 {
             Helper.showPI(string: "")
            let header = Utility.getHeader()
            var flag = false
            AppConstants.Cart = CartDBManager().getCartDocument()
            if CartDBManager().getCartFullCount(data: Item.init(data: item)).count > 0 {
                if item.QTY > (CartDBManager().getCartFullCount(data: Item.init(data: item)).last?.QTY)! {
                    flag = true
                }
            }
            let params = [
                APIRequestParams.QTY    :   item.QTY,
                APIRequestParams.CartId :   Utility.getCartId(),
                APIRequestParams.ChildId:   item.ChildProductId,
                APIResponceParams.UnitId:   item.UnitId,
                APIResponceParams.PackId:   item.PackId,
                APIResponceParams.StoreType: Utility.getSelectedSuperStores().type.rawValue,
                APIResponceParams.Increase: NSNumber.init(booleanLiteral: flag)
                ] as [String : Any]
            let url  = APIEndTails.CartNew
            CartDBManager().updateCartItem(data: item)
            return Observable.create { observer in
                RxAlamofire.requestJSON(.patch, url, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                    Helper.hidePI()
                    let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                    if errNum == .updated {
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                    else{
                        APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                }, onError: { (Error) in
                    //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                    Helper.hidePI()
                }).disposed(by: self.disposeBag)
                return Disposables.create()
                }.share(replay: 1)
        }else{
            return Observable.create { observer in
                observer.onNext(false)
                observer.onCompleted()
                return Disposables.create()
                }.share(replay: 1)
        }
    }
    
    
    //Delete Cart Data
    class func deleteCartData(item:CartItem) ->Observable<Bool> {
        Helper.showPI(string: StringConstants.UpdateCart())
        if !AppConstants.Reachable || !Helper.isLoggedIn(isFromHist: false) {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        var pack = item.PackId
        if pack.length == 0 {
            pack = "0"
        }
        
        let url  = APIEndTails.Cart + "/" + Utility.getCartId() + "/" + item.ChildProductId + "/" + item.UnitId + "/\(pack)"
        item.QTY = 0
        CartDBManager().updateCartItem(data: item)
        return Observable.create { observer in
            RxAlamofire.requestJSON(.delete, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .deleted {
                    observer.onNext(true)
                    observer.onCompleted()
                }else{
                     Helper.hidePI()
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    //Add Cart Data
    class func addCartData(item:CartItem) ->Observable<Bool> {
        //        Helper.showPI(string: StringConstants.UpdateCart)
        if !AppConstants.Reachable || !Helper.isLoggedIn(isFromHist: false) {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        var addOns:[Any] = []
        for inItem in item.AddonGroups {
            var stringArray:[String] = []
            if inItem.AddOns.count > 0 {
                for inForm in inItem.AddOns {
                    if inForm.Selected {
                        stringArray.append(inForm.Id)
                    }
                }
            }
            if stringArray.count > 0 {
                let data = [
                    APIResponceParams.AddOnsGrouped :   stringArray,
                    APIResponceParams.AddOnsId      :   inItem.Id
                    ] as [String : Any]
                addOns.append(data)
            }
        }
        
        let params = [
            APIRequestParams.QTY        :   item.QTY,
            APIRequestParams.ChildId    :   item.ChildProductId,
            APIResponceParams.UnitId    :   item.UnitId,
            APIResponceParams.StoreType :   Utility.getSelectedSuperStores().type.rawValue,
            APIResponceParams.AddOns    :   addOns
            ] as [String : Any]
        let header = Utility.getHeader()
        let url  = APIEndTails.Cart
        if CartDBManager().getCartCount(data: Item.init(data: item)).QTY == 0 {
            CartDBManager().updateCartItem(data: item)
        }else{
            let data = CartDBManager().getCartCount(data: Item.init(data: item))
            data.QTY = data.QTY + 1
            CartDBManager().updateCartItem(data: data)
        }
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, url, parameters:
                params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                    Helper.hidePI()
                    let bodyIn = body as! [String:Any]
                    if let data = bodyIn[APIResponceParams.Data] as? [String:Any] {
                        Utility.saveCartId(data: data[APIResponceParams.CartId] as! String)
                        if let timePass = data[APIResponceParams.PackId] as? Double{
                            item.PackId = String(describing: timePass)
                            let data = CartDBManager().getCartCount(data: Item.init(data: item))
                            CartDBManager().updateCartItem(data: data)
                        }

                    }
                    let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                    if errNum == .created {
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                    else{
                        APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                }, onError: { (Error) in
                    //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                    Helper.hidePI()
                }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    
    //Add Cart Data
    class func clearCartData() ->Observable<Bool> {
        //        Helper.showPI(string: StringConstants.UpdateCart)
        if !AppConstants.Reachable || !Helper.isLoggedIn(isFromHist: false) {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        
        let params = [
            APIRequestParams.customerId :  Utility.getProfileData().SId,
            APIRequestParams.CartId   :  Utility.getCartId(),
            ] as [String : Any]
        
        let header = Utility.getHeader()
        let url  = APIEndTails.ClearCart
        CartDBManager().deleteCartDocument()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, url, parameters:
                params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                    Helper.hidePI()
                    let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                    if errNum == .success {
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                    else{
                      APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                }, onError: { (Error) in
                    //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                    Helper.hidePI()
                }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    
    //Customise Cart Data
    class func updateAddonData(item:CartItem) ->Observable<Bool> {
        if !AppConstants.Reachable || !Helper.isLoggedIn(isFromHist: false) {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        if Utility.getCartId().length > 0 {
            let header = Utility.getHeader()
            var addOns:[Any] = []
            for inItem in item.AddonGroups {
                var stringArray:[String] = []
                if inItem.AddOns.count > 0 {
                    for inForm in inItem.AddOns {
                        if inForm.Selected {
                            stringArray.append(inForm.Id)
                        }
                    }
                }
                if stringArray.count > 0 {
                    let data = [
                        APIResponceParams.AddOnsGrouped :   stringArray,
                        APIResponceParams.AddOnsId      :   inItem.Id
                        ] as [String : Any]
                    addOns.append(data)
                }
            }
            let params = [
                APIResponceParams.AddOns        :   addOns,
                APIRequestParams.CartId         :   Utility.getCartId(),
                APIRequestParams.ChildId        :   item.ChildProductId,
                APIResponceParams.UnitId        :   item.UnitId,
                APIResponceParams.PackId        :   item.PackId,
                APIResponceParams.StoreType     : Utility.getSelectedSuperStores().type.rawValue
                ] as [String : Any]
            let url  = APIEndTails.Addons
            return Observable.create { observer in
                RxAlamofire.requestJSON(.patch, url, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                    Helper.hidePI()
                    let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                    if errNum == .updated {
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                    else{
                        APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                }, onError: { (Error) in
                    //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                    Helper.hidePI()
                }).disposed(by: self.disposeBag)
                return Disposables.create()
                }.share(replay: 1)
        }else{
            return Observable.create { observer in
                observer.onNext(false)
                observer.onCompleted()
                return Disposables.create()
                }.share(replay: 1)
        }
    }
    
    //Delete Cart Data
    class func deleteSendPackageItem(item:CartItem) ->Observable<Bool> {
    Helper.showPI(string: StringConstants.UpdateCart())
    if !AppConstants.Reachable{
    Helper.showNoInternet()
    return Observable.create{ observer in
    return Disposables.create()
    }.share(replay: 1)
    }
    let header = Utility.getHeader()
    let url = "\(APIEndTails.BaseUrl)dispatcher/cart/\(Utility.getProfileData().SId)/\(Utility.getCartId())/\(item.ChildProductId)/\(item.UnitId)/\(item.addedToCartOn)/2"
    item.QTY = 0
    CartDBManager().updateCartItem(data: item)
    return Observable.create { observer in
    RxAlamofire.requestJSON(.delete, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
    let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
    if errNum == .deleted {
    observer.onNext(true)
    observer.onCompleted()
    }else{
    Helper.hidePI()
    }
    }, onError: { (Error) in
    Helper.hidePI()
    }).disposed(by: self.disposeBag)
    return Disposables.create()
    }.share(replay: 1)
    }
    
}
