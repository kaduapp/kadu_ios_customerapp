//
//  HistoryTVE.swift
//  UFly
//
//  Created by 3 Embed on 04/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension HistoryVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.scrollView {
            updatePage(scrolView1: scrollView)
        }
        if scrollView == self.pastOrderTableView{
            if scrollView.contentOffset.y > 0 {
                Helper.setShadow(sender: navigationView)
            }else{
                Helper.removeShadow(sender: navigationView)
            }
        }
        if scrollView == self.activeOrderTableView{
            if scrollView.contentOffset.y > 0 {
                Helper.setShadow(sender: navigationView)
            }else{
                Helper.removeShadow(sender: navigationView)
            }
        }
        
    }
    
    func updatePage(scrolView1: UIScrollView) {
        var scrollIn = scrollView.contentOffset.x
        if Utility.getLanguage().Code == "ar" {
            scrollIn = scrollView.contentSize.width - scrollView.frame.size.width - scrollView.contentOffset.x
        }
        let scroll = (scrollIn / 2)
        distance.constant = scroll
        //self.view.updateConstraints()
        //self.view.layoutIfNeeded()
        let page:CGFloat = scrollIn / scrollView.frame.size.width
        
        if page == 0 {
            activeOrderButton.isSelected = false
            pastOrderButton.isSelected = false
        }
        else if page == 1 {
            activeOrderButton.isSelected = true
            pastOrderButton.isSelected = true
        }
        historyVM.currentPage = Int(page)
    }
}

//Mark: UITableViewDataSource
extension HistoryVC: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == activeOrderTableView {
            return historyVM.arrayOfActiveOrder.count
        }
        
        return historyVM.arrayOfPastOrder.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var array = [Order]()
        if tableView == activeOrderTableView {
            array = historyVM.arrayOfActiveOrder
        }else{
            array = historyVM.arrayOfPastOrder
        }
        let cell = activeOrderTableView.dequeueReusableCell(withIdentifier:  UIConstants.CellIds.HistoryTableCell, for: indexPath) as! HistoryTableCell
        
        cell.trackOrderBtn.tag = indexPath.section
        cell.helpBtn.tag = indexPath.section
        
        cell.trackOrderBtn.addTarget(self, action: #selector(HistoryVC.trackOrderBtnAction), for: .touchUpInside)
        cell.helpBtn.addTarget(self, action: #selector(HistoryVC.helpBtnAction), for: .touchUpInside)
        
        if tableView == activeOrderTableView {
            cell.updateActiveOrder(data: array[indexPath.section])
        }else {
            cell.updatePastOrder()
        }
        cell.setData(data: array[indexPath.section])
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //
    //    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var array = [Order]()
        if tableView == activeOrderTableView {
            array = historyVM.arrayOfActiveOrder
        }else{
            array = historyVM.arrayOfPastOrder
        }
        if array.count == 0 {
            return UIView()
        }
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormat.dateTime
        
        let resultDate = Helper.getDateString(value: array[section].BookingDate, format: DateFormat.dateTime, zone: true)
        let resultTime = Helper.getDateString(value: array[section].BookingDate, format: DateFormat.TimeFormatToDisplay, zone: true)
        return Helper.showHeader(title: StringConstants.PlacedOn() + " \(resultTime), \(resultDate)", showViewAll: false)
    }
    
    /// this method gets called When UserTap the Track order Button
    ///if current history page 0 goes HistoryToTrackOrder screen
    /// - Parameter sender: is of type UIButton
    @objc func trackOrderBtnAction(sender:UIButton){
        if self.historyVM.currentPage == 0{
            if historyVM.arrayOfActiveOrder.count > sender.tag {
                historyVM.selectedOrder = historyVM.arrayOfActiveOrder[sender.tag]
                // && historyVM.selectedOrder.Status >= 8
                if historyVM.selectedOrder.Delivery {
                    performSegue(withIdentifier: UIConstants.SegueIds.HistoryToTrackOrder, sender: self)
                }else{
                    performSegue(withIdentifier: UIConstants.SegueIds.HistoryToOrderDetail, sender: self)
                }
            }
        }else{
            self.historyVM.selectedOrder = historyVM.arrayOfPastOrder[sender.tag]
            performSegue(withIdentifier: UIConstants.SegueIds.HistoryToOrderDetail, sender: self)
        }
    }
    
    /// this method gets called When UserTap the help Button
    ///if current history page 0 goes HistoryToOrderDetailscreen
    /// - Parameter sender: is of type UIButton
    @objc func helpBtnAction(sender:UIButton){
        if historyVM.currentPage == 1
        {
            self.reOrder(data: historyVM.arrayOfPastOrder[sender.tag])
        }else{
            historyVM.selectedOrder = historyVM.arrayOfActiveOrder[sender.tag]
           
            if  historyVM.selectedOrder.Status >= 8 && historyVM.selectedOrder.Delivery {
                self.performSegue(withIdentifier: UIConstants.SegueIds.HistoryToChat, sender: self)
            }else{
                if Changebles.Zendesc {
                    self.performSegue(withIdentifier: String(describing: ZendeskVC.self), sender: self)
                }
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView == pastOrderTableView {
            self.historyVM.selectedOrder = historyVM.arrayOfPastOrder[indexPath.section]
            isPastOrder = true
        }else{
            self.historyVM.selectedOrder = historyVM.arrayOfActiveOrder[indexPath.section]
            isPastOrder = false
        }
        performSegue(withIdentifier: UIConstants.SegueIds.HistoryToOrderDetail, sender: self)
    }
    
}
