//
//  TrackingVC.swift
//  
//
//  Created by 3 Embed on 06/12/17.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import RxCocoa
import RxSwift

class TrackingVC: UIViewController {
    let disposeBag = DisposeBag()

    @IBOutlet weak var myLocationButton: UIButton!
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var trackingView: TrackingView!
    @IBOutlet weak var navigationWidth: NSLayoutConstraint!
    @IBOutlet weak var orderID: UILabel!
    @IBOutlet weak var orderTiming: UILabel!
    @IBOutlet var gestureRecognizer: UIPanGestureRecognizer!
    @IBOutlet weak var tableHeader: UIView!
    @IBOutlet weak var mapHight: NSLayoutConstraint!
    
    var open = false
    var dataOrder:Order? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialSetup()
        setUI()
        
    }
    
    @IBAction func closeBtnAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func myLocationButtonAction(_ sender: Any) {
        
        let loc = Utility.currentLatAndLong()
        let lat = loc.Latitude
        let lng = loc.Longitude
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat) ,longitude: CLLocationDegrees(lng) , zoom: 15.0)
        self.trackingView.mapView.animate(to: camera)
    }
    
    
    @IBAction func detailsAction(_ sender: Any) {
        
        self.performSegue(withIdentifier: UIConstants.SegueIds.TrackingToDetails, sender: self)
    }
    
    @IBAction func callButtonAction(_ sender: Any) {
        if let phoneNumber = dataOrder?.DriverMobile{
        if let url = URL(string: "telprompt:\(phoneNumber)") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
         }
        }
    }
    
    @IBAction func chatButtonAction(_ sender: Any) {
      
         self.performSegue(withIdentifier: String(describing: "TrackVCToChatVC"), sender: self)
    }
    
    @IBAction func fullScreenBtnAction(_ sender: Any) {
      //  performSegue(withIdentifier: UIConstants.SegueIds.TrackingVCToFullScreenMapVC, sender: self)
    }
    
    @IBAction func panGuestureRec(_ sender: UIPanGestureRecognizer) {
        //        var initialCenter = CGPoint()
        //        let piece = gestureRecognizer.view!
        //        let translation = gestureRecognizer.translation(in: piece.superview)
        //        let tableOffset = trackingView.mainTableView.contentOffset.y
        //        if gestureRecognizer.state == .ended && tableOffset == 0 {
        //
        //            tableHeader.bounds.size.height = 500
        //            mapHight.constant = 500
        //            trackingView.mainTableView.reloadData()
        //        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.TrackingToDetails {
            let nav = segue.destination as! UINavigationController
            if let viewController: OrderDetailVC = nav.viewControllers.first as! OrderDetailVC? {
                viewController.isTrackOrderVC = true
                viewController.orderDetailVM.selectedOrder = dataOrder
            }
        }
        else if segue.identifier == UIConstants.SegueIds.TrackingVCToFullScreenMapVC {
            
            let nav = segue.destination as! UINavigationController
            if let mapVC: FullScreenMapVC = nav.viewControllers.first as! FullScreenMapVC? {
                mapVC.dataOrder = self.dataOrder
            }
        }
       else if segue.identifier == String(describing: ZendeskVC.self) {
            let nav = segue.destination as! UINavigationController
            if let nextScene: ZendeskVC = nav.viewControllers.first as! ZendeskVC?{
                if let bookingId = self.dataOrder?.BookingId{
                nextScene.subJuctGiven = String(bookingId)
                }
                
            }
        }
        else if segue.identifier == String(describing: "TrackVCToChatVC") {
            let nav = segue.destination as! UINavigationController
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                if let bookingIdValue = self.dataOrder?.BookingId{
                    viewController.bookingID = String(describing: bookingIdValue)
                }
                viewController.custName = (self.dataOrder?.DriverName)!
                viewController.customerID = (self.dataOrder?.DriverId)!
                viewController.custImage = (self.dataOrder?.DriverImage)!
            }
        }
    }
}


extension TrackingVC : GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        
    }
    
    func mapViewDidStartTileRendering(_ mapView: GMSMapView) {
        
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        
    }
}

extension TrackingVC{
    /// observe for response of signinVM
    func didconnectMQTTResponse(){
        Helper.pathObserver.subscribe(onNext: { [weak self]data in
            self?.dataOrder?.setETA(data: data.0)
            self?.trackingView.mainTableView.reloadData()
        }).disposed(by: disposeBag)

        MQTTDelegate.message_response.subscribe(onNext: {[weak self]success in
            if self?.dataOrder?.BookingId == success.Id {
                if success.Action == 14 {
                    self?.dataOrder?.DriverLat = success.Latitude
                    self?.dataOrder?.DriverLong = success.Longitude
                    self?.trackingView.trackPath(data: (self?.dataOrder!)!)
                }else{
                    if success.Status == 8 || success.Status == 10  {
                        self?.dataOrder?.DriverId = success.DriverId
                        self?.dataOrder?.DriverName = success.DriverName
                        self?.dataOrder?.DriverImage = success.DriverImage
                        self?.dataOrder?.DriverEmail = success.DriverEmail
                        self?.dataOrder?.DriverMobile = success.DriverMobile
                    }
                    self?.dataOrder?.Status = success.Status
                    self?.dataOrder?.StatusMessage = success.Message
                    self?.trackingView.mainTableView.reloadData()
                    self?.trackingView.pathDrawable = true
                    self?.trackingView.trackPath(data: (self?.dataOrder!)!)
                }
            }
            
        }, onError: {error in
            print(error)
        }).disposed(by: disposeBag)
    }
}

extension TrackingVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
        let movedOffset: CGFloat = sender.contentOffset.y
        if sender == trackingView.mainTableView {
//            self.mapHight.constant = self.tableHeader.bounds.size.height - movedOffset
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
            
            if sender.contentOffset.y > 0 {
                Helper.addShadowToNavigationBar(controller: self.navigationController!)
            }else{
                Helper.removeShadowToNavigationBar(controller: self.navigationController!)
            }
            
        }
       
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        print(movedOffset)
//        if scrollView == trackingView.mainTableView {
//            if self.mapHight.constant > self.tableHeader.bounds.size.height-100 {
//                open = true
//                UIView.animate(withDuration: 1.0, animations: {
//                    self.tableHeader.bounds.size.height = scrollView.frame.size.height - 100
//                    self.mapHight.constant = scrollView.frame.size.height - 100
//                    self.trackingView.mainTableView.reloadData()
//                })
//            }
//        }
    }
}


