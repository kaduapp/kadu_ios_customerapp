//
//  Address.swift
//  UFly
//
//  Created by 3Embed on 23/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Address: NSObject {

    var Id                                  = ""
    var Latitude:Float                      = 0
    var Longitude:Float                     = 0
    var AddLine1                            = ""
    var AddLine2                            = ""
    var PlaceId                             = ""
    var City                                = ""
    var State                               = ""
    var Zipcode                             = ""
    var Country                             = ""
    var Tag                                 = ""
    var UserId                              = ""
    var UserType                            = ""
    var LandMark                            = ""
    var PhoneNumber                         = ""
    var FlatNo                              = ""
       
    init(data:[String:Any]) {
        
        if let titleTemp = data[APIResponceParams.PlaceId] as? String{
            PlaceId      = titleTemp
        }
        if let titleTemp = data[APIResponceParams.AddLine1] as? String{
            AddLine1     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.AddLine2] as? String{
            AddLine2     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Latitude] as? Float{
            Latitude     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Longitude] as? Float{
            Longitude    = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Latitude] as? Double{
            Latitude     = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.phoneNumber] as? String{
            PhoneNumber     = "\(titleTemp)"
        }
        if let titleTemp = data[APIResponceParams.Longitude] as? Double{
            Longitude    = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.IdAddress] as? String{
            Id           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.City] as? String{
            City         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.State] as? String{
            State        = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Pincode] as? String{
            Zipcode      = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Country] as? String{
            Country      = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Tag] as? String{
            Tag          = titleTemp
        }
        if let titleTemp = data[APIResponceParams.UserType] as? String{
            UserType     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.UserId] as? String{
            UserId       = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FlatNumber] as? String{
            FlatNo     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.LandMark] as? String{
            LandMark       = titleTemp
        }
    }

}
