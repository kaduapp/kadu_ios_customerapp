//
//  UIImage.swift
//  iDeliver
//
//  Created by Vasant Hugar on 09/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    /// Resize the Image
    ///
    /// - Parameter size: New Size
    /// - Returns: UIImage
    func resizeImage(size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
