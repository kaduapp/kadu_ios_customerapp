//
//  SavedAddressVC.swift
//  UFly
//
//  Created by Rahul Sharma on 22/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

protocol SavedAddressVCDelegate {
    func didSelectAddress(address: Address)
    
    func didSelectAddress(address: Address ,indexPath: IndexPath)
    func didSelectPickupAddress(address: Address)
}

class SavedAddressVC: UIViewController {
    
    @IBOutlet weak var addNewButtonView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var addNewBtn: UIButton!
    var navView = NavigationView().shared
    var isSendPackage:Bool = false
    var isSendAnythingPickupAddress = false

    var delegate:SavedAddressVCDelegate? = nil
    var isCheckout:Bool = false
    var indexPathSelected:IndexPath? = nil
    var savedAddressVM = SavedAddressVM()
    var emptyView = EmptyView().shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEmptyView()
        setVMResponse()
        if isCheckout {
            self.leftBarButton()
            Helper.PullToClose(scrollView: mainTableView)
            Helper.pullToDismiss?.delegate = self
        }
        savedAddressVM.getAddress()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateView()
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: UIConstants.PageTitles.AddressList)
        }
        UIApplication.shared.statusBarStyle = .lightContent
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
    }
    
    
    @IBAction func didTapAddLocation(_ sender: Any) {
        
        performSegue(withIdentifier: UIConstants.SegueIds.ToMap, sender: self)
    }
    
    
    @objc func deleteAddress(sender:UIButton!) {
        savedAddressVM.deleteAddress(tag:sender.tag)
    }
    
    @objc func editAddress(sender:UIButton!) {
        performSegue(withIdentifier: UIConstants.SegueIds.ToMap, sender: sender)
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.ToMap {
            if let viewController: AddAddressVC = segue.destination as? AddAddressVC{
                if let btn = sender as? UIButton {
                    viewController.addressAdded = savedAddressVM.arrayOfddress[btn.tag]
                    viewController.addressline1 = savedAddressVM.arrayOfddress[btn.tag].AddLine1
                    viewController.flagEdit = true
                    viewController.addAddressVM = savedAddressVM
                }
            }
        }
    }
    
    func setEmptyView() {
//        var flag:Bool = false
//        if isCheckout {
//            flag = true
//        }
        emptyView.setData(image: #imageLiteral(resourceName: "EmptyAddress"), title: StringConstants.EmptyAddress(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        emptyView.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
    }
    
    @objc func StartShopping() {
        Helper.changetTabTo(index: AppConstants.Tabs.Home)
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension SavedAddressVC {
    func setVMResponse() {
        SavedAddressVM.addressVM_response.subscribe(onNext: { success in
            if success.0 == SavedAddressVM.ResponseType.Load{
                self.mainTableView.reloadData()
            }else if success.0 == SavedAddressVM.ResponseType.Delete {
                if self.mainTableView.numberOfRows(inSection: 0) > success.1 {
                    Helper.showPI(string: "")
                    self.savedAddressVM.loaded = false
                    self.savedAddressVM.arrayOfddress.remove(at: success.1)
                    if self.mainTableView.numberOfRows(inSection: 0) != 1 {
                        self.mainTableView.deleteRows(at: [IndexPath.init(row: success.1, section: 0)], with: .left)
                    }
                    self.savedAddressVM.loaded = true
                    self.mainTableView.reloadData()
                    Helper.hidePI()
                }
            }else if success.0 == SavedAddressVM.ResponseType.Add {
                self.savedAddressVM.arrayOfddress.append(success.2)
//                let count = self.mainTableView.numberOfRows(inSection: 0)
//                self.mainTableView.insertRows(at: [IndexPath.init(row: count, section: 0)], with: UITableView.RowAnimation.right)
                self.mainTableView.reloadData()
            }else if success.0 == SavedAddressVM.ResponseType.Update {
                for item in self.savedAddressVM.arrayOfddress {
                    let index = self.savedAddressVM.arrayOfddress.index(of: item)
                    if success.2.Id == item.Id {
                        self.savedAddressVM.arrayOfddress[index!] = success.2
                        self.mainTableView.reloadData()
                        return
                    }
                }
            }
        }).disposed(by: self.savedAddressVM.disposeBag)
        APICalls.LogoutInfoTo.subscribe(onNext: { success in
            if success {
                if self.isCheckout {
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }).disposed(by: self.savedAddressVM.disposeBag)
    }
}

extension SavedAddressVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableView {
            if scrollView.contentOffset.y > 0 {
                if (self.navigationController != nil) {
                    Helper.addShadowToNavigationBar(controller: self.navigationController!)
                }
            }else{
                if (self.navigationController != nil) {
                    Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                }
            }
        }
    }
}

