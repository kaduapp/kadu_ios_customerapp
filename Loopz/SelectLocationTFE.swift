//
//  SelectLocationTFE.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension SelectLocationVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        if self.selectLocVM.addressList.count > 0  || searchBar.text?.count == 0{
        self.mainTableView.reloadData()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        noDelivery = false
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if ((searchBar.text!.length > 0  && self.selectLocVM.addressList.count > 0)  || searchBar.text?.count == 0){
            mainTableView.reloadData()
        }
        let language = Helper.getKeyboardLanguage(language:(searchBar.textInputMode?.primaryLanguage)!)
        selectLocVM.locationManager?.search(searchText: searchBar.text!,language:language)
    }
}







