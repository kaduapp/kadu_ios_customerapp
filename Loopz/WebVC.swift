//
//  WebVC.swift
//  FlexY
//
//  Created by 3 Embed on 15/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import WebKit

class WebVC: UIViewController {
    
    @IBOutlet weak var myWebView: WKWebView!
    var urlGiven:String? = nil
    var htmlDesc = ""
    var titleText = StringConstants.HelpAndSupport()
    var liveChat = LiveChatModel()
    var navView = NavigationView().shared
    let responseObserver = PublishSubject<(Bool,String)>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = titleText
        if urlGiven != nil {
            if let url = URL(string: urlGiven!) {
              //  let request = URLRequest(url: url)
                 myWebView.load(URLRequest(url: url))
               // myWebView.loadRequest(request)
            }
        }else if htmlDesc.length != 0{
            Helper.showPI(string: "")
            let htmlString:String! = htmlDesc
            myWebView.loadHTMLString(htmlString, baseURL: nil)
        }else{
            if Changebles.LiveChatAvailable {
                self.requestURL()
            }
        }
        
        // Do any additional setup after loading the view.
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: titleText)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
 extension WebVC: WKNavigationDelegate
{
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        Helper.showPI(string: "")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Helper.hidePI()
    }
   func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        Helper.showPI(string: "")
        
        switch navigationAction.navigationType {
        case .linkActivated:
              if navigationAction.targetFrame == nil {
                webView.load(navigationAction.request)
              }
              if let url = navigationAction.request.url, !url.absoluteString.contains("?success=") {
                UIApplication.shared.canOpenURL(url)
                  print(url.absoluteString)
                decisionHandler(.cancel)
              return
              }
              default:
                  break
          }

          if let url = navigationAction.request.url {
              print(url.absoluteString)
          }
        decisionHandler(.allow)
        /*
        let data = request.mainDocumentURL!.absoluteString
        if data.lowercased().range(of:"?success=") != nil {
            let array = Helper.getStringFromUrl(url:request.mainDocumentURL!)
            let src_Id = array[array.count - 2]
            let comma = data.index(of: "=")
            let code = data.suffix(from: data.index(after: comma!))
            if code == "false" {
                self.responseObserver.onNext((false,""))
                self.navigationController?.popViewController(animated: true)
            }else if code == "true" {
                self.responseObserver.onNext((true,src_Id))
                self.navigationController?.popViewController(animated: false)
            }
        }*/
     }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
     }
}

 


//Added
extension WebVC {
    
    /// Request URl
    func requestURL() {
        liveChat.getLiveChatAPIRequest { (liveChatResult) in
            
            switch liveChatResult {
            case .success(let urlRequest):
 
                self.myWebView.load(urlRequest)
                break
                
            case .failure(let error):
                Helper.hidePI()
                print(error)
                break
            }
        }
    }
}

