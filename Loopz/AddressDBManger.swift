//
//  AddressDBManger.swift
//  DelivX
//
//  Created by 3Embed on 26/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import GoogleMaps

class AddressDBManger: NSObject {
    
    
    /// get Address
    ///
    /// - Parameter type: 1 for [String:Any] and 2 for Location
    /// - Returns: Array of cart
    func getAddressDocument(type:Int) -> [Any] {
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.AddressCouchDB)
        let doc: CBLDocument = CouchDBEvents.getDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        let array = (dic["Value"] as? [Any])!
        if type == 1 {
            return array
        }
        var addressArray = [Location]()
        for item in array {
            let itemObject = item as! [String:Any]
            let data = Location.init(data: item as! [String:Any])
            if let titleTemp = itemObject[GoogleKeys.LocationLat] as? Double, let titleTemp2 = itemObject[GoogleKeys.LocationLong] as? Double {
                let locationin = CLLocation(latitude: CLLocationDegrees(titleTemp), longitude: CLLocationDegrees(titleTemp2))
                data.update(data: itemObject, location: locationin)
            }else if let titleTemp = itemObject[GoogleKeys.LocationLat] as? Float, let titleTemp2 = itemObject[GoogleKeys.LocationLong] as? Float {
                let locationin = CLLocation(latitude: CLLocationDegrees(titleTemp), longitude: CLLocationDegrees(titleTemp2))
                data.update(data: itemObject, location: locationin)
            }
            addressArray.append(data)
        }
        return addressArray
    }
    
    
    func updateAddressDocument(data:[Any]) {
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.AddressCouchDB)
        CouchDBEvents.updateDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!, documentArray: data as [AnyObject])
    }

    
    func insertAddress(data: [String:Any]) {
        let doc = getAddressDocument(type: 1) as [Any]
        for item in doc {
            let arrayItem = item as! [String:Any]
            if arrayItem[GoogleKeys.Description] as! String == data[GoogleKeys.Description] as! String {
                return
            }
        }
        var dataArray = [Any]()
        dataArray.append(data)
        if doc.count <= 4 {
            dataArray.append(contentsOf: doc)
        }else{
            for dataItem in doc {
                dataArray.append(dataItem)
                if dataArray.count >= 5 {
                    break
                }
            }
        }
        updateAddressDocument(data: dataArray)
    }
}
