//
//  APIEndTails.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class APIEndTails: NSObject {
    
    static let VersionAppStore                          = "http://itunes.apple.com/lookup?bundleId=\(Utility.BundleId)"
    
    static let BaseUrl                                  = Changebles.BaseURL
    static let BaseUrlSearch                            = Changebles.BaseURLSearch
    static let Customer                                 = "customer/"
    static let Business                                 = "business/"
    static let Products                                 = "products/"
    static let Product                                  = "product/"

    
    static let GuestLogin                               = BaseUrl + "guest/signIn"
    static let Address                                  = BaseUrl + "address"
    
    
    // For Adding New cart
    static let Cart                                     = BaseUrl + "cart"
    
    // FOr updating Cart with Addons
    static let CartNew                                  = BaseUrl + "cartNewWithAddOns"
    
    // For Adding Addons in Cart
    static let Addons                                   = BaseUrl + "cartAddOns"
    
    
    // To Delete the Cart On Empty
    static let ClearCart                                = BaseUrl + "dispatcher/cart/clear"
    
    
    static let Card                                     = BaseUrl + "card"
    
    // Get Order Details in History
    static let Order                                    = BaseUrl + Customer + "order"
    static let Login                                    = BaseUrl + Customer + "signIn"
    static let Logout                                   = BaseUrl + Customer + "logout"
    static let ValidatePhone                            = BaseUrl + Customer + "emailPhoneValidate"
    static let ForgotPassword                           = BaseUrl + Customer + "forgotPassword"
    static let GetOTP                                   = BaseUrl + Customer + "sendOtp" 
    static let VerifyOTP                                = BaseUrl + Customer + "verifyOtp"
    static let Signup                                   = BaseUrl + Customer + "signUp"
    static let RefreshToken                             = BaseUrl + Customer + "refreshToken"
    static let Configure                                = BaseUrl + Customer + "config"
    static let Version                                  = BaseUrl + Customer + "version"
    
    static let UpdatePassword                           = BaseUrl + Customer + "password"
    
    static let Profile                                  = BaseUrl + Customer + "profile"
    
    static let Rating                                   = BaseUrl + Customer + "rating"
    
    static let OperationZone                            = BaseUrl + "storeByStoreType"

    // Get All products along categories and subcategories
    static let HomeList                                 = BaseUrl + "productAndCategories/"
    
    static let GetProducts                              = BaseUrl + "products" //Business + Products
    
    static let GetDispensaries                          = BaseUrl + Business + "stores/"
    
    static let FareCalculate                            = BaseUrl + Business + "fare"
    
    // Get product details data
    static let ProductDetails                           = BaseUrl + Business + "productDetails/"
    
    // Get All Products like top deals offers
    static let ViewAll                                  = BaseUrl + Business + "home/"
    
    static let SearchProductName                        = BaseUrlSearch + "suggestions/"
    static let PopularSearchFilter                      = BaseUrlSearch + "storeWiseProductSuggestions/"
    static let SearchProducts                           = BaseUrlSearch + "searchFilter/"
    static let SearchPopular                            = BaseUrlSearch + "popularSearchFilter/"
    static let Languages                                = Changebles.BaseURL + "utility/languages"
    
    static let GetFilter                                = BaseUrlSearch + "filterParameters/"
    static let GetStoreFilter                           = BaseUrlSearch + "storeFilterParameters/"
    static let GetFilterCategory                        = BaseUrlSearch + "categoryfilterParameters/"
    static let Offers                                   = BaseUrlSearch + "offerslist/"
    static let SearchStore                              = BaseUrlSearch + "storeSearchFilter/"
    
    static let OrderDetail                              = Order  + "/detail/"
    static let ItemToWishList                           = WishList + "/product"
    static let HelpAndSupport                           = BaseUrl + Customer + "support"

    static let CheckPromocode                           = BaseUrl + Customer + "promoCodeValidation"
    static let Checkout                                 = BaseUrl + "checkout"
    static let GetPromocode                             = BaseUrl + "promoByCityId/"
    static let ValidateReferral                         = BaseUrl + "validateReferralCode/"
    static let ReferralCodeByUserId                     = BaseUrl + "referralCodeByUserId/"
    static let CancelReasons                            = BaseUrl + "reasons"
    static let Tip                                     = BaseUrl + "admin/tip"

    
    
    //Zendesk
    static let Zendesk                                  = BaseUrl + "zendesk/"
    static let Ticket                                   = Zendesk + "ticket"
    static let Comment                                  = Ticket + "/comments"
    static let HistoryTicket                            = Ticket + "/history/"
    static let SingleTicketDetails                      = Zendesk + "user/ticket/"
    
    //Wallet
    static let WalletDetails                            = BaseUrl + Customer + "walletDetail"
    static let RechargeWallet                           = BaseUrl + Customer + "rechargeWallet"
    static let WalletTransaction                        = BaseUrl + Customer + "walletTransaction/"
    static let VoucherRedeem                            = BaseUrl + "voucher"
    
    // Ideal Payment Completion API
    static let iDealScrId                               = ""
    
    
    
    // Get All Stores with Limit
    static let Store                                    = BaseUrl + "store"
    
    
    // For Making A Store Favorite
    static let FavouriteStore                           =  BaseUrl + Business + "store/" + "favorite"
    // Making A product Favrite
    static let Favourite                                = BaseUrl + "favorite"
    
    static let WishList                                 = BaseUrl + "wishList"
    // Get All Favorite stores
     static let favStores                               = BaseUrl + "store/favourite"
    
    
    // Get Super Categories
    static let SuperCategories                          = Store + "/categories"
    
    
    //Get Products for Dining Store
    static let RestuarantStore                          = BaseUrl + Business + Product
    //Laundry
    static let PickupEstimate                           = BaseUrl + "slots/pickup/"  + Utility.getAddress().ZoneId
    static let DeliveryEstimate                        = BaseUrl + "slots/delivery/"  + Utility.getAddress().ZoneId

    
    //Google Key Rotation
     static let KeyRotation                        = BaseUrl + "utility/keyRotation"
    // Send Anything Cart
    static let sendPackageCart = BaseUrl + "dispatcher/customOrder/cart"

 }
