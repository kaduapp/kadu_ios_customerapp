//
//  SelectLocationVM.swift
//  UFly
//
//  Created by 3 Embed on 08/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SelectLocationVM: NSObject {
    
    let SelectLocVMResponse                 = PublishSubject<(Bool,Bool)>()
    var addressList                         = [Location]()
    var recentSearchList                    = AddressDBManger().getAddressDocument(type: 2) as! [Location]
    var savedAddressList                    = SavedAddressDBManager().getAddressDocument()
    var locationManager:LocationManager?    = nil
    var locationSelected:Location?          = nil
    var dataAddress:[String : Any]          = [:]
    
    var didSelectRow = false
    
    
    /// this method having subscription to operationZoneAPI defined in LocationAPICalls
    ///
    /// - Parameter location: it is of type Location
    func getLocation(location: Location) {
        LocationAPICalls.operationZoneAPI(location:location, launch: false).subscribe(onNext: {[weak self] data in
            if data.keys.count == 0 {
                self?.SelectLocVMResponse.onNext((false,false))
                return
            }
            self?.locationSelected = location
            self?.dataAddress = [
                GoogleKeys.PlaceId             : location.PlaceId,
                GoogleKeys.Description         : location.LocationDescription,
                GoogleKeys.MainText            : location.MainText,
                GoogleKeys.AddressName         : location.Name,
                GoogleKeys.AddressCity         : location.City,
                GoogleKeys.AddressState        : location.State,
                GoogleKeys.AddressZIP          : location.Zipcode,
                GoogleKeys.AddressCountry      : location.Country,
                GoogleKeys.LocationLat         : location.Latitude,
                GoogleKeys.LocationLong        : location.Longitude,
                GoogleKeys.ZoneId              : data[APIResponceParams.Id] as! String,
                GoogleKeys.ZoneName            : data[APIResponceParams.Title] as! String,
                GoogleKeys.CityID              : data[GoogleKeys.CityID] as! String,
                GoogleKeys.CityName            : data[GoogleKeys.CityName] as! String
            ]
            self?.SelectLocVMResponse.onNext((true,true))
            Utility.saveCurrency(symbol: data[APIResponceParams.CurrencySymbol] as! String,currency: data[APIResponceParams.Currency] as! String, milageMetric: data[APIResponceParams.MilageMetric] as! String)
            CartDBManager().insertCartDocument(data:[])
            AddressDBManger().insertAddress(data: (self?.dataAddress)!)
            if self?.didSelectRow == true {
                self?.SelectLocVMResponse.onNext((false,true))
            }
        }, onError: {error in
        }).disposed(by: APICalls.disposesBag)
    }
    
}
