//
//  ItemDetailUIE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension ItemDetailVC {
    
    // Do any additional setup after loading the view.
    func initialSetup(){
        mainTableView.estimatedRowHeight = 176
        mainTableView.rowHeight = UITableView.automaticDimension
        didGetItemDetail()
        bottomCartView.updateAmount(data:itemDetailVM)
        unitView.selectedItem = itemDetailVM.item
        Helper.setShadow(sender: BottomView)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
//        Fonts.setPrimaryMedium(titleLabel)
//        self.titleLabel.text = itemDetailVM.item.Name
//        self.titleLabel.textColor = UIColor.clear
        unitViewHight.constant = 0
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 120
            navView.titleLabel.numberOfLines = 2
            navView.setData(title: itemDetailVM.item.Name)
            Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        }
    }
    
    //mentains detailDescription flag to show short discription and detail discription
    @objc func detailArrowBtnTapped() {
        if itemDetailVM.didTapDownArrow == true {
            itemDetailVM.didTapDownArrow = false
        } else {
            itemDetailVM.didTapDownArrow = true
        }
        UIView.performWithoutAnimation{
        //    let lastScrollOffset = self.mainTableView.contentOffset
            self.mainTableView.beginUpdates()
            var mySet: IndexSet = IndexSet.init()
            mySet.insert(1)
            self.mainTableView.reloadSections(mySet, with: .none)
            self.mainTableView.endUpdates()
            self.mainTableView.layer.removeAllAnimations()
            //self.mainTableView.setContentOffset(lastScrollOffset, animated: false)
        }
    }
    
}

