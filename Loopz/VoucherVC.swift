//
//  VoucherVC.swift
//  DelivX
//
//  Created by 3Embed on 11/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class VoucherVC: UIViewController {
    
    
    var dispose = DisposeBag()
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var separator: UIView!
    
    let voucherVM = VoucherVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.didGetResponse()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeObserver()
    }
    
    //MARK: - UIButton Action
    @IBAction func closeAction(_ sender: Any) {
        closeVC()
    }
    @IBAction func confirmAction(_ sender: Any) {
        let cell = mainTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! VoucherTVC
        if let text = cell.textField.text{
            if text.sorted().count > 0{
                voucherVM.voucherCode = cell.textField.text!
                voucherVM.redeemVoucher()
            }else{
                Helper.showAlert(message: StringConstants.VouchercodeMissing(), head: StringConstants.Message(), type: 1)
            }
        }
    }
}
