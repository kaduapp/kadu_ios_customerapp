//
//  Utility.swift
//  UFly
//
//  Created by 3Embed on 26/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import GoogleMaps
import FirebaseMessaging
import RxAlamofire
import RxCocoa
import RxSwift
import Alamofire


class Utility: NSObject {
    enum TypeServe:Int {
        case Login          = 1
        case ChangeStore    = 2
    }
    static let UtilityResponse = PublishSubject<Bool>()
    static let UtilityUpdate = PublishSubject<TypeServe>()
    
    ///Saving selected Location
    class func saveAddress(location:[String:Any]) {
        let dict = Helper.nullKeyRemoval(data: location)
        UserDefaults.standard.set(dict, forKey: UserDefaultConstants.SavedAddress)
        UserDefaults.standard.synchronize()
        if !Changebles.isGrocerOnly{
        Utility.saveStore(location: [:])
        }
        UtilityResponse.onNext(true)
    }
    //Getting Saved Location
    class func getAddress() -> Location {
        if let data = UserDefaults.standard.object(forKey: UserDefaultConstants.SavedAddress) as? [String : Any] {
            let location = Location.init(data: data)
            if let titleTemp = data[GoogleKeys.LocationLat] as? Float, let titleTemp2 = data[GoogleKeys.LocationLong] as? Float{
                let locationin = CLLocation(latitude: CLLocationDegrees(titleTemp), longitude: CLLocationDegrees(titleTemp2))
                location.update(data: data, location: locationin)
            }
            return location
        }
        return Location.init(data: [:])
    }
    
    
    ///Saving selected Location
    class func saveStore(location:[String:Any]) {
        
        let dict = Helper.nullKeyRemoval(data: location)
        UserDefaults.standard.set(dict, forKey: UserDefaultConstants.SavedStore)
        UserDefaults.standard.synchronize()
    }
    //Getting Saved Location
    class func getStore() -> Store {
        if let data = UserDefaults.standard.object(forKey: UserDefaultConstants.SavedStore) as? [String : Any] {
            let store = Store.init(data: data)
            //            if store.Id.length == 0 {
            //                store = Store.init(data:data)
            //            }
            return store
        }
        return Store.init(data: [:])
    }
    
    //saving the superStores
    
    class func saveSuperStores(stores:[Any]){
        DispatchQueue.main.async {
            SuperStoreDBManager().updateSuperStoreDocument(data: stores)
        }
    }
    
    //getting saved superStores
    class func getSuperStores() -> [SuperStore]{
        return SuperStoreDBManager().getSuperStoreDocument()
    }
    
    
    ///Saving selected superStores
    class func saveSelectedSuperStores(superStores:[String:Any]) {
        let dict = Helper.nullKeyRemoval(data: superStores)
        UserDefaults.standard.set(dict, forKey: UserDefaultConstants.SelectedSuperStore)
        UserDefaults.standard.synchronize()
    }
    //Getting Saved selected superStores
    class func getSelectedSuperStores() -> SuperStore {
        
        if Changebles.isGrocerOnly{
          
            var superStore = SuperStore.init(data: [:])
            superStore.type = .Grocery
            if let superStoreID = UserDefaults.standard.object(forKey: UserDefaultConstants.SelectedSuperStoreID) as? String{
            superStore.Id = superStoreID
            }
         return superStore
        }
        
        if let data = UserDefaults.standard.object(forKey: UserDefaultConstants.SelectedSuperStore) as? [String : Any] {
            let superStore = SuperStore.init(data: data)
            return superStore
        }
        return SuperStore.init(data: [:])
    }
    
    ///Device Details
    static var DeviceId: String {
        
        if let ID: String = UIDevice.current.identifierForVendor?.uuidString {
            return ID
        }
        else {
            return "iPhone_Simulator"
        }
    }
    
    static var DeviceType: Int {
        return 1//"iOS"
    }
    
    static var DeviceMake: String {
        return "Apple"
    }
    
    static var AppName: String {
        return (Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String)!
    }
    
    static var AppVersion: String {
        return (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!
    }
    
    static var BundlePrefix: String {
        return Bundle.main.infoDictionary!["AppIdentifierPrefix"] as! String
    }
    
    static var BundleId: String {
        return Bundle.main.infoDictionary?["CFBundleIdentifier"] as! String
    }
    
    static var DeviceName: String {
        return UIDevice.current.systemName as String
    }
    
    static var DeviceVersion: String {
        return UIDevice.current.systemVersion as String
    }
    
    static var DeviceModel: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    
    static var DeviceTime: String {
        let myString = Helper.getDateString(value: Date(),format: DateFormat.DateAndTimeFormatServer, zone: false)
        return myString
    }
    
    static var DeviceTimeWithoutBlankSpace: String {
        let myString = Helper.getDateString(value: Date(),format: DateFormat.DateAndTimeFormatServerGET, zone: false)
        return myString
    }
    
    //Get User Type
    static var UserType: Int {
        return 1//"iOS"
    }
    
    // Push Token
    static var PushToken: String {
        if let token: String = UserDefaults.standard.value(forKey: UserDefaultConstants.PushToken) as! String? {
            return token
        }
        return "default_" + UserDefaultConstants.PushToken
    }
    
    class func savePushToken(token : String) {
        UserDefaults.standard.set(token, forKey: UserDefaultConstants.PushToken)
        UserDefaults.standard.synchronize()
    }
    
    ///Set Login
    class func saveLogin(data:Bool) {
        UserDefaults.standard.set(data, forKey: UserDefaultConstants.IsLoggedIn)
        UserDefaults.standard.synchronize()
    }
    
    ///Getting login
    class func getLogin() -> Bool {
        return UserDefaults.standard.bool(forKey: UserDefaultConstants.IsLoggedIn)
    }
    
    ///Set Login
    class func saveGuestLogin(data:Bool){
        UserDefaults.standard.set(data, forKey: UserDefaultConstants.GuestLoggin)
        UserDefaults.standard.synchronize()
    }
    
    ///Getting login
    class func getGuestLogin() -> Bool{
        return UserDefaults.standard.bool(forKey: UserDefaultConstants.GuestLoggin)
    }
    
    ///Set Login
    class func saveCartId(data:String){
        UserDefaults.standard.set(data, forKey: UserDefaultConstants.CartId)
        UserDefaults.standard.synchronize()
    }
    
    ///Getting login
    class func getCartId() -> String{
        if UserDefaults.standard.string(forKey: UserDefaultConstants.CartId) == nil {
            return ""
        }
        return UserDefaults.standard.string(forKey: UserDefaultConstants.CartId)!
    }
    
    ///Saving selected Location
    class func saveProfileData(data:[String:Any]) {
        var dict = Helper.nullKeyRemoval(data: data)
        self.saveLogin(data: true)
        if let referral = dict[APIResponceParams.ReferralCode] as? String {
            self.saveReferralCode(data: referral)
        }
        if let dataSession = dict[APIResponceParams.SessionToken] as? String {
          dict.removeValue(forKey: APIResponceParams.SessionToken)
            self.saveSession(token: dataSession)
        }
        UserDefaults.standard.set(dict, forKey: UserDefaultConstants.ProfileData)
        UserDefaults.standard.synchronize()
    }
    
    ///Saving selected Location
    class func removeProfileData() {
        self.saveLogin(data: false)
    UserDefaults.standard.removeObject(forKey:UserDefaultConstants.ProfileData)
        self.saveReferralCode(data: "")
        self.saveSession(token: "")
        KeychainSwift().delete("sessionToken")
        KeychainSwift().clear()
        UserDefaults.standard.synchronize()
    }
    
    class func getProfileData() -> User {
        
        if self.getLogin() {
            let data     = UserDefaults.standard.object(forKey: UserDefaultConstants.ProfileData) as! [String : Any]
            let userData = User.init(data: data)
            return userData
        }
        else {
            var data:[String:Any] = [:]
            if  UserDefaults.standard.object(forKey: UserDefaultConstants.GuestProfileData) as? [String : Any] != nil {
                data = UserDefaults.standard.object(forKey: UserDefaultConstants.GuestProfileData) as! [String : Any]
            }
            let userData = User.init(data: data)
            return userData
            
        }
    }
    class func saveGuestProfileData(data:[String:Any],finish:@escaping (Bool) -> Void)  {
        var dict = Helper.nullKeyRemoval(data:data)
        Utility.saveLogin(data: false)
    
        if let dataSession = dict[APIResponceParams.SessionToken] as? String {
         dict.removeValue(forKey: APIResponceParams.SessionToken)
         self.saveGuestSession(token: dataSession)
        }
        
        UserDefaults.standard.set(dict, forKey: UserDefaultConstants.GuestProfileData)
        UserDefaults.standard.synchronize()
        finish(true)
    }
    
    class func saveReferralCode(data:String){
        UserDefaults.standard.set(data, forKey: UserDefaultConstants.ReferralCode)
        UserDefaults.standard.synchronize()
        
    }
    class func getReferralCode()->String{
        if UserDefaults.standard.string(forKey: UserDefaultConstants.ReferralCode) != nil {
            return UserDefaults.standard.string(forKey: UserDefaultConstants.ReferralCode)!
        }
        return ""
    }
    
    //session
    class func saveSession(token:String){
       KeychainSwift().set(token, forKey:"sessionToken")
    }
    class func saveGuestSession(token:String){
        KeychainSwift().set(token, forKey:"guestSessionToken")
    }
    
    class func getSessionToken() -> String{
        if self.getLogin() {
            if let token = KeychainSwift().get("sessionToken"){
                return token
            }
        }
         else{
            if let token = KeychainSwift().get("guestSessionToken"){
                return token
            }
        }
      
      return ""
    }
    
    
    ///Set Currency
    class func saveCurrency(symbol:String,currency:String,milageMetric:String) {
        UserDefaults.standard.set(milageMetric, forKey: UserDefaultConstants.MilageMetric)
        UserDefaults.standard.set(currency, forKey: UserDefaultConstants.Currency)
        UserDefaults.standard.set(symbol, forKey: UserDefaultConstants.CurrencySymbol)
        UserDefaults.standard.synchronize()
    }
    
    ///Getting CUrrency
    class func getCurrency() -> (String,String,String) {
        if UserDefaults.standard.string(forKey: UserDefaultConstants.Currency) != nil {
            return (UserDefaults.standard.string(forKey: UserDefaultConstants.Currency)!,UserDefaults.standard.string(forKey: UserDefaultConstants.CurrencySymbol)!,UserDefaults.standard.string(forKey: UserDefaultConstants.MilageMetric)!)
        }
        return ("USD","$","1")//1 miles or km
    }
    //Authentication Header
    class func getHeader() ->  HTTPHeaders {
        let headers : HTTPHeaders = [APIRequestParams.Language      : APIRequestParams.LanguageDefault(),
                                     APIRequestParams.Authorization :Utility.getSessionToken() ]
        return headers
    }
    class func getHeaderWithoutAuth() -> HTTPHeaders {
        
        let header : HTTPHeaders  = [APIRequestParams.Language      :APIRequestParams.LanguageDefault()]
        return header
    }
    
    class func subscribeFCMTopic() {
        if Utility.getLogin() {
            Messaging.messaging().subscribe(toTopic: Utility.getProfileData().FCMTopic)
        }
    }
    
    class func unsubscribeFCMTopic() {
        Messaging.messaging().unsubscribe(fromTopic: Utility.getProfileData().FCMTopic)
    }
    
    class func subscribeFCMTopicAll() {
        Messaging.messaging().subscribe(toTopic: "customer_" + Utility.getAddress().ZoneId)
        Messaging.messaging().subscribe(toTopic: "customer_" + Utility.getAddress().CityId)
    }
    
    class func unsubscribeFCMTopicAll() {
        Messaging.messaging().unsubscribe(fromTopic: "customer_" + Utility.getAddress().ZoneId)
        Messaging.messaging().unsubscribe(fromTopic: "customer_" + Utility.getAddress().CityId)
    }
    
    //save WishList
    class func saveWishList(data:[Any])
    {
        var updatedList:[Any] = []
        for item in data {
            updatedList.append(Helper.nullKeyRemoval(data: item as! [String:Any]))
        }
        UserDefaults.standard.set(updatedList, forKey: UserDefaultConstants.WishList)
        Utility.UtilityResponse.onNext(true)
    }
    
    //Get wishList
    class func getWishList() -> [Any]{
        if UserDefaults.standard.object(forKey: UserDefaultConstants.WishList) != nil {
            return UserDefaults.standard.object(forKey: UserDefaultConstants.WishList) as! [Any]
        }
        return []
    }
    
    
    class func updateWishList(data:[String:Any],type: Int){
        var preList:[Any] = []
        if UserDefaults.standard.object(forKey: UserDefaultConstants.WishList) != nil {
            preList = UserDefaults.standard.object(forKey: UserDefaultConstants.WishList) as! [Any]
        }
        var array:[Any] = []
        //add
        if type == -1 {
            array = [data]
            array.append(contentsOf: preList)
        }else if type == -2 {
            for n in 0...preList.count-1 {
                let itemTemp = PopupModel.init(data: preList[n] as! [String:Any])
                let itemData = PopupModel.init(data: data)
                if itemTemp.ID == itemData.ID {
                    preList.remove(at: n)
                    preList.insert(data, at: n)
                }
            }
            array = preList
        }else {
            array = preList;
            array.remove(at: type)
        }
        saveWishList(data: array)
    }
    
    class func saveLatAndLong(lat:Float, long:Float, address:String) {
        UserDefaults.standard.set(lat, forKey: UserDefaultConstants.CurrentLatitude)
        UserDefaults.standard.set(long, forKey: UserDefaultConstants.CurrentLongitude)
        UserDefaults.standard.set(address, forKey: UserDefaultConstants.CurrentAddress)
        UserDefaults.standard.synchronize()
    }
    
    class func currentLatAndLong() -> Location {
        let data = Location.init(data: [:])
        if UserDefaults.standard.string(forKey: UserDefaultConstants.CurrentLongitude) != nil && UserDefaults.standard.string(forKey: UserDefaultConstants.CurrentLatitude) != nil {
            data.Latitude = UserDefaults.standard.float(forKey: UserDefaultConstants.CurrentLatitude)
            data.Longitude = UserDefaults.standard.float(forKey: UserDefaultConstants.CurrentLongitude)
            data.Name = UserDefaults.standard.string(forKey: UserDefaultConstants.CurrentAddress)!
        }else{
            data.Latitude = 0
            data.Longitude = 0
            data.Name = StringConstants.EnableCurrentLocation()
            //            LocationManager.shared.start()
        }
        return data
    }
    
    class func connectMQTT() {
        //if Utility.getLogin() && MQTT.sharedInstance.isConnected == false {
        if  MQTT.sharedInstance.isConnected == false {
            MQTT.sharedInstance.createConnection()
        }
    }
    
    class func disconnectMQTT() {
        if Utility.getLogin() && MQTT.sharedInstance.isConnected == false {
            MQTT.sharedInstance.disconnectMQTTConnection()
        }
    }
    
    //    class func unsubscribeMQTTTopic() {
    //        MQTT.sharedInstance.unsubscribeTopic(topic:Utility.getProfileData().MQTTTopic)
    //        MQTT.sharedInstance.unsubscribeTopic(topic:"message/" + Utility.getProfileData().SId)
    //    }
    
    class func getIPAddressm() -> String {
        var address = "default"
        var addresses:[String] = []
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    let name: String = String(cString: (interface?.ifa_name)!)
                    if name.sorted().count != 0{
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
    
    class func getIPAddress() -> String {
        var addresses:[String] = []
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return "Default" }
        guard let firstAddr = ifaddr else { return "Default" }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        if addresses.count > 0 {
            for n in 1...addresses.count {
                if Helper.isValidIP(text: addresses[addresses.count-n]) {
                    return addresses[addresses.count-n]
                }
            }
        }
        return "Default"
    }
    
    
    
    
    //Logout
    // type 1 - session logout , 2 - user logout
    class func logout(type:Int,message:String) {
        if Utility.getLogin() {
            Utility.unsubscribeFCMTopic()
            Utility.disconnectMQTT()
            Utility.removeProfileData()
            CartDBManager().deleteCartDocument()
            CartDBManager.cartDBManager_response.onNext(false)
            SavedAddressDBManager().updateAddressDocument(data: [])
            Utility.saveWishList(data: [])
        }
        if type == 1 {
            var msg = message
            if message.length == 0 {
                msg = StringConstants.SessionInvalid()
            }
            var btn = StringConstants.Logout()
            if Utility.getLogin() == false {
                btn = StringConstants.OK()
            }
            
            Helper.showAlertReturn(message: msg, head: StringConstants.Message(), type: btn, closeHide: true, responce: CommonAlertView.ResponceType.SessionOut)
            CommonAlertView.AlertPopupResponse.subscribe(onNext: { (success) in
                if success == .SessionOut {
                    APICalls.LogoutInfoTo.onNext(true)
                }
            }).disposed(by: APICalls.disposesBag)
        }else{
            APICalls.LogoutInfoTo.onNext(true)
        }
    }
    
    
    //save recent search
    class func saveRecentSearch(data:[String])
    {
        UserDefaults.standard.set(data, forKey: UserDefaultConstants.Search)
        UserDefaults.standard.synchronize()
    }
    
    //Get recent searches
    class func getRecentSearch() -> [String]{
        if UserDefaults.standard.object(forKey: UserDefaultConstants.Search) != nil {
            return UserDefaults.standard.object(forKey: UserDefaultConstants.Search) as! [String]
        }
        return []
    }
    
    //AppConfigurationData
    class func setAppConfig(data:[String:Any]) {
        let dict = Helper.nullKeyRemoval(data: data)
        UserDefaults.standard.set(dict, forKey: UserDefaultConstants.AppConfig)
        UserDefaults.standard.synchronize()
    }
    
    class func getAppConfig() -> AppConfig {
        if let data = UserDefaults.standard.object(forKey: UserDefaultConstants.AppConfig) as? [String : Any] {
            let userData = AppConfig.init(data: data)
            return userData
        }
        return AppConfig.init(data: [:])
    }
    
    
    ///Getting Language
    class func getLanguage() -> Language {
        var language = Language([:])
        language.Name = "Spanish, Castilian"
        language.Code = "es"
        if UserDefaults.standard.string(forKey: UserDefaultConstants.LanguageCode) != nil {
            language.Name = UserDefaults.standard.string(forKey: UserDefaultConstants.LanguageName)!
            language.Code = UserDefaults.standard.string(forKey: UserDefaultConstants.LanguageCode)!
        }
        return language
    }
    
    ///Getting Language
    class func setLanguage(data:Language) {
        if  data.Code == "ar"{
            RTL.shared.initWithData(value: true)
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            UITableView.appearance().semanticContentAttribute = .forceRightToLeft
            UICollectionView.appearance().semanticContentAttribute = .forceRightToLeft
            
        }
        else{
            RTL.shared.initWithData(value: false)
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
            UITableView.appearance().semanticContentAttribute = .forceLeftToRight
            UICollectionView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        UserDefaults.standard.setValue(data.Name, forKey: UserDefaultConstants.LanguageName)
        UserDefaults.standard.setValue(data.Code, forKey: UserDefaultConstants.LanguageCode)
        UserDefaults.standard.synchronize()
        OneSkyOTAPlugin.setLanguage(data.Code)
        OneSkyOTAPlugin.checkForUpdate()
    }
    
    //AppConfigurationData
    class func setWallet(data:[String:Any]) {
        let dataIn = Helper.nullKeyRemoval(data: data)
        UserDefaults.standard.set(dataIn, forKey: UserDefaultConstants.WalletBalance)
        UserDefaults.standard.synchronize()
    }
    
    class func getWallet() -> Wallet {
        if let data = UserDefaults.standard.object(forKey: UserDefaultConstants.WalletBalance) as? [String:Any] {
            return Wallet.init(data: data)
        }
        return Wallet.init(data: [:])
    }
    
    /// response of all above Zendesks
    ///
    /// - Parameters:
    ///   - statusCode: 200 is success
    ///   - responseDict: response data after success
    class func checkResponse(statusCode:Int,responseDict: [String:Any]) -> APIResponseModel {
        Helper.hidePI()
        
        print(" Response: \(responseDict)");
        let responseCodes : APICalls.ErrorCode = APICalls.ErrorCode(rawValue: statusCode)!
        Helper.hidePI()
        switch responseCodes {
        case .success, .created, .deleted :
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            return responseModel

        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            return responseModel
        }
        
    }

}

