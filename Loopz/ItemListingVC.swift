
//
//  ItemListingVC.swift
//  UFly
//
//  Created by 3Embed on 12/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ListPlaceholder

class ItemListingVC: UIViewController {
    
    let disposeBag = DisposeBag()
    var loading = false
    var itemListingVM = ItemListingVM()
    var searchVM = SearchVM()
    var filterDetailModel = FilterDetailVM()
    var cartVM = CartVM()
    var selectedRow:Int  = 0
    //Outlets
    @IBOutlet var storeView: ItemListingView!
    @IBOutlet weak var itemCategoriesCollectionView: UICollectionView!
    @IBOutlet weak var subsubCategoryCollectionView: UICollectionView!
    @IBOutlet weak var titleForTheNav: UILabel!
    @IBOutlet weak var heightOfNav: NSLayoutConstraint!
    @IBOutlet weak var widthOfNav: NSLayoutConstraint!
    
    //    let transition = PopAnimator()
    var selectedSubSubCategory = ""

    var refreshControl: UIRefreshControl!
    var noProductsView = EmptyView().shared
    var isGuestLogin = false

    override func viewDidLoad() {
        super.viewDidLoad()
        cartVM.getCart()
        setObserver()
        didGetItems()
        searchVM.selectedCategory = (itemListingVM.controllerCategory?.Name)!
        searchVM.selectedSubCategory = itemListingVM.subCategorySelected.Name
        initialViewSetup()
        storeView.showBottom()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ItemListingVC.refresh), for: UIControl.Event.valueChanged)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        itemCategoriesCollectionView.setCollectionViewLayout(layout, animated: true)

        subsubCategoryCollectionView.isPagingEnabled = true
        itemCategoriesCollectionView.delegate = self
        itemCategoriesCollectionView.dataSource = self
        searchVM.isFromItemList = true
        
        if !Utility.getLogin(){
           isGuestLogin = true
        }

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if Utility.getLogin() && isGuestLogin{
            cartVM.getCart()
            isGuestLogin = false
        }
        
    }
    
    @objc func refresh() {
        itemListingVM.getItems()
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.refreshControl.endRefreshing()
        }
    }
    
    @IBAction func didTapGotItBtn(_ sender: UIButton) {
        storeView.hideBottom(sender: sender)
        subsubCategoryCollectionView.reloadData()
    }
    
    @IBAction func didTapViewCartBtn(_ sender: UIButton) {
        self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        performSegue(withIdentifier: UIConstants.SegueIds.ItemListingToSearch, sender: self)
        
    }
    
    @IBAction func menuBtnAction(_ sender: Any) {
        itemListingVM.bottomConstraint = Int(storeView.bottomBarHight.constant + storeView.viewCartBarHight.constant+20)
        performSegue(withIdentifier: UIConstants.SegueIds.ItemListingToMenu , sender: self)
    }
    
    @IBAction func filterAction(_ sender: Any) {
        itemCategoriesCollectionView.reloadData()
        self.performSegue(withIdentifier: String(describing: FilterContainerVC.self), sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.ItemListingToItemDetails {
            if let nav:UINavigationController = segue.destination as? UINavigationController {
                
                if let itemDetailVC: ItemDetailVC = nav.viewControllers.first as? ItemDetailVC {
                    
                    if let pair =  sender as? (IndexPath,[Item]){
                       itemDetailVC.itemDetailVM.item = pair.1[pair.0.row]
                        
                    }
                    
                    itemDetailVC.viewCartHandler = {status in
                        self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
                    }
                }
            }
        }else if segue.identifier == UIConstants.SegueIds.ItemListingToMenu {
            let menuVC: MenuVC =  segue.destination as! MenuVC
            menuVC.controllerSubCategoryArray = itemListingVM.SubSubCatArray
            menuVC.bottomConstraint = itemListingVM.bottomConstraint
            menuVC.selectedId = selectedSubSubCategory
            menuVC.delegate = self
        }else if segue.identifier == UIConstants.SegueIds.ItemListingToSearch {
            let nav = segue.destination as! UINavigationController
            if let searchVC: SearchVC =  nav.viewControllers.first as? SearchVC {
                searchVC.searchVM.isFromItemList = true
            }
        }else if segue.identifier == String(describing: FilterContainerVC.self) {
            if let searchVC: FilterContainerVC =  segue.destination as? FilterContainerVC {
                searchVC.context = 1
                searchVC.cat = (itemListingVM.controllerCategory?.Name)!
                searchVC.subCat = itemListingVM.subCategorySelected.Name
                searchVC.textLanguage = Utility.getLanguage().Code
                searchVC.searchVM = self.searchVM
                searchVC.filterDetailRef = self.filterDetailModel

            }
        }else if segue.identifier == UIConstants.SegueIds.ItemListingToDispensaries {
            if let dispenceries: DispensariesVC =  segue.destination as? DispensariesVC {
                dispenceries.dispensariesVM.controllerCategory = itemListingVM.controllerCategory!
                dispenceries.responceBack.subscribe(onNext: { (data) in
                    self.itemListingVM.store = data
                    self.initialViewSetup()
                }).disposed(by: self.searchVM.disposeBag)
            }
        }
    }
    
    func setObserver() {
        Helper.networkReachabilityChange.subscribe(onNext: { [weak self]dataGot in
            self?.itemListingVM.getItems()
        }).disposed(by: self.searchVM.disposeBag)
        searchVM.searchVM_response.subscribe(onNext: { [weak self]success in
            switch success {
            case .Items:
                if self?.searchVM.arrayOfExpandedStore.Products.count ?? 0 > 0{
                }
                else{
                    self?.noProductsView.setData(image: #imageLiteral(resourceName: "EmptySearch"), title: StringConstants.EmptyFilterResult(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
                }

                self?.subsubCategoryCollectionView.reloadData()
                break
            default:
                break
            }
        }).disposed(by: self.searchVM.disposeBag)
        
        searchVM.filterInfo.subscribe(onNext: { [weak self]data in
            if data.0 {
                switch data.1 {
                case APIRequestParams.BrandFilter :
                        self?.searchVM.brandArray = (self?.filterDetailModel.selectedBrands)!
                case APIRequestParams.ManufacturerFilter :
                        self?.searchVM.manufacturerArray = (self?.filterDetailModel.selectedManufacturer)!

                case APIRequestParams.CategoryFilter :
                        if data.2.count == 0 {
                            self?.searchVM.selectedCategory = ""
                            self?.searchVM.selectedSubCategory = ""
                            self?.searchVM.selectedSubSubCategory = ""
                        }else{
                            self?.searchVM.selectedCategory = data.2[0]
                            self?.searchVM.selectedSubCategory = data.2[1]
                            self?.searchVM.selectedSubSubCategory = data.2[2]
                        }
                case APIRequestParams.FilterPrice :

                            self?.searchVM.minPrice = (self?.filterDetailModel.selectedMinValue)!
                            self?.searchVM.maxPrice = (self?.filterDetailModel.selectedMaxValue)!
 
                case APIRequestParams.Sort :

                        self?.searchVM.selectedSort = (self?.filterDetailModel.selectedSort)!

    
                case APIRequestParams.FIlterOffer :

                        if data.2.count == 0 {
                            return
                        }
                        self?.searchVM.offer = data.2[0]
     
                case "":
                    self?.searchVM.brandArray = []
                    self?.searchVM.manufacturerArray = []
                    self?.searchVM.selectedCategory = (self?.itemListingVM.controllerCategory?.Name)!
                    self?.searchVM.selectedSubCategory = (self?.itemListingVM.subCategorySelected.Name)!
                    self?.searchVM.selectedSubSubCategory = ""
                    self?.searchVM.offer = ""
                    self?.searchVM.minPrice = 0
                    self?.searchVM.maxPrice = 0
                    self?.searchVM.selectedSort = -1
                    if self?.searchVM.brandArray.count == 0 && self?.searchVM.manufacturerArray.count == 0 && self?.searchVM.minPrice == 0 && self?.searchVM.maxPrice == 0 && self?.searchVM.offer == "" && self?.searchVM.selectedSort == -1 {
                        self?.searchVM.arrayOfExpandedStore  = Utility.getStore()
                        self?.searchVM.arrayOfStores = []
                        self?.searchVM.arrayOfExpandedStore.Products.removeAll()
                        self?.setCollectionViewBackground()
                        self?.subsubCategoryCollectionView.reloadData()
                        return
                    }
                    break
                default:
                    break
                }
                
                self?.searchVM.filterProductsWithinStore(text: "", language: Utility.getLanguage().Code)
            }else{
                if self?.searchVM.selectedSubCategory != nil {
                    FilterDetailVM.filterArray_responce.onNext(((self?.searchVM.selectedCategory)!,(self?.searchVM.selectedSubCategory)!,(self?.searchVM.selectedSubSubCategory)!,(self?.searchVM.brandArray)!,(self?.searchVM.minPrice)!,(self?.searchVM.maxPrice)!,(self?.searchVM.manufacturerArray)!,(self?.searchVM.offer)!,(self?.searchVM.selectedSort)!,0,"","",""))
                }
            }
        }).disposed(by: disposeBag)
    }
}




