//
//  SeeAllFooter.swift
//  DelivX
//
//  Created by 3Embed on 10/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SeeAllFooter: UICollectionReusableView {
        
    @IBOutlet weak var seeAllButton: UIButton!
    
    override func awakeFromNib() {
        Fonts.setPrimaryMedium(seeAllButton)
        seeAllButton.tintColor = Colors.AppBaseColor
        seeAllButton.setTitle(StringConstants.ViewAll(), for: .normal)
    }
}
