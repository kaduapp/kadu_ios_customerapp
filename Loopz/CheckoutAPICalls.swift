//
//  CheckoutAPICalls.swift
//  UFly
//
//  Created by 3Embed on 25/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class CheckoutAPICalls: NSObject {
    static let disposeBag = DisposeBag()
    //Add Cart Data
    class func checkDeliveryAvaiability(item:Address,status: Int,orderType: Int) ->Observable<(Bool, String)>  {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
            }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.ValidateLocation())
        let header = Utility.getHeader()
        var params = [
            APIRequestParams.Latitude   : item.Latitude,
            APIRequestParams.Longitude  : item.Longitude,
            APIRequestParams.Status     : status,
            APIRequestParams.OrderType  : orderType
            //            APIRequestParams.Store      : data,
            //            APIRequestParams.ZoneId     : Utility.getAddress().ZoneId
            ] as [String : Any]
        if  Utility.getSelectedSuperStores().type == .SendAnything
        {
            params.updateValue(item.Latitude, forKey:  APIRequestParams.pickUpLat)
            params.updateValue(item.Longitude, forKey:  APIRequestParams.pickUpLong)
        }
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.FareCalculate, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let bodyIn = body as? [String:Any]
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let array = bodyIn![APIResponceParams.Data] as? [Any]
                    var cartArray = [Cart]()
                    for itemCart in array! {
                        cartArray.append(Cart.init(data: itemCart as! [String:Any]))
                    }
                    CartDBManager().setDeliveryCharge(carts: cartArray)
                    if cartArray.count != 0
                    {
                    observer.onNext((true,"\(cartArray[0].convenienceFee)" + "+" + "\(cartArray[0].convenienceFeeType)"))
                    }else
                    {
                        observer.onNext((true,""))

                    }
                    observer.onCompleted()
                }else{
                      observer.onNext((false,""))
                    observer.onCompleted()
                    if status == 2 && errNum == .badRequest {
                        var string = bodyIn![APIResponceParams.Message] as! String
                        if let array = bodyIn![APIResponceParams.Data] as? [String]{
                            if array.count > 0 {
                                for item in array {
                                    string = string + "\n\(String(describing: array.index(of: item)! + 1))) " + item
                                }
                            }
                        }
                        Helper.showAlert(message: string, head: StringConstants.Error(), type: 1)
                        return
                    }
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
        }.share(replay: 1)
    }
    
    
    //Add Cart Data
    class func placeOrder(checkout : CheckoutVM) ->Observable<(Bool, String)> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
            }.share(replay: 1)
        }
        let header = Utility.getHeader()
        let item = checkout.selectedAddress ?? Address.init(data: [:])
        var cardid = Card.init(data: [:]).Id
        if checkout.selectedCard != nil{
            cardid = checkout.selectedCard!.Id
        }
        var add1 = item.AddLine1
        if add1.length > 0 {
            add1 = add1 + ", " + item.AddLine2
        }else{
            add1 = item.AddLine2
        }
        var add2 = item.City
        if add2.length > 0 {
            add2 = add2 + ", "
        }
        add2 = add2 + item.State
        if checkout.pickup == 0 {
            add1 = ""
            add2 = ""
        }
        var payByWallet = 0
        if checkout.paymentHandler.isPayByWallet{
            payByWallet = 1
        }
        var deuTime = Helper.getDateString(value:checkout.deliveryDate,format:DateFormat.DateFormatServer, zone: false) + " " + Helper.getDateString(value:checkout.deliveryTime,format:DateFormat.TimeFormatServer, zone: true)
        if checkout.booking == 1 {
            deuTime = Utility.DeviceTime;
        }
        let params = [
            APIRequestParams.Latitude : item.Latitude,
            APIRequestParams.Longitude : item.Longitude,
            APIRequestParams.PaymentType : checkout.paymentHandler.paymentType,
            APIRequestParams.Address1 : add1,
            APIRequestParams.Address2 : add2,
            APIRequestParams.OrderTime : Utility.DeviceTime,
            APIRequestParams.DeuTime : deuTime,
            APIRequestParams.BookingType : checkout.booking,
            APIRequestParams.PickupType : checkout.pickup,
            APIRequestParams.CartId : Utility.getCartId(),
            APIRequestParams.IPAddress : Utility.getIPAddress(),
            APIRequestParams.PromoCode : checkout.promocode,
            APIRequestParams.Discount : checkout.discount,
            APIRequestParams.addressId : item.Id,
            "deviceTime" : Utility.DeviceTime,
            APIRequestParams.PayByWallet : payByWallet,
            APIRequestParams.CardId : cardid,
            APIRequestParams.ExtraNotes :
                AppConstants.extraNotes,
            
            "storeType" : Utility.getSelectedSuperStores().type.rawValue,
            "pickUpLat": checkout.pickupAddress?.Latitude ?? 0,
            "pickUpLong": checkout.pickupAddress?.Longitude ?? 0,
            "estimatedPackageValue": checkout.packageEstimateValue,
            "driverTip"        : AppConstants.DriverTip
 
            ] as [String : Any]
        
        
        print("place order ===",params)
        
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.Order, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let bodyIn = body as? [String:Any]
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    AppConstants.extraNotes.removeAll()
                    let data = bodyIn![APIResponceParams.Data] as? [String:Any]
                    var orderId:String = ""
                    
                    if let id = data![APIResponceParams.OrderId] as? String{
                        orderId = id
                    }
                    if let id = data![APIResponceParams.OrderId] as? Int{
                        orderId = String(describing: id)
                    }
                    if let id = data![APIResponceParams.OrderId] as? Double{
                        orderId = String(describing: id)
                    }
                    CartDBManager().insertCartDocument(data: [])
                    observer.onNext((true,orderId))
                    observer.onCompleted()
                    Helper.showPISuccess()
                }else{
                    observer.onNext((false,""))
                    observer.onCompleted()
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                // Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
        }.share(replay: 1)
    }
    
    
       class func checkTip() ->Observable<[Tip]> {
               if !AppConstants.Reachable {
                   Helper.showNoInternet()
                   return Observable.create{ observer in
                       return Disposables.create()
                       }.share(replay: 1)
               }
           
    
    
               return Observable.create { observer in
                   RxAlamofire.requestJSON(.get, APIEndTails.Tip, parameters: nil, encoding: JSONEncoding.default, headers: ["language":"en"] ).debug().subscribe(onNext: { (head, body) in
                       Helper.hidePI()
                       let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                       
                                           if errNum == .success {
                                                            let bodyIn = body as![String:Any]
                                                            let data = bodyIn[APIResponceParams.Data] as! [Any]
                                                            var tipArr:[Tip] = []
                                                            for item in data {
                                                                tipArr.append(Tip.init(data: item as! [String:Any]))
                                                            }
                                                            observer.onNext(tipArr)
                                                            observer.onCompleted()
                                                        }
                                                        else{
                                                            APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                                                        }
                                           
                   }, onError: { (Error) in
                       Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                       Helper.hidePI()
                   }).disposed(by: self.disposeBag)
                   return Disposables.create()
                   }.share(replay: 1)
           }
    
    //Add Cart Data
    class func applyPromocode(amount:Float,promocode:String,payment:Int,total:Float,isWallet:Bool,storeIds :String) ->Observable<Float> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.PlaceOrder())
        let header = Utility.getHeader()
        var payByWallet = 0
        if isWallet{
            payByWallet = 1
        }
        
        let params = [
            APIRequestParams.UserId         : Utility.getProfileData().SId,
            APIRequestParams.PromoCode      : promocode,
            APIRequestParams.CityId         : Utility.getAddress().CityId,
            APIRequestParams.ZoneId         : Utility.getAddress().ZoneId,
            APIRequestParams.PaymentMethod  : payment,
            "storeIds"                      : storeIds,
            APIRequestParams.Amount         : amount,
            APIRequestParams.DeliveryFee    : AppConstants.TotalDeliveryPrice,
            APIRequestParams.PayAmount      : total,
            APIRequestParams.PayByWallet    : payByWallet
            ] as [String : Any]
        
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.CheckPromocode, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                let bodyIn = body as! [String:Any]
                if errNum == .success {
                    let data = bodyIn[APIResponceParams.Data] as! [String:Any]
                    if let discount:Float = (data[APIResponceParams.AmountCart] as? Float)   {
                        observer.onNext(discount)
                    }else if let discount:Double = (data[APIResponceParams.AmountCart] as? Double)   {
                        observer.onNext(Float(discount))
                    }else {
                        observer.onNext(0.0)
                    }
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //Get Cart Data
    class func getCoupons() ->Observable<[Coupon]> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let location = Utility.getAddress()
        let header = Utility.getHeaderWithoutAuth()
        let url  = APIEndTails.GetPromocode + "\(location.CityId)"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let bodyIn = body as![String:Any]
                    let data = bodyIn[APIResponceParams.Data] as! [Any]
                    var couponArr:[Coupon] = []
                    for item in data {
                        couponArr.append(Coupon.init(data: item as! [String:Any]))
                    }
                    observer.onNext(couponArr)
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //Checkout
    //Add Cart Data
    class func checkout() ->Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
      //  Helper.showPI(string: StringConstants.PlaceOrder)
        let header = Utility.getHeader()
        let params = [
            APIRequestParams.Latitude       : Utility.getAddress().Latitude,
            APIRequestParams.Longitude      : Utility.getAddress().Longitude
            ] as [String : Any]
        
        
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.Checkout, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                let bodyIn = body as! [String:Any]
                if errNum == .success {
                    observer.onNext(true)
                    observer.onCompleted()
                }else{
                    if errNum == .updated {
                        CartVM().getCart()
                    }
                    observer.onNext(false)
                    observer.onCompleted()
                    
                    Helper.showAlert(message: bodyIn[APIResponceParams.Message] as! String, head: StringConstants.ChangeType(), type: 2)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: Error.localizedDescription, head: StringConstants.Error(), type: 1)
              //  Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    //sending the scr id after success transation
    class func sendTheScrId(id:String,amount:Float,orderId:String) -> Observable<Bool>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        let params = [
            APIRequestParams.Source   : id,
            APIRequestParams.Amount2  : amount,
            APIRequestParams.Currency : Utility.getCurrency().0,
            APIRequestParams.OrderID    : orderId
            ] as [String : Any]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.iDealScrId, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                //if errNum == .success {
                observer.onNext(true)
                observer.onCompleted()
                //                }else{
                //                    observer.onNext(false)
                //                    observer.onCompleted()
                //                   // APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                //                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }

}
