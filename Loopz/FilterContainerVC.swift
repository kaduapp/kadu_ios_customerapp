//
//  FilterContainerVC.swift
//  DelivX
//
//  Created by 3Embed on 09/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class FilterContainerVC: UIViewController {

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainContainerView: UIView!
    var storeFilter = false
    var context = 0
    var text = ""
    var cat = ""
    var subCat = ""
    var textLanguage = ""
    let disposeBag = DisposeBag()
    var searchVM = SearchVM()
    var filterDetailRef = FilterDetailVM()
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
           self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUI() {
        Helper.setUiElementBorderWithCorner(element: self.mainContainerView, radius: 10, borderWidth: 0, color: Colors.AppBaseColor)
        Helper.setShadow(sender: self.view)
        self.heightConstraint.constant = 44 + 100 + (5 * 44)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == String(describing: FilterVC.self) {
            let nav = segue.destination as! UINavigationController
            if let controller = nav.viewControllers.first as? FilterVC {
                controller.text = text
                controller.textLanguage = textLanguage
                controller.filterVM.filterVM_response.subscribe(onNext: { success in
                    let count = controller.filterVM.arrayOfFilter.count + 2
                    self.heightConstraint.constant = CGFloat(142 + (count * 42))
                    self.view.updateConstraints()
                    self.view.layoutIfNeeded()
                }).disposed(by: disposeBag)
                controller.filterVM.context = context
                controller.filterVM.storeFilters = storeFilter
                controller.selectedCat = cat
                controller.selectedSubCat = subCat
                controller.searchVM = self.searchVM
                controller.filterDetailVM = self.filterDetailRef
            }
        }
    }
    

}
