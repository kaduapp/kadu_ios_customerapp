//
//  HistoryTableCell.swift
//  UFly
//
//  Created by 3 Embed on 04/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HistoryTableCell: UITableViewCell {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var deliveryCharge: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var helpBtn: UIButton!
    @IBOutlet weak var trackOrderBtn: UIButton!
    @IBOutlet weak var topSeperator: UIView!
    @IBOutlet weak var bottomSeperator: UIView!
    @IBOutlet weak var trackOrderBtnView: UIView!
    @IBOutlet weak var helpBtnView: UIView!
    
    @IBOutlet weak var HistView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    
    @IBOutlet weak var scheduledOn: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        updateView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    /// sets the button titles for active order screen
    func updatePastOrder(){
        Helper.setButtonTitle(normal: StringConstants.ReOrder() , highlighted: StringConstants.ReOrder(), selected: StringConstants.ReOrder(), button: helpBtn)
        Helper.setButtonTitle(normal: StringConstants.Details() , highlighted: StringConstants.Details(), selected: StringConstants.Details(), button: trackOrderBtn)
    }
    
    ///sets the button titles for past order screen
    func updateActiveOrder(data:Order) {
        if data.Delivery{
            Helper.setButtonTitle(normal: StringConstants.TrackOrder() , highlighted: StringConstants.TrackOrder(), selected: StringConstants.TrackOrder(), button: trackOrderBtn)
            
            if data.Status >= 8 && data.Status != 40 {
            Helper.setButtonTitle(normal: StringConstants.Chat() , highlighted: StringConstants.Chat(), selected: StringConstants.Chat(), button: helpBtn)
            }
            else{
            Helper.setButtonTitle(normal: StringConstants.HelpUpper() , highlighted: StringConstants.HelpUpper(), selected: StringConstants.HelpUpper(), button: helpBtn)
            }
        }else{
            
            Helper.setButtonTitle(normal: StringConstants.Details() , highlighted: StringConstants.Details(), selected: StringConstants.Details(), button: trackOrderBtn)

            Helper.setButtonTitle(normal: StringConstants.HelpUpper() , highlighted: StringConstants.HelpUpper(), selected: StringConstants.HelpUpper(), button: helpBtn)
        }
        
    }
    
    /// initial view setup
    func updateView(){
        scheduledOn.textColor  = Colors.PrimaryText
        storeName.textColor  = Colors.PrimaryText
        deliveryCharge.textColor     = Colors.SeconderyText
        deliveryCharge.text = StringConstants.DeliveryCharge()
        itemName.textColor   = Colors.SecoundPrimaryText
        status.textColor = Colors.AppBaseColor
        dateTime.textColor = Colors.SecoundPrimaryText
        Helper.setUiElementBorderWithCorner(element: HistView, radius: 5, borderWidth: 1, color: Colors.SeparatorLight)
        
        Fonts.setPrimaryRegular(scheduledOn)
        Fonts.setPrimaryMedium(storeName)
        Fonts.setPrimaryRegular(deliveryCharge)
        Fonts.setPrimaryMedium(itemName)
        Fonts.setPrimaryRegular(dateTime)
        Fonts.setPrimaryBold(amount)
        Fonts.setPrimaryBold(trackOrderBtn)
        Fonts.setPrimaryBold(helpBtn)
        
        Helper.setButtonTitle(normal: StringConstants.TrackOrder() , highlighted: StringConstants.TrackOrder(), selected: StringConstants.TrackOrder(), button: trackOrderBtn)
        
        Helper.setButtonTitle(normal: StringConstants.HelpUpper() , highlighted: StringConstants.HelpUpper(), selected: StringConstants.HelpUpper(), button: helpBtn)
        
        Helper.setUiElementBorderWithCorner(element: trackOrderBtn, radius: 2.0, borderWidth: 2.0, color: Colors.AppBaseColor)
        Helper.setUiElementBorderWithCorner(element: helpBtn, radius: 2.0, borderWidth: 2.0, color: Colors.AppBaseColor)
        
        
        Helper.setButton(button: trackOrderBtn , view: trackOrderBtnView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: false)
        Helper.setButton(button: helpBtn , view: helpBtnView, primaryColour: Colors.SecondBaseColor, seconderyColor: Colors.AppBaseColor,shadow: false)
        
    }
    
    /// updates the cell
    ///
    /// - Parameter data: it is of type order model object
    func setData(data:Order){
        statusImage.image = Helper.setImageForStatus(status: data.Status)
        status.text = data.StatusMessage
        if data.StatusMessage.length == 0 {
            status.text = Helper.setTitleForStatus(status: data.Status)
        }
        subTotal.text = Helper.df2so(Double( data.TotalAmount - data.DeliveryCharge))
        
        total.text = Helper.df2so(Double( data.TotalAmount))
        storeName.text = data.Storename
        dateTime.text = StringConstants.OrderId() + " \(data.BookingId)"
        var type = ""
        if data.Delivery == true {
            type = StringConstants.Delivery()
        }else {
            type = StringConstants.Pickup()
        }
        var time = ""
        if data.Now == true {
            time = StringConstants.ASAP()
        }else {
            let date = Helper.getDateString(value: (data.DueDate), format: DateFormat.dateTime, zone: true)
            let dateTime = Helper.getDateString(value: (data.DueDate), format: DateFormat.TimeFormatToDisplay, zone: true)
            time = "\(dateTime), \(date)"
        }
        
        if data.Now == true {
            scheduledOn.text = time + " " + type
        }else {
            scheduledOn.text = StringConstants.Scheduled() + " " + time + " " + type
        }
        
        if data.DeliveryCharge != 0 {
            itemName.text = "+" + Helper.df2so(Double( data.DeliveryCharge))
            itemName.textColor = Colors.Red
            
        }
        else {
            itemName.text = StringConstants.Free()
            itemName.textColor   = Colors.DeliveryBtn
            
        }
        itemName.isHidden = false
//        switch data.Status {
//        case 14,15,6,7:
//            totalLabel.text = StringConstants.FinalPaid
//            break
//        default:
//        totalLabel.text = StringConstants.FinalAmount
//            break
//        }
        if data.Active {
            totalLabel.text = StringConstants.FinalAmount()
        }
        else {
            totalLabel.text = StringConstants.FinalPaid()
        }
        //        deliveryCharge.text = data.PickAddress
        //        amount.text = Helper.setCurrency(amount: data.TotalAmount)
        //        itemName.text = ""
        //        if data.Items.count > 0 {
        //            for dat in data.Items {
        //                if itemName.text!.length > 0{
        //                    itemName.text = itemName.text! + "\n"
        //                }
        //                itemName.text = itemName.text! + dat
        //            }
        //        }
        
    }
}
