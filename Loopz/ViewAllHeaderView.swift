//
//  ViewAllHeaderView.swift
//  DelivX
//
//  Created by 3Embed on 29/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation


class ViewAllHeaderView: UICollectionReusableView
{
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var viewForDotteLine: UIView!

    override func awakeFromNib() {
        Helper.drawDottedLine(viewForDotteLine)
        self.layoutIfNeeded()
        Fonts.setPrimaryBold(titleLB, size: 25)
    }
}
