//
//  SavedAddressUIE.swift
//  UFly
//
//  Created by 3Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension SavedAddressVC {
    
    /// Setup initial ui
    func updateView(){
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
 
        mainTableView.backgroundColor = Colors.ScreenBackground
        Helper.setButton(button: addNewBtn,view:addNewButtonView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Fonts.setPrimaryMedium(addNewBtn)
        addNewBtn.setTitle(StringConstants.AddNewAddress(), for: .normal)
    }
    
    //Setup Left bar button
    func leftBarButton() {
        let btn = UIButton(type: .custom)
        btn.setImage(#imageLiteral(resourceName: "CloseIcon"), for: .normal)
        btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn.addTarget(self, action: #selector(close), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
        self.navigationItem.setLeftBarButtonItems([item], animated: true)
    }
}
