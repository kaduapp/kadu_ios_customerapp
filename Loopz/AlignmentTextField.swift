//
//  AlignmentTextField.swift
//  DelivX
//
//  Created by 3EMBED on 21/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AlignmentTextField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setTextAlignmentForLeftAlignedTextField()
    }
    func setTextAlignmentForLeftAlignedTextField(){
        if RTL.shared.isRTL
        {
            self.textAlignment = .right
            
        }else
        {
            self.textAlignment = .left
        }
    }
}
