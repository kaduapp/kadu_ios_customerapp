//
//  CartCheckoutBarView.swift
//  UFly
//
//  Created by 3 Embed on 23/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CartCheckoutBarView: UIView {
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var checkoutBarView: UIView!
    @IBOutlet weak var amountSeparator: UIView!
    @IBOutlet weak var amountTopSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    /// initial view setup
    func initialSetup(){
        
        Fonts.setPrimaryBold(totalAmountLabel)
        Fonts.setPrimaryBold(amount)
        totalAmountLabel.text = StringConstants.TotalAmount()
        
        checkoutBarView.backgroundColor = Colors.SecondBaseColor
        totalAmountLabel.textColor = Colors.PrimaryText
        amount.textColor = Colors.PrimaryText
        
        amountSeparator.backgroundColor = Colors.SeparatorLight
        amountTopSeparator.backgroundColor = Colors.SeparatorLight
        
    }
    
}
