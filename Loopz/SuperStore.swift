//
//  SuperStore.swift
//  DelivX
//
//  Created by 3Embed on 11/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct SuperStore
{
    var Id = ""
    var bannerImage: String             = ""
    var logoImage: String               = ""
    var description: String             = ""
    var visibility: Bool                = false
    var type: AppConstants.TypeOfStore  = .Grocery
    var typeName: String                = ""
    var fileName: String                = ""
    var seqId: String                   = ""
    var typeMsg: String                 = ""
    var visibilityMsg: String           = ""
    var editId: String                  = ""
    var categoryName: String            = ""
    var objectData:[String:Any]         = [:]
    var colorCode                       = ""
    var gifImage                        = ""
    var gifData                         = ""
    
    init(data:[String:Any])
    {
        objectData = data
        if let bannerImageTemp = data[APIResponceParams.IdAddress] as? String
        {
            Id = bannerImageTemp
        }
        if let bannerImageTemp = data[APIResponceParams.SuperStoreBannerImage] as? String
        {
            bannerImage = bannerImageTemp
        }
        if let logoImageTemp = data[APIResponceParams.LogoImage] as? String
        {
            logoImage = logoImageTemp
        }
        if let descriptionTemp = data[APIResponceParams.SuperStoreDesc] as? String
        {
            description = descriptionTemp
        }
        if let visibilityTemp = data[APIResponceParams.Visibility] as? Double
        {
            if visibilityTemp == 0
            {
                visibility = false
            }
            else if visibilityTemp == 1
            {
                visibility = true
            }
        }
        if let typeTemp = data[APIResponceParams.SuperStoreType] as? Int
        {
            if typeTemp > 7 {
                type = AppConstants.TypeOfStore(rawValue: 2)!
            }
            else {
                type = AppConstants.TypeOfStore.init(rawValue: typeTemp)!
            }
        }
        if let typeNameTemp = data[APIResponceParams.SuperStoreTypeName] as? String
        {
            typeName = typeNameTemp
        }
        if let fileNameTemp = data[APIResponceParams.SuperStoreFileName] as? String
        {
            fileName = fileNameTemp
        }
        if let seqIdTemp = data[APIResponceParams.SeqId] as? String
        {
            seqId = seqIdTemp
        }
        if let typeMsgTemp = data[APIResponceParams.TypeMsg] as? String
        {
            typeMsg = typeMsgTemp
        }
        if let visibilityMsgTemp = data[APIResponceParams.VisibilityMsg] as? String
        {
            visibilityMsg = visibilityMsgTemp
        }
        if let editIdTemp = data[APIResponceParams.EditId] as? String
        {
            editId = editIdTemp
        }
        if let categoryNameTemp = data[APIResponceParams.SuperStoreCategoryName] as? String
        {
            categoryName = categoryNameTemp
        }
        if let editIdTemp = data[APIResponceParams.CatTypeGif] as? String
        {
            gifImage = editIdTemp
            if gifImage.length == 0 {
                var gif = ""
                switch type {
                case .Restaurant:
                    gif = "Restaurant"
                    break
                case .Grocery:
                    gif = "Grocery"
                    break
                case .Shopping:
                    gif = "Fashion"
                    break
                default:
                    break
                }
                gifData = gif
            }
        }
        if let categoryNameTemp = data[APIResponceParams.ColorCode] as? String
        {
            colorCode = categoryNameTemp
        }
    }
}

