//
//  ConfirmOrderAPICalls.swift
//  DelivX
//
//  Created by 3EMBED on 26/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire
class ConfirmOrderAPICalls: NSObject {
    let rx_addResponse = PublishSubject<APIResponseModel>()
    let rx_updateResponse = PublishSubject<APIResponseModel>()
    let rx_deleteResponse = PublishSubject<APIResponseModel>()
    let rx_getResponse = PublishSubject<APIResponseModel>()
    
    let disposeBag = DisposeBag()
    
    func getEstimate(_ params:[String : Any]) {
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
        }
        
        let strUrl = APIEndTails.BaseUrl + "laundry/estimate"
        
        RxAlamofire
            .requestJSON(.post ,
                         strUrl,
                         parameters: params,
                         encoding: JSONEncoding.default,
                         headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: r.statusCode)!
                if errNum == .success
                {
                if let dict = json as? [String:Any] {
                    let statuscode:Int = r.statusCode
                    let response = Utility.checkResponse(statusCode: statuscode, responseDict: dict)
                    self.rx_addResponse.onNext(response)
                    
                }
                }else
                {
                     if let dict = json as? [String:Any] {
                    APICalls.basicParsing(data: dict, status: r.statusCode)
                    }
                }
            }, onError: { (error) in
                print(error.localizedDescription)
            }, onCompleted: {
                print("AddItemToCart completed")
            }, onDisposed: {
                print("AddItemToCart Disposed")
            }).disposed(by: disposeBag)
    }
    
    func Placeorder(_ params:[String : Any]) {
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
        }
        
        let strUrl = APIEndTails.BaseUrl + "laundry/sendPacket"
        
        RxAlamofire
            .requestJSON(.post ,
                         strUrl,
                         parameters: params,
                         encoding: JSONEncoding.default,
                         headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
               
                if let dict = json as? [String:Any] {
                    let statuscode:Int = r.statusCode
                    let response = Utility.checkResponse(statusCode: statuscode, responseDict: dict)
                    self.rx_addResponse.onNext(response)
                    if r.statusCode == 200
                    {
                     Helper.showPISuccess()
                    }
                }
            }, onError: { (error) in
                print(error.localizedDescription)
                
            }, onCompleted: {
                print("AddItemToCart completed")
            }, onDisposed: {
                print("AddItemToCart Disposed")
            }).disposed(by: disposeBag)
    }
    
}
