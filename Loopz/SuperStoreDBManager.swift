//
//  SuperStoreDBManager.swift
//  DelivX
//
//  Created by 3Embed on 27/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import GoogleMaps

class SuperStoreDBManager: NSObject {
    
    /// get Address
    ///
    /// - Parameter type: 1 for [String:Any] and 2 for Location
    /// - Returns: Array of cart
    func getSuperStoreDocument() -> [SuperStore] {
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.SuperStoreCouchDB)
        let doc: CBLDocument = CouchDBEvents.getDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        let array = (dic["Value"] as? [Any])!
        var dataArray = [SuperStore]()
        for item in array {
            let data = SuperStore.init(data: item as! [String:Any])     //
            dataArray.append(data)
        }
        return dataArray
    }
    
    // updates the address document with newly added address
    func updateSuperStoreDocument(data:[Any]) {
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.SuperStoreCouchDB)
        CouchDBEvents.updateDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!, documentArray: data as [AnyObject])
    }
}
