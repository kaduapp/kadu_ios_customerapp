//
//  AllWishListVM.swift
//  DelivX
//
//  Created by 3Embed on 04/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AllWishListVM: NSObject {
    
    var arrayOfWishList = PopupMenu.getWishList()
    let AllwishListVMResponse = PublishSubject<Int>()
    var selected:PopupModel = PopupModel.init(data: [:])
    let disposeBag = DisposeBag()
    /// delete WishList
    ///
    /// - Parameter index: selected index
    func deleteFavList(index: Int){
        FavouriteAPICalls.deleteWishList(listID: arrayOfWishList[index].ID).subscribe(onNext: {  success in
            Utility.updateWishList(data:[:], type: index)
            self.arrayOfWishList = PopupMenu.getWishList()
            self.AllwishListVMResponse.onNext(index)
            
        }, onError: {error in
        }).disposed(by: self.disposeBag)
    }
}
