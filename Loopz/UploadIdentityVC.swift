//
//  UploadIdentityVC.swift
//  UFly
//
//  Created by Rahul Sharma on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class UploadIdentityVC: UIViewController {
    
    @IBOutlet var uploadIDView: UploadIdentityView!
    
    let uploadIdVM = UploadIdentityVM()
    var uploadImageModel  = UploadImageModel.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        didupdate()
        uploadIdVM.pickerObj?.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Helper.transparentNavigation(controller: self.navigationController!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        Helper.nonTransparentNavigation(controller: self.navigationController!)
    }
    
    
    @IBAction func mmjAndIDBtnAction(_ sender: UIButton) {
        updateImage(sender: sender)
    }
    
    @IBAction func didTapDoneBtn(_ sender: Any) {
        uploadIdVM.updateProfile()
    }
    
    @IBAction func skipBtnAction(_ sender: Any) {
        Utility.UtilityUpdate.onNext(.Login)
        self.dismiss(animated: true, completion: nil)
        Helper.changetTabTo(index: AppConstants.Tabs.Home)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension UploadIdentityVC {
    
    /// observe for view model
    func didupdate(){
        uploadIdVM.UploadIdVMRespose.subscribe(onNext: {[weak self]success in
            self?.skipBtnAction(UIButton())
            }, onError: { error in
                print(error)
        }).disposed(by: uploadIdVM.disposeBag)
    }
    
    func didUpload(){
        uploadImageModel.UploadImageRespose.subscribe(onNext: { success in
            if success == .UploadIdentityVC {
                
            }
            
        }).disposed(by: uploadIdVM.disposeBag)
    }
    
}
