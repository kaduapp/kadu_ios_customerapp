//
//  HistoryViewController.swift
//  History
//
//  Created by 3Embed on 07/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class HistoryViewController: SegmentedViewController{
    
//    @IBAction func BackButtonACT(_ sender: UIBarButtonItem){
//        self.dismiss(animated: true, completion: nil)
//    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.segmentedScrollView.delegate = self
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.enableTheNavigationAttributtes(false)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        enableTheNavigationAttributtes(false)
    }
    
    func setupUI(){
        if let storyboard = self.storyboard{
            let headerViewController = storyboard
                .instantiateViewController(withIdentifier: "HeaderVC")
            
            let firstViewController = storyboard
                .instantiateViewController(withIdentifier: "HistoryTVC")
            firstViewController.title = "Active Order"
            
            let secondViewController = storyboard
                .instantiateViewController(withIdentifier: "HistoryTVC")
            secondViewController.title = "Past Order"
            
            self.headerViewController = headerViewController
            self.segmentControllers = [firstViewController,
                                       secondViewController]
            self.headerViewHeight = 70
            self.segmentTitleColor = UIColor(red: 133/255.0, green: 138/255.0, blue: 168/255.0, alpha: 1)
            self.segmentSelectedTitleColor =  UIColor(red: 36/255.0, green: 42/255.0, blue: 75/255.0, alpha: 1)
            self.selectedSegmentViewColor = UIColor.clear
            self.selectedSegmentViewBarColor = UIColor(red: 36/255.0, green: 42/255.0, blue: 75/255.0, alpha: 1)
            self.loadControllers()
        }
    }
    
    func enableTheNavigationAttributtes(_ boolean:Bool){
        var alphaValForTitle:CGFloat = 1
        
        if !boolean{
            alphaValForTitle = 0
        }
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: alphaValForTitle)]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
}

extension HistoryViewController:UIScrollViewDelegate{

    public func scrollViewDidScroll(_ scrollView: UIScrollView){
        if self.navigationController != nil{
            if scrollView.contentOffset.y > 35{
                self.enableTheNavigationAttributtes(true)
                Helper.addShadowToNavigationBar(controller: self.navigationController!)
            }
            else{
                self.enableTheNavigationAttributtes(false)
                Helper.removeShadowToNavigationBar(controller: self.navigationController!)
            }
        }
    }
}
