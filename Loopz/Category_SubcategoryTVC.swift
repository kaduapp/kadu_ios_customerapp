//
//  Category_SubcategoryTVC.swift
//  UFly
//
//  Created by 3Embed on 08/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Category_SubcategoryTVC: UITableViewCell {
    
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subCollectionView: UICollectionView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfSubcategory: NSLayoutConstraint!
    
    
    var controller:UIViewController? = nil
    var category:Category? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        if subCollectionView != nil {
            subCollectionView.delegate = self
            Helper.setShadow(sender: mainView)
            separator.backgroundColor = Colors.SeparatorLight.withAlphaComponent(0.3)
            setHeight(data: Category.init(data: [:]))
        }
        
        Fonts.setPrimaryRegular(descriptionLabel)
        descriptionLabel.textColor = Colors.SeconderyText
        
        titleLabel.textColor = Colors.PrimaryText
        Fonts.setPrimaryBold(titleLabel)
        
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(data:Category,cont:UIViewController?){
        setHeight(data:data)
        controller = cont
        category = data
        titleLabel.text = category?.Name
        descriptionLabel.text = category?.Desc
        if let tempImageUrl = category?.Image{
            if tempImageUrl.count > 0{
                imageWidth.constant = 70
            }else{
                imageWidth.constant = 0
            }
            
            Helper.setImage(imageView: mainImage, url: (category?.Image)!, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        }
        Helper.setImage(imageView: mainImage, url: (category?.Image)!, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        bannerImage.image = nil
        subCollectionView.reloadData()
    }
    
    func setHeight(data:Category) {
        category = data
        var height = 0
        topView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).withAlphaComponent(0.3)
        if (category?.selected)! {
            topView.backgroundColor = Colors.AppBaseColor.withAlphaComponent(0.3)
            let countItem = category?.SubCategories.count
            let cellSize = (UIScreen.main.bounds.width-70)/3
            var rows:Int = countItem!/3
            if countItem!%3 > 0 {
                rows += 1
            }
            height = Int(CGFloat(rows) * cellSize) + (rows+1) * 10
        }
        self.heightOfSubcategory.constant = CGFloat(height)
        self.updateConstraints()
        self.layoutIfNeeded()
    }
}
///UICollectionViewDelegate
extension Category_SubcategoryTVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (category?.SubCategories.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.SubCategoryCVC, for: indexPath) as! SubCategoryCVC
        if (category?.SubCategories.count)!-1 >= indexPath.row {
            let subCat = category?.SubCategories[indexPath.row]
            cell.setData(data:subCat!)
            cell.selectButton.tag = indexPath.row
            cell.selectButton.addTarget(self, action:#selector(Category_SubcategoryTVC.pressed) , for: UIControl.Event.touchUpInside)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        let width = (UIScreen.main.bounds.width-80)/3
        size.height = width
        size.width = width
        return size
    }
    
    @objc func pressed(sender : UIButton!) {
        let data = category?.SubCategories[sender.tag]
        let dataSend = [category as Any,data as Any] as [Any]
        controller?.performSegue(withIdentifier:UIConstants.SegueIds.HomeToItemList, sender: dataSend)
    }
}
