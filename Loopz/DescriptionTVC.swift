//
//  DescriptionTVC.swift
//  UFly
//
//  Created by 3 Embed on 25/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class DescriptionTVC: UITableViewCell {
    
    @IBOutlet weak var bottomButton: NSLayoutConstraint!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var discriptionDetail: UILabel!
    @IBOutlet weak var detailArrowBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /// initial View setup
    func initialSetup(){
         Fonts.setPrimaryRegular(discriptionDetail)
        
        separator.backgroundColor = Colors.SeparatorLarge
    
        discriptionDetail.textColor = Colors.SeconderyText
    }
    
    
    /// this method updates the discription
    ///
    /// - Parameters:
    ///   - data: it is of type item model object having discription property
    ///   - flag: true - shows detail discription false - short discription
    func updateCell(data: Item,flag: Bool){
        detailArrowBtn.isHidden = false
        if flag == true {
            detailArrowBtn.transform = CGAffineTransform.init(rotationAngle: CGFloat(-Double.pi/2))
            discriptionDetail.text = data.DetailedDescription
            discriptionDetail.numberOfLines = 0
            bottomButton.constant = 30
            self.updateConstraints()
            self.layoutIfNeeded()
        }
        else {
            detailArrowBtn.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi/2))
            discriptionDetail.text = data.ShortDescription
            discriptionDetail.numberOfLines = 2
            if data.DetailedDescription.length > 0 {
                bottomButton.constant = 30
            }else{
                discriptionDetail.numberOfLines = 0
                bottomButton.constant = 0
            }
            self.updateConstraints()
            self.layoutIfNeeded()
        }
        
    }
    
    func setIngredients(data: Item) {
        detailArrowBtn.isHidden = true
        discriptionDetail.text = data.Ingredients
        bottomButton.constant = 0
        self.updateConstraints()
        self.layoutIfNeeded()
    }
    
    func setDesclimer(data: String) {
        detailArrowBtn.isHidden = true
        discriptionDetail.text = data
        discriptionDetail.numberOfLines = 0
        bottomButton.constant = 0
        self.updateConstraints()
        self.layoutIfNeeded()
    }
}
