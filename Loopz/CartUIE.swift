//
//  CartUIE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension CartVC {
    
    
    func setBanner() {
        dummyView.addSubview(Helper.showUnVerifyNotification().0)
        dummyView.clipsToBounds = true
        if Helper.showUnVerifyNotification().1 {
            bannerHeight.constant = 20
        }else{
            bannerHeight.constant = 0
        }
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
    
    /// update the checkout bar number of stores and items
    func updateCheckOutBar(){
        checkoutBarView.amount.text = Helper.df2so(Double( AppConstants.TotalCartPriceWithTax))
    }
    
    /// initial view setup
    func initialSetup() {
        
        navView.titleLabel.alpha = 0
        Helper.setShadow(sender: bottomView)
        Fonts.setPrimaryMedium(pickupBtn)
        Fonts.setPrimaryMedium(deliveryBtn)
        Fonts.setPrimaryMedium(deliverType)
        Fonts.setPrimaryRegular(selectHow)
        mainCollectionView.backgroundColor =  Colors.ScreenBackground
        deliverType.text = StringConstants.DeliveryType()
        selectHow.text = StringConstants.SelectHow()
        
        deliverType.textColor  = Colors.PrimaryText
        selectHow.textColor =  Colors.SeconderyText
        Helper.setButtonTitle(normal: StringConstants.Delivery(), highlighted: StringConstants.Delivery(), selected: StringConstants.Delivery(), button: deliveryBtn)
        Helper.setButtonTitle(normal: StringConstants.Pickup(), highlighted: StringConstants.Pickup(), selected: StringConstants.Pickup(), button: pickupBtn)
        deliveryBtn.setTitleColor( Colors.SecondBaseColor, for: .normal)
        pickupBtn.setTitleColor( Colors.SecondBaseColor, for: .normal)
        
        Helper.setButton(button: pickupBtn,view:pickupView, primaryColour:  Colors.PickupBtn, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButton(button: deliveryBtn,view: deliveryView, primaryColour:  Colors.DeliveryBtn, seconderyColor: Colors.SecondBaseColor,shadow: true)
        
        Helper.removeNavigationSeparator(controller: self.navigationController!,image: false)
        
    }
    
    
    //Mark:- Keyboard ANimation
    override func keyboardWillShow(_ notification: NSNotification){
        let kbSize = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.size.height , right: 0.0)
        
        self.mainCollectionView.contentInset = contentInsets;
        self.mainCollectionView.scrollIndicatorInsets = contentInsets;
        
        
        // scroll to center
        if activeField != nil, let footer = mainCollectionView.layoutAttributesForSupplementaryElement(ofKind: UICollectionView.elementKindSectionFooter, at: IndexPath.init(item:0  , section:activeField.tag)){

        let topFooter = CGPoint.init(x: 0, y: footer.frame.origin.y - self.mainCollectionView.center.y )
          self.mainCollectionView.setContentOffset(topFooter, animated: true)
        }

    }
    
    override func keyboardWillHide(_ notification: NSNotification){
        self.mainCollectionView.contentInset = UIEdgeInsets.zero;
        self.mainCollectionView.scrollIndicatorInsets = UIEdgeInsets.zero;
    }
    
    ///shows checkout bar if cart is not empty
    func showCheckoutView(){
        var length = 0
        if cartVM.arrayOfCart.count > 0 {
            length = 155
            //checkoutBtn.isHidden = false
        }
        else{
            length = 0
            //checkoutBtn.isHidden = true
        }
        UIView.animate(withDuration: 0.15) {
            self.checkoutTopConstarint.constant = CGFloat(length)
            self.view.layoutIfNeeded()
        }
    }
    
    func setEmptyScreen() {
        emptyView.setData(image: #imageLiteral(resourceName: "EmptyCart"), title: StringConstants.EmptyCart(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        emptyView.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
    }
    
    @objc func StartShopping() {
        Helper.changetTabTo(index: AppConstants.Tabs.Home)
    }
}
