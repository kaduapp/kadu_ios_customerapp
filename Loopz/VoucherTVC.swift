//
//  VoucherTVC.swift
//  DelivX
//
//  Created by 3Embed on 11/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class VoucherTVC: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var separator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.Semantic()
        // Initialization code
        
        separator.backgroundColor = Colors.SeparatorLight
        Fonts.setPrimaryRegular(textField)
        textField.placeholder = StringConstants.VouchercodeMissing()
    }

}
