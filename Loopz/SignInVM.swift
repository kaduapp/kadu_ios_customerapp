//
//  SignInVM.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SignInVM: NSObject {
    
    // avriables
    var authentication = Authentication()
    let disposeBag = DisposeBag()
    var willClose = false
    var showAlert = true
    let loginVM_response = PublishSubject<ResponceType>()
    
    /// enum named ResponseType it is of type Integer
    ///
    /// - DismiissKeyBoard: having value 1 for dismissKeyBoard
    /// - SaveCreds: having value 2 for savingCredentials
    /// - GotoOTP: having value 3 for to go for OTP
    /// - GotoSignup: having value 4 for to go to signup
    enum ResponceType: Int {
        case SaveCreds          = 2
        case GotoOTP            = 3
        case GotoSignup         = 4
        case True               = 5
    }
  
    /// subscription for login API defined in AuthenticationAPICalls ,observe the response
    func gotoLogin() {
        AuthenticationAPICalls.loginAPI(user: self.authentication).subscribe(onNext: {[weak self]success in
            if success == true {
                self?.loginVM_response.onNext(ResponceType.SaveCreds)
            }else{
                self?.loginVM_response.onNext(ResponceType.GotoOTP)
            }
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    
    /// validates phone number
    func validatePhone() {
        Helper.showPI(string: StringConstants.Loading())
        AuthenticationAPICalls.validatePhoneEmail(user: authentication,verifyType: 1,showPop:false).subscribe(onNext: { [weak self]success in
            if success.0 == false {
                self?.authentication.ValidPhone = true
                Helper.hidePI()
                self?.loginVM_response.onNext(ResponceType.True)
                self?.touchIDLoginAction()
            }else{
                self?.authentication.ValidPhone = false
                self?.loginVM_response.onNext(ResponceType.GotoSignup)
            }
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    /// this method is to access the key chain to get user credential
    func touchIDLoginAction() {
        Helper.AuthenticateCredsToKeyChain(data: authentication) { (data) in
            self.authentication = data
            self.gotoLogin()
           // self.checkForData()
        }
    }
    
}
