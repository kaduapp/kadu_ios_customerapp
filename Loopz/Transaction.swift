//
//  Transaction.swift
//  DelivX
//
//  Created by 3Embed on 13/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation


struct Transaction {
    
    
    var Amount:Float = 0
    var Comment = ""
    var DateTxn = Date()
    var Title = ""
    var Id = ""
    var TypeTxn:Int = 0   //0 Credit 1 Debit
    init(data:[String:Any]) {
        if let temp = data[APIResponceParams.Amount] as? String {
            Amount = Float(temp)!
        }
        if let temp = data[APIResponceParams.Amount] as? Float {
            Amount = temp
        }
        if let temp = data[APIResponceParams.Comment] as? String {
            Comment = temp
        }
        if let temp = data[APIResponceParams.TxnDate] as? NSNumber {
            DateTxn = Date(timeIntervalSince1970: TimeInterval(truncating: temp))
        }
        if let temp = data[APIResponceParams.PaymentTypeText] as? String {
            Title = temp
        }
        if let temp = data[APIResponceParams.TxnId] as? String {
            Id = temp
        }
        if let temp = data[APIResponceParams.TxnType] as? String {
            if temp.lowercased() == "CREDIT".lowercased() {
                TypeTxn = 0
            }else{
                TypeTxn = 1
            }
        }
        
        
    }
    
}
