
//
//  SegmentedViewController.swift
//  History
//
//  Created by 3Embed on 04/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

@objc public protocol SegmentedViewControllerDelegate
{
    
    @objc optional func didSelectSegmentAtIndex(_ index:Int)
    
    @objc optional func didMoveToPage(_ controller: UIViewController, segment: SegmentTab?, index: Int)
}

@objc public protocol SegmentedViewControllerViewSource
{
    
    @objc optional func viewForSegmentControllerToObserveContentOffsetChange() -> UIView
}


@objc open class SegmentedViewController: UIViewController
{
    
    open var headerViewHeight: CGFloat = 0.0
        {
        didSet
        {
            segmentedScrollView.headerViewHeight = headerViewHeight
        }
    }
    
    open var segmentViewHeight: CGFloat = 40.0
        {
        didSet {
            segmentedScrollView.segmentViewHeight = segmentViewHeight
        }
    }
    
    open var headerViewOffsetHeight: CGFloat = 0.0
        {
        didSet {
            segmentedScrollView.headerViewOffsetHeight = headerViewOffsetHeight
        }
    }
    
    open var selectedSegmentViewColor = UIColor.lightGray {
        didSet {
            segmentedScrollView.selectedSegmentViewColor = selectedSegmentViewColor
        }
    }
    open var selectedSegmentViewBarColor = UIColor.black {
        didSet {
            segmentedScrollView.selectedSegmentViewBarColor = selectedSegmentViewBarColor
        }
    }
    
    open var selectedSegmentViewHeight: CGFloat = 5.0 {
        didSet {
            segmentedScrollView.selectedSegmentViewHeight = selectedSegmentViewHeight
        }
    }
    
    open var segmentTitleColor = UIColor.darkGray {
        didSet {
            segmentedScrollView.segmentTitleColor = segmentTitleColor
        }
    }
    
    open var segmentSelectedTitleColor = UIColor.black {
        didSet {
            segmentedScrollView.segmentSelectedTitleColor = segmentSelectedTitleColor
        }
    }
    
    open var segmentBackgroundColor = UIColor.white {
        didSet {
            segmentedScrollView.segmentBackgroundColor = segmentBackgroundColor
        }
    }
    
    
    open var segmentTitleFont = UIFont.systemFont(ofSize: 14.0) {
        didSet {
            segmentedScrollView.segmentTitleFont = segmentTitleFont
        }
    }
    
    open var segmentBounces = true {
        didSet {
            segmentedScrollView.segmentBounces = segmentBounces
        }
    }
    
    open var headerViewController: UIViewController? {
        didSet {
            setDefaultValuesToSegmentedScrollView()
        }
    }
    
    open var segmentControllers = [UIViewController]() {
        didSet {
            setDefaultValuesToSegmentedScrollView()
        }
    }
    
    open var segments: [SegmentTab] {
        get {
            
            if let segmentView = segmentedScrollView.segmentView {
                return segmentView.segments
            }
            
            return [SegmentTab]()
        }
    }
    
    open var segmentedScrollViewColor = UIColor.white  {
        didSet {
            segmentedScrollView.backgroundColor = segmentedScrollViewColor
        }
    }
    
    open var showsVerticalScrollIndicator = true {
        didSet {
            segmentedScrollView.sjShowsVerticalScrollIndicator = showsVerticalScrollIndicator
        }
    }
    
    open var showsHorizontalScrollIndicator = true {
        didSet {
            segmentedScrollView.sjShowsHorizontalScrollIndicator = showsHorizontalScrollIndicator
        }
    }
    
    open var disableScrollOnContentView: Bool = false {
        didSet {
            segmentedScrollView.sjDisableScrollOnContentView = disableScrollOnContentView
        }
    }
    
    open weak var delegate:SegmentedViewControllerDelegate?
    var segmentedScrollView = SegmentedScrollView(frame: CGRect.zero)
    var segmentScrollViewTopConstraint: NSLayoutConstraint?
    
    required public init(headerViewController: UIViewController?,
                         segmentControllers: [UIViewController]) {
        super.init(nibName: nil, bundle: nil)
        self.headerViewController = headerViewController
        self.segmentControllers = segmentControllers
        setDefaultValuesToSegmentedScrollView()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required public init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override open func loadView()
    {
        super.loadView()
        
        addSegmentedScrollView()
    }
    
    override open func viewDidLoad()
    {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        loadControllers()
        if #available(iOS 11, *)
        {
            segmentedScrollView.contentInsetAdjustmentBehavior = .never
        }
        else
        {
            automaticallyAdjustsScrollViewInsets = false
        }
        
    }


    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let topSpacing = Util.getTopSpacing(self)
        segmentedScrollView.topSpacing = topSpacing
        segmentedScrollView.bottomSpacing = Util.getBottomSpacing(self)
        segmentScrollViewTopConstraint?.constant = topSpacing
        segmentedScrollView.updateSubviewsFrame(view.bounds)
    }

    open func setSelectedSegmentAt(_ index: Int, animated: Bool)
    {
        
        if index >= 0 && index < segmentControllers.count
        {
            segmentedScrollView.segmentView?.didSelectSegmentAtIndex!(segments[index],
                                                                      index,
                                                                      animated)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "DidChangeSegmentIndex"),
                                            object: index)
        }
    }

    func setDefaultValuesToSegmentedScrollView()
    {
        
        segmentedScrollView.selectedSegmentViewColor    = selectedSegmentViewColor
        segmentedScrollView.selectedSegmentViewBarColor = selectedSegmentViewBarColor
        segmentedScrollView.selectedSegmentViewHeight   = selectedSegmentViewHeight
        segmentedScrollView.segmentTitleColor           = segmentTitleColor
        segmentedScrollView.segmentSelectedTitleColor   = segmentSelectedTitleColor
        segmentedScrollView.segmentBackgroundColor      = segmentBackgroundColor
        segmentedScrollView.segmentTitleFont            = segmentTitleFont
        segmentedScrollView.segmentBounces              = segmentBounces
        segmentedScrollView.headerViewHeight            = headerViewHeight
        segmentedScrollView.headerViewOffsetHeight      = headerViewOffsetHeight
        segmentedScrollView.segmentViewHeight           = segmentViewHeight
        segmentedScrollView.backgroundColor             = segmentedScrollViewColor
        segmentedScrollView.sjDisableScrollOnContentView = disableScrollOnContentView
    }


    func addSegmentedScrollView()
    {
        
        let topSpacing = Util.getTopSpacing(self)
        segmentedScrollView.topSpacing = topSpacing
        
        let bottomSpacing = Util.getBottomSpacing(self)
        segmentedScrollView.bottomSpacing = bottomSpacing
        
        view.addSubview(segmentedScrollView)
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[scrollView]-0-|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: ["scrollView": segmentedScrollView])
        view.addConstraints(horizontalConstraints)
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:[scrollView]-bp-|",
                                                                 options: [],
                                                                 metrics: ["tp": topSpacing,
                                                                           "bp": bottomSpacing],
                                                                 views: ["scrollView": segmentedScrollView])
        view.addConstraints(verticalConstraints)
        
        segmentScrollViewTopConstraint = NSLayoutConstraint(item: segmentedScrollView,
                                                            attribute: .top,
                                                            relatedBy: .equal,
                                                            toItem: view,
                                                            attribute: .top,
                                                            multiplier: 1.0,
                                                            constant: topSpacing)
        view.addConstraint(segmentScrollViewTopConstraint!)
        
        segmentedScrollView.setContentView()
        
        // selected segment at index
        segmentedScrollView.didSelectSegmentAtIndex = {[unowned self] (segment, index, animated) in
            
            let selectedController = self.segmentControllers[index]
            self.delegate?.didMoveToPage?(selectedController, segment: segment!, index: index)
        }
    }

    func addHeaderViewController(_ headerViewController: UIViewController)
    {
        
        addChild(headerViewController)
        segmentedScrollView.addHeaderView(headerViewController.view)
        headerViewController.didMove(toParent: self)
    }

    func addContentControllers(_ contentControllers: [UIViewController])
    {
        segmentedScrollView.addSegmentView(contentControllers, frame: view.bounds)
        
        var index = 0
        for controller in contentControllers{
            addChild(controller)
            segmentedScrollView.addContentView(controller.view, frame: view.bounds)
            controller.didMove(toParent: self)
            
            let delegate = controller as? SegmentedViewControllerViewSource
            var observeView = controller.view
            
            if let collectionController = controller as? UICollectionViewController {
                observeView = collectionController.collectionView
            }
            
            if let view = delegate?.viewForSegmentControllerToObserveContentOffsetChange?() {
                observeView = view
            }
            
            segmentedScrollView.addObserverFor(observeView!)
            index += 1
        }
        
        segmentedScrollView.segmentView?.contentView = segmentedScrollView.contentView
    }

    func loadControllers() {
        
        if headerViewController == nil
        {
            headerViewController = UIViewController()
            headerViewHeight = 0.0
        }
        
        addHeaderViewController(headerViewController!)
        addContentControllers(segmentControllers)
        
        //Delegate call for setting the first view of segments.
        var segment: SegmentTab?
        if segments.count > 0
        {
            segment = segments[0]
        }
        
        delegate?.didMoveToPage?(segmentControllers[0],
                                 segment: segment,
                                 index: 0)
    }

}
