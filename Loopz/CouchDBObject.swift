//
//  CouchDBObject.swift
//  Rogi
//
//  Created by NABEEL on 14/06/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CouchDBObject: NSObject {

    var database : CBLDatabase! = nil
    var manager : CBLManager! = nil
    static let sharedInstance = CouchDBObject()
    
    override init() {
        super.init()
        self.manager = CBLManager.sharedInstance()
        if !(self.manager != nil) {
            return
        }
        do {
            self.database = try self.manager.databaseNamed(DataBase.MYDB)
        }
        catch {
        }
        if !(self.database != nil) {
            return
        }
    }
    
}
