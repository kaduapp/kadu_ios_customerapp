//
//  Cart.swift
//  UFly
//
//  Created by 3Embed on 21/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Cart: NSObject {
    var StoreId = ""
    var StoreName = ""
    var StoreAreaName = ""
    var StoreLogo = ""
    var TotalPrice:Float = 0
    var CartDiscount:Float = 0
    var DeliveryFee:Float = 0
    var StoreLat:Double = 0
    var StoreLong:Double = 0
    var convenienceFee:Int = 0
    var convenienceFeeType = ""
    var StoreAddress = ""
    var Products = [CartItem]()
    var Dictionary = [String:Any]()
    var TotalWithTax:Float = 0
    var ExclusiveTaxes:[Any] = []
    var MinimumOrder:Double = 0
    var FreeDelivery:Double = 0
    var cartId = ""
    var StoreIsOpen  = false
     var storeType :Int = 2

    
    // Cart Allowed 1 :  Single Cart Single Store
    // Cart Allowed 2 :  Single Cart Multiple store
    // Cart Allowed 3 :  Single Cart Multiple Store Category
    var cartType:AppConstants.TypeOfCart = .SingleCartSingleStore

    
    
    init(data:[String:Any]) {
        super.init()
        if let titleTemp = data["storeType"] as? Int{
            storeType     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.convenienceFee] as? Double
        {
            convenienceFee =  Int(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.convenienceFeeType] as? Int
        {
            convenienceFeeType =  "\(titleTemp)"
        }
        if let titleTemp = data[APIResponceParams.CartStoreID] as? String{
            StoreId     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreName] as? String{
            StoreName    = titleTemp
        }
        if let titleTemp = data["areaName"] as? String{
            StoreAreaName    = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.CartStoreLogo] as? String{
            StoreLogo           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreLat] as? Float{
            StoreLat    = Double(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.CartStoreLong] as? Float{
            StoreLong           = Double(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.CartStoreLat] as? String{
            StoreLat    = Double(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.CartStoreLong] as? String{
            StoreLong           = Double(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.CartStoreLat] as? Double{
            StoreLat    = Double(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.CartStoreLong] as? Double{
            StoreLong           = Double(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.CartStoreTotal] as? Float{
            TotalPrice    = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.cartDiscount] as? Float{
            CartDiscount    = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.CartStoreDelFee] as? Float{
            DeliveryFee    = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartStoreTotal] as? Double{
            TotalPrice    = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.CartStoreDelFee] as? Double{
            DeliveryFee    = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.CartStoreAddress] as? String{
            StoreAddress    = titleTemp
        }
        if let titleTemp = data[APIResponceParams.StoreTotalPriceWithExcTaxes] as? Float{
            TotalWithTax    = titleTemp
        }
        if let titleTemp = data[APIResponceParams.StoreTotalPriceWithExcTaxes] as? Double{
            TotalWithTax    = Float(titleTemp)
        }
        ExclusiveTaxes.removeAll()
        if let titleTemp = data[APIResponceParams.ExclusiveTaxes] as? [Any] {
            ExclusiveTaxes           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Products] as? [Any]{
            for data in titleTemp {
                let cartItemData = CartItem.init(data: data as! [String:Any])
                cartItemData.StoreName = StoreName
                cartItemData.StoreId = StoreId
                cartItemData.StoreImage = StoreLogo
                cartItemData.StoreLat = StoreLat
                cartItemData.StoreLong = StoreLong
                cartItemData.StoreAddress = StoreAddress
                cartItemData.setDict()
                Products.append(cartItemData)
            }
        }
        if let titleTemp = data[APIResponceParams.FreeDeliveryAbove] as? Double{
            FreeDelivery    = titleTemp
        }
        if let titleTemp = data[APIResponceParams.MinimumOrder] as? Double{
            MinimumOrder    = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FreeDeliveryAbove] as? String{
            if titleTemp.length > 0 {
                
                var result = titleTemp.replacingOccurrences(of: ",", with: "")
                result = result.trimmingCharacters(in: .whitespaces)
                FreeDelivery    = Double(result)!
            }
        }
        if let titleTemp = data[APIResponceParams.MinimumOrder] as? String{
            if titleTemp.length > 0 {
                MinimumOrder    = Double(titleTemp)!
            }
        }
        
        if let cartTypeTemp = data[APIResponceParams.StoreCartType] as? Int{
            if storeType == 1{
                cartType = .SingleCartSingleStore
            }
            else{
                switch Int(cartTypeTemp) {
                case 1:
                    cartType = .SingleCartSingleStore
                    break
                case 3:
                    cartType = .SingleCartMulStoreCat
                    break
                default:
                    cartType = .SingleCartMulStore
                    break
                }
            }
        }
        if let titleTemp = data["cartId"] as? String{
            cartId    = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.StoreIsOpen] as? Bool{
            StoreIsOpen = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.StoreIsOpen] as? Int{
            if titleTemp == 0{
                StoreIsOpen = false
            }else{
                StoreIsOpen = true
            }
        }
        
        setUpDict()
        
    }
    
    init(item:CartItem) {
        super.init()
        StoreId = item.StoreId
        StoreName = item.StoreName
        StoreLogo = item.StoreImage
        StoreLong = item.StoreLat
        StoreLat = item.StoreLong
        StoreAddress = item.StoreAddress
        TotalPrice = item.FinalPrice * Float(item.QTY)
        Products = [item]
        setUpDict()
    }
   
    func setUpDict() {
        var dataProductArray = [Any]()
        for data in Products {
            data.setDict()
            if data.QTY != 0{
                dataProductArray.append(data.DictionaryOfCart)
            }
        }
        Dictionary = [
            APIResponceParams.CartStoreID                   :   StoreId,
            APIResponceParams.CartStoreName                 :   StoreName,
            APIResponceParams.CartStoreLogo                 :   StoreLogo,
            APIResponceParams.CartStoreTotal                :   TotalPrice,
            APIResponceParams.CartStoreLat                  :   StoreLat,
            APIResponceParams.CartStoreLong                 :   StoreLong,
            APIResponceParams.CartStoreDelFee               :   DeliveryFee,
            APIResponceParams.Products                      :   dataProductArray,
            APIResponceParams.CartStoreAddress              :   StoreAddress,
            APIResponceParams.StoreTotalPriceWithExcTaxes   :   TotalWithTax,
            APIResponceParams.ExclusiveTaxes                :   ExclusiveTaxes,
            APIResponceParams.MinimumOrder   :   MinimumOrder,
            APIResponceParams.FreeDeliveryAbove                :   FreeDelivery
        ]
        
    }
}
