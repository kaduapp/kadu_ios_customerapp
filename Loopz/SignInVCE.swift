//
//  SignInVCE.swift
//  UFly
//
//  Created by 3 Embed on 11/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - VNHCountryPickerDelegate
extension SignInVC: VNHCountryPickerDelegate {
    
    func didPick(country: VNHCounty) {
        signInView.phoneNumTF.text = country.dialCode
        signinVM.authentication.CountryCode = country.dialCode
    }
    
}

extension SignInVC {
    
//
//    @objc func viewMoveAccKeyboardInFilter(notification:NSNotification){
//        let kbSize = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
//        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.size.height, right: 0.0)
//        self.signInView.scrollView.contentInset = contentInsets;
//        self.signInView.scrollView.scrollIndicatorInsets = contentInsets;
//    }
//
//    @objc  func viewMoveToOriginalInFilter(){
//        self.signInView.scrollView.contentInset = UIEdgeInsets.zero;
//        self.signInView.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero;
//    }
    
    /// Dismiss Keyboard
    func dismissKeyboard(){

        if signInView.phoneNumTF.text?.sorted().count == signinVM.authentication.CountryCode.sorted().count{
            signInView.phoneNumTF.text = ""
            signinVM.authentication.Phone = ""
            signInView.phSeperartor.backgroundColor =  Colors.SeparatorLight
        }
        if signInView.activeTextField == nil {
            checkForData()
        }
        signInView.activeTextField = nil
    }
    
    //Subscribed Data from VM
    func subscribedData(data:SignInVM.ResponceType) {
        switch data {
        case .True:
            signInView.phNumTickMark.isHidden = false
            showHiddenField()
            signInView.passwordTF.becomeFirstResponder()
            let PhoneNumber = (signInView.phoneNumTF.text?.replacingOccurrences(of: signinVM.authentication.CountryCode, with: ""))!
            signinVM.authentication.Phone = PhoneNumber
            
        case .SaveCreds:
            Helper.SaveCredsToKeyChain(controller:self,key:self.signinVM.authentication){(flag) in
                self.dismiss(animated: true, completion: nil)
            }
            break
        case .GotoOTP:
            performSegue(withIdentifier: UIConstants.SegueIds.SigninToOTP, sender: self)
            break
        case .GotoSignup:
            //performSegue(withIdentifier: UIConstants.SegueIds.SigninToSignup,sender: self)
            self.gotoSignup()
            break
        }
    }
    
    
    /// this method validates the phone number and calls login method defined in signinVM
    func checkForData() {
        let PhoneNumber = (signInView.phoneNumTF.text?.replacingOccurrences(of: signinVM.authentication.CountryCode, with: ""))
        if PhoneNumber?.sorted().count == 0 {
            signInView.phoneNumTF.resignFirstResponder()
            return
        }
        if signinVM.authentication.ValidPhone == false && signInView.phoneNumTF.text?.sorted().count != signinVM.authentication.CountryCode.count {
            if Helper.isPhoneNumValid(phoneNumber: PhoneNumber!, code: self.signinVM.authentication.CountryCode) {
                signinVM.authentication.Phone = PhoneNumber!
                signinVM.validatePhone()
            }
            else{
                if signInView.phoneNumTF.text?.sorted().count != 0 {
                    Helper.showAlert(message: StringConstants.ValidPhoneNumber() , head: StringConstants.Error(), type: 1)
                }
            }
        }else{
            signinVM.authentication.Password = signInView.passwordTF.text!
            guard (signinVM.authentication.Password.sorted().count) >= 6 else {
                if signinVM.showAlert == true {
                    Helper.showAlert(message: StringConstants.PasswordMissing(), head: StringConstants.Error(), type: 1)
                }
                return
            }
            signinVM.gotoLogin()
        }
        
    }
    /// shows the password field for registered user
    func showHiddenField(){
        Helper.setButton(button: signInView.continueBtn,view:signInView.continueBtnContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
        signInView.continueBtn.isUserInteractionEnabled = false
        let length = 75
        UIView.animate(withDuration: 0.50) {
            self.signInView.passwordTFHight.constant = CGFloat(length)
            self.view.layoutIfNeeded()
        }
    }
    
    
    //Mark:- Keyboard ANimation
    override func keyboardWillShow(_ notification: NSNotification){
        // Do something here
        let info = notification.userInfo!
        let inputViewFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let screenSize: CGRect = UIScreen.main.bounds
        var frame = self.view.frame
        frame.size.height = screenSize.height - inputViewFrame.size.height
    }
    
    override func keyboardWillHide(_ notification: NSNotification){
         

    }
    
    
    
}

// MARK: - UIScrollViewDelegate
extension SignInVC: UIScrollViewDelegate {
    
//    func scrollViewDidScroll(_ sender: UIScrollView) {
//        // Update the page when more than 50% of the previous/next page is visible
//        let movedOffset: CGFloat = sender.contentOffset.y
//        print(movedOffset)
//        if sender == signInView.scrollView {
//            if movedOffset <= 0 {
//                sender.contentOffset.y = 0
//            }
//            let pageWidth: CGFloat = 200 - 88            //171 - 64
//            let ratio: CGFloat = (movedOffset) / (pageWidth)
//            
//            if ratio >= 1 {
//                navigationBackView.isHidden = false
//                navigationBackView.backgroundColor =  Colors.SecondBaseColor
//                navTitleText.textColor =  Colors.PrimaryText
//                closeBtn.tintColor =  Colors.PrimaryText
//            }else {
//                navigationBackView.isHidden = true
//                closeBtn.tintColor =  Colors.SecondBaseColor
//            }
//            
//        }
//    }
}











