//
//  StoresListVC.swift
//  DelivX
//
//  Created by 3Embed on 28/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class StoresListVC: UIViewController{
    
    @IBOutlet weak var headerDottedViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var offerCollectionBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var offerCollectionTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var OfferCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var offerCollectionView: UICollectionView!
    @IBOutlet weak var filterWidth: NSLayoutConstraint!
    @IBOutlet weak var searchbarINStore: UISearchBar!
    @IBOutlet weak var searchBarButton: UIButton!
    @IBOutlet weak var filterBarButton: UIButton!
    @IBOutlet weak var closeBarButton: UIButton!
    @IBOutlet weak var navTitle: UILabel!

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var titleLBHeaderView: UILabel!
    @IBOutlet weak var searchBTNHeaderView: UIButton!
    @IBOutlet weak var filterBTNHeaderView: UIButton!
    @IBOutlet weak var viewForDottedLine: UIView!
    @IBOutlet weak var cuastomLeftBarView: UIView!
    var noStoresView = EmptyView().shared
    let backButton = UIButton_RotateButtonClass()
    let navTitleLabel = AlignmentOfLabel.init()
    var viewAllFavorites = false
    var restaurantVM = ResturantsVM()
    var searchVM = SearchVM()
    var filterDetailModel = FilterDetailVM()
    
    var isNewStoreSelectedCallback : ((Bool) -> ()) = {
        arg in
        
    }
    
    @IBAction func searchAction(_ sender: UIButton){
        //Helper.changetTabTo(index: AppConstants.Tabs.Search)
        //searchbarINStore.becomeFirstResponder()
        performSegue(withIdentifier: String(describing: SearchStoreVC.self), sender: nil)
    }
    
    
    @IBAction func viewAllButtonAction(_ sender: Any) {
        
        let viewAllVC = self.storyboard?.instantiateViewController(withIdentifier: "StoresListVC") as! StoresListVC
        viewAllVC.viewAllFavorites = true
        self.navigationController?.pushViewController(viewAllVC, animated: true)
    }
    
    
    @IBAction func filterACT(_ sender: UIButton){
        self.performSegue(withIdentifier: String(describing: FilterContainerVC.self), sender: self)
    }
    
    
    @objc func close (){
        if viewAllFavorites || Changebles.isGrocerOnly{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.tabBarController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        
        if viewAllFavorites || Changebles.isGrocerOnly{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.tabBarController?.dismiss(animated: true, completion: nil)
        }
    }
    override func viewDidLoad(){
        super.viewDidLoad()
    
        let navView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 130, height: 40))
        navView.backgroundColor = UIColor.clear
        navTitleLabel.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width - 220, height: 40)
        navTitleLabel.backgroundColor = UIColor.clear
        navTitleLabel.textColor = UIColor.black
        navTitleLabel.font =  UIFont.systemFont(ofSize: 17)
        
        if RTL.shared.isRTL
        {
        backButton.frame = CGRect(x: navView.frame.size.width - 30, y: 0, width: 40, height: 40)
        backButton.rotateButton()

        }else
        {
            backButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        }
        
        if viewAllFavorites || Changebles.isGrocerOnly{
         backButton.setImage(#imageLiteral(resourceName: "BackIcon"), for: .normal)
        }
        else{
          backButton.setImage(#imageLiteral(resourceName: "CloseIcon"), for: .normal)
        }
        backButton.addTarget(self, action: #selector(close), for:    .touchUpInside)
        navView.addSubview( backButton)
        navView.addSubview(navTitleLabel)
        
        let leftBarbuttonItem = UIBarButtonItem(customView: navView)
        let searchButtonItem = UIBarButtonItem(customView: self.searchBarButton)
        let filterButtonItem = UIBarButtonItem(customView: self.filterBarButton)
        self.navigationItem.leftBarButtonItem = leftBarbuttonItem
        self.navigationItem.rightBarButtonItems = [searchButtonItem,filterButtonItem]
        
        if Utility.getSelectedSuperStores().type == .Restaurant {
            self.filterBarButton.isHidden = false
            self.filterBTNHeaderView.isHidden = false
        }
        if viewAllFavorites {
        self.filterBarButton.isHidden = true
        self.searchBarButton.isHidden = true
        self.filterBTNHeaderView.isHidden = true
         restaurantVM.getAllFavoriteStore()
        }
        else{
        restaurantVM.getData()
        }
        setLayoutWhenOffer(OfferAvaiable: false)
        setVM()
        setObserver()
        self.title = "   "
        Helper.drawDottedLine(viewForDottedLine)
        titleLBHeaderView.textColor = Colors.PrimaryText
        Fonts.setPrimaryBold(titleLBHeaderView, size: 25)
        
        if Changebles.isGrocerOnly{
        navTitleLabel.text = "Stores"
        titleLBHeaderView.text = "Stores"
        }
        else{
        navTitleLabel.text = Utility.getSelectedSuperStores().categoryName
        titleLBHeaderView.text = Utility.getSelectedSuperStores().categoryName
        }
        
        
        self.headerView.reloadInputViews()
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        setTheAlphaToNavigationViewsOnScroll(0)
        
        if Utility.getSelectedSuperStores().type != .Restaurant {
            filterWidth.constant = 0
             self.filterBarButton.isHidden = true
            filterBTNHeaderView.isHidden = true
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(true)
        if (self.navigationController?.viewControllers.count)! <= 1 {
            self.tabBarController?.tabBar.isHidden = false
        }
        if Utility.getSelectedSuperStores().type == .Restaurant {
            Utility.saveStore(location: [:])
        }
        
        if !Changebles.isGrocerOnly && !viewAllFavorites {
        Helper.PullToClose(scrollView: mainTableView)
        Helper.pullToDismiss?.delegate = self
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let headerView = mainTableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            //Comparison necessary to avoid infinite loop
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                mainTableView.tableHeaderView = headerView
            }
        }
    }
    
    func setVM(){
        restaurantVM.response_ResturantsVM.subscribe(onNext: { [weak self]data in
            self?.searchBTNHeaderView.isHidden = false
            if self?.restaurantVM.arrayOfOffer.count == 0{
                self?.setLayoutWhenOffer(OfferAvaiable: false)
            }else{
                self?.setLayoutWhenOffer(OfferAvaiable: true)
               self?.offerCollectionView.reloadData()
            }
            if self?.restaurantVM.arrayOfResturants.count == 0  && self?.restaurantVM.arrayOfFavorites.count == 0{
                self?.mainTableView.backgroundView = self?.noStoresView
                self?.noStoresView.setData(image: #imageLiteral(resourceName: "EmptyStore"), title: StringConstants.EmptyStore(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
                self?.searchBTNHeaderView.isHidden = true
                self?.filterBTNHeaderView.isHidden = true
                
            }else{
                
                self?.mainTableView.reloadData()
            }
            
        }).disposed(by: self.restaurantVM.disposeBag)
    }
    
    func setObserver() {
        searchVM.filterInfo.subscribe(onNext: { [weak self]data in
            if self != nil {
                    SearchAPICalls.filterStore(needle: "", cousine: (self?.filterDetailModel.selectedCuisines)!, sort:(self?.filterDetailModel.selectedSort)!, foodType:(self?.filterDetailModel.arrayOfFoodType)! , language: Utility.getLanguage().Code).subscribe(onNext: { [weak self]data in
                        self?.restaurantVM.arrayOfResturants.removeAll()
                        self?.restaurantVM.arrayOfResturants.append(contentsOf: data)
    
                        if self?.restaurantVM.arrayOfResturants.count == 0 && !self!.searchVM.isClearAll{
                            self?.setLayoutWhenOffer(OfferAvaiable: false)
                            self?.mainTableView.backgroundView = self?.noStoresView
                            self?.noStoresView.setData(image: #imageLiteral(resourceName: "EmptySearch"), title: StringConstants.noStoresFound(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
                        }else  {
                         self?.mainTableView.backgroundView = nil
                        }
    
                        self?.mainTableView.reloadData()
                    }).disposed(by: self!.restaurantVM.disposeBag)
            }
        }).disposed(by: self.searchVM.disposeBag)
    }

    
    func setLayoutWhenOffer(OfferAvaiable: Bool){
        print("height",self.view.frame.size.height/3)
        print("width", (self.view.frame.width/2 + self.view.frame.width/4))
        if OfferAvaiable{
            self.OfferCollectionViewHeight.constant =  180 //self.view.frame.size.height/3
            self.headerView.frame.size.height = self.headerView.frame.size.height +      self.OfferCollectionViewHeight.constant
            self.headerDottedViewHeightConstraint.constant = 0
            self.offerCollectionTopConstraint.constant = 7
            self.offerCollectionBottomConstraints.constant = 7
        }else{
            self.OfferCollectionViewHeight.constant = 0 //self.view.frame.size.height/3
           self.headerView.frame.size.height =     self.headerView.frame.size.height
            self.headerDottedViewHeightConstraint.constant = 2
            self.offerCollectionTopConstraint.constant = 0
            self.offerCollectionBottomConstraints.constant = 0
        }
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: FilterContainerVC.self) {
            if let itemDetailVC: FilterContainerVC = segue.destination as? FilterContainerVC {
                itemDetailVC.storeFilter = true
                itemDetailVC.searchVM = self.searchVM
                itemDetailVC.filterDetailRef = self.filterDetailModel
            }
        }else if segue.identifier == String(describing: ViewAllVC.self) {
            if let viewAllVC: ViewAllVC =  segue.destination as? ViewAllVC {
                let storeNew = Store(data: [APIResponceParams.RestuarantsStoreId : self.restaurantVM.arrayOfOffer[sender as! Int].StoreId])
                print(storeNew)
                viewAllVC.viewAllVM.store = storeNew
                viewAllVC.viewAllVM.arrayOfItems = []
                viewAllVC.viewAllVM.type = 2
                viewAllVC.offerId = self.restaurantVM.arrayOfOffer[sender as! Int].Id
                viewAllVC.viewAllVM.titleLabel = self.restaurantVM.arrayOfOffer[sender as! Int].Name
            }
        }
        else if segue.identifier == String(describing: RestHomeVC.self){
            if let restHome: RestHomeVC = segue.destination as? RestHomeVC {
                restHome.checkStoreFavStatus = {status,store in
                  self.updateFavoriteStores(store:store,isFav:status)
                }
            }
        }
        else if segue.identifier == String(describing: HomeVC.self){
            if let restHome: HomeVC = segue.destination as? HomeVC {
                restHome.checkStoreFavStatus = {status,store in
                 self.updateFavoriteStores(store:store,isFav:status)
                }
            }
        }
    }
    
    
    
    func updateFavoriteStores(store:Store,isFav:Bool){
        
        if isFav{
        store.StoreIsOpen = true
        self.restaurantVM.arrayOfFavorites.append(restaurantVM.selectedStoreForDetail)
        }
        else{
          if  let idx = self.restaurantVM.arrayOfResturants.index(of:restaurantVM.selectedStoreForDetail)
          {
             self.restaurantVM.arrayOfResturants.remove(at: idx)
        self.restaurantVM.arrayOfResturants.insert(restaurantVM.selectedStoreForDetail, at: idx)
          }else
          {
self.restaurantVM.arrayOfResturants.append(restaurantVM.selectedStoreForDetail)
            }
            
            
            self.restaurantVM.arrayOfFavorites.removeAll { (checkStore) -> Bool in
                if checkStore.Id == store.Id{
                    return true
                }
                 return false
            }
        }
        
        self.mainTableView.reloadData()
    }
}
