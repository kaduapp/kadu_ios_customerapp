//
//  HelpDetailVC.swift
//  DelivX
//
//  Created by 3 Embed on 12/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HelpDetailVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    
    var arraySupport:[Support] = []
    var titleText = ""
    var navView = NavigationView().shared

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainTableView.backgroundColor = Colors.ScreenBackground
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        // Do any additional setup after loading the view.
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: titleText)
        }
    }
    
    func readMoreBtnTapped(){
        performSegue(withIdentifier: UIConstants.SegueIds.HelpDetailToWebVC, sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = titleText
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: WebVC.self) {
            let data:Support = sender as! Support
            let vc:WebVC = segue.destination as! WebVC
            vc.urlGiven = data.Link
            vc.htmlDesc = data.Desc
            vc.titleText = data.Title
        }
    }
}
extension HelpDetailVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableView {
            
            if self.navigationController != nil {
                if scrollView.contentOffset.y > 0 {
                    if (self.navigationController != nil) {
                        Helper.addShadowToNavigationBar(controller: self.navigationController!)
                    }
                }else{
                    if (self.navigationController != nil) {
                        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                    }
                }
            }
            
        }
    }
    
    
}
