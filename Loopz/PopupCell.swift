//
//  PopupCell.swift
//  DelivX
//
//  Created by 3Embed on 21/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PopupCell: UITableViewCell {

    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var selectImage: UIImageView!
    @IBOutlet weak var selectLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        separatorView.backgroundColor = Colors.SeparatorLight
        selectLabel.textColor = Colors.PrimaryText
        Fonts.setPrimaryMedium(selectLabel)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(model:PopupModel) {
        selectLabel.text = model.String
        if model.Flag {
            selectImage.image = #imageLiteral(resourceName: "CheckBoxOn")
        }else{
            selectImage.image = #imageLiteral(resourceName: "CheckBoxOff")
        }
    }
    
    func setData(data : (String,Bool)) {
        selectLabel.text = data.0
        if data.1 {
            selectImage.image = #imageLiteral(resourceName: "CheckBoxOn")
        }else{
            selectImage.image = #imageLiteral(resourceName: "CheckBoxOff")
        }
    }

}
