//
//  CollectionSlotsViewModel.swift
//  DelivX
//
//  Created by 3EMBED on 23/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

enum tableType : Int
{
    case datesTableview = 1
    case slotsTableview = 2
}
enum slotTimeType : Int
{
    case startTime = 1
    case endTime = 2
}
enum slotType : Int
{
    case deliveryType = 10
    case pickupType = 11
}

class CollectionSlotsViewModel: NSObject {
    let rx_ReloadTableView = PublishSubject<Bool>()
    var disposeBag = DisposeBag()
    
    var slotsArray = [Datum]()
    var slots = [Slot]()
    var selectSlot: String? = nil
    var selectDate: String? = nil
    var slotId: String? = nil
    var pickeddateId: String? = nil
    var modalSheetTitle = "Collections Slots"
     //MARK:-Removing Spaces and newline For slotsTableView
    func removeSpacesandnewlineFromtime(time:String) -> String?
    {
        let timewithoutnewlineandspaces = time.replacingOccurrences(of: " \n ", with: " ", options: .regularExpression)
        return timewithoutnewlineandspaces
    }
      //MARK:-DateFormater For slotsTableView
    func returnTimeFromDateAfterFormating(_ dateString:String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "h:mm \n a"
        
        if let date = dateFormatterGet.date(from: dateString) {
          //  slotsLabel.text = dateFormatterPrint.string(from: date).lowercased()
            return dateFormatterPrint.string(from: date).lowercased()
        } else {
            print("There was an error decoding the string")
            return  dateString
        }
    }
    //MARK:-DateFormater For DatesTableView
    func returnDateAfterFormatting(_ dateString:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE \n d MMM"
        
        if let date = dateFormatterGet.date(from: dateString) {
            return dateFormatterPrint.string(from: date)
        } else {
             print("There was an error decoding the dateString")
            return  dateString
        }
    }
    
     //MARK:- UITableViewDelegate
    func getPickUpSlotsCount() -> Int {
        return slotsArray.count
    }
    func getSlotsCount() -> Int {
        return slots.count
    }
   
    func getIndexofDateID(dateid:String) -> Int
    {
        for (index,item) in slotsArray.enumerated() {
            
            if let date = item.id?.date , date == dateid
            {
                return index
            }           
        }
        return 0
    }
    func getSlotDate(at index: Int) -> (String?,String?) {
        if getPickUpSlotsCount() > index {
            let datumobj = slotsArray[index]
            if let date = datumobj.id,
                let dateStr = date.date ,let dateID = date.date{
                return  (returnDateAfterFormatting(dateStr),dateID)
            }
        }
         return(nil,nil)
    }
    
    //returnTimeFromDateAfterFormating(startTime)
    func getSlotTime(at index: Int) -> (String? ,String?,String?)  {
        if getSlotsCount() > index {
            let slotobj = slots[index]
            if let startTime =  slotobj.startDateTimestamp,
                let endTime = slotobj.endDateTimestamp , let id = slotobj._id {
                
            let startTimestamp = NSNumber.init(value:startTime )
            let endTimestamp = NSNumber.init(value:endTime )
             let timeStampStart =  Date(timeIntervalSince1970: TimeInterval(truncating: startTimestamp))
             let timeStampEnd =  Date(timeIntervalSince1970: TimeInterval(truncating: endTimestamp))
                return(Helper.getDateString(value: timeStampStart, format: "h:mm \n a", zone: true),
                       Helper.getDateString(value: timeStampEnd, format: "h:mm \n a", zone: true),id)
//           return(returnTimeFromDateAfterFormating(startString),returnTimeFromDateAfterFormating(endString),id)
                }
            }
        return(nil,nil,nil)
        }
    
    
    func getSelectedIndexForTableview(index:Int)    {
        if getPickUpSlotsCount() > index {
            let obj = self.slotsArray[index]
            
            self.slots = obj.slots as [Slot]
        }
    }
   
    
        // MARK:- API Methods
    func callSlots(Type:Int ,with LaundryType:Int){
        let slotType = self.getSlotsTypeFor(value: Type)
        switch slotType {
        case .pickupType :
           fetchSlotsFor("pickup", with: LaundryType)
            modalSheetTitle = "Collections Slots"
        case .deliveryType :
             fetchSlotsFor("delivery", with: LaundryType)
            modalSheetTitle = "Delivery Slots"
        }
    }
    
    
    func fetchSlotsFor(_ slotType: String ,with LaundryType:Int) {
        let rxAddCartAPI = CollectionSlotsAPICalls()
        if !rxAddCartAPI.rx_addResponse.hasObservers {
            rxAddCartAPI.rx_addResponse.subscribe(onNext: { (response) in
                print(response)
                
                if let responseData = response.data["data"] as? [[String:Any]] {
                    do {
                        let data = try JSONSerialization.data(withJSONObject: responseData, options: [])
                        self.slotsArray = try JSONDecoder().decode([Datum].self, from: data)
                         self.getSelectedIndexForTableview(index: 0)
                    } catch {
                        print("Error in Converting JSON to Data in rxCategoriesAndAttributesAPICall")
                    }
                }
                 self.rx_ReloadTableView.onNext(true)
            }, onError: { (error) in
                print("rx_addResponse \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_addResponse completed")
            }, onDisposed: {
                print("rx_addResponse disposed")
            }).disposed(by: disposeBag)
        }
        
        rxAddCartAPI.getAllSlotsFor(slotType, with: LaundryType)
        
    }
    

     //MARK:-  TableViewType Methods
    func getTableTypeForRowsCount(at index:Int) -> tableType {
        if index == 1 {
            return .datesTableview
        }   else {
            return .slotsTableview
        }
    }
    //MARK:-  TableViewType Methods
    func getSlotsTypeFor(value:Int) -> slotType {
        if value == 10 {
            return .pickupType
        }   else {
            return .deliveryType
        }
    }
    
    func getSlotTimeTypeFor(value:Int) -> slotTimeType {
        if value == 0 {
            return .startTime
        }   else {
            return .endTime
        }
    }
}
