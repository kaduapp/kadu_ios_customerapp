//
//  ConfirmAddressCell.swift
//  DelivX
//
//  Created by 3 Embed on 16/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ConfirmAddressCell: UITableViewCell {         // this cell re-used in payment options and confirm order
    // @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var addressTagName: UILabel!
    @IBOutlet weak var addressIcon: UIButton!
    @IBOutlet weak var addressIcon2: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cellSeparotor: UIView!
    @IBOutlet weak var rightArrowIcon: UIButton!
    @IBOutlet weak var addrToTitleHt: NSLayoutConstraint!
    
    @IBOutlet weak var changeLabel: UILabel!
    
    var cellColor = Colors.AppBaseColor
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        initialSetup(color:Colors.AppBaseColor,card: false)
    }
    
    func initialSetup(color:UIColor,card:Bool){
        // Fonts.setPrimaryMedium(headerTitle)
        cellColor = color
        Fonts.setPrimaryMedium(addressTagName)
        Fonts.setPrimaryRegular(addressLabel)
        cellSeparotor.backgroundColor = Colors.SeparatorLight
        rightArrowIcon.tintColor = Colors.PrimaryText
        addressTagName.textColor = color
        addressLabel.textColor = Colors.SeconderyText
        addressIcon.tintColor = color
        addressIcon.setImage(#imageLiteral(resourceName: "OtherAddress"), for: .normal)
        addressTagName.text = StringConstants.SelectAddress()
        addressIcon.isHidden = false
        addressIcon2.isHidden = true
        addressLabel.text = ""
        cellSeparotor.isHidden = true
        
        Fonts.setPrimaryMedium(changeLabel)
        changeLabel.textColor = Colors.SecoundPrimaryText
        changeLabel.text = StringConstants.ChangeText()
        changeLabel.isHidden = true

        adjustSpace()
    }
    
    func adjustSpace(){
        if addressLabel.text?.length != 0 {
            addrToTitleHt.constant = 12
        }
        else {
            addrToTitleHt.constant = 0
        }
    }
    
    
    /// set the selecte address
    ///
    /// - Parameter data: it is of type address model object
    func updateAddress(data:Address){
        
        addressTagName.text = OSLocalizedString(data.Tag, comment: data.Tag)
        var address = ""
        if data.FlatNo.length > 0{
            address = data.FlatNo + " "
        }
        if data.AddLine1.length > 0{
            address = address + "\(data.AddLine1 ),"
        }
        if data.AddLine2.length > 0{
            address = address + "\(data.AddLine2 ),"
        }
        if data.City.length > 1{
            address = address + "\(data.City ),"
        }
        if data.State.length > 1{
            address = address + "\(data.State )-"
        }
        if data.Zipcode.length > 0{
            address = address + "\(data.State ),"
        }
        if data.Country.length > 0{
            address = address + "\(data.Country )"
        }
        addressLabel.text = address
        addressIcon.setImage(Helper.setAddressImage(tag: data.Tag), for: .normal)
        cellSeparotor.isHidden = true
        
        if let count = addressLabel.text?.count , count > 0{
            self.changeLabel.isHidden = false
        }
        else{
            self.changeLabel.isHidden = true
        }

        adjustSpace()
    }
    
    func updatePayment(data:Card,payment:PaymentHandler){
        if payment.isPayByCard {
            addressIcon.isHidden = true
            addressIcon2.isHidden = false
            addressTagName.text = UIConstants.UIElement.Card + " " + data.Last4
            addressIcon2.image = Helper.cardImage(with: data.Brand.lowercased())

        }else{
            addressIcon.isHidden = false
            addressIcon2.isHidden = true
            addressIcon.setImage(#imageLiteral(resourceName: "Add_new"), for: .normal)
            addressTagName.text = StringConstants.SelectPayment()
        }
        if payment.isPayCashWallet{
            if Utility.getWallet().Amount > 0{
                let  walletString = StringConstants.Wallet() + "(" + Helper.df2so(Double( Utility.getWallet().Amount)) + ")"
                addressTagName.text = "\(walletString)  +  Cash"
            }
            else{
                addressTagName.text = StringConstants.Cash()
                addressIcon.setImage(#imageLiteral(resourceName: "Cash"), for: .normal)
                addressLabel.text = StringConstants.KeepChange()
            }

        }
        else if  payment.isPayCardWallet {
            if Utility.getWallet().Amount > 0{
                addressIcon.isHidden = false
                addressIcon2.isHidden = true
                addressIcon.setImage(#imageLiteral(resourceName: "Add_new"), for: .normal)
                let tempMoney =  StringConstants.Wallet() + "(" + Helper.df2so(Double( Utility.getWallet().Amount)) + ")"
                addressTagName.text = "Card \(data.Last4)  +  \(tempMoney)"
            }
            else{
                addressTagName.text = UIConstants.UIElement.Card + " " + data.Last4
                addressIcon2.image = Helper.cardImage(with: data.Brand.lowercased())
            }

        }
        else if payment.isPayByWallet {
            addressIcon.setImage(#imageLiteral(resourceName: "Wallet"), for: .normal)
            let tempMoney =  StringConstants.Wallet() + "(" + Helper.df2so(Double( Utility.getWallet().Amount)) + ")"
            addressTagName.text = tempMoney
        }else if payment.isPayByCash{
            addressTagName.text = StringConstants.Cash()
            addressIcon.setImage(#imageLiteral(resourceName: "Cash"), for: .normal)
            addressLabel.text = StringConstants.KeepChange()
        }else if payment.isPayByCard{
            addressTagName.text = UIConstants.UIElement.Card + " " + data.Last4
            addressIcon2.image = Helper.cardImage(with: data.Brand.lowercased())
        }
       
        adjustSpace()
        rightArrowIcon.isHidden = true
        cellSeparotor.isHidden = true
        self.changeLabel.isHidden = false

    }
    
    //Schedule sets
    func SetSchedule(data:CheckoutTime) {
        addressTagName.text = data.Head
        addressLabel.text = data.Body
        adjustSpace()
    }
    func UnselectedSchedule(pickup: Int) {
        addressIcon.tintColor =  Colors.FieldHeader
        addressIcon.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
        addressTagName.textColor =  Colors.FieldHeader
        addressLabel.textColor =  Colors.FieldHeader
        rightArrowIcon.isHidden = true
        adjustSpace()
    }
    
    func SelectedSchedule(pickup: Int) {
        addressIcon.tintColor = cellColor// Colors.PickupBtn
        addressTagName.textColor = cellColor// Colors.PickupBtn
        addressLabel.textColor = cellColor// Colors.PickupBtn
        addressIcon.setImage(#imageLiteral(resourceName: "CheckOn"), for: .normal)
        rightArrowIcon.isHidden = true
        adjustSpace()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    
    //Schedule sets
    func SetPromocode(promocode:String) {
        addressTagName.text = promocode
        addressLabel.text = String(format:StringConstants.AppliedPromocode(),promocode)
        addressIcon.setImage(#imageLiteral(resourceName: "coupon"), for: .normal)
        rightArrowIcon.setImage(#imageLiteral(resourceName: "CloseIcon"), for: .normal)
        rightArrowIcon.isHidden = false
        if promocode.length == 0 {
            addressTagName.text = StringConstants.PromocodeMissing()
            addressLabel.text = ""
            rightArrowIcon.isHidden = true
        }
        adjustSpace()
    }
}

