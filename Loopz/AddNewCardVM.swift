//
//  AddNewCardVM.swift
//  DelivX
//
//  Created by 3 Embed on 05/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AddNewCardVM: NSObject {
    
    enum ResponseType:Int {
        case AddCard    = 0
        case GetCard    = 1
        case Default    = 2
        case Delete     = 3
    }
   static var arrayOfCard:[Card] = []
   static let addNewCard_response = PublishSubject<(ResponseType)>()
    
    let disposeBag = DisposeBag()
    
    ///this method observe the card API a
    ///adds card to account
    /// - Parameter card: it is cardtoken
    func addCard(card:String) {
        CardAPICalls.addCard(token:card).subscribe(onNext: {success in
            AddNewCardVM.arrayOfCard.append(success)
            AddNewCardVM.addNewCard_response.onNext((ResponseType.AddCard))
        }, onError: {error in
            print(error)
        }).disposed(by: CardAPICalls.disposeBag)
    }
    
    
    ///this method observe the card API a
    ///get card to account
    func getCard() {
        CardAPICalls.getCard().subscribe(onNext: {success in
            AddNewCardVM.arrayOfCard.removeAll()
            AddNewCardVM.arrayOfCard.append(contentsOf:success)
            AddNewCardVM.addNewCard_response.onNext((ResponseType.GetCard))
        }, onError: {error in
            print(error)
        }).disposed(by: CardAPICalls.disposeBag)
    }
    
    ///this method observe the card API a
    ///adds card to account
    /// - Parameter card: it is card model
    func deleteCard(card:Card) {
        CardAPICalls.deleteCard(card: card).subscribe(onNext: {success in
            AddNewCardVM.arrayOfCard.remove(at: AddNewCardVM.arrayOfCard.index(of: card)!)
            AddNewCardVM.addNewCard_response.onNext((AddNewCardVM.ResponseType.Default))
        }, onError: {error in
            print(error)
        }).disposed(by: CardAPICalls.disposeBag)
    }
    
    
    ///this method observe the card API a
    ///adds card to account
    /// - Parameter card: it is card model
    func defaultCard(card:Card) {
        CardAPICalls.defaultCard(card: card).subscribe(onNext: {success in
            for item in AddNewCardVM.arrayOfCard {
                if item.Id == card.Id {
                    item.Default = 1
                }else{
                    item.Default = 0
                }
            }
            AddNewCardVM.addNewCard_response.onNext((AddNewCardVM.ResponseType.Default))
        }, onError: {error in
            print(error)
        }).disposed(by: CardAPICalls.disposeBag)
    }
    

}
