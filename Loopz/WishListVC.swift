//
//  WishListVC.swift
//  UFly
//
//  Created by 3 Embed on 04/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class WishListVC: UIViewController {
    
    
    @IBOutlet weak var viewCartBar: UIView!
    @IBOutlet weak var extraCharges: UILabel!
    @IBOutlet weak var viewCartBtn: UIButton!
    @IBOutlet weak var viewCartBarHight: NSLayoutConstraint!
    @IBOutlet weak var viewCartlabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var itemCount: UILabel!
    var navView = NavigationView().shared

    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    var selected:UICollectionViewCell? = nil
    let wishListVM = WishListVM()
//    let transition = PopAnimator()
    var emptyView = EmptyView().shared
    var responceFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEmptyScreen()
        didGetData()
        viewCartBar.backgroundColor =  Colors.AppBaseColor
        viewCartlabel.text = StringConstants.ViewCart()
        extraCharges.text = StringConstants.ExtraCharges()
        FavouriteAPICalls.getWishList(id:"0",pi:true).subscribe(onNext: { [weak self]data in
            self?.mainCollectionView.reloadData()
        }).disposed(by: self.wishListVM.disposeBag)
        
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        }
        
        if wishListVM.wishList.ID.length > 0 {
            wishListVM.getItemWishList()
            //setDataUI()
            navView.setData(title: wishListVM.wishList.String)
        }else{
            navView.setData(title:StringConstants.Favourite())
            wishListVM.getFav()
            wishListVM.setSubscribe()
            mainCollectionView.backgroundColor = UIColor.white
        }
        
        
        // Do any additional setup after loading the view.
        
        mainCollectionView.register(UINib(nibName: String(describing: ItemCommonCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // Helper.transparentNavigation(controller: self.navigationController!)
        self.showBottom()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // Helper.nonTransparentNavigation(controller: self.navigationController!)
    }
    
    @IBAction func uploadImage(_ sender: Any) {
        var flag = 0
        if wishListVM.wishList.Image.length == 0 {
            flag = 0
        } else {
            flag = 1
        }
        wishListVM.pickerObj?.setProfilePic(controller: self , tag: flag)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.ListToDetails || segue.identifier == UIConstants.SegueIds.FavToDetails {
            let button = sender as! UIButton
            if let nav:UINavigationController = segue.destination as? UINavigationController {
                if let itemDetailVC: ItemDetailVC = nav.viewControllers.first as? ItemDetailVC{
//                itemDetailVC.transitioningDelegate = self
                    itemDetailVC.itemDetailVM.item = wishListVM.arrayOfItems[button.tag]
                }
            }
        }
    }
    
}
extension WishListVC {
    /// observing selectLocation View Model
    func didGetData(){
        CartDBManager.cartDBManager_response.subscribe(onNext: { [weak self]success in
            self?.showBottom()
        }).disposed(by: self.wishListVM.disposeBag)
        self.wishListVM.wishListVMResponse.subscribe(onNext: { [weak self]success in
            self?.responceFlag = true
            switch success {
            case (-1 ,.GetFav) :
                // for favourites
                self?.mainCollectionView.reloadData()
                break
            case (-2,.GetItemWishList):
                //wishList
                //    self.mainTableView.reloadData()
                self?.mainCollectionView.reloadData()
                break
            case (-3,.UpdateWishList):
                //wishList Items
                break
                
            default:
                if success.1 == WishListVM.respType.Favourite {
                    self?.mainCollectionView.reloadData()//deleteItems(at: [index])
                }
                else {
                    self?.mainCollectionView.reloadData()
                }
                break
            }
            
        },onError : { error in
            print(error)
        }).disposed(by: self.wishListVM.disposeBag)
        
        wishListVM.uploadImageModel.UploadImageRespose.subscribe(onNext: { success in
            if success == .WishList {
                self.wishListVM.updateWishList()
            }
            
        }).disposed(by: self.wishListVM.disposeBag)
    }
    
    @IBAction func didTapViewCartBtn(_ sender: UIButton) {
        self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
    }
    
    func showBottom() {
        if AppConstants.CartCount > 1 {
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Items()
        }else{
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Item()
        }
        amount.text = Helper.df2so(Double(AppConstants.TotalCartPrice))
        var length = 0
        if AppConstants.CartCount > 0 {
            length = 50
        }
        UIView.animate(withDuration: 0.50) {
            self.viewCartBarHight.constant = CGFloat(length)
            self.view.layoutIfNeeded()
        }
    }
    
    /// hides the bottom bar
    ///
    /// - Parameter sender: is of UIButton type
    func hideBottom(sender: UIButton!) {
        UIView.animate(withDuration: 0.50) {
            self.viewCartBarHight.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}
extension WishListVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if self.navigationController != nil {
            if scrollView.contentOffset.y > 0 {
                if (self.navigationController != nil) {
                    Helper.addShadowToNavigationBar(controller: self.navigationController!)
                }
            }else{
                if (self.navigationController != nil) {
                    Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                }
            }
        }
    }
}


