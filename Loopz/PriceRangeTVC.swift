//
//  PriceRangeTVC.swift
//  DelivX
//
//  Created by 3Embed on 25/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
 
class PriceRangeTVC: UITableViewCell {

    @IBOutlet weak var maxValue: UILabel!
    @IBOutlet weak var minimumValue: UILabel!
    @IBOutlet weak var rangeBar: RangeSlider!
    override func awakeFromNib() {
        super.awakeFromNib()
        rangeBar.trackTintColor = Colors.SeparatorLarge
        rangeBar.trackHighlightTintColor = Colors.AppBaseColor
        maxValue.textColor = Colors.AppBaseColor
        Fonts.setPrimaryRegular(maxValue)
        
        minimumValue.textColor = Colors.AppBaseColor
        Fonts.setPrimaryRegular(minimumValue)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(min:Float,max:Float) {
        minimumValue.text = Helper.df2so(Double(min))
        maxValue.text = Helper.df2so(Double(max))
    }
}
