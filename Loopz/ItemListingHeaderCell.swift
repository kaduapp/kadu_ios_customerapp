//
//  ItemListingHeaderCell.swift
//  DelivX
//
//  Created by 3 Embed on 19/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ItemListingHeaderCell: UICollectionViewCell {
    
    var ViewStore:DispenceryView = DispenceryView()
    override func awakeFromNib() {
        super.awakeFromNib()
        ViewStore = DispenceryView().shared
        ViewStore.bounds = self.contentView.bounds
        ViewStore.frame.origin.x = 0
        ViewStore.frame.origin.y = 0
        self.contentView.addSubview(ViewStore)
        ViewStore.translatesAutoresizingMaskIntoConstraints = false
        let when = DispatchTime.now() + 0
        DispatchQueue.main.asyncAfter(deadline: when){
            self.addConstraint(NSLayoutConstraint(item: self.ViewStore, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: self.ViewStore, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: self.ViewStore, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: self.ViewStore, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0))
        }
    }
    
    func setData(data:Store) {
        ViewStore.isHidden = false
        ViewStore.setData(data: data)
    }
    
    func setEmpty(view:EmptyView) {
        view.bounds = self.contentView.bounds
        view.frame.origin.x = 0
        view.frame.origin.y = 0
        self.contentView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        let when = DispatchTime.now() + 0
        DispatchQueue.main.asyncAfter(deadline: when){
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 10))
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 10))
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -10))
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 10))
        }
        
        ViewStore.isHidden = true
    }
}
