//
//  AddOnVM.swift
//  DelivX
//
//  Created by 3Embed on 01/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AddOnVM: NSObject {
    var SelectedItem:Item = Item(data: [:], store: Store(data: [:]))
    var ArrayOfAddons:[AddOnGroup] = []
    let disposeBag = DisposeBag()
    let AddOnVM_response        = PublishSubject<Bool>()
    let AddOnsVM_cartUpdateResponse = PublishSubject<ResponseType>()
    let AddOnsVM_cartUpdateResponseFromCart = PublishSubject<(ResponseType,Item)>()
    enum ResponseType:Int {
        case Close  = 0
        case Add    = 1
        case Patch  = 2
    }
    func getTheAddOnList() {
        HomeAPICalls.getItemDetail(selectedItem: SelectedItem).subscribe(onNext: {[weak self]data in
            self?.SelectedItem = data
            self?.AddOnVM_response.onNext(true)
        }, onError: {error in
                print(error)
        }).disposed(by: self.disposeBag)
    }
    
    func customize() {
        CartAPICalls.updateAddonData(item: CartItem.init(item: SelectedItem)).subscribe(onNext: {[weak self]data in
            if data {
                CartVM().getCart()
                self?.AddOnVM_response.onNext(false)
            }
            }, onError: {error in
                print(error)
        }).disposed(by: self.disposeBag)
    }
    
    
    /// Check All Mandatory Fields added
    ///
    /// - Returns: True if fields are added else False
    func isMandatoryAddOnsAdded() -> Bool{
        for  addonGroup in ArrayOfAddons {
            if addonGroup.Mandatory && addonGroup.multiple {
                if SelectedItem.AddOnGroups.contains(where: { (addonGroup) -> Bool in
                    return true
                }){
                 for selectedGroup in self.SelectedItem.AddOnGroups{
                        if selectedGroup.Id == addonGroup.Id{
                            var count = 0
                            for addOn in selectedGroup.AddOns{
                                if addOn.Selected{
                                  count = count + 1
                                }
    
                            }
                            if count < selectedGroup.minimumLimit{
                            Helper.showAlert(message: "Please select a minimum of \(addonGroup.minimumLimit)  options in \(addonGroup.Name)", head: "Message", type:0 )
                               return false
                            }
                        }
                    }
                }
                else{
                 Helper.showAlert(message: "Please select \(addonGroup.Name) it is a mandatory Add On", head: "Message", type:0 )
                 return false
                }
            }
            else if !addonGroup.Mandatory && addonGroup.multiple {
                if SelectedItem.AddOnGroups.contains(where: { (addonGroup) -> Bool in
                    return true
                }){
                    for selectedGroup in self.SelectedItem.AddOnGroups{
                        if selectedGroup.Id == addonGroup.Id{
                            var count = 0
                            for addOn in selectedGroup.AddOns{
                                if addOn.Selected{
                                    count = count + 1
                                }
                                
                            }
                            if count != 0 && count < selectedGroup.minimumLimit{
                                Helper.showAlert(message: "Please select a minimum of \(addonGroup.minimumLimit)  options in \(addonGroup.Name)", head: "Message", type:0 )
                                return false
                            }
                        }
                    }
                }
            }
            
        }
        return true
    }
}
