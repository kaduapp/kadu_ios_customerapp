//
//  ScheduleTimeCell.swift
//  DelivX
//
//  Created by 3 Embed on 20/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
protocol ScheduleTimeCellDelegate: class  {
    func didTapDateButton(_ tagValue: Int)
}
class ScheduleTimeCell: UITableViewCell {

    @IBOutlet weak var dateSelect: UIButton!
    
    @IBOutlet weak var timeSelect: UIButton!
    @IBOutlet weak var selectDateLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var selectSlotLabel: UILabel!
    @IBOutlet weak var selectTime: UILabel!
    
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
     var delegate: ScheduleTimeCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }

    func initialSetup(){
        
        Fonts.setPrimaryMedium(selectDateLabel)
        Fonts.setPrimaryMedium(dateLabel)
        Fonts.setPrimaryMedium(selectSlotLabel)
        Fonts.setPrimaryMedium(selectTime)
        
        selectDateLabel.textColor = Colors.PrimaryText
        selectSlotLabel.textColor = Colors.PrimaryText
        
        selectDateLabel.text = StringConstants.SelectDate()
        selectSlotLabel.text = StringConstants.SelectTime()
        
    }
    
    func updateDateTime(date:Date, time:Date ,pickup:Int) {
        
        if pickup == 1 {
            dateLabel.textColor = Colors.DeliveryBtn
            selectTime.textColor = Colors.DeliveryBtn
        }else {
            selectTime.textColor = Colors.PickupBtn
            dateLabel.textColor = Colors.PickupBtn
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormat.dateTime
        var result = formatter.string(from: date)
        dateLabel.text = result
    
        formatter.dateFormat = DateFormat.TimeFormatToDisplay
        result = formatter.string(from: time)
         selectTime.text = result
        
    }
    
    @IBAction func selectDateTapped(_ sender: Any) {
        delegate?.didTapDateButton((sender as AnyObject).tag)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
