//
//  PaymentOptionsVC.swift
//  DelivX
//
//  Created by 3 Embed on 27/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PaymentOptionsVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    
  
    var cardArray   = [Card]()
    var priority = true
    var discount:Float   = 0
    let responseData = PublishSubject<(Card,PaymentHandler,Bool)>()
    var selectedIndex:IndexPath? = nil
    var navView = NavigationView().shared
    var methodHandler = PaymentHandler()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.PaymentOptions())
            selectedIndex = IndexPath.init(row: 0, section: 0)
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initialSetup()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initialSetup() {
        
        self.mainTableView.backgroundColor = Colors.ScreenBackground
        WalletAPICalls.getBalance().subscribe(onNext: { [weak self]data in
                self?.mainTableView.reloadData()
            }).disposed(by: APICalls.disposesBag)
        setUpVM()
        AddNewCardVM().getCard()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.title = ""
    }
    
}

extension PaymentOptionsVC {
    func setUpVM() {
        AddNewCardVM.addNewCard_response.subscribe(onNext: { [weak self]success in
            if success == AddNewCardVM.ResponseType.GetCard {
                self?.cardArray = AddNewCardVM.arrayOfCard
                self?.mainTableView.reloadData()
            }
            }, onError: { (error) in
                
        }, onCompleted: {
            
        }).disposed(by: CardAPICalls.disposeBag)
    }
}
extension PaymentOptionsVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableView {
            if self.navigationController != nil {
                let contentOffset = scrollView.contentOffset.y
                if contentOffset > 0 {
                    if (self.navigationController != nil) {
                        Helper.addShadowToNavigationBar(controller: self.navigationController!)
                    }
                }else{
                    if (self.navigationController != nil) {
                        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                    }
                }
            }
            
        }
    }
}
