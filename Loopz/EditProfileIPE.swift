//
//  EditProfileIPE.swift
//  UFly
//
//  Created by 3 Embed on 30/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - ImagePickerModelDelegate
extension EditProfileVC: ImagePickerModelDelegate {
    
    func didPickImage(selectedImage: UIImage , tag:Int) {
        //Image Removed
        if tag == 0 {
            
            if editProfileVM.btnTag == .mmjCard {
                editProfileVM.authentication.MMJUrl = ""
                editProfileVIew.pickedMMJImage = nil
                editProfileVIew.mmjCardImage.image = #imageLiteral(resourceName: "UploadDefault")
                editProfileVIew.mmjUploadedLabel.isHidden = true
                editProfileVIew.mmjCheckMark.isHidden = true
            }
           else if editProfileVM.btnTag == .profilePic {
                editProfileVM.authentication.ProfilePic = ""
                editProfileVIew.pickedProfileImage = nil
                editProfileVIew.profileImage.image = #imageLiteral(resourceName: "UserDefault")
                
            }
            else {
                editProfileVM.authentication.IDUrl = ""
                editProfileVIew.pickedIDImage = nil
                editProfileVIew.idCardImage.image = #imageLiteral(resourceName: "UploadDefault")
                editProfileVIew.idUploadedLabel.isHidden = true
                editProfileVIew.idCheckMark.isHidden = true
            }
        }//Image Added
        else {
            
            if editProfileVM.btnTag == .mmjCard {
                
                editProfileVIew.pickedMMJImage = selectedImage
                editProfileVIew.mmjCardImage.image = selectedImage
                editProfileVIew.mmjUploadedLabel.isHidden = false
                editProfileVIew.mmjCheckMark.isHidden = false
            }
            else if editProfileVM.btnTag == .profilePic {
                editProfileVIew.pickedProfileImage = selectedImage
                editProfileVIew.profileImage.image = selectedImage
                
                
            }
            else {
                editProfileVIew.pickedIDImage = selectedImage
                editProfileVIew.idCardImage.image = selectedImage
                editProfileVIew.idUploadedLabel.isHidden = false
                editProfileVIew.idCheckMark.isHidden = false
            }
            uploadImage()
        }
        if editProfileVIew.pickedIDImage != nil && editProfileVIew.pickedMMJImage != nil {
            editProfileVIew.bottomLabelView.isHidden = true
        }
        else {
            editProfileVIew.bottomLabelView.isHidden = true
        }
        
    }
    func uploadImage(){
        switch editProfileVM.btnTag {
        case .mmjCard?:
            editProfileVM.authentication.MMJUrl = Helper.uploadImage(data:editProfileVIew.pickedMMJImage!,path:AmazonKeys.MMJPic, page: .EditVC)
            break
        case .profilePic?:
            editProfileVM.authentication.ProfilePic = Helper.uploadImage(data:editProfileVIew.pickedProfileImage!,path:AmazonKeys.ProfilePic, page: .EditVC)
            break
        case .iDCard?:
            editProfileVM.authentication.IDUrl = Helper.uploadImage(data:editProfileVIew.pickedIDImage!,path: AmazonKeys.IDPic, page: .EditVC)
            break
        case .none:
            break
        }
    }
}

