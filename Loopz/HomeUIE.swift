//
//  HomeUIE.swift
//  UFly
//
//  Created by 3Embed on 07/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension HomeVC:UIScrollViewDelegate{
    
    /// initial viewsetup
    func setUIStatic(){
    
        if Changebles.isGrocerOnly{
            self.navigationItem.titleView = self.grocerNavigationView
            if self.grocerNavigationView != nil {
                grocerNavigationWidth.constant = UIScreen.main.bounds.size.width - 20
                addressHeadLabel.text = StringConstants.DeliveryLocation()
                addressHeadLabel.textColor = Colors.SecoundPrimaryText
                Fonts.setPrimaryMedium(addressHeadLabel)
                addressLabel.textColor = Colors.PrimaryText
                Fonts.setPrimaryMedium(addressLabel)
            }
            self.viewCartBarHight.constant = 0
            storeViewHeightConstraint.constant = 40
            self.mainTableView.tableHeaderView = nil
        }
        else{
        let navView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 70, height: 40))
        navView.backgroundColor = UIColor.clear
        navTitleLabel.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width - 160, height: 40)

        navTitleLabel.backgroundColor = UIColor.clear
        navTitleLabel.textColor = UIColor.black
        navTitleLabel.font =  UIFont.systemFont(ofSize: 17)
        
        if RTL.shared.isRTL
        {
            backButton.frame = CGRect(x: navView.frame.size.width-30, y: 0, width: 40, height: 40)
            backButton.rotateButton()

        }else
        {
            backButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
            
        }
        
        backButton.addTarget(self, action: #selector(backButtonAction), for:    .touchUpInside)
        backButton.setImage(UIImage(named: "BackIcon"), for: UIControl.State())
        
        navView.addSubview( backButton)
        navView.addSubview(navTitleLabel)
        
        let leftBarbuttonItem = UIBarButtonItem(customView: navView)
        let searchButtonItem = UIBarButtonItem(customView: self.searchButtonOutlet)
        let favButtonItem = UIBarButtonItem(customView: self.favButtonOutlet)
        self.navigationItem.leftBarButtonItem = leftBarbuttonItem
        self.navigationItem.rightBarButtonItems = [searchButtonItem,favButtonItem]
        self.navigationItem.titleView = self.navigationView
        storeViewHeightConstraint.constant = 0
        }
        
        mainTableView.backgroundColor = Colors.ScreenBackground
        Helper.drawDottedLine(viewForDottedLine)
        mainTableView.rowHeight = UITableView.automaticDimension
        Helper.removeNavigationSeparator(controller: self.navigationController!,image: true)
        
         if mainTableView.contentOffset.y > 0 {
            Helper.addShadowToNavigationBar(controller: self.navigationController!)
         }else{
            Helper.removeShadowToNavigationBar(controller: self.navigationController!)
         }
         Fonts.setPrimaryBold(selectStoreTitle, size: 25)
         Fonts.setPrimaryRegular(storeAddressLabel, size: 11)
         selectStoreTitle.textColor = Colors.PrimaryText
         storeAddressLabel.textColor = Colors.SeconderyText
        ratingHead.textColor = Colors.PrimaryText
        ratingValue.tintColor = Colors.PrimaryText
        distanceHead.textColor = Colors.PrimaryText
        distanceValue.textColor = Colors.PrimaryText
        costForTwoHead.textColor = Colors.PrimaryText
        costForTwoValue.textColor = Colors.PrimaryText
        
        ratingHead.text = StringConstants.Rating()
        distanceHead.text = StringConstants.FarAway()
        costForTwoHead.text = StringConstants.MinOrder()
       CartDBManager.cartDBManager_response.subscribe({ [weak self]success in
            self?.showBottom()
        }).disposed(by: self.homeVM.disposeBag)
        
        //Bottom CartView
        viewCartBar.backgroundColor =  Colors.AppBaseColor
        viewCartlabel.text = StringConstants.ViewCart()
        extraCharges.text = StringConstants.ExtraCharges()
    }
    
    
    @objc func storeChangeAction(){
        self.performSegue(withIdentifier: String(describing: StoresListVC.self), sender: nil)
    }
    
    @objc func backButtonAction(_ sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setStore(){
        
        if !Changebles.isGrocerOnly{
            
        self.navTitleLabel.text = homeVM.store.Name
        selectStoreTitle.text = homeVM.store.Name
        storeAddressLabel.text = homeVM.store.storeArea
        superTitle.text = Utility.getSelectedSuperStores().categoryName
        ratingValue.setTitle("\(Helper.clipDigit(valeu: homeVM.List_Store.StoreRating, digits: 1))", for: .normal)
            
        costForTwoValue.text = Helper.df2so(Double(homeVM.store.MinimumOrder))
            
            
        var distanceMetric = StringConstants.MileAway()
        if Int(Utility.getCurrency().2) == 1{
                distanceMetric = StringConstants.MileAway()
                distanceValue.text = "\(Helper.clipDigit(valeu: homeVM.List_Store.DistanceMiles, digits: 2))" + " " +  "\(distanceMetric)"
            }else{
                distanceMetric = StringConstants.KMAway()
                distanceValue.text = "\(Helper.clipDigit(valeu: homeVM.List_Store.DistanceKm.floatValue ?? 0.0, digits: 2))" + " " + "\(distanceMetric)"
        }
            
            stackViewHeight.constant = 40
            restaurantStack.isHidden = false
            secondDottedlineViewHeight.constant = 1
            Helper.drawDottedLine(secondViewForDottedLine)
            Helper.setImage(imageView: storeImage, url: homeVM.store.Image, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
            dispenceryViewHVC.hideLoader()
            
            if homeVM.store.Fav{
                self.favButtonOutlet.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
            }
            else{
                self.favButtonOutlet.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
            }
            
            
            if homeVM.store.Id.length == 0{
                dispenceryViewHVC.hideLoader()
                dispenceryViewHVC.showLoader()
                return
            }
        }
        else{
            storeNameLabel.text = homeVM.store.Name
            Helper.setImage(imageView: currentStoreImage, url: homeVM.store.Image, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        }
        self.loadViewIfNeeded()
    }

    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        if scrollView == self.mainTableView{
            if self.navigationController != nil{
                if mainTableView.contentOffset.y > 0{
                    
                    if !Changebles.isGrocerOnly{
                     Helper.addShadowToNavigationBar(controller: (self.navigationController)!)
                    }
                    if mainTableView.contentOffset.y > 25{
                       self.navTitleLabel.alpha = 1
                    }else{
                        self.navTitleLabel.alpha = 0
                    }
                }
                else{
                    Helper.removeShadowToNavigationBar(controller: (self.navigationController)!)
                   
                    
                }
            }
        }
    }
    
    /// shows bottomcort bar if cartcount is greater than 0
    func showBottom() {
        
      if  !Changebles.isGrocerOnly{
        if AppConstants.CartCount > 1 {
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Items()
        }else{
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Item()
        }
        amount.text = Helper.df2so(Double( AppConstants.TotalCartPrice))
        var length = 0
        if AppConstants.CartCount > 0 {
            length = 50
        }
        UIView.animate(withDuration: 0.50) {
            self.viewCartBarHight.constant = CGFloat(length)
            self.view.layoutIfNeeded()
        }
     }
    }
    
    /// hides the bottom bar
    ///
    /// - Parameter sender: is of UIButton type
    func hideBottom(sender: UIButton!) {
        UIView.animate(withDuration: 0.50) {
            self.viewCartBarHight.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    

}
