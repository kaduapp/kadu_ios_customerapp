//
//  CreateNewListView.swift
//  DelivX
//
//  Created by 3 Embed on 03/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CreateNewListView: UIView {
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var enterNameLabel: UILabel!
    @IBOutlet weak var newListNameTF: UITextField!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var titleView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialViewSetup()
        newListNameTF.Semantic()
    }
    
    ///  initial view setup
    func initialViewSetup(){
        Fonts.setPrimaryMedium(headLabel)
        Fonts.setPrimaryMedium(enterNameLabel)
        Fonts.setPrimaryMedium(doneBtn)
        Fonts.setPrimaryRegular(newListNameTF)
        
        enterNameLabel.textColor = Colors.SeconderyText
        headLabel.textColor = Colors.SecondBaseColor
        newListNameTF.textColor = Colors.PrimaryText
        
        Helper.setButtonTitle(normal: StringConstants.DoneLower(), highlighted: StringConstants.DoneLower(), selected: StringConstants.DoneLower(), button: doneBtn)
        doneBtn.tintColor = Colors.SecondBaseColor
        closeBtn.tintColor = Colors.SecondBaseColor
        separatorView.backgroundColor = Colors.AppBaseColor
        titleView.backgroundColor = Colors.AppBaseColor
        newListNameTF.becomeFirstResponder()
    }
}
