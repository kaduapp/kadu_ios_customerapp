//
//  OTPView.swift
//  UFly
//
//  Created by Rahul Sharma on 20/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class OTPView: UIView {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var textFieldOtp0: UITextField!
    @IBOutlet weak var textFieldOtp1: UITextField!
    @IBOutlet weak var textFieldOtp2: UITextField!
    @IBOutlet weak var textFieldOtp3: UITextField!
    @IBOutlet weak var textFieldOtp4: UITextField!
    @IBOutlet weak var textFieldOtp5: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var verifyButtonView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var verifyCodeLabel: UILabel!
    @IBOutlet weak var verifyNumTitle: UILabel!
    @IBOutlet weak var forgotPwLabel: UILabel!
    @IBOutlet weak var resendCode: UIButton!
    
    @IBOutlet weak var tfSeparator0: UIView!
    @IBOutlet weak var tfSeparator1: UIView!
    @IBOutlet weak var tfSeparator2: UIView!
    @IBOutlet weak var tfSeparator3: UIView!
    @IBOutlet weak var tfSeparator4: UIView!
    @IBOutlet weak var tfSeparator5: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateView()
        textFieldOtp0.Semantic()
        textFieldOtp1.Semantic()
        textFieldOtp2.Semantic()
        textFieldOtp3.Semantic()
        textFieldOtp4.Semantic()
        textFieldOtp5.Semantic()
    }
    
    /// view initial setup
    func updateView() {
        Helper.setButton(button: verifyButton,view:verifyButtonView, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.Verify(), highlighted: StringConstants.Verify(), selected: StringConstants.Verify(), button: verifyButton)
        Helper.setButtonTitle(normal: StringConstants.ResendOtp(), highlighted: StringConstants.ResendOtp(), selected: StringConstants.ResendOtp(), button: resendCode)
        
        Fonts.setPrimaryMedium(verifyButton)
        
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
        Fonts.setPrimaryMedium(textFieldOtp0)
        Fonts.setPrimaryMedium(textFieldOtp1)
        Fonts.setPrimaryMedium(textFieldOtp2)
        Fonts.setPrimaryMedium(textFieldOtp3)
        Fonts.setPrimaryMedium(textFieldOtp4)
        Fonts.setPrimaryMedium(textFieldOtp5)
        
        Fonts.setPrimaryBlack(forgotPwLabel)
        Fonts.setPrimaryMedium(verifyNumTitle)
        Fonts.setPrimaryRegular(headingLabel)
        Fonts.setPrimaryMedium(verifyCodeLabel)
        Fonts.setPrimaryMedium(resendCode)
        
        resendCode.setTitleColor( Colors.AppBaseColor, for: .normal)
        
        forgotPwLabel.text   = StringConstants.ForgotPasswordHead()
        verifyNumTitle.text  = StringConstants.VerifyNum()
        headingLabel.text    = StringConstants.OtpTitle()
        verifyCodeLabel.text = StringConstants.VerifyCode()
        
        textFieldOtp0.textColor  =  Colors.PrimaryText
        forgotPwLabel.textColor  =  Colors.HeadColor
        textFieldOtp1.textColor  =  Colors.PrimaryText
        textFieldOtp2.textColor  =  Colors.PrimaryText
        textFieldOtp3.textColor  =  Colors.PrimaryText
        verifyCodeLabel.textColor  =  Colors.SeconderyText
        headingLabel.textColor   =  Colors.PrimaryText
        verifyNumTitle.textColor =  Colors.PrimaryText
        
        Helper.updateText(text: StringConstants.ResendOtp(), subText: StringConstants.Resend(), resendCode, color:  Colors.AppBaseColor,link: "")
    }
    
    
    /// this method updates the mobile number entered by the user in login or signup screen to
    ///this otp screen
    /// - Parameter phoneNumber: phoneNumber it is of type string
    func updateMobileNumber(phoneNumber: String){
        Helper.updateText(text: StringConstants.OtpTitle() + " " + phoneNumber, subText: phoneNumber, headingLabel, color:  Colors.PrimaryText,link: "")
    }
    
    
    ///this method is to update the screen title based on flag
    /// if flag - true means otp is for forgot password ,flag - false means otp is for verifying phone number
    /// - Parameter flag: flag is of type Bool
    func comeFromFP(flag:Bool) {
        if flag == true {
            forgotPwLabel.text = StringConstants.ForgotPasswordHead()
            verifyNumTitle.text = StringConstants.ForgotPasswordHead()
        }
        else {
            forgotPwLabel.text = StringConstants.VerifyNum()
            verifyNumTitle.text = StringConstants.VerifyNum()
        }
    }
    
    /// this method is to clears otp if user tap on resend otp
    func clearOtp(){
        textFieldOtp0.text = ""
        textFieldOtp1.text = ""
        textFieldOtp2.text = ""
        textFieldOtp3.text = ""
        textFieldOtp4.text = ""
        textFieldOtp5.text = ""
        tfSeparator0.backgroundColor =  Colors.SeparatorLight
        tfSeparator1.backgroundColor =  Colors.SeparatorLight
        tfSeparator2.backgroundColor =  Colors.SeparatorLight
        tfSeparator3.backgroundColor =  Colors.SeparatorLight
        tfSeparator4.backgroundColor =  Colors.SeparatorLight
        tfSeparator5.backgroundColor =  Colors.SeparatorLight
        Helper.setButton(button: verifyButton,view:verifyButtonView, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
        textFieldOtp0.becomeFirstResponder()
    }
    
    /// Dismiss Keyboard
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
    
    /// this method gets called when user tap on verify otp to check otp has enterd or not
    func checkForData() -> Bool {
        guard textFieldOtp0.text?.sorted().count != 0 else {
            Helper.showAlert(message: StringConstants.EnterOtp(), head: StringConstants.Error(), type: 1)
            return false
        }
        guard textFieldOtp1.text?.sorted().count != 0 else {
            Helper.showAlert(message: StringConstants.EnterOtp(), head: StringConstants.Error(), type: 1)
            return false
        }
        guard textFieldOtp2.text?.sorted().count != 0 else {
            Helper.showAlert(message: StringConstants.EnterOtp(), head: StringConstants.Error(), type: 1)
            return false
        }
        guard textFieldOtp3.text?.sorted().count != 0 else {
            Helper.showAlert(message: StringConstants.EnterOtp(), head: StringConstants.Error(), type: 1)
            return false
        }
        guard textFieldOtp5.text?.sorted().count != 0 else {
            Helper.showAlert(message: StringConstants.EnterOtp(), head: StringConstants.Error(), type: 1)
            return false
        }
        return true
    }
    
}
