//
//  SearchAPICalls.swift
//  UFly
//
//  Created by 3 Embed on 28/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class SearchAPICalls: NSObject {
    
    //Get Result
    static let disposeBag = DisposeBag()
    class func searchStore(needle:String,cousine:String,rating:Int,foodType:String,language:String) -> Observable<[Store]> {
        print("Search text" + needle)
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        
        var array:[Any] = [
            [
                "match": [
                    "status": 1
                ]
            ],
            [
                "match": [
                    "serviceZones": Utility.getAddress().ZoneId
                ]
            ],
            [
                "match": [
                    "storeCategory.categoryId": Utility.getSelectedSuperStores().Id
                ]
            ],
            [
                "match": [
                    "storeType": Utility.getSelectedSuperStores().type.rawValue
                ]
            ]
        ]
        
        if needle.length > 0 {
            let data = [
                "match_phrase_prefix": [
                    "sName." + language : needle
                ]
            ]
            array.append(data)
        }
        
        if rating > 0 {
            let data = [
                    "range": [
                        "averageRating":[
                            "gte":0,
                            "lte":rating
                        ]
                    ]
                ]
            array.append(data)
        }
        
        if cousine.length > 0 {
            let data = [
                "match":[
                    "storeSubCategory.subCategoryName." + language:cousine
                ]
            ]
            array.append(data)
        }
        
        if foodType.length > 0 {
            let data = [
                 "match":[
                    "foodTypeName" : foodType
                    ]
            ]
            array.append(data)
        }
        
        let params : [String : Any] =    [
            "from": 0,
            "size": 10,
            "query": [
                "bool": [
                    "must": array
                ]
            ]
        ]
        let url = APIEndTails.SearchStore
        let temp = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var header = Utility.getHeader()
       // header.updateValue("\(Utility.getAddress().Latitude)", forKey: APIRequestParams.Latitude)
       //  header.updateValue("\(Utility.getAddress().Longitude)", forKey: APIRequestParams.Longitude)
        header[APIRequestParams.Latitude] = "\(Utility.getAddress().Latitude)"
        header[APIRequestParams.Longitude] = "\(Utility.getAddress().Longitude)"

       
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, temp!, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head,body) in
                let bodyIn = body as! [String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var arrayOfStores = [Store]()
                    if let array = bodyIn[APIResponceParams.Data] as? [Any] {
                        for dataFrom in array {
                            let data = Store.init(data:dataFrom as! [String:Any])
                            arrayOfStores.append(data)
                        }
                    }
                    observer.onNext(arrayOfStores)
                    observer.onCompleted()
                }else{
                    observer.onNext([])
                    observer.onCompleted()
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    
    
    class func filterStore(needle:String,cousine:[String],sort:Int,foodType:[String],language:String) -> Observable<[Store]> {
        print("Search text" + needle)
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        
        var array:[Any] = [
            [
                "match": [
                    "status": 1
                ]
            ],
            [
                "match": [
                    "serviceZones": Utility.getAddress().ZoneId
                ]
            ],
            [
                "match": [
                    "storeCategory.categoryId": Utility.getSelectedSuperStores().Id
                ]
            ],
            [
                "match": [
                    "storeType": Utility.getSelectedSuperStores().type.rawValue
                ]
            ]
        ]
        
        if needle.length > 0 {
            let data = [
                "match_phrase_prefix": [
                    "sName." + language : needle
                ]
            ]
            array.append(data)
        }
        
        
        var cuisineNames = ""
        if cousine.count > 0 {
            cuisineNames = cousine[0]
            if cousine.count > 1 {
                for i in 1...cousine.count - 1 {
                    cuisineNames = cuisineNames + "," + cousine[i]
                }
            }
        }
        
        if cuisineNames.length > 0 {
            let dataIt = [
                "match": [
                    "storeSubCategory.subCategoryName.\(language)" : cuisineNames
                ]
            ]
            array.append(dataIt)
        }
        
        var params : [String : Any] =    [
            "from": 0,
            "size": 10,
            "query": [
                "bool": [
                    "must": array
                ]
            ]
        ]
        
        if sort >= 0 {
            var dataIt:[String:Any] = [:]
            switch sort {
            case 0 :
                dataIt = ["averageRating": ["order": "desc"]]
                break
            case 1 :
                dataIt = ["costForTwo": ["order": "asc"]]
                break
            default:
                break
            }
            params.updateValue(dataIt, forKey: "sort")
        }
        
        let url = APIEndTails.SearchStore
        let temp = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var header = Utility.getHeader()
//        header.updateValue("\(Utility.getAddress().Latitude)", forKey: APIRequestParams.Latitude)
//        header.updateValue("\(Utility.getAddress().Longitude)", forKey: APIRequestParams.Longitude)
        header[APIRequestParams.Latitude] = "\(Utility.getAddress().Latitude)"
          header[APIRequestParams.Longitude] = "\(Utility.getAddress().Longitude)"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, temp!, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head,body) in
                let bodyIn = body as! [String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var arrayOfStores = [Store]()
                    if let array = bodyIn[APIResponceParams.Data] as? [Any] {
                        for dataFrom in array {
                            let data = Store.init(data:dataFrom as! [String:Any])
                            arrayOfStores.append(data)
                        }
                    }
                    observer.onNext(arrayOfStores)
                    observer.onCompleted()
                }else{
                    observer.onNext([])
                    observer.onCompleted()
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    
    class func searchProductName(needle:String,language:String) -> Observable<[String]> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        
        var keyParma = ""
        var keyValue = ""
        
        if Utility.getStore().Id.sorted().count == 0{
            keyParma = "zoneId"
            keyValue = Utility.getAddress().ZoneId
        }else{
            keyParma = "storeId"
            keyValue = Utility.getStore().Id
        }
        
        
        let params : [String : Any] =    [
            "from": 0,
            "size": 10,
            "query": [
                "bool": [
                    "must": [
                        [
                        "match": [
                            "status": 1
                            ]
                        ],
                        [
                        "match_phrase_prefix": [
                        "productname." + language : needle

                            ]
                        ],
                        [
                        "match": [
                            keyParma: keyValue
                            ]
                        ],
                        [
                          "match": [
                            "storeType": Utility.getSelectedSuperStores().type.rawValue
                            ]
                        ]
                    ]
                ]
            ]
        ]
        var header = Utility.getHeader()
      //  header.updateValue(language, forKey: APIRequestParams.Language)
        header[APIRequestParams.Language] = "\(language)"
         return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.SearchProductName, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head,body) in
                let bodyIn = body as! [String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var arrayOfItem = [String]()
                    if let array = bodyIn[APIResponceParams.Data] as? [Any]{
                        arrayOfItem = array as! [String]
                    }
                    observer.onNext(arrayOfItem)
                    observer.onCompleted()
                }else{
                    observer.onNext([])
                    observer.onCompleted()
//                    APICalls.basicParsing(data:body as! [String : Any] ,status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    class func popularSearchFilter(needle:String,language:String) -> Observable<([String],[Store])> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let params : [String : Any] =    [
            "from": 0,
            "size": 10,
            "query": [
                "bool": [
                    "must": [
                        [
                            "match": [
                                "status": 1
                            ]
                        ],
                        [
                            "match_phrase_prefix": [
                                "productname." + language : needle
                                
                            ]
                        ],
                        [
                            "match": [
                                "storeType": Utility.getSelectedSuperStores().type.rawValue
                            ]
                        ],
                        [
                            "match": [
                                "zoneId": Utility.getAddress().ZoneId
                            ]
                        ]
                    ]
                ]
            ]
        ]
        var header = Utility.getHeader()
        header[APIRequestParams.Language] = "\(language)"
        header[APIRequestParams.Latitude] = "\(Utility.getAddress().Latitude)"
        header[APIRequestParams.Longitude] = "\(Utility.getAddress().Longitude)"

      //  header.updateValue(language, forKey: APIRequestParams.Language)
      //  header.updateValue("\(Utility.getAddress().Latitude)", forKey: APIRequestParams.Latitude)
     //   header.updateValue("\(Utility.getAddress().Longitude)", forKey: APIRequestParams.Longitude)
        header[APIRequestParams.searchedItem] = needle
        header[APIRequestParams.storeCategoryId] = Utility.getSelectedSuperStores().Id
        header[APIRequestParams.storeType] = String(describing: Utility.getSelectedSuperStores().type.rawValue)
        header[APIRequestParams.ZoneId] = Utility.getAddress().ZoneId
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.PopularSearchFilter, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head,body) in
                let bodyIn = body as! [String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var arrayOfItem = [String]()
                    var arrayOfRestuarants = [Store]()
                    if let array = bodyIn[APIResponceParams.Data] as? [Any]{
                        arrayOfItem = array as! [String]
                    }
                    if let array = bodyIn[APIResponceParams.ResturantData] as? [Any]{
                        for rest in array{
                            if let data = rest as? [String:Any]{
                                arrayOfRestuarants.append(Store.init(data: data))
                            }
                        }
                    }
                    observer.onNext((arrayOfItem,arrayOfRestuarants))
                    observer.onCompleted()
                }else{
                    observer.onNext(([],[]))
                    observer.onCompleted()
                    //                    APICalls.basicParsing(data:body as! [String : Any] ,status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    class func searchProductPopular() -> Observable<[String]> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        var keyParma = ""
        var keyValue = ""

        
        let selectedSuperStore = Utility.getSelectedSuperStores()
        
      
        
        if Utility.getStore().Id.sorted().count == 0{
            keyParma = "zoneId"
            keyValue = Utility.getAddress().ZoneId
        }else{
            keyParma = "storeId"
            keyValue = Utility.getStore().Id
        }
        let params : [String : Any] =    [
            "from": 0,
            "size": 10,
            "query": [
                "bool": [
                    "must": [
                    ["match":  [keyParma: keyValue]],
                    ["match":  ["storeCategoryId": selectedSuperStore.Id]],
                    ["match":  [ "storeType": String(selectedSuperStore.type.rawValue)]]
                        ]]],
             "sort": [
                [
                    "count": [
                        "order": "desc"
                    ]
                ]
            ]
        ]
        
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.SearchPopular, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head,body) in
                let bodyIn = body as! [String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var arrayOfItem = [String]()
                    if let array = bodyIn[APIResponceParams.Data] as? [Any]{
                        arrayOfItem = array as! [String]
                        Utility.saveRecentSearch(data: arrayOfItem)
                    }
                    observer.onNext(arrayOfItem)
                    observer.onCompleted()
                    
                }else{
                    observer.onNext([])
                    observer.onCompleted()
//                    APICalls.basicParsing(data:body as! [String : Any] ,status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    
    class func searchProduct(needle:String,productName:String,brand:[String],manufacturer:[String],category:String,subcat:String,subsubCat:String,minPrice:Float,maxPrice:Float,offer:String,sort:Int,searchFrom:Int,language:String , isStoreSearch : Bool ) -> Observable<(Store,[Store])> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        var brandNames = ""
        if brand.count > 0 {
            brandNames = brand[0]
            if brand.count > 1 {
                for i in 1...brand.count - 1 {
                    brandNames = brandNames + "," + brand[i]
                }
            }
        }
        
        var manufacturerNames = ""
        if manufacturer.count > 0 {
            manufacturerNames = manufacturer[0]
            if manufacturer.count > 1 {
                for i in 1...manufacturer.count - 1 {
                    manufacturerNames = manufacturerNames + "," + manufacturer[i]
                }
            }
        }
        
        var array:[[String : Any]] =  [[
            "match": [
                "status": 1
            ]
            ]]
        if searchFrom != 1 {
            let dataIn = [
                "match": [
                    "zoneId": Utility.getAddress().ZoneId
                ]
            ]
            array.append(dataIn)
        }
        
        if searchFrom != 0 {
            let dataIn = [
                "match": [
                    "storeId": Utility.getStore().Id
                ]
            ]
            array.append(dataIn)
        }
        if needle.length > 0 {
            let dataIn = [
                "match_phrase_prefix": [
                    "productname." + language : needle
                ]
            ]
            array.append(dataIn)
        }
        if brandNames.length > 0 {
            let dataIt = [
                "match": [
                    "brandTitle." + language : brandNames
                ]
            ]
            array.append(dataIt)
        }
        if manufacturerNames.length > 0 {
            let dataIt = [
                "match": [
                    "manufactureName." + language : manufacturerNames
                ]
            ]
            array.append(dataIt)
        }
        if category.length > 0 {
            let dataIt = [
                "match": [
                    "catName." + language : category
                ]
            ]
            array.append(dataIt)
        }
        if subcat.length > 0 {
            let dataIt = [
                "match": [
                    "subCatName." + language  : subcat
                ]
            ]
            array.append(dataIt)
        }
        if subsubCat.length > 0 {
            let dataIt = [
                "match": [
                    "subSubCatName." + language : subsubCat
                ]
            ]
            array.append(dataIt)
        }
        if maxPrice > 0 {
            let dataIt = [
                "range": [
                    "units.floatValue": [
                        "gte": minPrice,
                        "lte": maxPrice
                        ]
                    ]
                ]
            array.append(dataIt)
        }
        if offer.length > 0 {
            let dataIt = [
                "match": [
                    "offer.status": 1
                    ]
                ]
            array.append(dataIt)
        }
        var params : [String : Any] =    [
            "from": 0,
            "size": 20,
            "query": [
                "bool": [
                    "must": array
                ]
            ]
        ]
        if sort >= 0 {
            var dataIt:[String:Any] = [:]
            switch sort {
            case 0 :
                dataIt = ["popular": ["order": "asc"]]
                break
            case 1 :
                dataIt = ["createdTimestamp": ["order": "asc"]]
                break
            case 2 :
                dataIt = ["units.floatValue": ["order": "asc"]]
                break
            case 3 :
                dataIt = ["units.floatValue": ["order": "desc"]]
                break
            default:
                break
            }
            params.updateValue(dataIt, forKey: "sort")
        }
        var header = Utility.getHeader()
        if productName.length > 0 {
          //  header.updateValue(productName, forKey: "searchedItem")
            header["searchedItem"] = productName

        }
        header["storeId"] = Utility.getStore().Id
       // header.updateValue(Utility.getStore().Id, forKey: "storeId")
        var storeId = "0"
        if Utility.getStore().Id.sorted().count > 0 && isStoreSearch{
            storeId = Utility.getStore().Id
        }
        header["storeId"] = storeId
      //  header.updateValue(storeId, forKey: "storeId")
        header["zoneId"] = Utility.getAddress().ZoneId
   //     header.updateValue(Utility.getAddress().ZoneId, forKey: "zoneId")
        
        let selectedSuperstore = Utility.getSelectedSuperStores()
        header["storeCategoryId"] = selectedSuperstore.Id

      //  header.updateValue(selectedSuperstore.Id, forKey: "storeCategoryId")
                header["storeType"] = String(selectedSuperstore.type.rawValue)
      //  header.updateValue(String(selectedSuperstore.type.rawValue), forKey: "storeType")
        header[APIRequestParams.Language] = language

      //  header.updateValue(language, forKey: APIRequestParams.Language)

        if searchFrom == 1 {
            header["popularStatus"] = "\(searchFrom)"

          //  header.updateValue("\(searchFrom)", forKey: "popularStatus")
        }else{
            header["popularStatus"] = "\(0)"

           // header.updateValue("\(0)", forKey: "popularStatus")
        }
        let url = APIEndTails.SearchProducts
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, url, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head,body) in
                let bodyIn = body as! [String:Any]
                
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    if let data = bodyIn[APIResponceParams.Data] as? [String:Any] {
                        var array:[Item] = []
                        
                        if let data2 = data[APIResponceParams.Products] as? [Any] {
                            for item in data2 {
                                array.append(Item.init(data: item as! [String : Any], store: Utility.getStore()))
                            }
                        }
                        var array2:[Store] = []
                        if let data2 = data[APIResponceParams.Stores] as? [Any] {
                            for item in data2 {
                                array2.append(Store.init(data: item as! [String : Any]))
                            }
                        }
                        let store = Utility.getStore()
                        store.Products.removeAll()
                        store.Products.append(contentsOf: array)
                        observer.onNext((store,array2))
                        observer.onCompleted()
                    }
                }else {//if errNum == .NotFound {
                    
                    let store = Utility.getStore()
                    observer.onNext((store,[]))
                    observer.onCompleted()
//                }else{
//                    APICalls.basicParsing(data:body as! [String : Any] ,status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    class func filterProducts(needle:String,productName:String,brand:[String],manufacturer:[String],category:String,subcat:String,subsubCat:String,minPrice:Float,maxPrice:Float,offer:String,sort:Int,searchFrom:Int,language:String , isStoreSearch : Bool ) -> Observable<(Store,[Store])> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        var brandNames = ""
        if brand.count > 0 {
            brandNames = brand[0]
            if brand.count > 1 {
                for i in 1...brand.count - 1 {
                    brandNames = brandNames + "," + brand[i]
                }
            }
        }
        
        var manufacturerNames = ""
        if manufacturer.count > 0 {
            manufacturerNames = manufacturer[0]
            if manufacturer.count > 1 {
                for i in 1...manufacturer.count - 1 {
                    manufacturerNames = manufacturerNames + "," + manufacturer[i]
                }
            }
        }
        
        var array:[[String : Any]] =  [[
            "match": [
                "status": 1
            ]
            ]]
        if searchFrom != 1 {
            let dataIn = [
                "match": [
                    "zoneId": Utility.getAddress().ZoneId
                ]
            ]
            array.append(dataIn)
        }
        
        if searchFrom != 0 {
            let dataIn = [
                "match": [
                    "storeId": Utility.getStore().Id
                ]
            ]
            array.append(dataIn)
        }
        if needle.length > 0 {
            let dataIn = [
                "match_phrase_prefix": [
                    "productname." + language : needle
                ]
            ]
            array.append(dataIn)
        }
        if brandNames.length > 0 {
            let dataIt = [
                "match": [
                    "brandTitle." + language : brandNames
                ]
            ]
            array.append(dataIt)
        }
        if manufacturerNames.length > 0 {
            let dataIt = [
                "match": [
                    "manufactureName." + language : manufacturerNames
                ]
            ]
            array.append(dataIt)
        }
        if category.length > 0 {
            let dataIt = [
                "match_phrase_prefix": [
                    "catName." + language : category
                ]
            ]
            array.append(dataIt)
        }
        if subcat.length > 0 {
            let dataIt = [
                "match_phrase_prefix": [
                    "subCatName." + language  : subcat
                ]
            ]
            array.append(dataIt)
        }
        if subsubCat.length > 0 {
            let dataIt = [
                "match_phrase_prefix": [
                    "subSubCatName." + language : subsubCat
                ]
            ]
            array.append(dataIt)
        }
        if maxPrice > 0 {
            let dataIt = [
                "range": [
                    "units.floatValue": [
                        "gte": minPrice,
                        "lte": maxPrice
                    ]
                ]
            ]
            array.append(dataIt)
        }
        if offer.length > 0 || sort == 4 {
            let dataIt = [
                "match": [
                    "offer.status": 1
                ]
            ]
            array.append(dataIt)
        }
        var params : [String : Any] =    [
            "from": 0,
            "size": 20,
            "query": [
                "bool": [
                    "must": array
                ]
            ]
        ]
        
        var isPopularQuery = 0
        if sort >= 0 {
            var dataIt:[String:Any] = [:]
            switch sort {
            case 0 :
                dataIt = ["count": ["order": "asc"]]
                isPopularQuery = 2
                break
            case 1 :
                dataIt = ["createdTimestamp": ["order": "asc"]]
                break
            case 2 :
                dataIt = ["units.floatValue": ["order": "asc"]]
                break
            case 3 :
                dataIt = ["units.floatValue": ["order": "desc"]]
                break
            default:
                break
            }
            params.updateValue(dataIt, forKey: "sort")
        }
        var header = Utility.getHeader()
        if productName.length > 0 {
            header["searchedItem"] = productName
           // header.updateValue(productName, forKey: "searchedItem")
        }
        header["storeId"] = Utility.getStore().Id

       // header.updateValue(Utility.getStore().Id, forKey: "storeId")
        var storeId = "0"
        if Utility.getStore().Id.sorted().count > 0 && isStoreSearch{
            storeId = Utility.getStore().Id
        }
        header["storeId"] = storeId

    //    header.updateValue(storeId, forKey: "storeId")
        header["zoneId"] = Utility.getAddress().ZoneId

       // header.updateValue(Utility.getAddress().ZoneId, forKey: "zoneId")
        
        let selectedSuperstore = Utility.getSelectedSuperStores()
        header["storeCategoryId"] = selectedSuperstore.Id

       // header.updateValue(selectedSuperstore.Id, forKey: "storeCategoryId")
        header["storeType"] = String(selectedSuperstore.type.rawValue)

     //   header.updateValue(String(selectedSuperstore.type.rawValue), forKey: "storeType")
      //  header.updateValue(language, forKey: APIRequestParams.Language)
        header[APIRequestParams.Language] = language
        
        if searchFrom == 1 {
            header["popularStatus"] = "\(searchFrom)"
           // header.updateValue("\(searchFrom)", forKey: "popularStatus")
        }else{
            header["popularStatus"] = "\(isPopularQuery)"
         //   header.updateValue("\(isPopularQuery)", forKey: "popularStatus")
        }
        let url = APIEndTails.SearchProducts
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, url, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head,body) in
                let bodyIn = body as! [String:Any]
                
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    if let data = bodyIn[APIResponceParams.Data] as? [String:Any] {
                        var array:[Item] = []
                        
                        if let data2 = data[APIResponceParams.Products] as? [Any] {
                            for item in data2 {
                                array.append(Item.init(data: item as! [String : Any], store: Utility.getStore()))
                            }
                        }
                        var array2:[Store] = []
                        if let data2 = data[APIResponceParams.Stores] as? [Any] {
                            for item in data2 {
                                array2.append(Store.init(data: item as! [String : Any]))
                            }
                        }
                        let store = Utility.getStore()
                        store.Products.removeAll()
                        store.Products.append(contentsOf: array)
                        observer.onNext((store,array2))
                        observer.onCompleted()
                    }
                }else {//if errNum == .NotFound {
                    
                    let store = Utility.getStore()
                    observer.onNext((store,[]))
                    observer.onCompleted()
                    //                }else{
                    //                    APICalls.basicParsing(data:body as! [String : Any] ,status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
}
