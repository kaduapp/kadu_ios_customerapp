//
//  TrackingUIE.swift
//  DelivX
//
//  Created by 3 Embed on 23/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension TrackingVC {
    
    
    /// initial View Setup
    func setUI() {
        orderID.textColor = Colors.PrimaryText
        orderTiming.textColor = Colors.SecoundPrimaryText
        Fonts.setPrimaryMedium(orderID)
        Fonts.setPrimaryRegular(orderTiming)
        detailButton.tintColor = Colors.AppBaseColor
        Helper.setUiElementBorderWithCorner(element: detailButton, radius: Helper.setRadius(element: detailButton), borderWidth: 1.5, color:Colors.AppBaseColor)
        
        navigationWidth.constant = UIScreen.main.bounds.size.width - 20
        trackingView.mainTableView.rowHeight = UITableView.automaticDimension
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        
    }
    
    
    /// initial View setup
    func initialSetup() {
        didconnectMQTTResponse()
        
        if dataOrder?.DriverLat == 0 && dataOrder?.DriverLong == 0{
            dataOrder?.DriverLat = (dataOrder?.PickLat)!
            dataOrder?.DriverLong = (dataOrder?.PickLong)!
            
        }

        trackingView.drawPath(data: dataOrder!)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            self.trackingView.trackPath(data: self.dataOrder!)
        }

        let Bid:Int = (dataOrder?.BookingId)!
        orderID.text = "\(Bid)"
        
        let itemCount = String(describing: dataOrder!.ItemsData.count)
        let amount = Helper.df2so(Double( dataOrder!.TotalAmount))
        let temp = "|"
        let dateString = Helper.getDateString(value: (dataOrder!.BookingDate), format: DateFormat.TimeFormatToDisplay, zone: true)
        orderTiming.text = dateString + " " + temp + " " + itemCount + " " + StringConstants.Items() + " " + amount
    }
}

