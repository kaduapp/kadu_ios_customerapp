//
//  WishListUIE.swift
//  DelivX
//
//  Created by 3Embed on 06/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
extension WishListVC {
    
    func setDataUI() {
        Helper.transparentNavigation(controller: self.navigationController!)
        
    }
}


extension WishListVC{//: UIViewControllerTransitioningDelegate {
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        if selected != nil {
//            transition.originFrame = selected!.superview!.convert(selected!.frame, to: nil)
//            transition.presenting = true
//        }
//        return transition
//    }
//
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        selected = nil
//        transition.presenting = false
//        return transition
//    }
    
    func setEmptyScreen() {
        var image = #imageLiteral(resourceName: "EmptyFavourite")
        var inTitle = StringConstants.EmptyFav()
        if wishListVM.wishList.ID.length > 0 {
            image = #imageLiteral(resourceName: "EmptyWishlist")
            inTitle = StringConstants.EmptyWishItems()
            
        }
        var str = StringConstants.StartOrdering()
        if wishListVM.wishList.ID.length > 0{
            str = StringConstants.StartShopping()
        }
        emptyView.setData(image: image, title: inTitle, buttonTitle: str,buttonHide: false)
        emptyView.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
    }
    
    @objc func StartShopping() {
        self.dismiss(animated: true, completion: nil)
       // self.navigationController?.popToRootViewController(animated: true)
       // Helper.changetTabTo(index: AppConstants.Tabs.Home)
    }
}
