//
//  SelectLocationUIE.swift
//  UFly
//
//  Created by 3 Embed on 08/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension SelectLocationVC {
    
    // setup the View
    func setUI() {
        emptyScreenLabel.textColor  =  Colors.PrimaryText
        Fonts.setPrimaryRegular(emptyScreenLabel)
        self.addressSearchBar.tintColor = Colors.PrimaryText
        selectLocVM.locationManager = LocationManager.shared
        selectLocVM.locationManager?.delegate = self
        let locationSelected = Utility.getAddress()
        let textFieldInsideSearchBar = self.addressSearchBar.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.Semantic()
        addressSearchBar.placeholder = StringConstants.SearchAddress()
        textFieldInsideSearchBar.font = UIFont.systemFont(ofSize: 14)
        Fonts.setPrimaryRegular(textFieldInsideSearchBar)
        mainTableView.backgroundColor = Colors.ScreenBackground
        if locationSelected.FullText.length == 0 {
            buttonWidthConstraint.constant = 10
            headView.updateConstraints()
            headView.layoutIfNeeded()
            closeButton.isHidden = true
        }
        addressSearchBar.becomeFirstResponder()
        emptyView.setData(image: #imageLiteral(resourceName: "NotDelivering"), title: StringConstants.NotInLocality(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
    }
    
    
}
