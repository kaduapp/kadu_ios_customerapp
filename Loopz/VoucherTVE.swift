//
//  VoucherTVE.swift
//  DelivX
//
//  Created by 3Embed on 11/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
// MARK: - Tableview Delegate
extension VoucherVC : UITableViewDelegate, UITableViewDataSource {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: VoucherTVC.self), for: indexPath) as! VoucherTVC
        cell.textField.becomeFirstResponder()
        cell.textField.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}
