//
//  UnitView.swift
//  DelivX
//
//  Created by 3 Embed on 05/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class UnitView: UIView {
    
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var unitBottomSeperator: UIView!
    @IBOutlet weak var unitCollectionView: UICollectionView!
    
    var selectedItem:Item? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    func initialSetup(){
       // Fonts.setPrimaryMedium(unit)
       // unit.text = StringConstants.Unit
        unitBottomSeperator.backgroundColor = Colors.SeparatorLight
    }
    
    func updateUnit(){
        
    }
    
}
