//
//  HelpTVE.swift
//  DelivX
//
//  Created by 3 Embed on 11/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

// MARK: - UITableViewDataSource
extension HelpVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0
        case 1:
            return 0
        default:
            return helpVM.arrayOfSupport.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.HelpTableCell) as! HelpTableCell
            cell.updateCell()
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.HelpTitleCell) as! HelpTitleCell
            
            if indexPath.section == 1 {
                cell.updateCell1()
            }
            if indexPath.section == 2
            {
                cell.updateCell(data: helpVM.arrayOfSupport[indexPath.row])
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView:HeaderView? = nil
        var stringHead = ""
        switch section {
        case 0:
            stringHead = StringConstants.HelpWithOrder()
        case 1:
            stringHead = ""
        
        default:
            stringHead = StringConstants.HelpWithQueries()
        }
        headerView = Helper.showHeader(title: stringHead,showViewAll: false) as? HeaderView
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        case 1:
            return 0
        default:
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 2 {
            sectionRow = indexPath.row
            if helpVM.arrayOfSupport[indexPath.row].SubCat.count == 0 {
                self.performSegue(withIdentifier: UIConstants.SegueIds.HelpToWeb, sender: helpVM.arrayOfSupport[indexPath.row])
                return
            }
        }
        selectedSection = indexPath.section
        performSegue(withIdentifier: UIConstants.SegueIds.HelpToHelpDetail, sender: helpVM.arrayOfSupport[indexPath.row])
    }
 
}
