//
//  EditProfileAPICalls.swift
//  UFly
//
//  Created by 3 Embed on 01/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire

class ProfileAPICalls: NSObject {
    
    static let disposeBag = DisposeBag()
    // getProfileData
    class func getProfile() -> Observable<Bool>{
        let header = Utility.getHeader()
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
//         Helper.showPI(string: StringConstants.Loading())
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, APIEndTails.Profile, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                let user:User? = Utility.getProfileData()
                if errNum == .success {
                    let bodyIn = body as![String:Any]
                    let data = bodyIn[APIResponceParams.Data] as! [String:Any]
                    user?.updateData(data: data, tool: true)
                    
                    observer.onNext(true)
                    observer.onCompleted()
                    
                }else if errNum == .Required {
                    observer.onNext(false)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
                
            }, onError: { (Error) in
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //EditProfile Data
    
    class func updateProfileData(user: Authentication) -> Observable<Bool>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        var params = [String:Any]()
        
        if  user.Phone.length != 0 {
            params.updateValue(user.Phone, forKey: APIRequestParams.Phone)
            params.updateValue(user.CountryCode, forKey: APIRequestParams.CountryCode)
        }
        if user.Name.length != 0 {
            params.updateValue(user.Name, forKey: APIRequestParams.FirstName)
        }
        if user.Email.length != 0 {
            params.updateValue(user.Email, forKey: APIRequestParams.Email)
        }
        if user.ProfilePic.length != 0 {
            params.updateValue(user.ProfilePic, forKey: APIRequestParams.ProfilePic)
        }
        if user.Password.length != 0 {
            params.updateValue(user.Password, forKey: APIRequestParams.Password)
        }
        if user.ProfilePic.length != 0 {
            params.updateValue(user.ProfilePic, forKey: APIRequestParams.ProfilePic)
        }
        if Utility.getProfileData().MMJCard.Url != user.MMJUrl && user.MMJUrl.length > 0 {
            params.updateValue(user.MMJUrl, forKey: APIResponceParams.MMJCard)
        }
        if Utility.getProfileData().IdCard.Url != user.IDUrl && user.IDUrl.length > 0 {
            params.updateValue(user.IDUrl, forKey: APIResponceParams.IDCard)
        }
        
        if params.keys.count == 0 {
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Loading())
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.Profile, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let bodyIn = body as![String:Any]
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    getProfile().subscribe().disposed(by: self.disposeBag)
                    observer.onNext(true)
                    observer.onCompleted()
                }
                APICalls.basicParsing(data: bodyIn, status: head.statusCode)
            }, onError: { (Error) in
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
        
    }
    
    
}
