//
//  SearchTFE.swift
//  UFly
//
//  Created by 3Embed on 18/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import Foundation


// MARK: - UITextFieldDelegate methods
extension SearchVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
           self.showCancelBtn()
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.hideCancelBtn()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        productNameTable.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField.text?.length)! >= 3 {
            self.performSegue(withIdentifier: String(describing: SearchDetailsVC.self), sender: textField.text!)
        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        if textField.text != "" {
            lang = Helper.getKeyboardLanguage(language: (textField.textInputMode?.primaryLanguage)!)
            searchVM.getProduct(text: textField.text!,language: lang)
        } else {
            searchVM.arrayOfItems.removeAll()
            productNameTable.reloadData()
        }
    }
}
