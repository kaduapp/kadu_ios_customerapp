//
//  PaymentTVE.swift
//  UFly
//
//  Created by 3 Embed on 16/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//
import UIKit

// MARK: - UITableViewDelegate
extension PaymentVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.isComingCheckout == true {
            self.responseData.onNext(cardArray[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        }else{
            selectedCard = indexPath.row
           if paymentMethod == 1 {
            self.performSegue(withIdentifier: UIConstants.SegueIds.AllCardsToDetails, sender: cardArray[indexPath.row])
            }
        }
        tableView.reloadData()
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if cardArray.count > 0 {
            tableView.backgroundView = nil
            return 1
        }else{
            if loaded {
                tableView.backgroundView = emptyView
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.PaymentTableCell, for: indexPath) as! PaymentTableCell


        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(PaymentVC.deleteCard), for: .touchUpInside)
        
        cell.deleteButton.isUserInteractionEnabled = true
        cell.updatePaymentCell(data: cardArray[indexPath.row])
        if paymentMethod == 3 {
            cell.setSelectedCard(data: cardArray[indexPath.row],id: cardArray[selectedCard].Id)
            cell.deleteButton.isUserInteractionEnabled = false
        }else if paymentMethod == 1 {
            if cardArray[indexPath.row].Default == 1 {
                cell.deleteButton.isHidden = false
            }
            else {
                cell.deleteButton.isHidden = true
            }
        }
        if indexPath.row == cardArray.count - 1{
        cell.cellseperator.isHidden = true
        }
        else {
            cell.cellseperator.isHidden = false
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return Helper.showHeader(title: StringConstants.SavedCards(), showViewAll: false)
    }
    
    @objc func deleteCard(_sender:UIButton) {
        AddNewCardVM().deleteCard(card: cardArray[_sender.tag])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            AddNewCardVM().deleteCard(card: cardArray[indexPath.row])
        }
    }
}



