
//
//  User.swift
//  UFly
//
//  Created by 3Embed on 18/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
struct VerifiedImage {
    var Verified = false
    var Url = ""
    var data:[String:Any] = [:]
}

class User: NSObject {
    
    var CountryCode                         = ""
    var Email                               = ""
    var FirstName                           = StringConstants.Login()
    var Phone                               = ""
    var SId                                 = ""
    var FCMTopic                            = ""
    var MQTTTopic                           = ""
    var RefferalCode                        = ""
    var userProfilePic                      = ""
    var IdCard                              = VerifiedImage()
    var MMJCard                             = VerifiedImage()
    var TicketID                            = ""
    var googleMapKeyMqtt                    = ""
    //"dpmName" : "Wilton",
    //suggestedDPName" : "Avinash S" new keys
    
    init(data:[String:Any]) {
        super.init()
     self.updateData(data: data,tool: false)
        if let titleTemp = data[APIResponceParams.CountryCode] as? String{
            CountryCode                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ReferralCode] as? String{
            RefferalCode                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FCMTopic] as? String{
            FCMTopic             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.MQTTTopic] as? String{
            MQTTTopic             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.googleMapKeyMqtt] as? String{
            googleMapKeyMqtt             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Sid] as? String{
            SId                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.TicketID] as? String{
            TicketID             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.TicketID] as? Int{
            TicketID             = String(titleTemp)
        }
    }
    func updateData(data:[String:Any] ,tool:Bool){
        if let titleTemp = data[APIResponceParams.TicketID] as? String{
            TicketID             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.TicketID] as? Int{
            TicketID             = String(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.FirstName] as? String{
            FirstName             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Email] as? String{
            Email                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Phone] as? String{
            Phone                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ProfilePic] as? String{
            userProfilePic                         = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.MMJCard] as? [String:Any]{
            MMJCard.data = titleTemp
            if let titleTemp1 = titleTemp[APIResponceParams.URL] as? String{
                MMJCard.Url                         = titleTemp1
            }
            if let titleTemp1 = titleTemp[APIResponceParams.Verified] as? Bool{
                MMJCard.Verified                         = titleTemp1
            }
        }
        if let titleTemp = data[APIResponceParams.IDCard] as? [String:Any]{
            IdCard.data = titleTemp
            if let titleTemp1 = titleTemp[APIResponceParams.URL] as? String{
                IdCard.Url                         = titleTemp1
            }
            if let titleTemp1 = titleTemp[APIResponceParams.Verified] as? Bool{
                IdCard.Verified                         = titleTemp1
            }
        }
        
        if tool {
            let dict = [
                APIResponceParams.CountryCode   :   CountryCode,
                APIResponceParams.ReferralCode  :   RefferalCode,
                APIResponceParams.FCMTopic      :   FCMTopic,
                APIResponceParams.MQTTTopic     :   MQTTTopic,
                APIResponceParams.Sid           :   SId,
                APIResponceParams.FirstName     :   FirstName,
                APIResponceParams.Email         :   Email,
                APIResponceParams.Phone         :   Phone,
                APIResponceParams.ProfilePic    :   userProfilePic,
                APIResponceParams.MMJCard       :   MMJCard.data,
                APIResponceParams.IDCard        :   IdCard.data,
                APIResponceParams.TicketID      :   TicketID,
                "googleMapKeyMqtt"              :   self.googleMapKeyMqtt
                ] as [String : Any]
            Utility.saveProfileData(data: dict)
        }
        
    }
    
}
