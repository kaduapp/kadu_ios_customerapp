//
//  CartCollectionCell.swift
//  UFly
//
//  Created by 3 Embed on 22/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CartCollectionCell: UICollectionViewCell,UIGestureRecognizerDelegate{
    @IBOutlet weak var customisationButton: UIButton!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemWeight: UILabel!
    
    @IBOutlet weak var oldPriceCutView: UIView!
    
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var newPrice: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var numberOfItems: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var AddOnTableView: UITableView!
    @IBOutlet weak var addOnTableHeightFromcartVc: NSLayoutConstraint!
    
    @IBOutlet weak var addOnsLabel: UILabel!
     @IBOutlet weak var outOfStock: UIButton!
    
    var pan: UIPanGestureRecognizer!
    var deleteLabel1: UILabel!
    var deleteLabel2: UILabel!
    //variable
    var observeBack = PublishSubject<Bool>()
    var dataItem:CartItem? = nil
    var cartDb = CartDBManager()
    let cartVM = CartVM()
    var dataOfAddOns:[AddOn] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.contentView.backgroundColor = UIColor.gray
        self.backgroundColor = UIColor.red
        deleteLabel1 = UILabel()
        deleteLabel1.text = StringConstants.DeleteSmall()
        deleteLabel1.textColor = UIColor.white
        self.insertSubview(deleteLabel1, belowSubview: self.contentView)
        
        deleteLabel2 = UILabel()
        deleteLabel2.text = StringConstants.DeleteSmall()
        deleteLabel2.textColor = UIColor.white
        self.insertSubview(deleteLabel2, belowSubview: self.contentView)
        
        pan = UIPanGestureRecognizer(target: self, action: #selector(onPan(_:)))
        pan.delegate = self
        self.addGestureRecognizer(pan)
    }
    
    override func prepareForReuse() {
        self.contentView.frame = self.bounds
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if (pan.state == UIGestureRecognizer.State.changed) {
            let p: CGPoint = pan.translation(in: self)
            let width = self.contentView.frame.width
            let height = self.contentView.frame.height
            self.contentView.frame = CGRect(x: p.x,y: 0, width: width, height: height);
            self.deleteLabel1.frame = CGRect(x: p.x - deleteLabel1.frame.size.width-10, y: 0, width: 100, height: height)
            self.deleteLabel2.frame = CGRect(x: p.x + width + deleteLabel2.frame.size.width, y: 0, width: 100, height: height)
        }
        
    }
    
    @objc func onPan(_ pan: UIPanGestureRecognizer) {

        if pan.state == UIGestureRecognizer.State.began {
        }
        else if pan.state == UIGestureRecognizer.State.changed {
            self.setNeedsLayout()
        }
        else if abs(pan.velocity(in: self).x) > 500  {
                let collectionView: UICollectionView = self.superview as! UICollectionView
                let indexPath: IndexPath = collectionView.indexPathForItem(at: self.center)!
                collectionView.delegate?.collectionView!(collectionView, performAction: #selector(onPan(_:)), forItemAt: indexPath, withSender: nil)
            } else {
             
                UIView.animate(withDuration: 0.2, animations: {
                    self.setNeedsLayout()
                    self.layoutIfNeeded()
                })
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return abs((pan.velocity(in: pan.view)).x) > abs((pan.velocity(in: pan.view)).y)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    /// initial view setup
    func initialSetup(){
        
        outOfStock.setTitleColor(UIColor.red, for: UIControl.State.normal)
   Helper.setButtonTitle(normal:StringConstants.outOfStock(),highlighted:StringConstants.outOfStock(),selected:StringConstants.outOfStock(),button:outOfStock)
        Fonts.setPrimaryRegular(addOnsLabel)
        Fonts.setPrimaryMedium(itemName)
        Fonts.setPrimaryRegular(itemWeight)
        Fonts.setPrimaryBold(numberOfItems)
        Fonts.setPrimaryMedium(newPrice)
        Fonts.setPrimaryRegular(oldPriceLabel)
        
        Helper.setUiElementBorderWithCorner(element: countView, radius: 1, borderWidth: 1, color: Colors.SeparatorLight)
         Helper.setUiElementBorderWithCorner(element: customisationButton, radius: 1, borderWidth: 1, color: Colors.SeparatorLight)
        Fonts.setPrimaryBold(minusBtn)
        Fonts.setPrimaryBold(plusBtn)
        
        addOnsLabel.textColor   = Colors.PrimaryText
        itemName.textColor =  Colors.PrimaryText
        itemWeight.textColor =  Colors.SecoundPrimaryText
        numberOfItems.textColor =  Colors.PrimaryText
        newPrice.textColor =  Colors.PrimaryText
        oldPriceLabel.textColor =  Colors.SeconderyText
        oldPriceCutView.backgroundColor =  Colors.SeconderyText
        
        plusBtn.tintColor =  Colors.AppBaseColor
        minusBtn.tintColor =  Colors.AppBaseColor
        
        plusBtn.setTitleColor(Colors.AppBaseColor, for: .normal)
        minusBtn.setTitleColor(Colors.AppBaseColor, for: .normal)
        
        Fonts.setPrimaryMedium(customisationButton)
        customisationButton.setTitleColor(Colors.DeliveryBtn, for: .normal)
        customisationButton.setTitle(StringConstants.customisation(), for: .normal)
        numberOfItems.adjustsFontSizeToFitWidth = true
//        self.AddOnTableView.delegate = self
//        self.AddOnTableView.dataSource = self
    }
    
    
    /// updates the cart item
    ///
    /// - Parameter data: is of type CartItem model object
    func setupData(data:CartItem) {
        dataItem = data
        Helper.setImage(imageView: itemImage, url: data.ItemImageURL, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        itemName.text = data.ItemName
        itemWeight.text = data.UnitName
        var totalAmound = data.FinalPrice
        var totalUnitOldAmount = data.UnitPrice
        numberOfItems.text = String(data.QTY)
        addOnsLabel.text = ""
        if data.AddOnAvailable {
            customisationButton.isHidden = false
            for each in data.Addons {
                addOnsLabel.text = addOnsLabel.text! + each.Name + ", "
                totalAmound = totalAmound + each.Price
                totalUnitOldAmount = totalUnitOldAmount + each.Price
            }
            
            var previewText = addOnsLabel.text!
            if previewText.length > 0{
                let endIndex = previewText.index(previewText.endIndex, offsetBy: -2)
                previewText = String(previewText[..<endIndex])
                addOnsLabel.text = previewText
            }
        }else{
            customisationButton.isHidden = true
        }
        print("Addon on cart here")
        print(data.Addons)
        self.dataOfAddOns = data.Addons
        self.AddOnTableView.reloadData()
        newPrice.text = Helper.df2so(Double( totalAmound))
        
        if data.Discount > 0{
            
           oldPriceLabel.isHidden = false
          oldPriceCutView.isHidden = false
         oldPriceLabel.text = Helper.df2so(Double( totalUnitOldAmount))
        }
        else{
            oldPriceLabel.isHidden = true
            oldPriceCutView.isHidden = true
        }
        
    }
    
    @IBAction func plusBtnAction(_ sender: Any){
         if (dataItem?.AddOnAvailable)! || Utility.getSelectedSuperStores().type == .Restaurant {
            updateCart()
        }else{
            dataItem?.QTY = (dataItem?.QTY)! + 1
            numberOfItems.text = String(describing: (dataItem?.QTY)!)
            cartVM.updateCart(cartItem: dataItem!)
        }
    }
    
    
    func updateCart() {

        if (dataItem?.AddOnAvailable)! {
            Helper.openAddonsFromCart(item: Item.init(data:dataItem! ), cart: false) { (success,newItem) in
                if success == .Add {
                    DispatchQueue.main.async {
                        self.dataItem?.QTY = 1
                        self.cartVM.addCart(cartItem: CartItem.init(item: newItem))
                    }
                }else if success == .Patch {
                    DispatchQueue.main.async {
                        let dataFull = CartDBManager().getCartFullCount(data:  Item.init(data:self.dataItem! ))
                        var data = CartItem.init(data: [:])
                        if dataFull.count > 0 {
                            data = dataFull.last!
                            if data.PackId != self.dataItem?.PackId{
                                data = self.dataItem!
                            }
                        }else{
                            data = self.dataItem!
                        }
                        data.QTY = data.QTY + 1
                        self.cartVM.updateCart(cartItem: data)
                      //  self.updateData()
                    }
                }
                let item = Item.init(data: self.dataItem!)
                item.RecentAddOns.removeAll()
                for each in (item.AddOnGroups) {
                    for eachIn in each.AddOns {
                        if eachIn.Selected {
                            item.RecentAddOns.append(eachIn.Id)
                        }
                    }
                }
            }
        }else{
            dataItem?.QTY = (dataItem?.QTY)! + 1
            numberOfItems.text = String(describing: (dataItem?.QTY)!)
            cartVM.updateCart(cartItem: dataItem!)
        }
    
    }

    
    @IBAction func minusBtnAction(_ sender: Any) {
         var count = (dataItem?.QTY)! - 1
        if count > 0 {
            dataItem?.QTY = count
            cartVM.updateCart(cartItem:dataItem!)
        }else {
            count = 0
            dataItem?.QTY = 0
            cartVM.deleteCart(cartItem:dataItem!)
            observeBack.onNext(true)
        }
        numberOfItems.text = String(describing: count)
    }
    
    @IBAction func customisationAction(_ sender: Any) {
     //   self.plusBtnAction(sender)
        Helper.finalController().tabBarController?.tabBar.isHidden = true
        Helper.openAddons(item: Item.init(data: dataItem!), cart: true) { save in
            let con = Helper.finalController()
            if con.navigationController != nil {
                if con.navigationController?.viewControllers.count == 1 {
                    con.tabBarController?.tabBar.isHidden = false
                }
            }
        } 
    }
}

extension CartCollectionCell: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataOfAddOns.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddOnTableViewCell.self), for: indexPath) as! AddOnTableViewCell
        cell.setDataforAddon(data: self.dataOfAddOns[indexPath.row])
        
        return cell
    }
}
