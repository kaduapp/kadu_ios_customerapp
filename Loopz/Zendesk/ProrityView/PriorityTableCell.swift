//
//  CancelTableCell.swift
//  LiveMPro
//
//  Created by Nabeel Gulzar on 04/10/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class PriorityTableCell: UITableViewCell {


    @IBOutlet weak var priorityLabelColor: UILabel!
    @IBOutlet weak var cancelLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
