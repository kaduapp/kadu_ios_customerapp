//
//  SwitchRTL.swift
//  DelivX
//
//  Created by 3EMBED on 21/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SwitchRTL: UISwitch {
    override func awakeFromNib() {
        super.awakeFromNib()
        if RTL.shared.isRTL
        {
            self.semanticContentAttribute = .forceRightToLeft
        }else
        {
            self.semanticContentAttribute = .forceLeftToRight

        }
    }
    
}
