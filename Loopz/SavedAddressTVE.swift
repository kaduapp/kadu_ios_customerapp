//
//  SavedAddressTVE.swift
//  UFly
//
//  Created by 3Embed on 13/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


extension SavedAddressVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if savedAddressVM.arrayOfddress.count == 0 {
            if savedAddressVM.loaded == true {
                tableView.backgroundView = emptyView
            }
            return 0
        }else {
            tableView.backgroundView = nil
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedAddressVM.arrayOfddress.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.SavedAddresTableCell, for: indexPath) as!SavedAddresTableCell
        
        cell.setData(data: savedAddressVM.arrayOfddress[indexPath.row])
        cell.deleteIcon.tag = indexPath.row
        cell.deleteIcon.addTarget(self, action: #selector(deleteAddress), for: .touchUpInside)
        
        cell.editButton.tag = indexPath.row
        cell.editButton.addTarget(self, action: #selector(editAddress), for: .touchUpInside)
        
        if indexPath.row == (savedAddressVM.arrayOfddress.count -  1){
            cell.separator.isHidden = true
        }
        else {
            cell.separator.isHidden = false
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            let btn = UIButton()
            btn.tag = indexPath.row
            self.deleteAddress(sender: btn)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let btn = UIButton()
        btn.tag = indexPath.row
        if isCheckout == true || isSendPackage == true {
            
            if Utility.getSelectedSuperStores().type == .Launder{
                self.dismiss(animated: true, completion: {
                    if self.delegate != nil {
                        self.delegate?.didSelectAddress(address: self.savedAddressVM.arrayOfddress[indexPath.row] , indexPath:self.indexPathSelected!)
                    }
                })
                
            }
            else if isSendPackage{
                
                if self.delegate != nil {
                    self.delegate?.didSelectAddress(address: self.savedAddressVM.arrayOfddress[indexPath.row],indexPath : indexPathSelected!)
                }
                self.navigationController?.popViewController(animated: true)
            }
            else if (Utility.getSelectedSuperStores().type == .SendAnything) && isSendAnythingPickupAddress {
                self.dismiss(animated: true, completion: {
                    if self.delegate != nil {
                        self.delegate?.didSelectPickupAddress(address: self.savedAddressVM.arrayOfddress[indexPath.row])
                    }
                })
                
            }
            else{
                self.dismiss(animated: true, completion: {
                    if self.delegate != nil {
                        self.delegate?.didSelectAddress(address: self.savedAddressVM.arrayOfddress[indexPath.row])
                    }
                })
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return Helper.showHeader(title: StringConstants.AddressListTitle(), showViewAll: false)
    }
}

