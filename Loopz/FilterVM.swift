//
//  FilterVM.swift
//  DelivX
//
//  Created by 3Embed on 13/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class FilterVM: NSObject {

    var selectedRating = 0
    var selectedCousine = ""
    var selectedFoodType = ""
    let filterVM_response = PublishSubject<Bool>()
    var arrayOfFilter:[String] = []
    var storeFilters = false
    var context = 0
    let disposeBag = DisposeBag()
    
    func setData(type:AppConstants.TypeOfStore) {
        switch type {
        case .Marijuana:
            arrayOfFilter = [StringConstants.Categories(),StringConstants.Price(),StringConstants.Offers()]
            break
        case .Shopping:
            arrayOfFilter = [StringConstants.Categories(),StringConstants.Brand(),StringConstants.Size(),StringConstants.Color(),StringConstants.Manufacturer()]
            break
        case .Grocery,.Restaurant:
            arrayOfFilter = [StringConstants.Sort(),StringConstants.Categories(),StringConstants.Brand(),StringConstants.Price(),StringConstants.Manufacturer()]
            if context == 1 {
                arrayOfFilter.remove(at: 1)
            }else
            if context == 2 {
                arrayOfFilter.removeLast()
            }else if context == 4 {
                arrayOfFilter.remove(at: arrayOfFilter.index(of: StringConstants.Brand())!)
            }
            
            if storeFilters {
                arrayOfFilter = [StringConstants.Sort(), StringConstants.Cousines()]
            }
            break
        default:
            
            print("Last Case")
        }

        filterVM_response.onNext(true)
    }
    
    
    var arrayOfSort     = [StringConstants.Popular(),StringConstants.Newest(),StringConstants.Lowtohigh(),StringConstants.Hightolow(),StringConstants.Offers()]
    
    var arrayOfRestaurantSort = [StringConstants.Rating(),StringConstants.CostForTwo()]
}
