//
//  Category.swift
//  UFly
//
//  Created by 3Embed on 02/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Category: NSObject {
    var Name                = ""
    var Id                  = "0"
    var Color               = ""
    var Desc                = ""
    var Image               = ""
    var BannerImage         = ""
    var SubCategories       = [SubCategory]()
    var selected            = false
    
    
    init(data:[String:Any]) {
        if let titleTemp = data[APIResponceParams.CategoryName] as? String{
            Name         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CategoryId] as? String{
            Id           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CategoryColor] as? String{
            Color        = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CategoryDesc] as? String{
            Desc         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CategoryImage] as? String{
            Image        = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CategoryBannerImage] as? String{
            BannerImage        = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SubCategories] as? [Any]{
            SubCategories.removeAll()
            for data in titleTemp {
                let cat = SubCategory.init(data: data as! [String : Any],store: Store.init(data: [:]))
                SubCategories.append(cat)
            }
        }
    }

}
