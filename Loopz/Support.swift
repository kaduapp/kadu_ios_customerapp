//
//  Support.swift
//  DelivX
//
//  Created by 3Embed on 18/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct Support {
    var Title = ""
    var Desc = ""
    var Link = ""
    var SubCat:[Support] = []
    
    init(data:[String:Any]) {
        if let titleTemp = data[APIResponceParams.FirstName] as? String{
            Title             = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.Desc] as? String{
            Desc                         = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.Link] as? String{
            Link                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SubCat] as? [Any] {
            SubCat.removeAll()
            for data in titleTemp {
                let dataTemp = data as! [String: Any]
                SubCat.append(Support.init(data: dataTemp))
            }
        }
    }
 
}
