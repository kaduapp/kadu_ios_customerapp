//
//  AmountTableCell.swift
//  UFly
//
//  Created by 3 Embed on 07/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AmountDetailCell: UITableViewCell {
    
    @IBOutlet weak var topseparator: UIView!
    @IBOutlet weak var itemTotalLabel: UILabel!
    @IBOutlet weak var itemTotal: UILabel!
    
    @IBOutlet weak var paidViaLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var grandTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    /// initial view setup
    func initialSetup() {
        if itemTotalLabel != nil {
            Fonts.setPrimaryRegular(itemTotalLabel)
            itemTotalLabel.textColor = Colors.PrimaryText
            itemTotalLabel.text = StringConstants.ItemTotal()
        }
        if itemTotal != nil {
            Fonts.setPrimaryRegular(itemTotal)
            itemTotal.textColor = Colors.PrimaryText
        }
        
        if paidViaLabel != nil {
            Fonts.setPrimaryRegular(paidViaLabel)
            Fonts.setPrimaryBold(totalLabel)
            Fonts.setPrimaryBold(grandTotal)
            paidViaLabel.textColor = Colors.SecoundPrimaryText
            totalLabel.textColor = Colors.PrimaryText
            grandTotal.textColor = Colors.PrimaryText
            totalLabel.text = StringConstants.Total()
            totalLabel.isHidden = true
        }
        if topseparator != nil {
            topseparator.backgroundColor = UIColor.clear
        }
    }
   
    
    /// this method updates the amount details of selected Order
    ///
    /// - Parameter data: it is of type Order Model object
    func updateAmount(data:Order){
        grandTotal.text = Helper.df2so(Double( data.TotalAmount))
        
       
        // 0 no payment 1 for Wallet card and 2 for cash 3 far card
        // 0 no payment 1 for card card and 2 for cash 3 far Wallet
        paidViaLabel.textColor = Colors.PrimaryText
        paidViaLabel.text = StringConstants.Total()
    }
    
    
    /// this method updates the amount details of selected Order
    ///
    /// - Parameter data: it is of type Order Model object
    func updateAmount(data:Tax){
        if data.Name != StringConstants.Taxes() && data.Name != StringConstants.Payment() {
            Fonts.setPrimaryRegular(itemTotalLabel)
            itemTotal.text = Helper.df2so(Double( data.Price))
        }else{
            Fonts.setPrimaryMedium(itemTotalLabel)
            itemTotal.text = ""
        }
        
        itemTotalLabel.text = data.Name
        if data.Value > 0 {
            itemTotalLabel.text = itemTotalLabel.text! + "(\(data.Value)%)"
        }
    }
    
}
