//
//  HeaderView.swift
//  
//
//  Created by 3Embed on 30/05/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import UIKit

class RestHomeHeader: UICollectionReusableView{
    
//    @IBOutlet weak var costView: UIStackView!
//    @IBOutlet weak var priceLB: UILabel!
//    @IBOutlet weak var priceDesLb: UILabel!
    @IBOutlet weak var distanceDesLB: UILabel!
    @IBOutlet weak var distanceLB: UILabel!
    
    
    @IBOutlet weak var minOrderValue: UILabel!
    @IBOutlet weak var minOrderLB: UILabel!
    @IBOutlet weak var ratingDesLB: UILabel!
    @IBOutlet weak var ratingLB: UILabel!
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var fastfoodLB: UILabel!
    @IBOutlet weak var recommendedLB: UILabel!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var headerSubView1: UIView!
    @IBOutlet weak var viewForDotteLine: UIView!
    @IBOutlet weak var recommendedHeight: NSLayoutConstraint!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var recommendedLBTop: NSLayoutConstraint!
    
    @IBOutlet weak var recommendedLB2: UILabel!
    
    @IBOutlet weak var headerSubView2: UIView!
    
    @IBOutlet weak var separatorView2: UIView!
    
    
    
    override func awakeFromNib(){
        setUI()
    }
    
    func setUI(){
        title1.textColor = Colors.PrimaryText
        Fonts.setPrimaryBold(title1, size: 25)
        fastfoodLB.textColor = Colors.PrimaryText
        ratingLB.textColor = Colors.PrimaryText
        ratingDesLB.textColor = Colors.PrimaryText
        distanceLB.textColor = Colors.PrimaryText
        distanceDesLB.textColor = Colors.PrimaryText
        minOrderValue.textColor = Colors.PrimaryText
        minOrderLB.textColor = Colors.PrimaryText
        recommendedLB.textColor = Colors.PrimaryText
        recommendedLB2.textColor = Colors.PrimaryText
        Helper.drawDottedLine(viewForDotteLine)
        separatorView.backgroundColor = Colors.ScreenBackground
        separatorView2.backgroundColor = Colors.ScreenBackground
        recommendedLB.text = StringConstants.Favourite()
        recommendedLB2.text = StringConstants.Recommended()
        ratingDesLB.text = StringConstants.Rating()
        minOrderLB.text = StringConstants.ForTwo()
        distanceDesLB.text = StringConstants.FarAway()
        self.layoutIfNeeded()
    }
    
    func setData(data:Store) {
        title1.text = data.Name
        
        fastfoodLB.text = data.SubcategoryName
        
        if Utility.getSelectedSuperStores().type.rawValue == 1
        {
            fastfoodLB.text = data.SubcategoryName + "\n" + data.storeArea
         }
        else if (data.SubcategoryName.isEmpty)
        {
            fastfoodLB.text = data.storeArea
        }
        
        var distanceMetric = ""
        if Int(Utility.getCurrency().2) == 1{
            distanceMetric = StringConstants.MileAway()
               distanceLB.text = "\(Helper.clipDigit(valeu:data.DistanceMiles, digits: 2))" + " " +  "\(distanceMetric)"
        }else{
            distanceMetric = StringConstants.KMAway()
            self.distanceLB.text = "\(Helper.clipDigit(valeu: data.DistanceKm.floatValue ?? 0.0, digits: 2))" + " " + "\(distanceMetric)"
            
        }
     
        ratingLB.text  =  Helper.clipDigit(valeu: data.StoreRating, digits: 1)
        minOrderValue.text = Helper.df2so(Double(data.CostForTwo))
        Helper.setImage(imageView: self.mainImage, url: data.Image, defaultImage: UIImage())
        
    }
    
}
