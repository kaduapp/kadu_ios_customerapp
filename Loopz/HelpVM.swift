//
//  HelpVM.swift
//  DelivX
//
//  Created by 3Embed on 18/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HelpVM: NSObject {
    var arrayOfSupport:[Support] = []
    let helpVM_response   = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    func helpAndSupport() {
        HelpAPICalls.getHelp().subscribe(onNext: { [weak self]success in
            self?.arrayOfSupport = success
            self?.helpVM_response.onNext(true)
        }, onError: { (Error) in
            
        }).disposed(by: self.disposeBag)
    }

}
