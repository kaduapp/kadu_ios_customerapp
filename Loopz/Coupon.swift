//
//  Coupon.swift
//  DelivX
//
//  Created by 3 Embed on 07/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

struct DiscountType {
    var TypeId = 0
    var TypeName = ""
    var Value = 0
    
    init(data:[String:Any]) {
        
        if let titleTemp = data[APIResponceParams.TypeId] as? Int{
            TypeId             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.TypeName] as? String{
            TypeName                         = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.Value] as? Int{
            Value                         = titleTemp
        }
    }
}
struct Coupon {
    var Code = ""
    var CityId = ""
    var Description = ""
    var EndTime:Date = Date()
    var HowItWorks = ""
    var MiniCartValue = 0
    var StartTime:Date = Date()
    var TermsAndConditions = ""
    var Title = ""
    var Discount = DiscountType(data: [:])
    
    init(data:[String:Any]){
        
        if let titleTemp = data[APIResponceParams.Code] as? String{
            Code             = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.IdAddress] as? String{
            CityId             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Description] as? String{
            Description                         = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.EndTime] as? String{
            EndTime                         =  Helper.getStringToDate(value: titleTemp, format: DateFormat.ISODateFormat, zone: false)!
        }
        if let titleTemp = data[APIResponceParams.HowItWorks] as? String{
            HowItWorks                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.MinimunCartValue] as? Int{
            MiniCartValue                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.StartTime] as? String{
            StartTime                         = Helper.getStringToDate(value: titleTemp, format: DateFormat.ISODateFormat, zone: false)!
        }
        if let titleTemp = data[APIResponceParams.TermsAndConditions] as? String{
            TermsAndConditions                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Title] as? String{
            Title                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Discount] as? [String:Any] {
           let _ = DiscountType.init(data: titleTemp)
        }
        
    }
    
}





