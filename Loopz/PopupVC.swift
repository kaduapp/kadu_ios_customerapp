//
//  PopupVC.swift
//  DelivX
//
//  Created by 3Embed on 21/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class PopupVC: UIViewController {

    var dispose = DisposeBag()
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var separator: UIView!
    
    let createNewListVM = CreateNewListVM()
    let popupVM = PopupVM()
    var selectedFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
       didUpdateList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      addObserver()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeObserver()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
         
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createNewListAction(_ sender:Any){
        if !selectedFlag {
            heightConstraint.constant = heightConstraint.constant
                + 75
            selectedFlag = true
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
            mainTableView.beginUpdates()
            mainTableView.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: .top)
            mainTableView.endUpdates()
            self.addButton.isHidden = true
        }
        
//      self.performSegue(withIdentifier: UIConstants.SegueIds.PopupToCreateNew, sender: self)
    }
    
    //MARK: - UIButton Action
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func addAction(_ sender: Any) {
        if mainTableView.numberOfRows(inSection: 0) > 0 {
            let cell = mainTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! CreateNewListCell
            guard cell.textField.text?.length != 0 else {
                Helper.showAlert(message: StringConstants.EnterListName(), head: StringConstants.Error(), type: 1)
                self.addButton.isHidden = false
                return
            }
            createNewListVM.updateList(listName:cell.textField.text!)
            self.addButton.isHidden = false

        }
    }
    
    
    
    @IBAction func confirmAction(_ sender: Any) {
        if popupVM.type == 1 {
            var index = 0
            var data = PopupModel.init(data: [:])
            if self.popupVM.arrayOfList.count != 0 {
                index = popupVM.selected[0] as! Int
                data = self.popupVM.arrayOfList[index] as! PopupModel
            }
            self.popupVM.popupVM_response.onNext([data.String])
            self.closeAction(sender)
        }else{
            popupVM.addToWishList()
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        dispose = DisposeBag()
    }
    
    deinit {
        print("diinited")
        print("\(String(describing:self)) deinited")
    }
    
}
extension PopupVC {
    
    
    /// observer to ViewModel
    func didUpdateList(){
        
        CreateNewListVM.createNewListVM_response.subscribe(onNext: {[weak self] success in
            self?.setUI()
        }, onError: { Error in
            print(Error.localizedDescription)
        }, onCompleted: {
            print("CreateNewListVM.createNewListVM_response completed")
        }) {
             print("CreateNewListVM.createNewListVM_response disposed")
        }.disposed(by: dispose)
        
        popupVM.popupVM_responseLocal.subscribe(onNext: {[weak self]success in
            if success == true {
                self?.popupVM.popupVM_response.onNext((self?.popupVM.selected)!)
                self?.closeAction(UIButton())
            }
        }, onError: {error in
            print(error)
        }).disposed(by: dispose)
        
        CreateNewListVM.createNewListVM_response.subscribe(onNext: {[weak self]success in
                self?.selectedFlag = false
            self?.mainTableView.reloadData()
            }, onError: {error in
                print(error)
        }).disposed(by: self.popupVM.disposeBag)
    }
    
}
