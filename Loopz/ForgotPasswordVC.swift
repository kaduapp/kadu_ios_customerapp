//
//  ResendVC.swift
//  UFly
//
//  Created by Rahul Sharma on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet var forgotPWView: ForgotPasswordView!
    var forgotPasswordVM = ForgotPasswordVM()
    //    var authentication = Authentication()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addObserverForLocal()
        self.navigationController?.navigationBar.isHidden = true
        //Helper.transparentNavigation(controller: self.navigationController!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        removeObserverForLocal()
        self.navigationController?.navigationBar.isHidden = false
        // Helper.nonTransparentNavigation(controller: self.navigationController!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didTapPhNumBtn(_ sender: Any) {
        dismissKeyboard()
        forgotPasswordVM.textFieldType = .Phone
        forgotPWView.initView(data: forgotPasswordVM.authentication,type:1)
        
    }
    
    @IBAction func didTapEmailAddrBtn(_ sender: Any) {
        forgotPWView.phNumCheckMark.isHidden = true
        dismissKeyboard()
        forgotPasswordVM.textFieldType = .Email
        forgotPWView.initView(data: forgotPasswordVM.authentication,type:2)
        
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapNextBtn(_ sender: Any) {
        //        let Phone = (forgotPWView.phNumTextField.text?.replacingOccurrences(of: forgotPasswordVM.authentication.CountryCode, with: ""))!
        dismissKeyboard()
        if forgotPWView.phNumTextField.text!.sorted().count != 0 && (forgotPWView.phNumTextField.text!.sorted().count != forgotPasswordVM.authentication.CountryCode.count) {
            forgotPasswordVM.callNext()
        }
        else {
            if forgotPasswordVM.textFieldType == .Email {
                Helper.showAlert(message: StringConstants.EmailAvailable(), head: StringConstants.Error(), type: 1)
            }
            else if forgotPasswordVM.textFieldType == .Phone {
                Helper.showAlert(message: StringConstants.PhoneNumberAvailable(), head: StringConstants.Error(), type: 1)
            }
        }
        
    }
    
    
    @IBAction func countryPickerAction(_ sender: Any) {
        //        let vnhStoryboard = UIStoryboard(name: UIConstants.ControllerIds.VNHCountryPicker, bundle: nil)
        //        if let nav: UINavigationController = vnhStoryboard.instantiateInitialViewController() as! UINavigationController? {
        //            let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker
        //            // delegate
        //            picker.delegate = self
        //            present(nav, animated: true, completion: nil)
        //        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier  == UIConstants.SegueIds.ForgotPWToOtp {
            let otpVC:OTPVC = segue.destination as! OTPVC
            otpVC.otpVM.authentication = forgotPasswordVM.authentication
            if forgotPWView.phNumTextField.text != "" {
                otpVC.otpVM.isFromForgotPW = true
            }
        }
    }
}


extension ForgotPasswordVC {
    /// observe forgotPassword ViewModel
    func initView() {
        forgotPasswordVM.fpVM_response.subscribe(onNext: {[weak self]success in
            if success {
                if self?.forgotPasswordVM.textFieldType == .Phone {
                    self?.performSegue(withIdentifier: UIConstants.SegueIds.ForgotPWToOtp, sender: self)
                }else{
                    self?.navigationController?.popViewController(animated: true)
                }
            }
            
        }).disposed(by: forgotPasswordVM.disposeBag)
        forgotPasswordVM.textFieldType = .Phone
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        forgotPWView.initView(data: forgotPasswordVM.authentication,type:1)
        self.forgotPWView.phNumTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    /// dismissKeyboard
    @objc func dismissKeyboard(){
        if forgotPasswordVM.textFieldType == .Phone{
            if forgotPWView.phNumTextField.text?.sorted().count == forgotPasswordVM.authentication.CountryCode.sorted().count{
                forgotPWView.phNumTextField.text = ""
            }
        }
        view.endEditing(true)
    }
    
    
    func addObserverForLocal(){
        NotificationCenter.default.addObserver(self,selector: #selector(viewMoveAccKeyboardInFilter(notification:)),name: UIResponder.keyboardWillShowNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(viewMoveToOriginalInFilter),name: UIResponder.keyboardWillHideNotification,object: nil)
    }
    
    func removeObserverForLocal(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func viewMoveAccKeyboardInFilter(notification:NSNotification){
        let kbSize = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.size.height, right: 0.0)
        self.forgotPWView.scrollView.contentInset = contentInsets;
        self.forgotPWView.scrollView.scrollIndicatorInsets = contentInsets;
    }
    
    @objc  func viewMoveToOriginalInFilter(){
        self.forgotPWView.scrollView.contentInset = UIEdgeInsets.zero;
        self.forgotPWView.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero;
    }
    
}
extension ForgotPasswordVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
        let movedOffset: CGFloat = sender.contentOffset.y
        
        print(movedOffset)
        if sender == forgotPWView.scrollView {
            if movedOffset <= 0 {
                sender.contentOffset.y = 0
            }
        }
    }
}
