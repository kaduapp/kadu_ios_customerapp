//
//  LanguageVC.swift
//  DelivX
//
//  Created by 3 Embed on 23/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LanguageVC: UIViewController {
    
    let disposeBag = DisposeBag()
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var mainTableView:UITableView!
    var selectedLanguage:Language = Utility.getLanguage()
    var languages:[Language] = []
    var navView = NavigationView().shared

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainTableView.backgroundColor = Colors.ScreenBackground
        Helper.setButton(button: submitButton, view: submitView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor, shadow: false)
        Helper.setButtonTitle(normal: StringConstants.Submit(), highlighted: StringConstants.Submit(), selected: StringConstants.Submit(), button: submitButton)
        getLanguages()
        // Do any additional setup after loading the view.
        
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.Language())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitAction(_ sender: Any) {
        Utility.setLanguage(data: selectedLanguage)
       // self.navigationController?.popViewController(animated: true)
        Helper.showPI(string: "")
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            Helper.hidePI()
  //          let wd = UIApplication.shared.delegate?.window
   //         let vc = (wd??.rootViewController)!
//            if vc is UINavigationController {
//                (vc as! UINavigationController).dismiss(animated: true, completion: nil)
//            }
            if Changebles.isGrocerOnly{
                Helper.setGrocerOnlyRootViewController()
            }else{
            self.dismiss(animated: true, completion: nil)
            }
            
        }
        
    }
    
    func getLanguages() {
        APICalls.languages().subscribe(onNext: { [weak self]data in
            self?.languages.removeAll()
            self?.languages.append(contentsOf: data)
            self?.mainTableView.reloadData()
        }).disposed(by: disposeBag)
    }

    
}
