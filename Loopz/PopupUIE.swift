//
//  PopupUIE.swift
//  DelivX
//
//  Created by 3 Embed on 26/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
extension PopupVC {
    
    /// initial ViewSetup
    func setUI() {
        //Done Button
        addButton.tintColor = Colors.PrimaryText
        titleLabel.textColor = Colors.PrimaryText
        titleView.backgroundColor = Colors.SecondBaseColor
        Helper.setUiElementBorderWithCorner(element: titleView, radius: 10, borderWidth: 0, color: UIColor.clear)
        Helper.setShadow(sender: self.view)
        //Title
        separator.backgroundColor = Colors.SeparatorLight
        Fonts.setPrimaryMedium(titleLabel)
        Helper.setButton(button: confirmButton, view: confirmView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor, shadow: true)
        Helper.setButtonTitle(normal: StringConstants.Confirm(), highlighted: StringConstants.Confirm(), selected: StringConstants.Confirm(), button: confirmButton)
        switch PopupVM.TypeOfService(rawValue: popupVM.type) {
        case .Reason?:
            titleLabel.text = StringConstants.CancelOrderMsg()
            HistoryAPICalls.getCancelReason().subscribe(onNext: {[weak self] (success) in
                self?.popupVM.arrayOfList = success
                self?.mainTableView.reloadData()
                self?.heightConstraint.constant = CGFloat((self?.popupVM.arrayOfList.count)! * 50)
                self?.view.updateConstraints()
                self?.view.layoutIfNeeded()
            }).disposed(by: dispose)
            addButton.isHidden = true
            break
        case .ItemDetail?:
            titleLabel.text = StringConstants.AddToList()
             popupVM.arrayOfList = PopupMenu.getWishList()
            mainTableView.reloadData()
            break
        default:
            break
        }
        heightConstraint.constant = CGFloat(popupVM.arrayOfList.count * 50)
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
}
