//
//  OfferTVCell.swift
//  DelivX
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class OfferTVCell: UITableViewCell {

    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageLarge: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setInitial()
        // Initialization code
    }

    func setInitial() {
        Fonts.setPrimaryRegular(descriptionLabel)
        Fonts.setPrimaryMedium(titleLabel)
        descriptionLabel.textColor =  Colors.SeconderyText
        titleLabel.textColor   =  Colors.PrimaryText
        separatorView.backgroundColor =  Colors.SeparatorLight
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(data:Offer) {
        titleLabel.text = data.Name
        descriptionLabel.text = data.Description
        Helper.setImage(imageView: imageLarge, url: data.ImageURL, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
    }
    
}
