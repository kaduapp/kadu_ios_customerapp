//
//  FilterDetailsVC.swift
//  DelivX
//
//  Created by 3Embed on 09/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FilterDetailsVC: UIViewController {

    let disposeBag = DisposeBag()
    @IBOutlet weak var clearButton: UIBarButtonItem!
    @IBOutlet weak var mainTableView: UITableView!
    let filterDetailVM = FilterDetailVM()
    var searchVMReference = SearchVM()
    var nest = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = filterDetailVM.title
        clearButton.title = StringConstants.Clear()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clearAction(_ sender: Any) {
        filterDetailVM.arrayOfTypesSelected = []
        if filterDetailVM.type == APIRequestParams.CategoryFilter || filterDetailVM.type == APIRequestParams.SubCategoryFilter || filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
            filterDetailVM.selectedCategory = ""
            filterDetailVM.SubCatSelected = ""
            filterDetailVM.SubSubCatSelected = ""
            filterDetailVM.selectedSection = -1
            searchVMReference.filterInfo.onNext((true,filterDetailVM.type,[]))
        }else if filterDetailVM.type == StringConstants.Cousines() {
            filterDetailVM.selectedCosine = ""
            searchVMReference.filterInfo.onNext((true,filterDetailVM.type,["\(filterDetailVM.selectedRating)",filterDetailVM.selectedFoodType,filterDetailVM.selectedCosine]))
        }else if filterDetailVM.type == StringConstants.FoodType() {
            filterDetailVM.selectedFoodType = ""
            searchVMReference.filterInfo.onNext((true,filterDetailVM.type,["\(filterDetailVM.selectedRating)",filterDetailVM.selectedFoodType,filterDetailVM.selectedCosine]))
        }else if  filterDetailVM.type == StringConstants.Rating() {
            filterDetailVM.selectedRating = 0
            searchVMReference.filterInfo.onNext((true,filterDetailVM.type,["\(filterDetailVM.selectedRating)",filterDetailVM.selectedFoodType,filterDetailVM.selectedCosine]))
        }
        else if filterDetailVM.type == APIRequestParams.FilterPrice{
           filterDetailVM.selectedMinValue = 0
           filterDetailVM.selectedMaxValue = 0
            searchVMReference.filterInfo.onNext((true,filterDetailVM.type,[]))
           
        }
        else{
            searchVMReference.filterInfo.onNext((true,filterDetailVM.type,[]))
        }
        
        mainTableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = filterDetailVM.title
        if filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
            filterDetailVM.type = APIRequestParams.SubCategoryFilter
        }
        getResponse()
        searchVMReference.filterInfo.onNext((false,filterDetailVM.type,[]))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = "   "
    }
    
    func getResponse() {
        Helper.networkReachabilityChange.subscribe(onNext: { [weak self]dataGot in
            self?.filterDetailVM.getFilter()
        }).disposed(by: disposeBag)
        filterDetailVM.filterDetails_responce.subscribe(onNext: { [weak self]success in
            if success {
                if self?.filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
                    for item in (self?.filterDetailVM.arrayOfCategory)! {
                        if item.Name == self?.filterDetailVM.selectedCategory {
                            for item2 in item.SubCategories {
                                if item2.Name == self?.filterDetailVM.SubCatSelected {
                                    if item2.SubSubCat.count > 0 {
                                        self?.performSegue(withIdentifier: String(describing: FilterSubCatVC.self), sender: self)
                                    }
                                }
                            }
                        }
                    }
                }else if (self?.filterDetailVM.selectedCategory.length)! > 0 && self?.filterDetailVM.type == APIRequestParams.CategoryFilter {
                    self?.filterDetailVM.type = APIRequestParams.SubCategoryFilter
                    self?.filterDetailVM.chosenCat = (self?.filterDetailVM.selectedCategory)!
                    for item in (self?.filterDetailVM.arrayOfCategory)! {
                        if item.Name == self?.filterDetailVM.selectedCategory {
                            let index = self?.filterDetailVM.arrayOfCategory.index(of: item)
                            self?.filterDetailVM.selectedSection = index!
                        }
                    }
                    self?.filterDetailVM.getFilter()
                }
                self?.mainTableView.reloadData()
            }
        }).disposed(by: disposeBag)
        switch filterDetailVM.type {
        case StringConstants.Brand():
            filterDetailVM.type = APIRequestParams.BrandFilter
            break
        case StringConstants.Manufacturer():
            filterDetailVM.type = APIRequestParams.ManufacturerFilter
            break
        case StringConstants.Categories():
            filterDetailVM.type = APIRequestParams.CategoryFilter
            break
        case StringConstants.Price():
            filterDetailVM.type = APIRequestParams.FilterPrice
            break
        case StringConstants.Cousines():
            filterDetailVM.type = StringConstants.Cousines()
            break
        case StringConstants.CostForTwo():
            filterDetailVM.type = StringConstants.CostForTwo()
            break
        case StringConstants.Rating():
            filterDetailVM.type = StringConstants.Rating()
            break
        case StringConstants.FoodType():
            filterDetailVM.type = StringConstants.FoodType()
            break
        default:
            break
        }
        FilterDetailVM.filterArray_responce.subscribe(onNext: { [weak self]data in
            switch self?.filterDetailVM.type {
            case APIRequestParams.CategoryFilter:
                self?.filterDetailVM.selectedCategory = data.0
                self?.filterDetailVM.SubCatSelected = data.1
                self?.filterDetailVM.SubSubCatSelected = data.2
                break
            case APIRequestParams.BrandFilter:
                self?.filterDetailVM.arrayOfTypesSelected = data.3
                break
            case APIRequestParams.FilterPrice:
                self?.filterDetailVM.selectedMinValue = data.4
                self?.filterDetailVM.selectedMaxValue = data.5
                break
            case APIRequestParams.ManufacturerFilter:
                self?.filterDetailVM.arrayOfTypesSelected = data.6
                break
            case APIRequestParams.FIlterOffer:
//                self?.filterDetailVM. = data.7
                break
            case APIRequestParams.Sort:
//                self?.filterDetailVM.sor = data.8
                break
            case StringConstants.Rating(),StringConstants.Cousines(),StringConstants.FoodType():
                self?.filterDetailVM.selectedRating = data.9
                self?.filterDetailVM.selectedCosine = data.10
                self?.filterDetailVM.selectedFoodType = data.11
                break
            default:
                break
            }
            self?.filterDetailVM.needle = data.12
            self?.mainTableView.reloadData()
        }).disposed(by: disposeBag)
        filterDetailVM.getFilter()
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == String(describing: FilterSubCatVC.self) {
            if let controller = segue.destination as? FilterSubCatVC {
                for item in self.filterDetailVM.arrayOfCategory {
                    if item.Name == self.filterDetailVM.selectedCategory {
                        for item2 in item.SubCategories {
                            if item2.Name == self.filterDetailVM.SubCatSelected {
                                if item2.SubSubCat.count > 0 {
                                    controller.arrayOfSubsubCat = item2.SubSubCat
                                }
                            }
                        }
                    }
                }
                controller.filterDetailVM.SubCatSelected = filterDetailVM.SubCatSelected
                controller.searchVMReference = self.searchVMReference
            }
            
        }
    }
    @IBAction func valueChanged(_ sender: Any) {
        
    }
    
}
