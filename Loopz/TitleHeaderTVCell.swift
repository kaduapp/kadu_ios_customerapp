//
//  TitleHeaderTVCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 20/02/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class TitleHeaderTVCell: UITableViewCell {

    
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var sectionTitle: UILabel!
    
    @IBOutlet weak var viewAllButtonOutlet: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewAllButtonOutlet.tintColor =  Colors.AppBaseColor
        viewAllButtonOutlet.setTitleColor( Colors.AppBaseColor, for: .normal)
        Fonts.setPrimaryMedium(viewAllButtonOutlet)
        sectionTitle.textColor =  Colors.PrimaryText
        sepratorView.backgroundColor = Colors.SeparatorLarge
        Fonts.setPrimaryMedium(sectionTitle)
        sepratorView.isHidden = true
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setHeaderTitle(title:String,showViewAll:Bool){
          sectionTitle.text = title
          self.viewAllButtonOutlet.isHidden = !showViewAll
        if showViewAll{
         sepratorView.isHidden = true
        }
        else{
          sepratorView.isHidden = false
        }
    }
}
