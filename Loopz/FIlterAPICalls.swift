//
//  FIlterAPICalls.swift
//  DelivX
//
//  Created by 3Embed on 19/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class FIlterAPICalls: NSObject {
    
    static let disposeBag = DisposeBag()
    // get Filter
    class func getFilter(selectedItem:String,needle:String,searchFrom: Int,catName:String,subCatName:String,offer:String,language:String) -> Observable<[Any]>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        var select = "0"
        switch selectedItem {
        case APIRequestParams.CategoryFilter:
            select = "1"
            break
        case APIRequestParams.SubCategoryFilter:
            select = "2"
            break
        case APIRequestParams.SubSubCategoryFilter:
            select = "3"
            break
        case APIRequestParams.BrandFilter:
            select = "4"
            break
        case APIRequestParams.ManufacturerFilter:
            select = "5"
            break
        case APIRequestParams.FilterPrice:
            select = "6"
            break
        default:
            break
        }
       // Helper.showPI(string: StringConstants.Loading())
        var par = [
            [
                "match": [
                    "storeId": Utility.getStore().Id
                ]
            ],
            [
                "match": [
                    "status": 1
                ]
            ]
        ]
        if needle.length > 0 {
            let data = [
                "match_phrase_prefix": [
                    "productname." + language: needle
                ]
            ]
            par.append(data)
        }
        if catName.length > 0 {
            let data = [
                "match": [
                    "catName." + language: catName
                ]
            ]
            par.append(data)
        }
        if offer.sorted().count > 0 {
            let dataIt = [
                "match": [
                    "offer.status": 1
                ]
            ]
            par.append(dataIt)
        }

        if subCatName.length > 0 {
            let data = [
                "match": [
                    "subCatName." + language: subCatName
                ]
            ]
            par.append(data)
        }
        let params = [
            "from": 0,
            "size": 10,
            "query": [
                "bool": [
                    "must": par
                ]
            ]
            ] as [String : Any]
        var header = Utility.getHeader()
        header["filterType"] = select
      //  header.updateValue(select, forKey: "filterType")
        header["popularStatus"] = "\(searchFrom)"
      //  header.updateValue("\(searchFrom)", forKey: "popularStatus")
        let url = APIEndTails.GetFilter
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, url, parameters: params,encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                var item:[Any] = []
                if errNum == .success {
                    
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [Any]{
                        for itemTemp in titleTemp {
                            if itemTemp is NSNull {
                                
                            }else{
                                if selectedItem == APIRequestParams.CategoryFilter {
                                    let catTemp = Category.init(data: [:])
                                    let it = itemTemp as! String
                                    catTemp.Name = it
                                    
                                    item.append(catTemp)
                                }else if selectedItem == APIRequestParams.SubCategoryFilter {
                                    let catTemp = SubCategory.init(data: [:], store: Utility.getStore())
                                    let it = itemTemp as! String
                                    catTemp.Name = it
                                    item.append(catTemp)
                                }else if selectedItem == APIRequestParams.SubSubCategoryFilter {
                                    var catTemp = SubSubCategory.init(data: [:])
                                    let it = itemTemp as! String
                                    catTemp.Name = it
                                    item.append(catTemp)
                                }else{
                                    if let dataFrom = itemTemp as? String {
                                        item.append(dataFrom)
                                    }else if let dataFrom = itemTemp as? Double {
                                        item.append("\(dataFrom)")
                                    }
                                }
                            }
                        }
                    }
                }
                Helper.hidePI()
                observer.onNext(item)
                observer.onCompleted()
                
            },onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    
    // get Filter
    class func getFilterStore(selectedItem:String) -> Observable<[Any]>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        var select = "0"
        switch selectedItem {
        case StringConstants.Cousines():
            select = "3"
            break
        case StringConstants.CostForTwo():
            select = "2"
            break
        case StringConstants.FoodType():
            select = "1"
            break
        case StringConstants.Rating():
            select = "4"
            break
        default:
            break
        }
        Helper.showPI(string: StringConstants.Loading())
        let par = [
            [
                "match": [
                    "serviceZones": Utility.getAddress().ZoneId
                ]
            ],
            [
                "match": [
                    "status": 1
                ]
            ],
            [
                "match": [
                    "storeCategory.categoryId": Utility.getSelectedSuperStores().Id
                ]
            ],
            [
                "match": [
                    "storeType": Utility.getSelectedSuperStores().type.rawValue
                ]
            ]
        ]
        let params = [
            "from": 0,
            "size": 10,
            "query": [
                "bool": [
                    "must": par
                ]
            ]
            ] as [String : Any]
        var header = Utility.getHeader()
        header["filterType"] = select
       // header.updateValue(select, forKey: "filterType")
        let url = APIEndTails.GetStoreFilter
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, url, parameters: params,encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                var item:[Any] = []
                if errNum == .success {
                    
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [Any]{
                        for itemTemp in titleTemp {
                            if itemTemp is NSNull {
                                
                            }else{
                              switch selectedItem {
                                case StringConstants.Cousines():
                                    item.append(itemTemp)
                                    break
                                case StringConstants.CostForTwo():
                                    break
                                case StringConstants.FoodType():
                                    item.append(itemTemp)
                                    break
                                case StringConstants.Rating():
                                    if let d = itemTemp as? Double {
                                        if d > 0 {
                                            for i in 1...Int(d) {
                                                item.append("\(i) \(StringConstants.andAbove())")
                                            }
                                        }
                                    }else if let d = itemTemp as? Float {
                                        if d > 0 {
                                            for i in 1...Int(d) {
                                                item.append("\(i) \(StringConstants.andAbove())")
                                            }
                                        }
                                    }else if let d = itemTemp as? Int {
                                        if d > 0 {
                                            for i in 1...Int(d) {
                                                item.append("\(i) \(StringConstants.andAbove())")
                                            }
                                        }
                                    }
                                    break
                                default:
                                    break
                                }
                            }
                        }
                    }
                }
                Helper.hidePI()
                observer.onNext(item)
                observer.onCompleted()
                
            },onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    // get Filter Category
    class func getFilterCategory(selectedItem:String) -> Observable<[SubCategory]>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Loading())
        let params = [
            APIRequestParams.ZoneId         : Utility.getAddress().ZoneId,
            APIRequestParams.StoreId        : Utility.getStore().Id,
            APIRequestParams.CategoryName   : selectedItem
        ]
        let header = Utility.getHeader()
        let url = APIEndTails.GetFilterCategory
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, url, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                var item:[SubCategory] = []
                if errNum == .success {
                    
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [Any]{
                        if titleTemp.count > 0 {
                            let cat = titleTemp[0] as! [String:Any]
                            let subCat = cat["subCategoryName"] as! [[String:Any]]
                            for itemSub in subCat {
                                let subcatTemp = SubCategory.init(data: [:], store: Utility.getStore())
                                subcatTemp.Name = itemSub["name"] as! String
                                let subsubtemp = itemSub["subSubCategoryName"] as! [String]
                                if subsubtemp.count > 0 {
                                    for lightdata in subsubtemp {
                                        var dataSubsub = SubSubCategory.init(data: [:])
                                        dataSubsub.Name = lightdata
                                        subcatTemp.SubSubCat.append(dataSubsub)
                                    }
                                }
                                item.append(subcatTemp)
                            }
                        }
                    }
                }
                Helper.hidePI()
                observer.onNext(item)
                observer.onCompleted()
                
            },onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(), head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
}
