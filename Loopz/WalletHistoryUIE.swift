//
//  WalletHistoryUIE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension WalletHistoryVC {
    
    
    /// initial view setup
    func updateView() {
        
        Helper.setButtonTitle(normal: StringConstants.Debits(), highlighted: StringConstants.Debits(), selected: StringConstants.Debits(), button: debitOrderButton)
        Helper.setButtonTitle(normal: StringConstants.Credits(), highlighted: StringConstants.Credits(), selected: StringConstants.Credits(), button: creditOrderButton)
        debitOrderButton.setTitleColor(Colors.PrimaryText, for: .normal)
        creditOrderButton.setTitleColor(Colors.Unselector, for: .normal)
        
        indicator.backgroundColor = Colors.PrimaryText
        
        Fonts.setPrimaryBold(creditOrderButton)
        Fonts.setPrimaryBold(debitOrderButton)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        creditOrderTableView.rowHeight = UITableView.automaticDimension
        debitTableView.rowHeight = UITableView.automaticDimension
        
        creditOrderTableView.backgroundColor = Colors.ScreenBackground
        debitTableView.backgroundColor = Colors.ScreenBackground
        
        self.separatorView.backgroundColor = Colors.ScreenBackground
    }
    
    /// this method Gets Called when scrollViewDidEndDragging, scrollViewDidEndDecelerating
    ///moves to next or previous screen with animation
    /// - Parameter page: is of type Integer
    func scrollToPage(page:Int){
        UIView.animate(withDuration: 0.3){
            self.scrollView.contentOffset.x = self.scrollView.frame.width*CGFloat(page)
            self.updatePage(scrolView1: self.scrollView)
        }
    }
    
    
    func setEmptyScreen() {
        emptyViewDebit.setData(image: #imageLiteral(resourceName: "EmptyCard"), title: StringConstants.NoDebits(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        emptyViewDebit.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
        emptyViewCredit.setData(image: #imageLiteral(resourceName: "EmptyCard"), title: StringConstants.NoCredits(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        emptyViewCredit.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
    }
    
    @objc func StartShopping() {
        Helper.changetTabTo(index: AppConstants.Tabs.Home)
    }
    
    func setVM() {
        historyVM.walletHistoryVM_response.subscribe(onNext: { [weak self]data in
            self?.debitTableView.reloadData()
            self?.creditOrderTableView.reloadData()
        }).disposed(by: historyVM.disposeBag)
    }
}
