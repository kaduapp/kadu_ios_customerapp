//
//  CollectionViewExtension.swift
// 
//
//  Created by 3Embed on 30/05/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import UIKit

extension RestHomeVC:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        var count = 0
        for each in restuarantsStoreVM.categories {
            count = count + each.SubCategories.count
        }
        return count + 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        if section == 0{
            return restuarantsStoreVM.favProducts.count
            
            
        }
        else if section == 1{
            return restuarantsStoreVM.recomendedProducts.count
            
            
        }else{
            
            var subCats:[SubCategory] = []
            for each in restuarantsStoreVM.categories {
                subCats.append(contentsOf: each.SubCategories)
            }
            
            return subCats[section - 2].Products.count
            
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
         if indexPath.section == 0{
             let cell: ItemCommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self) + "2", for: indexPath) as! ItemCommonCollectionViewCell
            cell.cartVM = self.cartVM
            cell.setValue(data:restuarantsStoreVM.favProducts[indexPath.row], isDelete: false)
            cell.addToCartBtn.tag = indexPath.row
            return cell
        }
        else if indexPath.section == 1{
            let cell: ItemCommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self), for: indexPath) as! ItemCommonCollectionViewCell
           cell.cartVM = self.cartVM
           cell.setValue(data:restuarantsStoreVM.recomendedProducts[indexPath.row], isDelete: false)
            cell.addToCartBtn.tag = indexPath.row
            return cell
        }
        else{
            let cell: ItemCommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self) + "2", for: indexPath) as! ItemCommonCollectionViewCell
            cell.cartVM =  self.cartVM
            var subCats:[SubCategory] = []
            for each in restuarantsStoreVM.categories {
                subCats.append(contentsOf: each.SubCategories)
            }
            var products:[Item] = []
            products.append(contentsOf: subCats[indexPath.section - 2].Products)
            cell.setValue(data:products[indexPath.row], isDelete: false)
            cell.addToCartBtn.tag = indexPath.row
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        if (kind == UICollectionView.elementKindSectionHeader){
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:String(describing: RestHomeHeader.self), for: indexPath) as! RestHomeHeader
            
            if indexPath.section == 1 {
                headerView.headerSubView1.isHidden = true
                headerView.recommendedLB.isHidden = true
                headerView.recommendedHeight.constant = 25
                headerView.separatorView.isHidden = true
                headerView.separatorHeight.constant = 10
                headerView.recommendedLBTop.constant = 10
                headerView.headerSubView2.isHidden = false
                if restuarantsStoreVM.recomendedProducts.count == 0 {
                    headerView.recommendedLB.isHidden = true
                    headerView.recommendedHeight.constant = 0
                    headerView.separatorView.isHidden = true
                    headerView.separatorHeight.constant = 0
                    headerView.recommendedLBTop.constant = 0
                    headerView.headerSubView2.isHidden = true
                }
            }
            else if indexPath.section == 0{
                headerView.headerSubView2.isHidden = true
                headerView.headerSubView1.isHidden = false
                headerView.recommendedLB.isHidden = false
                headerView.recommendedHeight.constant = 25
                headerView.separatorView.isHidden = false
                headerView.separatorHeight.constant = 10
                headerView.recommendedLBTop.constant = 10
                
                if restuarantsStoreVM.recomendedProducts.count == 0 && restuarantsStoreVM.favProducts.count == 0 {
                    headerView.recommendedLB.isHidden = true
                    headerView.recommendedHeight.constant = 0
                    headerView.separatorView.isHidden = true
                    headerView.separatorHeight.constant = 0
                    headerView.recommendedLBTop.constant = 0
                    headerView.headerSubView2.isHidden = true
                }
                
                if restuarantsStoreVM.favProducts.count == 0 {
                headerView.recommendedLB.text = StringConstants.Recommended()
                }
                else{
                 headerView.recommendedLB.text = StringConstants.Favourite()
                }
            }else{
                headerView.headerSubView1.isHidden = true
                 headerView.headerSubView2.isHidden = true
            }
            headerView.setData(data: restuarantsStoreVM.List_Store)
            return headerView
        }
        else if (kind == UICollectionView.elementKindSectionFooter){
            let footView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier:
                String(describing: RestHomeFooter.self), for: indexPath) as! RestHomeFooter
            
            if indexPath.section == 0{
                footView.setData(dataCat: Category.init(data: [:]), dataSubCat:SubCategory.init(data: [:], store:Store.init(data: [:]) ))
             return footView
            }
            var count = 1
            var subcats:[SubCategory] = []
            for each in restuarantsStoreVM.categories {
                subcats.append(contentsOf: each.SubCategories)
                if indexPath.section == count {
                    footView.setData(dataCat: each, dataSubCat: each.SubCategories[0])
                    break
                }
                count = count + each.SubCategories.count
                if count > indexPath.section   {
                    footView.setData(dataCat: Category.init(data: [:]), dataSubCat: subcats[indexPath.section - 1])
                    break
                }
            }
            return footView
        }
        return UICollectionReusableView()
    }
    
}

extension RestHomeVC:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
//        if section == 0 {
//            return 10
//        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        if section == 0 || section == 1  {
            return 10
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if  indexPath.section == 1 {
            return CGSize(width: CGFloat((UIScreen.main.bounds.width - 40)/2), height: 225)
        }
        return CGSize(width: collectionView.frame.width - 20, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        
        let widthForHeader = collectionView.bounds.width
        var heightForHeader = CGFloat(0)
        
        if section == 0 {
            heightForHeader = 170
            if restuarantsStoreVM.recomendedProducts.count == 0 && restuarantsStoreVM.favProducts.count == 0 {
              heightForHeader = 120
            }
        }
        else if section == 1{
            heightForHeader = 50
         if restuarantsStoreVM.recomendedProducts.count == 0 || restuarantsStoreVM.favProducts.count == 0  {
                heightForHeader = 0
          }
        }
        return CGSize(width: widthForHeader, height: heightForHeader)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize{
        
        let widthForFooter = collectionView.bounds.width
        if section == 0{
            return CGSize(width: widthForFooter, height: 0)
        }
        
        
        var count = 1
        var subcats:[SubCategory] = []

        for each in restuarantsStoreVM.categories {
            subcats.append(contentsOf: each.SubCategories)
            if section == count {
                if subcats[count - 1].Name.length == 0 {
                    return CGSize(width: widthForFooter, height: 78)
                }
                return CGSize(width: widthForFooter, height: 120)
            }
            count = count + each.SubCategories.count
            if count > section  {
                if subcats[section - 1].Name.length == 0 {
                    return CGSize(width: widthForFooter, height: 33)
                }
                return CGSize(width: widthForFooter, height: 75)
            }
        }
        return CGSize(width: widthForFooter, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
            if self.navigationController != nil{
                if scrollView.contentOffset.y > 0{
                    Helper.addShadowToNavigationBar(controller: (self.navigationController)!)
                    if scrollView.contentOffset.y > 25{
                        self.navTitleLabel.alpha = 1
                    }else{
                        self.navTitleLabel.alpha = 0
                    }
                }
                else{
                    Helper.removeShadowToNavigationBar(controller: (self.navigationController)!)
                 //   self.enableTheNavigationAttributtes(false)
                    
                }
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var products:[Item] = []
        if indexPath.section == 0 {
            products = restuarantsStoreVM.favProducts
        }
        else if indexPath.section == 1 {
            products = restuarantsStoreVM.recomendedProducts
        }else {
            var subcats:[SubCategory] = []
            for each in restuarantsStoreVM.categories {
                subcats.append(contentsOf: each.SubCategories)
            }
            products.append(contentsOf: subcats[indexPath.section - 2].Products)
        }
        performSegue(withIdentifier: String(describing: ItemDetailVC.self), sender: products[indexPath.row])
    }
}

