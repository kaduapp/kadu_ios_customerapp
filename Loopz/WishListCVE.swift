//
//  WishListTVE.swift
//  UFly
//
//  Created by 3 Embed on 04/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


//// MARK: - UITableViewDataSource
//extension WishListVC: UITableViewDataSource {
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return wishListVM.arrayOfItems.count
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.WishListTableCell) as! WishListTableCell
//        cell.removeBtn.tag = indexPath.row
//        cell.removeBtn.addTarget(self, action: #selector(WishListVC.removeFavAction), for: .touchUpInside)
//        cell.setData(item: wishListVM.arrayOfItems[indexPath.row])
//        return cell
//    }
//
//
//
//}


///UICollectionViewDelegate
extension WishListVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if wishListVM.arrayOfItems.count == 0 && responceFlag {
            collectionView.backgroundView = emptyView
        }else{
            collectionView.backgroundView = nil
        }
        return wishListVM.arrayOfItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell: ItemCommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self), for: indexPath) as! ItemCommonCollectionViewCell
        cell.favouriteButton.tag = indexPath.row
        cell.favouriteButton.addTarget(self, action: #selector(WishListVC.removeFavAction), for: .touchUpInside)
        var Key = false
        if wishListVM.wishList.ID.length > 0 {
            Key = true
        }
        cell.setValue(data: wishListVM.arrayOfItems[indexPath.row],isDelete: Key)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: CGFloat((UIScreen.main.bounds.width - 40)/2), height: 225)
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        var size = CGSize.zero
//        let cellWidth = (UIScreen.main.bounds.width-2)/2
//        let cellHight = (collectionView.frame.size.width)/2 + 92
//        size.height =  cellHight
//        size.width  =  cellWidth
//        return size
//    }
//
    
    @objc func removeFavAction(sender:UIButton) {
        if wishListVM.wishList.ID.length == 0 {
            wishListVM.favourite(data: sender.tag)
            
            
        }else{
            wishListVM.removeItemWishList(data: sender.tag)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let button = UIButton()
        button.tag = indexPath.row
        selected = collectionView.cellForItem(at: indexPath)
        performSegue(withIdentifier:UIConstants.SegueIds.FavToDetails, sender: button)
    }
    
}


