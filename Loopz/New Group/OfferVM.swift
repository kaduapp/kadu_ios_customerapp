//
//  OfferVM.swift
//  DelivX
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class OfferVM: NSObject {
    
    var arrayOfOffers:[Offer] = []
    var loaded = false
    let offerVM_response = PublishSubject<(Bool)>()
    let disposeBag = DisposeBag()
    
    
    func getOffers() {
        HomeAPICalls.getOffers().subscribe(onNext: {[weak self]result in
            self?.arrayOfOffers = result
            self?.loaded = true
            self?.offerVM_response.onNext((true))
        }, onError: {error in
            self.loaded = true
            self.offerVM_response.onNext((false))
        }).disposed(by: disposeBag)
    }
}
