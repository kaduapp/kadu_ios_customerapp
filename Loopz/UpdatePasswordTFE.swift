//
//  NewPWTextFieldDelegates.swift
//  UFly
//
//  Created by Rahul Sharma on 30/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension UpdatePasswordVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        newPWView.errorLabel.text = ""
        if textField == newPWView.newPasswordTF {
            newPWView.newPWSeparator.backgroundColor =  Colors.AppBaseColor
        }
        else {
            
            if textField.text == newPWView.newPasswordTF.text || textField.text == "" {
                newPWView.reEnterPWSeparator.backgroundColor = Colors.AppBaseColor
            }else{
                newPWView.reEnterPWSeparator.backgroundColor = UIColor.red //Colors.AppBaseColor
            }
            
            
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == newPWView.newPasswordTF {
            newPWView.reEnterPWTF.becomeFirstResponder()
            newPWView.newPWSeparator.backgroundColor =  Colors.SeparatorLight
        }
        else {
            newPWView.reEnterPWSeparator.backgroundColor =  Colors.SeparatorLight
        }
        
        if textField == newPWView.reEnterPWTF {
            
            if newPWView.newPasswordTF.text == newPWView.reEnterPWTF.text && (newPWView.newPasswordTF.text?.sorted().count)! >= 6 {
                Helper.setButton(button: newPWView.continueBtn,view:newPWView.continueBtnContainer, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
                newPWView.continueBtn.isUserInteractionEnabled = true
                newPWView.reEnterPWSeparator.backgroundColor =  Colors.SeparatorLight
                
            }
            else{
                Helper.setButton(button: newPWView.continueBtn,view:newPWView.continueBtnContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
                newPWView.continueBtn.isUserInteractionEnabled = false
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
        
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        if (textField.text?.sorted().count)! >= 6 {
            if textField == newPWView.newPasswordTF {
                newPWView.newPWCheckMark.isHidden = false
            }else{
                if textField.text == newPWView.newPasswordTF.text{
                    newPWView.reEnterPWCheckMark.isHidden = false
                    newPWView.reEnterPWSeparator.backgroundColor =  Colors.AppBaseColor
                    
                }
            }
        }
        else {
            if textField == newPWView.newPasswordTF {
                newPWView.newPWCheckMark.isHidden = true
            }else {
                if newPWView.newPasswordTF.text != textField.text && (textField.text?.sorted().count)! > 0{
                    self.newPWView.reEnterPWSeparator.backgroundColor = UIColor.red
                }
                newPWView.reEnterPWCheckMark.isHidden = true
            }
        }
        
        if textField == newPWView.reEnterPWTF {
            
            if newPWView.newPasswordTF.text == newPWView.reEnterPWTF.text && (newPWView.newPasswordTF.text?.sorted().count)! >= 6 {
                Helper.setButton(button: newPWView.continueBtn,view:newPWView.continueBtnContainer, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
                newPWView.continueBtn.isUserInteractionEnabled = true
                newPWView.reEnterPWCheckMark.isHidden = false
                newPWView.reEnterPWSeparator.backgroundColor =  Colors.AppBaseColor
            }
            else{
                
                Helper.setButton(button: newPWView.continueBtn,view:newPWView.continueBtnContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
                newPWView.continueBtn.isUserInteractionEnabled = false
                newPWView.reEnterPWCheckMark.isHidden = true
                newPWView.reEnterPWSeparator.backgroundColor =  UIColor.red
            }
            
        }
        
    }
    
}


