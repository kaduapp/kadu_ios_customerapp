//
//  HomeTBC.swift
//  DelivX
//
//  Created by 3Embed on 28/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class HomeTBC: UITabBarController {

    let disposeBag = DisposeBag()
    var tabBarIndex = 0
    override func viewDidLoad(){
        super.viewDidLoad()
        self.delegate = self
        didCartUpdate()
        if let viewControllers = self.viewControllers{
            //var home = viewControllers[0]
            var restHome = viewControllers[1]
            var history = viewControllers[2]
            var cart = viewControllers[3]
            var search = viewControllers[4]
            var searchRest = viewControllers[5]
            for i in 0...5 {
                let vc = viewControllers[i] as! UINavigationController
//                if (vc.viewControllers.first?.isKind(of: HomeVC.self))! {
//                    home = viewControllers[i]
//                }
                if (vc.viewControllers.first?.isKind(of: StoresListVC.self))! {
                    restHome = viewControllers[i]
                    if Utility.getSelectedSuperStores().type == .Restaurant {
                        let customTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "tab_restaurant_off"), selectedImage: UIImage(named: "tab_restaurant_on"))
                        customTabBarItem.imageInsets = UIEdgeInsets(top: 7,left: 0,bottom: -7,right: 0)
                        restHome.tabBarItem = customTabBarItem
                    }else if Utility.getSelectedSuperStores().type == .Grocery{
                        let customTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "tab_Grocer_off"), selectedImage: UIImage(named: "tab_Grocer_on"))
                        customTabBarItem.imageInsets = UIEdgeInsets(top: 7,left: 0,bottom: -7,right: 0)
                        restHome.tabBarItem = customTabBarItem
                    }else{
                        let customTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "HomeOff"), selectedImage: UIImage(named: "HomeOn"))
                        customTabBarItem.imageInsets = UIEdgeInsets(top: 7,left: 0,bottom: -7,right: 0)
                        restHome.tabBarItem = customTabBarItem
                        
                    }
                }
                if (vc.viewControllers.first?.isKind(of: SearchVC.self))! {
                    search = viewControllers[i]
                }
                if (vc.viewControllers.first?.isKind(of: HistoryVC.self))! {
                    history = viewControllers[i]
                }
                if (vc.viewControllers.first?.isKind(of: CartVC.self))! {
                    cart = viewControllers[i]
                }
                if (vc.viewControllers.first?.isKind(of: SearchHomeVC.self))! {
                    searchRest = viewControllers[i]
                }
            }
            
            if Utility.getSelectedSuperStores().type == .Restaurant {
                self.viewControllers = [restHome,searchRest,cart,history]
            } else {
                self.viewControllers = [restHome,search,cart,history]
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        updateTabBadge()
    }
}
// MARK: - UITabBarControllerDelegate
extension HomeTBC: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        tabBarIndex = tabBarController.selectedIndex
        AppConstants.SelectedIndex = AppConstants.Tabs(rawValue: tabBarIndex)!
        switch tabBarIndex {
        case 2,3:
            if !Helper.isLoggedIn(isFromHist: true){
                self.tabBarController?.selectedIndex = 0
            }
        default:
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AppDelegate.tabChange != nil {
            let temp = AppDelegate.tabChange
            AppDelegate.tabChange = nil
            Helper.changetTabTo(index: temp!)
        }
    }
    
}

extension HomeTBC {
    
    /// Observe CartDBManager
    func didCartUpdate(){
        CartDBManager.cartDBManager_response.subscribe(onNext: { [weak self]success in
            self?.updateTabBadge()
            if success == false {
                self?.tabBarController?.selectedIndex = 0
            }
        }).disposed(by: disposeBag)
    }
    
    /// update the TabBadge which holds the cartCount
    func updateTabBadge() {
        if (AppConstants.CartCount > 0) {
            let tabItem = self.tabBar.items![2]
            tabItem.badgeValue = String(AppConstants.CartCount)
        }else{
            let tabItem = self.tabBar.items![2]
            tabItem.badgeValue = nil
        }
    }
}
