//
//  Slots.swift
//  DelivX
//
//  Created by 3EMBED on 23/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct Slots: Codable {
    let message: String?
    let data: [Datum]
}

struct Datum: Codable {
    let id: ID?
    let slots: [Slot]
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case slots
    }
}

struct ID: Codable {
    let date: String?
}

struct Slot: Codable {
    let startTime, endTime: String?
    let startDateTimestamp, endDateTimestamp: Int?
    let startDateISO, endDateISO: String?
    let _id: String?
}
