//
//  CollectionandDeliverySlotCell.swift
//  DelivX
//
//  Created by 3EMBED on 26/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CollectionandDeliverySlotCell: UITableViewCell {
    @IBOutlet weak var slotTypeLabel: UILabel!
        @IBOutlet weak var slotTimeLabel: UILabel!
    @IBOutlet weak var slotDateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func UpdateCellWithDate(date:String ,slot:String)
    {
        slotDateLabel.text = date
        slotTimeLabel.text = slot
         
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
