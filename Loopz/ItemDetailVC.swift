//
//  ItemDetailVC.swift
//  UFly
//
//  Created by 3 Embed on 18/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class ItemDetailVC: UIViewController, UIViewControllerTransitioningDelegate {
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var bottomCartView: BottomBarView!
    @IBOutlet weak var unitView: UnitView!
    @IBOutlet weak var unitViewHight: NSLayoutConstraint!
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var unitCollectionView: UICollectionView!
    
    let responceBack = PublishSubject<Bool>()
    //Variables
    var itemDetailVM = ItemDetailVM()
    var cartVM = CartVM()
    var navView = NavigationView().shared
    var searchVM = SearchVM()
    var isPresentingSignVC = false
    var viewCartHandler : ((Bool) ->(Void)) = {
        status  in
    }
    
    var isGuestLogin = false

    override func viewDidLoad() {
        super.viewDidLoad()
        cartVM.getCart()
        initialSetup()
        itemDetailVM.getItemDetail()
        Helper.PullToClose(scrollView: mainTableView)
        Helper.pullToDismiss?.delegate = self
        self.navView.titleLabel.textColor = UIColor.clear
        if !Utility.getLogin(){
            isGuestLogin = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        itemDetailVM.updatedCount = itemDetailVM.item.CartQty
        self.bottomCartView.updateAmount(data:self.itemDetailVM)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        if itemDetailVM.item.Units.count == 0{
            self.unitViewHight.constant = 0
        }
        else{
            self.unitViewHight.constant = 46
        }
        
        if Utility.getLogin() && isGuestLogin{
            cartVM.getCart()
            isGuestLogin = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !isPresentingSignVC{
        self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ViewCartAction(_ sender: Any) {
        if !Helper.isLoggedIn(isFromHist: false) {
            isPresentingSignVC = true
            return
        }
        
        if Helper.checkCartType(arrayOfCarts:self.cartVM.arrayOfCart, storeType: self.itemDetailVM.item.TypeStore, cartType: self.itemDetailVM.item.cartType,completionHandler: { (success) in
            AppConstants.CartCount = 0
            AppConstants.TotalCartPrice = 0
            self.cartVM.arrayOfCart.removeAll()
            self.cartVM.arrayOfTax.removeAll()
            self.cartVM.dataItem = nil
            self.addItemToCart()
        }){
           self.addItemToCart()
        }

    }
    
    
    func addItemToCart(){
        if Utility.getSelectedSuperStores().type == .Restaurant || itemDetailVM.updatedCount == itemDetailVM.item.CartQty{
            if AppConstants.Cart.count > 0 {
                self.viewCartHandler(true)
                self.dismiss(animated: false, completion: nil)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            itemDetailVM.updatedCount = Int(bottomCartView.quantity.text!)!
            updateCart()
        }
    }
    
    func updateCart() {
        if itemDetailVM.item.AddOnGroups.count > 0 && itemDetailVM.item.AddOnAvailable && itemDetailVM.item.CartQty < itemDetailVM.updatedCount && Utility.getSelectedSuperStores().type == .Restaurant {
            Helper.openAddons(item: itemDetailVM.item, cart: false) { dataSuccess in
                if dataSuccess != .Close {
                    self.itemDetailVM.updateCart()
                    self.dismiss(animated: true) {
                    }
                }
            }
            //            performSegue(withIdentifier: String(describing: AddOnVC.self), sender: self)
        }else{
            self.itemDetailVM.updateCart()
            if Utility.getSelectedSuperStores().type != .Restaurant {
                self.dismiss(animated: true) {
                }
            }
        }
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        responceBack.onNext(true)
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func minusBtnAction(_ sender: Any) {
        let count = (itemDetailVM.updatedCount) - 1
        if count > 0 {
            itemDetailVM.updatedCount = count
        }else {
            itemDetailVM.updatedCount = 0
        }
        if Utility.getSelectedSuperStores().type == .Restaurant {
            updateCart()
        }else{
            self.bottomCartView.updateAmount(data:self.itemDetailVM)
        }
    }
    
    @IBAction func favAction(_ sender: Any) {
         itemDetailVM.favourite()
    }
    @IBAction func plusBtnAction(_ sender: Any) {
        
        if Helper.checkCartType(arrayOfCarts:self.cartVM.arrayOfCart, storeType: self.itemDetailVM.item.TypeStore, cartType: self.itemDetailVM.item.cartType,completionHandler: { (success) in
            AppConstants.CartCount = 0
            AppConstants.TotalCartPrice = 0
            self.cartVM.arrayOfCart.removeAll()
            self.cartVM.arrayOfTax.removeAll()
            self.cartVM.dataItem = nil
            self.plusItemToCart()
        }){
            self.plusItemToCart()
        }
    }
    
    func plusItemToCart(){
        
        if Utility.getSelectedSuperStores().type == .Restaurant {
            itemDetailVM.updatedCount = itemDetailVM.updatedCount + 1
            updateCart()
            return
        }
        else{
        
        if itemDetailVM.updatedCount >= itemDetailVM.item.availableQuantity{
            
            if itemDetailVM.item.availableQuantity == 1{
            
            Helper.showAlert(message: "", head: "This item has only \(itemDetailVM.item.availableQuantity) unit available", type: 0)
            }
            else{
             Helper.showAlert(message: "", head: "This item has only \(itemDetailVM.item.availableQuantity) units available", type: 0)
            }
        }
        else{
            itemDetailVM.updatedCount = itemDetailVM.updatedCount + 1
            self.bottomCartView.updateAmount(data:self.itemDetailVM)
        }
     }
    }
    
    
    //favourite Button Action
    @objc func favBtnTapped(sender: UIButton) {
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
            sender.isSelected = false
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
            sender.isSelected = true
            sender.tintColor = Colors.AppBaseColor
        }
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(favourite), object: nil)
        self.perform(#selector(favourite), with: nil, afterDelay: 0.3)
       
        mainTableView.reloadSections( IndexSet.init(integer: 0), with: .none)
    }
    @objc func favourite()
        {
           itemDetailVM.favourite()
       }
    @objc func instructionBtnTapped(sender: UIButton){
        
        
    }
    
    @objc func imageBtnTapped(sender: UIButton){
        
        if itemDetailVM.item.Images.count > 0 {
            self.performSegue(withIdentifier: UIConstants.CellIds.ItemDetailsToImages, sender: sender.tag)
        }
    }
    //Add to list Action
    @objc func addToListBtnTapped(sender: UIButton){
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
        performSegue(withIdentifier: UIConstants.SegueIds.ItemdetailToAddList, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.ItemdetailToAddList {
            if let viewController: PopupVC = segue.destination as? PopupVC {
                viewController.popupVM.type = 2
                viewController.popupVM.selected = (itemDetailVM.item.WishList)
                viewController.popupVM.item = itemDetailVM.item
                viewController.popupVM.popupVM_response.subscribe(onNext: {success in
                    self.itemDetailVM.item.WishList = success as! [String]
                }, onError: {error in
                    print(error)
                }).disposed(by: self.itemDetailVM.disposeBag)
            }
        }else if segue.identifier == UIConstants.CellIds.ItemDetailsToImages {
            if let viewController: ImageVC = segue.destination as? ImageVC {
                viewController.arrayImages = itemDetailVM.item.Images
                viewController.selected = sender as! Int
            }
        }else if segue.identifier == String(describing: AddOnVC.self){
            if let VC:AddOnVC = segue.destination as? AddOnVC{
//                VC.addOnVM.arrayOfAddOn = self.itemDetailVM.item.addOns
                VC.addOnVM.AddOnsVM_cartUpdateResponse.subscribe(onNext: { success in
                    if success == AddOnVM.ResponseType.Patch {
                        
                    }
                    }, onError: {error in
                        print(error)
                }).disposed(by: self.itemDetailVM.disposeBag)
            }
        }
    }
    
    @IBAction func shareAction(_ sender: Any) {
        DynamicLinking.getDynamicLink(itemId:itemDetailVM.item.Id).subscribe(onNext: { (success) in
            let activityViewController = UIActivityViewController(
                activityItems: [StringConstants.ShareProductText() + " " + Utility.AppName + "\n" + success],
                applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        }).disposed(by: self.itemDetailVM.disposeBag)
    }
    
}
extension ItemDetailVC {
    /// Observe For ViewModel
    func didGetItemDetail(){
        if !itemDetailVM.ItemDetailVM_response.hasObservers{
        self.itemDetailVM.ItemDetailVM_response.subscribe(onNext: {success in
                switch success {
                case .GetAPIResponse:
                    DispatchQueue.main.async {
                        self.mainTableView.reloadData()
                        self.initialSetup()
                        if self.itemDetailVM.item.Units.count == 0 || (self.itemDetailVM.item.Units.count == 1 && Utility.getSelectedSuperStores().type.rawValue == 1){
                            self.unitViewHight.constant = 0
                        } else {
                            self.unitViewHight.constant = 46
                        }
                        //UIView.animate(withDuration: 0.5, animations: {
                        self.view.updateConstraints()
                        self.view.layoutIfNeeded()
                        //})
                        self.unitView.unitCollectionView.reloadData()
                    }
                    break
                case .UpdateCart:
                    self.bottomCartView.updateAmount(data:self.itemDetailVM)
                    break
                case .FavTrue:
                  let cell = self.mainTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! ItemDetailTVC
                  cell.updateItemDetail(FavTrue: true)
                    break
                case .FavFalse:
                    let cell = self.mainTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! ItemDetailTVC
                    cell.updateItemDetail(FavTrue: false)
                    break
                }
            }).disposed(by: itemDetailVM.disposeBag)
            
        }
  }
}
