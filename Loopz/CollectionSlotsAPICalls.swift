//
//  CollectionSlotsAPICalls.swift
//  DelivX
//
//  Created by 3EMBED on 23/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire
class CollectionSlotsAPICalls: NSObject {
    let rx_addResponse = PublishSubject<APIResponseModel>()
     let rx_placeorderResponse = PublishSubject<APIResponseModel>()
    let rx_updateResponse = PublishSubject<APIResponseModel>()
    let rx_deleteResponse = PublishSubject<APIResponseModel>()
    let rx_getResponse = PublishSubject<APIResponseModel>()
    
    let disposeBag = DisposeBag()
    
    func getAllSlotsFor(_ slotType: String ,with LaundryType:Int) {
        var strUrl = ""
        if slotType == "pickup" {
            strUrl = APIEndTails.BaseUrl + "slots/" + "\(slotType)" + "/" +  Utility.getAddress().ZoneId
        }else
        {
            strUrl = APIEndTails.BaseUrl + "slots/" + "\(slotType)" + "/" +  Utility.getAddress().ZoneId + "/" + "\(LaundryType)"
        }
        RxAlamofire
            .requestJSON(.get ,
                         strUrl,
                         parameters: nil,
                         encoding: JSONEncoding.default,
                         headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
                if let dict = json as? [String:Any] {
                    let statuscode:Int = r.statusCode
                    let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: r.statusCode)!
                  if errNum == .success
                  {
                    let response = Utility.checkResponse(statusCode: statuscode, responseDict: dict)
                    self.rx_addResponse.onNext(response)
                  }else
                  {
                 
                    APICalls.basicParsing(data:json as! [String : Any],status:r.statusCode)

                    }
                }
            }, onError: { (error) in
                print(error.localizedDescription)
            }, onCompleted: {
                print("AddItemToCart completed")
            }, onDisposed: {
                print("AddItemToCart Disposed")
            }).disposed(by: disposeBag)
    }
    
    
    
}
