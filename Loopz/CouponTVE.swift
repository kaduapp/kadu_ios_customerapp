//
//  CouponTVE.swift
//  DelivX
//
//  Created by 3 Embed on 03/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension CouponVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
    return Helper.showHeader(title: StringConstants.AvailableCoupons(), showViewAll: false)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.updateCoupon(data: couponVM.couponArr[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
extension CouponVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return couponVM.couponArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:  UIConstants.CellIds.CouponTableCell) as! CouponTableCell
        cell.updateCouponCell(data: couponVM.couponArr[indexPath.row])
        cell.applyButton.tag = indexPath.row
        cell.moreButton.tag = indexPath.row
        cell.applyButton.addTarget(self, action:#selector(clickApply), for: .touchUpInside)
        cell.moreButton.addTarget(self, action:#selector(clickMore), for: .touchUpInside)
        return cell
    }
    
    @objc func clickApply(sender:UIButton) {
        self.updateCoupon(data: couponVM.couponArr[sender.tag])
        applyPromocode()
    }
    @objc func clickMore(sender:UIButton) {
        self.performSegue(withIdentifier: String(describing: CouponDetailsVC.self), sender: couponVM.couponArr[sender.tag])
    }
}
