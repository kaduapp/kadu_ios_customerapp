//
//  SignInView.swift
//  UFly
//
//  Created by Rahul Sharma on 14/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SignInView: UIView {
    
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var phoneNumView: UIView!
    @IBOutlet weak var phoneNumTF: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var facebookBtnContainer: UIView!
    
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var continueBtnContainer: UIView!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var passwordTFHight: NSLayoutConstraint!
    @IBOutlet weak var phNumTickMark: UIButton!
    @IBOutlet weak var pwNumTickMark: UIButton!
    
    @IBOutlet weak var forgotPwd: UIButton!
    @IBOutlet weak var loginTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!

    
    @IBOutlet weak var phSeperartor: UIView!
    @IBOutlet weak var pwSeparator: UIView!
    
    @IBOutlet weak var countryCodeBtn: UIButton!
    
    var activeTextField:UITextField!
    
    var phoneNum:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewSetup()
        phoneNumTF.Semantic()
        passwordTF.Semantic()
    }
    
    /// initial setup
    func viewSetup(){
        
        passwordTFHight.constant = 0
        Fonts.setPrimaryMedium(loginTitle)
        Fonts.setPrimaryRegular(subTitle)
        
        Fonts.setPrimaryMedium(passwordTF)
        Fonts.setPrimaryMedium(phoneNumTF)
        
        Fonts.setPrimaryMedium(continueBtn)
       
        
        phNumTickMark.tintColor =  Colors.AppBaseColor
        pwNumTickMark.tintColor =  Colors.AppBaseColor
        
        signUpBtn.setTitle(StringConstants.ClickHereToSignUp(), for: .normal)
        forgotPwd.setTitle(StringConstants.ForgotPassword(), for: .normal)
        
        loginTitle.text = StringConstants.LiveFor()
        subTitle.text = StringConstants.EnterPh()
        
        Helper.setButton(button: continueBtn,view:continueBtnContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.Continue(), highlighted: StringConstants.Continue(), selected: StringConstants.Continue(), button: continueBtn)
        Fonts.setPrimaryMedium(facebookBtn)
        
        Helper.setButton(button: facebookBtn,view:facebookBtnContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.LoginWithFacebook(), highlighted: StringConstants.LoginWithFacebook(), selected: StringConstants.LoginWithFacebook(), button: facebookBtn)
     
        phSeperartor.backgroundColor =  Colors.SeparatorLight
        pwSeparator.backgroundColor =  Colors.SeparatorLight
        
        signUpBtn.setTitleColor( Colors.AppBaseColor, for: .normal)
        forgotPwd.setTitleColor( Colors.AppBaseColor, for: .normal)
        passwordTF.textColor =  Colors.PrimaryText
        phoneNumTF.textColor =  Colors.PrimaryText
        loginTitle.textColor =  Colors.PrimaryText
        subTitle.textColor   =  Colors.SeconderyText
        
        Helper.updateText(text: (signUpBtn.titleLabel?.text)!, subText: StringConstants.signUp(), signUpBtn, color: Colors.AppBaseColor,link: "")
        
        phoneNumTF.placeholder = StringConstants.PhoneNumber()
        passwordTF.placeholder = StringConstants.Password()
        pwNumTickMark.isHidden = true
        phNumTickMark.isHidden = true
        continueBtn.isUserInteractionEnabled = false
    }
}







