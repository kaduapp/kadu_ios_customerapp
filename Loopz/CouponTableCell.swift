//
//  CouponTableCell.swift
//  DelivX
//
//  Created by 3 Embed on 03/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CouponTableCell: UITableViewCell {
    
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var codeBGView: UIView!
    @IBOutlet weak var couponBtn: UIButton!
    @IBOutlet weak var couponDiscript: UILabel!
    @IBOutlet weak var couponTitle: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }

    func initialSetup() {
        
        Fonts.setPrimaryMedium(couponBtn)
        Fonts.setPrimaryMedium(couponTitle)
        Fonts.setPrimaryMedium(applyButton)
        Fonts.setPrimaryRegular(couponDiscript)
        Fonts.setPrimaryMedium(moreButton)
        
        Helper.setUiElementBorderWithCorner(element: codeBGView, radius: 2, borderWidth: 1, color: Colors.AppBaseColor)
        codeBGView.backgroundColor = Colors.AppBaseColor.withAlphaComponent(0.2)
        applyButton.setTitleColor(Colors.AppBaseColor, for: .normal)
        couponBtn.setTitleColor(Colors.PrimaryText, for: .normal)
        couponDiscript.textColor = Colors.SecoundPrimaryText
        couponTitle.textColor = Colors.PrimaryText
        
        moreButton.setTitleColor(Colors.AppBaseColor, for: .normal)
        
        moreButton.setTitle(StringConstants.moreDetails().uppercased(), for: .normal)
        applyButton.setTitle(StringConstants.Apply(), for: .normal)
        
        
      
    }
    
    func updateCouponCell(data: Coupon){
        couponBtn.setTitle(data.Code, for: .normal)
        couponDiscript.text = data.Description
        couponDiscript.text = String(describing: data.Description.html2String)
        couponTitle.text = data.Title
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension String {
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}





