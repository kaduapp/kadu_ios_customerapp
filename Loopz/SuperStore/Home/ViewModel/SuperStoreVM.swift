//
//  SuperStoreVM.swift
//  DelivX
//
//  Created by 3Embed on 11/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class SuperStoreVM: NSObject
{
    let disposeBag = DisposeBag()
    var arrayOfSuperStores:[SuperStore]     = []
    var response_SuperStoreVM  = PublishSubject<Bool>()
    var city = ""
    var activeOrderCount = 0
    
    func getData(){
        
        HomeAPICalls.getStores(city: city).subscribe(onNext:
            { [weak self]data in
                self?.arrayOfSuperStores.removeAll()
                self?.arrayOfSuperStores.append(contentsOf: data)
                self?.response_SuperStoreVM.onNext(true)
        }).disposed(by: disposeBag)
        
    }
    
    func historyIn() {
        HistoryAPICalls.getOrderHistory(data: 2, storeType: Utility.getSelectedSuperStores().type.rawValue).subscribe(onNext: {result in
            Helper.hidePI()
            print("Active Order Here")
            print(result.count)
            self.activeOrderCount = result.count
            self.response_SuperStoreVM.onNext(true)
        }, onError: {error in
        }).disposed(by: disposeBag)
    }
 
}
