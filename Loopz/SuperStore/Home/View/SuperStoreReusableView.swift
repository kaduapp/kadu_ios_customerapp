//
//  CVReusableView.swift
//  tryForCollectionView
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class SuperStoreReusableView: UICollectionReusableView
{
    
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var backgroundViewForSearch: UIView!
   
    @IBOutlet weak var activateOrderCountLbl: UILabel!
    @IBOutlet weak var detailIndicatiorButton: UIButton!
    @IBOutlet weak var activeOrderButton: UIButton!
    override func awakeFromNib(){
        setUpUI()
    }
    
    func setUpUI(){
        searchTextField?.leftViewMode = .always
        searchTextField?.leftView = UIImageView.init(image: #imageLiteral(resourceName: "search.png"))
        
        
        Helper.setShadow(sender: self.backgroundViewForSearch)
        Helper.setUiElementBorderWithCorner(element: self.backgroundViewForSearch, radius: 5, borderWidth: 0, color: UIColor.clear)
        Fonts.setPrimaryBold(self.titleLB, size: 20)
        
        
        
        self.backgroundViewForSearch.backgroundColor = UIColor(red: 133/255.0, green: 171/255.0, blue: 242/255.0, alpha: 1)
        searchTextField?.backgroundColor = UIColor(red: 133/255.0, green: 171/255.0, blue: 242/255.0, alpha: 1)
        self.titleLB.textColor = UIColor(red:36/255.0, green: 42/255.0, blue: 75/255.0, alpha: 1)
        self.detailIndicatiorButton.tintColor = Colors.AppBaseColor
        self.activateOrderCountLbl.backgroundColor = Colors.AppBaseColor
        self.activateOrderCountLbl.textColor = Colors.SecondBaseColor
        self.activateOrderCountLbl.layer.cornerRadius = self.activateOrderCountLbl.frame.size.width/2
        self.activateOrderCountLbl.layer.masksToBounds = true
        
        
        //Helper.setUiElementBorderWithCorner(element: activeOrderButton, radius: 12.5, borderWidth: 0, color: Colors.AppBaseColor)
        
        
    }

//    func showTheSearchBarAndHideOthersLabel(_ boolean:Bool)
//    {
//        self.titleLB.isHidden = boolean
//        self.searchTextField.isHidden = !boolean
//        self.backgroundViewForSearch.isHidden = !boolean
//    }
    
    func setUiForSection(section: Int){
        
        if section == 0{
            activeOrderButton.isUserInteractionEnabled = true
            activateOrderCountLbl.isHidden = false
            titleLB.text = StringConstants.ActiveOrderInSmall()
            titleLB.textColor = Colors.AppBaseColor
            detailIndicatiorButton.isHidden = false
        }else if section == 1{
            activeOrderButton.isUserInteractionEnabled = false
            activateOrderCountLbl.isHidden = true
            detailIndicatiorButton.isHidden = true
            titleLB.text = StringConstants.Others()
            titleLB.textColor = UIColor(red:36/255.0, green: 42/255.0, blue: 75/255.0, alpha: 1)
        }
    }
}
