//
//  SuperStoreOtherCVCell.swift
//  DelivX
//
//  Created by 3Embed on 24/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SuperStoreOtherCVCell: UICollectionViewCell
{
    
    @IBOutlet weak var secondBackgroudView: UIView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var image2: UIImageView!
    
    override func awakeFromNib()
    {
        Helper.setUiElementBorderWithCorner(element: secondBackgroudView, radius: 5, borderWidth: 0, color: UIColor.gray)
        Helper.setShadow(sender: secondBackgroudView)
        setUpTheFontsAndColorForLabels()
    }
    
    func setUpTheFontsAndColorForLabels()
    {
        Fonts.setPrimaryBold(titleLB, size: 20)
        titleLB.textColor = Colors.SecondBaseColor
    }  
    
    func configureCell(data:SuperStore) {
        self.titleLB.text = data.categoryName
        var color = UIColor()
        switch data.type {
        case .Restaurant:
            color = Colors.Restaurant
            break
        case .Grocery:
            color = Colors.Grocery
            break
        case .Shopping:
            color = Colors.Fashion
            break
        default:
            break
        }
        if data.colorCode.length > 0 {
            secondBackgroudView.backgroundColor = Helper.getUIColor(color: data.colorCode)
        }else{
            secondBackgroudView.backgroundColor = color
        }
        if data.logoImage.length > 0 {
            Helper.setImage(imageView: self.image2, url: data.logoImage, defaultImage: UIImage())
        }
    }
}
