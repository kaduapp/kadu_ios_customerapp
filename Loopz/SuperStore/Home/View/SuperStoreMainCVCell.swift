//
//  SuperStoreMainCVCell.swift
//  DelivX
//
//  Created by 3Embed on 24/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import FLAnimatedImage

class SuperStoreMainCVCell: UICollectionViewCell
{
    @IBOutlet weak var gifImageHere: FLAnimatedImageView!
    //@IBOutlet weak var gifBG: UIImageView!
    @IBOutlet weak var dot2: UIImageView!
    @IBOutlet weak var dot1: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var des1LB: UILabel!
    @IBOutlet weak var des2LB: UILabel!
    @IBOutlet weak var des3LB: UILabel!
    @IBOutlet weak var sideImage: UIImageView!
    @IBOutlet weak var cellBackgroundView: UIView!
    var gifs = [FLAnimatedImage]()
    override func awakeFromNib()
    {
        
        Helper.setShadow(sender: cellBackgroundView)
        Helper.setUiElementBorderWithCorner(element: cellBackgroundView, radius: 5, borderWidth: 0, color: UIColor.gray)
        setUpTheFontsAndColorForLabels()
        
    }
    
    func setUpTheFontsAndColorForLabels(){
       
        
        Fonts.setPrimaryBold(titleLB, size: 28)
        Fonts.setPrimaryRegular(des1LB, size: 14)
        Fonts.setPrimaryRegular(des2LB, size: 14)
        Fonts.setPrimaryRegular(des3LB, size: 14)
        titleLB.textColor = Colors.SecondBaseColor
        des1LB.textColor = Colors.SecondBaseColor
        des2LB.textColor = Colors.SecondBaseColor
        des3LB.textColor = Colors.SecondBaseColor
        
    }
    
    func configureCell(data:SuperStore)
    {
        self.titleLB.text = data.categoryName
        
        var color = UIColor()
        switch data.type {
        case .Restaurant:
            color = Colors.Restaurant
            break
        case .Grocery:
            color = Colors.Grocery
            break
        case .Shopping:
            color = Colors.Fashion
            break
        default:
            break
        }
        
        if Utility.getLanguage().Code == "ar" {
            self.sideImage.transform = CGAffineTransform(scaleX: -1, y: 1)
        }else{
            self.sideImage.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
        
        if data.colorCode.length > 0 {
            cellBackgroundView.backgroundColor = Helper.getUIColor(color: data.colorCode)
        }else{
            cellBackgroundView.backgroundColor = color
        }
        Helper.setImage(imageView: self.sideImage, url: data.logoImage, defaultImage: UIImage())
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.gifs.removeAll()
            if data.gifImage.length > 0{
                do {
                    if let url = URL(string: data.gifImage){
                        self.gifs.append(try FLAnimatedImage.init(animatedGIFData: Data.init(contentsOf: url)))
                    }
                }catch let error {
                    print(error)
                }
            }else{
                DispatchQueue.main.async {
                    if let file = Bundle.main.path(forResource: data.gifData, ofType: "gif"){
                        self.gifs.append(FLAnimatedImage.init(animatedGIFData: NSData.init(contentsOfFile: file) as Data?))
                    }
                }
            }
        DispatchQueue.main.async {
            
            self.gifImageHere.animatedImage = self.gifs[0]
            
            }
        }
        
        if Utility.getLanguage().Code == "es" && data.typeName == "Food"{
            self.des1LB.text = "Comida"
            
        }else if Utility.getLanguage().Code == "es" && data.typeName == "Grocery"{
            self.des1LB.text = "Tienda de comestibles"
            
        }else{
            self.des1LB.text = data.typeName
        }
        
        DispatchQueue.main.async {
            self.dot1.isHidden = true
            self.des2LB.isHidden = true
            self.dot2.isHidden = true
            self.des3LB.isHidden = true
        }
    }
    
}
