//
//  SuperStoreCVE.swift
//  DelivX
//
//  Created by 3Embed on 20/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import Kingfisher

extension SuperStoreVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if superStoreVModel.arrayOfSuperStores.count > 4
        {
            return 2
        }
        else
        {
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0
        {
            if superStoreVModel.arrayOfSuperStores.count > 4
            {
                return 4
            }
            else
            {
                return superStoreVModel.arrayOfSuperStores.count
            }
        }
        else if section == 1
        {
            return superStoreVModel.arrayOfSuperStores.count - 4
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0{
            let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SuperStoreMainCVCell.self), for: indexPath) as! SuperStoreMainCVCell
            collectionViewCell.configureCell(data:superStoreVModel.arrayOfSuperStores[indexPath.row] )
            return collectionViewCell
        }else{
            let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SuperStoreOtherCVCell.self), for: indexPath) as! SuperStoreOtherCVCell
            collectionViewCell.configureCell(data:superStoreVModel.arrayOfSuperStores[indexPath.row + 4] )
            return collectionViewCell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    if indexPath.section == 0{
        
    currentIndexOfTheItem = indexPath.row
    Utility.saveSelectedSuperStores(superStores: superStoreVModel.arrayOfSuperStores[currentIndexOfTheItem].objectData)
        
    } else if indexPath.section == 1 {
    currentIndexOfTheItem = indexPath.row + 4
    Utility.saveSelectedSuperStores(superStores: superStoreVModel.arrayOfSuperStores[currentIndexOfTheItem].objectData)
    }
    DispatchQueue.main.async {

    switch Utility.getSelectedSuperStores().type{

    case .Launder :
    if Helper.checkCartType(arrayOfCarts:self.cartVM.arrayOfCart, storeType:Utility.getStore().TypeStore ,cartType:Utility.getStore().cartType,completionHandler: { (success) in

    self.cartVM.arrayOfCart.removeAll()
    self.cartVM.arrayOfTax.removeAll()
    self.cartVM.dataItem = nil
    let mainStoryBoard = UIStoryboard(name: "AddItem", bundle: nil)
    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LaunderTabBarVC") as! UITabBarController
    self.navigationController?.present(redViewController, animated: true, completion: nil)

    }){

    let mainStoryBoard = UIStoryboard(name: "AddItem", bundle: nil)
    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LaunderTabBarVC") as! UITabBarController
    self.navigationController?.present(redViewController, animated: true, completion: nil)

    }
    case .Restaurant,.Grocery,.Shopping,.Marijuana,.Pharmacy:
    if AppConstants.laundrycartexists
    {
        let alertController = UIAlertController.init(title: StringConstants.itemsInCart(), message: StringConstants.clearCartMessage(), preferredStyle: UIAlertController.Style.alert)
        
        let deleteAction = UIAlertAction.init(title: StringConstants.Yes(), style: UIAlertAction.Style.default) { (UIAlertAction) in
            CartVM().clearCart(handler: { (success) in print()
                self.performSegue(withIdentifier: String(describing: HomeTBC.self), sender: self)
            })
        }
        let okAction = UIAlertAction.init(title: StringConstants.No(), style:UIAlertAction.Style.default , handler: nil)
        alertController.addAction(deleteAction)
        alertController.addAction(okAction)
        Helper.finalController().present(alertController, animated: true, completion: nil)
    }
    else if Helper.checkIsCartHasAnyOtherSupercategory(arrayOfCarts: self.cartVM.arrayOfCart, currentType: Utility.getSelectedSuperStores().type.rawValue.self,  completionHandler: {
        (success) in

    self.cartVM.arrayOfCart.removeAll()
    self.cartVM.arrayOfTax.removeAll()
    self.cartVM.dataItem = nil
    self.performSegue(withIdentifier: String(describing: HomeTBC.self), sender: self)

    }){

    self.performSegue(withIdentifier: String(describing: HomeTBC.self), sender: self)
    }
    case .SendAnything:
      if Helper.checkIsCartHasAnyOtherSupercategory(arrayOfCarts: self.cartVM.arrayOfCart, currentType: Utility.getSelectedSuperStores().type.rawValue.self,  completionHandler: {
  (success) in
    self.cartVM.arrayOfCart.removeAll()
    self.cartVM.arrayOfTax.removeAll()
    self.cartVM.dataItem = nil
    let mainStoryBoard = UIStoryboard(name: "SendPackages", bundle: nil)
    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SendPackagesTBC") as! UITabBarController
    self.navigationController?.present(redViewController, animated: true, completion: nil)

    }){

    let mainStoryBoard = UIStoryboard(name: "SendPackages", bundle: nil)
    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SendPackagesTBC") as! UITabBarController
    self.navigationController?.present(redViewController, animated: true, completion: nil)

    }

    }

    }

    }

//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        if indexPath.section == 0{
//            currentIndexOfTheItem = indexPath.row
//            Utility.saveSelectedSuperStores(superStores: superStoreVModel.arrayOfSuperStores[currentIndexOfTheItem].objectData)
//        } else if indexPath.section == 1 {
//            currentIndexOfTheItem = indexPath.row + 4
//            Utility.saveSelectedSuperStores(superStores: superStoreVModel.arrayOfSuperStores[currentIndexOfTheItem].objectData)
//        }
//         DispatchQueue.main.async {
//            if Utility.getSelectedSuperStores().type == .Launder {
//                if Helper.checkCartType(arrayOfCarts:self.cartVM.arrayOfCart, storeType:Utility.getStore().TypeStore ,cartType:Utility.getStore().cartType,completionHandler: { (success) in
//
//                    self.cartVM.arrayOfCart.removeAll()
//                    self.cartVM.arrayOfTax.removeAll()
//                    self.cartVM.dataItem = nil
//                    let mainStoryBoard = UIStoryboard(name: "AddItem", bundle: nil)
//                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LaunderTabBarVC") as! UITabBarController
//                    self.navigationController?.present(redViewController, animated: true, completion: nil)
//
//                }){
//
//                    let mainStoryBoard = UIStoryboard(name: "AddItem", bundle: nil)
//                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LaunderTabBarVC") as! UITabBarController
//                    self.navigationController?.present(redViewController, animated: true, completion: nil)
//
//                }
//
//            } else {
//                if AppConstants.laundrycartexists
//                {
//                    let alertController = UIAlertController.init(title: StringConstants.itemsInCart(), message: StringConstants.clearCartMessage(), preferredStyle: UIAlertController.Style.alert)
//
//                    let deleteAction = UIAlertAction.init(title: StringConstants.Yes(), style: UIAlertAction.Style.default) { (UIAlertAction) in
//                        CartVM().clearCart(handler: { (success) in print()
//                            self.performSegue(withIdentifier: String(describing: HomeTBC.self), sender: self)
//                        })
//
//                    }
//
//                    let okAction = UIAlertAction.init(title: StringConstants.No(), style:UIAlertAction.Style.default , handler: nil)
//                    alertController.addAction(deleteAction)
//                    alertController.addAction(okAction)
//                    Helper.finalController().present(alertController, animated: true, completion: nil)
//                }else
//                {
//                self.performSegue(withIdentifier: String(describing: HomeTBC.self), sender: self)
//                }
//            }
//        }
////        if superStoreVModel.arrayOfSuperStores[currentIndexOfTheItem].type == .Restaurant {
////            self.performSegue(withIdentifier: String(describing: RestaurantsVC.self), sender: self)
////        }else{
////            self.performSegue(withIdentifier: String(describing: HomeTBC.self), sender: self)
////        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        if (kind == UICollectionView.elementKindSectionHeader){
            let headerView =  collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: SuperStoreReusableView.self), for: indexPath) as! SuperStoreReusableView
            headerView.setUiForSection(section: indexPath.section)
            if indexPath.section == 0 {
                headerView.activateOrderCountLbl.text = "\(self.superStoreVModel.activeOrderCount)"
                headerView.activeOrderButton.addTarget(self, action: #selector(tapDetailOfActiveOrder), for: .touchUpInside)
            }
            return headerView
        }
    
            
        else if (kind == UICollectionView.elementKindSectionFooter){
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SuperStoreReusableViewFooter", for: indexPath)
            return footerView
        }
        return UICollectionReusableView()
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if indexPath.section == 0
        {
            return CGSize(width: (UIScreen.main.bounds.width - 25), height: 100)
        }
        
        if indexPath.section == 1
        {
            return CGSize(width: CGFloat((UIScreen.main.bounds.width-35)/2), height: 110)
        }
        
        return CGSize(width: (UIScreen.main.bounds.width - 25), height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let widthForHeader:CGFloat = UIScreen.main.bounds.width - 25
        var heightForHeader:CGFloat = 0
        
//        if section == 0{
//            if superStoreVModel.activeOrderCount > 0 {
//                heightForHeader = 50
//            }
//        }
        
        if section == 1
        {
            heightForHeader = 60
        }
        
        return CGSize(width: widthForHeader, height: heightForHeader)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize
    {
        let widthForFooter:CGFloat = UIScreen.main.bounds.width - 25
        var heightForFooter:CGFloat = 10
        
        if section == 0
        {
            heightForFooter = 0
        }
        
        return CGSize(width: widthForFooter, height: heightForFooter)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        if section == 1
        {
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }
        
        return UIEdgeInsets.zero
    }
    
    @objc func tapDetailOfActiveOrder(){
        
         self.performSegue(withIdentifier: UIConstants.SegueIds.SuperStoreToHome, sender: self)
    }
}

class MediaFetcher {
   class func downloadGifFile(_ url: URL, completion: @escaping (_ imageURL: URL) -> () = { (imageURL) in} ) {
        KingfisherManager.shared.retrieveImage(with: url, options: .none, progressBlock: nil) { (image, error, cacheType, imageUrl) in
            guard error == nil else {
                return
            }
            guard let imageUrl = imageUrl else {
                return
            }
            completion(imageUrl)
        }
    }
}
