//
//  SuperStoreTFE.swift
//  DelivX
//
//  Created by 3Embed on 20/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension SuperStoreVC:UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}
