  //
//  SuperStoreVC.swift
//  DelivX
//
//  Created by 3Embed on 20/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SuperStoreVC: UIViewController {
    
    @IBOutlet weak var navWidth: NSLayoutConstraint!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var downArrow: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var navigationBar: UIView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    let disposeBag = DisposeBag()
    var UtilityObj = Utility()
    var currentIndexOfTheItem = 0
    var superStoreVModel = SuperStoreVM()
    var cartVM = CartVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        superStoreVModel.city = Utility.getAddress().CityId
        superStoreVModel.arrayOfSuperStores = Utility.getSuperStores()
        superStoreVModel.getData()
        setVM()
        
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
           self.checkAppVersion()
        cartVM.getCart()
        Utility.saveStore(location: [:])
        self.superStoreVModel.activeOrderCount = 0
        if Utility.getLogin(){
           //self.superStoreVModel.historyIn()
        }else{
            DispatchQueue.main.async {
                self.mainCollectionView.reloadData()
            }
        }
        
    }
    
    
    @IBAction func TapActiveOrderButton(_ sender: Any) {
    }
    
    func setVM() {
        superStoreVModel.response_SuperStoreVM.subscribe(onNext:
            { data in
            
            if self.superStoreVModel.arrayOfSuperStores.count == 1 &&  self.superStoreVModel.arrayOfSuperStores[0].type == .Grocery {
                Changebles.isGrocerOnly = true
                Utility.saveSelectedSuperStores(superStores: self.superStoreVModel.arrayOfSuperStores[0].objectData)
               Helper.setGrocerOnlyRootViewController()
                
                
             }
            else{
              
                 Changebles.isGrocerOnly = false
                if let rootViewController = UIApplication.shared.delegate?.window??.rootViewController,  rootViewController.isKind(of: GrocerTabBar.self){
                    Utility.saveSelectedSuperStores(superStores: self.superStoreVModel.arrayOfSuperStores[0].objectData)
                    let mainStoryBoard = UIStoryboard(name: "SuperStore", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SuperStoreNav") as! UINavigationController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }
                DispatchQueue.main.async {
                    self.mainCollectionView.reloadData()
                }
            
                    
            }
        }).disposed(by: disposeBag)
        
//        superStoreVModel.response_SuperStoreCount.subscribe(onNext: { data in
//        }).disposed(by: disposeBag)
        
        Utility.UtilityResponse.subscribe(onNext: { [weak self]success in
            if success == false {
                Helper.showPI(string: "")
                self?.superStoreVModel.city = Utility.getAddress().CityId
                self?.superStoreVModel.getData()
                self?.setLocation()
            }
        }).disposed(by: disposeBag)
    }
    
    //App Version Check
    func checkAppVersion() {
        CommonAlertView.AlertPopupResponse.subscribe(onNext: { data in
            if data == CommonAlertView.ResponceType.AppVersion {
                //UIApplication.shared.openURL(URL(string: DynamicLinkingConstants.Appstore)!)
                UIApplication.shared.open(URL(string: DynamicLinkingConstants.Appstore)!, options: [:], completionHandler: nil)
            }
        }, onError: {error in
        }).disposed(by: APICalls.disposesBag)
        DispatchQueue.global().async {
            do {
                let update = try APICalls.isUpdateAvailable()
                if update == true {
                    APICalls.appVersion()
                }
            } catch {
                print(error)
            }
        }
    }

}
