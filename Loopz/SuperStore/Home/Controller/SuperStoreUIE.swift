//
//  SuperStoreUIE.swift
//  DelivX
//
//  Created by 3Embed on 12/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension SuperStoreVC:UIScrollViewDelegate {
    func setUpUI(){
        Fonts.setPrimaryRegular(locationLabel, size: 14)
        profileButton.tintColor = Colors.PrimaryText
        downArrow.tintColor = Colors.SeparatorLight
        locationLabel.textColor = Colors.PrimaryText
        setLocation()
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        navWidth.constant = UIScreen.main.bounds.width - 50
    }
    
    /// updates the location
    func setLocation()
    {
        let locationStored = Utility.getAddress()
        if locationLabel != nil
        {
            if locationStored.Name.length > 0 {
                locationLabel.text = locationStored.Name
            }else if locationStored.MainText.length > 0 {
                locationLabel.text = locationStored.MainText
            }else{
                locationLabel.text = locationStored.FullText
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        let contentOffSet = scrollView.contentOffset.y
        if contentOffSet > 3{
            Helper.addShadowToNavigationBar(controller: self.navigationController!)
        }else{
            Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        }
    }
}
