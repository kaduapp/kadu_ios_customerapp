//
//  ViewAllVM.swift
//  DelivX
//
//  Created by 3Embed on 02/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewAllVM: NSObject {

    var arrayOfItems        = [Item]()
    var type                = 0
    var index               = 1
    var store               = Store.init(data: [:])
    var response_ViewAllVM  = PublishSubject<Bool>()
    var titleLabel          = ""
    let disposeBag = DisposeBag()
    func getData(id:String) {
        HomeAPICalls.getViewAllItems(type: type, store: store, index: index,id: id).subscribe(onNext: { [weak self]data in
            if self?.index == 0{
                self?.arrayOfItems.removeAll()
            }
            if data.count != 0 {
                self?.index = (self?.index)! + 1
            }
            self?.arrayOfItems.append(contentsOf: data)
            self?.response_ViewAllVM.onNext(true)
        }).disposed(by: self.disposeBag)
    }
    
}
