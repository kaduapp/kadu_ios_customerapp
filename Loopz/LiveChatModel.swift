//
//  LiveChatModel.swift
//  DelivX
//
//  Created by apple on 10/27/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxAlamofire
import RxSwift
import RxCocoa

enum LiveChatResult {
    case success(URLRequest)
    case failure(Bool)
}

class LiveChatModel: NSObject {
    
    let licence_url = "https://cdn.livechatinc.com/app/mobile/urls.json"
    let licence_Key = "9485015"
    
    func getLiveChatAPIRequest(completionHandler: @escaping (LiveChatResult) -> ()) {
        
        RxAlamofire.requestJSON(.get, licence_url, parameters: [:], headers: [:]).debug().subscribe(onNext: { (head, body) in
            let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
            if errNum == .success {
                let dataIn = body as! [String : Any]
                if dataIn.isEmpty == false {
                    
                    if (dataIn["error"] == nil) {
                        let url = URL(string: self.prepareURL(dataIn["chat_url"] as! String))
                        let request = URLRequest(url: url!)
                        completionHandler(.success(request))
                    } else {
                        completionHandler(.failure(false))
                    }
                }
            }else{
//                APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
            }
        }, onError: { (Error) in
            Helper.hidePI()
        }).disposed(by: APICalls.disposesBag)
    }
    
    /// Prepare URL
    ///
    /// - Parameter url: URL after Requesting
    /// - Returns: URL to be loaded on WebPage
    func prepareURL(_ url: String) -> String {
        
        var string: String = "https://\(url)"
        string = string.replacingOccurrences(of: "{%license%}", with: licence_Key)
        string = string.replacingOccurrences(of: "{%group%}", with: Utility.AppName)
        return string
    }
}
