//
//  StoreCell.swift
//  DelivX
//
//  Created by 3Embed on 16/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class StoreCell: UITableViewCell {
    
    var ViewStore:DispenceryView = DispenceryView().shared
    var ViewStoreTitle:HeaderView = HeaderView()
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func setFullStore() {
        ViewStore.bounds = self.contentView.bounds
        ViewStore.frame.origin.x = 0
        ViewStore.frame.origin.y = 0
        self.contentView.addSubview(ViewStore)
        ViewStore.translatesAutoresizingMaskIntoConstraints = false
        let when = DispatchTime.now() + 0
        DispatchQueue.main.asyncAfter(deadline: when){
            self.contentView.addConstraint(NSLayoutConstraint(item: self.ViewStore, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0))
            self.contentView.addConstraint(NSLayoutConstraint(item: self.ViewStore, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0))
            self.contentView.addConstraint(NSLayoutConstraint(item: self.ViewStore, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: 0))
            self.contentView.addConstraint(NSLayoutConstraint(item: self.ViewStore, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0))
        }
    }
    
    
    func setTitleStore() {
        ViewStoreTitle = Helper.showHeader(title:"" , showViewAll: true) as! HeaderView
        ViewStoreTitle.bounds = self.contentView.bounds
        ViewStoreTitle.frame.origin.x = 0
        ViewStoreTitle.frame.origin.y = 0
        self.contentView.addSubview(ViewStoreTitle)
        ViewStoreTitle.translatesAutoresizingMaskIntoConstraints = false
        let when = DispatchTime.now() + 0
        DispatchQueue.main.asyncAfter(deadline: when){
            self.contentView.addConstraint(NSLayoutConstraint(item: self.ViewStoreTitle, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0))
            self.contentView.addConstraint(NSLayoutConstraint(item: self.ViewStoreTitle, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0))
            self.contentView.addConstraint(NSLayoutConstraint(item: self.ViewStoreTitle, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: 0))
            self.contentView.addConstraint(NSLayoutConstraint(item: self.ViewStoreTitle, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0))
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data:Store) {
        setFullStore()
        ViewStore.setData(data: data)
    }
    
    
    func setDataTitle(data:Store) {
        setTitleStore()
        ViewStoreTitle.headerTitle.text = data.Name
        ViewStoreTitle.viewAllText.setTitle(StringConstants.Change(), for: .normal)
        ViewStore.setData(data: data)
    }

}
