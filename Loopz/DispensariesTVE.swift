//
//  DispensariesTVE.swift
//  UFly
//
//  Created by 3 Embed on 24/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - UITableViewDataSource
extension DispensariesVC: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dispensariesVM.store.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:  UIConstants.CellIds.DispensariesTableCell) as! DispensariesTableCell
        cell.setData(data: dispensariesVM.store[indexPath.section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        Utility.saveStore(location: dispensariesVM.store[indexPath.section].dataObject)
       Utility.UtilityUpdate.onNext(Utility.TypeServe.ChangeStore)
        self.responceBack.onNext(dispensariesVM.store[indexPath.section])
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return Helper.showHeader(title: "", showViewAll: false)
    }
}
extension DispensariesVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableView {
            if self.navigationController != nil {
                if scrollView.contentOffset.y > 0 {
                    Helper.addShadowToNavigationBar(controller: self.navigationController!)
                }else{
                    Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                }
                
            }
            
        }
    }
    
    
}
