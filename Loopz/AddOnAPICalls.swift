//
//  AddOnAPICalls.swift
//  DelivX
//
//  Created by 3Embed on 01/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire

class AddOnAPICalls: NSObject {
    //get the addons
    class func getAddOnData(selectedItem:Item) ->Observable<([AddOn])> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        let url = "" + selectedItem.Id
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                _ = body as![String:Any]
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let addOnDetails = [AddOn]()
                    observer.onNext(addOnDetails)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
                }, onError: { (Error) in
                            Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                    Helper.hidePI()
                }).disposed(by: APICalls.disposesBag)
                return Disposables.create()
            }.share(replay: 1)
    }
}
