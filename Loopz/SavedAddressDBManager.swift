//
//  SavedAddressDBManager.swift
//  DelivX
//
//  Created by 3Embed on 27/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import GoogleMaps

class SavedAddressDBManager: NSObject {

    /// get Address
    ///
    /// - Parameter type: 1 for [String:Any] and 2 for Location
    /// - Returns: Array of cart
    func getAddressDocument() -> [Location] {
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.SavedAddressCouchDB)
        let doc: CBLDocument = CouchDBEvents.getDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        let array = (dic["Value"] as? [Any])!
        var addressArray = [Location]()
        for item in array {
            let itemObject = item as! [String:Any]
            let data = Location.init(data: item as! [String:Any])     //
            if let titleTemp = itemObject[GoogleKeys.LocationLat] as? Float, let titleTemp2 = itemObject[GoogleKeys.LocationLong] as? Float{
                let locationin = CLLocation(latitude: CLLocationDegrees(titleTemp), longitude: CLLocationDegrees(titleTemp2))
                data.update(data: itemObject, location: locationin)
            }
            if let titleTemp = itemObject[GoogleKeys.LocationLat] as? Double, let titleTemp2 = itemObject[GoogleKeys.LocationLong] as? Double{
                let locationin = CLLocation(latitude: CLLocationDegrees(titleTemp), longitude: CLLocationDegrees(titleTemp2))
                data.update(data: itemObject, location: locationin)
            }
            addressArray.append(data)
        }
        return addressArray
    }
    
    // updates the address document with newly added address
    func updateAddressDocument(data:[Address]) {
        
        let convertedArray:[Any]    = convertAddress(data: data)
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.SavedAddressCouchDB)
        CouchDBEvents.updateDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!, documentArray: convertedArray as [AnyObject])
    }
    
    
    func convertAddress(data: [Address]) -> [Any] {
        var arrayConverted:[Any]   = []
        for item in data {
            let location = Location.init(data: [:])
            location.setFromAddress(data: item)
            let dataAddress = [
                GoogleKeys.PlaceId             : location.PlaceId,
                GoogleKeys.Description         : location.LocationDescription,
                GoogleKeys.MainText            : location.MainText,
                GoogleKeys.AddressName         : location.Name,
                GoogleKeys.AddressCity         : location.City,
                GoogleKeys.AddressState        : location.State,
                GoogleKeys.AddressZIP          : location.Zipcode,
                GoogleKeys.AddressCountry      : location.Country,
                GoogleKeys.LocationLat         : location.Latitude,
                GoogleKeys.LocationLong        : location.Longitude,
                GoogleKeys.ZoneId              : "",
                GoogleKeys.ZoneName            : "",
                APIResponceParams.Tag          : location.Tag
                ] as [String : Any]
            var available = false
            for address in arrayConverted {
                let addressIn = address as! [String:Any]
                if (addressIn[GoogleKeys.LocationLat] as! Float == dataAddress[GoogleKeys.LocationLat] as! Float && addressIn[GoogleKeys.LocationLong] as! Float == dataAddress[GoogleKeys.LocationLong] as! Float) || dataAddress[GoogleKeys.MainText] as! String == addressIn[GoogleKeys.MainText] as! String {
                    available = true
                }
            }
            if !available {
                arrayConverted.append(dataAddress)
            }
        }
        return arrayConverted
    }
    
    
    
    func deleteAddress(data:Address) {
        let converted:[String:Any]    = convertAddress(data: [data])[0] as! [String : Any]
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.SavedAddressCouchDB)
        let doc: CBLDocument = CouchDBEvents.getDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        let array = (dic["Value"] as? [Any])!
        var arrayUpdate:[Any] = []
        for item in array {
            let itemObject = item as! [String:Any]
            if (itemObject[GoogleKeys.LocationLat] as! Float != converted[GoogleKeys.LocationLat] as! Float && itemObject[GoogleKeys.LocationLong] as! Float != converted[GoogleKeys.LocationLong] as! Float) || itemObject[GoogleKeys.MainText] as! String != converted[GoogleKeys.MainText] as! String {
                arrayUpdate.append(item)
            }
        }
        CouchDBEvents.updateDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!, documentArray: arrayUpdate as [AnyObject])
    }
}
