//
//  CollectionSlotsViewController+TableView.swift
//  DelivX
//
//  Created by 3EMBED on 23/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation


extension CollectionSlotsViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tableType = viewModel.getTableTypeForRowsCount(at: tableView.tag)
        switch tableType {
        case .datesTableview :
            return viewModel.getPickUpSlotsCount()
        case .slotsTableview :
            return  viewModel.slots.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableType = viewModel.getTableTypeForRowsCount(at: tableView.tag)
        switch tableType {
        case .datesTableview :
            if let date = viewModel.getSlotDate(at: indexPath.row) as? (String,String),let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DatesTableViewCell.self), for: indexPath) as? DatesTableViewCell {
                cell.updateSlotDateCell(date.0)
                cell.backgroundLabel.backgroundColor = .white
                 cell.selectionStyle = .none
                if let pickeddateid = viewModel.pickeddateId, pickeddateid == date.1
                {
                    cell.backgroundLabel.backgroundColor = Helper.getUIColor(color:"3454DB").withAlphaComponent(0.04)
                    Fonts.setPrimaryBold(cell.datesLabel)
                    
                }else
                {
                    cell.backgroundLabel.backgroundColor = .white
                }
                return cell
            }
            
        case .slotsTableview :
            if  let time = viewModel.getSlotTime(at: indexPath.row) as? (String,String,String),let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SlotsTableViewCell.self), for: indexPath) as? SlotsTableViewCell {
                cell.updateSlotsLabel(time.0)
                cell.updateslotsendTimeLabel(time.1)
                if let pickeddateid = viewModel.slotId , pickeddateid == time.2
                {
                    cell.backgroundLabel.backgroundColor = Helper.getUIColor(color:"3454DB").withAlphaComponent(0.04)
                    tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                    Fonts.setPrimaryBold(cell.slotsLabel)
                    Fonts.setPrimaryBold(cell.slotsendTimeLabel)
                }else
                {
                    cell.backgroundLabel.backgroundColor = .white
                }
                 cell.selectionStyle = .none
               
                return cell
            }
       }
         return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let tableType = viewModel.getTableTypeForRowsCount(at: tableView.tag)
        switch tableType {
        case .datesTableview :
            if let cell = tableView.cellForRow(at: indexPath) as? DatesTableViewCell
            {
                Fonts.setPrimaryLight(cell.datesLabel as Any)
                cell.backgroundLabel.backgroundColor = .white
//                if let date = viewModel.getSlotDate(at: indexPath.row) as? (String,String),let pickeddateid = viewModel.pickeddateId, pickeddateid == date.1
//                {
//                    viewModel.slotId = nil
//                }
            }
        case .slotsTableview :
            if let cell = tableView.cellForRow(at: indexPath) as? SlotsTableViewCell
            {
                // self.viewModel.slotId = nil
                Fonts.setPrimaryLight(cell.slotsLabel)
                Fonts.setPrimaryLight(cell.slotsendTimeLabel)
                  cell.backgroundLabel.backgroundColor = .white
                

            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tableType = viewModel.getTableTypeForRowsCount(at: tableView.tag)
        switch tableType {
        case .datesTableview :
            if let cell = tableView.cellForRow(at: indexPath) as? DatesTableViewCell ,let dateStr  = cell.datesLabel.text
            {//#EFF0F7 rgba(52, 84, 219, 1)
                Fonts.setPrimaryBold(cell.datesLabel, size: 14.0)
                viewModel.getSelectedIndexForTableview(index: indexPath.row)
                self.slotsTableView.reloadData()
                
                 cell.backgroundLabel.backgroundColor = Helper.getUIColor(color:"3454DB").withAlphaComponent(0.04)
                 viewModel.selectDate = viewModel.removeSpacesandnewlineFromtime(time:dateStr)
               // viewModel.pickeddateId =
                if let selectDate = viewModel.getSlotDate(at: indexPath.row) as? (String,String)
                {
                    viewModel.pickeddateId = selectDate.1
                  
                }
            }
        case .slotsTableview :
            if let cell = tableView.cellForRow(at: indexPath) as? SlotsTableViewCell,let time = viewModel.getSlotTime(at: indexPath.row ) as? (String,String,String)
            {
                Fonts.setPrimaryBold(cell.slotsLabel)
                Fonts.setPrimaryBold(cell.slotsendTimeLabel)
                 cell.backgroundLabel.backgroundColor = Helper.getUIColor(color:"3454DB").withAlphaComponent(0.04)
                viewModel.selectSlot = viewModel.removeSpacesandnewlineFromtime(time:time.0 + " - " + time.1)
                viewModel.slotId = time.2
                
                
            }
         }
        
    }
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
        {
            let tableType = viewModel.getTableTypeForRowsCount(at: tableView.tag)
            switch tableType {
            case .datesTableview :
                return 57
            case .slotsTableview :
                return 57
            }
    }
    
}
