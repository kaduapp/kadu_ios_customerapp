//
//  NavigationView.swift
//  DelivX
//
//  Created by 3Embed on 22/10/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//


import UIKit

class NavigationView: UIView {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    var obj: NavigationView? = nil
    var shared: NavigationView {
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[9] as? NavigationView
            obj?.frame = CGRect(x: 0, y: 0, width: 300, height: 44)
            Fonts.setPrimaryMedium(obj?.titleLabel as Any)
            obj?.titleLabel.textColor = Colors.PrimaryText
        }
    
        return obj!
        
    }
    
    func setData(title: String) {
        if title.sorted().count > 0{
            titleLabel.text = title
        }else{
            titleLabel.text = ""
        }
        self.layoutIfNeeded()
        self.updateConstraintsIfNeeded()
    }
}

