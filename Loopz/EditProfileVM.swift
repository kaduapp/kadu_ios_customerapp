//
//  EditProfileVM.swift
//  UFly
//
//  Created by 3 Embed on 30/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class EditProfileVM: NSObject {
    
    
    /// this enum is of type Integer
    ///
    /// - validEmail: is having value 1 for valid email response
    /// - validPhoneNum:   is having value 2 for valid validPhoneNum response
    /// - updateProfile:  is having value 3 for valid updateProfile response
    /// - updatePhone:  is having value 4 for valid updatePhone response
    /// - Logout:  is having value 5 for valid Logout response
    enum ResponseType: Int {
        
        case validEmail = 1
        case validPhoneNum = 2
        case updateProfile = 3
        case updatePhone   = 4
        case Logout        = 5
    }
    
    enum ImageType:Int{
        case iDCard  = 200
        case mmjCard = 210
        case profilePic = 230
    }
    
    var btnTag:ImageType?
    var pickedImage:UIImage?
    
    var pickedIDImage = ""
    var pickedMMJImage = ""
    
    static let EditProfileVM_response = PublishSubject<ResponseType>()
    
    let pickerObj:ImagePickerModel? = ImagePickerModel.sharedImagePicker
    var uploadImageModel  = UploadImageModel.shared
    var userData:User? = nil
    var authentication = Authentication()
    
    var isValidEmail = false
    var isValidPhone = false
    var isSaveAction = false
    var profileUpdated = false
    let disposeBag = DisposeBag()
    
    /// validates email and phone number
    func validateData(){
        if !isValidEmail {
            if self.authentication.Email == userData?.Email {
                isValidEmail = true
                
            }else{
                isValidEmail = false
                self.validateEmailOrPhone(verifyType: 2)
            }
        }
        if !isValidPhone {
            if self.authentication.Phone == userData?.Phone  {
                self.isValidPhone = true
            }
            else {
                self.isValidPhone = false
                if self.authentication.Phone.count != 0 {
                    self.validateEmailOrPhone(verifyType: 1)
                }
            }
        }
//        if self.isValidPhone && self.isValidEmail {
//            updateProfile()
//        }
        
        if self.isValidPhone && self.isValidEmail && !profileUpdated {
            profileUpdated = true
            updateProfile()
        }

        
        
        
    }
    
    
    /// this method subscribe for update profile Api defined in EditProfileAPICalls
    func updateProfile(){
        if self.isValidPhone && self.isValidEmail {
            
            if userData?.Phone == authentication.Phone {
                authentication.Phone = ""
                
            }
            if userData?.Email == authentication.Email {
                authentication.Email = ""
            }
            ProfileAPICalls.updateProfileData(user:authentication).subscribe(onNext: { success in
                if success {
                    EditProfileVM.EditProfileVM_response.onNext(.updateProfile)
                    
                }
            } , onError: { error in
                print(error)
            }).disposed(by: disposeBag)
            
        }
    }
    
    
    /// validates email or phone number
    ///
    /// - Parameter verifyType: it is of type Integer 1 for phoneNumber 2 for emailId
    func validateEmailOrPhone(verifyType: Int) {
        AuthenticationAPICalls.validatePhoneEmail(user:authentication,verifyType: verifyType, showPop: true).subscribe(onNext: {
            [weak self]success in
            Helper.hidePI()
            if verifyType == 2 {
                self?.isValidEmail = success.0
                if success.0 {
                    EditProfileVM.EditProfileVM_response.onNext(.validEmail)
                }
            }
            else {
                self?.isValidPhone = success.0
                if self?.authentication.Phone != self?.userData?.Phone && (self?.isValidPhone)! {
                    EditProfileVM.EditProfileVM_response.onNext(.updatePhone)
                }else{
                    if (self?.isValidPhone)! {
                        EditProfileVM.EditProfileVM_response.onNext(.validPhoneNum)
                    }
                }
            }
            
        }, onError: {error in
            print(error)
        }).disposed(by: disposeBag)
    }
    
    /// observe logout for logout
    func logout() {
        AuthenticationAPICalls.logoutAPI().subscribe(onNext: { result in
            EditProfileVM.EditProfileVM_response.onNext(.Logout)
        } , onError: { error in
            print(error)
        }).disposed(by: disposeBag)
    }
    
    
}



