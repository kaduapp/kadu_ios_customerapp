//
//  ItemImageCVC.swift
//  UFly
//
//  Created by 3 Embed on 25/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ItemImageCVC: UICollectionViewCell {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    
    override func awakeFromNib() {
    }
    
    /// updates the item image
    ///
    /// - Parameters:
    ///   - data: is of type item model object having item images
    ///   - index: it is of type integer
    func updateCell(data:Item,index: Int) {
        if data.Images.count>0 {
            if data.Images.count > 0 {
                let url = data.Images[index]
                Helper.setImage(imageView: itemImage, url: url, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
            }
        }
    }
}
