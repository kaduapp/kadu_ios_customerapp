//
//  LocationAPICalls.swift
//  UFly
//
//  Created by 3Embed on 10/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire

class LocationAPICalls: NSObject {
    static let disposeBag = DisposeBag()
    //Operation ZOne API
    class func operationZoneAPI(location:Location, launch:Bool) -> Observable<[String:Any]> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        
        var url = ""
        if Changebles.isGrocerOnly{
         
          url = "\(APIEndTails.OperationZone)/\(2)/\(location.Latitude)/\(location.Longitude)"
        }
        else{
               url = "\(APIEndTails.OperationZone)/\(0)/\(location.Latitude)/\(location.Longitude)"
        }
        
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as! [String:Any]
                Helper.hidePI()
                let errNum = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var SuperStores:[SuperStore] = []
                    var storeIn:Store? = Store.init(data: [:])
                    
                    if let superCategoryDataArray =  bodyIn["categoryData"] as? [[String:Any]]{
                        
                        for data in superCategoryDataArray {
                            let superStore = SuperStore.init(data: data )
                            SuperStores.append(superStore)
                        }
                        
                        if SuperStores.count > 0 && !Changebles.isGrocerOnly{
                            UserDefaults.standard.set(SuperStores[0].Id, forKey: UserDefaultConstants.SelectedSuperStoreID)
                            observer.onNext(bodyIn[APIResponceParams.Data] as! [String : Any])
                            
                        }
                        else if   let storeDataArray = bodyIn["storeData"] as? [[String:Any]],storeDataArray.count > 0,SuperStores.count > 0{
                            
                            storeIn = Store.init(data: storeDataArray[0])
                            Utility.saveStore(location: storeDataArray[0])
                            UserDefaults.standard.set(SuperStores[0].Id, forKey: UserDefaultConstants.SelectedSuperStoreID)
                            observer.onNext(bodyIn[APIResponceParams.Data] as! [String : Any])
                        }
                        else{
                            
                            observer.onNext([:])
                        }
                        
                    }
                    observer.onCompleted()
                }else if errNum == .badRequest && launch == false {
                    observer.onNext([:])
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:bodyIn,status: head.statusCode)
                }

            }, onError: { (Error) in
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    

}
