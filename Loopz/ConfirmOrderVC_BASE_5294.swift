//
//  ConfirmOrderVC.swift
//  DelivX
//
//  Created by 3 Embed on 16/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ConfirmOrderVC: UIViewController {
    
    @IBOutlet weak var placeOrderButton: UIButton!
    @IBOutlet weak var mainTableVew: UITableView!
    @IBOutlet weak var placeOrderView: UIView!
    
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var totalView: UIView!
    
    var tempText = ""
    var checkoutVM = CheckoutVM()
    var navView = NavigationView().shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getWallet()
        self.setData()
        self.didGetResponse()
       // payOrder()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkoutVM.checkout()
        //self.title = StringConstants.ConfirmOrder()
    }
    
    @IBAction func placeOrderAction(_ sender: Any) {
        
        if checkoutVM.pickup == 1 {
            if checkoutVM.selectedAddress == nil {
                Helper.showAlert(message: StringConstants.SelectAddress(), head: StringConstants.Error(), type: 1)
                return
            }
        }
        if self.checkoutVM.paymentHandler.isPayByWallet && !(self.checkoutVM.paymentHandler.isPayCashWallet || self.checkoutVM.paymentHandler.isPayCardWallet){
            let amount = Utility.getWallet().Amount
            let totlaPrice =  AppConstants.TotalCartPriceWithTax + AppConstants.TotalDeliveryPrice - self.checkoutVM.discount
            if amount < totlaPrice {
                self.performSegue(withIdentifier: UIConstants.SegueIds.ConfirmToPaymentOp, sender: false)
                Helper.showAlert(message: StringConstants.InsufficientBalance(), head: StringConstants.Alert(), type: 1)
                return
            }
        }/*else if self.checkoutVM.Payment == .iDeal {
            
            checkoutVM.Amount = 0
            if checkoutVM.pickup == 1 {
                checkoutVM.Amount = AppConstants.TotalDeliveryPrice
            }
            checkoutVM.Amount = checkoutVM.Amount + AppConstants.TotalCartPriceWithTax - checkoutVM.discount
            //                bitPayVM.Amount = checkoutVM.Amount
            //                bitPayVM.CreateTransaction()
            payOrder()
            return
        }*/
        checkoutVM.placeOrder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == UIConstants.SegueIds.checkoutToSavedAddress {
            let nav = segue.destination as! UINavigationController
            if let viewController: SavedAddressVC = nav.viewControllers.first as! SavedAddressVC? {
                viewController.isCheckout = true
                viewController.delegate = self
            }
        } else if segue.identifier == UIConstants.SegueIds.DatePickerVC {
            if let viewController: DatePickerVC = segue.destination as? DatePickerVC {
                viewController.selectedDate = self.checkoutVM.deliveryDate
                let dataButton = sender as! UIButton
                if dataButton.tag == 10 {
                    viewController.ModeDate = true
                    viewController.TitleString = StringConstants.SelectDate()
                }else{
                    viewController.ModeDate = false
                    viewController.TitleString = StringConstants.SelectSlot()
                }
                viewController.Response_Picker.subscribe(onNext: { success in
                    if dataButton.tag == 10 {
                        self.checkoutVM.deliveryDate = success
                    }else{
                        self.checkoutVM.deliveryTime = success
                    }
                    var mySet: IndexSet = IndexSet.init()
                    mySet.insert(0)
                    self.mainTableVew.reloadSections(mySet, with: .fade)
                }).disposed(by: self.checkoutVM.disposeBag)
            }
        }
        else if segue.identifier == UIConstants.SegueIds.ConfirmVCToCouponVC {
            if let viewController:CouponVC = segue.destination as? CouponVC {
                viewController.checkoutVM = self.checkoutVM
            }
            
        }else if segue.identifier == UIConstants.SegueIds.ConfirmToPaymentOp {
            if let viewController: PaymentOptionsVC = segue.destination as? PaymentOptionsVC{
                self.title = ""
                let sen = sender as! Bool
                viewController.priority = sen
                viewController.discount = checkoutVM.discount
                viewController.methodHandler =  checkoutVM.paymentHandler
                viewController.responseData.subscribe(onNext: { success in
                    self.checkoutVM.selectedCard = success.0
                    if success.2 {
                        self.checkoutVM.paymentHandler = success.1
                        self.checkoutVM.isPaymentMethodSelected = true
                        self.mainTableVew.reloadData()
                        self.updateBtn()
                    }else{
                        self.checkoutVM.isPaymentMethodSelected = false
                        self.checkoutVM.placeOrder()
                    }
                }).disposed(by: self.checkoutVM.disposeBag)
            }
        }else if segue.identifier == String(describing: WebVC.self) {
            let linkIn = sender as! String
            if let viewController:WebVC = segue.destination as? WebVC {
//                viewController.urlGiven = "https://payments.hoody.nl/payment/"
                viewController.urlGiven = linkIn    //"https://payments.hoody.nl/payment/"

                viewController.responseObserver.subscribe(onNext: { [weak self]data in
                    if data.0{
                        self?.checkoutVM.src_Id = data.1
                        self?.checkoutVM.placeOrder()
                    }else{
                        Helper.showAlert(message: StringConstants.PaymentFailed(), head: StringConstants.Error(), type: 1)
                    }
                }).disposed(by: self.checkoutVM.disposeBag)
                viewController.titleText = StringConstants.Payment()
            }
        }
    }
}
extension ConfirmOrderVC {
    
    /// observe to view model
    func didGetResponse(){
        checkoutVM.checkoutVM_response.subscribe(onNext: {success in
            
            if success == CheckoutVM.ResponseType.AddressSelected{
                let btn = UIButton()
                btn.tag = 0
                self.removeBtnPressed(sender: btn)
                self.mainTableVew.reloadData()
            }else if success == CheckoutVM.ResponseType.AddressWrong {
                self.checkoutVM.selectedAddress = nil
                self.mainTableVew.reloadData()
            }else if success == CheckoutVM.ResponseType.OrderPlaced {
                
              /*  if self.checkoutVM.Payment == .iDeal{
                    CheckoutAPICalls.sendTheScrId(id:self.checkoutVM.src_Id , amount: self.checkoutVM.Amount,orderId:self.checkoutVM.orderId).subscribe { (success) in
                        print("Paid successFully")
                        Helper.changetTabTo(index: AppConstants.Tabs.History)
                        self.navigationController?.popToRootViewController(animated: true)
                        }.disposed(by: self.checkoutVM.disposeBag)

                    return
                } */
                Helper.changetTabTo(index: AppConstants.Tabs.History)
                WalletAPICalls.getBalance().subscribe({ data in
                self.navigationController?.popToRootViewController(animated: true)
                }).disposed(by: APICalls.disposesBag)
          
            }else if success == CheckoutVM.ResponseType.Promocode {
                self.mainTableVew.reloadData()
            }else if success == CheckoutVM.ResponseType.Checkout {
                self.navigationController?.popToRootViewController(animated: true)
            }
            self.amount.text = Helper.setCurrency(displayAmount: AppConstants.TotalCartPriceWithTax + AppConstants.TotalDeliveryPrice - self.checkoutVM.discount, price: AppConstants.TotalCartPriceWithTax + AppConstants.TotalDeliveryPrice - self.checkoutVM.discount).string
        }, onError: {error in
            print(error)
        }).disposed(by: self.checkoutVM.disposeBag)
        
        APICalls.LogoutInfoTo.subscribe(onNext: { success in
            if success {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }).disposed(by: self.checkoutVM.disposeBag)
    

    }
    
    func payOrder() {
        DynamicLinking.getDynamicLink(itemId: "/success").subscribe(onNext: { data in
            //let dataPrice:UInt = UInt(self.checkoutVM.Amount)
            let dataPrice:UInt = UInt(self.checkoutVM.Amount * 100)
            let sourceParams = STPSourceParams.sofortParams(withAmount: dataPrice,
                returnURL: data,
                country: "DE",
                statementDescriptor: "order")
            sourceParams.currency = "eur"
            sourceParams.type = .IDEAL
            STPAPIClient.shared().createSource(with: sourceParams) { (source, error) in
                if error == nil {
                    if source?.flow == .redirect {
                        Helper.hidePI()
                        UIApplication.shared.open((source?.redirect?.url)!, options: [:], completionHandler: nil)
                        AppDelegate.app_response.subscribe(onNext: { [weak self]data in
                            if data == 1 {
                                STPAPIClient.shared().retrieveSource(withId: (source?.stripeID)!, clientSecret: (source?.clientSecret)!, completion: { (source2, error2) in
                                    if source2?.status == .chargeable {
                                        self?.checkoutVM.src_Id = (source?.stripeID)!
                                        self?.checkoutVM.placeOrder()
                                    }else if source2?.status == .failed {
                                        Helper.showAlert(message: StringConstants.PaymentFailed(), head: StringConstants.Error(), type: 1)
                                    }
                                })

                            }
                        }).disposed(by: self.checkoutVM.disposeBag)
                    }
                }
            }
        }, onCompleted: {
            
        }).disposed(by: self.checkoutVM.disposeBag)

    }
}
extension ConfirmOrderVC: CouponVCDelegate {
    func couponRespons(success: Bool) {
        if success == true {
            self.mainTableVew.reloadData()
        }
    }
    
}

// MARK: - SavedAddressVCDelegate
extension ConfirmOrderVC: SavedAddressVCDelegate {
    
    func didSelectAddress(address: Address){
        checkoutVM.selectedAddress = address
        mainTableVew.reloadData()
        checkoutVM.getFare()
        updateBtn()
        CommonAlertView.AlertPopupResponse.subscribe(onNext: { data in
            if data == CommonAlertView.ResponceType.Delete {
                self.checkoutVM.promocode = ""
                self.checkoutVM.discount = 0
                self.amount.text = "\(AppConstants.TotalCartPrice)"
                self.mainTableVew.reloadData()
            }
        }, onError: {error in
        }).disposed(by: self.checkoutVM.disposeBag)
    }
}

extension ConfirmOrderVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableVew {
            if self.navigationController != nil {
                let contentOffset = scrollView.contentOffset.y
                if contentOffset > 0 {
                    Helper.addShadowToNavigationBar(controller: self.navigationController!)
                }else{
                    Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                }
            }
        }
    }
    
}

