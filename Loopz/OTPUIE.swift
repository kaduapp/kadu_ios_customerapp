//
//  OTPUIE.swift
//  DelivX
//
//  Created by 3 Embed on 25/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension OTPVC {
    
    /// dismiss keboard and the closes the screen
    func beforeDismiss() {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.dismissKeyboard()
        }) { (Bool) in
            UIView.animate(withDuration: 0.3, animations: {
                // _ = self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    /// initial view setup
    func updateView(){
        navigationBackView.isHidden = true
        navigationController?.navigationBar.backgroundColor = UIColor.clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view?.backgroundColor = UIColor.clear
        Helper.removeNavigationSeparator(controller: self.navigationController!,image: false)
        otpView.comeFromFP(flag: otpVM.isFromForgotPW)
        
        otpView.updateMobileNumber(phoneNumber:(otpVM.authentication?.CountryCode)!+(otpVM.authentication?.Phone)!)
        
        if otpVM.isFromForgotPW == false  {
            otpVM.getOTP(show: false)
        }
        if #available(iOS 12.0, *) {
            self.otpView.textFieldOtp0.textContentType = .oneTimeCode
            self.otpView.textFieldOtp1.textContentType = .oneTimeCode
            self.otpView.textFieldOtp2.textContentType = .oneTimeCode
            self.otpView.textFieldOtp3.textContentType = .oneTimeCode
            self.otpView.textFieldOtp4.textContentType = .oneTimeCode
            self.otpView.textFieldOtp5.textContentType = .oneTimeCode
        } else {
            // Fallback on earlier versions
        }
       

        self.otpView.textFieldOtp0.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.otpView.textFieldOtp1.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.otpView.textFieldOtp2.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.otpView.textFieldOtp3.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.otpView.textFieldOtp4.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.otpView.textFieldOtp5.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    
    func showDefaultOTP(){
        otpView.textFieldOtp0.text = "1"
        otpView.textFieldOtp1.text = "1"
        otpView.textFieldOtp2.text = "1"
        otpView.textFieldOtp3.text = "1"
        otpView.textFieldOtp4.text = "1"
        otpView.textFieldOtp5.text = "1"
        
        otpView.tfSeparator0.backgroundColor =  Colors.AppBaseColor
        otpView.tfSeparator1.backgroundColor =  Colors.AppBaseColor
        otpView.tfSeparator2.backgroundColor =  Colors.AppBaseColor
        otpView.tfSeparator3.backgroundColor =  Colors.AppBaseColor
        otpView.tfSeparator4.backgroundColor =  Colors.AppBaseColor
        otpView.tfSeparator5.backgroundColor =  Colors.AppBaseColor
        otpView.textFieldOtp5.becomeFirstResponder()
        Helper.setButton(button: otpView.verifyButton,view: otpView.verifyButtonView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
    }
    
    
    @objc override func keyboardWillShow(_ notification: NSNotification){
        // Do something here
        
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
        let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber
        
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: TimeInterval(truncating: duration), delay: 0, options: [UIView.AnimationOptions(rawValue: UInt(truncating: curve))], animations: {
            
            let info = notification.userInfo!
            let inputViewFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height - inputViewFrame.size.height
            self.view.frame = frame
            self.view.layoutIfNeeded()
            self.otpView.scrollView.scrollRectToVisible(CGRect.init(x:self.otpView.verifyButtonView.frame.origin.x , y: self.otpView.verifyButtonView.frame.origin.y + 200, width:self.otpView.verifyButtonView.frame.size.width , height: 70), animated: true)
            
            
        }, completion: nil)
        
    }
    
}

