//
//  SlotsTableViewCell.swift
//  DelivX
//
//  Created by 3EMBED on 25/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SlotsTableViewCell: UITableViewCell {
    @IBOutlet weak var slotsLabel: UILabel!

    @IBOutlet weak var backgroundLabel: UILabel!
    @IBOutlet weak var slotsendTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = .white
        self.preservesSuperviewLayoutMargins = false
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
    }
    //func to update SlotsLabel
    func updateSlotsLabel(_ str:String)
    {
//        let dateFormatterGet = DateFormatter()
//        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//
//        let dateFormatterPrint = DateFormatter()
//        dateFormatterPrint.dateFormat = "h:mm \n a"
//
//        if let date = dateFormatterGet.date(from: "2019-03-27T14:30:00.000Z") {
//            slotsLabel.text = dateFormatterPrint.string(from: date).lowercased()
//        } else {
//            print("There was an error decoding the string")
//        }
         slotsLabel.text = str
         Fonts.setPrimaryLight(slotsLabel)
    }
    func updateslotsendTimeLabel(_ str:String)  {
        slotsendTimeLabel.text = str
        Fonts.setPrimaryLight(slotsendTimeLabel)
    }
    //LabelviewModel.updateDateFromObject(obj)
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
