//
//  HomeVC.swift
//  UFly
//
//  Created by 3Embed on 07/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import ListPlaceholder
import RxSwift
import RxCocoa

class HomeVC: UIViewController {
    
    @IBOutlet weak var ratingValue: UIButton!
    @IBOutlet weak var ratingHead: UILabel!
    @IBOutlet weak var distanceValue: UILabel!
    @IBOutlet weak var distanceHead: UILabel!
    @IBOutlet weak var costForTwoValue: UILabel!
    @IBOutlet weak var costForTwoHead: UILabel!
    @IBOutlet weak var restaurantStack: UIStackView!
    //@IBOutlet weak var freedeliveryHeight: NSLayoutConstraint!
    let disposeBag = DisposeBag()
    let backButton = UIButton_RotateButtonClass()
    let navTitleLabel = UILabel.init()
    @IBOutlet weak var superTitle: UILabel!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var titleView: UIView!
    
    @IBOutlet weak var viewForDottedLine: UIView!
    
    @IBOutlet weak var dispenceryViewHVC: UIView!
    
    @IBOutlet weak var storeAddressLabel: UILabel!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var secondViewForDottedLine: UIView!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var secondDottedlineViewHeight: NSLayoutConstraint!
    @IBOutlet weak var selectStoreTitle: UILabel!
    //BottomCart View

    @IBOutlet weak var viewCartBar: UIView!
    @IBOutlet weak var extraCharges: UILabel!
    @IBOutlet weak var viewCartBtn: UIButton!
    @IBOutlet weak var viewCartBarHight: NSLayoutConstraint!
    @IBOutlet weak var viewCartlabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var itemCount: UILabel!
    
    @IBOutlet weak var favButtonOutlet: UIButton!
    
    
    @IBOutlet weak var searchButtonOutlet: UIButton!
    
    
    // Change Store View

    @IBOutlet weak var storeView: UIView!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var changeButtonView: UIView!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var currentStoreImage: UIImageView!
    @IBOutlet weak var storeViewHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressHeadLabel: UILabel!
    @IBOutlet weak var grocerNavigationWidth: NSLayoutConstraint!
    @IBOutlet weak var grocerNavigationView: UIView!
    @IBOutlet weak var navigationView: UIView!
    
    var checkStoreFavStatus : ((Bool,Store) ->(Void)) = {
        status,Store  in
    }
    var loading = true
    var refreshControl: UIRefreshControl!
    var homeVM = HomeVM()
     var cartVM = CartVM()
    var isGuestLogin = false
//    let transition = PopAnimator()
    var selected:UICollectionViewCell? = nil
    var ButtonSelected:UIView? = nil
    var selectedOffer = 0

    @IBAction func favoriteButtonAction(_ sender: Any) {
    
    if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
    FavouriteAPICalls.favouriteStore(store:homeVM.store).subscribe(onNext: { [weak self]result in
            if result{
                if let handler = self?.checkStoreFavStatus,let store = self?.homeVM.store {
                    handler(true,store)
                }
                self?.homeVM.store.Fav = true
                self?.favButtonOutlet.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
            }else{
                if let handler = self?.checkStoreFavStatus,let store = self?.homeVM.store {
                    handler(false,store)
                }
                self?.homeVM.store.Fav = false
                self?.favButtonOutlet.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
            }
            }, onError: { error in
                print(error)
        }).disposed(by: self.homeVM.disposeBag)
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
         performSegue(withIdentifier: String(describing: SearchVC.self), sender: nil)
    }
    

    // MARK: - UIButton Action
    @IBAction func addressButtonAction(_ sender: UIButton) {
        if AppConstants.CartCount > 0 {
            Helper.showAlertReturn(message:StringConstants.RemoveCartWarning() , head:StringConstants.Message() , type: StringConstants.DeleteSmall(), closeHide: false,responce:CommonAlertView.ResponceType.Address)
        }else{
            self.performSegue(withIdentifier:UIConstants.SegueIds.HomeToLocation, sender: self)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cartVM.getCart()
        homeVM.setSubscribe()
        homeVM.getData()
        didGetCategories()
        setUIStatic()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(HomeVC.refresh), for: UIControl.Event.valueChanged)
        mainTableView.addSubview(refreshControl)
        mainTableView.isHidden = true
        // Do any additional setup after loading the view.
        homeVM.List_Store = Utility.getStore()
        if !Utility.getLogin(){
            isGuestLogin = true
        }
    }
    
    @objc func refresh() {
        loading = true
        Helper.showPI(string: "")
        DispatchQueue.main.async {
            self.mainTableView.reloadData()
        }
        homeVM.getData()
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.refreshControl.endRefreshing()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setStore()
//        Helper.removeShadowToNavigationBar(controller: self.navigationController!)

        if  !Changebles.isGrocerOnly{
             self.showBottom()
        self.tabBarController?.tabBar.isHidden = true
        }
        
        if Utility.getLogin() && isGuestLogin{
            cartVM.getCart()
            isGuestLogin = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        if loading {
//            mainTableView.isHidden = false
//            mainTableView.reloadData()
//        }
        self.setLocation()
        //setBanner()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let headerView = mainTableView.tableHeaderView {
            let height = self.dispenceryViewHVC.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            //Comparison necessary to avoid infinite loop
//            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                mainTableView.tableHeaderView = headerView
//            }
        }
    }

    
    @IBAction func storeChangeAction(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "SuperStore", bundle: nil)
        let allStoresVC = storyboard.instantiateViewController(withIdentifier: "StoresListVC") as! StoresListVC
        self.navigationController?.pushViewController(allStoresVC, animated: true)
    }
    // MARK: - UIButton Action
    
    
    @IBAction func didTapViewCartBtn(_ sender: UIButton) {
        //        self.hideBottom(sender: sender)
        //        Helper.changetTabTo(index: AppConstants.Tabs.Cart)
        self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
    }

    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.HomeToItemList {
            if let viewController: ItemListingVC = segue.destination as? ItemListingVC{
                let data:[Any] = sender as! [Any]
                viewController.itemListingVM.controllerCategory = data[0] as? Category
                let dataSubcategory = data[1] as? SubCategory
                viewController.itemListingVM.subCategorySelected = dataSubcategory!
                if homeVM.store.Id.length > 0 {
                    viewController.itemListingVM.store = homeVM.store
                    viewController.itemListingVM.hideStore = true
                }
            }
        }else if segue.identifier == UIConstants.SegueIds.HomeToItemDetail {
            let button = sender as! Item
            if let nav:UINavigationController = segue.destination as? UINavigationController {
                if let itemDetailVC: ItemDetailVC = nav.viewControllers.first as? ItemDetailVC{
//                itemDetailVC.transitioningDelegate = self
                itemDetailVC.itemDetailVM.item = button
                itemDetailVC.viewCartHandler = {status in
                self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
                    }
                 if  !Changebles.isGrocerOnly{
                   self.tabBarController?.tabBar.isHidden = true
                    }
                }
            }
        }else if segue.identifier == String(describing: ViewAllVC.self) {
            if let viewAllVC: ViewAllVC =  segue.destination as? ViewAllVC {
                viewAllVC.viewAllVM.store = homeVM.store
                let senderIn:Int = sender as! Int
                switch senderIn {
                case 1:
                    viewAllVC.viewAllVM.arrayOfItems = homeVM.items
                    viewAllVC.viewAllVM.type = 1
                    break
                case 2:
                    viewAllVC.viewAllVM.arrayOfItems = homeVM.itemsLow
                    viewAllVC.viewAllVM.type = 2
                    break
                case 3:
                    viewAllVC.viewAllVM.arrayOfItems = homeVM.itemsFav
                    viewAllVC.viewAllVM.type = 0
                    break
                case 100:
                    viewAllVC.viewAllVM.arrayOfItems = []
                    viewAllVC.viewAllVM.type = 2
                    viewAllVC.offerId = homeVM.offers[selectedOffer].Id
                    viewAllVC.viewAllVM.titleLabel = homeVM.offers[selectedOffer].Name
                    selectedOffer = 0
                    break
                case 101:
                    viewAllVC.viewAllVM.arrayOfItems = []
                    viewAllVC.viewAllVM.type = 3
                    viewAllVC.offerId = homeVM.brands[selectedOffer].ID
                    viewAllVC.viewAllVM.titleLabel = homeVM.brands[selectedOffer].Name
                    selectedOffer = 0
                    break
                default:
                    break
                }
                if senderIn != 100 && senderIn != 101 {
                    viewAllVC.viewAllVM.titleLabel = homeVM.titles[senderIn]
                }
            }
        }
        else if segue.identifier == String(describing:SearchVC.self) {
            let search: SearchVC =  segue.destination as! SearchVC
            search.searchVM.isFromItemList = true
        }
        else{
            if let _ = segue.destination as? SelectLocationVC{
              //  self.tabBarController?.tabBar.isHidden = true
            }
        }
    }
}
extension HomeVC {
    
    ///observe for viewModel
    func didGetCategories() {
        
        homeVM.homeVM_response.subscribe(onNext: {[weak self]success in
            DispatchQueue.main.async() {
                self?.loading = false
                self?.mainTableView.isHidden = false
                if success {
                    if (self?.refreshControl.isRefreshing)! {
                    self?.refreshControl.endRefreshing()
                    }
                    Helper.hidePI()
                }
                if (self?.homeVM.store.Fav)!{
                    self?.favButtonOutlet.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
                }
                else{
                    self?.favButtonOutlet.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
                }
              
                
                DispatchQueue.main.async {
                    self?.mainTableView.reloadData()
                }
                self?.setStore()
                if self?.homeVM.store.Id.length == 0 {
                    self?.dismiss(animated: true, completion: {
                        Helper.showAlert(message: StringConstants.NoDataAvailable(), head: StringConstants.Error(), type: 1)
                    })
                }
            }
            
        }).disposed(by: disposeBag)
        Utility.UtilityResponse.subscribe(onNext: { [weak self]success in
            if success == false {
                self?.homeVM.store = Utility.getStore()
                self?.loading = true
                self?.mainTableView.reloadData()
                self?.homeVM.getData()
                self?.setLocation()
            }
        }).disposed(by: disposeBag)
        
        Utility.UtilityUpdate.subscribe(onNext: { [weak self]success in
            if success == Utility.TypeServe.ChangeStore {
                self?.homeVM.store = Utility.getStore()
                self?.loading = true
                Helper.showPI(string: "")
                DispatchQueue.main.async {
                   self?.mainTableView.reloadData()
                }
                
               
                self?.homeVM.getData()
            }
        }).disposed(by: disposeBag)
        
        CommonAlertView.AlertPopupResponse.subscribe(onNext: { data in
            if data == CommonAlertView.ResponceType.Address {
                self.performSegue(withIdentifier:UIConstants.SegueIds.HomeToLocation, sender: self)
            }
        }, onError: {error in
        }).disposed(by: disposeBag)
        
    }
    
    /// updates the location
    func setLocation() {
        let locationStored = Utility.getAddress()
        if addressLabel != nil {
         addressLabel.text = locationStored.FullText
        }
    }
}
