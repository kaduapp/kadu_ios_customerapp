//
//  HoshiExtension.swift
//  DelivX
//
//  Created by 3Embed on 09/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension UITextField {
    func Semantic() {
        if self.textAlignment != .center {
            if Utility.getLanguage().Code == "ar" {
                self.textAlignment = .right
            }else{
                self.textAlignment = .left
            }
        }
    }
}

extension UITextView {
    func Semantic() {
        if self.textAlignment != .center {
            if Utility.getLanguage().Code == "ar" {
                self.textAlignment = .right
            }else{
                self.textAlignment = .left
            }
        }
    }
}
