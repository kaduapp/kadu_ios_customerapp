//
//  CheckoutVM.swift
//  UFly
//
//  Created by 3Embed on 25/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


struct CheckoutTime {
    var Head = ""
    var Body = ""
}
struct LaundryCellType {
    var Head = ""
    var Body = ""
}
struct selectCellType {
    var ImageName = ""
    var Title = ""
}
 
class CheckoutVM: NSObject {
    let rx_ReloadTableView = PublishSubject<Bool>()
    let rx_showAlert = PublishSubject<Bool>()
    let rx_updateAmountLabel = PublishSubject<Bool>()

    var dataForAddressSelection:selectCellType{
        var data1 = selectCellType()
        data1.Title = StringConstants.SelectAddress()
         data1.ImageName = StringConstants.SelectAddressIconTitle()
        return data1
    }
    var dataForPaymentSelection:selectCellType{
        var data1 = selectCellType()
        data1.Title = StringConstants.SelectPayment()
        data1.ImageName = StringConstants.SelectPaymentIconTitle()
        return data1
    }
    var dataForLaundryTypeNormal:LaundryCellType {
        var data1 = LaundryCellType()
        data1.Head =  StringConstants.Normal()
        data1.Body = StringConstants.NormalExtend()
         return data1
    }
    var dataForLaundryTypeExpress:LaundryCellType {
        var data2 = LaundryCellType()
        data2.Head = StringConstants.Express()
        data2.Body = StringConstants.ExpressExtend()
        return data2
    }
    
    var dataScheduleForDelivery:[CheckoutTime] {
        var data1 = CheckoutTime()
        data1.Head =  StringConstants.Now()
        data1.Body = StringConstants.NowExtend()
        var data2 = CheckoutTime()
        data2.Head = StringConstants.Later()
        data2.Body = StringConstants.LaterExtend()
        return [data1,data2]
    }
    
    var dataScheduleForPickUp:[CheckoutTime] {
        var data1 = CheckoutTime()
        data1.Head =  StringConstants.Now()
        data1.Body = StringConstants.NowExtendForPickUp()
        var data2 = CheckoutTime()
        data2.Head = StringConstants.Later()
        data2.Body = StringConstants.LaterExtendForPickup()
        return [data1,data2]
    }
    enum ResponseType:Int {
        case AddressSelected = 0
        case AddressWrong    = 1
        case OrderPlaced     = 2
        case Promocode       = 4
        case Checkout        = 5
        case drivertip       = 6
    }
    enum LaundryType:Int {
        case normal  = 1
        case express = 2
    }
//    enum PaymentType:Int {
//        case Default = 0
//        case Card    = 1
//        case Cash    = 2
//        case Wallet  = 3
//        case iDeal   = 4
//    }
    
    var paymentHandler = PaymentHandler()
    var taxArray:[Tax] = []
    var tipArray:[Tip] = []
    let disposeBag = DisposeBag()
    let checkoutVM_response = PublishSubject<ResponseType>()
    var selectedAddress:Address? = nil
    var pickupAddress:Address? = nil
    var deliveryAddress:Address? = nil
    var selectedCard:Card? = nil
     var cartID:String? = nil
    var selectedLaundryType:Int? = nil // for Normal Or Express delivery
    var DeliveryDict = Dictionary<String, Any>()
    var deliveryDate:Date = Date().addingTimeInterval(TimeInterval(Utility.getAppConfig().LaterBookingBuffer))
    var deliveryTime:Date = Date().addingTimeInterval(TimeInterval(Utility.getAppConfig().LaterBookingBuffer))
    var booking = 1   //1 for now and 2 for later
    var pickup = 1    // 2 for pickup and 1 for delivery
    var laundrypickordelivery = 1
    var collectionSlotSelection:Int? = nil
    var deliverySlotSelection:Int? = nil
    var selectedSlotDate:String? = nil
    var selectedSlotTime:String? = nil
    
    var selectedDeliverySlotDate:String? = nil
    var selectedDeliverySlotTime:String? = nil
    
    var pickupSlotID:String? = nil
    var pickupDateID:String? = nil
    var deliverySlotID:String? = nil
    var deliveryDateID:String? = nil
     var storeCategoryID:String? = nil
    //0 no payment 1 for Card and 2 for cash 3 far Wallet 4 for iDeal

    var isPaymentMethodSelected = false
    var Amount:Float = 0
    var discount:Float = 0
    var deliveryFee:Float = 0
    var expressFee:Float = 0
    var pickupCharge:Float = 0
    var deliveryCharge:Float = 0
    var promocode:String = ""
    var txn:[String:Any] = [:]
    var src_Id:String = "0"
    var convenienceFee:Int = 0
     var convenienceFeeType = false
    var orderId = ""
    var allStoreIds = ""
    var isSameAsPickUpAddress = false
    var packageEstimateValue = ""

    
    
    func checkout() {
        CheckoutAPICalls.checkout().subscribe(onNext: { (success) in
            if success == false {
                self.checkoutVM_response.onNext(CheckoutVM.ResponseType.Checkout)
            }
        }, onError: { (Error) in
            
        }, onCompleted: {
            
        }).disposed(by: self.disposeBag)
    }
    func getTips(){
//        tipArray.append(Tip.init(data: [:]))
//             tipArray.append(Tip.init(data: [:]))
//             tipArray.append(Tip.init(data: [:]))
//             tipArray[0].tipValue = 3
//             tipArray[1].tipValue = 5
//             tipArray[2].tipValue = 7
        CheckoutAPICalls.checkTip().subscribe(onNext:{ [weak self]success in
            self?.tipArray = success
            self!.checkoutVM_response.onNext(ResponseType.drivertip)

        }).disposed(by: self.disposeBag)
        
    }
    
    /// subscription to the Fare API
    func getFare() {
        // if let lat = selectedAddress?.Latitude , long
        CheckoutAPICalls.checkDeliveryAvaiability(item: selectedAddress!,status: 1,orderType: self.pickup).subscribe(onNext: {result in
            let array =  result.1.components(separatedBy: "+")
            self.convenienceFee = Int(array[0].doubleValue ?? 0)
            var type = 0
            if  array.count > 1
            {
                type = Int(array[1].doubleValue ?? 0)
            }
            self.convenienceFeeType = Bool(truncating: type as NSNumber)
            if result.0 {
                self.checkoutVM_response.onNext(ResponseType.AddressSelected)
            }else{
                self.checkoutVM_response.onNext(ResponseType.AddressWrong)
            }
        }, onError: {error in
        }).disposed(by: disposeBag)
    }
    /// subscription to the Fare API
    func getFareLaundry() {
        CheckoutAPICalls.checkDeliveryAvaiability(item: selectedAddress!,status: 1,orderType: self.pickup).subscribe(onNext: {result in
            self.convenienceFee = Int(result.1.doubleValue ?? 0)
            if result.0 {
                           self.checkoutVM_response.onNext(ResponseType.AddressSelected)
                       }else{
                           self.checkoutVM_response.onNext(ResponseType.AddressWrong)
                       }
        }, onError: {error in
        }).disposed(by: disposeBag)
    }
    
    /// subscription to the place order API

    func placeOrder(checkoutVM:CheckoutVM) {
        Amount = 0
        if pickup == 2 {
            Amount = AppConstants.TotalDeliveryPrice
        }
        Amount = Amount + AppConstants.TotalCartPriceWithTax - discount
        Helper.showPI(string: StringConstants.PlaceOrder())
        CheckoutAPICalls.placeOrder(checkout : checkoutVM).subscribe(onNext: {result in
            
            if result.0{
                self.orderId = result.1
                self.checkoutVM_response.onNext(ResponseType.OrderPlaced)
                
            }
        }, onError: {error in
        }).disposed(by: disposeBag)
    }
    
    ///having subscription to the CardAPI
    func applyPromocode(code:String) {
        
        if pickup == 2 {
            Amount = AppConstants.TotalDeliveryPrice
        }
        
        Amount = Amount + AppConstants.TotalCartPriceWithTax - discount
        CheckoutAPICalls.applyPromocode(amount: AppConstants.TotalCartPriceWithTax, promocode: code, payment: self.paymentHandler.paymentType, total: Amount,isWallet:paymentHandler.isPayByWallet,storeIds:allStoreIds).subscribe(onNext: {result in
            self.discount = result
            self.promocode = code
            self.checkoutVM_response.onNext(ResponseType.Promocode)
        }, onError: {error in
        }).disposed(by: disposeBag)
    }
    
    
    func fetchSlotsFor(_ LaundryType:Int) {
        let rxAddCartAPI = ConfirmOrderAPICalls()
        if !rxAddCartAPI.rx_addResponse.hasObservers {
            rxAddCartAPI.rx_addResponse.subscribe(onNext: { (response) in
                if let responseData = response.data["data"] as? [String:Any]{
                    do {
                        if let deliveryfee = responseData["deliveryFee"] as? [String:Any] ,let Amount = deliveryfee["estimateAmount"] as? Double
                       {
                        
                      // deliveryPriceFromLaundromatToCustomer -- deliverycharge
                        //deliveryPriceFromCustomerToLaundromat -- pickupcharge
                        self.DeliveryDict.removeAll()
                        self.DeliveryDict = deliveryfee
                        self.deliveryFee =  Float(Amount)
                        if let expressFee = deliveryfee["expressDeliveryCharge"] as? Double ,let deliverycharge = deliveryfee["deliveryPriceFromLaundromatToCustomer"] as? Double,let pickupcharge = deliveryfee["deliveryPriceFromCustomerToLaundromat"] as? Double
                        {
                            self.expressFee = Float(expressFee)
                            self.pickupCharge = Float(pickupcharge)
                            self.deliveryCharge = Float(deliverycharge)
                             AppConstants.TotalDeliveryPrice = Float(Amount - expressFee)
                            // AppConstants.TotalDeliveryPrice = Float(Amount)
                        }else
                        {
                             AppConstants.TotalDeliveryPrice = Float(Amount)
                        }
                        }
                    } catch {
                        print("Error in Converting JSON to Data in rxCategoriesAndAttributesAPICall")
                    }
                }
                self.rx_updateAmountLabel.onNext(true)
            }, onError: { (error) in
                print("rx_addResponse \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_addResponse completed")
            }, onDisposed: {
                print("rx_addResponse disposed")
            }).disposed(by: disposeBag)
        }
        //,let storeId = storeCategoryID ,let cartID = cartID // Utility.getStore()
         if let picklat = self.pickupAddress?.Latitude,let picklong = self.pickupAddress?.Longitude,let droplat = self.deliveryAddress?.Latitude,let droplong = self.deliveryAddress?.Longitude  {
            let params = ["pickupLatitude": picklat ,
                          "pickupLongitude": picklong,
                          "dropLatitude": droplat,
                          "dropLongitude": droplong,
                          "laundryType": LaundryType,
                          "storeCategoryId":Utility.getSelectedSuperStores().Id,
                          "storeType": Utility.getSelectedSuperStores().type.rawValue] as [String : Any]
            rxAddCartAPI.getEstimate(params)
        }
    }
    
    
    func Placeorder(_ LaundryType:Int) {
        Helper.showPI(string: StringConstants.PlaceOrder())
        let rxAddCartAPI = ConfirmOrderAPICalls()
        if !rxAddCartAPI.rx_addResponse.hasObservers {
            rxAddCartAPI.rx_addResponse.subscribe(onNext: { (response) in
                if let responseData = response.data["data"] as? [String:Any]{
                    do {
                        print(responseData)
                        
                       if  response.httpStatusCode == 200
                        {
                              Helper.showPISuccess()
                            self.rx_showAlert.onNext(true)
                        }
                        
                    } catch {
                        print("Error in Converting JSON to Data in rxCategoriesAndAttributesAPICall\(error.localizedDescription)")
                    }
                }
                self.rx_showAlert.onNext(false)
            }, onError: { (error) in
                print("rx_addResponse \(error.localizedDescription)")
                Helper.hidePI()
            }, onCompleted: {
                print("rx_addResponse completed")
               
            }, onDisposed: {
                print("rx_addResponse disposed")
            }).disposed(by: disposeBag)
        }
        
        if let picklat = self.pickupAddress?.Latitude,let picklong = self.pickupAddress?.Longitude,let droplat = self.deliveryAddress?.Latitude,let droplong = self.deliveryAddress?.Longitude, let pickupslotID = self.pickupSlotID ,let deliveryslotID = self.deliverySlotID, let cartID = self.cartID {
            let params = [
                "paymentType": self.paymentHandler.paymentType ,
                "cartId": cartID,
                "pickupLatitude": picklat ,
                "pickupLongitude": picklong,
                "dropLatitude": droplat,
                "dropLongitude": droplong,
                "pickupSlotId":pickupslotID,
                "dropSlotId":deliveryslotID,
                "bookingDate":Utility.DeviceTime,
                "dueDatetime":Utility.DeviceTime,
                "serviceType":3,
                "bookingType":1,
                "deviceTime":Utility.DeviceTime,
                "storeCategoryId":Utility.getSelectedSuperStores().Id,
                "storeType":Utility.getSelectedSuperStores().type.rawValue,
                "extraNote":""
                ] as [String : Any]
           rxAddCartAPI.Placeorder(params)
        }

    }
}
