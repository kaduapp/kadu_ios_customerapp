//
//  Cart.swift
//  UFly
//
//  Created by 3Embed on 20/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

class CartItem: NSObject {
    var QTY                         =   0
    var ParentProductId             =   ""
    var ChildProductId              =   ""
    var StoreId                     =   ""
    var ItemName                    =   ""
    var UpcNumber                   =   ""
    var Sku                         =   ""
    var ItemImageURL                =   ""
    var UnitPrice:Float             =   0
    var StoreName                   =   ""
    var StoreImage                  =   ""
    var CartId                      =   ""
    var StoreLat:Double             =   0
    var StoreLong:Double            =   0
    var StoreAddress                =   ""
    var UnitId                      =   ""
    var UnitName                    =   ""
    var Discount:Float              =   0
    var FinalPrice:Float            =   0
    var OfferId                     =   ""
    var ExclusiveTaxes:[Any]        =   []
    var Addons:[AddOn]              =   []
    var AddonGroups:[AddOnGroup]    =   []
    var AddOnAvailable              =   false
    var PackId                      =   ""
    var ItemCartAddonGroups:[AddOnGroup]    =   []
    var selectedAddons:[[String:Any]] =   []
    var DictionaryOfCart            =   [String:Any]()
    var addedToCartOn = 0
    
    init(data:[String:Any]) {
        super.init()
        
        if let titleTemp = data["addedToCartOn"] as? Int{
        addedToCartOn = titleTemp
        }
        
        if let titleTemp = data[APIRequestParams.ParentId] as? String{
            ParentProductId      = titleTemp
        }
        if let titleTemp = data[APIRequestParams.ChildId] as? String{
            ChildProductId     = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CartId] as? String{
            CartId     = titleTemp
        }
        if let titleTemp = data[APIRequestParams.QTY] as? Int{
            QTY     = titleTemp
        }
        if let titleTemp = data[APIRequestParams.QTY] as? String{
            QTY     = Int(titleTemp)!
        }
        if let titleTemp = data[APIRequestParams.SKUNumber] as? String{
            Sku     = titleTemp
        }
        if let titleTemp = data[APIRequestParams.UpcNumber] as? String{
            UpcNumber    = titleTemp
        }
        if let titleTemp = data[APIRequestParams.StoreId] as? String{
            StoreId           = titleTemp
        }
        if let titleTemp = data[APIRequestParams.StoreName] as? String{
            StoreName         = titleTemp
        }
        if let titleTemp = data[APIRequestParams.StoreLogo] as? String{
            StoreImage        = titleTemp
        }
        if let titleTemp = data[APIRequestParams.ItemName] as? String{
            ItemName      = titleTemp
        }
        if let titleTemp = data[APIRequestParams.Itemimage] as? String{
            ItemImageURL      = titleTemp
        }
        if let titleTemp = data[APIRequestParams.ItemPrice] as? Float{
            UnitPrice           = titleTemp
        }
        if let titleTemp = data[APIRequestParams.ItemPrice] as? Double{
            UnitPrice           = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.UnitId] as? String{
            UnitId           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.UnitName] as? String{
            UnitName           = titleTemp
        }
        if let titleTemp = data[APIRequestParams.StoreLat] as? Double{
            StoreLat      = titleTemp
        }
        if let titleTemp = data[APIRequestParams.StoreLong] as? Double{
            StoreLong           = titleTemp
        }
        if let titleTemp = data[APIRequestParams.StoreLat] as? String{
            StoreLat      = Double(titleTemp)!
        }
        if let titleTemp = data[APIRequestParams.StoreLong] as? String{
            StoreLong           = Double(titleTemp)!
        }
        if let titleTemp = data[APIResponceParams.CartStoreAddress] as? String{
            StoreAddress           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? Float{
            Discount           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.AppliedDiscount] as? Double{
            Discount           = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? Float{
            FinalPrice           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.FinalPrice] as? Double{
            FinalPrice           = Float(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.OfferId] as? String{
            OfferId           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Taxes] as? [Any] {
            ExclusiveTaxes           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.PackId] as? String {
            PackId           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.PackId] as? Int {
            PackId           = String(titleTemp)
        }
        if let titleTemp = data[APIResponceParams.AddOnsAvailable] as? Int {
            if titleTemp == 1 {
                AddOnAvailable = true
            }else{
                AddOnAvailable  = false
            }
        }
        Addons.removeAll()
        selectedAddons.removeAll()
        if let titleTemp = data[APIResponceParams.SelectedAddons] as? [[String:Any]] {
            for temp in titleTemp{
                let newData = AddOn.init(data: temp)
                Addons.append(newData)
                selectedAddons.append(temp)
            }
        }
        
        if let titleTemp = data[APIResponceParams.AddOns] as? [[String:Any]] {
            for temp in titleTemp{
                ItemCartAddonGroups.append(AddOnGroup.init(data: temp))
            }
        }
        
        
        if let titleTemp = data["allAddOnsData"] as? [[String:Any]] {
            AddonGroups.removeAll()
            for temp in titleTemp{
                let newData = AddOnGroup.init(data: temp,selected:[])
                AddonGroups.append(newData)
            }
        }
     
        setDict()
    }
    
    init(item:Item) {
        super.init()
        ChildProductId              = item.Id
        ParentProductId             = item.ParentId
        QTY                         = item.CartQty
        StoreId                     = item.StoreId
        StoreName                   = item.StoreName
        StoreImage                  = item.StoreLogo
        ItemName                    = item.Name
        UpcNumber                   = item.UPC
        Sku                         = item.SKU
        UnitId                      = item.UnitId
        if item.Images.count > 0 {
            ItemImageURL            = item.Images[0]
        }
        StoreAddress                = item.StoreAddress
        UnitPrice                   = item.Price
        CartId                      = item.CartId
        StoreLat                    = item.StoreLat
        StoreLong                   = item.StoreLong
        UnitId                      = item.UnitId
        UnitName                    = item.UnitName
        FinalPrice                  = item.FinalPrice
        Discount                    = item.Discount
        OfferId                     = item.OfferId
        AddonGroups                 = item.AddOnGroups
        selectedAddons.removeAll()
        Addons.removeAll()
        if item.AddOnGroups.count > 0 {
            for each in item.AddOnGroups {
                if each.AddOns.count > 0 {
                    for eachAddon in each.AddOns {
                        if eachAddon.Selected {
                            selectedAddons.append(eachAddon.Dictionary)
                            Addons.append(eachAddon)
                        }
                    }
                }
            }
        }
        AddOnAvailable              = item.AddOnAvailable
        PackId                      = item.PackId
        self.setDict()
    }
    
    func setDict() {
        DictionaryOfCart = [
            APIRequestParams.ChildId            :   ChildProductId,
            APIRequestParams.ParentId           :   ParentProductId,
            APIRequestParams.QTY                :   QTY,
            APIRequestParams.SKUNumber          :   Sku,
            APIRequestParams.UpcNumber          :   UpcNumber,
            APIRequestParams.StoreId            :   StoreId,
            APIRequestParams.StoreName          :   StoreName,
            APIRequestParams.StoreLogo          :   StoreImage,
            APIRequestParams.ItemName           :   ItemName,
            APIRequestParams.Itemimage          :   ItemImageURL,
            APIRequestParams.ItemPrice          :   UnitPrice,
            APIRequestParams.StoreLat           :   StoreLat,
            APIRequestParams.StoreLong          :   StoreLong,
            APIRequestParams.Currency           :   Utility.getCurrency().0,
            APIRequestParams.CurrencySymbol     :   Utility.getCurrency().1,
            APIResponceParams.CartStoreAddress  :   StoreAddress,
            APIRequestParams.City               :   Utility.getAddress().City,
            APIResponceParams.SelectedAddons    :   selectedAddons,
            APIResponceParams.PackId            :   PackId,
            APIRequestParams.CustomerName       :   Utility.getProfileData().FirstName
        ]
        if DictionaryOfCart[APIRequestParams.CustomerName] as! String == StringConstants.Login() {
            DictionaryOfCart.updateValue("", forKey: APIRequestParams.CustomerName)
        }
        if CartId.length > 0 {
            DictionaryOfCart.updateValue(CartId, forKey: APIRequestParams.CartId)
        }
        if UnitId.length > 0 {
            DictionaryOfCart.updateValue(UnitId, forKey: APIResponceParams.UnitId)
        }
        if UnitName.length > 0 {
            DictionaryOfCart.updateValue(UnitName, forKey: APIResponceParams.UnitName)
        }
        if AddOnAvailable {
            DictionaryOfCart.updateValue(1, forKey: APIResponceParams.AddOnsAvailable)
        }else{
            DictionaryOfCart.updateValue(0, forKey: APIResponceParams.AddOnsAvailable)
        }
        DictionaryOfCart.updateValue(Discount, forKey: APIResponceParams.AppliedDiscount)
        DictionaryOfCart.updateValue(FinalPrice, forKey: APIResponceParams.FinalPrice)
        DictionaryOfCart.updateValue(OfferId, forKey: APIResponceParams.OfferId)
        DictionaryOfCart.updateValue(ExclusiveTaxes, forKey: APIResponceParams.Taxes)
    }
}
