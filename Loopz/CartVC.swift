//
//  CartVC.swift
//  UFly
//
//  Created by 3 Embed on 20/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CartVC: UIViewController {
    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    @IBOutlet weak var dummyView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var checkoutBarView: CartCheckoutBarView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var checkoutTopConstarint: NSLayoutConstraint!
    
    ///put in checkoutBarView view class
    @IBOutlet weak var deliveryBtn: UIButton!
    @IBOutlet weak var pickupBtn: UIButton!
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var pickupView: UIView!
    @IBOutlet weak var deliverType: UILabel!
    @IBOutlet weak var selectHow: UILabel!
    @IBOutlet weak var bottomConstraintsCollectionView: NSLayoutConstraint!
    var activeField : UITextView!
    //@IBOutlet weak var navWidth: NSLayoutConstraint!
    
    let cartVM = CartVM()
    //    let transition = PopAnimator()
    var selected:UICollectionViewCell? = nil
    var emptyView = EmptyView().shared
    var load = false
    var navView = NavigationView().shared
    var isCartFromProfile = false
     var checkoutVM = CheckoutVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEmptyScreen()
        initialSetup()
        didGetResponse()
        self.checkoutTopConstarint.constant = 0
        if (self.navigationController?.viewControllers.count)! <= 1 && !Changebles.isGrocerOnly {
            leftBarButton()
        }
        if self.navigationController != nil && !Changebles.isGrocerOnly {
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.Cart())
        }
        else{
          if (self.navigationController?.viewControllers.count)! > 2 && Changebles.isGrocerOnly {
                backBarButton()
            }
            
            topTitleLabel.text = StringConstants.Cart()
            Fonts.setPrimaryBold(topTitleLabel)
            topTitleLabel.textColor = Colors.PrimaryText
        }
        
        
        
        
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        removeObserver()
    }
    
    @IBAction func backACT(_ sender: UIBarButtonItem){
        
        if Changebles.isGrocerOnly{
            self.navigationController?.popViewController(animated: true)
        }
        else{
        Utility.saveStore(location: [:])
        if isCartFromProfile{
            self.navigationController?.dismiss(animated: true, completion:nil)
        } else {
            self.tabBarController?.dismiss(animated: true, completion: nil)
         }
        }
    }
    
    //Setup Left bar button
    func leftBarButton() {
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "CloseIcon"), style: .plain, target: self, action: #selector(backACT(_:)))
        item.imageInsets = UIEdgeInsets.init(top: 0, left: -4, bottom: 0, right: -4)
        self.navigationItem.setLeftBarButtonItems([item], animated: true)
    }
    
    func backBarButton() {
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "BackIcon"), style: .plain, target: self, action: #selector(backACT(_:)))
        item.imageInsets = UIEdgeInsets.init(top: 0, left: -4, bottom: 0, right: -4)
        self.navigationItem.setLeftBarButtonItems([item], animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        updateCheckOutBar()
        self.showCheckoutView()
        self.setBanner()
        if load == false {
            Helper.showPI(string: "")
        }
        self.cartVM.getCart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    
    @IBAction func pickupBtnAction(_ sender: Any) {
        if self.checkIsStoreOpen(){
        cartVM.orderType = 2
        cartVM.getFare()
        }
    }
    
    @IBAction func deliveryTypeAction(_ sender: Any) {
          if self.checkIsStoreOpen(){
        cartVM.orderType = 1
        cartVM.getFare()
        }
    }
    
    func checkIsStoreOpen() -> Bool{
        for cart in self.cartVM.arrayOfCart{
            if !cart.StoreIsOpen{
                Helper.showAlert(message: "\(cart.StoreName) is \(StringConstants.Closed())", head: StringConstants.Alert(), type:1)
                return false
            }
        }
        return true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier ==  UIConstants.SegueIds.CartToItemDetail {
            let item = sender as! Item
            if let nav:UINavigationController = segue.destination as? UINavigationController {
                if let itemDetailVC: ItemDetailVC = nav.viewControllers.first as? ItemDetailVC{
                    itemDetailVC.itemDetailVM.item = item
                    //                self.tabBarController?.tabBar.isHidden = true
                    //                itemDetailVC.transitioningDelegate = self
                }
            }
        }else if segue.identifier ==  UIConstants.SegueIds.CartToCheckout {
            if let itemDetailVC: ConfirmOrderVC = segue.destination as? ConfirmOrderVC {
                itemDetailVC.checkoutVM = self.checkoutVM
                itemDetailVC.checkoutVM.allStoreIds = cartVM.storeIds
                itemDetailVC.checkoutVM.pickup = cartVM.orderType
                itemDetailVC.checkoutVM.taxArray = cartVM.arrayOfTax
            }
        }
    }
    
}

extension CartVC {
    /// update for viewModel
    func didGetResponse(){
        CartVM.cartVM_response.subscribe(onNext: {[weak self]success in
            if self == nil {
                return
            }
            Helper.hidePI()
            if success.0 == CartVM.ResponceType.ReloadList{
                self?.load = true
                var savedOne = 0
                var cartOne = 0
                let cart = AppConstants.Cart
                self?.cartVM.arrayOfCart = cart
                if cart.count > 0 {
                    for i in 0...(self?.cartVM.arrayOfCart.count)! - 1 {
                        savedOne = savedOne + (self?.cartVM.arrayOfCart[i].Products.count)!
                        cartOne = cartOne + cart[i].Products.count
                    }
                }
                self?.showCheckoutView()
                self?.updateCheckOutBar()
                self?.cartVM.arrayOfTax = success.2
                
                self?.mainCollectionView.reloadData()
            }else if success.0 == CartVM.ResponceType.GetFare {
                if Helper.isLoggedIn(isFromHist: false) {
                    self?.performSegue(withIdentifier: UIConstants.SegueIds.CartToCheckout, sender: nil)
                }
            }
            }, onError: {error in
                print(error)
                
        }).disposed(by: self.cartVM.disposeBag)
    }
}





extension CartVC : UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeField = textView
        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
      
        if textView.text == StringConstants.addExtraNotes() {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        activeField = nil
        if textView.text.isEmpty {
            textView.text = StringConstants.addExtraNotes()
            textView.textColor = UIColor.lightGray
         AppConstants.extraNotes.removeValue(forKey: self.cartVM.arrayOfCart[textView.tag].StoreId)
        }
        else{
        AppConstants.extraNotes.updateValue(textView.text, forKey: self.cartVM.arrayOfCart[textView.tag].StoreId)
        }
    }
    
}
