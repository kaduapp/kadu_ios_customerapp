//
//  SubCategory.swift
//  UFly
//
//  Created by 3Embed on 12/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SubCategory: NSObject {

    var Id                      = "0"
    var Name                    = ""
    var Desc                    = ""
    var Image                   = ""
    var Products:[Item]         = []
    var StoreIn                 = Store.init(data: [:])
    var SubSubCat:[SubSubCategory] = []
    
    init(data: [String:Any], store:Store) {
        StoreIn = store
        if let titleTemp = data[APIResponceParams.SubCategoryName] as? String{
            Name                         = titleTemp.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if let titleTemp = data[APIResponceParams.SubCategoryId] as? String{
            Id             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SubCategoryDesc] as? String{
            Desc                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CategoryImage] as? String{
            Image             = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.Products] as? [Any]{
            Products.removeAll()
            for item in titleTemp {
                Products.append(Item.init(data: item as! [String:Any], store: StoreIn))
            }
        }
    }
}
