//
//  ImageCell.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var imageInCell: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        self.mainScrollView.addGestureRecognizer(doubleTapGest)
       // self.initialSetup()
    }
    
    
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if mainScrollView.zoomScale == 1 {
            mainScrollView.zoom(to: zoomRectForScale(scale: mainScrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            mainScrollView.setZoomScale(1, animated: true)
        }
    }
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageInCell.frame.size.height / scale
        zoomRect.size.width  = imageInCell.frame.size.width  / scale
        let newCenter = imageInCell.convert(center, from: mainScrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return self.imageInCell
//    }
    /*
    func initialSetup() {
        mainScrollView.maximumZoomScale = 5
        mainScrollView.minimumZoomScale = 0.5
    }*/
}
