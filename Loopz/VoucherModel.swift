//
//  VoucherModel.swift
//  DelivX
//
//  Created by 3Embed on 11/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VoucherVM: NSObject {
    
    var voucherCode:String = ""
    let VoucherVM_response = PublishSubject<(Bool,String)>()
    let VoucherVM_responseWallet = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    func redeemVoucher() {
        WalletAPICalls.redeemVoucher(voucher: voucherCode).subscribe(onNext: {[weak self]success in
                self?.VoucherVM_response.onNext(success)
            }, onError: {error in
                print(error)
        }).disposed(by: self.disposeBag)
    }
}
