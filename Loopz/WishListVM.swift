//
//  WishListVM.swift
//  UFly
//
//  Created by 3Embed on 18/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class WishListVM: NSObject {
    enum respType :Int{
        case GetFav = 1
        case GetItemWishList = 2
        case RemoveItemWishList = 3
        case UpdateWishList = 4
        case Favourite = 5
       
    }
    let disposeBag = DisposeBag()
    let pickerObj:ImagePickerModel? = ImagePickerModel.sharedImagePicker
    let wishListVMResponse = PublishSubject<(Int,respType)>()
    var arrayOfItems = [Item]()
    var wishList = PopupModel.init(data: [:])
    var uploadImageModel  = UploadImageModel.shared
    
    func getFav() {
        FavouriteAPICalls.favourite().subscribe(onNext: { data in
            self.arrayOfItems = data
            self.wishListVMResponse.onNext((-1 ,.GetFav))
        }, onError: {error in
        }).disposed(by: self.disposeBag)
    }

    func getItemWishList() {
        FavouriteAPICalls.getWishList(id: wishList.ID,pi: true).subscribe(onNext: { data in
            self.arrayOfItems = data
            self.wishListVMResponse.onNext((-2,.GetItemWishList))
        }, onError: {error in
        }).disposed(by: self.disposeBag)
    }
    
    func removeItemWishList(data:Int) {
        FavouriteAPICalls.removeItemFromWishLists(list: arrayOfItems[data], item: wishList).subscribe(onNext: { response in
            self.arrayOfItems.remove(at: data)
            self.wishListVMResponse.onNext((data,.RemoveItemWishList))
        }, onError: {error in
        }).disposed(by: self.disposeBag)
    }
    
    // Calling remove fav from list
    func favourite(data:Int){
        FavouriteAPICalls.favourite(item: arrayOfItems[data]).subscribe(onNext: { result in
            self.arrayOfItems.remove(at: data)
            self.wishListVMResponse.onNext((data,.Favourite))
        }, onError: { error in
            print(error)
        }).disposed(by: self.disposeBag)
    }
    
    func updateWishList() {
        var data = PopupModel.init(data: [:])
        data.Image = wishList.Image
        data.String = wishList.String
        FavouriteAPICalls.editWishList(data: data, item: wishList).subscribe(onNext: { response in
            if response == true {
                self.wishList.String = data.String
                self.wishList.Image = data.Image
                self.wishListVMResponse.onNext((-3,.UpdateWishList))
            }
        }, onError: {error in
        }).disposed(by: self.disposeBag)
    }
    
    
    func setSubscribe() {
        FavouriteAPICalls.favVMResponse.subscribe(onNext: { [weak self]data in
            if data.1 == 2 {
                self?.arrayOfItems.removeAll()
                self?.arrayOfItems.append(contentsOf: data.0)
            } else {
                if Utility.getStore().Id == data.0[0].StoreId {
                    if data.1 == 1 {
                        self?.arrayOfItems.append(data.0[0])
                        self?.wishListVMResponse.onNext((-1 ,.GetFav))
                    }else if data.1 == 0 {
                        for item in (self?.arrayOfItems)! {
                            if item.Id == data.0[0].Id {
                               self?.wishListVMResponse.onNext((-1 ,.GetFav))
                            }
                        }
                        self?.wishListVMResponse.onNext((-1 ,.GetFav))
                    }
                }
            }
        }).disposed(by: self.disposeBag)
    }
}
