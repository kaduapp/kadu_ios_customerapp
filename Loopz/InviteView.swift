//
//  InviteView.swift
//  UFly
//
//  Created by Rahul Sharma on 14/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class InviteView: UIView {
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var validTillLabel: UILabel!
    @IBOutlet weak var inviteBtnView: UIView!
    @IBOutlet weak var inviteBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     initialSetUp()
    }
    
    
    /// initial View setup
    func initialSetUp(){
        
        Fonts.setPrimaryBlack(headingLabel)
        Fonts.setPrimaryRegular(subTitle)
        Fonts.setPrimaryBold(title)
        Fonts.setPrimaryMedium(validTillLabel)
        Fonts.setPrimaryMedium(inviteBtn)
        
        headingLabel.textColor  =  Colors.HeadColor
        title.textColor  =  Colors.PrimaryText
        title.text = String(format: StringConstants.inviteCode(), Utility.AppName)
        subTitle.textColor  =  Colors.SeconderyText
        validTillLabel.textColor  =  Colors.AppBaseColor
    
        Helper.setButton(button: inviteBtn,view:inviteBtnView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        
        Helper.setButtonTitle(normal: StringConstants.InviteFriends(), highlighted: StringConstants.InviteFriends(), selected: StringConstants.InviteFriends(), button: inviteBtn)
        
              //\(StringConstants.YourRefCodeIs)
        headingLabel.text = StringConstants.Invite()
        
        //Helper.updateText(text: validTillLabel.text!, subText: Utility.getReferralCode(), validTillLabel, color: Colors.AppBaseColor))
        backgroundImage.clipsToBounds = true
        print( "Referred: \(Utility.getReferralCode())")
        
        
//        border.strokeColor =  Colors.AppBaseColor).cgColor
    }
    
    func updateReferralCode(){
        
       validTillLabel.text = Utility.getReferralCode()
        
    }

}
