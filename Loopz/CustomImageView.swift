//
//  CustomImageView.swift
//  DelivX
//
//  Created by 3EMBED on 21/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if RTL.shared.isRTL
        {
            self.transform =  CGAffineTransform(scaleX: -1.0,y: 1.0)

        }
    }
}
