//
//  ItemListingView.swift
//  UFly
//
//  Created by Rahul Sharma on 02/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ItemListingView: UIView {
    @IBOutlet weak var bottomBarView: UIView!
    @IBOutlet weak var viewCartBar: UIView!
    @IBOutlet weak var gotItBtn: UIButton!
    @IBOutlet weak var freeDeliveryAbove: UILabel!
    @IBOutlet weak var minimumOrder: UILabel!

    @IBOutlet weak var extraCharges: UILabel!
    @IBOutlet weak var viewCartBtn: UIButton!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bottomBarHight: NSLayoutConstraint!
    @IBOutlet weak var viewCartBarHight: NSLayoutConstraint!
    @IBOutlet weak var viewCartlabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var itemCount: UILabel!
    @IBOutlet weak var star1: UILabel!
    @IBOutlet weak var star2: UILabel!
    
    var headerView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    /// initial ViewSetup
    func initialSetup(){
       
        Fonts.setPrimaryBold(gotItBtn)
        Fonts.setPrimaryRegular(minimumOrder)
        Fonts.setPrimaryRegular(freeDeliveryAbove)
        
        freeDeliveryAbove.textColor =  Colors.SecondBaseColor
        minimumOrder.textColor =  Colors.SecondBaseColor
        star1.textColor =  Colors.gotItBtnColor
        star2.textColor =  Colors.gotItBtnColor
      
        viewCartBar.backgroundColor =  Colors.AppBaseColor
        viewCartlabel.text = StringConstants.ViewCart()
       
        gotItBtn.setTitle(StringConstants.GotIt(), for: .normal)
        menuBtn.setTitle(StringConstants.Menu(), for: .normal)
        
        Helper.setButtonTitle(normal: StringConstants.GotIt(), highlighted: StringConstants.GotIt(), selected: StringConstants.GotIt(), button:gotItBtn)
        Helper.setButtonTitle(normal: StringConstants.Menu(), highlighted: StringConstants.Menu(), selected: StringConstants.Menu(), button:menuBtn)
        
        menuBtn.titleLabel?.textColor =  Colors.Red
        Helper.setShadow(sender: menuBtn)
        menuBtn.layer.cornerRadius = CGFloat(Helper.setRadius(element: menuBtn))
    
        extraCharges.text = StringConstants.ExtraCharges()
        gotItBtn.setTitleColor(Colors.gotItBtnColor, for: .normal)
        
        //******************
        menuBtn.isHidden = true
    }
    
    /// adding constraints Header View
    func addHedderToStore(){
//        storeCollectionView.contentInset = UIEdgeInsetsMake(160, 0, 0, 0)
//                headerView = storeDetail
//                storeDetail.removeFromSuperview()
//                storeCollectionView.addSubview(headerView)
//        headerView.translatesAutoresizingMaskIntoConstraints = false
//        
//        storeCollectionView.addConstraint(NSLayoutConstraint(item: headerView, attribute: .top, relatedBy: .equal, toItem: storeCollectionView, attribute: .top, multiplier: 1, constant: -160))
//        
//        storeCollectionView.addConstraint(NSLayoutConstraint(item: headerView, attribute: .leading, relatedBy: .equal, toItem: storeCollectionView, attribute: .leading, multiplier: 1, constant: 0))
//        
//        storeCollectionView.addConstraint(NSLayoutConstraint(item: headerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 165))
//        
//        storeCollectionView.addConstraint(NSLayoutConstraint(item: headerView, attribute: .width, relatedBy: .equal, toItem: storeCollectionView, attribute: .width, multiplier: 1, constant: 0))
        
    }
    
    
    func updateBottomBar(data: Store){
        let storeCart = CartDBManager().getCartStore(data: data)
        minimumOrder.text = ""//Helper.setCurrency(displayAmount: data.MinimumOrder, price: data.MinimumOrder).string + "\(StringConstants.FixedApplicable)"
        if storeCart.TotalPrice >= data.FreeDelivery {
            freeDeliveryAbove.text = StringConstants.FreeDeliveryAvailable()
        }else{
            freeDeliveryAbove.text = Helper.df2so(Double( data.FreeDelivery - storeCart.TotalPrice)) + " \(StringConstants.FreeDelivery())"
        }
    }
    
    
  
    /// hides the bottom bar
    ///
    /// - Parameter sender: is of UIButton type
    func hideBottom(sender: UIButton!) {
        if sender == viewCartBtn {
            UIView.animate(withDuration: 0.50) {
                self.viewCartBarHight.constant = 0
                self.layoutIfNeeded()
            }
        }
        else {
            UIView.animate(withDuration: 0.50) {
                self.bottomBarHight.constant = 0
                self.layoutIfNeeded()
            }
        }
    }
   
    
    
    /// shows bottomcort bar if cartcount is greater than 0
    func showBottom() {
        if AppConstants.CartCount > 1 {
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Items()
        }else{
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Item()
        }
        amount.text = Helper.df2so(Double( AppConstants.TotalCartPrice))
        var length = 0
        if AppConstants.CartCount > 0 {
            length = 50
        }
        UIView.animate(withDuration: 0.50) {
            self.viewCartBarHight.constant = CGFloat(length)
//            self.layoutIfNeeded()
        }
    }
}
