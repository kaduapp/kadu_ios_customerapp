//
//  AllWishListVC.swift
//  DelivX
//
//  Created by 3Embed on 04/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//ADD WISH LIST

import UIKit

class AllWishListVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    var navView = NavigationView().shared

    var emptyView = EmptyView().shared
    var allWishListVM = AllWishListVM()
    var addSelected = false
    let createNewListVM = CreateNewListVM()
    @IBOutlet weak var addtoWishListView: UIView!
    @IBOutlet weak var addtoWishListButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.WishList())
        }
        setEmptyScreen()
        didGetData()
        setObserver()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == UIConstants.SegueIds.AllWishListToWishListDetails {
            if let otpVC:WishListVC = segue.destination as? WishListVC {
                otpVC.wishListVM.wishList = allWishListVM.selected
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        Helper.nonTransparentNavigation(controller: self.navigationController!)
//        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // Helper.removeShadowToNavigationBar(controller: self.navigationController!)
    }
    
    @IBAction func addWishList(_ sender: UIButton) {
        addSelected = true
        mainTableView.backgroundView = nil
        mainTableView.beginUpdates()
        mainTableView.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: .top)
        mainTableView.endUpdates()
    }
    
    @IBAction func addAction(_ sender: Any) {
        if mainTableView.numberOfRows(inSection: 0) > 0 {
            let cell = mainTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! CreateNewListCell
            guard cell.textField.text?.length != 0 else {
                Helper.showAlert(message: StringConstants.EnterListName(), head: StringConstants.Error(), type: 1)
                return
            }
            createNewListVM.updateList(listName:cell.textField.text!)            
        }
    }
}
extension AllWishListVC {
    
    /// observer to ViewModel
    func didGetData(){
        Utility.UtilityResponse.subscribe(onNext: { (success) in
            if success == true {
                self.allWishListVM.arrayOfWishList = PopupMenu.getWishList()
                self.mainTableView.reloadData()
            }
        }).disposed(by: self.allWishListVM.disposeBag)
        
        CreateNewListVM.createNewListVM_response.subscribe(onNext: {[weak self]success in
            self?.addSelected = false
            self?.mainTableView.reloadData()
            }, onError: {error in
                print(error)
        }).disposed(by: self.allWishListVM.disposeBag)

    }
    
}

extension AllWishListVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableView {
            if self.navigationController != nil {
                if scrollView.contentOffset.y > 0 {
                    if (self.navigationController != nil) {
                        Helper.addShadowToNavigationBar(controller: self.navigationController!)
                    }
                }else{
                    if (self.navigationController != nil) {
                        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                    }
                }
            }
        }
    }
}


