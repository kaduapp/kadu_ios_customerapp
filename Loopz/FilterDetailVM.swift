//
//  FilterDetailVM.swift
//  DelivX
//
//  Created by 3Embed on 19/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class FilterDetailVM: NSObject {
    
    var chosenCat = ""
    var chosenSubCat = ""
    var searchFrom = 0
    var selectedSort = -1
    var selectedSection = -1
    var selectedCategory = ""
    var SubCatSelected = ""
    var SubSubCatSelected = ""
    var selectedRating = 0
    var selectedCosine = ""
    var selectedFoodType = ""
    var needle = ""
    var needleLanguage = ""
    var offer = ""
    var maxValue:Float = 0
    var minValue:Float = 0
    var selectedMaxValue:Float = 0
    var selectedMinValue:Float = 0
    var type = ""
    var title = ""
    var arrayOfTypes:[String] = []
    var selectedBrands :[String] = []
    var selectedManufacturer:[String] = []
    var selectedCategories:[String] = []
    var arrayOfCategory:[Category] = []
    var selectedCuisines:[String] = []
    var arrayOfFoodType :[String] = []
    var arrayOfTypesSelected:[String] = []
    var arrayOfSelectedFilters :[String] = []
    let filterDetails_responce = PublishSubject<Bool>()
    var storeFilter = false
    let disposeBag = DisposeBag()
    
    
    //Cat,SubCat,SubSubCat,[Brand],minPrice,maxPrice,[manufacturer],offer,sort,Rating,Cousine,Foodtype,data string
    
    static let filterArray_responce = PublishSubject<(String,String,String,[String],Float,Float,[String],String,Int,Int,String,String,String)>()
    
    
    func getFilter() {
        if !storeFilter {
            FIlterAPICalls.getFilter(selectedItem: type,needle: needle,searchFrom: searchFrom,catName: chosenCat,subCatName: chosenSubCat,offer: offer, language: needleLanguage).subscribe(onNext: { [weak self]data in
                if self?.type == APIRequestParams.FilterPrice {
                    if data.count > 0 {
                        let dataIn = data as! [String]
                        if let maxPrice = Float(dataIn[0]){
                        self?.maxValue = maxPrice
                        }
                    }
                }else if ((data as? [Category]) != nil) {
                    let dataIn = data as! [Category]
                    self?.arrayOfCategory = dataIn
                }else if ((data as? [SubCategory]) != nil) {
                    let dataIn = data as! [SubCategory]
                    for item in (self?.arrayOfCategory)! {
                        if item.Name == self?.selectedCategory {
                            item.SubCategories = dataIn
                        }
                    }
                }else if self?.type == APIRequestParams.SubSubCategoryFilter {
                    let dataIn = data as! [SubSubCategory]
                    for item in (self?.arrayOfCategory)! {
                        if item.Name == self?.selectedCategory {
                            for item2 in item.SubCategories {
                                if item2.Name == self?.SubCatSelected {
                                    item2.SubSubCat = dataIn
                                }
                            }
                        }
                    }
                }else{
                    self?.arrayOfTypes = data as! [String]
                }
                self?.filterDetails_responce.onNext(true)
            }).disposed(by: self.disposeBag)
        }else{
            FIlterAPICalls.getFilterStore(selectedItem: type).subscribe(onNext: { [weak self]data in
                if self?.type == APIRequestParams.FilterPrice {
                    if data.count > 0 {
                        let dataIn = data as! [String]
                        self?.maxValue = Float(dataIn[0])!
                    }
                }else if ((data as? [Category]) != nil) {
                    let dataIn = data as! [Category]
                    self?.arrayOfCategory = dataIn
                }else if ((data as? [SubCategory]) != nil) {
                    let dataIn = data as! [SubCategory]
                    for item in (self?.arrayOfCategory)! {
                        if item.Name == self?.selectedCategory {
                            item.SubCategories = dataIn
                        }
                    }
                }else if self?.type == APIRequestParams.SubSubCategoryFilter {
                    let dataIn = data as! [SubSubCategory]
                    for item in (self?.arrayOfCategory)! {
                        if item.Name == self?.selectedCategory {
                            for item2 in item.SubCategories {
                                if item2.Name == self?.SubCatSelected {
                                    item2.SubSubCat = dataIn
                                }
                            }
                        }
                    }
                }else{
                    self?.arrayOfTypes = data as! [String]
                }
                self?.filterDetails_responce.onNext(true)
            }).disposed(by: self.disposeBag)
        }
    }
}
