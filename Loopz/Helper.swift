//
//  Helper.swift
//  UFly
//
//  Created by 3Embed on 26/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import Kingfisher
import Stripe
import ReachabilitySwift
import libPhoneNumber_iOS
import RxAlamofire
import RxSwift
import RxCocoa
import GoogleMaps
import GooglePlaces
import CoreLocation
import PullToDismiss
import LatLongToTimezone
import Network

class Helper: NSObject {
    
    static var pullToDismiss:PullToDismiss? = nil
    static let touchMe = TouchIDAuth()
    static var keychainManager:KeychainManager? = KeychainManager.shared
    static let reachability = Reachability.init(hostname: "www.google.com")
    static var alertPopup:UIAlertController? = nil
    static let networkReachabilityChange = PublishSubject<Bool>()
    static let pathObserver = PublishSubject<(String,Int)>()
    static let disposeBag = DisposeBag()
    
    
    class func setGrocerOnlyRootViewController(){
        let when = DispatchTime.now()
        DispatchQueue.main.asyncAfter(deadline: when){
            let mainStoryBoard = UIStoryboard(name: "Grocer", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "GrocerTabBar") as! UITabBarController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    
    
    
    ///Set ui element Border and corner
    class func setUiElementBorderWithCorner(element:UIView, radius:Float, borderWidth:Float , color:UIColor) {
        element.layer.cornerRadius = CGFloat(radius)
        element.layer.borderWidth = CGFloat(borderWidth)
        element.layer.borderColor = color.cgColor
        element.clipsToBounds = true
    }
    //Get UIColor from HexColorCode
    class func getUIColor(color:String) -> UIColor {
        var cString:String = color.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.length) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //Set Button
    class func setButton(button:UIButton,view:UIView,primaryColour:UIColor,seconderyColor:UIColor,shadow:Bool) {
        //Set Background
        DispatchQueue.main.async {
            button.setBackgroundImage(Helper.GetImageFrom(color: primaryColour), for: .normal)
            button.setBackgroundImage(Helper.GetImageFrom(color: seconderyColor), for: .highlighted)
            button.setBackgroundImage(Helper.GetImageFrom(color: seconderyColor), for: .selected)
        }
        //Set Border and Corner
        button.layer.cornerRadius = CGFloat(UIConstants.ButtonCornerRadius)
        button.layer.borderWidth = CGFloat(UIConstants.ButtonBorderWidth)
        button.layer.borderColor = primaryColour.cgColor
        if primaryColour == UIColor.white || primaryColour == Colors.SecondBaseColor || primaryColour == UIColor.clear {
            button.layer.borderColor = seconderyColor.cgColor
        }
        button.clipsToBounds = true
        
        
        if view.isKind(of: UIView.classForCoder()) {
            if shadow {
                self.setShadow(sender: view)
            }
            view.backgroundColor = UIColor .clear
        }
        
        //Set Title
        var pC = primaryColour
        var sC = seconderyColor
        if seconderyColor == UIColor.clear {
            sC = UIColor.white
        }
        if primaryColour == UIColor.clear {
            pC = UIColor.white
        }
        button.setTitleColor(sC, for: .normal)
        button.setTitleColor(pC, for: .highlighted)
        button.setTitleColor(pC, for: .selected)
    }
    
    
    //Set Button
    class func setButtonAndView(button:UIButton,view:UIView,primaryColour:UIColor,seconderyColor:UIColor,shadow:Bool) {
        //Set Background
        DispatchQueue.main.async {
            button.setBackgroundImage(Helper.GetImageFrom(color: primaryColour), for: .normal)
            button.setBackgroundImage(Helper.GetImageFrom(color: seconderyColor), for: .highlighted)
            button.setBackgroundImage(Helper.GetImageFrom(color: seconderyColor), for: .selected)
        }
        //Set Border and Corner
        button.layer.cornerRadius = CGFloat(UIConstants.ButtonCornerRadius)
        button.layer.borderWidth = CGFloat(UIConstants.ButtonBorderWidth)
        button.layer.borderColor = primaryColour.cgColor
        if primaryColour == UIColor.white || primaryColour == Colors.SecondBaseColor || primaryColour == UIColor.clear {
            button.layer.borderColor = seconderyColor.cgColor
        }
        button.clipsToBounds = true
        view.backgroundColor = UIColor .white
        
        //Set Title
        var pC = primaryColour
        var sC = seconderyColor
        if seconderyColor == UIColor.clear {
            sC = UIColor.white
        }
        if primaryColour == UIColor.clear {
            pC = UIColor.white
        }
        button.setTitleColor(sC, for: .normal)
        button.setTitleColor(pC, for: .highlighted)
        button.setTitleColor(pC, for: .selected)
    }
    
    //Set Button Title
    class func setButtonTitle(normal:String,highlighted:String,selected:String,button:UIButton) {
        button.setTitle(normal, for: .normal)
        button.setTitle(highlighted, for: .highlighted)
        button.setTitle(selected, for: .selected)
    }
    
    //Get Image From Color
    static func GetImageFrom(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    //Multiple ColourText
    class func updateText(text: String,subText:String,_ sender: Any,color:UIColor,link: String) {
        
        let range = (text.lowercased() as NSString).range(of: subText.lowercased())
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value:color , range: range)
        var size = 0
        if let element = sender as? UITextView
        {
            size = Int((element.font?.pointSize)! + CGFloat(2))
        }else if let element = sender as? UIButton {
            size = Int((element.titleLabel?.font.pointSize)! + CGFloat(2))
        }else if let element = sender as? UILabel {
            size = Int((element.font?.pointSize)! + CGFloat(2) )
        }
        else if let element = sender as? UITextField {
            size = Int((element.font?.pointSize)! + CGFloat(2) )
        }
        
        
        let font = UIFont(name: Fonts.Primary.Medium , size: CGFloat(size))
        attributedString.addAttribute(NSAttributedString.Key.font, value:font!, range: range)
        if let element = sender as? UIButton {
            element.setAttributedTitle(attributedString, for: .normal)
            let range2 = (text as NSString).range(of: text)
            let attributedString2 = NSMutableAttributedString(string:text)
            attributedString2.addAttribute(NSAttributedString.Key.foregroundColor, value:color , range: range2)
            element.setAttributedTitle(attributedString2, for: .highlighted)
            element.setAttributedTitle(attributedString2, for: .selected)
        }else if let element = sender as? UILabel {
            element.attributedText = attributedString
        }
        else if let element = sender as? UITextView {
            attributedString.addAttribute(.link, value: link, range: range)
            element.attributedText = attributedString
            element.linkTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.foregroundColor.rawValue: color])
        }
        else if let element = sender as? UITextField {
            element.attributedText = attributedString
        }
    }
    
    
    //Handling Progress Indicator 
    class func showNoInternet() {
        Helper.hidePI()
        let progress: NoInternetView = NoInternetView.shared
        progress.showAlert()
    }
    
    class func hideNoInternet(){
        LoadingProgress.shared.hide()
    }
    
    //Handling Progress Indicator
    class func showPI(string:String) {
        //        if CommonAlertView.obj == nil {
        let progress: LoadingProgress = LoadingProgress.shared
        progress.showPI(message: string)
        //        }
    }
    class func showPIWhite(string:String) {
        //        if CommonAlertView.obj == nil {
        let progress: LoadingProgress = LoadingProgress.shared
        progress.showPIWhite(message: string)
        //        }
    }
    //Handling Progress Indicator
    class func showPISuccess() {
        let progress: SuccessView = SuccessView.shared
        progress.showPI()
    }
    
    class func hidePI(){
        LoadingProgress.shared.hide()
    }
    
    //Handling Alert Messages
    class func showAlert(message:String, head:String, type:Int) {
        //        Helper.hidePI()
        //        let alert:CommonAlertView = CommonAlertView.shared
        //        alert.showAlert(message: message, head: head, type: type)
        Helper.hidePI()
        alertPopup?.dismiss(animated: false, completion: nil)
        alertPopup = UIAlertController(title:head, message: message, preferredStyle: UIAlertController.Style.alert)
        alertPopup?.addAction(UIAlertAction(title: StringConstants.OK(), style: UIAlertAction.Style.cancel, handler: nil))
        if UIApplication.shared.delegate?.window != nil {
            let vc = Helper.finalController()
            vc.present(alertPopup!, animated: true, completion: nil)
            
        }
        
    }
    
     class func showAlertWithOutTitle(message:String, type:Int) {
                   Helper.hidePI()
                   alertPopup?.dismiss(animated: false, completion: nil)
                    alertPopup = UIAlertController(title:nil, message: message, preferredStyle: UIAlertController.Style.alert)
                    alertPopup?.addAction(UIAlertAction.init(title: StringConstants.OK() , style:  UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
                        let vc = Helper.finalController()
                       vc.dismiss(animated: true, completion: nil)
                   }))
               if UIApplication.shared.delegate?.window != nil {
                      let vc = Helper.finalController()
                       vc.present(alertPopup!, animated: true, completion: nil)
                   }
    }
    
    //Handling Alert Messages
    class func showAlertReturn(message:String, head:String, type:String, closeHide:Bool,responce:CommonAlertView.ResponceType) {
        Helper.hidePI()
        alertPopup?.dismiss(animated: true, completion: nil)
        alertPopup = UIAlertController(title:head, message: message, preferredStyle: UIAlertController.Style.alert)
        if closeHide == false {
            alertPopup?.addAction(UIAlertAction(title: (Utility.getLanguage().Code == "es" ? StringConstants.Cancel() : "Cancelar"), style: UIAlertAction.Style.destructive, handler: nil))
        }
        alertPopup?.addAction(UIAlertAction(title: type, style: UIAlertAction.Style.default){ action -> Void in
            CommonAlertView.AlertPopupResponse.onNext(responce)
            // Put your code here
        })
        
        if UIApplication.shared.delegate?.window != nil {
            let vc = Helper.finalController()
            vc.present(alertPopup!, animated: true, completion: nil)
        }
    }
    
    class func hideAlert() {
        CommonAlertView.shared.hideAlert()
    }
    
    
    //Handling Alert Messages
    class func showUnVerifyNotification() -> (NotificationView,Bool) {
        let alert:NotificationView = NotificationView.shared
        let userData = Utility.getProfileData()
        if Utility.getLogin() == false {
            alert.notificationLabel.text = ""
            return (alert,false)
        }
        if Utility.getStore().TypeStore != .Marijuana {
            alert.notificationLabel.text = ""
            return (alert,false)
        }
        if userData.IdCard.Verified == false || userData.MMJCard.Verified == false {
            if userData.IdCard.Url.length == 0 || userData.MMJCard.Url.length == 0 {
                alert.notificationLabel.text = StringConstants.NotComplete()
            }else{
                alert.notificationLabel.text = StringConstants.NotVarified()
            }
            return (alert,true)
        }else{
            alert.notificationLabel.text = ""
            return (alert,false)
        }
    }
    
    
    
    
    /// return header view
    ///
    /// - Parameters:
    ///   - title: title of the header
    ///   - showViewAll: true - adds showViewAll
    /// - Returns: returs header view with title and shadow
    class func showHeader(title: String , showViewAll: Bool)-> View{
        let header:HeaderView = HeaderView().shared
        header.headerTitle.text = title
        if showViewAll {
            header.viewAllView.isHidden = false
        }else{
            header.viewAllView.isHidden = true
        }
        if header.headerTitle.text?.length == 0{
            header.viewAllView.isHidden = true
            header.heightConstraint.constant = 0
        } else {
            header.heightConstraint.constant = 50
        }
        return header
    }
    
    
    
    
    class func showAddonHeader(title: String ,subTitle:String, showViewAll: Bool , showSeperator:Bool)-> View{
        let header:HeaderView = HeaderView().shared3
        header.headerTitle.text = title
        header.subTitle.text = subTitle
        if header.headerTitle.text?.length == 0{
            header.heightConstraint.constant = 0
        } else {
            header.heightConstraint.constant = 50
        }
        header.backgroundColor = UIColor.white
        header.headerTitle.textColor = Colors.PrimaryText
        Fonts.setPrimaryRegular(header.headerTitle, size: 14)
        header.subTitle.textColor = Colors.SeconderyText
        Fonts.setPrimaryRegular(header.subTitle, size: 14)
        return header
 }
    
    
    class func showHeader(title: String , showViewAll: Bool , showSeperator:Bool)-> View{
        let header:HeaderView = HeaderView().shared
        header.headerTitle.text = title
        if showViewAll {
            header.viewAllView.isHidden = false
        }else{
            header.viewAllView.isHidden = true
        }
        if showSeperator {
            header.seperator1.isHidden = false
        }else{
            header.seperator1.isHidden = true
        }
        if header.headerTitle.text?.length == 0{
            header.viewAllView.isHidden = true
            header.heightConstraint.constant = 0
            if showSeperator{
                header.heightConstraint.constant = 20
            }
        } else {
            header.heightConstraint.constant = 50
        }
        header.backgroundColor = UIColor.white
        header.headerTitle.textColor = Colors.PrimaryText
        Fonts.setPrimaryRegular(header.headerTitle, size: 14)
        return header
    }
    
    class func showHeaderSearch(store: Store)-> View{
        let header:HeaderView = HeaderView().shared2
        header.headerTitle.text = store.Name
        header.detailText.text = "\(store.StoreRating) | \(store.EstimatedTime) | \(store.Address)"
        return header
    }
    //Add Shadow
    class func setShadow(sender:UIView) {
        
        sender.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(0))
        sender.layer.shadowColor = UIColor.lightGray.cgColor
        sender.layer.shadowOpacity = 0.5
    }
    //removeshadow
    class func removeShadow(sender:UIView) {
        
        sender.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(0))
        sender.layer.shadowColor = UIColor.clear.cgColor
        sender.layer.shadowOpacity = 0
    }
    
    //Remove separation from navigation
    class func removeNavigationSeparator(controller:UINavigationController,image:Bool) {
        if image {
            controller.navigationBar.setBackgroundImage(Helper.GetImageFrom(color: Colors.SecondBaseColor), for: UIBarMetrics.default)
        }else{
            controller.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        }
        
        controller.navigationBar.shadowImage = UIImage()
    }
    
    //Transparent navigation
    class func transparentNavigation(controller:UINavigationController) {
        
        controller.navigationBar.backgroundColor = UIColor.clear
        controller.navigationBar.isTranslucent = true
        Helper.removeNavigationSeparator(controller: controller, image: false)
    }
    
    //NonTransparent navigation
    class func nonTransparentNavigation(controller:UINavigationController) {
        controller.navigationBar.backgroundColor = Colors.SecondBaseColor
        //        controller.navigationBar.isTranslucent = false
        controller.navigationBar.tintColor = Colors.PrimaryText
        controller.navigationBar.barTintColor = Colors.SecondBaseColor
        controller.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.PrimaryText]
        controller.navigationBar.setBackgroundImage(Helper.GetImageFrom(color: Colors.SecondBaseColor), for: UIBarMetrics.default)
    }
    
    ///Adding Done Button On Keyboard
    class func addDoneButtonOnTextField(tf:UITextField, vc: UIView){
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem.init(title: StringConstants.Done(), style: .plain, target: vc, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        tf.inputAccessoryView = keyboardToolbar
    }
    
    class func setCountryCode()-> String{
//        var country = NSLocale.current.regionCode
//        if Utility.getAddress().Country.sorted().count != 0 {
//            country = Utility.getAddress().Country
//        }else{
//            if GoogleKeys.myCountry.length > 0 {
//                country = GoogleKeys.myCountry
//            }else{
//                return "+31"
//            }
//        }
//        let picker = VNHCountryPicker.dialCode(code: country!)
//        if picker.dialCode.length == 0{
//            return "+31"
//        }
//        return picker.dialCode
         return "+57"
    }
    
    class func setCountryCode(code:String) -> VNHCounty{
        
        if code.length > 0 {
            return VNHCountryPicker.fromDialCode(code: code)
        }else{
            var country = NSLocale.current.regionCode
            if GoogleKeys.myCountry.length > 0 {
                country = GoogleKeys.myCountry
            }
            return VNHCountryPicker.dialCode(code: country!)
        }
    }
    
    
    /// Validate Email address
    class func isValidEmail(text:String) -> Bool {
        
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}", options: .caseInsensitive)
            return regex.firstMatch(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, text.length)) != nil
        }
        catch {
            return false
        }
    }
    
    
    class func isValidIP(text:String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", options: .caseInsensitive)
            return regex.firstMatch(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, text.length)) != nil
        }
        catch {
            return false
        }
    }
    
    
    
    //validate PhoneNumber
    class func isValidPhoneNumber(text:String) -> Bool {
        
        let charcter  = NSCharacterSet(charactersIn: "+0123456789").inverted
        var filtered: String!
        let inputString: NSArray = text.components(separatedBy: charcter) as NSArray
        filtered = inputString.componentsJoined(by: "") as String?
        if text == ""{
            return false
        }
        return text == filtered
    }
     
    //Open Login page
    class func isLoggedIn(isFromHist: Bool) -> Bool{
        let vc = Helper.finalController()
        if Utility.getLogin(){
            return true
        }
        else{
            let viewController = UIStoryboard(name: UIConstants.AuthenticationStoryboard, bundle: nil).instantiateViewController(withIdentifier: UIConstants.ControllerIds.SignInVC) as! SignInVC
            let navController = UINavigationController(rootViewController: viewController)
            navController.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "BackIcon")
            navController.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "BackIcon")
            navController.modalPresentationStyle = .fullScreen
            vc.present(navController, animated:true, completion: nil)
            if isFromHist {
                if vc.isBeingDismissed {
                    Helper.changetTabTo(index: AppConstants.Tabs.Home)
                }
            }
            
            return false
        }
    }
    
    
    //    class func goToCartScreen(controller:UIViewController){
    //        let vc = controller.view?.window?.rootViewController
    //        let viewController = UIStoryboard(name: UIConstants.MainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: UIConstants.ControllerIds.CartVC) as! CartVC
    //       let navController = UINavigationController(rootViewController: viewController)
    //
    //         navController.pushViewController(viewController, animated: true)
    //
    //    }
    
    //Text is number
    class func isNumber(text:String) -> Bool {
        return !text.isEmpty && text.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    // text is numeric
    class func isNumeric(text: String) -> Bool {
        guard text.length > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."]
        return Set(text).isSubset(of: nums)
    }
    
    
    //Text is Name
    class func isValidName(textName:String) -> Bool {
        
        var returnValue = true
        
        let text = textName.replacingOccurrences(of: " ", with: "")
        let mobileRegEx = "^[a-zA-Z]+$"               //"[A-Za-z]{3}"  // {3} -> at least 3 alphabet are compulsory.
        
        do {
            let regex = try NSRegularExpression(pattern: mobileRegEx)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    //Save CredentiALS TO KEYCHAIN
    class func SaveCredsToKeyChain(controller:UIViewController,key:Authentication,finish:@escaping (Bool) -> Void) {
        let params : [String : String] =    [
            "key"                   :key.Phone,
            "password"              :key.Password
        ]
        Helper.keychainManager?.SaveData(data: params){(flag) in
            finish(flag)
        }
    }
    
    //Authenticate CredentiALS TO KEYCHAIN
    class func AuthenticateCredsToKeyChain(data:Authentication,finish:@escaping (Authentication) -> Void) {
        if let password = Helper.keychainManager?.GetData(data: data.Phone){
            if password.length == 0 {
                return
            }
            touchMe.authenticateUser() { message in
                data.Password = password
                finish(data)
            }
        }
    }
    
    //Set Image From Url
    class func setImage(imageView:UIImageView,url:String,defaultImage:UIImage) {
        imageView.image = nil
        imageView.kf.indicatorType = .activity
        if  let trimmedString = url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        {
            let trimmedStrings = trimmedString.replacingOccurrences(of: "http://s3.amazonaws.com", with: "https://s3.amazonaws.com")

            imageView.kf.setImage(with: URL(string: trimmedStrings), placeholder: defaultImage)
        }
    }
    
    
    //  Converted to Swift 4 by Swiftify v4.2.35826 - https://objectivec2swift.com/
    class func image(with sourceImage: UIImage?, scaledToWidth i_width: Float) -> UIImage? {
        let oldWidth = Float(sourceImage?.size.width ?? 0.0)
        let scaleFactor: Float = i_width / oldWidth
        
        let newHeight = Float((sourceImage?.size.height ?? 0.0) * CGFloat(scaleFactor))
        let newWidth: Float = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width: CGFloat(newWidth), height: CGFloat(newHeight)))
        sourceImage?.draw(in: CGRect(x: 0, y: 0, width: CGFloat(newWidth), height: CGFloat(newHeight)))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    //Add shadow to NavigationController
    class func addShadowToNavigationBar(controller:UINavigationController){
        controller.navigationBar.layer.masksToBounds = false
        controller.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        controller.navigationBar.layer.shadowOpacity = 0.8
        controller.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        controller.navigationBar.layer.shadowRadius = 2
    }
    
    class func removeShadowToNavigationBar(controller:UINavigationController){
        
        controller.navigationBar.layer.masksToBounds = true
        controller.navigationBar.layer.shadowColor = UIColor.white.cgColor
        controller.navigationBar.layer.shadowOpacity = 0
        controller.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        controller.navigationBar.layer.shadowRadius = 0
        
    }
    
    //TabChange
    class func changetTabTo(index: AppConstants.Tabs){
        
        if finalController().tabBarController != nil {
            let vc = finalController().tabBarController
            vc?.selectedIndex = index.rawValue
        }
    }
    
    //set radius half of the view Hight for Rounded View
    class func setRadius(element:UIView) -> Float {
        let radius:Float = Float(element.frame.size.height/2)
        return radius
    }
    
    
    /// Get Card Image
    ///
    /// - Parameter type: Type of Car
    /// - Returns: Return Value is Card Image
    class func cardImage(with type: String) -> UIImage {
        
        switch type {
            
        case "visa":
            return STPImageLibrary.brandImage(for: STPCardBrand.visa)
        case "mastercard":
            return STPImageLibrary.brandImage(for: STPCardBrand.masterCard)
        case "discover":
            return STPImageLibrary.brandImage(for: STPCardBrand.discover)
        case "american express":
            return STPImageLibrary.brandImage(for: STPCardBrand.amex)
        case "amex":
            return STPImageLibrary.brandImage(for: STPCardBrand.amex)
        case "jcb":
            return STPImageLibrary.brandImage(for: STPCardBrand.JCB)
        case "diners club":
            return STPImageLibrary.brandImage(for: STPCardBrand.dinersClub)
        default:
            return STPImageLibrary.brandImage(for: STPCardBrand.unknown)
        }
    }
    
    class func clipDigit(valeu:Float, digits: Int) -> String {
        return String(format:"%.\(digits)f", valeu)
    }
    
    class func getDateString(value:Date,format:String,zone:Bool) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale.init(identifier: "en_US_POSIX")// for 24 hours format
        if zone {
            formatter.timeZone = Helper.getTimeZoneFromPickUpLoaction()
        }
        let myString = formatter.string(from: value)
        return myString
    }
    
    class func getStringToDate(value:String,format:String,zone:Bool) -> Date? {
        let formatter = DateFormatter()
        if zone {
            formatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        }
        formatter.locale = Locale.init(identifier: "en_US_POSIX")
        formatter.dateFormat = format
        
        if let myString = formatter.date(from: value) {
            return myString
        }
        return Date()
    }
    
    class func setAddressImage(tag:String) -> UIImage {
        if tag.lowercased() == StringConstants.Home().lowercased() {
            return #imageLiteral(resourceName: "HomeAddress")
        }else if tag.lowercased() == StringConstants.Work().lowercased(){
            return #imageLiteral(resourceName: "WorkAddress")
        }else{
            return #imageLiteral(resourceName: "OtherAddress")
        }
    }
    
    //set dark background
    class func blurEffect(view:UIView) {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.sendSubviewToBack(blurEffectView)
    }
    
    class func getTimeZoneFromPickUpLoaction() -> TimeZone? {
        let manager = Utility.currentLatAndLong()
        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(manager.Latitude), longitude: CLLocationDegrees(manager.Longitude))
        return TimezoneMapper.latLngToTimezone(location)
    }
    
    
    
    //MARK: - Reachability
    class func checkAppReachability() {
        //Reachability check here
        Reachability.rx.isReachable
            .subscribe(onNext: { isReachable in
                if AppConstants.Reachable == false && isReachable == true {
                    AppConstants.Reachable = isReachable
                    Helper.networkReachabilityChange.onNext(true)
                }
                AppConstants.Reachable = isReachable
                let progress: NoInternetView = NoInternetView.shared
                if isReachable {
                    if NoInternetView.obj != nil {
                        progress.hideAlert()
                    }
                }else{
                    //                    if NoInternetView.obj == nil {
                    progress.showAlert()
                    //                    }
                }
            }).disposed(by: Helper.disposeBag)
        
        do{
            try Helper.reachability?.startNotifier()
        }catch{
        }
    }
    
    class func addnewcheckAppReachability(){
           let progress: NoInternetView = NoInternetView.shared
           if #available(iOS 12.0, *) {
               let monitor = NWPathMonitor()
              
               monitor.pathUpdateHandler = { path in
                          if path.status == .satisfied {
                              print("We're connected!")
                           DispatchQueue.main.async {
                              AppConstants.Reachable = true
                               progress.hideAlert()
                           }
                                
                          } else {
                              print("No connection.")
                            
                            DispatchQueue.main.async {
                               AppConstants.Reachable = false
                                progress.showAlert()
                           }
                             
                               
                          }

                          print(path.isExpensive)
                      }
                      
                      let queue = DispatchQueue.global(qos: .background)
                      monitor.start(queue: queue)
               
               
               
               
           } else {
               // Fallback on earlier versions
           }
           
           
           
           
           
       }
       
    
    
    
    
    //Set Currency
    class func setCurrency(displayAmount: Float,price:Float) -> NSAttributedString {
        if displayAmount == price || price == 0 {
            var pri = price
            if displayAmount != 0 {
                pri = displayAmount
            }
            var priceString = Helper.clipDigit(valeu:pri, digits: 2)
            if Utility.getCurrency().1.lowercased() == "eur".lowercased() {
                priceString = priceString.replacingOccurrences(of: ".", with: ",")
            }
            return NSMutableAttributedString(string:Utility.getCurrency().1 + " " + priceString)
        }
        var text = Utility.getCurrency().1 + " " + Helper.clipDigit(valeu:price, digits: 2) + " " + Utility.getCurrency().1 + " " + Helper.clipDigit(valeu:displayAmount, digits: 2)
        if Utility.getCurrency().1.lowercased() == "eur".lowercased() {
            text = text.replacingOccurrences(of: ".", with: ",")
        }
        var priceString = Helper.clipDigit(valeu:price, digits: 2)
        if Utility.getCurrency().1.lowercased() == "eur".lowercased() {
            priceString = priceString.replacingOccurrences(of: ".", with: ",")
        }
        return Helper.updateTextCurrency(textCross: Utility.getCurrency().1 + "" + priceString, text: text)
    }
    
    class func df2so(_ price: Double) -> String{
            let numberFormatter = NumberFormatter()
            numberFormatter.groupingSeparator = ","
            numberFormatter.groupingSize = 3
            numberFormatter.usesGroupingSeparator = true
            numberFormatter.decimalSeparator = "."
            numberFormatter.numberStyle = .decimal
            numberFormatter.maximumFractionDigits = 2
        let price:String = Utility.getCurrency().1 + "" + numberFormatter.string(from: price as NSNumber)!
            return price
        }
    //Cross Text
    class func updateTextCurrency(textCross: String,text:String) -> NSAttributedString {
        
        let range = (text.lowercased() as NSString).range(of: textCross.lowercased())
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value:2 , range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value:Colors.SeparatorDark , range: range)
        return attributedString
    }
    
    
    
    
    //Red Color Text
    class func updateRedColor(textCross: String,text:String) -> NSAttributedString {
        
        let range = (text.lowercased() as NSString).range(of: textCross.lowercased())
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor.red , range: range)
        return attributedString
    }
    
    
    
    
    
    //Upload Image
    class func uploadImage(data:UIImage,path:String,page: Page) -> String {
        var strUrl = path + Utility.DeviceTimeWithoutBlankSpace + ".png"
        strUrl = strUrl.replacingOccurrences(of: " ", with: "_")
        strUrl = strUrl.lowercased()
        // Resize Image
        let resizedImage = data.resizeImage(size: CGSize(width: 100, height: 100))
        let temp = UploadImage(image: resizedImage, path: strUrl)
        let finalUrl = AmazonKeys.Url + AmazonKeys.Bucket + "/" + strUrl
        
        // Upload Images in Background Using Singletone Class
        let upMoadel = UploadImageModel.shared
        upMoadel.imageIndex = 0
        upMoadel.uploadImages = [temp]
        upMoadel.start(page: page)
        return finalUrl
    }
    
    
    /// validate phone number
    ///
    /// - Parameters:
    ///   - phoneNumber: phoneNumber
    ///   - code: countryCode
    /// - Returns: true or false
    class func isPhoneNumValid(phoneNumber: String , code:String) -> Bool{
      //  let number = phoneNumber
        let phoneUtil = NBPhoneNumberUtil()
        do {
            ///mobileNumber Validation
            let phoneNumber = try phoneUtil.parse(phoneNumber, defaultRegion: VNHCountryPicker.dialCode(code:code).code)
            let validPhoneNumber: Bool = phoneUtil.isValidNumber(phoneNumber)
//             let phoneRegex = "^[1-9+]{4}[0-9]{8}"
//            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
//             if code == "+1242" && !validPhoneNumber
//             {
//                let num = "\(code)" + "\(number)"
//                print(num)
//                return  phoneTest.evaluate(with:num)
//            }
            
            return validPhoneNumber
        }
        catch let error as NSError {
            print(error.localizedDescription)
            return false
        }
    }
    
    class func drawPath(mapView:GMSMapView, order:Order) {
        var destination = ""
        var destinationType = 0 // 0: for pickUp and 1: for destination
        let origin = "\(order.DriverLat),\(order.DriverLong)"
        if order.Status == 8 || order.Status == 10 {
            destination = "\(order.PickLat),\(order.PickLong)"
            destinationType = 0
        }else{
            destination = "\(order.DropLat),\(order.DropLong)"
            destinationType = 1
        }
        
         APICalls.keyRotationImpression(type: .direction)

        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(GoogleKeys.GoogleMapKey)"
        RxAlamofire.requestJSON(.get, url, parameters: [:], headers: [:]).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as![String:Any]
            Helper.hidePI()
            let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
            if errNum == .success {
                let routes = bodyIn["routes"] as! [Any]
                print(routes)
                if let route = bodyIn["routes"] as? [[String:Any]]{
                    if let firstRoute = route.first{
                        if let leg = firstRoute["legs"] as? [[String:Any]]{
                            if let first = leg.first{
                                if let duration = first["duration"] as? [String:Any]{
                                    if let text = duration["text"] as? String{
                                        print(text)
                                        var timing = text
                                        let arrayData:[String] = text.components(separatedBy: " ")
                                        if let timeRemaining:Int = arrayData.first?.integerValue{
                                            if timeRemaining <= 1{
                                                timing = StringConstants.AlmostReached()
                                            }
                                        }
                                        Helper.pathObserver.onNext((timing,destinationType))
                                    }
                                }
                            }
                        }
                    }
                }

                var arrayPaths = [GMSPath]()
                if routes.count > 0 {
                    for route in routes
                    {
                        let routeIn = route as! [String:Any]
                        let routeOverviewPolyline = routeIn["overview_polyline"] as? [String:Any]
                        let points = routeOverviewPolyline!["points"]
                        let path = GMSPath.init(fromEncodedPath: points! as! String)
                        let polyline = GMSPolyline.init(path: path)
                        polyline.strokeWidth = 4;
                        polyline.strokeColor = Colors.PrimaryText
                        polyline.map = mapView
                        arrayPaths.append(path!)
                        mapView.animate(with: GMSCameraUpdate.fit(GMSCoordinateBounds(path: polyline.path!), withPadding: 10))

                    }
                }else{
                    let path = GMSMutablePath.init()
                    path.add(CLLocationCoordinate2DMake(CDouble(order.PickLat), CDouble(order.PickLong)))
                    path.add(CLLocationCoordinate2DMake(CDouble(order.DropLat), CDouble(order.DropLong)))

                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeWidth = 3;
                    polyline.strokeColor = Colors.PrimaryText
                    polyline.map = mapView;
                    mapView.animate(with: GMSCameraUpdate.fit(GMSCoordinateBounds(path: polyline.path!), withPadding: 10))

                    arrayPaths.append(path)
                }
            }else{
                let path = GMSMutablePath.init()
                path.add(CLLocationCoordinate2DMake(CDouble(order.PickLat), CDouble(order.PickLong)))
                path.add(CLLocationCoordinate2DMake(CDouble(order.DropLat), CDouble(order.DropLong)))

                let polyline = GMSPolyline.init(path: path)
                polyline.strokeWidth = 3;
                polyline.strokeColor = Colors.PrimaryText
                mapView.animate(with: GMSCameraUpdate.fit(GMSCoordinateBounds(path: polyline.path!), withPadding: 10))
                polyline.map = mapView;
            }
        }, onError: { (Error) in
            
        }).disposed(by: Helper.disposeBag)
    }
    
    class func setMarker(latitude:Float, longitude:Float, icon:UIImage) -> GMSMarker {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(CLLocationDegrees(latitude), CLLocationDegrees(longitude))
        marker.groundAnchor = CGPoint(x:0.25, y:0.25)
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.icon = icon
        return marker
    }
    class func setMarker(latitude:Float, longitude:Float, iconView:UIView) -> GMSMarker {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(CLLocationDegrees(latitude), CLLocationDegrees(longitude))
        marker.groundAnchor = CGPoint(x:0.25, y:0.25)
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.tracksViewChanges = false
        marker.iconView = iconView
        return marker
        
    }

    class func fitAllMarkers(data:[GMSMarker], mapView:GMSMapView) {
        var bounds = GMSCoordinateBounds()
        for marker in data {
            bounds = bounds.includingCoordinate(marker.position)
        }
        mapView.animate(with: GMSCameraUpdate.fit(bounds))
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
    }
    
    
    func htmalToString(){
        
        
        
    }
    
    
    /// Remove null from Objects
    ///
    /// - Returns: Object without Null
    class func nullKeyRemoval(data:[String:Any]) -> [String:Any] {
        var dict = data
        let keysToRemove = Array(dict.keys).filter { dict[$0] is NSNull }
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        var keysToUpdate = Array(dict.keys).filter { dict[$0] is [String:Any] }
        for key in keysToUpdate {
            dict.updateValue(nullKeyRemoval(data: dict[key] as! [String : Any]), forKey: key)
        }
        keysToUpdate = Array(dict.keys).filter { dict[$0] is [Any] }
        for key in keysToUpdate {
            let dataArray = dict[key] as! [Any]
            var temp:[Any] = []
            for item in dataArray {
                if item is NSNull {
                    
                }else{
                    if ((item as? [String:Any]) != nil){
                        temp.append(nullKeyRemoval(data: item as! [String:Any]))
                    }else{
                        temp.append(item)
                    }
                }
            }
            dict.updateValue(temp, forKey: key)
        }
        return dict
    }
    
    
    //To get the fonts
    class func getFonts(){
        for family: String in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
    
    
    //Set Status Images
    class func setImageForStatus(status:Int) -> UIImage {
        switch status {
        case 16:
            return #imageLiteral(resourceName: "Cancelled")
        case 0:
            return #imageLiteral(resourceName: "NewOrder")
        case 1:
            return #imageLiteral(resourceName: "NewOrder")
        case 2:
            return #imageLiteral(resourceName: "Cancelled")
        case 3:
            return #imageLiteral(resourceName: "Rejected")
        case 4:
            return #imageLiteral(resourceName: "ConfirmedOn")
        case 5:
            return #imageLiteral(resourceName: "ReceviedOn")
        case 6:
            return #imageLiteral(resourceName: "DeliveredOn")
        case 7:
            return #imageLiteral(resourceName: "DeliveredOn")
        case 8,21:
            return #imageLiteral(resourceName: "ConfirmedOn")
        case 9:
            return #imageLiteral(resourceName: "Cancelled")
        case 10:
            return #imageLiteral(resourceName: "ReceviedOn")
        case 11:
            return #imageLiteral(resourceName: "DriverAtStoreOn")
        case 12:
            return #imageLiteral(resourceName: "pickedUpOn")
        case 13:
            return #imageLiteral(resourceName: "DriverAtLocOn")
        case 14:
            return #imageLiteral(resourceName: "DeliveredOn")
        case 15:
            return #imageLiteral(resourceName: "DeliveredOn")
        case 18,20:
            return #imageLiteral(resourceName: "Cancelled")
        default:
            return #imageLiteral(resourceName: "Rejected")
        }
    }
    
    class func setTitleForStatus(status:Int) -> String {
        switch status {
        case 16:
            return StringConstants.Cancelled()
        case 0:
            return StringConstants.ProcessingPayment()
        case 1:
            return StringConstants.NewOrder()
        case 2:
            return StringConstants.CancelledByManager()
        case 3:
            return StringConstants.RejectedByManager()
        case 4:
            return StringConstants.AcceptedByManager()
        case 5:
            return StringConstants.OrderReady()
        case 6:
            return StringConstants.OrderPicked()
        case 7:
            return StringConstants.OrderCompleted()
        case 8:
            return StringConstants.DriverAccept()
        case 9:
            return StringConstants.DriverCancelled()
        case 10:
            return StringConstants.DriverOnTheWay()//to store
        case 11:
            return StringConstants.DriverAtStore()
        case 12:
            return StringConstants.DriverOnTheWay()
        case 13:
            return StringConstants.DriverReached()// delivery location
        case 14:
            return StringConstants.Delivered()
        case 15:
            return StringConstants.OrderCompleted()
        case 18:
            return StringConstants.Expired()
        default:
            return ""
        }
    }
    
    
    /// Pull to close Controller
    ///
    /// - Parameter scrollView: scrolling elemnt to trigger close
    class func PullToClose(scrollView:UIScrollView) {
        Helper.pullToDismiss = PullToDismiss(scrollView: scrollView)
    }
    
    
    class func DotedBorder(sender: UIView){
        let border = CAShapeLayer()
        border.strokeColor = Colors.PrimaryText.cgColor
        border.fillColor = nil
        border.lineDashPattern = [1,3]
        border.path = UIBezierPath(rect: sender.bounds).cgPath
        border.frame = (sender as AnyObject).bounds
        sender.layer.addSublayer(border)
    }
    
    //ReviewPage Opening
    class func openReview(controller:UIViewController,order:Order) {
        let viewController = UIStoryboard(name: UIConstants.PopupStoryboard, bundle: nil).instantiateViewController(withIdentifier: UIConstants.ControllerIds.ReviewVC) as! ReviewVC
        viewController.reviewVM.orderRecieved = order
        let navController = UINavigationController(rootViewController: viewController)
        navController.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "CloseIcon")
        navController.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "CloseIcon")
        navController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.present(navController, animated:true, completion: nil)
    }
    
    
    
    class func finalController() -> UIViewController {
        
        if let wd = UIApplication.shared.delegate?.window {
            var vc = wd?.rootViewController
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).viewControllers.last
            }
            if(vc is UITabBarController){
                vc = (vc as! UITabBarController).viewControllers?[(vc as! UITabBarController).selectedIndex]
            }
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).viewControllers.last
            }
            while(vc?.presentedViewController != nil && (vc?.presentedViewController?.isKind(of: UIViewController.classForCoder()))!) {
                vc = vc?.presentedViewController
                if(vc is UINavigationController){
                    vc = (vc as! UINavigationController).viewControllers.last
                }
            }
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).viewControllers.last
            }
            if(vc is UITabBarController){
                vc = (vc as! UITabBarController).viewControllers?[(vc as! UITabBarController).selectedIndex]
            }
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).viewControllers.last
            }
            while(vc?.presentedViewController != nil && (vc?.presentedViewController?.isKind(of: UIViewController.classForCoder()))!) {
                vc = vc?.presentedViewController
                if(vc is UINavigationController){
                    vc = (vc as! UINavigationController).viewControllers.last
                }
            }
            return vc!
        }
        return UIViewController()
        
    }
    
    class func drawDottedLine(_ viewForDotteLine:UIView){
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = Colors.SeparatorDark.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [7, 3] // 7 is the length of dash, 3 is length of the gap.
        let p0 = CGPoint(x: viewForDotteLine.bounds.minX, y: viewForDotteLine.bounds.origin.y)
        let p1 = CGPoint(x: viewForDotteLine.bounds.maxX, y: viewForDotteLine.bounds.origin.y)
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        viewForDotteLine.layer.addSublayer(shapeLayer)
        viewForDotteLine.clipsToBounds = true
    }
    
    class func getKeyboardLanguage(language:String) -> String{
        if language.contains("es") {
            return "es"
        }else if language.contains("fr") {
            return "fr"
        }else if language.contains("nl") {
            return "nl"
        }else if language.contains("ar") {
            return "ar"
        }else{
            return "en"
        }
    }
    
    
    class func openAddons(item:Item,cart:Bool,finish:@escaping (AddOnVM.ResponseType) -> Void) {
        let viewController = UIStoryboard(name: UIConstants.PopupStoryboard, bundle: nil).instantiateViewController(withIdentifier: String(describing:AddOnVC.self)) as! AddOnVC
        viewController.addOnVM.SelectedItem = item
        viewController.fromCart = cart
      viewController.addOnVM.AddOnsVM_cartUpdateResponse.subscribe(onNext: { success in
            viewController.dismiss(animated: true, completion: {
                finish(success)
            })
        }).disposed(by: Helper.disposeBag)
        viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        Helper.finalController().present(viewController, animated:true, completion: nil)
    }
    
    
    class func openAddonsFromCart(item:Item,cart:Bool,finish:@escaping (AddOnVM.ResponseType,Item) -> Void) {
        let viewController = UIStoryboard(name: UIConstants.PopupStoryboard, bundle: nil).instantiateViewController(withIdentifier: String(describing:AddOnVC.self)) as! AddOnVC
        viewController.addOnVM.SelectedItem = item
        viewController.fromCart = cart
    viewController.addOnVM.AddOnsVM_cartUpdateResponseFromCart.subscribe(onNext: { (success,newItem) in
            viewController.dismiss(animated: true, completion: {
                finish(success,newItem)
            })
        }).disposed(by: Helper.disposeBag)
        viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        Helper.finalController().present(viewController, animated:true, completion: nil)
    }
    
    class func getStringFromUrl(url:URL) -> [String]{
        let urlString = url.absoluteString
        return urlString.components(separatedBy: "/")
    }
    
    
   
    //Cart Type
    
    class func checkCartType(arrayOfCarts:[Cart],storeType:AppConstants.TypeOfStore,cartType:AppConstants.TypeOfCart,completionHandler:@escaping (Bool) -> ()) -> Bool{
        
        let currentStore = Utility.getStore()
        
        if currentStore.Id == ""{
            currentStore.cartType = cartType
            currentStore.TypeStore = storeType
        }
        
        
        if arrayOfCarts.count == 0{
            return true
        }
        
        if currentStore.cartType.rawValue == 1 && arrayOfCarts.count == 1 {
            if currentStore.Id == arrayOfCarts[0].StoreId{
               return true
            }
            }
        
        if (currentStore.cartType.rawValue == 2 || currentStore.cartType.rawValue == 3 ) //&& //arrayOfCarts.count > 0 && arrayOfCarts[0].cartType.rawValue != 1
        {
                     return true
                }
        
       let alertController = UIAlertController.init(title: StringConstants.itemsInCart(), message: StringConstants.clearCartMessage(), preferredStyle: UIAlertController.Style.alert)

       let deleteAction = UIAlertAction.init(title: StringConstants.Yes(), style: UIAlertAction.Style.default) { (UIAlertAction) in
          CartVM().clearCart(handler: { (success) in
            completionHandler(true)
        })

       }
    
       let okAction = UIAlertAction.init(title: StringConstants.No(), style:UIAlertAction.Style.default , handler: nil)
        alertController.addAction(deleteAction)
        alertController.addAction(okAction)
        Helper.finalController().present(alertController, animated: true, completion: nil)
        return false
    }
    
    

    class func checkIsCartHasAnyOtherSupercategory(arrayOfCarts:[Cart],currentType:Int, completionHandler:@escaping (Bool) -> ()) -> Bool{

    if arrayOfCarts.count == 0{
    return true
    }


    if arrayOfCarts.count > 0,arrayOfCarts[0].storeType == currentType {
    return true

    }

    let alertController = UIAlertController.init(title: StringConstants.itemsInCart(), message: StringConstants.clearCartMessage(), preferredStyle: UIAlertController.Style.alert)

    let deleteAction = UIAlertAction.init(title: StringConstants.Yes(), style: UIAlertAction.Style.default) { (UIAlertAction) in
    CartVM().clearCart(handler: { (success) in
    completionHandler(true)
    })

    }

    let okAction = UIAlertAction.init(title: StringConstants.No(), style:UIAlertAction.Style.default , handler: nil)
    alertController.addAction(deleteAction)
    alertController.addAction(okAction)
    Helper.finalController().present(alertController, animated: true, completion: nil)
    return false
    }
    
    ///Adding Done Button On Keyboard
    class func addDoneButtonOnTextView(_ textView :UITextView, vc: UIView){
    let keyboardToolbar = UIToolbar()
    keyboardToolbar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    keyboardToolbar.sizeToFit()
    let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
    target: nil, action: nil)
    let doneBarButton = UIBarButtonItem.init(title: StringConstants.Done(), style: .plain, target: vc, action: #selector(UIView.endEditing(_:)))
    keyboardToolbar.items = [flexBarButton, doneBarButton]
    textView.inputAccessoryView = keyboardToolbar
    }
    
    
    class func  getDateString12hrsFormat(dateStr:String) -> String
    {
      let dateFormatterGet = DateFormatter()
      dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
      
      let dateFormatterPrint = DateFormatter()
       dateFormatterPrint.locale = Locale.init(identifier: Utility.getLanguage().Code)// for 24 hours format
           dateFormatterPrint.timeZone = Helper.getTimeZoneFromPickUpLoaction()
      dateFormatterPrint.dateFormat = "dd-MMM ,HH:mm"
      
      let dateFormattercurrentDate = DateFormatter()
          dateFormattercurrentDate.locale = Locale.init(identifier: Utility.getLanguage().Code)// for 24 hours format
              dateFormattercurrentDate.timeZone = Helper.getTimeZoneFromPickUpLoaction()
            dateFormattercurrentDate.dateFormat = "h:mm a"
        dateFormattercurrentDate.amSymbol = "AM"
        dateFormattercurrentDate.pmSymbol = "PM"
        
      if let date = dateFormatterGet.date(from: dateStr) ,date ==  NSDate() as Date  {
          print(dateFormatterPrint.string(from: date))
          return dateFormattercurrentDate.string(from: date)
      } else {
          return dateFormattercurrentDate.string(from: dateFormatterGet.date(from: dateStr)!)
      }
      }
    
    

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}


extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

