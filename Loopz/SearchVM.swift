//
//  SearchVM.swift
//  UFly
//
//  Created by 3 Embed on 28/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchVM {
    
    var searchFrom = 0
    var productName = ""
    ///constants
    let filterInfo = PublishSubject<(Bool,String,[String])>()
    var loadPopular = false
    var isClearAll = false
    let disposeBag = DisposeBag()
    let searchVM_response = PublishSubject<ResponseType>()
    enum ResponseType:Int {
        case Store = 1
        case ItemName = 2
        case Items = 3
        case Recent = 4
    }
    var brandArray:[String] = []
    var manufacturerArray:[String] = []
    //variables
    var arrayOfStores = [Store]()
    var arrayOfItems = [String]()
    var arrayOfExpandedStore = Store.init(data: [:])
    var isFromItemList = false
    var recentSearchList = Utility.getRecentSearch()
    var selectedCategory = ""
    var selectedSubCategory = ""
    var selectedSubSubCategory = ""
    var selectedSort = -1
    var offer = ""
    var minPrice:Float = 0
    var maxPrice:Float = 0
    
    var resultfromFilter = false
    
    /// observable for search API
    ///
    /// - Parameters:
    ///   - text: is of type string entered on search bar
    ///   - data: is of type int 0 for products , 1- store,2 - store and products
    func getProduct(text:String,language:String) {
        
        if Utility.getSelectedSuperStores().type == .Restaurant{
            SearchAPICalls.popularSearchFilter(needle: text, language: language).subscribe(onNext: { [weak self]success in
                self?.arrayOfItems = success.0
                self?.arrayOfStores = success.1
                self?.searchVM_response.onNext(ResponseType.ItemName)
                }, onError: { (error) in
                    print(error)
            }).disposed(by: disposeBag)
        }else{
            getStores(text: text)
            SearchAPICalls.searchProductName(needle: text,language: language).subscribe(onNext: {[weak self]success in
                self?.arrayOfItems  = success
                self?.searchVM_response.onNext(ResponseType.ItemName)
                }, onError: {error in
                    print(error)
            }).disposed(by: disposeBag)
        }
    }
    
    
    func getPopular() {
        SearchAPICalls.searchProductPopular().subscribe(onNext: {[weak self]success in
            self?.recentSearchList  = success
            self?.searchVM_response.onNext(ResponseType.Recent)
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    func getStores(text:String) {
        //        SearchAPICalls.searchStore(needle: text).subscribe(onNext: {[weak self]success in
        //            self?.arrayOfStores  = success
        //            self?.searchVM_response.onNext(ResponseType.Store)
        //            }, onError: {error in
        //                print(error)
        //        }).disposed(by: disposeBag)
    }
    
    func getProductsWithinStore(text:String,language:String) {
        //        getStores(text: text)
        
        SearchAPICalls.searchProduct(needle: text,productName: productName,brand:brandArray,manufacturer:manufacturerArray,category:selectedCategory,subcat:selectedSubCategory,subsubCat:selectedSubSubCategory,minPrice: minPrice,maxPrice: maxPrice,offer:offer,sort:selectedSort,searchFrom: searchFrom,language: language, isStoreSearch: self.isFromItemList).subscribe(onNext: {[weak self]success in
            self?.arrayOfExpandedStore  = success.0
            self?.arrayOfStores = success.1
            self?.searchVM_response.onNext(ResponseType.Items)
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    
    func filterProductsWithinStore(text:String,language:String) {
        SearchAPICalls.filterProducts(needle: text,productName: productName,brand:brandArray,manufacturer:manufacturerArray,category:selectedCategory,subcat:selectedSubCategory,subsubCat:selectedSubSubCategory,minPrice: minPrice,maxPrice: maxPrice,offer:offer,sort:selectedSort,searchFrom: searchFrom,language: language, isStoreSearch: self.isFromItemList).subscribe(onNext: {[weak self]success in
            
            self?.arrayOfExpandedStore  = success.0
            self?.arrayOfStores = success.1
            self?.searchVM_response.onNext(ResponseType.Items)
            self?.resultfromFilter = true
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    
    

}
