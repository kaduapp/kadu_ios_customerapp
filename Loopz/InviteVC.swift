//
//  InviteVC.swift
//  UFly
//
//  Created by Rahul Sharma on 14/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class InviteVC: UIViewController {
    
    var inviteVM = InviteVM()
    
    @IBOutlet var inviteView: InviteView!
    var navView = NavigationView().shared
    override func viewDidLoad() {
        super.viewDidLoad()
// Do any additional setup after loading the view.
        setUI()
         didGeteferralCode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUI()
       inviteVM.getReferralCode()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Helper.nonTransparentNavigation(controller: self.navigationController!)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.tintColor =  Colors.PrimaryText
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:  Colors.PrimaryText]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func inviteFreindsAction(_ sender: Any) {
        
        DynamicLinking.getDynamicLink(itemId:"").subscribe(onNext: { (success) in
                         let url = DynamicLinkingConstants.DynamicDomain + "?link=" + DynamicLinkingConstants.AppDomain + "/" + "referCode:" + "\(Utility.getReferralCode())" + "/11" + "&apn=" + DynamicLinkingConstants.AndroidBundleId // "https://rinn.page.link/?link=https://www.rinn.com/" +  "referCode:" + "\(Utility.getReferralCode())" + "/11" + "&apn=" + "com.customer.rinnapp"
            let activityViewController = UIActivityViewController(
                activityItems: [AppConstants.ShareText() + "\n" + url],
                applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        }).disposed(by: self.inviteVM.disposeBag)
        
    }
        
}
extension InviteVC {
    
    /// initial viewsetup
    func setUI(){
       // self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:  Colors.SecondBaseColor]
        if let navBar = self.navigationController{
            Helper.transparentNavigation(controller: navBar)
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: UIConstants.PageTitles.Referrals)
            navView.navView.backgroundColor = UIColor.clear
            navView.backgroundColor = UIColor.clear
            navView.titleLabel.textColor = Colors.SecondBaseColor
            navBar.navigationBar.tintColor =  Colors.SecondBaseColor
        }
    }
    
    
    func didGeteferralCode(){
        inviteVM.invite_response.subscribe(onNext: { success in
            self.inviteView.updateReferralCode()
        }).disposed(by: self.inviteVM.disposeBag)
    }
    
}
