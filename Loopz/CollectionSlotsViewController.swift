//
//  CollectionSlotsViewController.swift
//  DelivX
//
//  Created by 3EMBED on 22/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
 
protocol CollectionSlotsDelegate: class  {
    func returnSelectedSlotDateandTime(_ Date:String? ,_ time:String?  ,_ slotID:String?,_ dateID: String?,slotType:Int?)
}

class CollectionSlotsViewController: UIViewController {

    @IBOutlet weak var datesTableView: UITableView!
    @IBOutlet weak var slotsTableView: UITableView!

    @IBOutlet weak var ModalViewTitleLabel: UILabel! 
    let viewModel = CollectionSlotsViewModel()
    var slotsType:Int = 0
    var laundryType:Int = 0
    var disposeBag = DisposeBag()
    var delegate:CollectionSlotsDelegate? = nil
    var pickedSlotID:String? = nil
    var pickedDateID:String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        addSubscriberForReloadTableView()
        viewModel.callSlots(Type: slotsType, with: laundryType)
        ModalViewTitleLabel.text = viewModel.modalSheetTitle
        Helper.showPI(string: "")
       addLineOnTop(tableview: datesTableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addSubscriberForReloadTableView()
    }
    
 // MARK:- Method to dismiss sheet
    @IBAction func dismissSheet(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK:- RxVariable Methods
    
    func addSubscriberForReloadTableView() {
        if !viewModel.rx_ReloadTableView.hasObservers {
            viewModel.rx_ReloadTableView.subscribe(onNext: { (isReload) in
                if isReload {
                    // Table Reload task
                    DispatchQueue.main.async {
                        self.datesTableView.reloadData()
                        self.slotsTableView.reloadData()
                        self.datesTableView.separatorColor = .clear
                        self.slotsTableView.separatorColor = .clear
                        if let  pickedSlotID = self.pickedSlotID , let dateid = self.pickedDateID
                        {
                            print(pickedSlotID)
                            self.viewModel.slotId = pickedSlotID
                            self.viewModel.pickeddateId = dateid
                           // print("cell selected \(self.viewModel.getIndexofDateID(dateid: dateid))")
                            self.selectTablecellViewsatselectedIndexFor(datesTableView: self.datesTableView, withrow: self.viewModel.getIndexofDateID(dateid: dateid))
                       // self.selectTablecellViewsatFirstIndexFor(datesTableView: self.datesTableView,slotsTableView: self.slotsTableView)
                        }else{
                              self.selectTablecellViewsatFirstIndexFor(datesTableView: self.datesTableView,slotsTableView: self.slotsTableView)
                        }
                        Helper.hidePI()
                    }
                }
            }, onError: { (error) in
                print(error.localizedDescription)
                 Helper.hidePI()
            }, onCompleted: {
                print("rx_ReloadTableView completed")
                 Helper.hidePI()
            }, onDisposed: {
                print("rx_ReloadTableView Disposed")
            }).disposed(by: disposeBag)
        }
    }
    //MARK:- TableviewCell Selected for firstTime
    func selectTablecellViewsatFirstIndexFor( datesTableView: UITableView ,slotsTableView: UITableView )
    {
        let indexPath = IndexPath(row: 0, section: 0)
        if viewModel.getPickUpSlotsCount() > 0 {
            datesTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none )
            tableView(datesTableView, didSelectRowAt: indexPath)
        }
        if viewModel.getSlotsCount() > 0 {
            slotsTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            tableView(slotsTableView, didSelectRowAt: indexPath)
            
            
        }
    }
    //MARK:- TableviewCell Selected on user selection
    func selectTablecellViewsatselectedIndexFor( datesTableView: UITableView ,withrow:Int  )
    {
       
        if viewModel.getPickUpSlotsCount() > 0 {
            datesTableView.selectRow(at: IndexPath(row:withrow, section: 0), animated: true, scrollPosition: .none )
            tableView(datesTableView, didSelectRowAt: IndexPath(row:withrow, section: 0))
            self.slotsTableView.reloadData()
           // self.viewModel.slotId = nil
        }
        
//        if viewModel.getSlotsCount() > 0 {
//             let indexPath = IndexPath(row: 0, section: 0)
//            slotsTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
//            tableView(slotsTableView, didSelectRowAt: indexPath)
//            
//            
//        }
    }
    // MARK: Apply Button Action
    @IBAction func ApplyButtonTapped(_ sender: Any) {
        if let selectedDate = viewModel.selectDate, let selectedSlot = viewModel.selectSlot,let  id = viewModel.slotId ,let selecteddateID = viewModel.pickeddateId ,  let _ = viewModel.slots.first(where: { $0._id == id }) 
        {
            
            delegate?.returnSelectedSlotDateandTime(selectedDate, selectedSlot,id,selecteddateID,slotType: slotsType)
            self.dismissSheet(sender)
        }else
        {
              Helper.showAlert(message: "Please select slot to Apply", head: "", type: -1)
        }
    }
     // MARK: TableViews
    func addLineOnTop(tableview:UITableView)
    {
        let border = CALayer()
        let red = Colors.HeaderBackground//UIColor(red: 137.0/255.0, green: 142.0/255.0, blue: 170.0/255.0, alpha: 1.0)
        border.backgroundColor = red.cgColor
        border.frame = CGRect(x: tableview.frame.width-1, y: 0, width: 1.0, height: tableview.frame.height)
        tableview.layer.addSublayer(border)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

