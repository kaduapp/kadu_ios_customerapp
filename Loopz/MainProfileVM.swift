//
//  MainProfileVM.swift
//  UFly
//
//  Created by 3 Embed on 01/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MainProfileVM: NSObject {
    
    let disposeBag = DisposeBag()
    static let MainProfileVM_response = PublishSubject<Bool>()
    var userData = Utility.getProfileData()
    var profileModel = ProfileModelLibrary.menuList
    var showPayment = false
    
    /// this method make subscripton to get profile API defined in EditProfileAPICalls
    // and observe for that response
    func getProfile() {
        ProfileAPICalls.getProfile().subscribe(onNext: { success in
            if success {
                self.userData = Utility.getProfileData()
                MainProfileVM.MainProfileVM_response.onNext(true)
            }
        } , onError: { error in
            print(error)
        }).disposed(by: disposeBag)
    }
    
    /// observe logout for logout
    func logout() {
        AuthenticationAPICalls.logoutAPI().subscribe(onNext: { result in
            EditProfileVM.EditProfileVM_response.onNext(.Logout)
        } , onError: { error in
            print(error)
        }).disposed(by: disposeBag)
    }    
}
