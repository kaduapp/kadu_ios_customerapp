//
//  ChangePWTF.swift
//  DelivX
//
//  Created by 3 Embed on 08/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension ChangePasswordVC: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case changePasswordView.oldPasswordTF:
            changePasswordView.oldPWSeperartor.backgroundColor =  Colors.SeparatorLight
            changePasswordView.newPWSeperartor.backgroundColor =  Colors.AppBaseColor
            changePasswordView.newPasswordTF.becomeFirstResponder()
            break
        case changePasswordView.newPasswordTF:
            changePasswordView.newPWSeperartor.backgroundColor =  Colors.SeparatorLight
            changePasswordView.reEnterPwTickMark.backgroundColor =  Colors.AppBaseColor
            changePasswordView.reEnterTF.becomeFirstResponder()
            break
        case changePasswordView.reEnterTF:
            changePasswordView.reEnterPwTickMark.backgroundColor =  Colors.SeparatorLight
            changePasswordView.reEnterTF.resignFirstResponder()
            break
        default:
            break
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    
}
