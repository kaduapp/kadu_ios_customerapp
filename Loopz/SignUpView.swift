//
//  SignUpView.swift
//  UFly
//
//  Created by Rahul Sharma on 19/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SignUpView: UIView {
    
    @IBOutlet weak var signUpScrollView: UIScrollView!
    @IBOutlet weak var signUpButtonView: UIView!
    @IBOutlet weak var contentScrollView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var checkMark: UIButton!
    
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var referralTF: UITextField!
    
    @IBOutlet weak var signupButton:UIButton!
    @IBOutlet weak var nextBtnTitle: UILabel!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var signUpTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBOutlet weak var refferalTitle: UILabel!
    @IBOutlet weak var termsAndC: UITextView!
    
    @IBOutlet weak var phNumCheckMark: UIButton!
    @IBOutlet weak var nameCheckMark: UIButton!
    @IBOutlet weak var emailCheckMark: UIButton!
    @IBOutlet weak var pwCheckMark: UIButton!
    @IBOutlet weak var refCheckMark: UIButton!
    
    @IBOutlet weak var phNumSeparator: UIView!
    @IBOutlet weak var nameSeparator: UIView!
    @IBOutlet weak var emailSeparator: UIView!
    @IBOutlet weak var pwSeparator: UIView!
    @IBOutlet weak var btnSeparator: UIView!
    @IBOutlet weak var refSeparator: UIView!
    
    @IBOutlet weak var countryCodeBtn: UIButton!
    @IBOutlet weak var refHight: NSLayoutConstraint!
    
    var activeTextField: UITextField!
    
    var phoneNum:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewSetup()
        termsAndC.Semantic()
        mobileNumberTF.Semantic()
        nameTF.Semantic()
        emailTF.Semantic()
        passwordTF.Semantic()
        referralTF.Semantic()
    }
    
    /// initial view setup
    func viewSetup(){
        refHight.constant = 0
        self.updateConstraints()
        self.layoutIfNeeded()
        
        Helper.addDoneButtonOnTextField(tf: mobileNumberTF, vc: self)
        
        Helper.setButtonAndView(button: signupButton,view:signUpButtonView, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.signUp(), highlighted: StringConstants.signUp(), selected: StringConstants.signUp(), button: signupButton)
        
        Fonts.setPrimaryMedium(signupButton)
        Fonts.setPrimaryMedium(nextBtnTitle)
        Fonts.setPrimaryMedium(mobileNumberTF)
        Fonts.setPrimaryMedium(emailTF)
        Fonts.setPrimaryMedium(nameTF)
        Fonts.setPrimaryMedium(passwordTF)
        Fonts.setPrimaryBold(signUpTitle)
        Fonts.setPrimaryMedium(nextBtnTitle)
        Fonts.setPrimaryRegular(refferalTitle)
        Fonts.setPrimaryRegular(termsAndC)
        Fonts.setPrimaryRegular(subTitle)
        Fonts.setPrimaryRegular(referralTF)
        
        phNumCheckMark.tintColor  =  Colors.AppBaseColor
        nameCheckMark.tintColor  =  Colors.AppBaseColor
        emailCheckMark.tintColor  =  Colors.AppBaseColor
        pwCheckMark.tintColor  =  Colors.AppBaseColor
        refCheckMark.tintColor  =  Colors.AppBaseColor
        
        phNumSeparator.backgroundColor =  Colors.SeparatorLight
        nameSeparator.backgroundColor  =  Colors.SeparatorLight
        emailSeparator.backgroundColor =  Colors.SeparatorLight
        pwSeparator.backgroundColor    =  Colors.SeparatorLight
        
        mobileNumberTF.textColor  =  Colors.PrimaryText
        emailTF.textColor         =  Colors.PrimaryText
        nameTF.textColor          =  Colors.PrimaryText
        passwordTF.textColor      =  Colors.PrimaryText
        referralTF.textColor      =  Colors.PrimaryText
        
        mobileNumberTF.placeholder = StringConstants.PhoneNumber()
        emailTF.placeholder        = StringConstants.Email()
        nameTF.placeholder         = StringConstants.Name()
        passwordTF.placeholder     = StringConstants.Password()
        referralTF.placeholder     = StringConstants.Refferal()

        signUpTitle.text           = StringConstants.signUp()
        subTitle.text              = String(format:StringConstants.CreateAnAcc(),Utility.AppName)
        refferalTitle.text         = StringConstants.RefferalCode()
        
        termsAndC.textColor        =  Colors.PrimaryText

        signUpTitle.textColor =  Colors.PrimaryText
        subTitle.textColor   =  Colors.SeconderyText
        termsAndC.text = StringConstants.Terms()
        Helper.updateText(text: termsAndC.text!, subText: StringConstants.TAndC(), termsAndC, color: Colors.AppBaseColor,link: AppConstants.TermsAndCLink)
        
        
        if mobileNumberTF.text?.sorted().count != 0 {
            if Helper.isValidPhoneNumber(text: mobileNumberTF.text!){
                phNumCheckMark.isSelected = true
            }
        }
        else {
            phNumCheckMark.isSelected = false
        }
    }
}
