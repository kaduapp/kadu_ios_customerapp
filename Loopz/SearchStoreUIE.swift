//
//  SearchStoreUIE.swift
//  DelivX
//
//  Created by 3EMBED on 14/11/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//


import UIKit

extension SearchStoreVC{
    
    func setBanner() {
        dummyView.addSubview(Helper.showUnVerifyNotification().0)
        dummyView.clipsToBounds = true
        if Helper.showUnVerifyNotification().1 {
            bannerHeight.constant = 20
        }else{
            bannerHeight.constant = 0
        }
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
    
    func setEmptyScreen() {
        restuarantEmptyView.setData(image: #imageLiteral(resourceName: "EmptyStore"), title: StringConstants.No() + " " + Utility.getSelectedSuperStores().typeName + " " + StringConstants.Found(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
    }
    
    @objc func StartShopping() {
        Helper.changetTabTo(index: AppConstants.Tabs.Home)
    }
    
    /// sets navigation
    func setNavigation(){
        
        Helper.transparentNavigation(controller: self.navigationController!)
        self.navigationController?.navigationBar.backgroundColor = Colors.SecondBaseColor
        if let navBarCount = self.navigationController?.viewControllers.count {
            if navBarCount > 1{
                NavigationWidth.constant = UIScreen.main.bounds.size.width - 60
            }else{
                NavigationWidth.constant = UIScreen.main.bounds.size.width - 20
            }
        }
        
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        self.searchTF.addTarget(self, action: #selector(SearchVC.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
          self.leftBarButton()
        //Helper.setShadow(sender: dispensariesProductsView)
    }
    
    func setupUI() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        restuarantTableView.rowHeight = UITableView.automaticDimension
        restuarantTableView.backgroundColor = Colors.ScreenBackground
        Helper.setUiElementBorderWithCorner(element: searchView, radius: 7, borderWidth: 0, color: UIColor.white)
        cancelButton.setTitle(StringConstants.Cancel(), for: .normal)
        Fonts.setPrimaryMedium(cancelButton)
        self.cancelBtnTrailingConstaraint.constant = 5
        self.searchHeaderView.setNeedsUpdateConstraints()
        self.searchHeaderView.layoutIfNeeded()
    }
    
    func leftBarButton() {
            let item = UIBarButtonItem(image: #imageLiteral(resourceName: "BackIcon"), style: .plain, target: self, action: #selector(closeAction(_:)))
            item.imageInsets = UIEdgeInsets.init(top: 0, left: -4, bottom: 0, right: -4)
            self.navigationItem.setLeftBarButtonItems([item], animated: true)
    }
    
    @objc func closeAction(_ sender: UIButton) {
         self.view.endEditing(true)
         self.navigationController?.popViewController(animated: true)
    }
}
