//
//  PopupVM.swift
//  DelivX
//
//  Created by 3Embed on 05/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PopupVM: NSObject {

    var arrayOfList = [Any]()
    var item:Item = Item.init(data: CartItem.init(data: [:]))
    
    enum TypeOfService:Int {
        
        case Reason     = 1
        case ItemDetail = 2
    }
    
    var type                    = 0// selected type from the controller eg: payment
    var radio                   = true// single selection
    var selected:[Any]          = [0]// selected options
    let popupVM_response        = PublishSubject<[Any]>()
    let popupVM_responseLocal   = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    func addToWishList() {
        var arrayDynamic:[PopupModel] = []
        for data in arrayOfList {
            var dataIn = data as! PopupModel
            let array = selected as! [String]
            if array.contains(dataIn.ID) {
                dataIn.Flag = true
            }
            arrayDynamic.append(dataIn)
        }
        FavouriteAPICalls.addItemToWishLists(list: arrayDynamic, item: item).subscribe(onNext: {[weak self]success in
            if success == true {
                self?.popupVM_responseLocal.onNext(true)
            }else{
                
            }
        }, onError: {error in
            print(error)
        }).disposed(by: self.disposeBag)
    }
}
