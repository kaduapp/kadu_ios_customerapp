//
//  ImageVC.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ImageVC: UIViewController{
    
    
    @IBOutlet weak var bottomImageCollectionView: UICollectionView!
    @IBOutlet weak var mainCollectionview: UICollectionView!
    
    var arrayImages:[String] = []
    var selected = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let indexPath = IndexPath(item: selected, section: 0)
        self.mainCollectionview.scrollToItem(at: indexPath, at: .left, animated: true)
        let cell = self.bottomImageCollectionView.cellForItem(at: indexPath) as! ImageSmallCell
        cell.ImageOuterView.layer.borderColor = Colors.AppBaseColor.cgColor
        cell.ImageOuterView.layer.borderWidth = 1
    }
    
    @IBAction func closeScreen(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
