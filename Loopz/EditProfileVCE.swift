//
//  EditProfileVCE.swift
//  UFly
//
//  Created by 3 Embed on 11/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - VNHCountryPickerDelegate
extension EditProfileVC: VNHCountryPickerDelegate {
    func didPick(country: VNHCounty) {
        editProfileVIew.phoneNum.text = country.dialCode
        editProfileVM.authentication.CountryCode = country.dialCode
    }
}

extension EditProfileVC {
    
    /// dismiss Keyboard
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

}

// MARK: - UIScrollViewDelegate
extension EditProfileVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ sender: UIScrollView) {
        let movedOffset: CGFloat = sender.contentOffset.y
        if sender == editProfileVIew.scrollView {
            if movedOffset <= 0 {
                sender.contentOffset.y = 0
            }
        }
    }
}
