//
//  WalletHistoryVC.swift
//  DelivX
//
//  Created by 3Embed on 13/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class WalletHistoryVC: UIViewController {

    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var contentWidth: NSLayoutConstraint!
    @IBOutlet weak var debitTableView: UITableView!
    @IBOutlet weak var creditOrderTableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContent: UIView!
    @IBOutlet weak var debitOrderButton: UIButton!
    @IBOutlet weak var creditOrderButton: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var indicator: UIView!
    @IBOutlet weak var distance: NSLayoutConstraint!
    var historyVM = WalletHistoryVM()
    var emptyViewDebit = EmptyView().shared
    var emptyViewCredit = EmptyView().shared
    var navView = NavigationView().shared

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.Wallet())
        }
        setEmptyScreen()
        updateView()
        setVM()
        historyVM.getHistory()
        contentWidth.constant = 2 * UIScreen.main.bounds.size.width
        // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController != nil{
            Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Utility.getLanguage().Code == "ar" {
            scrollToPage(page: 1)
        } else {
            scrollToPage(page: 0)
        }
    }
    
    @IBAction func didTapDebits(_ sender: Any) {
        
        if Utility.getLanguage().Code == "ar" {
            scrollToPage(page: 1)
        } else {
            scrollToPage(page: 0)
        }
    }
    
    @IBAction func didTapCredits(_ sender: Any) {
        
        if Utility.getLanguage().Code != "ar" {
            scrollToPage(page: 1)
        } else {
            scrollToPage(page: 0)
        }
    }
}
