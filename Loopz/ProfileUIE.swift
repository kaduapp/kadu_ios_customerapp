//
//  ProfileUIE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension ProfileVC {
    
    
    func setNavigation(){
        self.navigationController?.isNavigationBarHidden = true
        profileTableView.backgroundColor = Colors.ScreenBackground
        
        if self.navigationController != nil && !Changebles.isGrocerOnly{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.ProfileTitle())
        }
        else{
        Fonts.setPrimaryBold(titleLabel)
        titleLabel.textColor = Colors.PrimaryText
        titleLabel.text = "Profile"
        }
        settop()
            
    }
}

extension ProfileVC:UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        settop()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        settop()
    }
    
    func settop() {
        UIView.animate(withDuration: 0.5) {
            if self.profileTableView.contentOffset.y > 0 {
                self.titleHeight.constant = 44
                Helper.setShadow(sender: self.titleView)
            }else{
                self.titleHeight.constant = 0
                Helper.removeShadow(sender: self.titleView)
            }
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
    }
}
