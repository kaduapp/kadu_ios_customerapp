//
//  OTPVC.swift
//  UFly
//
//  Created by Rahul Sharma on 14/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class OTPVC: UIViewController {
    //outlets
    @IBOutlet weak var navigationBackView: UIView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var otpView: OTPView!
    
    //variables
    var otpVM = OTPVM()
    var editProfileVM = EditProfileVM ()
    var signupVM    = SignupVM()
    
    var str = ""
    var oldLength = 0
    var replacementLength = 0
    var oneTouchOTP = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateView()
        didGetServiceResponse()
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Helper.transparentNavigation(controller: self.navigationController!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addObserver()
        if Changebles.showDefaultOTP{
            self.showDefaultOTP()
        }
            else{
            otpView.textFieldOtp0.becomeFirstResponder()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        Helper.nonTransparentNavigation(controller: self.navigationController!)
        self.removeObserver()
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.beforeDismiss()
    }
    
    @IBAction func didTapVerifyButton(_ sender: Any) {
        var data = otpView.textFieldOtp0.text! + otpView.textFieldOtp1.text! + otpView.textFieldOtp2.text!
        data = data + otpView.textFieldOtp3.text! + otpView.textFieldOtp4.text!
        let textOTP = data + otpView.textFieldOtp5.text!
        
        if otpView.checkForData() {
            otpVM.verify(otp:textOTP)
        }
        
    }
    
    @IBAction func didTapResendCode(_ sender: Any){
        otpView.clearOtp()
        oneTouchOTP = ""
        otpVM.getOTP(show: true)
        self.dismissKeyboard()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.OtpToEnterNewPW {
            let updatePWVC:UpdatePasswordVC = segue.destination as! UpdatePasswordVC
            updatePWVC.updatePasswordVM.authentication = self.otpVM.authentication!
        }
    }
    
}
extension OTPVC {
    
    /// this method  will be having subscription to updateProfile, get Otp,updatePW
    //observe EditProfile View Model and OTPVM
    func didGetServiceResponse(){
        
        otpVM.otpVM_response.subscribe(onNext: {[weak self]success in
            self?.subscribe(data:success)
            }, onError: {error in
                print(error)
        }).disposed(by: otpVM.disposeBag)
        
        EditProfileVM.EditProfileVM_response.subscribe(onNext: { [weak self]success in
            if success == .updateProfile {
                self?.navigationController?.popViewController(animated: true)
            }
            
            }, onError: {error in
                print(error)
        }).disposed(by: otpVM.disposeBag)
        
        signupVM.signupVM_response.subscribe(onNext: { [weak self]success in
            switch success {
            case .Signup:
                self?.otpVM.signIn()
                break
            default:
                break
            }
        }).disposed(by: otpVM.disposeBag)
    }
    
    /// updates the View(initial setup)
    
    
}
