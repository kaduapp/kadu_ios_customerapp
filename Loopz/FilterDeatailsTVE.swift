//
//  File.swift
//  DelivX
//
//  Created by 3Embed on 19/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import SwiftRangeSlider

extension FilterDetailsVC : UITableViewDelegate,UITableViewDataSource {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if filterDetailVM.type == APIRequestParams.CategoryFilter || filterDetailVM.type == APIRequestParams.SubCategoryFilter || filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
            if filterDetailVM.arrayOfCategory.count > 0 {
                return filterDetailVM.arrayOfCategory.count
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let head = Helper.showHeader(title: filterDetailVM.arrayOfCategory[section].Name, showViewAll: true) as! HeaderView
        head.fullButton.isHidden = false
        head.fullButton.tag = section
        
        head.viewAllArrow.setImage(nil, for: .normal)
        head.headerTitle.textColor = Colors.HeaderTitle
        if filterDetailVM.selectedCategory == filterDetailVM.arrayOfCategory[section].Name {
            head.headerTitle.textColor = Colors.AppBaseColor
            if filterDetailVM.SubCatSelected == "" {
                head.viewAllArrow.setImage(#imageLiteral(resourceName: "CheckIcon"), for: .normal)
            }
        }
        head.viewAllText.setTitle("", for: .normal)
        head.fullButton.addTarget(self, action:#selector(updateSubCat) , for: UIControl.Event.touchUpInside)
        head.backgroundColor = UIColor.white
        return head
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if filterDetailVM.type == APIRequestParams.CategoryFilter || filterDetailVM.type == APIRequestParams.SubCategoryFilter || filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
            if filterDetailVM.arrayOfCategory.count > 0 {
                return 50
            }
        }
        return 0
    }
    @objc func updateSubCat(sender:UIButton) {
        if filterDetailVM.selectedSection == sender.tag {
            filterDetailVM.selectedSection = -1
            filterDetailVM.SubCatSelected = ""
            filterDetailVM.selectedCategory = ""
        }else{
            filterDetailVM.selectedCategory = filterDetailVM.arrayOfCategory[sender.tag].Name
            filterDetailVM.SubCatSelected = ""
            filterDetailVM.selectedSection = sender.tag
            if filterDetailVM.arrayOfCategory[sender.tag].SubCategories.count == 0 {
                filterDetailVM.type = APIRequestParams.SubCategoryFilter
                filterDetailVM.chosenCat = filterDetailVM.selectedCategory
                filterDetailVM.getFilter()
            }
        }
        searchVMReference.filterInfo.onNext((true,filterDetailVM.type,[filterDetailVM.selectedCategory,filterDetailVM.SubCatSelected,filterDetailVM.SubSubCatSelected]))
        mainTableView.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterDetailVM.type == APIRequestParams.FilterPrice {
            return 1
        }else if filterDetailVM.type == APIRequestParams.CategoryFilter || filterDetailVM.type == APIRequestParams.SubCategoryFilter || filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
            if filterDetailVM.arrayOfCategory.count > 0 {
                if filterDetailVM.selectedSection == section {
                    return filterDetailVM.arrayOfCategory[section].SubCategories.count
                }else{
                    return 0
                }
            }
        }
        return filterDetailVM.arrayOfTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if filterDetailVM.type == APIRequestParams.FilterPrice {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PriceRangeTVC.self), for: indexPath) as! PriceRangeTVC
            if filterDetailVM.maxValue > 0 {
                cell.rangeBar.minimumValue = Double(filterDetailVM.minValue)
                cell.rangeBar.maximumValue = Double(filterDetailVM.maxValue)
            }else{
                cell.rangeBar.minimumValue = Double(filterDetailVM.minValue)
                cell.rangeBar.maximumValue = Double(filterDetailVM.selectedMaxValue)
            }
            var max = filterDetailVM.selectedMaxValue
            if filterDetailVM.selectedMaxValue <= 0 {
                max = filterDetailVM.maxValue
            }
            cell.setData(min: filterDetailVM.selectedMinValue, max: max)
            cell.rangeBar.upperValue = Double(max)
            cell.rangeBar.lowerValue = Double(filterDetailVM.selectedMinValue)
            cell.rangeBar.delegate = self
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FilterTVC.self), for: indexPath) as! FilterTVC
        cell.accessoryType = .none
        if filterDetailVM.type == APIRequestParams.CategoryFilter || filterDetailVM.type == APIRequestParams.SubCategoryFilter || filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
            cell.titleLabel.text = filterDetailVM.arrayOfCategory[indexPath.section].SubCategories[indexPath.row].Name
            if filterDetailVM.SubCatSelected == filterDetailVM.arrayOfCategory[indexPath.section].SubCategories[indexPath.row].Name {
                cell.accessoryType = .checkmark
            }
        }else{
            if filterDetailVM.arrayOfTypesSelected.contains(filterDetailVM.arrayOfTypes[indexPath.row]) {
                cell.accessoryType = .checkmark
            }else if filterDetailVM.type == StringConstants.Rating() || filterDetailVM.type == StringConstants.CostForTwo() || filterDetailVM.type == StringConstants.FoodType() || filterDetailVM.type == StringConstants.Cousines() {
                switch filterDetailVM.type {
                case StringConstants.Rating():
                    if filterDetailVM.selectedRating == indexPath.row + 1 {
                        cell.accessoryType = .checkmark
                    }
                    break
                case StringConstants.Cousines():
                    if filterDetailVM.selectedCosine == filterDetailVM.arrayOfTypes[indexPath.row] {
                        cell.accessoryType = .checkmark
                    }
                    break
                case StringConstants.FoodType():
                    if filterDetailVM.selectedFoodType == filterDetailVM.arrayOfTypes[indexPath.row] {
                        cell.accessoryType = .checkmark
                    }
                    break
                default:
                    break
                }
            }
            
            cell.titleLabel.text = filterDetailVM.arrayOfTypes[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if filterDetailVM.type == APIRequestParams.FilterPrice {
            return
        }
        if filterDetailVM.type == APIRequestParams.SubCategoryFilter {
            if filterDetailVM.SubCatSelected == filterDetailVM.arrayOfCategory[indexPath.section].SubCategories[indexPath.row].Name {
                filterDetailVM.SubCatSelected = ""
            }else{
                filterDetailVM.SubCatSelected = filterDetailVM.arrayOfCategory[indexPath.section].SubCategories[indexPath.row].Name
            }
            if filterDetailVM.SubCatSelected.length > 0 {
                if filterDetailVM.arrayOfCategory[indexPath.section].SubCategories[indexPath.row].SubSubCat.count == 0 {
                    filterDetailVM.chosenSubCat = filterDetailVM.SubCatSelected
                    filterDetailVM.type = APIRequestParams.SubSubCategoryFilter
                    filterDetailVM.getFilter()
                }else{
                    self.performSegue(withIdentifier: String(describing: FilterSubCatVC.self), sender: self)
                }
            }else{
                filterDetailVM.type = APIRequestParams.SubCategoryFilter
                filterDetailVM.getFilter()
            }
            searchVMReference.filterInfo.onNext((true,APIRequestParams.CategoryFilter,[filterDetailVM.selectedCategory,filterDetailVM.SubCatSelected,""]))
            tableView.reloadData()
        }else if filterDetailVM.type == StringConstants.Rating() || filterDetailVM.type == StringConstants.CostForTwo() || filterDetailVM.type == StringConstants.FoodType() || filterDetailVM.type == StringConstants.Cousines() {
            switch filterDetailVM.type {
            case StringConstants.Rating():
                if filterDetailVM.selectedRating == indexPath.row + 1 {
                    filterDetailVM.selectedRating = 0
                }else{
                    filterDetailVM.selectedRating = indexPath.row + 1
                }
                break
            case StringConstants.Cousines():
                if filterDetailVM.selectedCosine == filterDetailVM.arrayOfTypes[indexPath.row] {
                    filterDetailVM.selectedCosine = ""
                }else{
                    filterDetailVM.selectedCosine = filterDetailVM.arrayOfTypes[indexPath.row]
                }
                break
            case StringConstants.FoodType():
                if filterDetailVM.selectedFoodType == filterDetailVM.arrayOfTypes[indexPath.row] {
                    filterDetailVM.selectedFoodType = ""
                }else{
                    filterDetailVM.selectedFoodType = filterDetailVM.arrayOfTypes[indexPath.row]
                }
                break
            default:
                break
            }
            searchVMReference.filterInfo.onNext((true,filterDetailVM.type,["\(filterDetailVM.selectedRating)",filterDetailVM.selectedFoodType,filterDetailVM.selectedCosine]))
            tableView.reloadData()
        }else{
            if indexPath.section == 0 {
                if filterDetailVM.arrayOfTypesSelected.contains(filterDetailVM.arrayOfTypes[indexPath.row]) {
                    filterDetailVM.arrayOfTypesSelected.remove(at: filterDetailVM.arrayOfTypesSelected.index(of: filterDetailVM.arrayOfTypes[indexPath.row])!)
                    tableView.reloadRows(at: [indexPath], with: .none)
                }else{
                    filterDetailVM.arrayOfTypesSelected.append(filterDetailVM.arrayOfTypes[indexPath.row])
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
                searchVMReference.filterInfo.onNext((true,filterDetailVM.type,filterDetailVM.arrayOfTypesSelected))
            }
        }
    }
    
}
 
extension FilterDetailsVC:SliderDelegate {
    func slider(_ rangeSlider: RangeSlider, didUpdate ratingMin: Float, ratingMax: Float) {
        let cell = mainTableView.cellForRow(at: IndexPath.init(item: 0, section: 0)) as! PriceRangeTVC
        cell.setData(min: ratingMin, max: ratingMax)
        searchVMReference.filterInfo.onNext((true,filterDetailVM.type,["\(ratingMin)","\(ratingMax)"]))
    }
    
    
}
