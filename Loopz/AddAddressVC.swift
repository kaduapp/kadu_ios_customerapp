//
//  LocationSelectVC.swift
//  Rogi
//
//  Created by NABEEL on 15/06/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import RxCocoa
import RxSwift
protocol AddressDelegate {
    func AddressDelegate(location:LocationManager)
}
class AddAddressVC: UIViewController {
    
    @IBOutlet weak var mylocationButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var mapMarker: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressTF: HoshiTextField!
    @IBOutlet weak var flatTextField: HoshiTextField!
    @IBOutlet weak var landmarkTextField: HoshiTextField!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var tagTF: HoshiTextField!
    @IBOutlet weak var saveDeliveryLocation: UILabel!
    @IBOutlet weak var otherTagHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var saveAsLabel: UILabel!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var workBtn: UIButton!
    @IBOutlet weak var othersBtn: UIButton!
    @IBOutlet var countrycodeBtn: UIButton!
    @IBOutlet var phoneNumberTextField: HoshiTextField!
    var countryCode:String = ""
    var phoneNum:[String] = []
    
    var zoomLevelofMap:Float = 15
    var addressList = [Location]()
    var flagEdit = false
    var addAddressVM = SavedAddressVM()
    var addressSelected = Location(data: [:])
    var AddressDelegate:AddressDelegate! = nil
    var addressAdded:Address? = nil
    var flagMapChange = Bool()
    var locationManager:LocationManager? = nil
    let disposeBag = DisposeBag()
    var flatNumber = ""
    var landmark = ""
    var phonenumber = ""
    var addressline1 = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryCode = Helper.setCountryCode()
        phoneNumberTextField.text = countryCode
        self.initialSetup()
        self.initialise()
        addressTF.Semantic()
        flatTextField.Semantic()
        landmarkTextField.Semantic()
        tagTF.Semantic()
        tagTF.placeholder = StringConstants.ADDRESSTAG()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        Helper.nonTransparentNavigation(controller: self.navigationController!)
        removeObserver()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Helper.transparentNavigation(controller: self.navigationController!)
    }
    
    
    // MARK: - UIButton Action
    
    @IBAction func countryCodeAction(_ sender: Any) {
        
        let vnhStoryboard = UIStoryboard(name: UIConstants.ControllerIds.VNHCountryPicker, bundle: nil)
        if let nav: UINavigationController = vnhStoryboard.instantiateInitialViewController() as! UINavigationController? {
            let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker
            // delegate
            picker.delegate = self as? VNHCountryPickerDelegate
            present(nav, animated: true, completion: nil)
        }
    }
    
    @IBAction func tabButtonAction(_ sender: UIButton) {
        
        for i in 0...2 {
            let viewIn:UIView = self.view.viewWithTag(100 + i)!
            let btnIn:UIButton = self.view.viewWithTag(200 + i) as! UIButton
            btnIn.tintColor =  Colors.AppBaseColor
            viewIn.backgroundColor =  Colors.AppBaseColor
        }
        sender.tintColor =  Colors.PrimaryText
        let viewIn = self.view.viewWithTag(sender.tag - 100)
        viewIn?.backgroundColor =  Colors.PrimaryText
        switch sender.tag-200 {
        case 0:
            tagTF.text =  StringConstants.Home()
            otherTagHeightConstraint.constant = 0
            break
        case 1:
            tagTF.text = StringConstants.Work()
            otherTagHeightConstraint.constant = 0
            break
        default:
            tagTF.text =  ""
            if flagEdit {
                tagTF.text = addressAdded?.Tag
            }
            otherTagHeightConstraint.constant = 55
            break
        }
        animateView()
    }
    
    @IBAction func ConfirmButtonAction(_ sender: Any) {
        
        saveButton.isUserInteractionEnabled = false
        
        let addressData = SavedAddressDBManager().getAddressDocument()
        for addressTemp in addressData {
            
            
            if Helper.clipDigit(valeu: addressTemp.Latitude, digits: 4)  == Helper.clipDigit(valeu: addressSelected.Latitude, digits: 4) && Helper.clipDigit(valeu: addressTemp.Longitude, digits: 4) == Helper.clipDigit(valeu: addressSelected.Longitude, digits: 4) && addressAdded?.FlatNo == flatNumber && addressAdded?.LandMark == landmark {
                self.saveButton.isUserInteractionEnabled = true
                Helper.showAlert(message: StringConstants.AddressAlreadySaved(), head: StringConstants.Error(), type: 1)
                return
            }
            
            
            
            
        }
        Helper.showPI(string: "")
        if addressAdded == nil {
            addressAdded = Address.init(data: [:])
        }
        if tagTF.text?.length == 0 {
            Helper.showAlert(message: StringConstants.TagMissing(), head: StringConstants.Error(), type: 1)
            self.saveButton.isUserInteractionEnabled = true
            
            return
        }
        addressAdded?.Tag = tagTF.text!
        
        addressAdded?.FlatNo = self.flatNumber
        addressAdded?.LandMark = self.landmark
        addressAdded?.PhoneNumber =  String(phoneNumberTextField.text!.suffix(phoneNumberTextField.text!.count - countryCode.count))
        if flagEdit {
            addAddressVM.updateAddress(location: addressSelected, address: addressAdded!, finish: { (success) in
                self.saveButton.isUserInteractionEnabled = true
                Helper.hidePI()
                if success == true {
                    _ = self.navigationController?.popViewController(animated: true)
                }
            })
            return
        }
        
        addAddressVM.addAddress(location: addressSelected, address: addressAdded!, finish: { (success) in
            Helper.hidePI()
            self.saveButton.isUserInteractionEnabled = true
            if success == true {
                _ = self.navigationController?.popViewController(animated: true)
            }
        })
    }
    @IBAction func myLocationAction(_ sender: UIButton) {
        let loc = Utility.currentLatAndLong()
        let lat = loc.Latitude
        let lng = loc.Longitude
        
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat) ,longitude: CLLocationDegrees(lng) , zoom: zoomLevelofMap)
        self.mapView.animate(to: camera)
        let locationAddress = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))
        locationManager?.updateLocationData(location: locationAddress, placeIn: addressSelected,flag: false)
        
        
    }
}

extension AddAddressVC {
    func setData() {
        APICalls.LogoutInfoTo.subscribe(onNext: { [weak self]success in
            if success {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }).disposed(by: self.addAddressVM.disposeBag)
    }
}
