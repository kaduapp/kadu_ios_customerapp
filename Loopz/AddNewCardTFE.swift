////
//  AddNewCardTFE.swift
//  DelivX
//
//  Created by 3 Embed on 19/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension AddNewCardVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        switch textField {
        case addNewCardView.cardNumberTF:
            let trimmedString = textField.text!.replacingOccurrences(of: " ", with: "")
            if trimmedString.count % 4 == 0 && trimmedString.count > 0 {
                if range.length == 0 {
                    textField.text = textField.text! + " "
                }else{
                    textField.text = textField.text!.trimmingCharacters(in: .whitespaces)
                }
                
            }
            if textField.text!.replacingOccurrences(of: " ", with: "").length == maxcardLength && range.length == 0 {
                addNewCardView.expiresTF.becomeFirstResponder()
            }
            return newString.replacingOccurrences(of: " ", with: "").length <= maxcardLength
            
        case addNewCardView.expiresTF:
            if textField.text?.length == 2 && range.length == 0{
                textField.text = textField.text! + "/"
            }
            else {
                if textField.text?.length == 2 {
                    textField.text = textField.text!.replacingOccurrences(of: "/", with: "")
                }
            }
            let maxLength = 5
            if textField.text?.length == 5 && range.length == 0 {
                addNewCardView.cvvTF.becomeFirstResponder()
            }
            return newString.length <= maxLength
            
        case addNewCardView.cvvTF:
            return newString.length <= maxCvvLength
            
        default:
            break
        }
        
        return true
    }
    
    func keyboardInputShouldDelete(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        Helper.addDoneButtonOnTextField(tf: textField, vc: self.addNewCardView)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField{
        case addNewCardView.cardNumberTF:
            addNewCardView.expiresTF.becomeFirstResponder()
            break
        case addNewCardView.expiresTF:
            addNewCardView.cvvTF.becomeFirstResponder()
            break
        case addNewCardView.cvvTF:
            textField.resignFirstResponder()
            break
        default:
            break
        }
        return true
    }
}


