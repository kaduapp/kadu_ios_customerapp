//
//  FullScreenMapVC.swift
//  DelivX
//
//  Created by 3 Embed on 23/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxAlamofire
import GoogleMaps
import GooglePlaces
import CoreLocation
import RxSwift

class FullScreenMapVC: UIViewController {

    @IBOutlet weak var fullMapView: GMSMapView!
    @IBOutlet weak var orderStatusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusTitle: UILabel!
    @IBOutlet weak var statusViewHight: NSLayoutConstraint!
    
    var dataOrder:Order? = nil
    let trackingModel = TrackingModelLibrary.trackList
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        didconnectMQTTResponse()
        initialViewSetup()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
       dismiss(animated: true, completion: nil)
    }
    
    @IBAction func swipeTopToBottom(_ sender: UISwipeGestureRecognizer) {
           closeMap()
    }
}
extension FullScreenMapVC {
    /// observe for response of signinVM
    func didconnectMQTTResponse(){
        MQTTDelegate.message_response.subscribe(onNext: {success in
            if self.dataOrder?.BookingId == success.Id && success.Action != 12 {
                self.dataOrder?.Status = success.Status
                self.dataOrder?.StatusMessage = success.Message
                self.checkStatus()
            }
        }, onError: {error in
            print(error)
        }).disposed(by: self.disposeBag)
    }

    
    
    /// check Status
    func checkStatus() {
        let status:NSInteger = (dataOrder?.Status)!
        switch status {
        case 8,9 :    //0
            updateView(status:0)
            break
        case 10: //1
            updateView(status:1)
            break
        case 11: //2
            updateView(status:2)
            break
        case 12: //3
            updateView(status:3)
            break
        case 13:  //4
            updateView(status:4)
            break
        case 14: //5
            updateView(status:5)
            break
        default:
            break
        }
    }
    
    
    /// updates the status
    ///
    /// - Parameter status: is of type int 0 to 5
    func updateView(status: Int){
        statusImage.image = trackingModel[status].selectedOrder
        statusTitle.text = trackingModel[status].orderStatusTitle
    }
  
}


// MARK: - GMSMapViewDelegate
extension FullScreenMapVC : GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    
    func mapViewDidStartTileRendering(_ mapView: GMSMapView) {
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    }
}
