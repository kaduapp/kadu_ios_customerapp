//
//  KeychainManager.swift
//  TouchToKeychain
//
//  Created by 3Embed on 24/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import KeychainSwift

class KeychainManager: NSObject {
    
    static var shareObj: KeychainManager? = nil
    static var Keychain: KeychainSwift? = nil
    /// Create Shared Instance
    static var shared: KeychainManager {
        if shareObj == nil {
            shareObj = KeychainManager()
            Keychain = KeychainSwift()
            Keychain?.synchronizable = true
            Keychain?.accessGroup = Utility.BundlePrefix + Utility.BundleId
        }
        return shareObj!
    }
    //Save Data in Keychain
    func SaveData(data:[String:String],finish:@escaping (Bool) -> Void) {
        if (KeychainManager.Keychain?.set(data["password"]!, forKey: data["key"]!))!{
            finish(true)
        }else{
            finish(false)
        }
    }

    //Get Data from Keychain
    func GetData(data:String) -> String {
        if KeychainManager.Keychain?.get(data) != nil {
            return (KeychainManager.Keychain?.get(data))!
        }else{
            return ""
        }
        
    }
    
    //Delete Data from Keychain
    func DeleteData(data:String) {
        KeychainManager.Keychain?.delete(data)
        KeychainManager.Keychain?.clear()
    }
}
