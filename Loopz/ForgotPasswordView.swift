//
//  ForgotPWView.swift
//  UFly
//
//  Created by Rahul Sharma on 28/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ForgotPasswordView: UIView {
    
    @IBOutlet weak var forgotPasswordSideHead: UILabel!
    @IBOutlet weak var navigationBackView: UIView!
    @IBOutlet weak var navTitleText: UILabel!
    
    @IBOutlet weak var forgotPWLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var enterMobileNumLabel: UILabel!
    @IBOutlet weak var phNumberBtn: UIButton!
    @IBOutlet weak var emailAddressBtn: UIButton!
    @IBOutlet weak var phNumTextField: UITextField!
    @IBOutlet weak var phNumCheckMark: UIButton!
    @IBOutlet weak var phNumSeparator: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var nextBtnContainer:UIView!
    @IBOutlet weak var orLabel: UILabel!
    
    @IBOutlet weak var phBtnSeparator: UIView!
    @IBOutlet weak var emailBtnSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetUp()
        phNumTextField.Semantic()
    }
    
    
    /// View initial setup
    func initialSetUp(){
        navigationBackView.isHidden = true
        
        Fonts.setPrimaryBlack(forgotPWLabel)
        Fonts.setPrimaryRegular(enterMobileNumLabel)
        Fonts.setPrimaryMedium(navTitleText)
        Fonts.setPrimaryMedium(forgotPasswordSideHead)
        Fonts.setPrimaryMedium(phNumberBtn)
        Fonts.setPrimaryMedium(emailAddressBtn)
        Fonts.setPrimaryRegular(phNumTextField)
        forgotPasswordSideHead.textColor =  Colors.PrimaryText
        forgotPasswordSideHead.text = StringConstants.ForgotPasswordHead()
        orLabel.text = StringConstants.OR()
        forgotPWLabel.text = StringConstants.ForgotPasswordHead()
        enterMobileNumLabel.text = StringConstants.EnterYourMobNum()
        navTitleText.text = StringConstants.ForgotPasswordHead()
        
        Helper.setButton(button: nextBtn,view:nextBtnContainer, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.Next(), highlighted: StringConstants.Next(), selected: StringConstants.Next(), button: nextBtn)
        Helper.setButtonTitle(normal: StringConstants.PhoneNumber(), highlighted: StringConstants.PhoneNumber(), selected: StringConstants.PhoneNumber(), button: phNumberBtn)
        Helper.setButtonTitle(normal: StringConstants.Email(), highlighted: StringConstants.Email(), selected: StringConstants.Email(), button: emailAddressBtn)
        
        emailAddressBtn.setTitleColor(  Colors.AppBaseColor, for: .normal)
        phNumberBtn.setTitleColor(  Colors.AppBaseColor, for: .normal)
        
        
        Fonts.setPrimaryMedium(nextBtn)
        
        phNumTextField.textColor =  Colors.PrimaryText
        forgotPWLabel.textColor =  Colors.HeadColor
        enterMobileNumLabel.textColor =  Colors.SeconderyText
        orLabel.textColor =  Colors.PrimaryText
        navTitleText.textColor =  Colors.PrimaryText
        phNumCheckMark.tintColor =  Colors.AppBaseColor
        
    }
    
    /// this metho updates the textfield type, keyboard type and place holder
    /// based on the type
    /// - Parameters:
    ///   - data: it is of type Authentication
    ///   - type: it is of type Integer 1 for phoneNumber textfield 0 for email Textfield
    func initView(data:Authentication,type:Int) {
        
        if type == 1 {
            phNumTextField.keyboardType = UIKeyboardType.phonePad
            phNumTextField.placeholder = StringConstants.PhoneNumber()
            phBtnSeparator.backgroundColor =  Colors.AppBaseColor
            emailBtnSeparator.backgroundColor =  Colors.SecondBaseColor
            phNumTextField.text = data.CountryCode + data.Phone
            if data.Phone.count == 0 {
                phNumCheckMark.isHidden = true
            }
            else {
                phNumCheckMark.isHidden = false
            }
        }else{
            phNumTextField.keyboardType = UIKeyboardType.emailAddress
            phNumTextField.placeholder = StringConstants.Email()
            phBtnSeparator.backgroundColor =  Colors.SecondBaseColor
            emailBtnSeparator.backgroundColor =  Colors.AppBaseColor
            phNumTextField.text = data.Email
        }
    }
}

// MARK: - UIScrollViewDelegate
extension ForgotPasswordView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
        let movedOffset: CGFloat = sender.contentOffset.y
        if sender == scrollView {
            let pageWidth: CGFloat = 238 - 64             //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            if ratio >= 1 {
                navigationBackView.isHidden = false
                navigationBackView.backgroundColor =  Colors.SecondBaseColor
                navTitleText.textColor =  Colors.PrimaryText
            }else {
                navigationBackView.isHidden = true
            }
        }
    }
}

