//
//  WishListTFE.swift
//  DelivX
//
//  Created by 3Embed on 17/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension WishListVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text != nil) || textField.text?.length != 0 {
            wishListVM.wishList.String = textField.text!
            self.wishListVM.updateWishList()
        }else{
            textField.text = wishListVM.wishList.String
        }
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

