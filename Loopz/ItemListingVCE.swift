//
//  ItemListingVCE.swift
//  UFly
//
//  Created by 3 Embed on 12/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension ItemListingVC: MenuVCDelegate {
    
    /// this method updates the subCategories
    ///
    /// - Parameter data: it is of type SubCategory Model
    func didSelectSubCat(data: Any){
        let dataFrom = data as! SubSubCategory
        selectedSubSubCategory = dataFrom.Id
    }
    
    /// subscription to cartAPI
    func isCartUpdated(){
        CartDBManager.cartDBManager_response.subscribe(onNext: { success in
            self.storeView.showBottom()
        }).disposed(by: disposeBag)
        CartVM.cartVM_response.subscribe(onNext: { data in
            if data.0 == CartVM.ResponceType.SuccessNormal {
                self.storeView.updateBottomBar(data: (self.itemListingVM.store)!)
                 self.cartVM.arrayOfCart = data.1
            }
        }).disposed(by: disposeBag)
    }
    /// subscription to get Items API
    func didGetItems(){
        itemListingVM.ItemListingVM_response.subscribe(onNext: { success in
            
            self.loading = false
            if success == true {
                self.storeView.updateBottomBar(data: self.itemListingVM.store!)
                self.setCollectionViewBackground()
                self.subsubCategoryCollectionView.reloadData()
                self.itemCategoriesCollectionView.reloadData()
            }
            
        }).disposed(by: disposeBag)
    }
    
    /// initial view setup
    func initialViewSetup(){
        self.titleForTheNav.text = itemListingVM.subCategorySelected.Name
        if itemListingVM.subCategorySelected.Name.length == 0 {
            self.titleForTheNav.text = itemListingVM.controllerCategory?.Name
        }
        self.widthOfNav.constant = UIScreen.main.bounds.width - 100
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        storeView.addHedderToStore()
        isCartUpdated()
        itemListingVM.getItems()
    }
    
    func setCollectionViewBackground(){
        
        var productCount = 0
        if let count = self.itemListingVM.store?.Products.count, count > 0{
            productCount = count
        }
        
        if self.itemListingVM.SubSubCatArray.count > 0 || productCount > 0{

        }
        else{
            self.noProductsView.setData(image: #imageLiteral(resourceName: "EmptySearch"), title: StringConstants.EmptyFilterResult(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        }
    }
    
    
}
