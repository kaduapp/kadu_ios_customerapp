//
//  FilterVC.swift
//  DelivX
//
//  Created by 3Embed on 09/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {

    
    @IBOutlet weak var applyButton: UIBarButtonItem!
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var subFiltersTableView: UITableView!
    @IBOutlet weak var rightButton: UIBarButtonItem!
    
    var selectedCat = ""
    var selectedSubCat = ""
    var filterVM = FilterVM()
    var filterDetailVM = FilterDetailVM()
    var searchVM = SearchVM()
    var text = ""
    var textLanguage = ""
    var selectedCellIndex = 0
    var sortArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        filterVM.setData(type: Utility.getStore().TypeStore)
        rightButton.title = StringConstants.Clear()
        applyButton.title = StringConstants.Apply()
        self.mainTableView.backgroundColor = Colors.SeparatorLarge
        if (Utility.getSelectedSuperStores().type == .Restaurant){
          sortArray = filterVM.arrayOfRestaurantSort
        }
        else{
           sortArray = filterVM.arrayOfSort
       }
        filterDetailVM.type = APIRequestParams.Sort
       
        getResponse()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
   
        searchVM.isClearAll = false
        if (Utility.getSelectedSuperStores().type == .Restaurant){
        self.searchVM.filterInfo.onNext((true,filterDetailVM.type,["\(filterDetailVM.selectedRating)",filterDetailVM.selectedFoodType,filterDetailVM.selectedCosine]))
        }
        else{
        self.searchVM.filterInfo.onNext((true,filterDetailVM.type,filterDetailVM.arrayOfTypesSelected))
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = StringConstants.Filter()
        //+ " & " + StringConstants.Sort()
        observerAction()
        searchVM.filterInfo.onNext((false,APIRequestParams.Sort,[]))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = "  "
    }
    
    @IBAction func clearAction(_ sender: Any) {
        self.searchVM.selectedCategory = ""
        self.searchVM.selectedSubCategory = ""
        self.searchVM.selectedSubSubCategory = ""
        self.searchVM.brandArray = []
        self.searchVM.minPrice = 0
        self.searchVM.maxPrice = 0
        self.searchVM.manufacturerArray = []
        self.searchVM.offer = ""
        self.filterDetailVM.selectedManufacturer = []
        self.filterDetailVM.selectedBrands = []
        self.filterDetailVM.selectedCategories = []
        self.filterDetailVM.selectedCuisines = []
        self.filterDetailVM.selectedCosine = ""
        self.filterDetailVM.selectedFoodType = ""
        self.filterDetailVM.selectedMinValue = 0
        self.filterDetailVM.selectedMaxValue = 0
        self.filterDetailVM.selectedSort = -1
        self.filterVM.selectedCousine = ""
        self.filterVM.selectedFoodType = ""
        self.filterVM.selectedRating = 0
        self.filterDetailVM.arrayOfTypesSelected.removeAll()
        self.filterDetailVM.arrayOfSelectedFilters.removeAll()
        searchVM.filterInfo.onNext((true,"",[]))
        if (Utility.getSelectedSuperStores().type == .Restaurant){
        searchVM.isClearAll = true
        }
        else{
        searchVM.isClearAll = false
        }
        mainTableView.reloadData()
        subFiltersTableView.reloadData()
         self.dismiss(animated: true, completion: nil)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == String(describing: FilterDetailsVC.self) {
            let btn = sender as! Int
            if let controller = segue.destination as? FilterDetailsVC {
                controller.filterDetailVM.title = filterVM.arrayOfFilter[btn]
                controller.filterDetailVM.type = filterVM.arrayOfFilter[btn]
                controller.filterDetailVM.needle = text
                controller.filterDetailVM.needleLanguage = textLanguage
                controller.filterDetailVM.chosenCat = selectedCat
                controller.filterDetailVM.chosenSubCat = selectedSubCat
                controller.filterDetailVM.storeFilter = filterVM.storeFilters
                controller.filterDetailVM.offer = searchVM.offer
                controller.searchVMReference = searchVM

                if filterVM.context == 3 {
                    controller.filterDetailVM.searchFrom = 1
                }else{
                    controller.filterDetailVM.searchFrom = 0
                }
            }
        }
    }
    
    
    func observerAction() {
        FilterDetailVM.filterArray_responce.subscribe(onNext: { [weak self]data in
            self?.searchVM.selectedCategory = data.0
            self?.searchVM.selectedSubCategory = data.1
            self?.searchVM.selectedSubSubCategory = data.2
            self?.searchVM.brandArray = data.3
            self?.searchVM.minPrice = data.4
            self?.searchVM.maxPrice = data.5
            self?.searchVM.manufacturerArray = data.6
            self?.searchVM.offer = data.7
           // self?.filterDetailVM.selectedSort = data.8
            self?.filterVM.selectedRating = data.9
            self?.filterVM.selectedCousine = data.10
            self?.filterVM.selectedFoodType = data.11
            self?.mainTableView.reloadData()
        }).disposed(by: self.searchVM.disposeBag)
    }
    
    
    
    
    func getResponse() {
        
        Helper.networkReachabilityChange.subscribe(onNext: { [weak self]dataGot in
            self?.filterDetailVM.getFilter()
        }).disposed(by: self.searchVM.disposeBag)
        filterDetailVM.filterDetails_responce.subscribe(onNext: { [weak self]success in
            if success {
                if self?.filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
                    for item in (self?.filterDetailVM.arrayOfCategory)! {
                        if item.Name == self?.filterDetailVM.selectedCategory {
                            for item2 in item.SubCategories {
                                if item2.Name == self?.filterDetailVM.SubCatSelected {
                                    if item2.SubSubCat.count > 0 {
                                        self?.performSegue(withIdentifier: String(describing: FilterSubCatVC.self), sender: self)
                                    }
                                }
                            }
                        }
                    }
                }
                else if self?.filterDetailVM.type == APIRequestParams.Sort{
                    
                    
                }
                
                else if (self?.filterDetailVM.selectedCategory.length)! > 0 && self?.filterDetailVM.type == APIRequestParams.CategoryFilter {
                    self?.filterDetailVM.type = APIRequestParams.SubCategoryFilter
                    self?.filterDetailVM.chosenCat = (self?.filterDetailVM.selectedCategory)!
                    for item in (self?.filterDetailVM.arrayOfCategory)! {
                        if item.Name == self?.filterDetailVM.selectedCategory {
                            let index = self?.filterDetailVM.arrayOfCategory.index(of: item)
                            self?.filterDetailVM.selectedSection = index!
                        }
                    }
                    self?.filterDetailVM.getFilter()
                }
                
                
                self?.subFiltersTableView.reloadData()
            }
        }).disposed(by:self.searchVM.disposeBag)
        
        switch filterDetailVM.type {
        case StringConstants.Brand():
            filterDetailVM.type = APIRequestParams.BrandFilter
            break
        case StringConstants.Manufacturer():
            filterDetailVM.type = APIRequestParams.ManufacturerFilter
            break
        case StringConstants.Categories():
            filterDetailVM.type = APIRequestParams.CategoryFilter
            break
        case StringConstants.Price():
            filterDetailVM.type = APIRequestParams.FilterPrice
            break
        case StringConstants.Cousines():
            filterDetailVM.type = StringConstants.Cousines()
            break
        case StringConstants.CostForTwo():
            filterDetailVM.type = StringConstants.CostForTwo()
            break
        case StringConstants.Rating():
            filterDetailVM.type = StringConstants.Rating()
            break
        case StringConstants.FoodType():
            filterDetailVM.type = StringConstants.FoodType()
            break
        default:
            break
        }
        FilterDetailVM.filterArray_responce.subscribe(onNext: { [weak self]data in
            switch self?.filterDetailVM.type {
            case APIRequestParams.CategoryFilter:
                self?.filterDetailVM.selectedCategory = data.0
                self?.filterDetailVM.SubCatSelected = data.1
                self?.filterDetailVM.SubSubCatSelected = data.2
                break
            case APIRequestParams.BrandFilter:
                self?.filterDetailVM.arrayOfTypesSelected = data.3
                break
            case APIRequestParams.FilterPrice:
                self?.filterDetailVM.selectedMinValue = data.4
                self?.filterDetailVM.selectedMaxValue = data.5
                break
            case APIRequestParams.ManufacturerFilter:
                self?.filterDetailVM.arrayOfTypesSelected = data.6
                break
            case APIRequestParams.FIlterOffer:
                //                self?.filterDetailVM. = data.7
                break
            case APIRequestParams.Sort:
                //                self?.filterDetailVM.sor = data.8
                break
            case StringConstants.Rating(),StringConstants.Cousines(),StringConstants.FoodType():
                self?.filterDetailVM.selectedRating = data.9
                self?.filterDetailVM.selectedCosine = data.10
                self?.filterDetailVM.selectedFoodType = data.11
                break
            default:
                break
            }
            self?.filterDetailVM.needle = data.12
            self?.subFiltersTableView.reloadData()
        }).disposed(by: self.searchVM.disposeBag)
        filterDetailVM.getFilter()
    }
    

    
}
