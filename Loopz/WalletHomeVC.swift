//
//  WalletHomeVC.swift
//  DelivX
//
//  Created by 3 Embed on 22/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class WalletHomeVC: UIViewController {
    
    @IBOutlet weak var balanceHead: UILabel!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var separatorBalance: UIView!
    @IBOutlet weak var lightLimit: UILabel!
    @IBOutlet weak var hardLimit: UILabel!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var headerSeparator: UIView!
    @IBOutlet weak var listHead: UILabel!
    @IBOutlet weak var footerHead: UILabel!
    @IBOutlet weak var moneyView: UIView!
   
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var moneyTF: UITextField!
    @IBOutlet weak var moneySeparator: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var bottomButtonView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var softColorView: UIView!
    @IBOutlet weak var softText: UILabel!
    @IBOutlet weak var hardColorView: UIView!
    @IBOutlet weak var hardText: UILabel!
    @IBOutlet weak var voucherButton: UIButton!
    
    let disposeBag = DisposeBag()
    
    var paymentMethord:Int = 0
    var isComingCheckout = false
    var delegate: PaymentVCDelegate? = nil
    var cardArray   = [Card]()
    let responseData = PublishSubject<Card>()
    var amount:Float = 0
    var addedAmount = ""
    
    var amountIn:Float = 0
    var selectedCard = 0
    static let addMoneyResp = PublishSubject<Bool>()
    var navView = NavigationView().shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainTableView.backgroundColor = Colors.ScreenBackground
        self.amount = Utility.getWallet().Amount
        validateUI()
        setBalance()
        // Do any additional setup after loading the view.
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.Wallet())
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addObserver()
        WalletAPICalls.getBalance().subscribe(onNext: { [weak self]data in
            self?.amount = Utility.getWallet().Amount
            self?.setBalance()
        }).disposed(by: disposeBag)
        CardAPICalls.getCard().subscribe(onNext: { [weak self]data in
            self?.cardArray = data
            self?.mainTableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        removeObserver()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction  func addAction(_ sender: Any){
        if cardArray.count == 0 {
            Helper.showAlert(message: StringConstants.NoCardsAvailable(), head: StringConstants.Error(), type: 1)
            return
        }
        if let amountIn = moneyTF.text?.floatValue{
            if amountIn > 0{
                Helper.showAlertReturn(message: StringConstants.WalletConfirmation() + Helper.df2so(Double( amountIn)) + StringConstants.QuestionMark(), head: StringConstants.Confirm(), type: StringConstants.Confirm(), closeHide: false, responce: CommonAlertView.ResponceType.WalletAmount)
            }
        }
        textFieldDidEndEditing(moneyTF)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.title = ""
    }
    
    @IBAction func VoucherAction(_ sender: UIButton) {
        let viewController = UIStoryboard(name: UIConstants.PopupStoryboard, bundle: nil).instantiateViewController(withIdentifier: String(describing:VoucherVC.self)) as! VoucherVC
        viewController.voucherVM.VoucherVM_responseWallet.subscribe(onNext: { [weak self]success in
            if success{
                WalletAPICalls.getBalance().subscribe(onNext: { [weak self]data in
                    self?.amount = Utility.getWallet().Amount
                    self?.setBalance()
                }).disposed(by: self!.disposeBag)
            }
        }).disposed(by: disposeBag)
        viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(viewController, animated:true, completion: nil)
    }
    @IBAction func historyAction(_ sender: Any) {
        self.performSegue(withIdentifier: String(describing: WalletHistoryVC.self), sender: self)
    }
}
