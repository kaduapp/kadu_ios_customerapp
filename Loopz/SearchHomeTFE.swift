//
//  SearchHomeTFE.swift
//  DelivX
//
//  Created by 3Embed on 24/10/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

// MARK: - UITextFieldDelegate methods
extension SearchHomeVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        restuarantTableView.reloadData()
        dishesTableView.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if((newString as String).isEmpty)
        {
            searchVM.arrayOfItems.removeAll()
            searchVM.arrayOfStores.removeAll()
            restuarantTableView.reloadData()
            dishesTableView.reloadData()
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField.text?.length)! >= 3 {
            self.performSegue(withIdentifier: String(describing: SearchDetailsVC.self), sender: textField.text!)
        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        if textField.text != "" {
            searchVM.loadPopular = false
            lang = Helper.getKeyboardLanguage(language: (textField.textInputMode?.primaryLanguage)!)
            searchVM.getProduct(text: textField.text!,language: lang)
        }
//        else {
//            searchVM.loadPopular = true
//            searchVM.arrayOfItems.removeAll()
//            searchVM.arrayOfStores.removeAll()
//            restuarantTableView.reloadData()
//            dishesTableView.reloadData()
//        }
        setUIforNaviagtion()
    }
}
