//
//  SignupVM.swift
//  UFly
//
//  Created by 3Embed on 07/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SignupVM: NSObject {
    
    enum ResponseType:Int {
        
        case ValidateEmail = 1
        case ValidatePhone = 2
        case Signup        = 3
        case RegisteredEmail = 4
        case RegisteredPhone = 5
        case validateReferral = 6
        case MoveOTP            = 7
    }
    
    let disposeBag = DisposeBag()
    var authentication = Authentication()
    var validEmail = false
    var validPhone = false
    var doSignUp = false
    var signUp = false
    var didTapColse = false
    let signupVM_response = PublishSubject<ResponseType>()
    var validationMessage = ""
    
    /// validate the email
    func validateEmail() {
        if signUp {
            return
        }
        AuthenticationAPICalls.validatePhoneEmail(user:authentication,verifyType: 2, showPop: true).subscribe(onNext: {[weak self]success in
            self?.validEmail = success.0
            if success.0 == false {
                Helper.hidePI()
                 self?.validationMessage = success.1
                self?.signupVM_response.onNext(ResponseType.RegisteredEmail)
            }else{
                self?.signupVM_response.onNext(ResponseType.ValidateEmail)
                if self?.doSignUp == true {
                    if self?.validPhone == true {
                        //                        self.signup()
                        self?.signupVM_response.onNext(ResponseType.MoveOTP)
                    }else{
                        self?.validatePhone()
                    }
                }
            }
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    /// validates phone number
    func validatePhone() {
        if signUp {
            return
        }
        let phoneNumber = authentication.Phone.replacingOccurrences(of: authentication.CountryCode, with: "")
        authentication.Phone = phoneNumber
        
        AuthenticationAPICalls.validatePhoneEmail(user:authentication,verifyType: 1, showPop: true).subscribe(onNext: {[weak self]success in
            self?.validPhone = success.0
            if success.0 == false {
                Helper.hidePI()
                self?.validationMessage = success.1
                self?.signupVM_response.onNext(ResponseType.RegisteredPhone)
                
            }else{
                self?.signupVM_response.onNext(ResponseType.ValidatePhone)
                if self?.doSignUp == true {
                    if self?.validEmail == true {
                        //                        self.signup()
                        self?.signupVM_response.onNext(ResponseType.MoveOTP)
                    }else{
                        self?.validateEmail()
                    }
                }
            }
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    
    /// this method having subscription for the signup API call defined in AuthenticationAPICalls
    ///
    /// - Parameter view:
    func signup(data:Authentication) {
        if signUp {
            return
        }
        signUp = true
        Helper.showPI(string: StringConstants.SigningUp())
        AuthenticationAPICalls.signup(user:data).subscribe(onNext: {[weak self]success in
            if success == true {
                self?.signupVM_response.onNext(ResponseType.Signup)
            }
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    
    
    func validateReferralCode(reffCode: String){
        AuthenticationAPICalls.validateReferralCode(refCode: reffCode).subscribe(onNext: { [weak self]success in
            if success == false {
                self?.signupVM_response.onNext(ResponseType.validateReferral)
            }
            
        }).disposed(by: disposeBag)
    }
    
}
