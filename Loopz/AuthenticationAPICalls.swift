//
//  AuthenticationAPICalls.swift
//  UFly
//
//  Created by 3Embed on 10/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import Firebase
import FirebaseMessaging
import Alamofire

class AuthenticationAPICalls: NSObject {
    static let disposeBag = DisposeBag()
    //GuestLogin API
    class func guestLoginAPI() -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeaderWithoutAuth()
        let params : [String : Any] = [
            APIRequestParams.DeviceId                :Utility.DeviceId,
            APIRequestParams.AppVersion              :Utility.AppVersion,
            APIRequestParams.DevMake                 :Utility.DeviceMake,
            APIRequestParams.DevType                 :Utility.DeviceType,
            APIRequestParams.DevModel                :Utility.DeviceModel,
            APIRequestParams.OSVersion               :Utility.DeviceVersion,
            APIRequestParams.DeviceTime              :Utility.DeviceTime,
            APIRequestParams.Latitude                :Double(LocationManager.shared.latitute),
            APIRequestParams.Longitude               :Double(LocationManager.shared.longitude)
        ]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.GuestLogin, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let data = bodyIn[APIResponceParams.Data] as! [String:Any]
                    Utility.saveGuestLogin(data: true)
                    Utility.saveGuestProfileData(data: data){ (success) in
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //Login API
    class func loginAPI(user:Authentication) -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.LoggingIn())
        let header = Utility.getHeader()
        let params : [String : Any] =    [
            APIRequestParams.UserName                :user.Phone,
            APIRequestParams.Password                :user.Password,
            APIRequestParams.DeviceId                :Utility.DeviceId,
            APIRequestParams.PushToken               :Utility.PushToken,
            APIRequestParams.AppVersion              :Utility.AppVersion,
            APIRequestParams.DevMake                 :Utility.DeviceMake,
            APIRequestParams.DevType                 :Utility.DeviceType,
            APIRequestParams.DevModel                :Utility.DeviceModel,
            APIRequestParams.OSVersion               :Utility.DeviceVersion,
            APIRequestParams.DeviceTime              :Utility.DeviceTime,
            APIRequestParams.Latitude                :Utility.getAddress().Latitude,
            APIRequestParams.Longitude               :Utility.getAddress().Longitude,
            APIRequestParams.CountryCode             :user.CountryCode
        ]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.Login, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    
                    let data = bodyIn[APIResponceParams.Data] as! [String:Any]
                    Utility.saveProfileData(data: data)
                    Utility.subscribeFCMTopic()
                    Utility.connectMQTT()
                    CartAPICalls.getCartData().subscribe(onNext: {success in
                    }, onError: {error in
                        Helper.hidePI()
                    }).disposed(by: self.disposeBag)
                    observer.onNext(true)
                    observer.onCompleted()
                }else if errNum == .Required {
                    observer.onNext(false)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //Validate Phone Number
    class func validatePhoneEmail(user:Authentication,verifyType:Int,showPop:Bool) -> Observable<(Bool,String)> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeaderWithoutAuth()
        let params : [String : Any] =    [
            APIRequestParams.Phone                   :user.Phone,
            APIRequestParams.CountryCode             :user.CountryCode,
            APIRequestParams.VerifyType              :verifyType,
            APIRequestParams.Email                   :user.Email
        ]
        
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.ValidatePhone, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                
                var message = ""
                if let data = body as? [String : Any],let msg = data[APIResponceParams.Message] as? String {
                    message = msg
                }
                if errNum == .success {
                    observer.onNext((true,message))
                    observer.onCompleted()
                }else{
                    observer.onNext((false,message))
                    observer.onCompleted()
                    if showPop {
                       //APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //ForgotPassword
    class func ForgotPassword(emailOrMobile : String,code:String,VerifyType:Int,ProcessType: Int) -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Loading())
        let header = Utility.getHeaderWithoutAuth()
        let params : [String : Any] =    [
            APIRequestParams.EmailOrMobile                   :emailOrMobile,
            APIRequestParams.VerifyType                      :VerifyType,
            APIRequestParams.CountryCode                     :code
        ]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.ForgotPassword, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    
                    observer.onNext(true)
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    
                    observer.onCompleted()
                    
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //GetOTP From Signup
    class func getOTP(user:Authentication,show:Bool) -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Loading())
        let header = Utility.getHeaderWithoutAuth()
        let params : [String : Any] =    [
            APIRequestParams.Phone                   :user.Phone,
            APIRequestParams.CountryCode             :user.CountryCode,
            ]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.GetOTP, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    observer.onNext(true)
                    observer.onCompleted()
                }
                else{
                    observer.onNext(false)
                }
                if show {
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //verifyOTP From Signup
    class func verifyOTP(user:Authentication) -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeaderWithoutAuth()
        let params : [String : Any] =    [
            APIRequestParams.Phone                   :user.Phone,
            APIRequestParams.CountryCode             :user.CountryCode,
            APIRequestParams.OTPCode                 :user.Otp
        ]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.VerifyOTP, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    observer.onNext(true)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    observer.onNext(false)
                    observer.onCompleted()
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
        
    }
    
    //Signup
    class func signup(user:Authentication) -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        let params : [String : Any] =    [
            APIRequestParams.FirstName                  :user.Name,
            APIRequestParams.Email                      :user.Email,
            APIRequestParams.CountryCode                :user.CountryCode,
            APIRequestParams.Phone                      :user.Phone,
            APIRequestParams.Password                   :user.Password,
            APIRequestParams.ProfilePic                 :user.ProfilePic,
            APIRequestParams.LoginType                  :user.LoginType,
            APIRequestParams.OSVersion                  :Utility.DeviceVersion,
            APIRequestParams.ReferralCode               :user.ReferralCode,
            APIRequestParams.Latitude                   :LocationManager.shared.latitute,
            APIRequestParams.Longitude                  :LocationManager.shared.longitude,
            APIRequestParams.TermsAndCond               :1,
            APIRequestParams.DeviceId                   :Utility.DeviceId,
            APIRequestParams.PushToken                  :Utility.PushToken,
            APIRequestParams.AppVersion                 :Utility.AppVersion,
            APIRequestParams.DevMake                    :Utility.DeviceMake,
            APIRequestParams.DevType                    :Utility.DeviceType,
            APIRequestParams.DevModel                   :Utility.DeviceModel,
            APIRequestParams.DeviceTime                 :Utility.DeviceTime,
            APIRequestParams.IPAddress                  :Utility.getIPAddress(),
            APIRequestParams.City                       :Utility.getAddress().City,
            APIRequestParams.CityId                     :Utility.getAddress().CityId,
            APIRequestParams.ZoneId                     :Utility.getAddress().ZoneId,
            APIRequestParams.CurrencySymbol             :Utility.getCurrency().1,
            APIRequestParams.Currency                   :Utility.getCurrency().0
        ]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.Signup, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                
                if errNum == .success {
                    observer.onNext(true)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    observer.onNext(false)
                    observer.onCompleted()
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //Logout API
    class func logoutAPI() -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Loading())
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.Logout, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    
                    Utility.logout(type:0,message: "")
                    observer.onNext(true)
                    observer.onCompleted()
                }else if errNum == .Required {
                    observer.onNext(false)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //Update New Password
    class func updatePassword(user:Authentication) -> Observable<Bool>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Updating())
        let header = Utility.getHeaderWithoutAuth()
        let params : [String : Any] =    [
            APIRequestParams.CountryCode             :user.CountryCode,
            APIRequestParams.Phone                   :user.Phone,
            APIRequestParams.Password                :user.Password,
            APIRequestParams.OTPCode                 :user.Otp]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.UpdatePassword, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    //  APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    //  CommonAlertView.AlertPopupResponse.subscribe(onNext: {success in
                    observer.onNext(true)
                    
                    //  })
                    
                    observer.onCompleted()
                }else if errNum == .Required {
                    observer.onNext(false)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //
    class func validateReferralCode(refCode: String) -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header : HTTPHeaders = [:]        //Utility.getHeaderWithoutAuth()
        let params : [String : Any] =    [:]
        let url = APIEndTails.ValidateReferral + "\(refCode)"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                
                if errNum == .success {
                    observer.onNext(true)
                    observer.onCompleted()
                    
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    observer.onNext(false)
                    observer.onCompleted()
                    
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    class func getReferralCode() -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header : HTTPHeaders = [:]        //Utility.getHeaderWithoutAuth()
        let params : [String : Any] =    [:]
        let url = APIEndTails.ReferralCodeByUserId + Utility.getProfileData().SId
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let bodyIn = body as! [String : Any]
                    let data = bodyIn[APIResponceParams.Data] as! [String:Any]
                    Utility.saveReferralCode(data: data[APIResponceParams.ReferralCode] as! String)
                    observer.onNext(true)
                    observer.onCompleted()
                    
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    observer.onNext(false)
                    observer.onCompleted()
                }
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
}
