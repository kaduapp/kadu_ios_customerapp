//
//  SubSubCategory.swift
//  DelivX
//
//  Created by 3Embed on 09/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct SubSubCategory {
    var Id                      = ""
    var Name                    = ""
    var Desc                    = ""
    var Image                   = ""
    var Products:[Item]         = []
    
    init(data: [String:Any]) {
        if let titleTemp = data[APIResponceParams.SubSubCatName] as? String{
            Name                         = titleTemp.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if let titleTemp = data[APIResponceParams.SubSubCatId] as? String{
            Id             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.SubCategoryDesc] as? String{
            Desc                         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.CategoryImage] as? String{
            Image             = titleTemp
        }
        
        if let titleTemp = data[APIResponceParams.Products] as? [Any]{
            Products.removeAll()
            for item in titleTemp {
                Products.append(Item.init(data: item as! [String:Any], store: Utility.getStore()))
            }
        }
    }
}
