//
//  FavouriteAPICalls.swift
//  UFly
//
//  Created by 3Embed on 18/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class FavouriteAPICalls: NSObject {
    
    static let disposeBag = DisposeBag()
    static let favVMResponse = PublishSubject<([Item],Int)>()
    static let favStoreVMResponse = PublishSubject<([Store],Int)>()
    //Get Store Data
    class func favourite(item:Item) ->Observable<Bool> {
        //       Helper.showPI(string: StringConstants.Loading())
        let params : [String : Any] =    [
            APIRequestParams.ParentId               :item.ParentId,
            APIRequestParams.ChildId                :item.Id
        ]
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header : HTTPHeaders = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.Favourite, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .created {
                    observer.onNext(true)
                    favVMResponse.onNext(([item],1))
                    observer.onCompleted()
                }else if errNum == .deleted {
                    observer.onNext(false)
                    favVMResponse.onNext(([item],0))
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //Get Fav Data
    class func favourite() ->Observable<[Item]> {
        Helper.showPI(string: StringConstants.Loading())
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        let url = APIEndTails.Favourite + "/\(Utility.getAddress().ZoneId)" + "/\(self.getStoreId())"
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let bodyIn = body as![String:Any]
                    var Items = [Item]()
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [Any]{
                        for data in titleTemp {
                            let cat = Item.init(data: data as! [String : Any], store: Store.init(data: [:]))
                            cat.Fav = true
                            Items.append(cat)
                        }
                    }
                    favVMResponse.onNext((Items,2))
                    observer.onNext(Items)
                    observer.onCompleted()
                }else if errNum == .NotFound {
                    observer.onNext([])
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    /// <#Description#>
    ///
    /// - Parameter id: if id is 0 will get wishLists else items i wishList
    /// - Returns: return array of items
    class func getWishList(id:String,pi:Bool) ->Observable<[Item]> {
        if pi {
            Helper.showPI(string: StringConstants.Loading())
        }
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        if Utility.getGuestLogin() == false {
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        let url = APIEndTails.WishList + "/" + Utility.getAddress().ZoneId + "/" + id + "/0" //+ self.getStoreId()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if pi {
                    Helper.hidePI()
                }
                if errNum == .success {
                    let bodyIn = body as![String:Any]
                    if let data = bodyIn[APIResponceParams.Data] as? [Any] {
                        if id == "0" {
                            Utility.saveWishList(data:data)
                        }else{
                            var Items = [Item]()
                            if let titleTemp = bodyIn[APIResponceParams.Data] as? [Any]{
                                for data in titleTemp {
                                    let cat = Item.init(data: data as! [String : Any], store: Store.init(data: [:]))
                                    Items.append(cat)
                                }
                            }
                            observer.onNext(Items)
                            observer.onCompleted()
                        }
                    }
                }else if errNum == .NotFound {
                    if id == "0" {
                        Utility.saveWishList(data:[])
                    }else{
                        observer.onNext([])
                        observer.onCompleted()
                    }
                } else {
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
                
            }, onError: { (Error) in
                Helper.hidePI()
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),
                                 head: StringConstants.Error(), type: 1)
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //create wishList
    class func createWishList(listName: String) -> Observable<Bool>{
        
        Helper.showPI(string: StringConstants.Loading())
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        
        let params : [String : Any] = [
            APIRequestParams.FirstName  : listName,
            APIRequestParams.ZoneId     : Utility.getAddress().ZoneId,
            APIRequestParams.StoreId    : self.getStoreId()
        ]
        
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.WishList, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .created {
                    let bodyIn = body as![String:Any]
                    if let data = bodyIn[APIResponceParams.Data] as? [String:Any] {
                        Utility.updateWishList(data:data, type: -1)
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
                
            }, onError: { (Error) in
                Helper.hidePI()
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //create wishList
    class func addItemToWishLists(list: [PopupModel], item: Item) -> Observable<Bool>{
        
        Helper.showPI(string: StringConstants.Loading())
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        var arrayOfList:[Any] = []
        for data in list {
            let dataTemp = [
                APIRequestParams.Id         : data.ID,
                APIRequestParams.Checked    : data.Flag
                ] as [String : Any]
            arrayOfList.append(dataTemp)
        }
        let params : [String : Any] = [
            APIRequestParams.List           : arrayOfList,
            APIRequestParams.ChildId        : item.Id,
            APIRequestParams.ParentId       : item.ParentId,
            APIRequestParams.ZoneId         : Utility.getAddress().ZoneId,
            APIRequestParams.StoreId        : item.StoreId
            ] as [String : Any]
        
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.ItemToWishList, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .created {
                    observer.onNext(true)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
                
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    //remove wishList items
    class func removeItemFromWishLists(list: Item, item: PopupModel) -> Observable<Bool>{
        
        Helper.showPI(string: StringConstants.Loading())
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        var arrayOfList:[Any] = []
        for data in [list] {
            let dataTemp = [
                APIRequestParams.ChildId         : data.Id,
                APIRequestParams.ParentId        : data.ParentId
                ] as [String : Any]
            arrayOfList.append(dataTemp)
        }
        let params : [String : Any] = [
            APIRequestParams.ListId         : item.ID,
            APIRequestParams.Product        : arrayOfList
            ] as [String : Any]
        
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.ItemToWishList, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .deleted {
                    observer.onNext(true)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
                
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    // delete wishList
    class func deleteWishList(listID: String) -> Observable<Bool>{
        Helper.showPI(string: StringConstants.Delete())
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        let params : [String : Any] = [APIRequestParams.ListId: listID]
        let url = APIEndTails.WishList + "/" + listID
        return Observable.create { observer in
            RxAlamofire.requestJSON(.delete, url, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .deleted {
                    observer.onNext(true)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //remove wishList items
    class func editWishList(data: PopupModel, item: PopupModel) -> Observable<Bool>{
        
        Helper.showPI(string: StringConstants.Loading())
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let params : [String : Any] = [
            APIRequestParams.ListId             : item.ID,
            APIRequestParams.FirstName          : data.String,
            APIResponceParams.Image             : data.Image
            ] as [String : Any]
        
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.WishList, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .updated {
                    let bodyIn = body as! [String:Any]
                    let dataIn = bodyIn[APIResponceParams.Data] as! [String : Any]
                    Utility.updateWishList(data:dataIn, type: -2)
                    observer.onNext(true)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
                
            }, onError: { (Error) in
                Helper.hidePI()
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
            }, onCompleted: {
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    class func getStoreId() -> String{
        var id = "0"
        let storeid = Utility.getStore().Id
        if storeid.length != 0{
            id = storeid
        }
        return id
    }
    
    class func favouriteStore(store:Store) ->Observable<Bool> {
        //       Helper.showPI(string: StringConstants.Loading())
        let params : [String : Any] =    [
            //APIRequestParams.ZoneId            :Utility.getAddress().ZoneId,
            APIRequestParams.StoreId           :store.Id
        ]
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.FavouriteStore, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .created {
                    observer.onNext(true)
                    favStoreVMResponse.onNext(([store],1))
                    observer.onCompleted()
                }else if errNum == .deleted {
                    observer.onNext(false)
                    favStoreVMResponse.onNext(([store],0))
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
}



