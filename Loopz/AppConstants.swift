//
//  AppConstants.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class AppConstants: NSObject {
    enum OrderStatus:Int {
        case New                = 1
        case ManagerCancel      = 2
        case ManagerReject      = 3
        case ManagerAccept      = 4
        case OrderReady         = 5
        case OrderPicked        = 6
        case OrderCompleted     = 7
        case AcceptedByDriver   = 8
        case RejectedByDriver   = 9
        case OntheWay           = 10
        case Arrived            = 11
        case DeliveryStart      = 12
        case ReachedLocation    = 13
        case Completed          = 14
        case Submitted          = 15
    }
    
    enum Tabs:Int {
        case Home           = 0
        case Search         = 1
        case Cart           = 2
        case History        = 3
        case Profile        = 4
    }
    
    enum TypeOfStore:Int {
        case Restaurant     = 1
        case Grocery        = 2
        case Shopping       = 3
        case Marijuana      = 4
        case Launder        = 5
        case Pharmacy       = 6
        case SendAnything   = 7
    }
    
    // Cart Allowed 1 :  Single Cart Single Store
    // Cart Allowed 2 :  Single Cart Multiple store
    // Cart Allowed 3 :  Single Cart Multiple Store Category
    enum TypeOfCart:Int {
        case SingleCartSingleStore     = 1
        case SingleCartMulStore        = 2
        case SingleCartMulStoreCat     = 3
    }
    
    static var SelectedIndex = Tabs.Home
    
    //static let disposeBag = DisposeBag()
    
    class func ShareText() -> String {
        return String(format: StringConstants.ShareText() ,Utility.AppName, Utility.getReferralCode())
    }
    
    
    static let TermsAndCLink                    = Utility.getAppConfig().TermsAndCondition
    
    
    
    /// Additional notes dictionary for Cart
    static var extraNotes = [String:String]()
    
    //Stripe Keys
    static var StripeKey:String {
        let appStripe = Utility.getAppConfig().StripeKey
        if appStripe.length > 0 {
            return appStripe
        }
        return "pk_test_IBYk0hnidox7CDA3doY6KQGi"
    }
    
    
    
    //Cart
    static var CartCount                    = 0
    static var TotalCartPrice:Float         = 0
    static var TotalCartPriceWithTax:Float  = 0
    static var TotalDeliveryPrice:Float     = 0
    static var DriverTip:Float              = 0
    static var TotalconvenienceFee:Float    = 0
    static var ExpressFee:Float             = 0
    static var CartDiscount:Float           = 0
    static var Cart:[Cart]                  = []
    static var Reachable                    = true
    static var laundrycartexists            = false
 
    class func menuTitles() -> [String] {
        return [StringConstants.ManageAddress(),
StringConstants.Payment(),
StringConstants.Favourite(),
StringConstants.WishList(),
StringConstants.Offers(),
StringConstants.IdentityCard(),
StringConstants.FAQs(),
StringConstants.ReferFriends(),
StringConstants.HelpAndSupport(),
StringConstants.Language(),
StringConstants.Logout(),
StringConstants.MyOrders(),
StringConstants.HelpAndSupport()]
    }
    
    class func status() -> [String] {
        return [StringConstants.OrderConfirmed() ,StringConstants.DriverAssigned(),StringConstants.DriverAtStore(),StringConstants.OrderPickedUp(),StringConstants.DriverLocation(),StringConstants.OrderDelivered(),StringConstants.NewOrder()]
    }
    
    class func orderHelp() -> [String] {
        return [StringConstants.HaveNotReceived(),StringConstants.MissingOrder(),StringConstants.DifferentOrder(),StringConstants.SpillageOrder(),StringConstants.Received(),StringConstants.PaymentQuery()]
    }
    class func helpQuery() -> [String] {
        return [StringConstants.GeneralQueries(),StringConstants.Legal()]
    }
    class func PaymentMethods() -> [String] { // Use for Profile Payment
        return [StringConstants.Wallet(),StringConstants.CreditAndDebit(),StringConstants.PayOnDelivery()]
    }
    
    class func PaymentMethodsForCheckOut() -> [String] { // use for Checkout Page
        return [StringConstants.Wallet(),StringConstants.CreditAndDebit(),StringConstants.iDeal(), StringConstants.PayOnDelivery()]
    }
    class func GeneralQuery() -> [String] {
        return [StringConstants.PlaceOrderQuery(),StringConstants.UnableToLogin(),StringConstants.PaymentRelatedQ(),StringConstants.CouponQuery()]
    }
    class func Legal() -> [String] {
        return [StringConstants.TermsOfUse(),StringConstants.PrivacyPolicy(),StringConstants.Cancellations(),StringConstants.TermsOfUseForLoopz()]
    }
}

//DateFormats
struct DateFormat {
    static let DateAndTimeFormatServer                  = "yyyy-MM-dd HH:mm:ss"
    static let DateAndTimeFormatServerGET               = "yyyyMMddHHmmss"
    static let DateFormatToDisplay                      = "dd MMM yyyy"
    static let DateFormatServer                         = "yyyy-MM-dd"
    static let TimeFormatServer                         = "HH:mm:ss"
    static let TimeFormatToDisplay                      = "hh:mm a"
    static let dateTime                                 = "EEE dd MMM yyyy"
    static let ISODateFormat                            = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let Year                                     = "yyyy"
    static let Short                                    = "yy"
    static let DateAndTimeForServer                     = "YYYY-MM-dd HH:MM"
}


struct money {
     static let addMoney = [100,200,300]
}

/// Database
struct DataBase {
    static let CartCouchDB                              = "cartDB"
    static let AddressCouchDB                           = "addressDB"
    static let SavedAddressCouchDB                      = "savedAddressDB"
    static let SuperStoreCouchDB                        = "superStoreDB"
    static let MYDB                                     = "mydb"
}
struct DBStructure {
    var customerName                                    = "DB"
    var value:[AnyObject]                               = []
    var nameKey                                         = "Name"
    var valueKey                                        = "Value"
}
