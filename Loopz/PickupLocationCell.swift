//
//  PickupLocationCell.swift
//  DelivX
//
//  Created by 3EMBED on 20/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PickupLocationCell: UITableViewCell {
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressTagTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Fonts.setPrimaryRegular(addressLabel)
        addressLabel.textColor = Colors.lightBlueBlack
        addressLabel.adjustsFontSizeToFitWidth = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateAddress(data:Address){
        
        var address = ""
        if data.FlatNo.length > 0{
            address = data.FlatNo + " "
        }
        if data.AddLine1.length > 0{
            address = address + "\(data.AddLine1 ),"
        }
        if data.AddLine2.length > 0{
            address = address + "\(data.AddLine2 ),"
        }
        if data.City.length > 1{
            address = address + "\(data.City ),"
        }
        if data.State.length > 1{
            address = address + "\(data.State )-"
        }
        if data.Zipcode.length > 0{
            address = address + "\(data.State ),"
        }
        if data.Country.length > 0{
            address = address + "\(data.Country )"
        }
        addressLabel.text = address
        
        switch data.Tag {
        case StringConstants.Home():
            let image = UIImage(named: "HomeAddress")?.withRenderingMode(.alwaysTemplate)
            iconButton.setImage(image, for: .normal)
        case StringConstants.Work():
            let image = UIImage(named: "WorkAddress")?.withRenderingMode(.alwaysTemplate)
            iconButton.setImage(image, for: .normal)
        default:
            let image = UIImage(named: "OtherAddress")?.withRenderingMode(.alwaysTemplate)
            iconButton.setTitle(data.Tag, for: .normal)
            iconButton.setImage(image, for: .normal)
            
        }
     
        iconButton.tintColor = Colors.lightBlueBlack
         addressTagTitle.text = data.Tag
    }
    
}

