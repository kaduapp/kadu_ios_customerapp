//
//  LaunchUIE.swift
//  UFly
//
//  Created by 3 Embed on 08/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension LaunchVC {

    /// Setup Detect button and Setup Manual Button
    func validateUI() {
        //Setup Detect button
        if Utility.getAddress().FullText.length == 0 {
            playVideo()
        }
        self.view.bringSubviewToFront(self.bottomView)
        Helper.setButton(button: detectLocationButton, view: UIView(), primaryColour: Colors.AppBaseColor, seconderyColor:Colors.SecondBaseColor,shadow: true)
        if Utility.getLanguage().Code == "es" {
           
            Helper.setButtonTitle(normal: "DETECTAR MI UBICACIÓN", highlighted: " DETECTAR MI UBICACIÓN", selected: " DETECTAR MI UBICACIÓN", button: detectLocationButton)
        }else {
            Helper.setButtonTitle(normal: StringConstants.DetectYourLocation(), highlighted: StringConstants.DetectYourLocation(), selected: StringConstants.DetectYourLocation(), button: detectLocationButton)
        }
       
        Fonts.setPrimaryMedium(detectLocationButton)
        detectLocationButton.buttonTextAlignMent()
        setManualButton.buttonTextAlignMent()
        //Setup Manual Button
        var colour = Colors.SecondBaseColor
        if Changebles.DarkSplash == false {
            colour = Colors.PrimaryText
        }
        Helper.setButton(button: setManualButton, view: UIView(), primaryColour: .clear, seconderyColor: .white,shadow: true)
        Fonts.setPrimaryMedium(setManualButton)
        if Utility.getLanguage().Code == "es" {
            Helper.setButtonTitle(normal: "Escoger localización A mano", highlighted: "Escoger localización A mano", selected: "Escoger localización A mano", button: setManualButton)
        }else {
        //
        Helper.setButtonTitle(normal: StringConstants.SetLocation() + " " + StringConstants.Manually(), highlighted: StringConstants.SetLocation() + " " + StringConstants.Manually(), selected: StringConstants.SetLocation() + " " + StringConstants.Manually(), button: setManualButton)
      
        Helper.updateText(text: (setManualButton.titleLabel?.text)!, subText: StringConstants.Manually(), setManualButton, color: Colors.AppBaseColor,link: "")
        }
    }
  
    /// setup bottom view
    func setView() {
        var length = 0
        if Utility.getAddress().FullText.length > 0 {
            length = 0
        }else{
            launchVM.locationManager = LocationManager.shared
            length = 112
        }
        UIView.animate(withDuration: 0.5) {
            
            self.buttonsHight.constant = CGFloat(length)
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
            
        }
        
    }
    
    
    /// Play Video
    func playVideo() {
        guard let path = Bundle.main.path(forResource: "Splash", ofType:".mp4") else {
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        layer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.clear.cgColor
//        layer?.repeatCount = 10
        layer.frame = self.view.frame
        layer.videoGravity = .resizeAspectFill
        self.view.layer.addSublayer(layer)
        player.play()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { _ in
            player.seek(to: CMTime.zero)
            player.play()
        }
    }
}

