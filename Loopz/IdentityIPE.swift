//
//  IdentityIPE.swift
//  UFly
//
//  Created by 3Embed on 07/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension UploadIdentityVC: ImagePickerModelDelegate {
    
    
    
    /// its imagePicker model Delegate method
    /// when user pics the image this method gets called
    /// - Parameters:
    ///   - selectedImage: it is of type UIImage,if tag - 0 it will be having default image , tag - 1 means user selected image
    ///   - tag: if tag is zero means,user has not chosen any image,or removed earlier image
    func didPickImage(selectedImage: UIImage , tag:Int) {
        
        if tag == 0 {
            
            if uploadIdVM.btnTag == .mmjCard {
                uploadIdVM.authentication.MMJUrl = ""
                uploadIDView.pickedMMJImage = nil
                uploadIDView.mmjCardImage.image = #imageLiteral(resourceName: "UploadDefault")
                uploadIDView.mmjUploadedLabel.isHidden = true
                uploadIDView.mmjCheckMark.isHidden = true
            }
            else {
                uploadIdVM.authentication.IDUrl = ""
                uploadIDView.pickedIDImage = nil
                uploadIDView.idCardImage.image = #imageLiteral(resourceName: "UploadDefault")
                uploadIDView.idUploadedLabel.isHidden = true
                uploadIDView.idCheckMark.isHidden = true
            }
        }
        else{
            
            if uploadIdVM.btnTag == .mmjCard {
                uploadIdVM.authentication.ProfilePic = ""
                uploadIDView.pickedMMJImage = selectedImage
                uploadIDView.mmjCardImage.image = selectedImage
                uploadIDView.mmjUploadedLabel.isHidden = false
                uploadIDView.mmjCheckMark.isHidden = false
            }
            else {
                
                uploadIDView.pickedIDImage = selectedImage
                uploadIDView.idCardImage.image = selectedImage
                uploadIDView.idUploadedLabel.isHidden = false
                uploadIDView.idCheckMark.isHidden = false
            }
            uploadImage()
        }
        
        if uploadIDView.pickedIDImage != nil || uploadIDView.pickedMMJImage != nil {
            Helper.setButton(button: uploadIDView.doneBtn,view: uploadIDView.doneBtnView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        }
        else {
            Helper.setButton(button: uploadIDView.doneBtn,view: uploadIDView.doneBtnView, primaryColour: Colors.DoneBtnNormal, seconderyColor:Colors.SecondBaseColor,shadow: true)
        }
    }
    //upload selected images
    func uploadImage(){
        switch uploadIdVM.btnTag {
        case .mmjCard?:
            uploadIdVM.authentication.MMJUrl = Helper.uploadImage(data:uploadIDView.pickedMMJImage!,path:AmazonKeys.MMJPic, page: .UploadIdentityVC)
            break
        case .iDCard?:
            uploadIdVM.authentication.IDUrl = Helper.uploadImage(data:uploadIDView.pickedIDImage!,path: AmazonKeys.IDPic, page: .UploadIdentityVC)
            break
        case .none:
            break
        }
    }
}
