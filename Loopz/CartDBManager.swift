//
//  CartDBManager.swift
//  UFly
//
//  Created by 3Embed on 20/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CartDBManager: NSObject {
    static let cartDBManager_response = PublishSubject<Bool>()
    
    func getCartDocument() -> [Cart] {
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.CartCouchDB)
        let doc: CBLDocument = CouchDBEvents.getDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        let array = (dic["Value"] as? [Any])!
        var cartArray = [Cart]()
        AppConstants.CartCount = 0
        AppConstants.TotalDeliveryPrice = 0
        AppConstants.TotalCartPrice = 0
        for item in array {
            let data = Cart.init(data: item as! [String:Any])
            AppConstants.TotalDeliveryPrice = AppConstants.TotalDeliveryPrice + data.DeliveryFee
            if data.Products.count > 0 {
                cartArray.append(data)
                AppConstants.CartCount = AppConstants.CartCount + data.Products.count
                for dataProduct in data.Products {
                    var totalAmound = dataProduct.FinalPrice
                    for each in dataProduct.Addons {
                        totalAmound = totalAmound + each.Price
                    }
                    AppConstants.TotalCartPrice = AppConstants.TotalCartPrice + (totalAmound * Float(dataProduct.QTY))
                }
            }
        }
        AppConstants.Cart.removeAll()
        AppConstants.Cart.append(contentsOf: cartArray)
        CartDBManager.cartDBManager_response.onNext(true)
        return cartArray
    }
    
    func deleteCartDocument() {
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.CartCouchDB)
        CouchDBEvents.deleteDocument(database: CouchDBObject.sharedInstance.database, id: customerDocID!)
        AppConstants.CartCount = 0
        AppConstants.Cart.removeAll()
        CartDBManager.cartDBManager_response.onNext(true)
    }
    
    
    func insertCartDocument(data:[Cart]) {
        let customerDocID = UserDefaults.standard.string(forKey: DataBase.CartCouchDB)
        var dataArray = [Any]()
        AppConstants.CartCount = 0
        AppConstants.TotalDeliveryPrice = 0
        AppConstants.TotalCartPrice = 0
        AppConstants.Cart.removeAll()
        for item in data {
            
            AppConstants.TotalDeliveryPrice = AppConstants.TotalDeliveryPrice + item.DeliveryFee
            if item.Products.count > 0 {
                item.setUpDict()
                AppConstants.Cart.append(item)
                dataArray.append(item.Dictionary)
                AppConstants.CartCount = AppConstants.CartCount + item.Products.count
                for dataProduct in item.Products {
                    var totalAmound = dataProduct.FinalPrice
                    for each in dataProduct.Addons {
                        totalAmound = totalAmound + each.Price
                    }
                    AppConstants.TotalCartPrice = AppConstants.TotalCartPrice + (totalAmound * Float(dataProduct.QTY))
                }
            }
        }
        CouchDBEvents.updateDocument(database: CouchDBObject.sharedInstance.database, documentId: customerDocID!, documentArray: dataArray as [AnyObject])
        CartDBManager.cartDBManager_response.onNext(false)
    }
    
    func updateCartItem(data:CartItem) {
        var array = AppConstants.Cart
        var flagItem = false
        var flagStore = false
        var storeLocation   = 0
        var productLocation = 0
        
        var storeIn:Cart? = nil
        var itemIn:CartItem? = nil
        
        if array.count > 0 {
            for singleStore in array {
                storeLocation = array.index(of: singleStore)!
                if singleStore.StoreId == data.StoreId {
                    flagStore = true
                    storeIn = singleStore
                    if singleStore.Products.count > 0 {
                        for singleItem in singleStore.Products {
                            productLocation = singleStore.Products.index(of: singleItem)!
                            if singleItem.ChildProductId == data.ChildProductId {
                                if singleItem.UnitId == data.UnitId{
                                    
                                    if singleItem.selectedAddons.count == data.Addons.count {
                                        var flag = true
                                        var arrayOne:[String] = []
                                        var arrayTwo:[String] = []
                                        if data.Addons.count > 0 {
                                            for i in 0...data.Addons.count - 1 {
                                                arrayOne.append(data.Addons[i].Id)
                                                arrayTwo.append(singleItem.Addons[i].Id)
                                            }
                                        }
                                        for each in arrayOne {
                                            if !arrayTwo.contains(each) {
                                                flag = false
                                            }
                                        }
                                        if flag {
                                            flagItem = true
                                            itemIn = singleItem
                                            let changeqty = data.QTY - (itemIn?.QTY)!
                                            singleStore.TotalPrice = singleStore.TotalPrice + (Float(changeqty) * (itemIn?.FinalPrice)!)
                                            itemIn?.QTY = data.QTY
                                            break
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break
                }
            }
        }
        if flagStore == false {
            storeIn = Cart.init(item: data)
            array.append(storeIn!)
        }else{
            if flagItem == true {
                let countData = (itemIn?.QTY)!
                if countData > 0 {
                    storeIn?.Products[productLocation] = itemIn!
                }else{
                    storeIn?.Products.remove(at: productLocation)
                }
            }else{
                storeIn?.Products.append(data)
            }
            let countData = (storeIn?.Products.count)!
            if countData > 0 {
                array[storeLocation] = storeIn!
            }else{
                array.remove(at: storeLocation)
            }
        }
        insertCartDocument(data: array)
    }
    
    func getCartCount(data:Item) -> CartItem {
        let array = AppConstants.Cart
        if array.count > 0 {
            for singleStore in array {
                if singleStore.StoreId == data.StoreId {
                    if singleStore.Products.count > 0 {
                        for singleItem in singleStore.Products {
                            if singleItem.ChildProductId == data.Id && singleItem.UnitId == data.UnitId {
                                if singleItem.selectedAddons.count == data.RecentAddOns.count {
                                    var flag = true
                                    for each in singleItem.Addons {
                                        if !data.RecentAddOns.contains(each.Id) {
                                            flag = false
                                            break
                                        }
                                    }
                                    if flag {
                                        return singleItem
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return CartItem(data: [:])
    }
    
    func getCartFullCount(data:Item) -> [CartItem] {
        let array = AppConstants.Cart
        var arrayItem:[CartItem] = []
        if array.count > 0 {
            for singleStore in array {
                if singleStore.StoreId == data.StoreId {
                    if singleStore.Products.count > 0 {
                        for singleItem in singleStore.Products {
                            if singleItem.ChildProductId == data.Id && singleItem.UnitId == data.UnitId {
                                arrayItem.append(singleItem)
                            }
                        }
                    }
                }
            }
        }
        if data.AddOnAvailable && arrayItem.count > 1
        {
            for CartItem in arrayItem
            {
                if CartItem.PackId == data.PackId
                {
                    arrayItem.removeAll()
                    arrayItem.append(CartItem)
                    break
                }
                
            }
        }
        
        return arrayItem
    }
    
    func setDeliveryCharge(carts:[Cart]) {
        let array = AppConstants.Cart
        var cartUpdate = [Cart]()
        for cartItem in array {
            for data in carts {
                if cartItem.StoreId == data.StoreId {
                    cartItem.DeliveryFee = data.DeliveryFee
                }
            }
            cartUpdate.append(cartItem)
        }
        insertCartDocument(data: cartUpdate)
    }
    
    
    func getCartStore(data:Store) -> Cart {
        if Utility.getLogin() {
            let array = AppConstants.Cart
            if array.count > 0 {
                for singleStore in array {
                    if singleStore.StoreId == data.Id {
                        if singleStore.Products.count > 0 {
                            return singleStore
                        }
                    }
                }
            }
        }
        return Cart(data: [:])
    }
}
