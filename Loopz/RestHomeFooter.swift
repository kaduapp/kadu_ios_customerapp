//
//  FooterView.swift
//  
//
//  Created by 3Embed on 30/05/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import UIKit

class RestHomeFooter:UICollectionReusableView{
    
    @IBOutlet weak var TitleView: UIView!
    @IBOutlet weak var SubTitleView: UIView!
    @IBOutlet weak var ViewForDottedLine: UIView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var SubTitleLabel: UILabel!
    @IBOutlet weak var CountLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib(){
        setUI()
    }
    
    func setUI(){
        TitleLabel.textColor = Colors.PrimaryText
        SubTitleLabel.textColor = Colors.PrimaryText
        CountLabel.textColor = Colors.SeparatorDark
        
        Fonts.setPrimaryBold(TitleLabel)
        Fonts.setPrimaryMedium(SubTitleLabel)
        Fonts.setPrimaryRegular(CountLabel)
        
        Helper.drawDottedLine(ViewForDottedLine)
        separatorView.backgroundColor = Colors.ScreenBackground
    }
    
    func setData(dataCat:Category,dataSubCat:SubCategory) {
        if dataCat.Id.length == 0 || dataCat.Name.length == 0 {
            TitleView.isHidden = true
            separatorView.isHidden = true
        }else{
            TitleView.isHidden = false
            separatorView.isHidden = false
        }
        if dataSubCat.Id.length == 0 || dataSubCat.Name.length == 0 {
            SubTitleView.isHidden = true
        }else{
            SubTitleView.isHidden = false
        }
        TitleLabel.text = dataCat.Name
        SubTitleLabel.text = dataSubCat.Name
        CountLabel.text = "\(dataSubCat.Products.count) " + StringConstants.Items()
    }
}
