//
// ConfirmOrderTVE.swift
// DelivX
//
// Created by 3 Embed on 16/01/18.
// Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

extension ConfirmOrderVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch Utility.getSelectedSuperStores().type {
        case .Launder:
            return CheckoutCell.LaundryCells().count
        case .SendAnything:
            return CheckoutCell.sendPackagesCells().count
        default:
            return CheckoutCell.otherTypeCells().count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var switchCase : CheckoutCell
        
        switch Utility.getSelectedSuperStores().type {
        case .Launder:
            switchCase = CheckoutCell.LaundryCells()[section]
        case .SendAnything:
            switchCase = CheckoutCell.sendPackagesCells()[section]
        default:
            switchCase = CheckoutCell.otherTypeCells()[section]
        }
        
        switch switchCase {
        case .DeliveryType:
            if checkoutVM.booking == 2 {
                return 3
            }else {
                return 2
            }
        case .DeliveryAddress :
            if checkoutVM.pickup == 1 {
                return 1
            }
            return 0
        case .LaundrySlots:
            return 2
        case .Taxes :
            var dataCount = 4 + 2 //driverTip and conveince Fee
            if checkoutVM.taxArray.count > 0 {
                dataCount = dataCount + 1 + checkoutVM.taxArray.count
            }
            return dataCount
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var dataSchdule = [CheckoutTime()]
        var color = Colors.PickupBtn
        dataSchdule = checkoutVM.dataScheduleForPickUp
        if checkoutVM.pickup == 1 {
            color = Colors.DeliveryBtn
            dataSchdule = checkoutVM.dataScheduleForDelivery
        }
        
        var switchCase : CheckoutCell
        
        switch Utility.getSelectedSuperStores().type {
        case .Launder:
            switchCase = CheckoutCell.LaundryCells()[indexPath.section]
        case .SendAnything:
            switchCase = CheckoutCell.sendPackagesCells()[indexPath.section]
        default:
            switchCase = CheckoutCell.otherTypeCells()[indexPath.section]
        }
        
        switch switchCase {
        case .DeliveryType:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ConfirmAddressCell, for: indexPath) as! ConfirmAddressCell
            cell.initialSetup(color: color, card: false)
            switch indexPath.row {
                
            case 0:
                if checkoutVM.booking == 1 {
                    cell.SelectedSchedule(pickup: self.checkoutVM.pickup)
                }else{
                    cell.UnselectedSchedule(pickup: self.checkoutVM.pickup)
                }
                cell.cellSeparotor.isHidden = false
                break
            case 1:
                if checkoutVM.booking == 2 {
                    cell.SelectedSchedule(pickup: self.checkoutVM.pickup)
                }else{
                    cell.UnselectedSchedule(pickup: self.checkoutVM.pickup)
                }
                cell.cellSeparotor.isHidden = true
                cell.changeLabel.isHidden = true
                
                break
            default:
                let dateCell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ScheduleTimeCell, for: indexPath) as! ScheduleTimeCell
                dateCell.updateDateTime(date: checkoutVM.deliveryDate, time: checkoutVM.deliveryTime,pickup: checkoutVM.pickup)
                dateCell.dateSelect.addTarget(self, action:#selector(ConfirmOrderVC.pressed) , for: UIControl.Event.touchUpInside)
                dateCell.timeSelect.addTarget(self, action:#selector(ConfirmOrderVC.pressed) , for: UIControl.Event.touchUpInside)
                return dateCell
            }
            cell.SetSchedule(data: dataSchdule[indexPath.row])
            return cell
            
        case .LaundryPickup:
            if checkoutVM.pickupAddress != nil
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.PickupLocationCell, for: indexPath) as! PickupLocationCell
                cell.updateAddress(data: checkoutVM.pickupAddress!)
                cell.selectionStyle = .none
                return cell
            }else
            {
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelectTableViewCell.self), for: indexPath) as? SelectTableViewCell
                {
                    cell.UpdateCell(checkoutVM.dataForAddressSelection , false)
                    cell.selectionStyle = .none
                    return cell
                }
            }
            
        case .PickupAddress:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ConfirmAddressCell, for: indexPath) as! ConfirmAddressCell
            cell.initialSetup(color: color, card: false)
            if checkoutVM.pickupAddress != nil {
                cell.updateAddress(data: checkoutVM.pickupAddress!)
            }else{
                cell.adjustSpace()
            }
            return cell
            
        case .DeliveryAddress:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ConfirmAddressCell, for: indexPath) as! ConfirmAddressCell
            cell.initialSetup(color: color, card: false)
            if checkoutVM.selectedAddress != nil {
                cell.updateAddress(data: checkoutVM.selectedAddress!)
            }else{
                cell.adjustSpace()
            }
            return cell
            
        case .LaundryDelivery :
            
            if let deliveryAddress = checkoutVM.deliveryAddress ,let pickupAddress = checkoutVM.pickupAddress
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.DeliveryLocationCell, for: indexPath) as! DeliveryLocationCell
                cell.updateAddress(data: deliveryAddress)
                cell.updateselectButtonImage(devileryAddres: deliveryAddress ,PickupAddress:pickupAddress)
                cell.selectionStyle = .none
                cell.delegate = self
                
                return cell
            }else
            {
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelectTableViewCell.self), for: indexPath) as? SelectTableViewCell
                {
                    if checkoutVM.pickupAddress != nil
                    {
                        cell.UpdateCell(checkoutVM.dataForAddressSelection, true)
                        cell.setSameAddressButtonImage(isSelected: checkoutVM.isSameAsPickUpAddress)
                    }else
                    {
                        cell.UpdateCell(checkoutVM.dataForAddressSelection, false)
                    }
                    cell.delegate = self
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .LaundrySlots:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CollectionandDeliverySlotCell.self), for: indexPath) as? CollectionandDeliverySlotCell
            {
                switch indexPath.row {
                case 0:
                    cell.slotTypeLabel.text = StringConstants.CollectionSlot()
                    if let selectedslot = checkoutVM.collectionSlotSelection ,let date = checkoutVM.selectedSlotDate ,let time = checkoutVM.selectedSlotTime
                    {
                        if selectedslot == 10
                        {
                            cell.UpdateCellWithDate(date: date, slot: time)
                        }
                    }
                    break
                case 1:
                    cell.slotTypeLabel.text = StringConstants.DeliverySlot()
                    if let selectedslot = checkoutVM.deliverySlotSelection ,let date = checkoutVM.selectedDeliverySlotDate ,let time = checkoutVM.selectedDeliverySlotTime
                    {
                        if selectedslot == 11
                        {
                            cell.UpdateCellWithDate(date: date, slot: time)
                        }
                    }
                    break
                default:
                    break
                }
                return cell
            }
            return UITableViewCell()
            
        case .DriverTip:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TipTableViewCell.self), for: indexPath) as? TipTableViewCell
            {
                cell.checkoutVM = checkoutVM
                cell.selectionStyle = .none
                cell.setButtonselected(amount: "  $" + "\(AppConstants.DriverTip)")
                cell.tipcollectionView.reloadData()
                self.addDoneBtn(cell.tipTextField as UITextField)
                return cell
            }
            
        case .Payemnet:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ConfirmAddressCell, for: indexPath) as! ConfirmAddressCell
            cell.initialSetup(color: color, card: true)
            var card = Card.init(data: [:])
            if checkoutVM.selectedCard != nil {
                card = checkoutVM.selectedCard!
            }
            cell.updatePayment(data:card , payment:checkoutVM.paymentHandler )
            return cell
            
        case .Promocode:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ConfirmAddressCell, for: indexPath) as! ConfirmAddressCell
            cell.initialSetup(color: color, card: true)
            cell.SetPromocode(promocode: checkoutVM.promocode)
            cell.rightArrowIcon.tag = 10
            cell.rightArrowIcon.addTarget(self, action:#selector(ConfirmOrderVC.removeBtnPressed) , for: UIControl.Event.touchUpInside)
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaymentDescribeCell.self), for: indexPath) as! PaymentDescribeCell
            var tax = Tax.init(data: [:])
            cell.valueLabel.isHidden = false
            Fonts.setPrimaryRegular(cell.titleLabel)
            switch indexPath.row {
            case 0:
                tax.Name = StringConstants.SubTotal()
                tax.Price = AppConstants.TotalCartPrice
                break
            case 1:
                tax.Name = StringConstants.DeliveryCharge()
                tax.Price = AppConstants.TotalDeliveryPrice
                break
            case 2:
                tax.Name = StringConstants.Discount()
                tax.Price = checkoutVM.discount
                break
            case 3:
                tax.Name = StringConstants.YourSavings()
                tax.Price = AppConstants.CartDiscount + checkoutVM.discount
                break
            case 4:
                tax.Name = StringConstants.DriverTip()
                tax.Price = AppConstants.DriverTip
                break
            case 5:
                if Utility.getLanguage().Code == "es"{
                    tax.Name = "Costes de Gestión"
                    tax.Price = AppConstants.TotalconvenienceFee
                }else{
                    tax.Name = StringConstants.Conveniencefee()
                    tax.Price = AppConstants.TotalconvenienceFee
                }
                break
            case 6:
                tax.Name = StringConstants.Taxes().uppercased()
                tax.Price = 0
                cell.valueLabel.isHidden = true
                Fonts.setPrimaryMedium(cell.titleLabel)
                break
            default:
                tax = checkoutVM.taxArray[indexPath.row - 7]
                break
            }
            cell.setData(data: tax)
            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var switchCase : CheckoutCell
        switch Utility.getSelectedSuperStores().type {
        case .Launder:
            switchCase = CheckoutCell.LaundryCells()[indexPath.section]
        case .SendAnything:
            switchCase = CheckoutCell.sendPackagesCells()[indexPath.section]
        default:
            switchCase = CheckoutCell.otherTypeCells()[indexPath.section]
        }
        
        switch switchCase {
        case .LaundryPickup:
            if checkoutVM.pickupAddress != nil
            {
                return 119
            }
            else
            {
                return 80
            }
        case .LaundryDelivery:
            if checkoutVM.pickupAddress != nil && checkoutVM.deliveryAddress != nil
            {
                return 161
            }
            else if checkoutVM.pickupAddress != nil{
                return 140
            }
            else
            {
                return 80
            }
            
        case .LaundrySlots:
            return 106
            
        case .DriverTip:
            
            var height:CGFloat = UITableView.automaticDimension
            
            if checkoutVM.tipArray.count == 0
            {
                
                height = 0
                
            }
            
            return height
            
        case .Taxes:
            
            var height:CGFloat = UITableView.automaticDimension
            
            switch indexPath.row{
                
            case 0 :
                if AppConstants.TotalCartPrice <= 0{
                    height = 0
                }
            case 1:
                if AppConstants.TotalDeliveryPrice <= 0{
                    height = 0
                }
            case 2:
                if checkoutVM.discount <= 0{
                    height = 0
                }
            case 3:
                if AppConstants.CartDiscount + checkoutVM.discount <= 0{
                    height = 0
                }
            case 4:
                if checkoutVM.tipArray.count == 0{
                    height = 0
                }
            default:
                print("No Action")
            }
            
            return height
            
        default:
            return UITableView.automaticDimension
            
        }
    }
    
}
extension ConfirmOrderVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        var switchCase : CheckoutCell
        switch Utility.getSelectedSuperStores().type {
        case .Launder:
            switchCase = CheckoutCell.LaundryCells()[section]
        case .SendAnything:
            switchCase = CheckoutCell.sendPackagesCells()[section]
        default:
            switchCase = CheckoutCell.otherTypeCells()[section]
        }
        
        if checkoutVM.pickup != 1 && switchCase == .DeliveryAddress {
            return 0
        }
        if checkoutVM.tipArray.count == 0 && switchCase == .DriverTip {
            return 0
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView:HeaderView? = nil
        var stringHead = ""
        
        var switchCase : CheckoutCell
        switch Utility.getSelectedSuperStores().type {
        case .Launder:
            switchCase = CheckoutCell.LaundryCells()[section]
        case .SendAnything:
            switchCase = CheckoutCell.sendPackagesCells()[section]
        default:
            switchCase = CheckoutCell.otherTypeCells()[section]
        }
        
        switch switchCase
        {
        case .DeliveryType:
            
            stringHead = StringConstants.OrderTimeTitle()
            
        case .LaundryPickup :
            stringHead = StringConstants.PickupLocationLower()
            
        case .PickupAddress:
            stringHead = StringConstants.PickupLocationLower()
            
        case .DeliveryAddress, .LaundryDelivery:
            if checkoutVM.pickup == 1 {
                stringHead = StringConstants.DeliveryLocationLower()
            }else{
                stringHead = ""
            }
        case .Payemnet:
            stringHead = StringConstants.PaymentMethod()
            
        case .Promocode:
            stringHead = StringConstants.Promocode()
            break
            
        case .LaundrySlots:
            stringHead = StringConstants.collectionandDeliveryLower()
            
        case .DriverTip:
            stringHead = StringConstants.TipYourCourier()
            
        default:
            stringHead = StringConstants.PaymentBreak()
        }
        
        headerView = Helper.showHeader(title: stringHead,showViewAll: false) as? HeaderView
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        var switchCase : CheckoutCell
        
        switch Utility.getSelectedSuperStores().type {
        case .Launder:
            switchCase = CheckoutCell.LaundryCells()[indexPath.section]
        case .SendAnything:
            switchCase = CheckoutCell.sendPackagesCells()[indexPath.section]
        default:
            switchCase = CheckoutCell.otherTypeCells()[indexPath.section]
        }
        
        switch switchCase {
        case .DeliveryType:
            
            switch indexPath.row {
            case 0: //Now
                let cell = tableView.cellForRow(at: indexPath) as! ConfirmAddressCell
                if checkoutVM.booking == 2 {
                    checkoutVM.booking = 1
                    cell.SelectedSchedule(pickup: self.checkoutVM.pickup)
                    if tableView.numberOfRows(inSection: indexPath.section) == 3 {
                        tableView.deleteRows(at: [IndexPath.init(row: 2, section: 0)], with: .top)
                    }
                }
                let cellTemp = tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as! ConfirmAddressCell
                cellTemp.UnselectedSchedule(pickup: self.checkoutVM.pickup)
                break
            case 1: //Schedule
                let cell = tableView.cellForRow(at: indexPath) as! ConfirmAddressCell
                checkoutVM.deliveryDate = Date().addingTimeInterval(TimeInterval(Utility.getAppConfig().LaterBookingBuffer))
                checkoutVM.deliveryTime = Date().addingTimeInterval(TimeInterval(Utility.getAppConfig().LaterBookingBuffer))
                if checkoutVM.booking == 1 {
                    checkoutVM.booking = 2
                    cell.SelectedSchedule(pickup: self.checkoutVM.pickup)
                    if tableView.numberOfRows(inSection: indexPath.section) == 2 {
                        tableView.insertRows(at: [IndexPath.init(row: 2, section: 0)], with: .top)
                    }
                }
                if let cellTemp = tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? ConfirmAddressCell {
                    cellTemp.UnselectedSchedule(pickup: self.checkoutVM.pickup)
                }
            default: //Calender
                _ = tableView.cellForRow(at: indexPath) as! ScheduleTimeCell
                break
            }
        case .LaundryPickup ,.DeliveryAddress,.PickupAddress:
            
            performSegue(withIdentifier: UIConstants.SegueIds.checkoutToSavedAddress, sender: indexPath)
            
            
            
        case .LaundryDelivery:
            if checkoutVM.pickupAddress != nil
            {
                performSegue(withIdentifier: UIConstants.SegueIds.checkoutToSavedAddress, sender: indexPath)
            }else
            {
                Helper.showAlert(message: StringConstants.SelectAddresss(), head: "", type: 1)
            }
        case .LaundrySlots:
            if let _ = tableView.cellForRow(at: indexPath) as? CollectionandDeliverySlotCell
            {
                if (indexPath.row == 0)
                {
                    checkoutVM.collectionSlotSelection = 10
                    self.performSegue(withIdentifier: UIConstants.SegueIds.collectionSlot, sender: (checkoutVM.collectionSlotSelection ,indexPath.row+1,checkoutVM.pickupSlotID,checkoutVM.pickupDateID))
                    
                }else
                {
                    if checkoutVM.pickupSlotID != nil
                    {
                        checkoutVM.deliverySlotSelection = 11
                        self.performSegue(withIdentifier: UIConstants.SegueIds.collectionSlot, sender: (checkoutVM.deliverySlotSelection ,indexPath.row+1,checkoutVM.deliverySlotID,checkoutVM.deliveryDateID))
                    }else
                    {
                        Helper.showAlert(message:StringConstants.Selectcollectionslot(), head: "", type: 1)
                    }
                    
                }
                
            }
            
        case .Payemnet:
            if Utility.getSelectedSuperStores().type == .Launder {
                if checkoutVM.pickupSlotID != nil && checkoutVM.deliverySlotID != nil
                {
                    performSegue(withIdentifier:UIConstants.SegueIds.ConfirmToPaymentOp, sender: true)
                }else
                {
                    Helper.showAlert(message:StringConstants.CollectionAndDelivery(), head: "", type: 1)
                }
            }
            else{
                self.performSegue(withIdentifier: UIConstants.SegueIds.ConfirmToPaymentOp, sender: true)
            }
            
        case .Promocode:
            applyPressed()
            
        default:
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @objc func pressed(sender : UIButton!) {
        self.performSegue(withIdentifier: UIConstants.SegueIds.DatePickerVC, sender: sender)
    }
    
    @objc func removeBtnPressed(sender:UIButton){
        if checkoutVM.promocode.length > 0 && sender.tag == 10 {
            Helper.showAlertReturn(message: StringConstants.RemovePromocode(), head: StringConstants.Warning(), type: StringConstants.OK(), closeHide: false, responce: CommonAlertView.ResponceType.Delete)
        }else{
            self.checkoutVM.promocode = ""
            self.checkoutVM.discount = 0
            self.mainTableVew.reloadData()
        }
    }
    
    func applyPressed(){
        if checkoutVM.promocode.length == 0 {
            performSegue(withIdentifier: UIConstants.SegueIds.ConfirmVCToCouponVC, sender: self)
            
        }
    }
}


enum CheckoutCell: CaseIterable {
    case DeliveryType,PickupAddress,DeliveryAddress,Payemnet,Promocode,Taxes,LaundryPickup,DriverTip,LaundryDelivery,LaundrySlots
    
    static func sendPackagesCells() -> [CheckoutCell] {
        return [self.DeliveryType,self.PickupAddress,self.DeliveryAddress,self.Payemnet,self.Promocode,self.Taxes]
    }
    
    static func otherTypeCells() -> [CheckoutCell] {
        return [self.DeliveryType,self.DeliveryAddress,self.Payemnet,self.Promocode,self.DriverTip,self.Taxes]
    }
    
    static func LaundryCells() -> [CheckoutCell] {
        return [self.LaundryPickup,self.LaundryDelivery, self.LaundrySlots, self.Payemnet,self.Taxes]
    }
    
    
}
