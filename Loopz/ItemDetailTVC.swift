//
//  ItemDetailTVC.swift
//  UFly
//
//  Created by 3 Embed on 24/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class ItemDetailTVC: UITableViewCell {
    
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var ImageCollectionView: UICollectionView!
    @IBOutlet weak var compareBtn: UIButton!
    @IBOutlet weak var imagePageControll: UIPageControl!
    
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var subCatName: UILabel!
    @IBOutlet weak var tHC: UILabel!
    @IBOutlet weak var cBD: UILabel!
    @IBOutlet weak var cbdSeperator: UIView!
    
    @IBOutlet weak var oldPriceCutView: UIView!
    @IBOutlet weak var oldPrice: UILabel!
    @IBOutlet weak var newPrice: UILabel!
    @IBOutlet weak var unitSeperator: UIView!
    
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var favLabel: UILabel!
    @IBOutlet weak var addToListLabel: UILabel!
    
    @IBOutlet weak var instructionBtn: UIButton!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var addToListBtn: UIButton!
    @IBOutlet weak var btnsTopSeparator: UIView!
    
    @IBOutlet weak var favButtonClick: UIButton!
    @IBOutlet weak var addtoListClick: UIButton!
    
    var selectedItem:Item? = nil
    var isFromItemList = false
    var itemDetailVM = ItemDetailVM()
     var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /// initial view setup
    func initialSetup(){
       // Helper.removeShadowToNavigationBar(controller: )
        instructionBtn.tintColor =  Colors.AppBaseColor
        favBtn.tintColor =  Colors.AppBaseColor
        addToListBtn.tintColor =  Colors.AppBaseColor
        
        Fonts.setPrimaryRegular(instructionsLabel)
        Fonts.setPrimaryRegular(favLabel)
        Fonts.setPrimaryRegular(addToListLabel)
        
        instructionsLabel.textColor =  Colors.PrimaryText
        favLabel.textColor =  Colors.PrimaryText
        addToListLabel.textColor =  Colors.SecoundPrimaryText
        
        instructionsLabel.text = StringConstants.Instructions()
        favLabel.text = StringConstants.Favouritei()
        addToListLabel.text = StringConstants.AddToList()
        
        Fonts.setPrimaryMedium(itemName)
        Fonts.setPrimaryMedium(storeName)
        
        Fonts.setPrimaryRegular(distance)
        Fonts.setPrimaryRegular(compareBtn)
        Fonts.setPrimaryRegular(discount)
        Fonts.setPrimaryRegular(subCatName)
        Fonts.setPrimaryRegular(tHC)
        Fonts.setPrimaryRegular(cBD)
        Fonts.setPrimaryRegular(oldPrice)
        Fonts.setPrimaryMedium(newPrice)
        
        itemName.textColor =  Colors.PrimaryText
        storeName.textColor =   Colors.SecoundPrimaryText
        distance.textColor =   Colors.SecoundPrimaryText
        discount.textColor =   Colors.AppBaseColor
        subCatName.textColor =   Colors.SecoundPrimaryText
        tHC.textColor =   Colors.AppBaseColor
        cBD.textColor =   Colors.AppBaseColor
        oldPrice.textColor =   Colors.SecoundPrimaryText
        newPrice.textColor =   Colors.PrimaryText
        oldPriceCutView.backgroundColor =  Colors.SeconderyText
        
        imagePageControll.currentPage = 0
        imagePageControll.currentPageIndicatorTintColor =  Colors.AppBaseColor
        imagePageControll.pageIndicatorTintColor =  Colors.SeparatorLight
        separator.backgroundColor =  Colors.SeparatorLight
        unitSeperator.backgroundColor =  Colors.SeparatorLarge
        cbdSeperator.backgroundColor =  Colors.SeparatorLight
        btnsTopSeparator.backgroundColor =  Colors.SeparatorLight
        
        Helper.setUiElementBorderWithCorner(element: compareBtn, radius: 2.0, borderWidth: 1.0, color:  Colors.SeparatorLight)
        Helper.setUiElementBorderWithCorner(element: discount, radius: 2.0, borderWidth: 1.0, color:  Colors.AppBaseColor)
        Helper.setButtonTitle(normal: StringConstants.Compare(), highlighted: StringConstants.Compare(), selected: StringConstants.Compare(), button: compareBtn)
    }
    
    /// this method updates item
    ///
    /// - Parameter data: it is of type item model object 
    func updateCell(data: Item){
        selectedItem = data
        storeName.text = data.StoreName
        tHC.text = "\(data.THC)" + StringConstants.THC()
        cBD.text = "\(data.CBD)" + StringConstants.CBD()
        if data.THC == 0 && data.CBD == 0 {
            tHC.text = ""
            cBD.text = ""
        }
        newPrice.text = Helper.df2so(Double( data.FinalPrice))
        
        if data.Discount > 0{
            oldPrice.isHidden = false
            oldPriceCutView.isHidden = false
            oldPrice.text = Helper.df2so(Double( data.Price))
        }
        else{
          oldPrice.isHidden = true
          oldPriceCutView.isHidden = true
        }
       
        
        if data.AddOnGroups.count > 0 && Utility.getStore().TypeStore == .Restaurant{
               subCatName.text = StringConstants.Customizable()
        }
        else{
            subCatName.text = data.SubCategory
        }
        
        
        distance.text =  "\(Helper.clipDigit(valeu: data.StoreDistanceMiles , digits: 2)) miles away"
        itemName.text = data.Name
        if data.Images.count > 1{
            imagePageControll.numberOfPages = data.Images.count
        }
        else{
            imagePageControll.numberOfPages = 0
        }
        
        if data.Fav == true {
//            self.favBtn.isSelected = true
            favLabel.text = StringConstants.Favourited()
            favLabel.textColor =  Colors.AppBaseColor
            self.favBtn.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
        }
        else{
//            self.favBtn.isSelected = false
            favLabel.textColor =  Colors.SecoundPrimaryText
            favLabel.text = StringConstants.Favouritei()
            self.favBtn.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
        }
        ImageCollectionView.reloadData()
    }
    
    
    func updateItemDetail(FavTrue:Bool){
       
        if (FavTrue){
        self.favLabel.text = StringConstants.Favourited()
        self.favLabel.textColor =  Colors.AppBaseColor
        self.favBtn.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
        }
        else{
        self.favLabel.textColor =  Colors.SecoundPrimaryText
        self.favLabel.text = StringConstants.Favouritei()
        self.favBtn.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
        }
    }
    
    
//    func didGetItemDetail(){
//
//        if !itemDetailVM.ItemDetailVM_response.hasObservers{
//       itemDetailVM.ItemDetailVM_response.subscribe(onNext: {success in
//            switch success {
//            case .FavTrue:
//                self.favLabel.text = StringConstants.Favourited()
//                self.favLabel.textColor =  Colors.AppBaseColor
//                self.favBtn.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
//                break
//            case .FavFalse:
//                self.favLabel.textColor =  Colors.SecoundPrimaryText
//                self.favLabel.text = StringConstants.Favouritei()
//                self.favBtn.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
//                break
//            case .UpdateCart:
//                break
//            case .GetAPIResponse:
//                break
//            }
//        }).disposed(by: self.itemDetailVM.disposeBag)
//    }
//  }
}

/*
// MARK: - UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension ItemDetailTVC: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedItem != nil {
            if collectionView == ImageCollectionView {
                return (self.selectedItem?.Images.count)!
            }
            else {
                return (self.selectedItem?.Units.count)!
            }
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.ItemImageCVC, for: indexPath) as! ItemImageCVC
        
        if selectedItem != nil {
            cell.updateCell(data:selectedItem!, index: indexPath.row)
            cell.imageButton.addTarget(self, action: #selector(ItemDetailVC.imageBtnTapped), for: UIControlEvents.touchUpInside)
            cell.imageButton.tag = indexPath.row
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        let cellWidth = collectionView.frame.size.width//UIScreen.main.bounds.width
        let cellHight = collectionView.frame.size.height
        
        size.height =  cellHight
        size.width  =  cellWidth
        return size
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
    }
    
}
extension ItemDetailTVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        imagePageControll.currentPage = Int(pageNumber)
    }
    
  
    
 }*/
