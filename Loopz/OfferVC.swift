//
//  OfferVC.swift
//  DelivX
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class OfferVC: UIViewController {

    
    @IBOutlet weak var mainTableView: UITableView!
    var emptyView = EmptyView().shared
    var offerVM = OfferVM()
    let disposeBag = DisposeBag()
    var navView = NavigationView().shared
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        offerVM.getOffers()
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.Offers())
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = "   "
    }
    
    func setUI() {
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
        mainTableView.backgroundColor = Colors.ScreenBackground
        emptyView.setData(image:#imageLiteral(resourceName: "EmptyOffer"), title: StringConstants.Nocouponsavailable(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        
        offerVM.offerVM_response.subscribe(onNext: { [weak self]success in
            self?.mainTableView.reloadData()
        }).disposed(by: disposeBag)
        Helper.networkReachabilityChange.subscribe(onNext: { [weak self]dataGot in
            self?.offerVM.getOffers()
        }).disposed(by: self.offerVM.disposeBag)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == String(describing: ViewAllVC.self) {
            if let viewAllVC: ViewAllVC =  segue.destination as? ViewAllVC {
                viewAllVC.viewAllVM.store = Utility.getStore()
                viewAllVC.viewAllVM.arrayOfItems = []
                viewAllVC.viewAllVM.type = 2
                let senderIn = sender as! Offer
                viewAllVC.offerId = senderIn.Id
                viewAllVC.viewAllVM.titleLabel = senderIn.Name
            }
        }
    }
    

}
