//
//  WalletHistoryCell.swift
//  DelivX
//
//  Created by 3Embed on 14/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class WalletHistoryCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var separator: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Fonts.setPrimaryRegular(dateLabel, size: 12)
        Fonts.setPrimaryBold(priceLabel, size: 14)
        Fonts.setPrimaryBold(titleLabel, size: 12)
        Fonts.setPrimaryRegular(bodyLabel, size: 12)
        
        dateLabel.textColor = Colors.SeparatorDark
        priceLabel.textColor = Colors.PrimaryText
        titleLabel.textColor = Colors.PrimaryText
        bodyLabel.textColor = Colors.PrimaryText
        
        separator.backgroundColor = Colors.SeparatorLarge
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(data:Transaction) {
        let resultDate = Helper.getDateString(value: data.DateTxn, format: DateFormat.dateTime, zone: true)
        let resultTime = Helper.getDateString(value: data.DateTxn, format: DateFormat.TimeFormatToDisplay, zone: true)
        dateLabel.text = "\(resultTime), \(resultDate)"
        titleLabel.text = data.Title
        bodyLabel.text = data.Comment
         if data.TypeTxn == 0 {
            
                   let str1 = Helper.df2so(Double( data.Amount))
                   priceLabel.text = "+ " + str1
                   priceLabel.textColor = Colors.Red
            
               }else{
            
                       let str1 = "- "  + Helper.df2so(Double( abs(data.Amount)))
                       priceLabel.text = str1
                       priceLabel.textColor = Colors.DeliveryBtn
            
               }
    }
    
}
