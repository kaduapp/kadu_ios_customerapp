//
//  SearchUIE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


extension  SearchVC {
    
    /// sets navigation
    func setNavigation(){
        
        Helper.transparentNavigation(controller: self.navigationController!)
        self.navigationController?.navigationBar.backgroundColor = Colors.SecondBaseColor
        if let navBarCount = self.navigationController?.viewControllers.count {
            if navBarCount > 1{
                NavigationWidth.constant = UIScreen.main.bounds.size.width - 44
            }else{
                NavigationWidth.constant = UIScreen.main.bounds.size.width - 20
            }
        }
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        self.searchTF.addTarget(self, action: #selector(SearchVC.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        //Helper.setShadow(sender: dispensariesProductsView)
        
       
    }
    
    //setup UI
    func setupUI() {
        
        Helper.setUiElementBorderWithCorner(element: searchView, radius: 7, borderWidth: 0, color: UIColor.white)
        cancelButton.setTitle(StringConstants.Cancel(), for: .normal)
        Fonts.setPrimaryMedium(cancelButton)
        
        self.cancelBtnTrailingConstaraint.constant = 5
        self.searchHeaderView.setNeedsUpdateConstraints()
        self.searchHeaderView.layoutIfNeeded()
        noProductsView.setData(image: #imageLiteral(resourceName: "EmptySearch"), title: StringConstants.EmptySearchProduct(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        
    }
    
    
    //hide cancel button
    func hideCancelBtn(){
//        self.cancelBtnTrailingConstaraint.constant = -53
//        self.searchHeaderView.setNeedsUpdateConstraints()
//
//        UIView.animate(withDuration: 0.3) {
//           self.searchHeaderView.layoutIfNeeded()
//            self.cancelButton.isHidden = true
//        }
    }
     //show cancel button
    func showCancelBtn(){
//        self.cancelBtnTrailingConstaraint.constant = 5
//        self.searchHeaderView.setNeedsUpdateConstraints()
//       self.cancelButton.isHidden = false
//        UIView.animate(withDuration: 0.3) {
//            self.searchHeaderView.layoutIfNeeded()
//        }
    }
    
    
    /// shows close Icon if this controller is presenting modally
    func showCloseIcon(){
        
        if searchVM.isFromItemList {
            closeIconWidth.constant = 44
        }
        else {
            closeIconWidth.constant = 0
        }
        
    }
    
    
    func setBanner() {
        dummyView.addSubview(Helper.showUnVerifyNotification().0)
        dummyView.clipsToBounds = true
        if Helper.showUnVerifyNotification().1 {
            bannerHeight.constant = 20
        }else{
            bannerHeight.constant = 0
        }
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
}
