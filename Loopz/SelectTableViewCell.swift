//
//  SelectTableViewCell.swift
//  DelivX
//
//  Created by 3EMBED on 28/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
protocol SelectCellDelegate: class  {
    func sameAddressButtonTapped(_ sender: UIButton)
}
class SelectTableViewCell: UITableViewCell {
    @IBOutlet weak var iconButton: UIButton!
    
    @IBOutlet weak var select_Label: UILabel!
    
    @IBOutlet weak var sameAdressLabel: UILabel!
    @IBOutlet weak var sameAddressBtn: UIButton!
    var delegate: SelectCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let image = UIImage(named: "OtherAddress")?.withRenderingMode(.alwaysTemplate)
        iconButton.setImage(image, for: .normal)
        iconButton.tintColor = Colors.lightBlueBlack
    }
    func UpdateCell(_ data:selectCellType)
    {
        select_Label.text = data.Title
        Fonts.setPrimaryRegular(select_Label)
    }
    func UpdateCell(_ data:selectCellType , _ sameButton:Bool)
    {
        select_Label.text = data.Title
         Fonts.setPrimaryRegular(select_Label)
        if sameButton {
           // sameAddressBtn.isHidden = false
            sameAdressLabel.isHidden = false
        }else
        {
           // sameAddressBtn.isHidden = true
            sameAdressLabel.isHidden = true
        }
        
    }
    
    func setSameAddressButtonImage(isSelected:Bool){
        if isSelected{
          sameAddressBtn.setImage(UIImage(named: "CheckBoxOn") , for: .normal)
        }
        else{
         sameAddressBtn.setImage(UIImage(named: "CheckBoxOff") , for: .normal)
        }
    }
    
    @IBAction func sameAddressButtonTapped(_ sender: Any) {
        delegate?.sameAddressButtonTapped(sender as! UIButton )

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
