//
//  ConfirmPaymentCell.swift
//  DelivX
//
//  Created by 3 Embed on 16/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ConfirmPaymentCell: UITableViewCell {
    
    //    @IBOutlet weak var headerTitle: UILabel!
    //    @IBOutlet weak var headerSeparator: UIView!
    //
    
    
    
    
    @IBOutlet weak var taxValue: UILabel!
    @IBOutlet weak var taxTitle: UILabel!
    
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var gstLabel: UILabel!
    @IBOutlet weak var deliveryChargeLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var itemTotal: UILabel!
    @IBOutlet weak var gst: UILabel!
    @IBOutlet weak var deliveryCharge: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var totalSeparator: UIView!
    
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var applycouponButton: UIButton!
    @IBOutlet weak var deletePromocode: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    
    /// initial setup
    func initialSetup(){
        
        totalSeparator.backgroundColor =  Colors.SeparatorLight
        // headerSeparator.backgroundColor =  Colors.SeparatorLight
        //  Fonts.setPrimaryMedium(headerTitle)
        
        
        Fonts.setPrimaryRegular(taxTitle)
        Fonts.setPrimaryRegular(taxValue)
        
        Fonts.setPrimaryRegular(itemTitleLabel)
        Fonts.setPrimaryRegular(gstLabel)
        Fonts.setPrimaryRegular(deliveryChargeLabel)
        
        Fonts.setPrimaryRegular(discount)
        Fonts.setPrimaryRegular(discountLabel)
        
        Fonts.setPrimaryRegular(itemTotal)
        Fonts.setPrimaryRegular(gst)
        Fonts.setPrimaryRegular(deliveryCharge)
        
        Fonts.setPrimaryMedium(totalLabel)
        Fonts.setPrimaryMedium(total)
        
        total.textColor =  Colors.PrimaryText
        totalLabel.textColor =  Colors.PrimaryText
        discount.textColor =  Colors.AppBaseColor
        
        itemTotal.textColor =  Colors.PrimaryText
        gst.textColor =  Colors.PrimaryText
        deliveryCharge.textColor =  Colors.PrimaryText
        
        itemTitleLabel.textColor =  Colors.PrimaryText
        
        gstLabel.textColor =  Colors.PrimaryText
        deliveryChargeLabel.textColor =  Colors.PrimaryText
        
        taxValue.textColor =  Colors.PrimaryText
        taxTitle.textColor =  Colors.PrimaryText
        
        Fonts.setPrimaryMedium(deletePromocode)
        deletePromocode.setTitle(StringConstants.DeleteCaps().uppercased(), for: .normal)
        
        
        discountLabel.textColor =  Colors.PrimaryText
        
        discountLabel.text = StringConstants.Discount() + " :"
        itemTitleLabel.text = StringConstants.SubTotal() + " :"
        gstLabel.text = ""                   //StringConstants.GST + " :"
        totalLabel.text = StringConstants.ToPay()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
    
    /// updates the payment
    ///
    /// - Parameters:
    ///   - discountValue: is of type float
    ///   - pickup: 1 for delivery 2 for pickup
    func updatePayment(discountValue:Float,pickup: Int,promocode:String){
        itemTotal.text = Helper.df2so(Double( AppConstants.TotalCartPrice))
        total.text = Helper.df2so(Double( AppConstants.TotalCartPriceWithTax + AppConstants.TotalDeliveryPrice - discountValue))
        
        gst.text = ""
        taxValue.text = Helper.df2so(Double( AppConstants.TotalCartPriceWithTax - AppConstants.TotalCartPrice))
        taxTitle.text = StringConstants.Tax()
        
        if discountValue == 0 {
            discount.text = StringConstants.ApplyCoupon()
            deletePromocode.isHidden = true
            gstLabel.text = ""
        }else{
            discount.text = "- " + Helper.df2so(Double( discountValue))
            deletePromocode.isHidden = false
            gstLabel.text = promocode
        }
        if pickup == 2 {
            deliveryCharge.text = ""
            deliveryChargeLabel.text = ""
            deletePromocode.tintColor =  Colors.PickupBtn
            discount.textColor =  Colors.PickupBtn
        }
        else {
            deletePromocode.tintColor =  Colors.DeliveryBtn
            discount.textColor =  Colors.DeliveryBtn
            deliveryCharge.text = Helper.df2so(Double( AppConstants.TotalDeliveryPrice))
            deliveryChargeLabel.text = StringConstants.DeliveryCharge() + " :"
        }
    }
}
