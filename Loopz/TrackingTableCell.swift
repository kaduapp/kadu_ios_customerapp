//
//  TrackingTableCell.swift
//  UFly
//
//  Created by 3 Embed on 06/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class TrackingTableCell: UITableViewCell {
    
    @IBOutlet weak var driverHeight: NSLayoutConstraint!
    @IBOutlet weak var driverETALabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverImage: UIImageView!
    
    @IBOutlet weak var orderStatusImage: UIImageView!
    @IBOutlet weak var orderStatusTitle: UILabel!
    @IBOutlet weak var orderSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    func initialSetup(){
        
        Fonts.setPrimaryBold(orderStatusTitle)
        Fonts.setPrimaryRegular(orderSubTitle)
        orderStatusTitle.textColor = Colors.PrimaryText
        orderSubTitle.textColor = Colors.SecoundPrimaryText
        
        if driverNameLabel != nil{
        Fonts.setPrimaryBold(driverNameLabel)
        Fonts.setPrimaryRegular(driverETALabel)
        driverNameLabel.textColor = Colors.PrimaryText
        driverETALabel.textColor = Colors.SecoundPrimaryText
        Helper.setUiElementBorderWithCorner(element: driverImage, radius: Helper.setRadius(element: driverImage), borderWidth: 1, color: Colors.AppBaseColor)
        }
        
    }
    
    
    func setOrderStatus(order:Order){
     orderStatusImage.image = UIImage.gifImageWithName("live_status")
        
     //   let status = order.Status
        
           orderStatusTitle.text = order.StatusMessage
         orderSubTitle.text = order.StatusMessage
       /* switch status {
        case 1:
//            orderStatusTitle.text = StringConstants.OrderPlaced()
            orderSubTitle.text = StringConstants.YourOrderConfirmed()
            
        case 4:
//            orderStatusTitle.text = StringConstants.AcceptedbyStore()
            orderSubTitle.text = StringConstants.OrderAcceptedByStore()
            
        case 8:
//            orderStatusTitle.text = StringConstants.AcceptedByDriver()
            orderSubTitle.text  =  StringConstants.AcceptedByDriverMessage()
            
        case 10:
//            orderStatusTitle.text = StringConstants.EnrouteStore()
            orderSubTitle.text = StringConstants.EnrouteStoreMessage()
            
        case 11:
//            orderStatusTitle.text = StringConstants.DriverReachedStore()
            orderSubTitle.text = StringConstants.DriverReachedStoreMessage()
            
        case 12:
//            orderStatusTitle.text = StringConstants.WayToYourLocation()
            orderSubTitle.text = StringConstants.WayToYourLocationMessage()
            
        case 13:
//            orderStatusTitle.text = StringConstants.DriverReachedAtLocation()
            orderSubTitle.text = StringConstants.DriverReachedLocationMessage()
            
        case 14:
//            orderStatusTitle.text = StringConstants.DeliveredSuccessfully()
            orderSubTitle.text = StringConstants.DeliveredSuccessfullyMessage()
            
        case 15:
//            orderStatusTitle.text = StringConstants.DeliveryCompleted()
            orderSubTitle.text = StringConstants.DeliveryCompletedMessage()
        default:
            orderStatusTitle.text = ""
            orderSubTitle.text = StringConstants.YourOrderConfirmed()
        }*/
        
        
    }
    
    func setChatStatus(order:Order){
        orderStatusImage.image =  UIImage.init(named: "Rejected")
        orderStatusTitle.text = StringConstants.chatWithUs()
        orderSubTitle.text =  StringConstants.waitTime()
    }

    
    func updateDriverCell(order:Order) {
        if order.Status >= 8 {
        orderStatusTitle.text = StringConstants.DriverAssigned()
        driverHeight.constant = 42
        Helper.setImage(imageView: driverImage, url: order.DriverImage, defaultImage: #imageLiteral(resourceName: "UserDefault"))
        driverETALabel.text = order.ETA
        driverNameLabel.text = order.DriverName
        driverHeight.constant = 42
        orderStatusImage.image = UIImage(named: "DriverAtLocOn")
        }
       
    }
    
}
