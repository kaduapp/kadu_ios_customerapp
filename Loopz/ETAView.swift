//
//  ETAView.swift
//  DelivX
//
//  Created by 3Embed on 08/10/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ETAView: UIView {

    @IBOutlet weak var labelView: UIView!
    @IBOutlet weak var etaLabel: UILabel!
    @IBOutlet weak var etaImageView: UIImageView!
    var obj: ETAView? = nil
    var shared: ETAView {
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[8] as? ETAView
            obj?.frame = CGRect(x: 0, y: 0, width: 100, height: 160)
            Fonts.setPrimaryMedium(obj?.etaLabel as Any)
            obj?.etaImageView.image = #imageLiteral(resourceName: "MapPin")
            if let object = obj?.labelView{
                Helper.setUiElementBorderWithCorner(element: object, radius: 5, borderWidth: 1, color: Colors.PrimaryText)
            }
        }
        return obj!
    }
    
    func setData(title: String) {
        if title == StringConstants.AlmostReached(){
            etaLabel.text = StringConstants.AlmostReached()
            labelView.isHidden = false
        }else if title.sorted().count > 0{
            etaLabel.text = title + "\n" + StringConstants.Away().lowercased()
            labelView.isHidden = false
        }else{
            etaLabel.text = ""
            labelView.isHidden = true
        }
        self.layoutIfNeeded()
        self.updateConstraintsIfNeeded()
    }
}
