//
//  ItemImageTableCell.swift
//  UFly
//
//  Created by 3 Embed on 18/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import ListPlaceholder

class ItemImageTableCell: UITableViewCell {
    @IBOutlet weak var headHeight: NSLayoutConstraint!
    var timer = Timer.init()
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var itemImageCV: UICollectionView!
    
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        timer.invalidate()
        timerSet()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func timerSet() {
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    
    @objc func update(sender: Timer) {
//        let visibleRect = CGRect(origin: itemImageCV.contentOffset, size: itemImageCV.bounds.size)
//        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
//        let indexPathTemp = itemImageCV.indexPathForItem(at: visiblePoint)
//        if itemImageCV.numberOfItems(inSection: 0) > (indexPathTemp?.row)! + 1 {
//            let indexPath = IndexPath.init(row: (indexPathTemp?.row)! + 1, section: 0)
//            itemImageCV.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.right, animated: true)
//        }
    }
}

///UICollectionViewDelegate and UICollectionViewDataSource
extension ItemImageTableCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ItemImageCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.ItemImageCollectionCell, for: indexPath) as! ItemImageCollectionCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        
        let cellWidth = collectionView.frame.size.width-40
        let cellHight = collectionView.frame.size.height-20
        
        size.height =  cellHight
        size.width  =  cellWidth
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
