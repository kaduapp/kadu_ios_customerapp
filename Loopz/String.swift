//
//  File.swift
//  DelivX
//
//  Created by 3Embed on 11/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation


extension String {
    var doubleValue: Double? {
        if self.length == 0 {
            return 0
        }
        return Double(self)
    }
    var floatValue: Float? {
        if self.length == 0 {
            return 0
        }
        return Float(self)
    }
    var integerValue: Int? {
        if self.length == 0 {
            return 0
        }
        return Int(self)
    }
    
    var length: Int {
        return self.sorted().count
    }
    func slice(from: String, to: String) -> String? {

        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }

}
