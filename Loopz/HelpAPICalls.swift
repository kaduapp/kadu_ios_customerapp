//
//  HelpAPICalls.swift
//  DelivX
//
//  Created by 3Embed on 18/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class HelpAPICalls: NSObject {
    static let disposeBag = DisposeBag()
    //Get Store Data
    class func getHelp() ->Observable<[Support]> {
        Helper.showPI(string: StringConstants.Loading())
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeaderWithoutAuth()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, APIEndTails.HelpAndSupport, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var arrayOfData = [Support]()
                    let bodyGet = body as! [String:Any]
                    let array = bodyGet[APIResponceParams.Data] as! [Any]
                    for dataFrom in array {
                        let data = Support.init(data: dataFrom as! [String : Any])
                        arrayOfData.append(data)
                    }
                    observer.onNext(arrayOfData)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: HelpAPICalls.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
}
