//
//  CartSectionFooterView.swift
//  UFly
//
//  Created by 3 Embed on 22/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CartSectionFooterView: UICollectionReusableView {
    
    @IBOutlet weak var addNewItemsBtn: UIButton!
    @IBOutlet weak var addNotesTextview: UITextView!
    @IBOutlet weak var addNotesTextviewHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        addNotesTextview.Semantic()
        Fonts.setPrimaryRegular(addNotesTextview, size: 12)
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem.init(title: StringConstants.Done(), style: .plain, target: self, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        addNotesTextview.inputAccessoryView = keyboardToolbar
//        initialSetup()
    }
    
    //initial viewsetup
    func initialSetup(){
        Fonts.setPrimaryBold(addNewItemsBtn)
        Helper.setButtonTitle(normal: StringConstants.AddNewItems(), highlighted: StringConstants.AddNewItems(), selected: StringConstants.AddNewItems(), button: addNewItemsBtn)
        addNewItemsBtn.setTitleColor(Colors.BlueColor, for: .normal)
    }
    
    
    
    /// Set Extra Notes per store
    ///
    /// - Parameter notes: note added for store
    func setExtraNotes(notes:String){
        if  notes.isEmpty{
            self.addNotesTextview.text = StringConstants.addExtraNotes()
            self.addNotesTextview.textColor = UIColor.lightGray
        }
        else{
             self.addNotesTextview.text = notes
            self.addNotesTextview.textColor = UIColor.black
        }
    }
    
}
