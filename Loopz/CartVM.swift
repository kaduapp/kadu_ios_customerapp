//
//  CartVM.swift
//  UFly
//
//  Created by 3Embed on 21/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CartVM: NSObject {
    
    //variables
    let cartManager = CartDBManager()
    var arrayOfCart:[Cart] = []
    var arrayOfTax:[Tax] = []
    var dataItem:Item? = nil
    let disposeBag = DisposeBag()
    var orderType = 1
     enum ResponceType:Int {
        case ReloadList         = 1
        case SuccessNormal      = 2
        case GetFare            = 3
    }

    var storeIds:String{
        var ids = [String]()
        for cart in arrayOfCart{
          ids.append(cart.StoreId)
        }
        let formattedArray = (ids.map{String($0)}).joined(separator: ",")
        return formattedArray
    }
    static let cartVM_response = PublishSubject<(ResponceType,[Cart],[Tax])>()
    
    /// subscription to the Fare API
    func getFare() {
        for item in arrayOfCart {
            if item.MinimumOrder > Double(item.TotalPrice) {
                Helper.showAlert(message: String(format: StringConstants.MinOrderCheck(), item.StoreName), head: StringConstants.Message(), type: 1)
                return
            }
        }
        
        
        
        var type = 0
        if orderType == 2 {
            type = 1
        }else{
            type = 2
        }
        CheckoutAPICalls.checkDeliveryAvaiability(item: Address.init(data: [:]),status: 2,orderType: type).subscribe(onNext: {result in
            if result.0 {
                CartVM.cartVM_response.onNext((ResponceType.GetFare,self.arrayOfCart, self.arrayOfTax))

            }
        }, onError: {error in
        }).disposed(by: APICalls.disposesBag)
    }
    
    
    /// Observe for the Cart GetCart API Defined in CartAPICalls
    func getCart() {
        if Utility.getLogin() || Utility.getGuestLogin() {
            CartAPICalls.getCartData().subscribe(onNext: {success in
                self.arrayOfCart.removeAll()
                self.arrayOfTax.removeAll()
                self.arrayOfCart = success.0
                self.arrayOfTax = success.1
                print(self.arrayOfCart)
                CartVM.cartVM_response.onNext((ResponceType.ReloadList,self.arrayOfCart,self.arrayOfTax))
                CartVM.cartVM_response.onNext((ResponceType.SuccessNormal,self.arrayOfCart,self.arrayOfTax))


            }, onError: {error in
                print(error)
            }).disposed(by: APICalls.disposesBag)
        }
    }
   
    
    /// this method to delete the cart if there is no items inside the cart
    ///
    /// - Parameter cartItem: is of type cartItem Model Object
    
    
    func deleteCart(cartItem:CartItem) {
       AppConstants.extraNotes.removeValue(forKey: cartItem.StoreId)
        CartAPICalls.deleteCartData(item: cartItem).subscribe(onNext: {success in
            if Helper.finalController().isKind(of: CartVC.classForCoder()) {
                self.getCart()
            }
            
             Helper.hidePI()
        }, onError: {error in
            print(error)
        }).disposed(by: APICalls.disposesBag)
    }
    
    
    
    /// this method to delete the cart if there is no items inside the cart
    ///
    /// - Parameter cartItem: is of type cartItem Model Object
    func deleteCartInCartVC(cartItem:CartItem) {
        AppConstants.extraNotes.removeValue(forKey: cartItem.StoreId)
        CartAPICalls.deleteCartData(item: cartItem).subscribe(onNext: {success in
            if Helper.finalController().isKind(of: CartVC.classForCoder()) {
                self.getCart()
            }
        }, onError: {error in
            print(error)
        }).disposed(by: APICalls.disposesBag)
    }
    
    /// this method updates the cart whenever items gets added or removed from Cart
    ///
    /// - Parameter cartItem: is of type CartItem Model Object
    func updateCart(cartItem:CartItem) {
        CartAPICalls.updateCartData(item: cartItem).subscribe(onNext: {success in
            if Helper.finalController().isKind(of: CartVC.classForCoder()) {
                self.getCart()
            }
            CartVM.cartVM_response.onNext((ResponceType.ReloadList,self.arrayOfCart,self.arrayOfTax))
            CartVM.cartVM_response.onNext((ResponceType.SuccessNormal,self.arrayOfCart,self.arrayOfTax))

        }, onError: {error in
            print(error)
        }).disposed(by: APICalls.disposesBag)
    }
    
    
    ///this method observe the cart API a
    ///adds items to cart
    /// - Parameter cartItem: it is of type CartItem model object
    func addCart(cartItem:CartItem) {
        CartAPICalls.addCartData(item: cartItem).subscribe(onNext: {success in
          //  if Helper.finalController().isKind(of: CartVC.classForCoder()) {
                self.getCart()
          //  }
        }, onError: {error in
            print(error)
        }).disposed(by: APICalls.disposesBag)
    }
    
    
    /// This Method is invoked to clear cart.
    ///
    /// - Parameter cartItem: cart item object
    func clearCart(handler:@escaping (Bool) -> ()) {
        CartAPICalls.clearCartData().subscribe(onNext: {success in
         AppConstants.extraNotes.removeAll()
         handler(true)
        }, onError: {error in
            print(error)
         handler(false)
        }).disposed(by: APICalls.disposesBag)
    }
    
    func deleteSendPackageItem(cartItem:CartItem) {
         CartAPICalls.deleteSendPackageItem(item: cartItem).subscribe(onNext: {success in
             self.getCart()
             Helper.hidePI()
         }, onError: {error in
             print(error)
         }).disposed(by: APICalls.disposesBag)
     }
    
}
