//
//  ProfileAPICalls.swift
//  UFly
//
//  Created by 3Embed on 10/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire

class AddressAPICalls: NSObject {
   static let disposeBag = DisposeBag()
    //Get Address
    class func getAddress() -> Observable<[Address]> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.GettingAddress())
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, "\(APIEndTails.Address)/\(Utility.getProfileData().SId)", parameters: nil, headers: header).debug().subscribe(onNext: { (head,body) in
                print(head,body)
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var arrayOfData = [Address]()
                    let bodyGet = body as! [String:Any]
                    let array = bodyGet[APIResponceParams.Data] as! [Any]
                    for dataFrom in array {
                        let data = Address.init(data: dataFrom as! [String : Any])
                        arrayOfData.append(data)
                    }
                    SavedAddressDBManager().updateAddressDocument(data: arrayOfData)
                    observer.onNext(arrayOfData)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any] ,status:head.statusCode)
                }
                
            }, onError: { (Error) in
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
        
    }
    
    
    //Delete Address
    class func deleteAddress(address:Address)  -> Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.DeletingAddress())
        let header = Utility.getHeader()
        let url = APIEndTails.Address + "/" + address.Id
        return Observable.create { observer in
            RxAlamofire.requestJSON(.delete, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    observer.onNext(true)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    //Add Address
    class func addAddress(location:Location,address:Address) -> Observable<Address>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.AddingAddress())
        let header = Utility.getHeader()
        var line1 = ""
        var line2 = ""
     
        if location.MainText.length > 0{
            line1 =  "\(location.MainText )"
        }
        if location.Name.length > 0 && !line1.contains(location.Name){
            if line1.length == 0{
            line1 = "\(location.Name )"
            }
            else{
             line2 = "\(location.Name )"
            }
        }
  
        let params : [String : Any] =    [
            APIRequestParams.AddLine1                :line1,
            APIRequestParams.AddLine2                :line2,
            APIRequestParams.City                    :location.City,
            APIRequestParams.State                   :location.State,
            APIRequestParams.Country                 :location.Country,
            APIRequestParams.PlaceId                 :location.PlaceId,
            APIRequestParams.Pincode                 :location.Zipcode,
            APIRequestParams.Latitude                :location.Latitude,
            APIRequestParams.Longitude               :location.Longitude,
            APIRequestParams.Tag                     :address.Tag,
            APIRequestParams.FlatNumber              :address.FlatNo,
            APIRequestParams.LandMark                :address.LandMark,
            APIRequestParams.phoneNumber              :address.PhoneNumber
        ]
        
        
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.Address, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let bodyGet = body as! [String:Any]
                    let data = Address.init(data:bodyGet[APIResponceParams.Data] as! [String:Any])
                    observer.onNext(data)
                    observer.onCompleted()
                }else{
                    let data = Address.init(data:[:])
                    observer.onNext(data)
                    observer.onCompleted()
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.hidePI()
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //Update Address
    class func updateAddress(location:Location,address:Address) -> Observable<Address>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        var line1 = ""
        var line2 = ""
        
        if location.FullText.length > 0{
            line1 =  "\(location.FullText )"
        }
        if location.Name.length > 0 && !line1.contains(location.Name){
            if line1.length == 0{
                line1 = "\(location.Name )"
            }
            else{
                line2 = "\(location.Name )"
            }
        }
        
        let params : [String : Any] =    [
            APIRequestParams.AddLine1                :line1,
            APIRequestParams.AddLine2                :line2,
            APIRequestParams.City                    :location.City,
            APIRequestParams.State                   :location.State,
            APIRequestParams.Country                 :location.Country,
            APIRequestParams.PlaceId                 :location.PlaceId,
            APIRequestParams.Pincode                 :location.Zipcode,
            APIRequestParams.Latitude                :location.Latitude,
            APIRequestParams.Longitude               :location.Longitude,
            APIRequestParams.Tag                     :address.Tag,
            APIRequestParams.FlatNumber              :address.FlatNo,
            APIRequestParams.LandMark                :address.LandMark,
            APIRequestParams.IdAddress               :address.Id
        ]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.Address, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let bodyGet = body as! [String:Any]
                    let data = Address.init(data:bodyGet[APIResponceParams.Data] as! [String:Any])
                    observer.onNext(data)
                    observer.onCompleted()
                }else{
                    let data = Address.init(data:[:])
                    observer.onNext(data)
                    observer.onCompleted()
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }, onCompleted: {
                Helper.hidePI()
            }) {
                
                }.disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
}
