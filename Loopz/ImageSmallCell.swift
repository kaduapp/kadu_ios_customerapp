//
//  ImageSmallCell.swift
//  DelivX
//
//  Created by 3EMBED on 15/11/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ImageSmallCell: UICollectionViewCell {
    
    @IBOutlet weak var ImageOuterView: UIView!
    @IBOutlet weak var SmallmageView: UIImageView!
    
}
