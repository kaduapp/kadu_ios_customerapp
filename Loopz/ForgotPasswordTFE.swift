//
//  ForgotPWTextFieldDelegate.swift
//  UFly
//
//  Created by Rahul Sharma on 28/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - UITextFieldDelegate
extension ForgotPasswordVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        forgotPWView.phNumSeparator.backgroundColor =  Colors.AppBaseColor
        if forgotPasswordVM.textFieldType == .Phone {
            //  Helper.addDoneButtonOnTextField(tf: textField, vc: self.forgotPWView)
            if textField.text?.sorted().count == 0 {
                textField.text = Helper.setCountryCode()
                forgotPasswordVM.authentication.CountryCode = Helper.setCountryCode()
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        forgotPWView.phNumSeparator.backgroundColor =  Colors.SeparatorLight
        if forgotPasswordVM.textFieldType == .Phone {
            let temp = forgotPasswordVM.authentication.CountryCode
            forgotPasswordVM.authentication.Phone = (textField.text?.replacingOccurrences(of: temp, with: ""))!
        }else{
            forgotPasswordVM.authentication.Email = textField.text!
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        dismissKeyboard()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        forgotPasswordVM.phoneNum.append(string)
        if forgotPasswordVM.textFieldType == .Phone{
            if range.location+1 == forgotPasswordVM.authentication.CountryCode.sorted().count{
                return false
            }
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        if forgotPasswordVM.textFieldType == .Phone {
            let Phone = (textField.text?.replacingOccurrences(of: forgotPasswordVM.authentication.CountryCode, with: ""))!
            
            if (Helper.isPhoneNumValid(phoneNumber: Phone, code:forgotPasswordVM.authentication.CountryCode)) {
                forgotPWView.phNumCheckMark.isHidden = false
                Helper.setButton(button: forgotPWView.nextBtn,view: forgotPWView.nextBtnContainer, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
            }
            else {
                forgotPWView.phNumCheckMark.isHidden = true
                Helper.setButton(button: forgotPWView.nextBtn,view:forgotPWView.nextBtnContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
            }
        }
        else {
            if (Helper.isValidEmail(text: textField.text!)) {
                forgotPWView.phNumCheckMark.isHidden = false
            }
            else {
                forgotPWView.phNumCheckMark.isHidden = true
            }
        }
    }
}



