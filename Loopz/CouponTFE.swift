//
//  CouponTFE.swift
//  DelivX
//
//  Created by 3 Embed on 03/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension CouponVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.tintColor = Colors.PrimaryText
       tempText = textField.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        applyPromocode()
        tempText = textField.text!
        return true
    }
    
    
    func applyPromocode() {

        self.view.endEditing(true)
        if tempText.length == 0 {
            Helper.showAlert(message: StringConstants.PromocodeMissing(), head: StringConstants.Error(), type: 1)
            //self.mainTableView.reloadData()
            return
        }
        checkoutVM.applyPromocode(code: tempText)

    }
}
