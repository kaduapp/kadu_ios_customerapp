//
//  SelectLocationE.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

//MARK:- LocationManager Delegate
extension SelectLocationVC : LocationManagerDelegate {
    
    func didFailToUpdateLocation() {
        
    }
    func didUpdateLocation(location: Location, search:Bool, update:Bool) {
        if update == false {
            selectLocVM.getLocation(location:location)
        }else{
//            var mySet: IndexSet = IndexSet.init()
//            mySet.insert(0)
//            self.mainTableView.reloadSections(mySet, with: .fade)
            self.mainTableView.reloadData()
        }
    }
    func didChangeAuthorization(authorized: Bool) {
        
    }
    func didUpdateSearch(locations: [Location]) {
        if locations.count > 0 || self.addressSearchBar.text?.count == 0{
        selectLocVM.addressList = locations
        mainTableView.reloadData()
        }
    }
    
}

extension SelectLocationVC:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == mainTableView {
            if movedOffset > 0 {
                Helper.setShadow(sender: headView)
            }else{
                Helper.removeShadow(sender: headView)
            }
        }
    }
    
}

