//
//  HeaderVC.swift
//  History
//
//  Created by 3Embed on 04/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class HeaderVC: UIViewController{
    @IBOutlet weak var seperatorView: UIView!

    override func viewDidLoad(){
        super.viewDidLoad()
        self.settingDottedLine(headerView: seperatorView)
    }
    
    func settingDottedLine(headerView:UIView){
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 193/255.0, green: 193/255.0, blue: 193/255.0, alpha: 1).cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [2,2] // 2 is the length of dash, 2 is length of the gap.
        let p0 = CGPoint(x: headerView.bounds.minX, y: headerView.bounds.origin.y)
        let p1 = CGPoint(x: headerView.bounds.maxX, y: headerView.bounds.origin.y)
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        headerView.layer.addSublayer(shapeLayer)
        headerView.clipsToBounds = true
        headerView.layoutIfNeeded()
    }

}

