//
//  MCDViewController.swift
//  
//
//  Created by 3Embed on 29/05/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class RestHomeVC: UIViewController{
    
    @IBOutlet weak var favButton: UIButton!
     @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var viewCartBar: UIView!
    @IBOutlet weak var extraCharges: UILabel!
    @IBOutlet weak var viewCartBtn: UIButton!
    @IBOutlet weak var viewCartBarHight: NSLayoutConstraint!
    @IBOutlet weak var viewCartlabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var itemCount: UILabel!
    
    @IBOutlet weak var navSubTitle: UILabel!
    var checkStoreFavStatus : ((Bool,Store) ->(Void)) = {
        status,Store  in
    }
    
    let backButton =  UIButton_RotateButtonClass()
    let navTitleLabel = UILabel.init()
    var cartVM = CartVM()
    var restuarantsStoreVM = RestuarantsStoreVM()
    
    /// Variable to hold if guest is login.
    /// So if user login on view will appear call cart API on view will appear.
    var isGuestLogin = false
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var collectionViewMain: UICollectionView!
    
   @objc func backButtonAction(_ sender: UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapViewCartBtn(_ sender: UIButton) {
//        self.hideBottom(sender: sender)
//        Helper.changetTabTo(index: AppConstants.Tabs.Cart)
        self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
    } 
    
    @IBAction func tapFavButton(_ sender: Any) {
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
     FavouriteAPICalls.favouriteStore(store:restuarantsStoreVM.store).subscribe(onNext: { [weak self]result in
            if result{
                if let handler = self?.checkStoreFavStatus,let store = self?.restuarantsStoreVM.store {
                    handler(true,store)
                }
                self?.restuarantsStoreVM.store.Fav = true
                self?.favButton.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
            }else{
                if let handler = self?.checkStoreFavStatus,let store = self?.restuarantsStoreVM.store {
                    handler(false,store)
                }
                self?.restuarantsStoreVM.store.Fav = false
                self?.favButton.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
            }
            }, onError: { error in
                print(error)
        }).disposed(by: self.restuarantsStoreVM.disposeBag)
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
//        if Utility.getSelectedSuperStores().type == .Restaurant {
//        performSegue(withIdentifier: String(describing: SearchHomeVC.self), sender: nil)
//        } else {
        performSegue(withIdentifier: String(describing: SearchVC.self), sender: nil)
//        }
    }
    override func viewDidLoad(){
        
        super.viewDidLoad()
        self.cartVM.getCart()
        collectionViewMain.register(UINib(nibName: String(describing: ItemCommonCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self))
         restuarantsStoreVM.getData()
         restuarantsStoreVM.setFavProductSubscribe()
         restuarantsStoreVM.List_Store = Utility.getStore()
        self.setResponseObserver()
         setUI()
        CartDBManager.cartDBManager_response.subscribe(onNext: { [weak self]success in
            self?.showBottom()
        }).disposed(by: self.restuarantsStoreVM.disposeBag)
        
        if !Utility.getLogin(){
            isGuestLogin = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Utility.getLogin() && isGuestLogin{
           self.cartVM.getCart()
            isGuestLogin = false
        }
        self.showBottom()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: ItemDetailVC.self) {
            let button = sender as! Item
            if let nav:UINavigationController = segue.destination as? UINavigationController {
                if let itemDetailVC: ItemDetailVC = nav.viewControllers.first as? ItemDetailVC{
                    //                itemDetailVC.transitioningDelegate = self
                    itemDetailVC.itemDetailVM.item = button
                    itemDetailVC.viewCartHandler = {status in
                      self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
                        }
                }
            }
        }else if segue.identifier == String(describing:MenuVC.self) {
            let menuVC: MenuVC =  segue.destination as! MenuVC
            var array:[Category] = []
            let cat = Category.init(data: [:])
            cat.Name = StringConstants.Recommended()
            array.append(cat)
            array.append(contentsOf: restuarantsStoreVM.categories)
            
            menuVC.controllerSubCategoryArray = array
            menuVC.bottomConstraint = Int(viewCartBarHight.constant + 15)
//            menuVC.selectedId = selectedSubSubCategory
            menuVC.delegate = self
        }
        else if segue.identifier == String(describing:SearchVC.self) {
         let search: SearchVC =  segue.destination as! SearchVC
              search.searchVM.isFromItemList = true
        }
    }
    @IBAction func menuAction(_ sender: Any) {
        performSegue(withIdentifier: String(describing: MenuVC.self), sender: self)
    }
    
    
    func setResponseObserver(){
        
    restuarantsStoreVM.restuarantsStoreVM_response.subscribe(onNext: {[weak self]result in
            if result{
                if let name =  self?.restuarantsStoreVM.store.Name{
                    self?.navTitleLabel.text = name //+ "\n" + subcateory
                    if let subcateory =  self?.restuarantsStoreVM.store.SubcategoryName{
                        self?.navSubTitle.text = subcateory
                    }
                }
                if (self?.restuarantsStoreVM.store.Fav)!{
                    self!.favButton.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
                }
                else{
                    self?.favButton.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
                }
                self?.collectionViewMain.reloadData()
            }
            }, onError: {error in
        }).disposed(by: restuarantsStoreVM.disposeBag)
        
    restuarantsStoreVM.ProductFav_response.subscribe(onNext: {[weak self]result in
            if result{
              self?.collectionViewMain.reloadData()
            }
            }, onError: {error in
        }).disposed(by: restuarantsStoreVM.disposeBag)
    }
    
}


extension RestHomeVC:MenuVCDelegate {
    /// this method updates the subCategories
    ///
    /// - Parameter data: it is of type SubCategory Model
    func didSelectSubCat(data: Any){
        let dataFrom = data as! Category
        if dataFrom.Id.length == 1 && dataFrom.Id == "0" {
            
            if dataFrom.SubCategories.count > 0 || restuarantsStoreVM.recomendedProducts.count > 0 {
            collectionViewMain.scrollToItem(at: IndexPath.init(row: 0, section: 1 ), at: UICollectionView.ScrollPosition.top, animated: true)
            collectionViewMain.contentOffset.y =  collectionViewMain.contentOffset.y - 70
            }
        }else{
            var count = 2
            for each in restuarantsStoreVM.categories {
                if each.Id == dataFrom.Id {
                    collectionViewMain.scrollToItem(at: IndexPath.init(row: 0, section: count ), at: UICollectionView.ScrollPosition.top, animated: true)
                     collectionViewMain.contentOffset.y = collectionViewMain.contentOffset.y - 70
                    break
                }else{
                    count = count + each.SubCategories.count
                }
            }
        }
//        storeView.storeCollectionView.reloadData()
//        selectedSubSubCategory = dataFrom.Id
//        for i in 0...self.itemListingVM.SubSubCatArray.count - 1 {
//            let dataIn = self.itemListingVM.SubSubCatArray[i]
//            if dataIn.Id == selectedSubSubCategory {
//                //                var addIndex = 1
//                //                if itemListingVM.hideStore {
//                //                    addIndex = 0
//                //                }
//                storeView.storeCollectionView.scrollToItem(at: IndexPath.init(row: 0, section: i /*+ addIndex*/), at: UICollectionViewScrollPosition.top, animated: true)
//                self.self.storeView.storeCollectionView.contentOffset.y = self.self.storeView.storeCollectionView.contentOffset.y - 50 //50 is the section header hight
//            }
//        }
    }
}
