//
//  ChangePasswordVew.swift
//  DelivX
//
//  Created by 3 Embed on 08/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ChangePasswordVew: UIView {

    @IBOutlet weak var oldPasswordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var reEnterTF: UITextField!
    
    @IBOutlet weak var oldPWSeperartor: UIView!
    @IBOutlet weak var newPWSeperartor: UIView!
    @IBOutlet weak var reEnterPWSeparator: UIView!
    
    @IBOutlet weak var oldPwTickMark: UIButton!
    @IBOutlet weak var newPwTickMark: UIButton!
    @IBOutlet weak var reEnterPwTickMark: UIButton!
    
    @IBOutlet weak var changeBtnView: UIView!
    @IBOutlet weak var changeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialViewSetup()
        oldPasswordTF.Semantic()
        newPasswordTF.Semantic()
        reEnterTF.Semantic()
    }
    
    func initialViewSetup(){
    
        Fonts.setPrimaryMedium(oldPasswordTF)
        Fonts.setPrimaryMedium(newPasswordTF)
        Fonts.setPrimaryMedium(reEnterTF)
        
        oldPasswordTF.placeholder = StringConstants.OldPassword()
        newPasswordTF.placeholder = StringConstants.NewPassword()
        reEnterTF.placeholder = StringConstants.ReEnterPassword()
       
        oldPWSeperartor.backgroundColor =  Colors.SeparatorLight
        newPWSeperartor.backgroundColor =  Colors.SeparatorLight
        reEnterPWSeparator.backgroundColor =  Colors.SeparatorLight
        
        Helper.setButton(button: changeBtn,view:changeBtnView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.Change(), highlighted: StringConstants.Change(), selected: StringConstants.Change(), button: changeBtn)
        
    }
    
}
