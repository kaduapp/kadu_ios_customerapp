//
//  OfferTVE.swift
//  DelivX
//
//  Created by 3Embed on 23/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension OfferVC: UITableViewDataSource,UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        if offerVM.arrayOfOffers.count == 0 {
            if offerVM.loaded == true {
                tableView.backgroundView = emptyView
            }
            return 0
        }else {
            tableView.backgroundView = nil
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerVM.arrayOfOffers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OfferTVCell.self), for: indexPath) as!OfferTVCell
        cell.setData(data: offerVM.arrayOfOffers[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: String(describing: ViewAllVC.self), sender: offerVM.arrayOfOffers[indexPath.row])
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return Helper.showHeader(title: StringConstants.AvailableOffers(), showViewAll: false)
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension OfferVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableView {
            if scrollView.contentOffset.y > 0 {
                if (self.navigationController != nil) {
                    Helper.addShadowToNavigationBar(controller: self.navigationController!)
                }
            }else{
                if (self.navigationController != nil) {
                    Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                }
            }
        }
    }
}
