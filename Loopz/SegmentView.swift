//
//  SegmentView.swift
//  History
//
//  Created by 3Embed on 04/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class SegmentView: UIScrollView
{
    var selectedSegmentViewColor: UIColor?
    {
        didSet
        {
            selectedSegmentView?.backgroundColor = selectedSegmentViewColor
        }
    }
    var selectedSegmentViewBarColor: UIColor?
    {
        didSet
        {
            SelectedSegmentViewBar?.backgroundColor = selectedSegmentViewBarColor
        }
    }
    
    var titleColor: UIColor?
    {
        didSet {
            for segment in segments {
                segment.titleColor(titleColor!)
            }
        }
    }
    
    var selectedTitleColor: UIColor?
    {
        didSet {
            for segment in segments {
                segment.selectedTitleColor(selectedTitleColor)
            }
        }
    }
    
    var segmentBackgroundColor: UIColor?
    {
        didSet {
            for segment in segments {
                segment.backgroundColor = segmentBackgroundColor
            }
        }
    }
    var font: UIFont?
    var selectedSegmentViewHeight: CGFloat?
    let kSegmentViewTagOffset = 100
    var segmentViewOffsetWidth: CGFloat = 10.0
    var segments = [SegmentTab]()
    var segmentContentView: UIView?
    var didSelectSegmentAtIndex: DidSelectSegmentAtIndex?
    var selectedSegmentView: UIView?
    var xPosConstraints: NSLayoutConstraint?
    var contentViewWidthConstraint: NSLayoutConstraint?
    var selectedSegmentViewWidthConstraint: NSLayoutConstraint?
    var contentSubViewWidthConstraints = [NSLayoutConstraint]()
    var controllers: [UIViewController]?
    var SelectedSegmentViewBar:UIView?
    var contentView: ContentView? {
        didSet {
            contentView!.addObserver(self,
                                     forKeyPath: "contentOffset",
                                     options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old],
                                     context: nil)
        }
    }
    
    required override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        bounces = false
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(SegmentView.didChangeSegmentIndex(_:)),
                                               name: NSNotification.Name("DidChangeSegmentIndex"),
                                               object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        contentView!.removeObserver(self,
                                    forKeyPath: "contentOffset",
                                    context: nil)
        
        NotificationCenter.default.removeObserver(self,
                                                  name:NSNotification.Name("DidChangeSegmentIndex"),
                                                  object: nil)
    }
    
    @objc func didChangeSegmentIndex(_ notification: Notification)
    {
        
        //deselect previous buttons
        for button in segments
        {
            button.isSelected = false
        }
        
        // select current button
        let index = notification.object as? Int
        
        if index! < segments.count
        {
            let button = segments[index!]
            button.isSelected = true
        }
    }
    
    func setSegmentsView(_ frame: CGRect)
    {
        
        let segmentWidth = widthForSegment(frame)
        createSegmentContentView(segmentWidth)
        
        var index = 0
        for controller in controllers!
        {
            createSegmentFor(controller, width: segmentWidth, index: index)
            index += 1
        }
        
        createSelectedSegmentView(segmentWidth)
        
        //Set first button as selected
        let button = segments.first!
        button.isSelected = true
    }
    
    func createSegmentContentView(_ titleWidth: CGFloat)
    {
        
        segmentContentView = UIView(frame: CGRect.zero)
        segmentContentView!.translatesAutoresizingMaskIntoConstraints = false
        addSubview(segmentContentView!)
        
        let contentViewWidth = titleWidth * CGFloat((controllers?.count)!)
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[contentView]|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: ["contentView": segmentContentView!,
                                                                           "mainView": self])
        addConstraints(horizontalConstraints)
        
        contentViewWidthConstraint = NSLayoutConstraint(item: segmentContentView!,
                                                        attribute: .width,
                                                        relatedBy: .equal,
                                                        toItem: nil,
                                                        attribute: .notAnAttribute,
                                                        multiplier: 1.0,
                                                        constant: contentViewWidth)
        addConstraint(contentViewWidthConstraint!)
        
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[contentView(==mainView)]|",
                                                                 options: [],
                                                                 metrics: nil,
                                                                 views: ["contentView": segmentContentView!,
                                                                         "mainView": self])
        addConstraints(verticalConstraints)
        
    }
    
    func createSegmentFor(_ controller: UIViewController, width: CGFloat, index: Int)
    {
        
        let segmentView = getSegmentTabForController(controller)
        segmentView.tag = (index + kSegmentViewTagOffset)
        segmentView.translatesAutoresizingMaskIntoConstraints = false
        segmentContentView!.addSubview(segmentView)
        
        if segments.count == 0
        {
            
            let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]",
                                                                       options: [],
                                                                       metrics: nil,
                                                                       views: ["view": segmentView])
            segmentContentView!.addConstraints(horizontalConstraints)
            
        } else {
            
            let previousView = segments.last
            let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:[previousView]-0-[view]",
                                                                       options: [],
                                                                       metrics: nil,
                                                                       views: ["view": segmentView,
                                                                               "previousView": previousView!])
            segmentContentView!.addConstraints(horizontalConstraints)
        }
        
        let widthConstraint = NSLayoutConstraint(item: segmentView,
                                                 attribute: .width,
                                                 relatedBy: .equal,
                                                 toItem: nil,
                                                 attribute: .notAnAttribute,
                                                 multiplier: 1,
                                                 constant: width)
        segmentContentView!.addConstraint(widthConstraint)
        contentSubViewWidthConstraints.append(widthConstraint)
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                                                 options: [],
                                                                 metrics: nil,
                                                                 views: ["view": segmentView])
        segmentContentView!.addConstraints(verticalConstraints)
        segments.append(segmentView)
        
        
    }

    func createSelectedSegmentView(_ width: CGFloat){
        
        let segmentView = UIView()
        SelectedSegmentViewBar = UIView()
        SelectedSegmentViewBar!.backgroundColor = selectedSegmentViewBarColor
        SelectedSegmentViewBar!.isUserInteractionEnabled = false
        SelectedSegmentViewBar!.isExclusiveTouch = true
        SelectedSegmentViewBar!.translatesAutoresizingMaskIntoConstraints = false
        SelectedSegmentViewBar?.layer.cornerRadius = 5
        SelectedSegmentViewBar?.clipsToBounds = true
        segmentView.addSubview(SelectedSegmentViewBar!)
        
        
        
        let widthConstraint = NSLayoutConstraint(item: SelectedSegmentViewBar!, attribute: .width, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 120)
        
        let heightConstraint = NSLayoutConstraint(item: SelectedSegmentViewBar!, attribute: .height, relatedBy: .equal,
                                                  toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 5)
        
        let xConstraint = NSLayoutConstraint(item: SelectedSegmentViewBar!, attribute: .centerX, relatedBy: .equal, toItem: segmentView, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yConstraint = NSLayoutConstraint(item: SelectedSegmentViewBar!, attribute: .centerY, relatedBy: .equal, toItem: segmentView, attribute: .top, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([widthConstraint, heightConstraint, xConstraint, yConstraint])
        
        
        
        segmentView.backgroundColor = selectedSegmentViewColor
        segmentView.translatesAutoresizingMaskIntoConstraints = false
        segmentContentView!.addSubview(segmentView)
        selectedSegmentView = segmentView
        
        xPosConstraints = NSLayoutConstraint(item: segmentView,
                                             attribute: .leading,
                                             relatedBy: .equal,
                                             toItem: segmentContentView!,
                                             attribute: .leading,
                                             multiplier: 1.0,
                                             constant: 0.0)
        segmentContentView!.addConstraint(xPosConstraints!)
        
        let segment = segments.first
        
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:[view(==segment)]",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: ["view": segmentView,
                                                                           "segment": segment!])
        segmentContentView!.addConstraints(horizontalConstraints)
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:[view(height)]|",
                                                                 options: [],
                                                                 metrics: ["height": selectedSegmentViewHeight!],
                                                                 views: ["view": segmentView])
        segmentContentView!.addConstraints(verticalConstraints)
    }
    
    func getSegmentTabForController(_ controller: UIViewController) -> SegmentTab {
        
        var segmentTab: SegmentTab?
        
        if controller.navigationItem.titleView != nil
        {
            segmentTab = SegmentTab.init(view: controller.navigationItem.titleView!)
        }
        else
        {
            
            if let title = controller.title
            {
                segmentTab = SegmentTab.init(title: title)
            }
            else
            {
                segmentTab = SegmentTab.init(title: "")
            }
            
            segmentTab?.backgroundColor = segmentBackgroundColor
            segmentTab?.titleColor(titleColor!)
            segmentTab?.selectedTitleColor(selectedTitleColor!)
            segmentTab?.titleFont(font!)
            
        }
        segmentTab?.didSelectSegmentAtIndex = didSelectSegmentAtIndex
        
        return segmentTab!
    }
    
    func widthForSegment(_ frame: CGRect) -> CGFloat
    {
        
        var maxWidth: CGFloat = 0
        for controller in controllers!
        {
            
            var width: CGFloat = 0.0
            if let view = controller.navigationItem.titleView
            {
                width = view.bounds.width
            }
            else if let title = controller.title
            {
                
                width = title.widthWithConstrainedWidth(.greatestFiniteMagnitude,
                                                        font: font!)
            }
            
            if width > maxWidth
            {
                maxWidth = width
            }
        }
        
        let width = Int(maxWidth + segmentViewOffsetWidth)
        let totalWidth = (width) * (controllers?.count)!
        if totalWidth < Int(frame.size.width)
        {
            maxWidth = (frame.size.width) /  CGFloat((controllers?.count)!)
        }
        else
        {
            maxWidth = CGFloat(width)
        }
        
        return maxWidth
    }
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        
        if let change = change as [NSKeyValueChangeKey : AnyObject]?
        {
            if let old = change[NSKeyValueChangeKey.oldKey], let new = change[NSKeyValueChangeKey.newKey]
            {
                if !(old.isEqual(new))
                {
                    //update selected segment view x position
                    let scrollView = object as? UIScrollView
                    var changeOffset = (scrollView?.contentSize.width)! / contentSize.width
                    let value = (scrollView?.contentOffset.x)! / changeOffset
                    
                    if !value.isNaN
                    {
                        selectedSegmentView?.frame.origin.x = (scrollView?.contentOffset.x)! / changeOffset
                    }
                    
                    //update segment offset x position
                    let segmentScrollWidth = contentSize.width - bounds.width
                    let contentScrollWidth = scrollView!.contentSize.width - scrollView!.bounds.width
                    changeOffset = segmentScrollWidth / contentScrollWidth
                    contentOffset.x = (scrollView?.contentOffset.x)! * changeOffset
                }
            }
        }
    }
    
    func didChangeParentViewFrame(_ frame: CGRect) {
        
        let segmentWidth = widthForSegment(frame)
        let contentViewWidth = segmentWidth * CGFloat((controllers?.count)!)
        contentViewWidthConstraint?.constant = contentViewWidth
        
        for constraint in contentSubViewWidthConstraints
        {
            constraint.constant = segmentWidth
        }
        
        let changeOffset = (contentView?.contentSize.width)! / contentSize.width
        let value = (contentView?.contentOffset.x)! / changeOffset
        
        if !value.isNaN {
            xPosConstraints!.constant = (selectedSegmentView?.frame.origin.x)!
            layoutIfNeeded()
        }
    }
}
