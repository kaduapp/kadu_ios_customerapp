//
//  ForgotPasswordVM.swift
//  UFly
//
//  Created by 3Embed on 19/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ForgotPasswordVM: NSObject {
    
    
    /// this enum indicates the textField type it is of type Int
    ///
    /// - Phone: it is having int value 1 for phone Number Textfield
    /// - Email: it is having int value 2 for emailId Textfield
    enum TextFieldType:Int {
        case Phone = 1
        case Email = 2
    }
    
    var phoneNum:[String] = []
    var textFieldType:TextFieldType?
    var authentication = Authentication()
    let fpVM_response = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    
    /// this method observe the ForgotPassword API Response define in AuthenticationAPICalls
    func callNext() {
        
        var verifyType = 0
        var processType = 0
        var emailOrMobile = ""
        var code = ""
        
        if textFieldType == .Phone {
            verifyType = 1
            processType = 2
            emailOrMobile = authentication.Phone
            code = authentication.CountryCode
            
        }
        else {
            verifyType = 2
            processType = 2
            code = ""
            emailOrMobile = authentication.Email
        }
        
        AuthenticationAPICalls.ForgotPassword(emailOrMobile: emailOrMobile, code: code, VerifyType: verifyType, ProcessType: processType).subscribe(onNext: {[weak self]result in
            self?.fpVM_response.onNext(result)
            }, onError: {error in
                print(error)
        }).disposed(by: self.disposeBag)
    }
}
