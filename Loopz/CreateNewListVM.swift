//
//  CreateNewListVM.swift
//  DelivX
//
//  Created by 3 Embed on 03/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CreateNewListVM: NSObject {
    
    let disposeBag = DisposeBag()
    static let createNewListVM_response = PublishSubject<Bool>()
    
    /// creats the user enterd wishList name
    ///
    /// - Parameter listName: is of type string
    func updateList(listName:String){
        FavouriteAPICalls.createWishList(listName: listName).subscribe(onNext: {result in
            CreateNewListVM.createNewListVM_response.onNext(true)
        }, onError: {error in
        }).disposed(by: disposeBag)
    }
}
