//
//  ReviewHeaderCell.swift
//  DelivX
//
//  Created by 3Embed on 09/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ReviewHeaderCell: UITableViewCell {

    @IBOutlet weak var destinationSectionHeight: NSLayoutConstraint!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var destinationTitle: UILabel!
    @IBOutlet weak var destinationHeight: NSLayoutConstraint!
    @IBOutlet weak var storeAddress: UILabel!
    @IBOutlet weak var storeAddressTitle: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var dotedView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Helper.DotedBorder(sender: dotedView)
        
        Fonts.setPrimaryBold(totalValue)
        Fonts.setPrimaryRegular(dateLabel)
        Fonts.setPrimaryMedium(storeAddressTitle)
        Fonts.setPrimaryRegular(storeAddress)
        Fonts.setPrimaryMedium(destinationTitle)
        Fonts.setPrimaryRegular(destinationAddress)
        
        destinationAddress.textColor = Colors.SeconderyText
        storeAddress.textColor = Colors.SeconderyText
        destinationTitle.textColor = Colors.PrimaryText
        destinationTitle.textColor = Colors.PrimaryText
        totalValue.textColor = Colors.PrimaryText
        dateLabel.textColor = Colors.PrimaryText
        self.contentView.backgroundColor = Colors.ScreenBackground
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data:Order) {
        storeAddressTitle.text = data.Storename.uppercased()
        storeAddress.text = data.PickAddress
       
        if data.Delivery {
            destinationAddress.text = data.DropAddress
            destinationTitle.text = StringConstants.DeliveryLocation()
            destinationHeight.constant = 15
            destinationSectionHeight.constant = 70
            dotedView.isHidden = false
        }
        else {
            destinationAddress.text = ""
            destinationTitle.text = ""
            destinationHeight.constant = 0
            destinationSectionHeight.constant = 0
            dotedView.isHidden = true
            
        }
        let dateString = Helper.getDateString(value: data.BookingDate, format: DateFormat.dateTime, zone: true)
        let amount = Helper.df2so(Double( data.TotalAmount))
        totalValue.text = amount
        dateLabel.text = dateString
    }

}
