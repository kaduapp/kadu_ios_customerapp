//
//  AppDelegate.swift
//  UFly
//
//  Created by 3Embed on 26/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import Stripe
import GoogleMaps
import GooglePlaces
import CoreLocation
import UserNotifications
import AWSS3
import Firebase
import Messages
import RxSwift
import RxCocoa
import Fabric
import Crashlytics
import FBSDKCoreKit
import FirebaseMessaging
import FirebaseDynamicLinks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    static let app_response = PublishSubject<Int>()
    static var tabChange:AppConstants.Tabs? = nil//AppConstants.Tabs.Home
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      //  FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        Stripe.setDefaultPublishableKey(AppConstants.StripeKey)
        
        self.setupNavigation()
        self.setupNotification(application: application)
        self.checkCouchDB()
       // Fabric.with([STPAPIClient.self, Crashlytics.self])
         Messaging.messaging().delegate = self
        self.uploadImage()
        UIApplication.shared.isIdleTimerDisabled = false
        self.setAppConfig()
        self.initMap()
        Helper.addnewcheckAppReachability()
        OneSkyOTAPlugin.provideAPIKey(OneSky.PublicKey, apiSecret: nil, projectID: OneSky.AppId)
        OneSkyOTAPlugin.setLanguage(Utility.getLanguage().Code)
        OneSkyOTAPlugin.checkForUpdate()
       //

        if  Utility.getLanguage().Code == "ar"{
           RTL.shared.initWithData(value: true)
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            UITableView.appearance().semanticContentAttribute = .forceRightToLeft
            UICollectionView.appearance().semanticContentAttribute = .forceRightToLeft

        }
        else{
        RTL.shared.initWithData(value: false)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
        UITableView.appearance().semanticContentAttribute = .forceLeftToRight
        UICollectionView.appearance().semanticContentAttribute = .forceLeftToRight
        }

       UNUserNotificationCenter.current().delegate = self
        let remoteNotif = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: Any]
        if remoteNotif != nil {
            
            if let action = remoteNotif!["action"] as? String {
                
                //   Helper.showAlert(message: "action12", head: "action12", type: 1)
                
                if action == "11" {
                    
                    // Helper.showAlert(message: "action", head: "action", type: 1)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1 ) {
                        self.callMyOrdersOnInitialLaunch()
                        
                        
                    }
                    
                }
                
            }
        }
         
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        Utility.disconnectMQTT()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if !AppConstants.Reachable{
            AppConstants.Reachable = true
            Helper.addnewcheckAppReachability()
            Helper.hideNoInternet()
        }
        
        Utility.setLanguage(data: Utility.getLanguage())
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        center.removeAllDeliveredNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 100
        UIApplication.shared.applicationIconBadgeNumber = 0
        checkCouchDB()
        Utility.connectMQTT()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - Upload Image
    func uploadImage() {
        let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
        amazonWrapper.setConfigurationWithRegion(AmazonKeys.Zone, accessKey: AmazonKeys.AmazonAccessKey, secretKey: AmazonKeys.AmazonSecretKey)
    }
    
    
    //MARK: - Google MAP
    func initMap() {
        GMSServices.provideAPIKey(GoogleKeys.GoogleMapKey)
        GMSPlacesClient.provideAPIKey(GoogleKeys.GoogleMapKey)
        //let manager = LocationManager.shared
        //manager.start()
    }
    
    //MARK: - Setup Navigation Bar
    func setupNavigation() {
        UINavigationBar.appearance().backgroundColor = Colors.SecondBaseColor
        UINavigationBar.appearance().tintColor = Colors.PrimaryText
        UINavigationBar.appearance().barTintColor = Colors.SecondBaseColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.PrimaryText]
    }
    
    //MARK: - Create Database
    func checkCouchDB() {
        //Cart Database
        var cartDocID = UserDefaults.standard.string(forKey: DataBase.CartCouchDB)
        if (cartDocID == nil) {
            let customer = DBStructure()
            var dict = [String: AnyObject]()
            dict[customer.nameKey] = customer.customerName as AnyObject
            dict[customer.valueKey] = customer.value as AnyObject
            let doc: CBLDocument? = CouchDBEvents.createDocument(database: CouchDBObject.sharedInstance.database, withArray: (dict))
            let docID: String = doc!.documentID
            UserDefaults.standard.set(docID, forKey: DataBase.CartCouchDB)
            UserDefaults.standard.synchronize()
            cartDocID = docID;
            CartVM().getCart()
            
        }
        else{
            CartVM().getCart()
        }
        
        //Create address DB
        var addressDocID = UserDefaults.standard.string(forKey: DataBase.AddressCouchDB)
        if (addressDocID == nil) {
            let customer = DBStructure()
            var dict = [String: AnyObject]()
            dict[customer.nameKey] = customer.customerName as AnyObject
            dict[customer.valueKey] = customer.value as AnyObject
            let doc: CBLDocument? = CouchDBEvents.createDocument(database: CouchDBObject.sharedInstance.database, withArray: (dict))
            let docID: String = doc!.documentID
            UserDefaults.standard.set(docID, forKey: DataBase.AddressCouchDB)
            UserDefaults.standard.synchronize()
            addressDocID = docID;
        }
        
        
        //Create saved address DB
        var savedAddressDocID = UserDefaults.standard.string(forKey: DataBase.SavedAddressCouchDB)
        if (savedAddressDocID == nil) {
            let customer = DBStructure()
            var dict = [String: AnyObject]()
            dict[customer.nameKey] = customer.customerName as AnyObject
            dict[customer.valueKey] = customer.value as AnyObject
            let doc: CBLDocument? = CouchDBEvents.createDocument(database: CouchDBObject.sharedInstance.database, withArray: (dict))
            let docID: String = doc!.documentID
            UserDefaults.standard.set(docID, forKey: DataBase.SavedAddressCouchDB)
            UserDefaults.standard.synchronize()
            savedAddressDocID = docID;
        }
        
        //Create saved address DB
        var superStoreDocID = UserDefaults.standard.string(forKey: DataBase.SuperStoreCouchDB)
        if (superStoreDocID == nil) {
            let customer = DBStructure()
            var dict = [String: AnyObject]()
            dict[customer.nameKey] = customer.customerName as AnyObject
            dict[customer.valueKey] = customer.value as AnyObject
            let doc: CBLDocument? = CouchDBEvents.createDocument(database: CouchDBObject.sharedInstance.database, withArray: (dict))
            let docID: String = doc!.documentID
            UserDefaults.standard.set(docID, forKey: DataBase.SuperStoreCouchDB)
            UserDefaults.standard.synchronize()
            superStoreDocID = docID;
        }
    }
    
    
    
    //MARK: - Setup Notification
    func setupNotification(application: UIApplication) {
        //FCM Push
        FirebaseApp.configure()
    // iOS 10 support
        if #available(iOS 10,iOSApplicationExtension 10.0, *) {
            
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        
        application.registerForRemoteNotifications()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(tokenRefreshNotification(notification:)),
                                               name: NSNotification.Name.InstanceIDTokenRefresh,
                                               object: nil)
    }
    
    // [START connect_to_fcm]
    func connectToFcm() {
        print("Connecting to FCM.")
        Messaging.messaging().delegate = self
        Utility.subscribeFCMTopic()
        Utility.subscribeFCMTopicAll()
    }
    
    
    // [START refresh_token]
    @objc func tokenRefreshNotification(notification: Notification) {
        print("Connecting to FCM Token.")
        if let refreshedToken = Messaging.messaging().fcmToken {
            print("InstanceID token: \(refreshedToken)")
            Utility.savePushToken(token: refreshedToken)
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
        
        var token = NSString(format: "%@", deviceToken as CVarArg)
        token = token.replacingOccurrences(of: "<", with: "") as NSString
        token = token.replacingOccurrences(of: ">", with: "") as NSString
        token = token.replacingOccurrences(of: " ", with: "") as NSString
        
        print("\n\nPush Token: \(token)\n\n")
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        
        if let action = data["action"] as? String{
            if action == "11" {
               // self.callMyordersVC(data as! [String : Any])
            }
        }
        
        
        
        
        
        print("Push notification received: \(data)\n\n\n")
        if let action = data["action"] as? String {
            if action == "12" {                          //12 - session out
                Utility.logout(type: 1,message: "")
            }
        }else if let action = data["action"] as? Int {
            if action == 12 {
                Utility.logout(type: 1,message: "")
            }
        }
        
/*
 
        if let action = data["action"] as? String {
            if action == "11" { // Order Status
                if let dict = data as? [String:Any]{
                    if let dictData = dict["data"] as? String{
                        if let data = dictData.data(using: .utf8) {
                            do {
                                let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                                if let statusData = jsonDictionary, let status = statusData["status"] as? Int,status == 2{
                                    var message = OSLocalizedString("Order Cancelled", comment:"Order Cancelled" )
                                    var title = OSLocalizedString("Cancelled", comment: "Cancelled")
                                    if let statusMessage = dict["msg"] as? String,let titleMessage = dict["title"] as? String{
                                        message = statusMessage
                                        message = message.replacingOccurrences(of: ", Enter your reason.", with:"" )
                                        title = titleMessage
                                    }
                                    CommonAlertView.shared.showAlert(message:message , head: title , type: 1)
                                }
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    }
                }
                
            }
        }  */
        if let action = data["action"] as? String {
            if action == "15" {                          //12 - session out
                
            }
        }else if let action = data["action"] as? Int {
            if action == 15 {
                
            }
        }
    }
    
    
    func callMyordersVC(_ orderid: [String : Any]) {
            self.window!.rootViewController?.dismiss(animated: true,  completion:{ () in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4 ) {
                let viewController = UIStoryboard(name: UIConstants.HistoryStoryboard, bundle: nil).instantiateViewController(withIdentifier: UIConstants.ControllerIds.OrderDetailVC) as! OrderDetailVC
    //            viewController.isFromProfile = true
                    let order = Order.init(data: orderid)
                    viewController.orderDetailVM.selectedOrder = order
                let navController = UINavigationController(rootViewController: viewController)
                navController.navigationBar.backIndicatorImage =  #imageLiteral(resourceName: "CloseIcon")// imageLiteral(resourceName: "CloseIcon")
                navController.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "CloseIcon") //imageLiteral(resourceName: "CloseIcon")
                navController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                let rootViewController = self.window!.rootViewController as! UINavigationController
                //   self.window.present(navController, animated:true, completion: nil)
                    rootViewController.visibleViewController!.present(navController, animated: false, completion: nil) }}
                )
            //self.window!.rootViewController?.dismiss(animated: true,  completion: { (completion) in
    //print("test")        })
            
        }

    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)\n\n\n")
    }
    
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
                if let error = error {
                    print("Request Authorization Failed (\(error), \(error.localizedDescription))")
                }
                completionHandler(success)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Push message received: \(remoteMessage.appData)\n\n\n")
        if let action = remoteMessage.appData["action"] as? String {
            if action == "11" {
                if let temp = remoteMessage.appData["data"] as? [String : Any] {
                    let data = MQTTMessageModel(data:temp)
                    data.Action = Int(action)!
                    MQTTDelegate.message_response.onNext(data)
                }
            }
        }else if let action = remoteMessage.appData                                                                                                                                                                                                                                                                                                                                                                                                 ["action"] as? Int {
            if action == 11 {
                let data = MQTTMessageModel(data:remoteMessage.appData["data"] as! [String : Any])
                data.Action = action
                MQTTDelegate.message_response.onNext(data)
            }
        }
        
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        switch shortcutItem.type {
        case Utility.BundleId + ".Search":
            AppDelegate.tabChange = .Search
            break
        case Utility.BundleId + ".Cart":
            AppDelegate.tabChange = .Cart
            break
        case Utility.BundleId + ".History":
            AppDelegate.tabChange = .History
            break
        default:
            break
        }
        if AppDelegate.tabChange != nil && Utility.getAddress().FullText.length > 0 && Utility.getGuestLogin() == true {
            let temp = AppDelegate.tabChange
            AppDelegate.tabChange = nil
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                Helper.changetTabTo(index: temp!)
            }
        }
    }
    
    
    //MARK: - Deep Linking
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        if let referaalString = userActivity.webpageURL?.absoluteString , referaalString.range(of:"referCode:") != nil , let referCode = referaalString.slice(from: "referCode:", to: "/11")
        {
            

              let   alertPopup = UIAlertController(title:"Welcome To Rinn", message: "Use " + "\(referCode)" + " code to use it while sign up.", preferredStyle: UIAlertController.Style.alert)
                                   alertPopup.addAction(UIAlertAction.init(title: StringConstants.Cancel() , style:  UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
                                       let vc = Helper.finalController()
                                      vc.dismiss(animated: true, completion: nil)
                                  }))
                alertPopup.addAction(UIAlertAction.init(title: StringConstants.Copy() , style:  UIAlertAction.Style.default, handler: { (UIAlertAction) in
                    UIPasteboard.general.string = referCode
                }))
         
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4 ) {
                let vc = Helper.finalController()
                vc.present(alertPopup, animated: true, completion: nil)
            }

        }
        let dynamicLinks = DynamicLinks.dynamicLinks()
        let handled = dynamicLinks.handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            // ...
//            let urlString = dynamiclink?.url?.absoluteString
//            let array:[String] = (urlString?.components(separatedBy: "/"))!
//
            guard let urlString = dynamiclink?.url?.absoluteString else {
                return
            }
            let array:[String] = (urlString.components(separatedBy: "/"))
            if array.count > 0 {
                for item in array {
                    if item.range(of:"itemId:") != nil {
                        let itemIn = Item.init(data:[:],store:Store.init(data:[:]))
                        itemIn.Id = item.replacingOccurrences(of: "itemId:", with: "")
                        print("\n\n\n\n\n\n\n\n\(itemIn.Id)")
                        if itemIn.Id.sorted().count > 0 {
                            if let wd = UIApplication.shared.delegate?.window {
                                var vc = wd!.rootViewController
                                if(vc is UINavigationController){
                                    vc = (vc as! UINavigationController).viewControllers.last
                                }
                                if vc?.presentedViewController != nil {
                                    vc = vc?.presentedViewController
                                }
                                let viewController = UIStoryboard(name: UIConstants.MainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: UIConstants.ControllerIds.ItemDetailVC) as! ItemDetailVC
                                viewController.itemDetailVM.item = itemIn
                                
                                let navigationVC = UINavigationController.init(rootViewController: viewController)
                                vc?.present(navigationVC, animated:true, completion: nil)
                            }

                        }
                    }
                }
            }
            if array.contains("success"){
                AppDelegate.app_response.onNext(1)
            }

        }
        
        return handled
    }
    
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        //return application(app, open: url,
        let stripeHandled = Stripe.handleURLCallback(with: url)
        if (stripeHandled) {
            return true
        } else {
            return application(app, open: url,
                           sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                           annotation: "")
        }
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
                print(dynamicLink)
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            // ...
            return true
        }
//        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//        // Add any custom logic here.
//        return handled
        return false
    }
    
    
    //App Config
    func setAppConfig() {
        APICalls.configure().subscribe(onNext: { [weak self]success in
//            Messaging.messaging().subscribe(toTopic: Utility.getAppConfig().AllCitiesPush)
//            Messaging.messaging().subscribe(toTopic: Utility.getAppConfig().AllPush)
            STPPaymentConfiguration.shared().publishableKey = AppConstants.StripeKey
            
            self?.initMap()
        }).disposed(by: APICalls.disposesBag)
    }
    
    
    
    func setCountryCode() {
        let picker: VNHCountryPicker = VNHCountryPicker()
        picker.getCountry()
    }
    
    
    // This method handles opening universal link URLs (e.g., "https://example.com/stripe_ios_callback")
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                let stripeHandled = Stripe.handleURLCallback(with: url)
                if (stripeHandled) {
                    return true
                } else {
                    // This was not a stripe url – do whatever url handling your app
                    // normally does, if any.
                }
            }
        }
        return false
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
           print(notification.request.content.userInfo)
           completionHandler([.alert, .badge, .sound])
       }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let application = UIApplication.shared
        if(application.applicationState == .active){
            if let userInfo = response.notification.request.content.userInfo as? [String : AnyObject] {
                if let action = userInfo["action"] as? String   {
                    if action == "11" {
                        do {
                            
                            self.callMyOrdersOnInitialLaunch()
                            
                        }
                        catch {
                            print(error)
                        }
                    }
                }
                
            }
        }
        
        if(application.applicationState == .inactive){
            if let userInfo = response.notification.request.content.userInfo as? [String : AnyObject] {
                
                
                if let action = userInfo["action"] as? String  {
                    if action == "11" {
                        
                        do {
                            
                            self.callMyOrdersOnInitialLaunch()
                            
                        }
                        catch {
                            print(error)
                        }
                        
                    }
                }
            }
            
        }
        
        if(application.applicationState == .background){
            if let userInfo = response.notification.request.content.userInfo as? [String : AnyObject] {
                
                if let action = userInfo["action"] as? String  {
                    if action == "11" {
                        do {
                     
                            self.callMyOrdersOnInitialLaunch()
                            
                        }
                        catch {
                            print(error)
                        }
                        
                    }
                }
            }
            
        }
        completionHandler()
        
    }
    func callMyOrdersOnInitialLaunch()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4 ) {
            let viewController = UIStoryboard(name: UIConstants.HistoryStoryboard, bundle: nil).instantiateViewController(withIdentifier: UIConstants.ControllerIds.HistoryVC) as! HistoryVC
          //  viewController.isFromProfile = true
            let navController = UINavigationController(rootViewController: viewController)
            navController.navigationBar.backIndicatorImage =  #imageLiteral(resourceName: "CloseIcon")
            navController.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "CloseIcon")
            navController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            let rootViewController = self.window!.rootViewController as! UINavigationController
            rootViewController.visibleViewController!.present(navController, animated: false, completion: nil) }
    }
    
}

