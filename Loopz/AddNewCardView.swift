//
//  AddNewCardView.swift
//  UFly
//
//  Created by 3 Embed on 17/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import Stripe

class AddNewCardView: UIView ,STPPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardNumSeparator: UIView!
    @IBOutlet weak var expireTFSeparator: UIView!
    @IBOutlet weak var cvvSeparator: UIView!
    @IBOutlet weak var addCardView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var cardNumberTF: HoshiTextField!
    @IBOutlet weak var expiresTF: HoshiTextField!
    @IBOutlet weak var cvvTF: HoshiTextField!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var orSeparator1: UIView!
    @IBOutlet weak var orSeparator2: UIView!
    
    @IBOutlet weak var scanView: UIView!
    @IBOutlet weak var scanButton: UIButton!
    
    
    
    var paymentTextField: STPPaymentCardTextField?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardNumberTF.Semantic()
        expiresTF.Semantic()
        cvvTF.Semantic()
        initialSetup()
    }

    /// initial viewsetup
    func initialSetup() {
        setUp()
        cardImage.image = STPImageLibrary.brandImage(for: STPCardBrand.unknown)
        Helper.setButton(button: addBtn,view:addCardView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.AddNewCard(), highlighted: StringConstants.AddNewCard(), selected: StringConstants.AddNewCard(), button: addBtn)
        Helper.setButtonTitle(normal: StringConstants.ScanCard(), highlighted: StringConstants.ScanCard(), selected: StringConstants.ScanCard(), button: scanButton)
      paymentTextField?.becomeFirstResponder()
        Fonts.setPrimaryMedium(addBtn)
        Fonts.setPrimaryRegular(cardNumberTF)
        Fonts.setPrimaryRegular(expiresTF)
        Fonts.setPrimaryRegular(cvvTF)
        
        cardNumberTF.textColor =  Colors.PrimaryText
        expiresTF.textColor =  Colors.PrimaryText
        cvvTF.textColor =  Colors.PrimaryText
        
        
        
        cardNumberTF.placeholder = StringConstants.CardNumber()
        cvvTF.placeholder = StringConstants.CVV()
        expiresTF.placeholder = StringConstants.Expires()
       orLabel.text = StringConstants.OR()
        orLabel.textColor =  Colors.SeparatorLight
        orSeparator1.backgroundColor =  Colors.SeparatorLight
        orSeparator2.backgroundColor =  Colors.SeparatorLight
        Helper.setButton(button: scanButton, view: scanView, primaryColour:  Colors.SecondBaseColor, seconderyColor:  Colors.AppBaseColor, shadow: false)
    }
    
    func setUp() {
        
        paymentTextField               = STPPaymentCardTextField()
        paymentTextField?.delegate     = self
        paymentTextField?.cursorColor  = UIColor.blue
        paymentTextField?.borderColor  = UIColor.clear
        paymentTextField?.borderWidth  = 1
        paymentTextField?.font         = UIFont.boldSystemFont(ofSize: 12)
        paymentTextField?.textColor    = UIColor.black;
        paymentTextField?.cornerRadius = 2.0;
        paymentTextField?.frame        = CGRect(x: 0,y: 0,width: cardNumberTF.frame.size.width,height: 40)
    }
    
    
}
