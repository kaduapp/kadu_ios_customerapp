//
//  SavedAddresTableCell.swift
//  UFly
//
//  Created by Rahul Sharma on 22/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SavedAddresTableCell: UITableViewCell {

    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var locationIcon: UIButton!
    @IBOutlet weak var deleteIcon: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var mainCardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initialSetup(){
        Fonts.setPrimaryRegular(addressLabel)
        Fonts.setPrimaryMedium(title)
        locationIcon.tintColor =  Colors.PrimaryText
        addressLabel.textColor =  Colors.SeconderyText
        title.textColor   =  Colors.PrimaryText
        
        Fonts.setPrimaryMedium(deleteIcon)
        Fonts.setPrimaryMedium(editButton)
        deleteIcon.setTitleColor( Colors.Red, for: .normal)
        editButton.setTitleColor( Colors.BlueColor, for: .normal)
        editButton.setTitle(StringConstants.Edit().uppercased(), for: .normal)
        deleteIcon.setTitle(StringConstants.DeleteCaps().uppercased(), for: .normal)
        
        separator.backgroundColor =  Colors.SeparatorLight
    }
    
    

    func setData(data:Address) {
        
        title.text = OSLocalizedString(data.Tag, comment: data.Tag)
        addressLabel.text = AddressUtility.getSortedAdress(data)
        if data.Tag.lowercased() == StringConstants.Home().lowercased() {
            locationIcon.setImage(#imageLiteral(resourceName: "HomeAddress"), for: .normal)
        }else if data.Tag.lowercased() == StringConstants.Work().lowercased(){
            locationIcon.setImage(#imageLiteral(resourceName: "WorkAddress"), for: .normal)
        }else{
            locationIcon.setImage(#imageLiteral(resourceName: "OtherAddress"), for: .normal)
        }
    }
}
