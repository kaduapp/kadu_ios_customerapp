//
//  SearchTVE.swift
//  UFly
//
//  Created by 3 Embed on 28/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

// MARK: - UIScrollViewDelegate methods

extension SearchVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            //            updatePage(scrollView1: scrollView)
        }
    }
    
}

// MARK: - UITableViewDataSource UITableViewDelegate Methods
extension SearchVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchVM.loadPopular {
            if searchVM.arrayOfItems.count > 0 {
                tableView.backgroundView = nil
                return 1
            }else if searchVM.recentSearchList.count > 0 {
                tableView.backgroundView = nil
                return 1
            }
            else{
                tableView.backgroundView = noProductsView
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchVM.arrayOfItems.count > 0 {
            return searchVM.arrayOfItems.count
        }
        if searchVM.recentSearchList.count > 0 {
            return searchVM.recentSearchList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if searchVM.arrayOfItems.count > 0 {
            return Helper.showHeader(title: StringConstants.Suggestions(), showViewAll: false)
        }else if searchVM.recentSearchList.count > 0 {
            return Helper.showHeader(title: StringConstants.RecentSearches(), showViewAll: false)
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return Helper.showHeader(title: "", showViewAll: false)
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ProductsCell) as! ProductsCell
        if searchVM.arrayOfItems.count > 0 {
            cell.setName(item: searchVM.arrayOfItems[indexPath.row])
            Helper.updateText(text: searchVM.arrayOfItems[indexPath.row], subText:searchTF.text! , cell.productNameLabel, color: Colors.SearchHighlight,link: "")
        }else{
            cell.setName(item: searchVM.recentSearchList[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        searchTF.resignFirstResponder()
        lang = Utility.getLanguage().Code

        if searchVM.arrayOfItems.count > 0 {
            self.searchTF.text = searchVM.arrayOfItems[indexPath.row]
            self.performSegue(withIdentifier: String(describing: SearchDetailsVC.self), sender: searchVM.arrayOfItems[indexPath.row])
            productName = searchVM.arrayOfItems[indexPath.row]
        }else{
            self.searchTF.text = searchVM.recentSearchList[indexPath.row]
            self.performSegue(withIdentifier: String(describing: SearchDetailsVC.self), sender: searchVM.recentSearchList[indexPath.row])
            productName = searchVM.recentSearchList[indexPath.row]
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
}
