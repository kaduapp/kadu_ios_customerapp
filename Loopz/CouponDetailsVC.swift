//
//  CouponDetailsVC.swift
//  DelivX
//
//  Created by 3Embed on 28/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CouponDetailsVC: UIViewController {

    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var bottomButtonView: UIView!
    @IBOutlet weak var howItWorkBody: UILabel!
    @IBOutlet weak var howitworkTitle: UILabel!
    @IBOutlet weak var validTillDate: UILabel!
    @IBOutlet weak var validTillTitle: UILabel!
    @IBOutlet weak var couponTitle: UILabel!
    @IBOutlet weak var couponCodeButton: UIButton!
    @IBOutlet weak var couponBGView: UIView!
    var coupon:Coupon = Coupon.init(data: [:])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func gotItAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setUI() {
        Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour: Colors.SecondBaseColor, seconderyColor: Colors.AppBaseColor, shadow: true)
        Helper.setButtonTitle(normal: StringConstants.okGotIt(), highlighted: StringConstants.okGotIt(), selected: StringConstants.okGotIt(), button: bottomButton)
        
        couponBGView.backgroundColor = Colors.AppBaseColor.withAlphaComponent(0.2)
        Helper.setUiElementBorderWithCorner(element: couponBGView, radius: 2, borderWidth: 2, color: Colors.AppBaseColor)
        couponCodeButton.setTitle(coupon.Code, for: .normal)
        couponCodeButton.setTitleColor(Colors.PrimaryText, for: .normal)
        couponTitle.text = coupon.Title
        couponTitle.textColor = Colors.PrimaryText
        
        validTillTitle.textColor = Colors.AppBaseColor
        validTillTitle.text = StringConstants.ValidThru().uppercased()
        Fonts.setPrimaryMedium(validTillTitle)
        
        validTillDate.textColor = Colors.SecoundPrimaryText
        validTillDate.text = Helper.getDateString(value: coupon.EndTime, format: DateFormat.dateTime, zone: false) + " " + Helper.getDateString(value: coupon.EndTime, format: DateFormat.TimeFormatToDisplay, zone: false)
        Fonts.setPrimaryRegular(validTillDate)
        
        howitworkTitle.textColor = Colors.AppBaseColor
        howitworkTitle.text = StringConstants.howItWorks().uppercased()
        Fonts.setPrimaryMedium(howitworkTitle)
        
        howItWorkBody.textColor = Colors.SecoundPrimaryText
        howItWorkBody.text = StringConstants.howToWork()
        Fonts.setPrimaryRegular(howItWorkBody)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
