//
//  APIResponceParams.swift
//  UFly
//
//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class APIResponceParams: NSObject {
    
    //Config
    static let AllCitiesPush                            = "aallCitiesCustomerspushTopics"
    static let AllPush                                  = "allCustomerspushTopics"
    static let GoogleMapKeys                            = "custGoogleMapKeys"
    static let GooglePlaceKeys                          = "custGooglePlaceKeys"
    static let StripeKey                                = "stripeKey"
    static let LaterBookingBuffer                       = "latebookinginterval"
    static let googleMapKey                             = "googleMapKey"

    
    
    

    //Basic
    static let Message                                  = "message"
    static let Data                                     = "data"
    
    //Login API
    static let CountryCode                              = "countryCode"
    static let Email                                    = "email"
    static let FirstName                                = "name"
    static let Phone                                    = "mobile"
    static let Sid                                      = "sid"
    static let SessionToken                             = "token"
    static let FCMTopic                                 = "fcmTopic"
    static let MQTTTopic                                = "mqttTopic"
    static let googleMapKeyMqtt                         = "googleMapKeyMqtt"
    static let ReferralCode                             = "referralCode"
    static let TicketID                                 = "requester_id"
    static let ProfilePic                               = "profilePic"
    static let Profile                                  = "profile"
    static let MMJCard                                  = "mmjCard"
    static let IDCard                                   = "identityCard"
    static let Verified                                 = "verified"
    static let URL                                      = "url"
  
    
    //Address
    static let IdAddress                                = "_id"
    static let Associated                               = "associated"
    static let AddLine1                                 = "addLine1"
    static let AddLine2                                 = "addLine2"
    static let City                                     = "city"
    static let State                                    = "state"
    static let Country                                  = "country"
    static let PlaceId                                  = "placeId"
    static let Pincode                                  = "pincode"
    static let Tag                                      = "taggedAs"
    static let UserType                                 = "userType"
    static let UserId                                   = "userId"
    static let Latitude                                 = "latitude"
    static let Longitude                                = "longitude"
    static let Id                                       = "zoneId"
    static let Title                                    = "title"
    static let FlatNumber                               = "flatNumber"
    static let LandMark                                 = "landmark"
    static let CurrencySymbol                           = "currencySymbol"
    static let Currency                                 = "currency"
    static let MilageMetric                             = "mileageMetric"
    //Store
    static let Stores                                   = "stores"
    static let Store                                    = "store"
    static let StoreAddress                             = "businessAddress";
    static let StoreAddress2                            = "storeAddr";
    static let NextOpenTime                             = "nextOpenTime";
    static let StoreName                                = "businessName";
    static let StoreIsOpen                              = "storeIsOpen";
    static let Today                                    = "today";
    static let StoreId                                  = "businessId";
    static let StoreLat                                 = "businessLatitude";
    static let StoreLong                                = "businessLongitude";
    
    static let DriverLat                                = "driverLatitude";
    static let DriverLong                               = "driverLongitude";
    
    static let StoreImage                               = "images";
    static let DistanceKm                               = "distanceKm";
    static let DistanceMiles                            = "distanceMiles"
    static let EstimatedTime                            = "estimatedTime"
    static let Categories                               = "categories";
    static let StoreRating                              = "businessRating"
    static let FreeDeliveryAbove                        = "freeDeliveryAbove"
    static let Orders                                   = "ordersArray"
    static let MinimumOrder                             = "minimumOrder"
    static let BusinessImage                            = "businessImage"
    static let StoreSubCats                             = "storeSubCats"
    static let StoreSubCatsId                           = "id"
    static let ResturantData                            = "resturantData"
    static let offerData                                = "offerData"
    static let favStore                                 = "favStore"

   
    //Category
    static let CategoryId                               = "categoryId"
    static let CategoryName                             = "categoryName"
    static let CategoryDesc                             = "description"
    static let CategoryImage                            = "imageUrl"
    static let CategoryBannerImage                      = "bannerUrl"
    static let SubCategories                            = "subCategories"
    static let CategoryColor                            = "categoryColor"
    //SubCategories
    static let SubCategoryDesc                          = "description"
    static let SubCategoryId                            = "subCategoryId"
    static let SubCategoryName                          = "subCategoryName"
    static let SubCategoryName2                         = "storeSubCategory"
    
    //Item
    static let ItemImages                               = "mobileImage"
    static let ItemId                                   = "childProductId"
    static let ItemName                                 = "productName"
    static let ItemName2                                 = "itemName"
    static let ItemPrice                                = "priceValue"
    static let ItemTCH                                  = "THC"
    static let ItemCBD                                  = "CBD"
    static let Fav                                      = "isFavorite"
    static let outOfStock                               = "outOfStock"
    
    static let FavCount                                 = "productFavCount"
    static let ItemDescription                          = "shortDescription"
    static let Weight                                   = "weight"
    static let ItemPCU                                  = "upc"
    static let ItemSKU                                  = "sku"
    static let DetailedDescription                      = "detailedDescription"
    static let Effects                                  = "additionalEffects"
    static let Units                                    = "units"
    static let UnitId                                   = "unitId"
    static let UnitPrice                                = "unitPrice"
    static let UnitName                                 = "unitName"
    static let Value                                    = "value"
    static let AppliedDiscount                          = "appliedDiscount"
    static let FinalPrice                               = "finalPrice"
    static let FinalPriceList                           = "finalPriceList"
    static let OfferId                                  = "offerId"
    static let Offers                                   = "offers"
    static let OfferTitle                               = "offerTitle"
    static let OfferBanner                              = "offerBanner"
    static let offerStoreId                             = "storeId"
    static let MobileImage                              = "mobileImage"
    static let ImageId                                  = "imageId"
    static let ImageText                                = "imageText"
    static let Keyword                                  = "keyword"
    static let Mobile                                   = "mobile"
    static let Thumbnail                                = "thumbnail"
    static let availableQuantity                        = "availableQuantity"
    
    //favorite
    static let ProductFav                               = "productFav"
    
    //Card
    static let Brand                                    = "brand"
    static let DefaultCard                              = "isDefault"
    static let DefaultCardWallet                        = "default"
    static let ExpMonth                                 = "exp_month"
    static let ExpYear                                  = "exp_year"
    static let ExpMonth1                                 = "expMonth"
    static let ExpYear1                                  = "expYear"
    static let Funding                                  = "funding"
    static let CardId                                   = "id"
    static let Last4                                    = "last4"
    static let Name                                     = "name"
    static let CardToken                                = "token"
    
    
    //Wallet
    
    static let WalletHardLimit                          = "walletHardLimit"
    static let WalletSoftLimit                          = "walletSoftLimit"

    
    
    //Cart
    static let CartStoreID                              = "storeId"
    static let convenienceFee                           = "convenienceFee"
    static let convenienceFeeType                       = "convenienceFeeType"
    static let CartStoreName                            = "storeName"
    static let CartStoreLat                             = "storeLatitude"
    static let CartStoreLong                            = "storeLongitude"
    static let CartStoreAddress                         = "storeAddress"
    static let CartStoreLogo                            = "storeLogo"
    static let CartStoreTotal                           = "storeTotalPrice"
    static let CartStoreDelFee                          = "storeDeliveryFee"
    static let Products                                 = "products"
    static let LowestPrice                              = "lowestPrice"
    static let ProductsFav                              = "favProducts"
    static let favRestaurantProduct                     = "favProduct"
    static let CartId                                   = "cartId"
    static let Cart                                     = "cart"
    static let TotalPrice                               = "totalPrice"
    static let TotalPriceWithTax                        = "finalTotalIncludingTaxes"
    static let Recommended                              = "recomendedProducts"
    static let cartDiscount                             = "cartDiscount"

    
    
    //History
    static let Bid                                      = "orderId"
    static let Bid2                                     = "bid"
    static let PickAddress                              = "pickAddress"
    static let DropAddress                              = "dropAddress"
    static let DriverName                               = "driverName"
    static let DriverID                                 = "driverId"
    static let DriverImage                              = "driverImage"
    static let DriverEmail                              = "driverEmail"
    static let DriverMobile                             = "driverMobile"
    static let Status                                   = "statusCode"
    static let StatusMessage                            = "statusMessage"
    static let MessageStatus                            = "statusMessage"
    static let ReceiverName                             = "receiverName"
    static let TotalAmount                              = "totalAmount"
    static let items                                    = "items"
    static let itemName                                 = "itemName"
    static let storeName                                = "storeName"
    static let PickLat                                  = "pickupLat"
    static let PickLong                                 = "pickupLong"
    static let DropLat                                  = "dropLat"
    static let DropLong                                 = "dropLong"
    static let ServiceType                              = "serviceType"
    
    
    //Order
    static let BookingDate                              = "bookingDate"
    static let PaymentType                              = "paymentType"
    static let PaidBy                                   = "paidBy"
    static let Wallet                                   = "wallet"
    static let Card                                     = "card"
    static let CardChargeId                             = "cardChargeId"
    static let Cash                                     = "cash"
    static let Reviewed                                 = "reviewed"
    static let OrderId                                  = "orderId"
    static let payByWallet                              = "payByWallet"
    static let driverTip                                = "driverTip"
    static let couponCode                               = "couponCode"


   //selected popup
    static let unAssign                                 = "unAssign"
    static let ListName                                 = "listName"
    static let ListId                                   = "listId"
    static let ListImage                                = "listImage"
    
    //WishList
    static let WishList                                 = "wishList"
    static let Image                                    = "image"
    
    
    //Support
    static let Desc                                     = "desc"
    static let Link                                     = "link"
    static let SubCat                                   = "subcat"
    
    
    //promocode
    static let AmountCart                               = "discountAmount"
    static let FinalAmount                              = "finalAmount"
    
    
    static let Code                                     = "code"
    static let Description                              = "description"
    static let Discount                                 = "discount"
    static let TypeId                                   = "typeId"
    static let TypeName                                 = "typeName"
    static let EndTime                                  = "endTime"
    static let HowItWorks                               = "howItWorks"
    static let MinimunCartValue                         = "minimumPurchaseValue"
    static let StartTime                                = "startTime"
    static let TermsAndConditions                       = "termsAndConditionsUrl"
    static let PrivacyAndPolicy                       = "privacyPoliciesUrl"
    
    
    
    // Reasons
    static let Reason                                   = "reasons"
    
    
    //Rating
    static let DriverRating                             = "attributes"
    
    
    
    
    //SubSub Category
    static let SubSubCatName                            = "subSubCategoryName"
    static let SubSubCatId                              = "subSubCategoryId"
    static let SubSubCategories                         = "subSubCategories"
    
    
    //Taxes
    static let Price                                    = "price"
    static let TaxName                                  = "taxCode"
    static let TaxId                                    = "taxId"
    static let TaxValue                                 = "taxValue"
    static let ExclusiveTaxes                           = "exclusiveTaxes"
    static let Taxes                                    = "taxes"
    static let StoreTotalPriceWithExcTaxes              = "storeTotalPriceWithExcTaxes"
    static let DeliveryCharge                           = "deliveryCharge"
    static let SubTotal                                 = "subTotalAmount"
    static let StoreType                                = "storeType"
    static let Ingredients                              = "ingredients"
    
    //Tip
    static let tipValue                                 = "tipValue"
    static let statusMsg                                = "statusMsg"
    static let status                                   = "status"
    //banner
    static let BannerImage                              = "bannerImage"
    static let BrandName                                = "brandName"
    static let Logo                                     = "logoImage"
    static let Brands                                   = "brands"
    
    static let WalletBalance                            = "walletBalance"
    static let CreditTransctions                        = "creditTransctions"
    static let DebitTransctions                         = "debitTransctions"
    
    
    
    static let Amount                                   = "amount"
    static let Comment                                  = "comment"
    static let PaymentTypeText                          = "trigger"
    static let TxnDate                                  = "txnDate"
    static let TxnId                                    = "txnId"
    static let TxnType                                  = "txnType"
    //Super Store
    
    static let SuperStoreBannerImage                    = "bannerImage"
    static let LogoImage                                = "logoImage"
    static let SuperStoreDesc                           = "description"
    static let Visibility                               = "visibility"
    static let SuperStoreType                           = "type"
    static let SuperStoreTypeName                       = "typeName"
    static let SuperStoreFileName                       = "fileName"
    static let SeqId                                    = "seqId"
    static let TypeMsg                                  = "typeMsg"
    static let VisibilityMsg                            = "visibilityMsg"
    static let EditId                                   = "editId"
    static let SuperStoreCategoryName                   = "categoryName"
    static let CatTypeGif                               = "catTypeGif"
    static let ColorCode                                = "colorCode"
    //Restuarants
    
    static let StoreAddr                                = "storeAddr"
    static let StoreBillingAddr                         = "storeBillingAddr"
    static let RestuarantsMinimumOrder                  = "minimumOrder"
    static let RestuarantsFreeDeliveryAbove             = "freeDeliveryAbove"
    static let RestuarantsStoreType                     = "storeType"
     static let StoreCartType                     = "cartsAllowed"
    static let StoreTypeMsg                             = "storeTypeMsg"
    static let Distance                                 = "distance"
    static let RestuarantsStoreName                     = "storeName"
    static let StoreDescription                         = "storeDescription"
    static let RestuarantsBannerImage                   = "bannerImage"
    static let RestuarantsLogoImage                     = "logoImage"
    static let RestuarantsType                          = "type"
    static let RestuarantsTypeMsg                       = "typeMsg"
    static let RestuarantsStoreId                       = "storeId"
    static let AverageRating                            = "averageRating"
    static let CostForTwo                               = "costForTwo"
    static let FoodTypeName                             = "foodTypeName"
    static let subcategoryId                            = "subcategoryId"
    static let subcategoryName                          = "subcategoryName"
    static let FranchiseId                              = "franchiseId"
    static let FranchiseName                            = "franchiseName"
    
    //AddOns
    static let AddOns                                   = "addOns"
    static let AddOnsAvailable                          = "addOnAvailable"
    static let AddOnsId                                 = "id"
    static let storeId                                 = "storeId"
    static let AddOnName                                = "name"
    static let AddOnsMandatory                          = "mandatory"
    static let AddOnsMultiple                          = "multiple"
    static let AddOnslimit                              = "addOnLimit"
    static let minimumLimit                              = "minimumLimit"
    static let maximumLimit                              = "maximumLimit"
    static let AddOnsDescription                        = "description"
    static let AddOnsGrouped                            = "addOnGroup"
    static let AddOnsprice                              = "price"
    static let FirstCategoryId                          = "firstCategoryId"
    static let FirstCategoryName                        = "firstCategoryName"
    static let SecondCategoryId                         = "secondCategoryId"
    static let SecondCategoryName                       = "secondCategoryName"
    static let ThirdCategooryId                         = "thirdCategoryId"
    static let ThirdCategoryName                        = "thirdCategoryName"
    static let SelectedAddons                           = "selectedAddOns"
    static let PackId                                   = "packId"
    static let Increase                                 = "increase"
    static let en                                       = "en"
    static let units                                    = "units"
    static let phoneNumber                              = "phoneNumber"
}
