//
//  SearchStoreTFE.swift
//  DelivX
//
//  Created by 3EMBED on 14/11/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

// MARK: - UITextFieldDelegate methods
extension SearchStoreVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        restuarantTableView.reloadData()
//        dishesTableView.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField.text?.length)! >= 3 {
            self.performSegue(withIdentifier: String(describing: SearchDetailsVC.self), sender: textField.text!)
        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        if textField.text != "" {
            searchVM.loadPopular = false
            lang = Helper.getKeyboardLanguage(language: (textField.textInputMode?.primaryLanguage)!)
            SearchAPICalls.searchStore(needle: searchTF.text!, cousine: "",rating: 0, foodType: "", language: lang).subscribe(onNext: { [weak self]data in
                self?.searchVM.arrayOfStores.removeAll()
                self?.searchVM.arrayOfStores.append(contentsOf: data)
                if data.count == 0{
                    self?.restuarantTableView.backgroundView = self?.restuarantEmptyView
                }else{
                    self?.restuarantTableView.backgroundView = nil
                }
                self?.restuarantTableView.reloadData()
            }).disposed(by: self.searchVM.disposeBag)
        } else {
            searchVM.loadPopular = true
            searchVM.arrayOfItems.removeAll()
            searchVM.arrayOfStores.removeAll()
            self.restuarantTableView.backgroundView = self.restuarantEmptyView
            restuarantTableView.reloadData()
        }
    }
}
