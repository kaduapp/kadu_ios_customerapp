//
//  DispensariesVM.swift
//  UFly
//
//  Created by 3 Embed on 24/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DispensariesVM: NSObject {
    
    let disposeBag = DisposeBag()
    let DispensariesVM_response = PublishSubject<Bool>()
    var store = [Store]()
    var controllerCategory = Category.init(data: [:])
    
    
    /// <#Description#>
    func getDispensaries(){
        HomeAPICalls.getDispensaries(category: controllerCategory.Id).subscribe(onNext: { result in
            self.store = result
            print(result)
            self.DispensariesVM_response.onNext(true)
        }, onError: { error in
            print(error)
        }).disposed(by: disposeBag)
        
    }
}

