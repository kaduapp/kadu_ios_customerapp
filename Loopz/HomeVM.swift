//
//  HomeVM.swift
//  UFly
//
//  Created by 3Embed on 08/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class HomeVM: NSObject {
    
    let disposeBag = DisposeBag()
    let homeVM_response = PublishSubject<Bool>()
    var categories = [Category]()
    var items = [Item]()
    var itemsFav = [Item]()
    var itemsLow = [Item]()
    var store = Store.init(data: [:])
    var List_Store = Store.init(data: [:])
    var offers:[Offer] = []
    var brands:[Brand] = []
    var titles = [StringConstants.Offers(),StringConstants.TrendingProducts(),StringConstants.EveryDayLowPrice(),StringConstants.Favourite(),StringConstants.Categories()]
    
    
    /// subscription to categories API defined in HomeAPI Call
    func getData() {
        
        HomeAPICalls.categoryData().subscribe(onNext: {[weak self]result in
            self?.categories = result.0
            self?.items = result.1
            self?.itemsFav = result.2
            self?.store = result.3
            self?.itemsLow = result.4
            self?.offers = result.5
            self?.brands = result.6
            self?.homeVM_response.onNext(true)
            }, onError: {error in
        }).disposed(by: disposeBag)
    }
    
    func setSubscribe() {
        FavouriteAPICalls.favVMResponse.subscribe(onNext: { [weak self]data in
            if data.1 == 2 {
                self?.itemsFav.removeAll()
                self?.itemsFav.append(contentsOf: data.0)
                self?.homeVM_response.onNext(true)
            } else {
                if self?.store.Id == data.0[0].StoreId {
                    if data.1 == 1 {
                        self?.itemsFav.append(data.0[0])
                        self?.homeVM_response.onNext(true)
                    }else if data.1 == 0 {
                        for item in (self?.itemsFav)! {
                            if item.Id == data.0[0].Id {
                                self?.itemsFav.remove(at: (self?.itemsFav.index(of: item))!)
                            }
                        }
                        self?.homeVM_response.onNext(false)
                    }
                }
            }
        }).disposed(by: self.disposeBag)
    
    }
    
}
