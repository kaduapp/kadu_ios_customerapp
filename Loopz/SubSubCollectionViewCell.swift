//
//  SubSubCollectionViewCell.swift
//  DelivX
//
//  Created by 3EMBED on 19/04/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SubSubCollectionViewCell: UICollectionViewCell {
    
    var itemListVM = ItemListingVM()
    var itemListingVC = ItemListingVC()
    var Products = [Item]()
    
    @IBOutlet weak var itemCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    itemCollectionView.register(UINib(nibName: String(describing: ItemCommonCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self))
    }
    
    
}

extension SubSubCollectionViewCell : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        //Case for Products in sub subcategory array or Filters
        if Products.count > 0{
            return Products.count
        }
        
        // Case if there are products in Store
     
        if let count = itemListVM.store?.Products.count, count > 0{
        return count
        }
        
        return 0
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:ItemCommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCommonCollectionViewCell", for: indexPath) as! ItemCommonCollectionViewCell
        
        //Case for Products in sub subcategory array or Filters
        
        if Products.count > 0{
          
         cell.setValue(data: Products[indexPath.row],isDelete: false)
        }
        else{
        
        //Case for Products in Store
        if let product = itemListVM.store?.Products[indexPath.row]{
        cell.setValue(data: product,isDelete: false)
        }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         //Case for Products in sub subcategory array
             if Products.count > 0{
            self.itemListingVC.performSegue(withIdentifier: UIConstants.SegueIds.ItemListingToItemDetails, sender: (indexPath,Products))
             }
             else{
        
          //Case for Products in Store

           self.itemListingVC.performSegue(withIdentifier: UIConstants.SegueIds.ItemListingToItemDetails, sender: (indexPath,itemListVM.store?.Products))
        }
    }
 
    
 
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
                return CGSize(width: CGFloat((UIScreen.main.bounds.width - 40)/2), height: 225)
            }
    
    
    
}
