//
//  OrderDetailUIE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension OrderDetailVC {
    
    /// inital view setup
    func initialSetup() {
        Helper.PullToClose(scrollView: mainTableView)
        Helper.pullToDismiss?.delegate = self
        navigationWidth.constant = UIScreen.main.bounds.size.width - 20
        mainTableView.rowHeight = UITableView.automaticDimension
        Fonts.setPrimaryMedium(navTitle)
        Fonts.setPrimaryRegular(navSubTitle)
        mainTableView.backgroundColor = Colors.ScreenBackground
        cancelBtn.setTitle(StringConstants.Cancel(), for: .normal)
        navTitle.textColor = Colors.PrimaryText
        navSubTitle.textColor = Colors.SecoundPrimaryText
        cancelBtn.titleLabel?.textColor = Colors.Red
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
    }
    
    
    /// updates the orderID on the NavigationBar
    func updateOrderID() {
        if  (orderDetailVM.selectedOrder?.Status)! < 9 && (orderDetailVM.selectedOrder?.Status)! != 3 && (orderDetailVM.selectedOrder?.Status)! != 2 {
            cancelBtn.isHidden = false
        }
        else {
            cancelBtn.isHidden = true
        }
        if (orderDetailVM.selectedOrder?.Status)! < 15 && (orderDetailVM.selectedOrder?.Status)! >= 8 {
            cancelBtn.isHidden = false
            cancelBtn.titleLabel?.textColor = Colors.AppBaseColor
            cancelBtn.setTitle(StringConstants.Track(), for: .normal)
        }
        
        if orderDetailVM.selectedOrder?.statusCode == 1{
            cancelBtn.isHidden = false
        }else{
            cancelBtn.isHidden = true
        }

        let bid:Int = (orderDetailVM.selectedOrder?.BookingId)!
        navTitle.text = "\(bid)"
        let dateString = Helper.getDateString(value: (orderDetailVM.selectedOrder?.BookingDate)!, format: DateFormat.TimeFormatToDisplay, zone: true)
        let order:Order = (self.orderDetailVM.selectedOrder)!
        
        let itemCount = String(describing: order.ItemsData.count)
        let amount = Helper.df2so(Double( order.TotalAmount))
        let temp = "|"
        
        navSubTitle.text = dateString + " " + temp + " " + itemCount + " " + StringConstants.Items() + " " + amount
        
    }
    
}
