//
//  CheckOutProfileCell.swift
//  UFly
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import Kingfisher

class MainProfileCell: UITableViewCell {
    
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileSeperator: UIView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var cellBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /// initial Viewsetup
    func initialSetup(){
        Helper.setShadow(sender: profileImage)
        //Fonts.setPrimaryBold(profileName)
        Fonts.setPrimaryBold(profileName, size: 18)
        profileName.textColor =  Colors.PrimaryText
        
        //Fonts.setPrimaryRegular(phoneLabel)
        Fonts.setPrimaryRegular(phoneLabel, size: 12)
        phoneLabel.textColor =  Colors.SeconderyText
        
        emailLabel.isHidden = true
        Fonts.setPrimaryRegular(emailLabel)
        emailLabel.textColor =  Colors.SeconderyText
        profileImage.layer.cornerRadius = CGFloat(Helper.setRadius(element: profileImage))
        profileImage.clipsToBounds = true
        profileSeperator.backgroundColor =  Colors.ScreenBackground
        profileName.text = StringConstants.Login()
        
        if Changebles.isGrocerOnly{
            self.editBtn.isHidden = true
        }
        else{
            self.editBtn.isHidden = false
        }
        //Helper.setButtonTitle(normal: StringConstants.Edit, highlighted: StringConstants.Edit, selected: StringConstants.Edit, button: editBtn)
        
        
        
    }
    
    /// this method updates profile
    ///
    /// - Parameter userData: it is of type User model Object
    func updateProfile(userData: User) {
        var prefix = ""
        if Utility.getLogin() {
            prefix = StringConstants.Hi() + ", "
        }
        profileName.text = prefix + userData.FirstName
        emailLabel.text = ""//userData.Email
        phoneLabel.text = userData.CountryCode + userData.Phone
        Helper.setImage(imageView: profileImage, url: userData.userProfilePic, defaultImage: #imageLiteral(resourceName: "UserDefault") )
    }
 
}
