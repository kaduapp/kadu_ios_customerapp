//
//  LoadingProgress.swift
//  Channel 40
//
//  Created by STARK on 04/02/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class LoadingProgress: UIView {
    
    @IBOutlet weak var progressImage: UIImageView!
    private static var obj: LoadingProgress? = nil
    static var shared: LoadingProgress {

        if obj == nil {
            obj = UINib(nibName: "LoadingProgress", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as? LoadingProgress
            obj?.frame = UIScreen.main.bounds
        }
        return obj!
    }
    @IBOutlet weak var loadingHead: UILabel!
    
   private func setup() {
        let window:UIWindow = UIApplication.shared.delegate!.window!!
        window.addSubview(self)
        
//        self.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
//
//        UIView.animate(withDuration: 0.0, delay: 0.0, options: .beginFromCurrentState, animations: {() -> Void in
//            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
//
//        }, completion: {(_ finished: Bool) -> Void in
//        })
    
        let when = DispatchTime.now() + 30
        
        DispatchQueue.main.asyncAfter(deadline: when){
            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
//            UIView.animate(withDuration: 0.0, delay: 0.0, options: .beginFromCurrentState, animations: {() -> Void in
//                self.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
//            }, completion: {(_ finished: Bool) -> Void in
                self.removeFromSuperview()
//            })
        }
    
    }
    
    func showPI(message: String) {
        LoadingProgress.obj?.backgroundColor = Colors.SecondBaseColor.withAlphaComponent(0.3)
        DispatchQueue.main.async() { // or you can use NSOperationQueue.mainQueue().addOperationWithBlock()
            self.setup()
            Fonts.setPrimaryMedium(self.loadingHead)
            self.loadingHead.text = message
            let jeremyGif = UIImage.gifImageWithName("loading_orange")
            self.progressImage.image = jeremyGif
        }
        
        
        let when = DispatchTime.now() + 30
        
        DispatchQueue.main.asyncAfter(deadline: when){
            self.hide()
        }
    }
    
    func showPIWhite(message: String) {
        LoadingProgress.obj?.backgroundColor  = Colors.SecondBaseColor
        DispatchQueue.main.async() { // or you can use NSOperationQueue.mainQueue().addOperationWithBlock()
            self.setup()
            Fonts.setPrimaryMedium(self.loadingHead)
            self.loadingHead.text = message
            let jeremyGif = UIImage.gifImageWithName("loading_orange")
            self.progressImage.image = jeremyGif
        }
        
        
        let when = DispatchTime.now() + 30
        
        DispatchQueue.main.asyncAfter(deadline: when){
            self.hide()
        }
    }
    
    func hide() {
       DispatchQueue.main.async() { // or you can use NSOperationQueue.mainQueue().addOperationWithBlock()
            self.removeFromSuperview()
        }
    }
    
    
    func setLabel(text:String) {
        loadingHead.text = text
    }
}
