//
//  PaymentListCell.swift
//  DelivX
//
//  Created by 3 Embed on 30/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PaymentListCell: UITableViewCell {        //Reused in profile payment list and language

    @IBOutlet weak var paymentTypeTitle: UILabel!
    @IBOutlet weak var languageSelectBtn: UIButton!    // for language Screen only
    @IBOutlet weak var separator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //Fonts.setPrimaryMedium(paymentTypeTitle)
        Fonts.setPrimaryRegular(paymentTypeTitle, size: 14)
        paymentTypeTitle.textColor = Colors.PrimaryText
        if languageSelectBtn != nil {
            languageSelectBtn.tintColor = Colors.AppBaseColor
        }
        if separator != nil {
            separator.backgroundColor = Colors.SeparatorLight
        }
    }

    func updateCell(data:Language,flag:Bool) {
        paymentTypeTitle.text = data.Name
        if flag {
            languageSelectBtn.setImage(#imageLiteral(resourceName: "CheckOn"), for: .normal)
        }else{
            languageSelectBtn.setImage(#imageLiteral(resourceName: "Check_off"), for: .normal)
        }
    }
    
    func updateCell(string:String) {
        paymentTypeTitle.text = string
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
