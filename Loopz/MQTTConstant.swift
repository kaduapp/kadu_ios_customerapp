//
//  MQTTConstant.swift
//  KarruPro
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

struct MQTTConstants {
//    ///Ask server guy for host and port.
//    static let host = "45.76.152.163"
//    static let port:UInt32 = 1883
//
//    /// your current userID
//    static let userID = Utility.userId
//    static let userName = "uberforall"
//    static let password = "uberforall"

    static let host = Changebles.host
    static let port:UInt32 = Changebles.port

    /// your current userID
    static let userID = Utility.AppName + "_" + Utility.getProfileData().SId + "_" + Utility.DeviceId
    static let userName = Changebles.userName
    static let password = Changebles.password

}


