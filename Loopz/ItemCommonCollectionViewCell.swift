//
//  ItemCommonCollectionViewCell.swift
//  DelivX
//
//  Created by 3Embed on 03/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ItemCommonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var oldPriceCut: UIView!
    @IBOutlet weak var cartButtonsContainer: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var backFullView: UIView!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var rightHandDescription: UILabel!
    @IBOutlet weak var leftHandDescription: UILabel!
    @IBOutlet weak var descriptionSeparator: UIView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var effectHeight: NSLayoutConstraint!
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var addCartView: UIView!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var customizableLabel: UILabel!
    @IBOutlet weak var addViewHeight: NSLayoutConstraint!
    
    var dataItem:Item? = nil
    var cartDb = CartDBManager()
    var cartVM = CartVM()
    var isFromSearchTab = false
    let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    
    
    @IBAction func didTapAddToCart(_ sender: Any) {
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
        if Helper.checkCartType(arrayOfCarts:self.cartVM.arrayOfCart, storeType:Utility.getStore().TypeStore , cartType:Utility.getStore().cartType ,completionHandler: { (success) in
            AppConstants.CartCount = 0
            AppConstants.TotalCartPrice = 0
            self.cartVM.arrayOfCart.removeAll()
            self.cartVM.arrayOfTax.removeAll()
            self.cartVM.dataItem = nil
            self.dataItem?.setCart()
            self.addItemToCart()
        }){
           self.addItemToCart()
        }
    }
    
    
    func addItemToCart(){
     
      self.dataItem?.CartQty = 1
      if (self.dataItem?.AddOnAvailable)! && (dataItem?.AddOnGroups.count)! > 0 && Utility.getSelectedSuperStores().type == .Restaurant {
      Helper.openAddons(item: self.dataItem!, cart: false) { success in
            if success == .Add {
                        self.dataItem?.CartQty = 1
                        self.updateData()
                        DispatchQueue.main.async {
                          self.cartVM.addCart(cartItem: CartItem.init(item: self.dataItem!))
                        }
                        self.addToCartBtn.isHidden = true
                        self.addCartView.isHidden = false
                        self.setRecentUpdate()
                    }
                }
        }else{
                self.updateData()
                DispatchQueue.main.async {
                    self.cartVM.addCart(cartItem: CartItem.init(item: self.dataItem!))
                }
                self.addToCartBtn.isHidden = true
                self.addCartView.isHidden = false
        }
    }
    
    
    func setRecentUpdate() {
        self.dataItem?.RecentAddOns.removeAll()
        for each in (self.dataItem?.AddOnGroups)! {
            for eachIn in each.AddOns {
                if eachIn.Selected {
                    self.dataItem?.RecentAddOns.append(eachIn.Id)
                }
            }
        }
    }
    
    
    
    @IBAction func plusBtnAction(_ sender: Any) {
        
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
         print("array of Cart",self.cartVM.arrayOfCart)
        if Helper.checkCartType(arrayOfCarts:self.cartVM.arrayOfCart, storeType: Utility.getStore().TypeStore, cartType: Utility.getStore().cartType,completionHandler: { (success) in
            self.cartVM.arrayOfCart.removeAll()
            self.cartVM.arrayOfTax.removeAll()
            self.cartVM.dataItem = nil
            self.dataItem?.CartQty = 0
            self.cartVM.getCart()
            self.checkAvailableQuantity()
            
        }){
            
          self.checkAvailableQuantity()
            
        }
    }
    
    
    func checkAvailableQuantity(){
        
        if Utility.getSelectedSuperStores().type == .Restaurant {
            self.plusItemToCart()
        }
            
        else{
            if let updatedCount = self.dataItem?.CartQty, let qty = self.dataItem?.availableQuantity,updatedCount >= qty {
                
                if qty == 1{
                Helper.showAlert(message: "", head: "This item has only \(qty) unit available", type: 0)
                }
                else{
                    Helper.showAlert(message: "", head: "This item has only \(qty) units available", type: 0)
                }
            }
            else{
                self.plusItemToCart()
            }
        }

    }
    
    func plusItemToCart(){
        
        dataItem?.CartQty = (dataItem?.CartQty)! + 1
        if (dataItem?.AddOnAvailable)! && (dataItem?.AddOnGroups.count)! > 0 {
            Helper.openAddons(item: dataItem!, cart: false) { success in
                if success == .Add {
                    DispatchQueue.main.async {
                        self.dataItem?.CartQty = 1
                        self.cartVM.addCart(cartItem: CartItem.init(item: self.dataItem!))
                        self.updateData()
                    }
                }else if success == .Patch {
                    DispatchQueue.main.async {
                        let dataFull = CartDBManager().getCartFullCount(data: self.dataItem!)//getCartCount(data: self.dataItem!)
                        var data = CartItem.init(data: [:])
                        if dataFull.count > 0 {
                            data = dataFull.last!
                        }else{
                            data = CartItem.init(item: self.dataItem!)
                        }
                        data.QTY = data.QTY + 1
                        self.cartVM.updateCart(cartItem: data)
                        self.updateData()
                    }
                }
                self.addToCartBtn.isHidden = true
                self.addCartView.isHidden = false
                self.setRecentUpdate()
            }
        }else{
            updateData()
            DispatchQueue.main.async {
                let dataFull = CartDBManager().getCartFullCount(data: self.dataItem!)//getCartCount(data: self.dataItem!)
                var data = CartItem.init(data: [:])
                if dataFull.count > 0 {
                    data = dataFull.last!
                }else{
                    data = CartItem.init(item: self.dataItem!)
                }
                data.QTY = data.QTY + 1
                self.cartVM.updateCart(cartItem: data)
                self.updateData()
                
            }
        }
    }
    
    @IBAction func minusBtnAction(_ sender: Any) {
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
        let count = (dataItem?.CartQty)! - 1
        
        if count > 0 {
            dataItem?.CartQty = count
            addToCartBtn.isHidden = true
            addCartView.isHidden = false
            DispatchQueue.main.async {
                let data = CartDBManager().getCartFullCount(data: self.dataItem!).last
                data?.QTY = (data?.QTY)! - 1
                self.cartVM.updateCart(cartItem: data!)
            }
        }else {
            dataItem?.CartQty = 0
            addToCartBtn.isHidden = false
            addCartView.isHidden = true
            DispatchQueue.main.async {
                self.cartVM.deleteCart(cartItem: CartDBManager().getCartFullCount(data: self.dataItem!).last!)
            }
        }
        updateData()
        
    }
    
    
    /// this method is sets the getItems API respose data
    //
    /// - Parameter data: data it is of type Item
    func setValue(data:Item, isDelete: Bool) {
        
           dataItem = data
        
        
        if customizableLabel != nil {
        if let isAvailable = dataItem?.AddOnAvailable,isAvailable , Utility.getStore().TypeStore == .Restaurant{
            
            if addViewHeight != nil{
                addViewHeight.constant = 40}
             customizableLabel.text = StringConstants.Customizable()
        }
        else{
             if addViewHeight != nil{
                addViewHeight.constant = 30}
             customizableLabel.text = ""
         }
        }
        
        if favouriteButton != nil {
            if isDelete {
                self.favouriteButton.setImage(#imageLiteral(resourceName: "Delete"), for: .normal)
                favouriteButton.tintColor =  Colors.Red
            }
            else {
                if data.Fav {
                    self.favouriteButton.setImage(#imageLiteral(resourceName: "Favourited"), for: .normal)
                }else{
                    self.favouriteButton.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
                }
                favouriteButton.tintColor =  Colors.AppBaseColor
            }
        }
        
        if itemImage != nil {
            if data.Images.count > 0 {
                let url = data.Images[0]
                Helper.setImage(imageView: itemImage, url: url, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
            }else{
                itemImage.image = #imageLiteral(resourceName: "SingleShowDefault")
            }
        }
        
        if storeNameLabel != nil{
            if isFromSearchTab{
                storeNameLabel.text = data.StoreName
            }
            else{
                storeNameLabel.text = ""
            }
        }
        
        itemNameLabel.text = data.Name
        if subtitle != nil {
            subtitle.text = data.SecondCategoryName.uppercased()
        }
        
        if totalPriceLabel != nil {
            totalPriceLabel.text = Helper.df2so(Double( data.FinalPrice))
        }
        
        if oldPriceLabel != nil {
            
            if data.Discount > 0{
                oldPriceCut.isHidden = false
                oldPriceLabel.isHidden = false
                oldPriceLabel.text = Helper.df2so(Double( data.Price))
            }
            else{
                oldPriceCut.isHidden = true
                oldPriceLabel.isHidden = true
            }
        }
        if effectHeight != nil {
            if effectHeight != nil {
                if data.THC == 0 && data.CBD == 0 {
                    effectHeight.constant = 0
                }else{
                    effectHeight.constant = 21
                }
            }
            rightHandDescription.text = "\(data.THC)" + StringConstants.THC()
            leftHandDescription.text = "\(data.CBD)" + StringConstants.CBD()
        }
        
        if data.outOfStock && Utility.getSelectedSuperStores().type != .Restaurant{
         addToCartBtn.setTitleColor(UIColor.red, for: UIControl.State.normal)
      Helper.setButtonTitle(normal:StringConstants.outOfStock(),highlighted:StringConstants.outOfStock(),selected:StringConstants.outOfStock(),button:addToCartBtn)
          self.addToCartBtn.isEnabled = false
           return
        }
        
        addToCartBtn.setTitleColor(Colors.AppBaseColor, for: UIControl.State.normal)
    Helper.setButtonTitle(normal:StringConstants.AddCart(),highlighted:StringConstants.AddCart(),selected:StringConstants.AddCart(),button:addToCartBtn)
        
     
        let count = (dataItem?.CartQty)!
        
        if count > 0 {
            dataItem?.CartQty = count
            addToCartBtn.isHidden = true
            addCartView.isHidden = false
        }else {
            dataItem?.CartQty = 0
            addToCartBtn.isHidden = false
            addCartView.isHidden = true
        }
        quantity.text = String(describing: count)
     
    }
    
    
    /// initial ViewSetup
    func initialSetup(){
        if favouriteButton != nil {
            favouriteButton.tintColor =  Colors.AppBaseColor
        }
        if cartButtonsContainer != nil {
            Helper.setUiElementBorderWithCorner(element: cartButtonsContainer, radius:1, borderWidth: 1,color: Colors.SeparatorLight)
        }else{
            Helper.setUiElementBorderWithCorner(element: addToCartBtn, radius:1, borderWidth: 1,color: Colors.SeparatorLight)
            if plusBtn != nil {
                Helper.setUiElementBorderWithCorner(element: plusBtn, radius: Helper.setRadius(element: plusBtn), borderWidth: 1.0,color: Colors.AppBaseColor)
            }
            if minusBtn != nil {
                Helper.setUiElementBorderWithCorner(element: minusBtn, radius: Helper.setRadius(element: minusBtn), borderWidth: 1.0,color:  Colors.AppBaseColor)
            }
        }
        
        if backFullView != nil {
            Helper.setUiElementBorderWithCorner(element: backFullView, radius: 3, borderWidth: 0, color:  Colors.SeparatorLight)
            Helper.setShadow(sender: backFullView)
        }
        Helper.setButtonTitle(normal:StringConstants.AddCart(),highlighted:StringConstants.AddCart(),selected:StringConstants.AddCart(),button:addToCartBtn)
        
        if leftHandDescription != nil {
            leftHandDescription.textColor =  Colors.AppBaseColor
            rightHandDescription.textColor =  Colors.AppBaseColor
            Fonts.setPrimaryRegular(leftHandDescription)
            Fonts.setPrimaryRegular(rightHandDescription)
        }
        if totalPriceLabel != nil {
            totalPriceLabel.textColor =  Colors.PrimaryText
            Fonts.setPrimaryBold(totalPriceLabel)
        }
        if subtitle != nil {
            subtitle.textColor =  Colors.SeconderyText
        }else{
            if totalPriceLabel != nil {
                totalPriceLabel.textColor =  Colors.SeconderyText
                Fonts.setPrimaryBold(totalPriceLabel)
            }
        }
        itemNameLabel.textColor =  Colors.PrimaryText
        
        
        Fonts.setPrimaryMedium(itemNameLabel)
        if oldPriceLabel != nil {
            Fonts.setPrimaryBold(oldPriceLabel)
            oldPriceLabel.textColor =  Colors.SeconderyText
        }
        
        if oldPriceCut != nil {
            oldPriceCut.backgroundColor =  Colors.SeconderyText
        }
        Fonts.setPrimaryRegular(quantity)
        
        addToCartBtn.setTitleColor(Colors.AppBaseColor, for: UIControl.State.normal)
        
        if plusBtn != nil {
            plusBtn.tintColor = Colors.AppBaseColor
            minusBtn.tintColor = Colors.AppBaseColor
            plusBtn.setTitleColor(Colors.AppBaseColor, for: .normal)
            minusBtn.setTitleColor(Colors.AppBaseColor, for: .normal)
            Fonts.setPrimaryBold(minusBtn)
            Fonts.setPrimaryBold(plusBtn)
            Fonts.setPrimaryBold(quantity)
        }
        
        CartVM.cartVM_response.subscribe(onNext: {success in
            self.cartVM.arrayOfCart = success.1
            self.dataItem?.setCart()
            self.updateData()
        }).disposed(by: self.disposeBag)
        CartDBManager.cartDBManager_response.subscribe(onNext: { success in
            if success == false {
                self.dataItem?.setCart()
                self.updateData()
            }
        }).disposed(by: self.disposeBag)
        
        if customizableLabel != nil{
            customizableLabel.textColor = Colors.SeconderyText
            Fonts.setPrimaryMedium(customizableLabel)
        }
    }
    
    
    /// updates cell
    func updateData(){
        if (dataItem != nil) {
            let count = (self.dataItem?.CartQty)!
            if count > 0 {
                self.dataItem?.CartQty = count
                self.addToCartBtn.isHidden = true
                self.addCartView.isHidden = false
            }else {
                self.dataItem?.CartQty = 0
                self.addToCartBtn.isHidden = false
                self.addCartView.isHidden = true
            }
            self.quantity.text = String(describing: (self.dataItem?.CartQty)!)
            self.dataItem?.CartId = Utility.getCartId()
        }
    }
    
}
