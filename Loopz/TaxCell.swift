//
//  TaxCell.swift
//  DelivX
//
//  Created by 3Embed on 10/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class TaxCell: UICollectionViewCell {
    
    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var value:UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Fonts.setPrimaryRegular(title)
        Fonts.setPrimaryRegular(value)
        title.textColor = Colors.PrimaryText
        value.textColor = Colors.PrimaryText
        topSeparator.backgroundColor = Colors.SeparatorLight
    }
    
    func setData(data:Tax) {
        title.text = data.Name
        if data.Value > 0 {
            title.text = title.text! + "(\(data.Value)%)"
        }
        value.text = Helper.df2so(Double( data.Price))
    }
    
    func setSavings() {
        
        title.text = StringConstants.YourSavings()
        value.text = Helper.df2so(Double(AppConstants.CartDiscount))
        
    }

}
