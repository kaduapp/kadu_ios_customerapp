//
//  AddressUtility.swift
//  DelivX
//
//  Created by Rahul Sharma on 11/10/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

class AddressUtility: NSObject {
    
    class func getSortedAddressFromLocation(_ location: Location) -> String {
        
        var address = ""
        if location.FullText.count > 0 && !address.contains(location.FullText){
                   address = address + "\(location.FullText )"
             let result = "\(location.FullText )".split(separator: ",")
           
            if location.Country.length == 0 {
                 if result.count > result.count - 1 {
              location.Country = String(result[result.count - 1])
                }
                 if address.contains(location.FullText) {
                    location.State = String(result[result.count - 2].dropLast(6))
            }
                if address.contains(location.Zipcode){
                    location.Zipcode = String(result[result.count - 2].dropFirst(((result[result.count - 2]).count - 6)))
                }
                if address.contains(location.City) {
                                  location.City = String(result[result.count - 3])
                          }
            }
               }else if location.MainText.length > 0{
            address = address + "\(location.MainText ), "
        }
        if location.Name.length > 0 && !address.contains(location.Name){
            address = address + "\(location.Name ), "
        }
        if location.City.length > 1 && !address.contains(location.City){
            address = address + "\(location.City ), "
        }
        if location.State.length > 1 && !address.contains(location.State){
            address = address + "\(location.State ), "
        }
        if location.Zipcode.length > 0 && !address.contains(location.Zipcode){
            address = address + "\(location.Zipcode ), "
        }
        
        if location.Country.length > 0 {
            address = address + "\(location.Country )"
        }
        else{
            if address.length > 0{
                let endIndex = address.index(address.endIndex, offsetBy: -2)
                address = String(address[..<endIndex])
            }
        }
        return address
    }
    
    class func getSortedAdress(_ data : Address) -> String{
        var address = ""
        if data.PhoneNumber.length > 0{
            address = "\(data.PhoneNumber), "
        }
        if data.FlatNo.length > 0{
            address =  address + "\(data.FlatNo), "
        }
        if data.AddLine1.length > 0{
            address = address + "\(data.AddLine1 ), "
        }
        if data.AddLine2.length > 0 && !address.contains(data.AddLine2){
            address = address + "\(data.AddLine2 ), "
        }
        if data.City.length > 1 && !address.contains(data.City){
            address = address + "\(data.City ), "
        }
        if data.State.length > 1 && !address.contains(data.State){
            address = address + "\(data.State ), "
        }
        if data.Zipcode.length > 0 && !address.contains(data.Zipcode){
            address = address + "\(data.Zipcode ), "
        }
        if data.Country.length > 0 {
            address = address + "\(data.Country )"
        }
        else{
            if address.length > 0{
                let endIndex = address.index(address.endIndex, offsetBy: -2)
                address = String(address[..<endIndex])
            }
        }
        
        if data.LandMark.length > 0{
            address = "\(address)\n\(data.LandMark)"
        }
        
        return address
    }
    
}

