//
//  ItemListingTVE.swift
//  UFly
//
//  Created by 3 Embed on 12/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

///UICollectionViewDelegate
extension ItemListingVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.searchVM.arrayOfExpandedStore.Products.count > 0{
          return 1
        }
        else if self.itemListingVM.SubSubCatArray.count > 0{
            
            return  self.itemListingVM.SubSubCatArray.count
        }
        
      return 1
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SubSubCollectionViewCell.self), for: indexPath) as! SubSubCollectionViewCell
        cell.itemListVM = self.itemListingVM
        cell.itemListingVC = self
        
        if self.searchVM.arrayOfExpandedStore.Products.count > 0{
         cell.Products = self.searchVM.arrayOfExpandedStore.Products
        }
        else if self.itemListingVM.SubSubCatArray.count > 0{
        cell.Products = self.itemListingVM.SubSubCatArray[indexPath.row].Products
        }
        else{
        cell.Products.removeAll()
        }
        cell.itemCollectionView.reloadData()
        return cell

   }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: self.view.frame.size.width, height: self.subsubCategoryCollectionView.frame.size.height)
    }
        
}
