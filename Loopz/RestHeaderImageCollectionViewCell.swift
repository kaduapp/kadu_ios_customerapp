//
//  RestHeaderImageCollectionViewCell.swift
//  DelivX
//
//  Created by 3EMBED on 26/11/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class RestHeaderImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var offerImageView: UIImageView!
}
