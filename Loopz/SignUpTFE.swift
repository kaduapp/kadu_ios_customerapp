//
//  SignupTFE.swift
//  UFly
//
//  Created by 3Embed on 07/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension SignUpVC : UITextFieldDelegate{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.length == 1 {
            Helper.setButtonAndView(button: signUpView.signupButton, view: signUpView.signUpButtonView, primaryColour: Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor, shadow: true)
            
        }
        
        switch textField {
        case signUpView.mobileNumberTF:
            signUpView.phNumSeparator.backgroundColor =  Colors.AppBaseColor
            signUpView.phoneNum.append(string)
            if range.location+1 == signupVM.authentication.CountryCode.sorted().count{
                return false
            }
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case signUpView.nameTF:
            signUpView.nameSeparator.backgroundColor =  Colors.AppBaseColor
            break
        case signUpView.emailTF:
            signUpView.emailSeparator.backgroundColor =  Colors.AppBaseColor
            break
        case signUpView.passwordTF:
            signUpView.pwSeparator.backgroundColor =  Colors.AppBaseColor
            
        case signUpView.referralTF:
            let maxLength = 5
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            signUpView.refSeparator.backgroundColor =  Colors.AppBaseColor
            return newString.length <= maxLength
        default: break
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //        var frame = textField.frame
        //        frame.origin.y = frame.origin.y + 50
        //        scrollView.scrollRectToVisible(frame, animated: true)
        switch textField {
        case self.signUpView.mobileNumberTF:
            signUpView.phNumSeparator.backgroundColor =  Colors.AppBaseColor
            break
        case self.signUpView.nameTF:
            signUpView.nameSeparator.backgroundColor =  Colors.AppBaseColor
            break
        case self.signUpView.emailTF:
            signUpView.emailSeparator.backgroundColor =  Colors.AppBaseColor
            break
        case self.signUpView.passwordTF:
            signUpView.pwSeparator.backgroundColor =  Colors.AppBaseColor
            break
        case self.signUpView.referralTF:
            signUpView.refSeparator.backgroundColor =  Colors.AppBaseColor
            break
        default: break
            
        }
        if textField == signUpView.mobileNumberTF {
            Helper.addDoneButtonOnTextField(tf: textField, vc: self.signUpView)
            if textField.text?.sorted().count == 0 {
                textField.text = Helper.setCountryCode()
                signupVM.authentication.CountryCode = Helper.setCountryCode()
            }
        }
        signUpView.activeTextField = textField
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if signupVM.doSignUp == true {
            validateFields()
            return
        }
        signUpView.activeTextField = nil
        switch textField {
        case signUpView.mobileNumberTF:
            signUpView.phNumSeparator.backgroundColor =  Colors.SeparatorLight
            let phoneNumber = signUpView.mobileNumberTF.text?.replacingOccurrences(of: signupVM.authentication.CountryCode, with: "")
            signupVM.authentication.Phone = phoneNumber!
            signUpView.nameTF.becomeFirstResponder()
            
            if Helper.isPhoneNumValid(phoneNumber: phoneNumber!, code: signupVM.authentication.CountryCode)  == true {
                signupVM.validatePhone()
            }
            else {
            }
            
            break
        case signUpView.nameTF:
            signUpView.nameSeparator.backgroundColor =  Colors.SeparatorLight
            signupVM.authentication.Name = signUpView.nameTF.text!
            break
        case signUpView.emailTF:
            signUpView.emailSeparator.backgroundColor =  Colors.SeparatorLight
            if Helper.isValidEmail(text: signUpView.emailTF.text!) {
                self.signUpView.emailTF.text = textField.text
                signupVM.authentication.Email = signUpView.emailTF.text!
                signupVM.validateEmail()
            }
            break
        case signUpView.passwordTF:
            signUpView.pwSeparator.backgroundColor =  Colors.SeparatorLight
            signupVM.authentication.Password = signUpView.passwordTF.text!
            break
        case  signUpView.referralTF :
            signUpView.refSeparator.backgroundColor =   Colors.SeparatorLight
            
            break
        default: break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
            
        case self.signUpView.mobileNumberTF:
            self.signUpView.nameTF.becomeFirstResponder()
            break
        case self.signUpView.nameTF:
            self.signUpView.emailTF.becomeFirstResponder()
            break
        case self.signUpView.emailTF:
            self.signUpView.passwordTF.becomeFirstResponder()
            break
        case self.signUpView.passwordTF:
            textField.resignFirstResponder()
            break
        case self.signUpView.referralTF:
            textField.resignFirstResponder()
            break
        default: break
            
        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        signupVM.doSignUp = false
        switch textField {
            
        case self.signUpView.mobileNumberTF:
            if self.signUpView.mobileNumberTF.text?.sorted().count != 0 {
                
                let count = signupVM.authentication.CountryCode.sorted().count
                let phoneNumber = signUpView.mobileNumberTF.text?.replacingOccurrences(of: signupVM.authentication.CountryCode, with: "")
                self.signupVM.validPhone = false
                if Helper.isPhoneNumValid(phoneNumber: phoneNumber!, code: signupVM.authentication.CountryCode) && (self.signUpView.mobileNumberTF.text?.sorted().count)! > count {
                    self.signUpView.phNumCheckMark.isHidden = false
                }else {
                    self.signUpView.phNumCheckMark.isHidden = true
                    
                }
            }
            break
        case self.signUpView.nameTF:
            if Helper.isValidName(textName: self.signUpView.nameTF.text!) && (self.signUpView.nameTF.text!.sorted().count) >= 3 {
                self.signUpView.nameCheckMark.isHidden = false
            }
            else {
                self.signUpView.nameCheckMark.isHidden = true
            }
            break
        case self.signUpView.emailTF:
            self.signupVM.validEmail = false
            if Helper.isValidEmail(text: textField.text!) {
                self.signUpView.emailCheckMark.isHidden = false
            } else {
                self.signUpView.emailCheckMark.isHidden = true
            }
            break
        case self.signUpView.passwordTF:
            if (self.signUpView.passwordTF.text?.sorted().count)! >= 6 {
                self.signUpView.pwCheckMark.isHidden = false
            } else {
                self.signUpView.pwCheckMark.isHidden = true
            }
            
            break
            
        case self.signUpView.referralTF:
            if  (self.signUpView.referralTF.text?.sorted().count)! == 5 {
                self.signUpView.refCheckMark.isHidden = false
                signupVM.validateReferralCode(reffCode: signUpView.referralTF.text! )
            }
            else{
                self.signUpView.refCheckMark.isHidden = true
            }
            break
            
        default: break
            
        }
        if self.signUpView.phNumCheckMark.isHidden == false && self.signUpView.emailCheckMark.isHidden == false && signUpView.nameTF.text!.sorted().count != 0 && self.signUpView.pwCheckMark.isHidden == false {
            Helper.setButtonAndView(button: signUpView.signupButton,view:signUpView.signUpButtonView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
            signUpView.signupButton.isUserInteractionEnabled = true
        }else {
            Helper.setButtonAndView(button: signUpView.signupButton, view: signUpView.signUpButtonView, primaryColour: Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor, shadow: true)
            signUpView.signupButton.isUserInteractionEnabled = false
        }
    }
    
}

extension SignUpVC:UITextViewDelegate{
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
}
