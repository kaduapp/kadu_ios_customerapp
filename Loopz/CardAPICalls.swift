//
//  CardAPICalls.swift
//  DelivX
//
//  Created by 3Embed on 06/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire

class CardAPICalls: NSObject {
    
    static let disposeBag = DisposeBag()
    //Add Cart Data
    class func addCard(token:String) ->Observable<Card> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        let url  = APIEndTails.Card
        let params = [
            APIRequestParams.CardToken  : token,
            APIRequestParams.Email      : Utility.getProfileData().Email
        ]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, url, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    if let bodyIn = body as? [String:Any] {
                        if let bodyData = bodyIn[APIResponceParams.Data] as? [String:Any] {
                            observer.onNext(Card.init(data: bodyData))
                            observer.onCompleted()
                        }
                    }
                    
                }
                //APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
            }, onError: { (Error) in
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //get list of card Data
    class func getCard() ->Observable<[Card]> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, APIEndTails.Card, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let bodyIn = body as! [String : Any]
                    
                    var arrayCard:[Card] = []
                    if let data = bodyIn[APIResponceParams.Data] as? [Any] {
                        for dataItem in data {
                            arrayCard.append(Card.init(data: dataItem as! [String:Any]))
                        }
                    }
                    observer.onNext(arrayCard)
                    observer.onCompleted()
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }

    
    
    //Add Cart Data
    class func deleteCard(card:Card) ->Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        let url = APIEndTails.Card + "/" + card.Id
        return Observable.create { observer in
            RxAlamofire.requestJSON(.delete, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    observer.onNext(true)
                    observer.onCompleted()
                }
                APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
            }, onError: { (Error) in
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //Add Cart Data
    class func defaultCard(card:Card) ->Observable<Bool> {
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        let params = [
            APIRequestParams.CardId     : card.Id
        ]
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.Card, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    observer.onNext(true)
                    observer.onCompleted()
                }
                APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
            }, onError: { (Error) in
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: self.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
}
