//
//  AddItemsViewContrller+CollectionView.swift
//  DelivX
//
//  Created by Rahul Sharma on 13/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

extension AddItemViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getCategoriesCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let categoryName = viewModel.getCategoryName(at: indexPath.item),
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddItemCategoryCollectionViewCell.self), for: indexPath) as? AddItemCategoryCollectionViewCell {
              print("cellForItemAt Selected indexpath = \(viewModel.getSelectedIndexForCollectionView())")
            
            cell.lineView.frame =  CGRect(x:  cell.lineView.frame.origin.x, y: cell.lineView.frame.origin.y, width: cell.frame.size.width, height: 2)
            let selectedIndex = viewModel.getSelectedIndexForCollectionView()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0 ) {
            if selectedIndex == indexPath.row {
                cell.lineView.backgroundColor = Colors.DarkBlueBlack
                print("selected row is \(indexPath.row)")
            }
            else {
                cell.lineView.backgroundColor = .clear
            }
            }
            cell.titleLabel.text = categoryName
            return cell
        }
        return UICollectionViewCell()
    }
}

extension AddItemViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectedIndexForCollectionView = indexPath.row
        if let cell = collectionView.cellForItem(at: indexPath) as? AddItemCategoryCollectionViewCell  {
            cell.lineView.backgroundColor = Colors.DarkBlueBlack
         }
        
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        collectionView.reloadData()

    }
   
}

extension AddItemViewController:UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        if let categoryName = viewModel.getCategoryName(at: indexPath.item) {
            
          //  let height =  categoryName.height(withConstrainedWidth: 30.0, font:UIFont (name: "Muli-Bold", size: 14.0) ??  UIFont.boldSystemFont(ofSize: 19.0) )
            let width =  categoryName.widthWithConstrainedWidth(197.0, font: UIFont (name: "Muli-Bold", size: 14.0) ??  UIFont.boldSystemFont(ofSize: 19.0))
            size = CGSize(width: width+30, height: 50)
        }
        
        return size
    }
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
