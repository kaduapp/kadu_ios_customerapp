//
//  PopoverTableViewModel.swift
//  DelivX
//
//  Created by Rahul Sharma on 19/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift

class PopoverTableViewModel {
    
    var storeCategoryId = ""
    var searchString = ""
    var categoryId = ""
    var searchedItems = [SearchItemModel]()
    
    var disposeBag = DisposeBag()
    let rx_ReloadTableview = PublishSubject<Bool>()
    
    // MARK:- UITableViewDataSource Methods
    func getSearchItemCount() -> Int {
        return searchedItems.count
    }
    
    func getSearchItem(at index: Int) -> SearchItemModel? {
        if getSearchItemCount() > index {
            return searchedItems[index]
        }
        return nil
    }
    
    func clearSearchItems() {
        searchedItems = [SearchItemModel]()
    }
    
    // MARK:- API Methods
    func e() {
        if searchString.count == 0 {
            clearSearchItems()
            rx_ReloadTableview.onNext(true)
        } else {
            let rxSearchItemAPICall = SearchItemAPI()
            rxSearchItemAPICall.rx_APIResponse
                .subscribe(onNext: { (response) in
                    print(response)
                    if let responseData = response.data["data"] as? [[String:Any]] {
                        do {
                            let data = try JSONSerialization.data(withJSONObject: responseData, options: [])
                            self.searchedItems = try JSONDecoder().decode([SearchItemModel].self, from: data)
                        } catch {
                            print("Error in Converting JSON to Data in rxCategoriesAndAttributesAPICall")
                        }
                        self.rx_ReloadTableview.onNext(true)
                    }
                }, onError: { (error) in
                    self.clearSearchItems()
                    print(error.localizedDescription)
                }, onCompleted: {
                    print("Completed rxCategoriesAndAttributesAPICall")
                }, onDisposed: {
                    print("Disposed rxCategoriesAndAttributesAPICall")
                }).disposed(by: disposeBag)
            rxSearchItemAPICall.searchItems(storeCategoryId, categoryId, searchString)
        }
    }
}
