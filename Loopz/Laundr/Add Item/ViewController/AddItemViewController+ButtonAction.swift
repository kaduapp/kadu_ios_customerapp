//
//  AddItemViewController+ButtonAction.swift
//  DelivX
//
//  Created by Rahul Sharma on 13/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

extension AddItemViewController {
    
    @IBAction func deleteNewItemCellAction(_ sender: UIButton) {
        print("Delete New Item - \(sender.tag)")
        let index = sender.tag - 1000
        viewModel.removeItem(at: index-1)
    }
    /*else  if let item = viewModel.getLastItem(),
     let _ = item.itemName,let pid = item.productId, pid != "0"
     {
     item.quantity = "10"
     viewModel.createItem()
     addItemTableView.reloadSections([0], with: .automatic)
     }*/
    @IBAction func addNewItemCellAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
        
        //,let item = viewModel.getItem(at: indexPath.row - 1), let itempid = item.productId, itempid == "0"
        if let item = viewModel.getLastItem(),
            let _ = item.itemName,
            let _ = item.quantity {
            if let cellcontent = sender.superview, let cell = cellcontent.superview as? AddNewItemTableViewCell ,let indexPath = addItemTableView.indexPath(for: cell), let prod = viewModel.getItem(at: indexPath.row - 2), prod.addedToCartOn != 1 {
                viewModel.addItemToCart(prod)
            }
           
            viewModel.createItem()
            addItemTableView.reloadData()
            //addItemTableView.reloadSections([0], with: .automatic)
            // addItemTableView.cellForRow(at:  viewModel.getItemsCount()-1)
            let indexPath2 = IndexPath(row: viewModel.getItemsCount(), section: 0)
            if let addcell = addItemTableView.cellForRow(at: indexPath2) as? AddItemTableViewCell
            {
                addcell.itemTextField.becomeFirstResponder()
            }
        }else
        {
            Helper.showAlert(message: "Please fill item name and item quantity to update Cart.", head: "", type: 1)
            
        }
    }
    
    @IBAction func checkOutAction(_ sender: UIButton) {
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
                viewModel.updateCustomerOrderInstructions()
    }
    
    @objc func doneBtnAction(_ sender: UIButton) {
        // handle done button action for textview
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            self.view.endEditing(true)
        }
        if sender.tag == 0
        {
           self.view.endEditing(true)
            return
        }
        
        if !Helper.isLoggedIn(isFromHist: false) {
            return
        }
        self.view.endEditing(true)
        let index = sender.tag - 2000
        let indexPath = IndexPath(row: index, section: 0)
        if let cell = addItemTableView.cellForRow(at: indexPath) as? AddItemTableViewCell,
            let quantity = cell.quantityTextField.text, quantity.count > 0,
            let intValue = Int(quantity), let itemname = cell.itemTextField.text, quantity != itemname  {
            if let item = viewModel.getItem(at: indexPath.row - 1), let productId = item.productId {
                if productId == "0" {
                    viewModel.updateItemQuantity(intValue)
                    if let item = viewModel.getLastItem() {
                        viewModel.addItemToCart(item)
                    }
                } else if viewModel.updateItemQuantity(productId, intValue) {
                    if let _ = item.addedToCartOn {
                        viewModel.updateItemInCart(item: item)
                    } else {
                        viewModel.addItemToCart(item)
                    }
                } else {
                    viewModel.updateItemQuantity(intValue)
                    if let item = viewModel.getLastItem() {
                        viewModel.addItemToCart(item)
                    }
                }
                cell.deleteBtn.isHidden = false
            }
        }else
        {
            Helper.showAlert(message: "Please fill item name and quantity to update Cart.", head: "", type: 1)
            
        }
    }
}
