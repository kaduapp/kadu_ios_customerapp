//
//  AddItemsViewContrller+TableView.swift
//  DelivX
//
//  Created by Rahul Sharma on 13/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


extension AddItemViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1 + viewModel.getItemsCount() + 1
        case 2:
            return viewModel.getOrderTypesCount()
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let rowType = viewModel.getCellTypeForRow(at: indexPath.row)
            switch rowType {
            case .header:
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddItemHeaderTableViewCell.self), for: indexPath) as? AddItemHeaderTableViewCell {
                    return cell
                }
                
            case .item:
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddItemTableViewCell.self), for: indexPath) as? AddItemTableViewCell {
                    if let item = viewModel.getItem(at: indexPath.item - 1) {
                        cell.updateCellWithItem(item)
                    }
                    cell.quantityTextField.tag = indexPath.row + 2000
                    if cell.quantityTextField.inputAccessoryView == nil {
                        print("Item Number = \(indexPath.row + 2000)")
                        self.addDoneBtn(cell.quantityTextField)
                      
                    }
                    cell.deleteBtn.tag = indexPath.row + 1000
                    cell.itemTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
                    cell.quantityTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
                    
                    return cell
                }
                
            case .addNewItem:
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddNewItemTableViewCell.self), for: indexPath) as? AddNewItemTableViewCell {
                    return cell
                }
            }
            
        case 1:
            switch indexPath.item {
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OrderInstructionTableViewCell.self), for: indexPath) as? OrderInstructionTableViewCell {
                    
                    return cell
                }
                break
                
            case 0...viewModel.getOrderTypesCount():
                if let orderType = viewModel.getOrderType(at: indexPath.row),
                    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelectOrderTypeTableViewCell.self), for: indexPath) as? SelectOrderTypeTableViewCell {
                    cell.updateSelectedOrderTypeCell(orderType)
                    return cell
                }
                break
                
            default:
                break
            }
        case 2:
            switch indexPath.item {
            case 0...viewModel.getOrderTypesCount():
                if let orderType = viewModel.getOrderType(at: indexPath.row),
                    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelectOrderTypeTableViewCell.self), for: indexPath) as? SelectOrderTypeTableViewCell {
                    cell.updateSelectedOrderTypeCell(orderType)
                    return cell
                }
                break
                
            default:
                break
            }
            default:
                switch indexPath.item {
                case 0:
                    if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SpecailInstructionTableViewCell.self), for: indexPath) as? SpecailInstructionTableViewCell {
                        cell.delegate = self
                       self.addDoneBtn(cell.descriptionTextView)
                       
                        return cell
                    }
                    break
                    
                case 1:
                    if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CheckoutButtonTableViewCell.self), for: indexPath) as? CheckoutButtonTableViewCell {
                        return cell
                    }
                    break
                    
                default:
                    break
            }
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
        let sectionHeaderView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 70))
        sectionHeaderView.backgroundColor = UIColor.white
         sectionHeaderView.addSubview(categoryCollectionView)
        return sectionHeaderView
        }
        else{
            return nil
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        if section == 0{
        return 70
        }
        return 0
    }

   
}

extension AddItemViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2 :
            if let orderType = viewModel.getOrderType(at: indexPath.row) {
                self.openOrderTypeViewController(orderType)
                viewModel.selectedIndexForCustomerInstructions = indexPath.row
            }
            break
        default:
            break
        }
    }
}
