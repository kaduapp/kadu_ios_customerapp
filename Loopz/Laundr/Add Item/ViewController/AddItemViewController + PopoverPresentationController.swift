//
//  AddItemViewController + PopoverPresentationController.swift
//  DelivX
//
//  Created by Rahul Sharma on 19/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift

extension AddItemViewController: UIPopoverPresentationControllerDelegate {
    
    func openPopUpTableViewController(_ searchText: String) {
        self.viewModel.searchItems()
    }
    
    func openPopUpTableViewController1(_ searchText: String) {
        let storyboard = UIStoryboard.init(name: "PopoverTableViewStoryboard", bundle: nil)
        if let selectedCategoryId = viewModel.getSelectedCategoryId(),
            let tableViewController = storyboard.instantiateViewController(withIdentifier: String(describing: PopoverTableViewController.self)) as? PopoverTableViewController {
            tableViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            let width = self.view.bounds.size.width - 100
            tableViewController.preferredWidth = width
            tableViewController.preferredContentSize = CGSize(width: width, height: 50)
            let popoverPresentationController = tableViewController.popoverPresentationController
            popoverPresentationController?.sourceView = self.activeTextField
            popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.activeTextField.frame.origin.y, width: self.activeTextField.frame.size.width/2 + 10, height: self.activeTextField.frame.size.height)
            popoverPresentationController?.delegate = self
            popoverPresentationController?.permittedArrowDirections = .up
            tableViewController.viewModel.searchedItems = viewModel.searchedItems
            self.viewModel.categoryId = selectedCategoryId
            self.viewModel.storeCategoryId = viewModel.getSuperStoreId()
            self.tableViewController = tableViewController
            addSubscriberForRxVariablesInPopoverTableView()
             self.present(tableViewController, animated: true, completion: nil)
        }
    }

    func addSubscriberForRxVariablesInPopoverTableView() {
        if let tableViewController = self.tableViewController {
            if tableViewController.rx_SelectedSearchItem.hasObservers {
                disposeBagForSearchItems = DisposeBag()
            }
            tableViewController.rx_SelectedSearchItem.subscribe(onNext: { (selectedProductItem) in
                self.disposeBagForSearchItems = DisposeBag()
                if let productName = selectedProductItem.productName,
                    let productId = selectedProductItem.id {
                    self.viewModel.updateItem(productName, productId)
                    if productId == "0" {
                        print("Wait for Update")
                    } else {
                        self.viewModel.rx_ReloadTableView.onNext(true)
                    }
                }
                self.view.endEditing(true)
            }, onError: { (error) in
                print("Got Error in rx_SelectedBusStop  = \(error.localizedDescription)")
            }, onCompleted: {
                print("Got rx_SelectedBusStop Completed")
            }, onDisposed: {
                print("Got rx_SelectedBusStop Disposed")
            }).disposed(by: disposeBagForSearchItems)
        }
    }

    func addSubscriberForDismissPopoverTableView() {
        if let tableViewController = self.tableViewController {
            tableViewController.rx_SelectedSearchItem.subscribe(onNext: { (selectedProductItem) in
                self.disposeBagForSearchItems = DisposeBag()
                if let productName = selectedProductItem.productName,
                    let productId = selectedProductItem.id {
                    self.viewModel.updateItem(productName, productId)
                    self.viewModel.rx_ReloadTableView.onNext(true)
                }
                self.view.endEditing(true)
            }, onError: { (error) in
                print("Got Error in rx_SelectedBusStop  = \(error.localizedDescription)")
            }, onCompleted: {
                print("Got rx_SelectedBusStop Completed")
            }, onDisposed: {
                print("Got rx_SelectedBusStop Disposed")
            }).disposed(by: disposeBagForSearchItems)
        }
    }

    func updateSearchString(_ searchString: String) {
         if let tableViewController = self.tableViewController ,let tbl = tableViewController.tableView{
            tableViewController.viewModel.searchString = searchString
            tableViewController.viewModel.searchedItems = viewModel.searchedItems
            tbl.reloadData()
        }
    }
    
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        print("popoverPresentationController prepare For Pop Over Presentation")
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        self.view.endEditing(true)
        print("popoverPresentationController popover Presentation Controller Did Dismiss Popover")
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

extension AddItemViewController:PopoverTableVCDelegate
{
    func presentPopOver() {
        
    }
    
    
}
