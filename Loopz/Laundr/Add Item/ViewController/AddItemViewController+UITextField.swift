//
//  AddItemViewController+UITextField.swift
//  DelivX
//
//  Created by Rahul Sharma on 18/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift

extension AddItemViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField


     }
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            if textField.placeholder == "Item Name",
                let _ = textField.text {
            viewModel.searchString = updatedText
            }
        }
        return true
    }
    @objc func textFieldDidChange(_ textField : UITextField) {
        activeTextField = textField
        if textField.placeholder == "Item Name",
            let _ = textField.text {
            viewModel.searchItems()
 //let index = sender.tag - 2000
        } else if textField.placeholder == "Quantity",
            let quantity = textField.text,
            quantity.count > 0,
            let intValue = Int(quantity) ,let item = viewModel.getItem(at: (textField.tag - 2000) - 1)  , let productId = item.productId , productId == "0"{
            viewModel.updateItemQuantity(intValue)
        }else if textField.placeholder == "Quantity",
            let quantity = textField.text,
            quantity.count > 0,
            let intValue = Int(quantity) ,let item = viewModel.getItem(at: (textField.tag - 2000) - 1)  , let _ = item.productId , item.addedToCartOn != 1 {
            viewModel.updateItemQuantity(intValue, at: (textField.tag - 2000) - 1)
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let count = textField.text?.count, count > 0{
        activeTextField = textField
        if textField.placeholder == "Item Name",
            let _ = textField.text {
            viewModel.searchItems()
        } else if textField.placeholder == "Quantity",
            let quantity = textField.text,
            quantity.count > 0,
            let intValue = Int(quantity) {
            viewModel.updateItemQuantity(intValue)
            if let item = viewModel.getLastItem() {
                viewModel.addItemToCart(item)
            }
        }
        // code to move next textfield on tapping "Next" Button
        let superview = textField.superview
        var foundtextfield = false
        superview?.subviews.forEach {
            print($0)
            if $0 is UITextField
            {
                $0.becomeFirstResponder()
                foundtextfield = true
            }
        }
        if foundtextfield
        {
            foundtextfield = false
            return false
        }
     }
        textField.resignFirstResponder()
        return true
    }
}
