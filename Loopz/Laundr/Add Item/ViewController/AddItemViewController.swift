//
//  AddItemViewController.swift
//  DelivX
//
//  Created by Rahul Sharma on 11/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
class AddItemViewController: UIViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkoutButtonContainer: UIView!
    @IBOutlet weak var dottedLineView: UIView!
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var addItemTableView : UITableView!
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var navWidth : NSLayoutConstraint!
    @IBOutlet weak var navView : UIView!
    var categoryCollectionView : UICollectionView!
    
    var navtitleLabel: UILabel!
    var activeField: UITextField?
    var activetextView: UITextView?
    weak var activeTextField: UITextField!
    weak var tableViewController: PopoverTableViewController?
    var orderInstructionsAlert:UIAlertController!
    let viewModel = AddItemViewModel()
    var disposeBag = DisposeBag()
    var disposeBagForSearchItems = DisposeBag()
    var keyboardHeight :CGFloat? = nil
    var checkoutVM = CheckoutVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = Utility.getSelectedSuperStores().categoryName
        self.navTitle.text = Utility.getSelectedSuperStores().categoryName
        registerRequiredCells()
        addSubscriberForRxVariables()
        Helper.showPI(string: StringConstants.FetchingCategories())
      viewModel.drawDottedLine(start: CGPoint(x: dottedLineView.bounds.minX, y: dottedLineView.bounds.minY), end: CGPoint(x: dottedLineView.bounds.maxX, y: dottedLineView.bounds.maxY), view:dottedLineView)
        // Do any additional setup after loading the view.
             self.viewModel.getCategoriesAndAttributes()
            self.viewModel.getCartItems()
          self.checkoutButton.isUserInteractionEnabled = false
        // Hiding Line Label
        lineLabel.isHidden = true
        Helper.transparentNavigation(controller: self.navigationController!)
         //  self.view.addSubview(titleView)
         navWidth.constant = UIScreen.main.bounds.width
        
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.itemSize = CGSize.init(width: 100, height: 100)
        categoryCollectionView =  UICollectionView.init(frame:     CGRect.init(x: 0, y: 10, width: self.view.frame.size.width, height: 50)
            , collectionViewLayout:flowLayout )
        let nibName = UINib(nibName: "AddItemCategoryCollectionViewCell", bundle:nil)
        categoryCollectionView.register(nibName, forCellWithReuseIdentifier: "AddItemCategoryCollectionViewCell")
        categoryCollectionView.backgroundColor = UIColor.white
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        viewModel.selectedIndexForCollectionView  = 0

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addObserver()
        addSubscriberForRxVariables()
        Helper.PullToClose(scrollView:addItemTableView)
        Helper.pullToDismiss?.delegate = self
 
        self.navigationController?.navigationBar.addSubview(navView)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
         self.removeObserver()
        navView.removeFromSuperview()

    }
 
    @IBAction func closeAction(_ sender: UIButton) {
    self.tabBarController?.dismiss(animated: true, completion: nil)
    }
    
    
    func registerRequiredCells() {
        addItemTableView.register(UINib(nibName: "SelectOrderTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectOrderTypeTableViewCell")
    }
    
    func addSubscriberForRxVariables() {
        addSuscriberForUpdateUI()
        addSuscriberForCollectionViewReload()
        addSuscriberForTableViewReload()
        addSuscriberForCheckoutAction()
        addSuscriberForpopOverView()
        addSuscriberForCheckoutButtonUpdateUI()
    }
     
    
    func addDoneBtn(_ sender: Any) {
        //Add done button to numeric pad keyboard
        
        let toolbarDone = UIToolbar()
        toolbarDone.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneBtnAction(_:)))
        if let textField = sender as? UITextField {
            done.tag = textField.tag
            toolbarDone.items = [flexSpace, done] // You can even add cancel button too
            textField.inputAccessoryView = toolbarDone
        } else if let textView = sender as? UITextView {
            
            toolbarDone.items = [flexSpace, done] // You can even add cancel button too
            textView.inputAccessoryView = toolbarDone
        }
    }

    func openOrderTypeViewController(_ orderType: StoreCategoryAttributeDetail) {
       
        self.performSegue(withIdentifier: "toOrderType", sender: orderType)

        //present modally over current context
    }
    
    func addSuscriberForUpdateUI() {
        if !viewModel.rx_UpdateUI.hasObservers {
            viewModel.rx_UpdateUI.subscribe(onNext: { (updateUI) in
                if updateUI {
                
                    self.categoryCollectionView.reloadData()
                    self.addItemTableView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3 ) {
                self.selectTablecellViewsatFirstIndexFor(categoryCollectionView:self.categoryCollectionView)
                        
                    self.addItemTableView.reloadData()
                    }
                }
            }, onError: { (error) in
                print("rx_UpdateUI error = \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_UpdateUI Completed")
            }, onDisposed: {
                print("rx_UpdateUI Disposed")
            }).disposed(by: disposeBag)
        }
    }
    func addSuscriberForCheckoutButtonUpdateUI() {
        if !viewModel.rx_UpdateCheckoutButtonUI.hasObservers {
            viewModel.rx_UpdateCheckoutButtonUI.subscribe(onNext: { (updateUI) in
                if updateUI {
                    if self.viewModel.isCartAvailable()
                    {
                        Helper.setButton(button: self.checkoutButton, view: self.checkoutButtonContainer, primaryColour:  Colors.AppBaseColor, seconderyColor:  Colors.SecondBaseColor, shadow: true)
                        self.checkoutButton.isUserInteractionEnabled = true
                        print("color update")
                        //color update
                     /*  self.checkoutButton.backgroundColor = UIColor(red: 98.0/255.0, green: 201.0/255.0, blue: 98.0/255.0, alpha: 1.0)
                         self.checkoutButton.titleLabel?.textColor = .white
                        self.checkoutButton.isUserInteractionEnabled = true*/
                    }
                }
            }, onError: { (error) in
                print("rx_UpdateUI error = \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_UpdateUI Completed")
            }, onDisposed: {
                print("rx_UpdateUI Disposed")
            }).disposed(by: disposeBag)
        }
    }
    func selectTablecellViewsatFirstIndexFor( categoryCollectionView: UICollectionView )
    {
//        let indexPath = IndexPath(row: 0, section: 0)
//        if viewModel.getCategoriesCount() > 0 {
//        self.collectionView(self.categoryCollectionView, didSelectItemAt: indexPath)
//        self.categoryCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally )
//        }
//        self.showCursorforItemTextFieldinTableViewCell()
    }
    func showCursorforItemTextFieldinTableViewCell()
    {
        let indexPath2 = IndexPath(row: 1, section: 0)
         if viewModel.getItemsCount() > 0 {
        if let addcell = addItemTableView.cellForRow(at: indexPath2) as? AddItemTableViewCell
        {
            addcell.itemTextField.becomeFirstResponder()
        }
        }
    }
      func addSuscriberForCheckoutAction()
      {
        if !viewModel.rx_UpdateButtonAction.hasObservers {
            viewModel.rx_UpdateButtonAction.subscribe(onNext: { (isReload) in
                if isReload {
                    
        self.performSegue(withIdentifier: UIConstants.SegueIds.seguetoConfirmOrder, sender: nil)
                }
            }, onError: { (error) in
                print("rx_ReloadTableView error = \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_ReloadTableView Completed")
            }, onDisposed: {
                print("rx_ReloadTableView Disposed")
            }).disposed(by: disposeBag)
        }
        
    }
    func addSuscriberForCollectionViewReload() {
        if !viewModel.rx_ReloadCollectionView.hasObservers {
            viewModel.rx_ReloadCollectionView.subscribe(onNext: { (updateUI) in
                if updateUI {
                    self.categoryCollectionView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3 ) {
                        self.addItemTableView.reloadData()
                    }
                }
            }, onError: { (error) in
                print("rx_ReloadCollectionView error = \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_ReloadCollectionView Completed")
            }, onDisposed: {
                print("rx_ReloadCollectionView Disposed")
            }).disposed(by: disposeBag)
        }
    }
    
    func addSuscriberForpopOverView() {
        if !viewModel.rx_presentPopOverView.hasObservers {
            viewModel.rx_presentPopOverView.subscribe(onNext: { (isReload) in
                if isReload {
                    
                    if self.viewModel.getSearchItemCount() > 0 {
                        if self.tableViewController == nil {
                        self.openPopUpTableViewController1("")
                        }else{
                            self.updateSearchString("")
                        }
                    }else{
                        if self.viewModel.searchString.isNotEmpty
                        {
                           self.viewModel.updateItem(self.viewModel.searchString, "0")
                        }
                        
                        if let popoverPresentationController = self.tableViewController
                        {
                            popoverPresentationController.dismiss(animated: true, completion: nil)
                        }
                    }
                 }
            }, onError: { (error) in
                print("rx_ReloadTableView error = \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_ReloadTableView Completed")
            }, onDisposed: {
                print("rx_ReloadTableView Disposed")
            }).disposed(by: disposeBag)
        }
    }
    func addSuscriberForTableViewReload() {
        if !viewModel.rx_ReloadTableView.hasObservers {
            viewModel.rx_ReloadTableView.subscribe(onNext: { (isReload) in
                if isReload {
                    self.addItemTableView.reloadData()
                    
                    if self.viewModel.isCartAvailable()
                    {
                        Helper.setButton(button: self.checkoutButton, view: self.checkoutButtonContainer, primaryColour:  Colors.AppBaseColor, seconderyColor:  Colors.SecondBaseColor, shadow: true)
                        self.checkoutButton.isUserInteractionEnabled = true
                        //color update
                    }else
                    {
                        Helper.setButton(button: self.checkoutButton, view: self.checkoutButtonContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor:  Colors.SecondBaseColor, shadow: true)
                        self.checkoutButton.isUserInteractionEnabled = false
                        self.showCursorforItemTextFieldinTableViewCell()
                        //gray update
                    }
                }
            }, onError: { (error) in
                print("rx_ReloadTableView error = \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_ReloadTableView Completed")
            }, onDisposed: {
                print("rx_ReloadTableView Disposed")
            }).disposed(by: disposeBag)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.seguetoConfirmOrder {
            if let viewController: ConfirmOrderVC = segue.destination as? ConfirmOrderVC ,let cartID = viewModel.cartItem?.cartId{
               viewController.cartID = cartID
                viewController.isOrderPlaced = {isSucces in
                   self.clearLaundryCart()
                }
               viewController.checkoutVM = self.checkoutVM
               viewController.storeCategoryId = viewModel.storeCategoryId
            }
        }else if segue.identifier == "toOrderType"
        {
            if let viewController: OrderTypeViewController = segue.destination as? OrderTypeViewController ,let orderType = sender as? StoreCategoryAttributeDetail{
                viewController.viewModel.setSelectedOrderType(orderType)
                viewController.delegate = self
            }
        }
    }
    
    
    /// Clear cart on placing order successfully and update UI
    func clearLaundryCart(){
      viewModel.items.removeAll()
      self.viewDidLoad()
    }
    
    
}
extension AddItemViewController:OrderTypeDelegate
{
    func getselectedAttribute(_ attribute:Attribute? ,_ orderType: StoreCategoryAttributeDetail?){
        if let attribute = orderType, let index = viewModel.selectedIndexForCustomerInstructions
        {
        viewModel.attributeDetails.remove(at:index )
        viewModel.attributeDetails.insert(attribute, at: index)
              self.addItemTableView.reloadData()
         }
     }
    
    
}
extension AddItemViewController:SpecailInstructionCellDelegate
{
    func textBeginEditing(_ textView: UITextView) {
         activetextView = textView
            viewModel.specialInstructionsString = textView.text

        if let cellcontent = textView.superview, let cell = cellcontent.superview as? SpecailInstructionTableViewCell,let indexPath = addItemTableView.indexPath(for: cell) , let keyboardHeight = keyboardHeight
        {
            let updatedframe = viewModel.returnUpdatedFrameWith(frame: addItemTableView.rectForRow(at: indexPath), cell: cell)
            let scrollPoint = CGPoint(x: 0, y: (updatedframe.origin.y - keyboardHeight)+20)
            addItemTableView.setContentOffset(scrollPoint, animated: true)
            
        }
    }
    
    
}

extension AddItemViewController:UIScrollViewDelegate
{
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        var offset = scrollView.contentOffset.y / 35
        if self.navigationController != nil {
            if scrollView.contentOffset.y > 1{
                Helper.addShadowToNavigationBar(controller: self.navigationController!)
            }
            else{
                Helper.removeShadowToNavigationBar(controller: self.navigationController!)
            }
        }
        if offset > 30{
            offset = 1
            setTheAlphaToNavigationViewsOnScroll(offset)
        }
        else{
            setTheAlphaToNavigationViewsOnScroll(offset)
        }
    }
    
    func setTheAlphaToNavigationViewsOnScroll(_ offset:CGFloat){
         self.navTitle.alpha = offset
        self.titleLabel.alpha = 1 - offset
    }
}


extension AddItemViewController {
    
    //Mark:- Keyboard ANimation
    override func keyboardWillShow(_ notification: NSNotification){
        let kbSize = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.size.height , right: 0.0)
       keyboardHeight = kbSize.size.height
        self.addItemTableView.contentInset = contentInsets;
        self.addItemTableView.scrollIndicatorInsets = contentInsets;
    }
    
    override func keyboardWillHide(_ notification: NSNotification){
     
        self.addItemTableView.contentInset = UIEdgeInsets.zero;
        self.addItemTableView.scrollIndicatorInsets = UIEdgeInsets.zero;
    }
    
    
}
