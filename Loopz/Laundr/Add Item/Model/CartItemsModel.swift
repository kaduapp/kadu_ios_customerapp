//
//  CartItemsModel.swift
//  DelivX
//
//  Created by Rahul Sharma on 22/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation

class CartItemsModel {
    var cartId: String?
    var customOrderInfo = [[String:Any]]()
    var specialInfo: String?
    var products = [Product]()
    
    init() {}
    
    init(cartDetails: [String:Any]) {
        if let cardId = cartDetails["cartId"] as? String {
            self.cartId = cardId
        }
        if let customOrderInfo = cartDetails["customOrderInfo"] as? [[String:Any]] {
            self.customOrderInfo = customOrderInfo
        }
        if let specialInfo = cartDetails["specialInfo"] as? String {
            self.specialInfo = specialInfo
        }
        if let products = cartDetails["products"] as? Array<[String:Any]> {
            self.products = products.map {
                Product(productDetail: $0)
            }
        }
    }
}

class Product {
    var productId: String?
    var itemName: String?
    var quantity: Int?
    var addedToCartOn: Int?
    
    init() {}
    
    init(productDetail: [String:Any]) {
        if let productId = productDetail["productId"] as? String {
            self.productId = productId
        }
        if let itemName = productDetail["itemName"] as? String {
            self.itemName = itemName
        }
        if let quantity = productDetail["quantity"] as? Int {
            self.quantity = quantity
        }
        if let productId = productDetail["productId"] as? String ,productId != "0"  {
            self.addedToCartOn = 1
        } 
    }
}
