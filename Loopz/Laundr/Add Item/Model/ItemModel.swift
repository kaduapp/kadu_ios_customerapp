//
//  ItemModel.swift
//  DelivX
//
//  Created by Rahul Sharma on 18/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation

class ItemModel {
    var id: String?
    var itemName: String?
    var quantity: Int?
    
    init() {}
}

struct ProductCategoryDetail: Codable {
    let id, categoryName: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case categoryName
    }
}

class StoreCategoryAttributeDetail: Codable {
    let id: String?
    let attributes: [Attribute]?
    let storeCategoryGroupName: String?
    var selectedAttributeId: String?
    var selectedAttributeName: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case attributes, storeCategoryGroupName, selectedAttributeId
    }
}

struct Attribute: Codable {
    let id, attributeName: String?
}
