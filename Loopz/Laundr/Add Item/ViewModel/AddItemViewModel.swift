//
//  AddItemViewModel.swift
//  DelivX
//
//  Created by Rahul Sharma on 14/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift


enum RowType: Int {
    case header = 0
    case item = 1
    case addNewItem = 2
}
class AddItemViewModel {
    
    var productCategorys = [ProductCategoryDetail]()
    var attributeDetails = [StoreCategoryAttributeDetail]()
    var searchedItems = [SearchItemModel]()
    var selectedIndexForCollectionView: Int?
    var selectedIndexForCustomerInstructions: Int?
    var specialInstructionsString:String = ""
    var searchString:String  = ""
    var items = [Product]()
    var storeCategoryId = ""
    var categoryId = ""
    var cartItem: CartItemsModel?
    
    var disposeBag = DisposeBag()
    let rx_UpdateUI = PublishSubject<Bool>()
    let rx_ReloadCollectionView = PublishSubject<Bool>()
    let rx_ReloadTableView = PublishSubject<Bool>()
    let rx_UpdateButtonAction = PublishSubject<Bool>()
    let rx_presentPopOverView = PublishSubject<Bool>()
    let rx_UpdateCheckoutButtonUI = PublishSubject<Bool>()

    
    // MARK:- UICollectionViewDataSource Methods
    func getCategoriesCount() -> Int {
        if selectedIndexForCollectionView == nil {
            selectedIndexForCollectionView = 0
        }
        return productCategorys.count
    }
    
    func getCategoryName(at index: Int) -> String? {
        if getCategoriesCount() > index,
            let categoryName = productCategorys[index].categoryName {
            return categoryName
        }
        return nil
    }
    
    func getSelectedCategoryId() -> String? {
        if let index = selectedIndexForCollectionView,
            getCategoriesCount() > index,
            let categoryId = productCategorys[index].id {
            return categoryId
        }
        return nil
    }
    
    func getSuperStoreId() -> String {
        return Utility.getSelectedSuperStores().Id
    }

    
    // MARK:- UICollectionViewDelegate Methods
    func setSelectedIndexForCollectionView(at index: Int) {
        
        selectedIndexForCollectionView = index
     }
    
    func getSelectedIndexForCollectionView() -> Int {
        if let index = selectedIndexForCollectionView {
             return index
        }
        return 0
    }

    //MARK:- UITableViewDataSource
    func createItem() {
         items.append(Product())
    }
    //Method update frames when keyboard is present
    func returnUpdatedFrameWith(frame:CGRect ,cell:UITableViewCell)-> CGRect
    {
        let updatedRect = CGRect(x: frame.origin.x, y: frame.origin.y+55+cell.frame.size.height, width: frame.size.width, height: frame.size.height) // 50 - height for collection View 5 - for spacing
        
        return updatedRect
    }
    
    func updateItem(_ name: String, _ id: String) {
        if let item = items.last,let _ = item.itemName
        {
                self.rx_UpdateCheckoutButtonUI.onNext(true)
        }
        if id == "0" {
            if let item = items.last {
                item.itemName = name
                item.productId = id
            }
        } else if let index = items.index(where: {$0.productId == id}),
            let item = getItem(at: index) {
            item.itemName = name
            item.productId = id
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4 ) {
                  Helper.showAlert(message: "Selected item exists in cart.\nPlease update Quantity.", head: "", type: 1)
            }
          

        } else {
            if let item = items.last {
                item.itemName = name
                item.productId = id
            }
        }
    }
    func updateItemQuantity(_ productId: String, _ quantity: Int) -> Bool {
        var isUpdated = false
        if let index = items.index(where: {$0.productId == productId}), let item = getItem(at: index) {
            if let oldQuantity = item.quantity, oldQuantity != quantity {
                isUpdated = true
            }
            item.quantity = quantity
            // isUpdated = false
        }
        return isUpdated
    }
   
    func updateItemQuantity(_ quantity: Int) {
        if let item = items.last {
            item.quantity = quantity
        }
    }
    
    func updateItemQuantity(_ quantity: Int ,at index:Int) {
        if let item = getItem(at : index) {
            item.quantity = quantity
        }
    }
    
    func getItemsCount() -> Int {
        if items.count == 0 {
            createItem()
        }
        return items.count
    }
    
    func isCartAvailable() -> Bool
    {
        if let item = self.items.last,let _ = item.itemName,let _ = item.quantity, let productId = item.productId , productId != "0" ,self.items.count >= 1
        {
            return true
        }else if  self.items.count >= 2 //
        {
             return true
        }else
        {
            return false
        }
    }
    func getItem(at index: Int) -> Product? {
        if getItemsCount() > index {
            return items[index]
        }
        return nil
    }
    
    func removeItem(at index: Int) {
        if getItemsCount() > index {
            if let item = getItem(at: index) {
                deleteItemFromCart(item: item)
            }
        }
    }
    
    func getLastItem() -> Product? {
        if getItemsCount() > 0 {
            return items.last
        }
        return nil
    }
    // Order Types methods
    func getOrderTypesCount() -> Int {
        return attributeDetails.count
    }
    
    func getOrderType(at index: Int) -> StoreCategoryAttributeDetail? {
        if getOrderTypesCount() > index {
            return attributeDetails[index]
        }
        return nil
    }

    func getCellTypeForRow(at index:Int) -> RowType {
        if index == 0 {
            return .header
        } else if (getItemsCount() == 0 && index == 1) || (getItemsCount() > 0 && index == getItemsCount() + 1) {
            return .addNewItem
        } else {
            return .item
        }
    }
    // MARK:- UITableViewDataSource Methods
    func getSearchItemCount() -> Int {
        return searchedItems.count
    }
    
    func getSearchItem(at index: Int) -> SearchItemModel? {
        if getSearchItemCount() > index {
            return searchedItems[index]
        }
        return nil
    }
    
    func clearSearchItems() {
        searchedItems = [SearchItemModel]()
    }
      // MARK:- View updating methods
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [3, 2] // 2 is the length of dash, 1 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat, view: UIView)  {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
    }
    // MARK:- API Methods
    func getCategoriesAndAttributes() {
        
        if getSuperStoreId().count > 0 {
            let rxCategoriesAndAttributesAPICall = StoreCategoriesAndAttributesAPI()
            rxCategoriesAndAttributesAPICall.rx_APIResponse
                .subscribe(onNext: { (response) in
                    print(response)
                    if let responseData = response.data["data"] as? [String:Any],
                        let productCategoryDetails = responseData["productCategoryDetails"] as? [[String:Any]] {
                        do {
                            let data = try JSONSerialization.data(withJSONObject: productCategoryDetails, options: [])
                            self.productCategorys = try JSONDecoder().decode([ProductCategoryDetail].self, from: data)
                            Helper.hidePI()
                        } catch {
                            print("Error in Converting JSON to Data in rxCategoriesAndAttributesAPICall")
                        }
                    }
                    if let responseData = response.data["data"] as? [String:Any],
                        let storeCategoryAttributeDetails = responseData["storeCategoryAttributeDetails"] as? [[String:Any]] {
                        do {
                            let data = try JSONSerialization.data(withJSONObject: storeCategoryAttributeDetails, options: [])
                             self.attributeDetails = try JSONDecoder().decode([StoreCategoryAttributeDetail].self, from: data)
                            Helper.hidePI()
                        } catch {
                            print("Error in Converting JSON to Data in rxCategoriesAndAttributesAPICall")
                        }
                    }
                    self.rx_UpdateUI.onNext(true)
                }, onError: { (error) in
                    print(error.localizedDescription)
                     Helper.hidePI()
                }, onCompleted: {
                    print("Completed rxCategoriesAndAttributesAPICall")
                    Helper.hidePI()
                }, onDisposed: {
                    print("Disposed rxCategoriesAndAttributesAPICall")
                }).disposed(by: disposeBag)
            rxCategoriesAndAttributesAPICall.getStoreCategoriesAndAttributes(getSuperStoreId())
        }
    }
    
    func addItemToCart(_ item: Product) {
        let rxAddCartAPI = CartAPI()
        if !rxAddCartAPI.rx_addResponse.hasObservers {
            rxAddCartAPI.rx_addResponse.subscribe(onNext: { (response) in
                print(response)
                if let data = response.data["data"] as? [String:Any],
                    let cartId = data["cartId"] as? String,
                    let productId = data["productId"] as? String {
                    item.productId = productId
                    if item.addedToCartOn != 1
                    {
                        item.addedToCartOn = 1
                    }
                    if let cart = self.cartItem {
                        cart.cartId = cartId
                    } else {
                        self.cartItem = CartItemsModel()
                        self.cartItem?.cartId = cartId
                     }
                }
                self.rx_ReloadTableView.onNext(true)
            }, onError: { (error) in
                print("rx_addResponse \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_addResponse completed")
            }, onDisposed: {
                print("rx_addResponse disposed")
            }).disposed(by: disposeBag)
        }
        if let itemName = item.itemName,
            let quantity = item.quantity {
            let params = ["productId": item.productId ?? "0",
                          "productName": itemName,
                          "quantity": quantity,
                          "zoneId": Utility.getAddress().ZoneId,
                          "storeType": 5] as [String : Any]
            rxAddCartAPI.addItemToCart(params)
        }
    }
    
    func searchItems() {
        if let selectedCategoryId = self.getSelectedCategoryId(){
            self.categoryId = selectedCategoryId
        }
        self.storeCategoryId = self.getSuperStoreId()
        if searchString.count == 0 {
            clearSearchItems()
             rx_presentPopOverView.onNext(true)
        } else {
            let rxSearchItemAPICall = SearchItemAPI()
            rxSearchItemAPICall.rx_APIResponse
                .subscribe(onNext: { (response) in
                    print(response)
                    if let responseData = response.data["data"] as? [[String:Any]] {
                        do {
                            let data = try JSONSerialization.data(withJSONObject: responseData, options: [])
                            self.searchedItems = try JSONDecoder().decode([SearchItemModel].self, from: data)
                        } catch {
                            print("Error in Converting JSON to Data in rxCategoriesAndAttributesAPICall")
                        }
                         self.rx_presentPopOverView.onNext(true)
                    }
                }, onError: { (error) in
                    self.clearSearchItems()
                    print(error.localizedDescription)
                }, onCompleted: {
                    print("Completed rxCategoriesAndAttributesAPICall")
                }, onDisposed: {
                    print("Disposed rxCategoriesAndAttributesAPICall")
                }).disposed(by: disposeBag)
            rxSearchItemAPICall.searchItems(storeCategoryId, categoryId, searchString)
        }
    }
    func updateItemInCart(item: Product) {
        let rxUpdateCartAPI = CartAPI()
        if !rxUpdateCartAPI.rx_updateResponse.hasObservers {
            rxUpdateCartAPI.rx_updateResponse.subscribe(onNext: { (response) in
                print(response)
                self.rx_ReloadTableView.onNext(true)
            }, onError: { (error) in
                print("rx_updateResponse \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_updateResponse completed")
            }, onDisposed: {
                print("rx_updateResponse disposed")
            }).disposed(by: disposeBag)
            if let productId = item.productId,
                let quantity = item.quantity,
                let cart = self.cartItem,
                let cartId = cart.cartId {
                let params = ["productId": productId,
                              "cartId": cartId,
                              "quantity": quantity] as [String : Any]
                rxUpdateCartAPI.updateItemToCart(params)
            }
        }
    }
    
    func deleteItemFromCart(item: Product) {
        let rxDeleteCartAPI = CartAPI()
        if !rxDeleteCartAPI.rx_deleteResponse.hasObservers {
            rxDeleteCartAPI.rx_deleteResponse.subscribe(onNext: { (response) in
                print(response)
                if response.httpStatusCode == 202 {
                    if let productId = item.productId,
                        let index = self.items.index(where: {$0.productId == productId}) {
                        self.items.remove(at: index)
                    }
                }
                self.rx_ReloadTableView.onNext(true)
            }, onError: { (error) in
                print("rx_deleteResponse \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_deleteResponse completed")
            }, onDisposed: {
                print("rx_deleteResponse disposed")
            }).disposed(by: disposeBag)
        }
        if let itemId = item.productId,
            let cart = self.cartItem,
            let cartId = cart.cartId {
            rxDeleteCartAPI.deleteItemToCart(cartId, itemId)
        }
    }
    
    func getCartItems() {
        let rxGettCartAPI = CartAPI()
        if !rxGettCartAPI.rx_getResponse.hasObservers {
            rxGettCartAPI.rx_getResponse.subscribe(onNext: { (response) in
                if let dict = response.data["data"] as? [String:Any] {
                    self.cartItem = CartItemsModel(cartDetails:dict)
                    if let cart = self.cartItem,
                        cart.products.count > 0 {
                        self.items = cart.products
                    }
                    self.rx_ReloadTableView.onNext(true)
                }
            }, onError: { (error) in
                print("rx_getResponse \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_getResponse completed")
            }, onDisposed: {
                print("rx_getResponse disposed")
            }).disposed(by: disposeBag)
        }
        rxGettCartAPI.getCart()
    }
    //posting params
    
    func updateCustomerOrderInstructions() {
        let rxUpdateCartAPI = CustomerOrderInstructionsAPI()
        if !rxUpdateCartAPI.rx_APIResponse.hasObservers {
            rxUpdateCartAPI.rx_APIResponse.subscribe(onNext: { (response) in
                print(response)
                self.rx_UpdateButtonAction.onNext(true)
            }, onError: { (error) in
                print("rx_updateResponse \(error.localizedDescription)")
            }, onCompleted: {
                print("rx_updateResponse completed")
            }, onDisposed: {
                print("rx_updateResponse disposed")
            }).disposed(by: disposeBag)
            
            if let cart = self.cartItem,
                let cartId = cart.cartId {
                let params = ["specialInfo": specialInstructionsString,
                              "cartId": cartId,
                              "customOrderInfo": self.attributeDetails.compactMap({$0.selectedAttributeName})] as [String : Any]
                rxUpdateCartAPI.CustomerOrderInstructionsupdate(params)
            }
        }
    }
    
   
    
}
