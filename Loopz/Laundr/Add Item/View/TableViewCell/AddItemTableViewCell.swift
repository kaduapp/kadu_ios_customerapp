//
//  AddItemTableViewCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 13/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AddItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemTextField: UITextField!
    @IBOutlet weak var quantityTextField: UITextField!
    @IBOutlet weak var deleteBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateCellWithItem(_ item: Product) {
        var checkDeleteBtn = 0
        if let itemName = item.itemName {
            itemTextField.text = itemName
            if let productId = item.productId, productId != "0" {
                itemTextField.isUserInteractionEnabled = false
            }
            checkDeleteBtn += 1
        } else {
            itemTextField.text = ""
            itemTextField.isUserInteractionEnabled = true
        }
        if let quantity = item.quantity {
            quantityTextField.text = "\(quantity)"
            quantityTextField.isUserInteractionEnabled = true
            checkDeleteBtn += 1
        } else {
            quantityTextField.text = ""
            quantityTextField.isUserInteractionEnabled = true
            if let count = itemTextField.text?.count, count > 0{
            DispatchQueue.main.async {
            self.quantityTextField.becomeFirstResponder()
            }
          }
        }
        
        if checkDeleteBtn == 2 && item.addedToCartOn == 1 {
            deleteBtn.isHidden = false
        } else {
            deleteBtn.isHidden = true
        }
    }
}
