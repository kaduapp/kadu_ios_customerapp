//
//  SpecailInstructionTableViewCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 13/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
protocol  SpecailInstructionCellDelegate {
    func textBeginEditing(_ textView: UITextView)
}

class SpecailInstructionTableViewCell: UITableViewCell,UITextViewDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    var placeholderLabel : UILabel!
    var delegate:SpecailInstructionCellDelegate? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        descriptionTextView.delegate = self as UITextViewDelegate
        descriptionTextView.autocorrectionType = .no
        placeholderLabel = UILabel()
        placeholderLabel.text = "Any Special Instructions..."
        placeholderLabel.font = descriptionTextView.font
        placeholderLabel.sizeToFit()
        descriptionTextView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (descriptionTextView.font?.pointSize)! / 2)
        placeholderLabel.textColor = Colors.lightBlueBlack
        placeholderLabel.isHidden = !descriptionTextView.text.isEmpty
        self.addLineOnTop()
    }

    func addLineOnTop()
    {
        let border = CALayer()
        let red = UIColor(red: 239.0/255.0, green: 240.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        border.backgroundColor = red.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: 9.0)
        self.layer.addSublayer(border)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
         delegate?.textBeginEditing(textView)
        return true
    }
    func textViewDidChangeSelection(_ textView: UITextView) {
         delegate?.textBeginEditing(textView)
    }
    func  textViewDidBeginEditing(_ textView: UITextView) {
          delegate?.textBeginEditing(textView)
    }
    func textViewDidChange(_ textView: UITextView) {
         placeholderLabel.isHidden = !textView.text.isEmpty
       //  delegate?.textBeginEditing(textView)
    }
}
