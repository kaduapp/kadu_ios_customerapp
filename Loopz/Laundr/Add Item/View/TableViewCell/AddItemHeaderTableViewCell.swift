//
//  AddItemHeaderTableViewCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 13/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AddItemHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var itemLabel: AlignmentOfLabel!
    @IBOutlet weak var quantityLabel: AlignmentOfLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
