//
//  SelectOrderTypeTableViewCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 14/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SelectOrderTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var orderTypeLabel: AlignmentOfLabel!
    @IBOutlet weak var choosedOrderTypeLabel: AlignmentOfLabel!
    @IBOutlet weak var arrowImageView: UIImageView!    
    
    @IBOutlet weak var orderTypeLabelLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var arrowImageViewTrailingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    //3,99,210
    func updateSelectedOrderTypeCell(_ orderType: StoreCategoryAttributeDetail) {
        if let name = orderType.storeCategoryGroupName {
            self.orderTypeLabel.text = name
            self.choosedOrderTypeLabel.textColor = UIColor(red: 3.0/255.0, green: 99.0/255.0, blue: 210.0/255.0, alpha: 1.0)
         }
        if let selectedAttributeId = orderType.selectedAttributeId,
            let index = orderType.attributes?.index(where: {$0.id == selectedAttributeId}) {
            if let attribute = orderType.attributes?[index],
                let attributeName = attribute.attributeName {
                self.choosedOrderTypeLabel.text = attributeName
            }
        } else {
            self.choosedOrderTypeLabel.text = "Select"
            self.choosedOrderTypeLabel.textColor = .gray
        }
       self.selectionStyle = .none
    }
}
