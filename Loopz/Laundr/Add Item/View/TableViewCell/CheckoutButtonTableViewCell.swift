//
//  CheckoutButtonTableViewCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 18/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CheckoutButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var checkoutButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
