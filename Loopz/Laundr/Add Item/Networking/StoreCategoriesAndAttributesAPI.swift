//
//  StoreCategoriesAndAttributesAPI.swift
//  DelivX
//
//  Created by Rahul Sharma on 18/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire


class StoreCategoriesAndAttributesAPI {
    
    let rx_APIResponse = PublishSubject<APIResponseModel>()
    let disposeBag = DisposeBag()
    
    func getStoreCategoriesAndAttributes(_ superStoreId: String) {
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
        }
        
        let strUrl = APIEndTails.BaseUrl + "category/" + superStoreId
        RxAlamofire
        .requestJSON(.get,
                     strUrl,
                     parameters: nil,
                     encoding: JSONEncoding.default,
                     headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: r.statusCode)!
                if errNum == .success
                {
                if  let dict  = json as? [String:Any] {
                    let statuscode:Int = r.statusCode
                    let response = Utility.checkResponse(statusCode: statuscode, responseDict: dict)
                    self.rx_APIResponse.onNext(response)
 
                }
                }else
                {
                     if  let dict  = json as? [String:Any] {
                    APICalls.basicParsing(data: dict, status: r.statusCode)
                    }
                }
            }, onError: { (error) in
                self.rx_APIResponse.onError(error)
            }, onCompleted: {
                print("StoreCategoriesAndAttributes Completed")
            }, onDisposed: {
                print("StoreCategoriesAndAttributes Disposed")
            }).disposed(by: disposeBag)
    }
}
