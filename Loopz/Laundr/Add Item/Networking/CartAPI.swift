//
//  CartAPI.swift
//  DelivX
//
//  Created by Rahul Sharma on 19/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class CartAPI {
    let rx_addResponse = PublishSubject<APIResponseModel>()
    let rx_updateResponse = PublishSubject<APIResponseModel>()
    let rx_deleteResponse = PublishSubject<APIResponseModel>()
    let rx_getResponse = PublishSubject<APIResponseModel>()

    let disposeBag = DisposeBag()
    
    func addItemToCart(_ params: [String:Any]) {
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
        }
        
        let strUrl = APIEndTails.BaseUrl + "cart/laundry"
        RxAlamofire
        .requestJSON(.post,
                     strUrl,
                     parameters: params,
                     encoding: JSONEncoding.default,
                     headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: r.statusCode)!
                if errNum == .created
                {
                if let dict = json as? [String:Any] {
                    let statuscode:Int = r.statusCode
                    let response = Utility.checkResponse(statusCode: statuscode, responseDict: dict)
                    self.rx_addResponse.onNext(response)
                  //  APICalls.basicParsing(data: dict, status:  r.statusCode)

                }else
                {
                    APICalls.basicParsing(data:json as! [String : Any],status:r.statusCode)
                }
                }
            }, onError: { (error) in
                print(error.localizedDescription)
            }, onCompleted: {
                print("AddItemToCart completed")
            }, onDisposed: {
                print("AddItemToCart Disposed")
            }).disposed(by: disposeBag)
    }
    
    func updateItemToCart(_ params: [String:Any]) {

        if !AppConstants.Reachable {
            Helper.showNoInternet()
        }
        
        let strUrl = APIEndTails.BaseUrl + "cart/laundry"
        RxAlamofire
        .requestJSON(.patch,
                     strUrl,
                     parameters: params,
                     encoding: JSONEncoding.default,
                     headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: r.statusCode)!
                if errNum == .updated
                {
                if let dict = json as? [String:Any] {
                    let statuscode:Int = r.statusCode
                    let response = Utility.checkResponse(statusCode: statuscode, responseDict: dict)
                    self.rx_updateResponse.onNext(response)
                 //   APICalls.basicParsing(data: dict, status:  r.statusCode)

                }
                }else
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0 ) {
                        APICalls.basicParsing(data:json as! [String : Any],status:r.statusCode)

                    }

                }
            }, onError: { (error) in
                print(error.localizedDescription)
            }, onCompleted: {
                print("updateItemToCart completed")
            }, onDisposed: {
                print("updateItemToCart Disposed")
            }).disposed(by: disposeBag)
    }
    
    func deleteItemToCart(_ cartId: String, _ productId: String) {
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
        }
        
        let strUrl = APIEndTails.BaseUrl + "cart/laundry" + "/" + cartId + "/" + productId
        RxAlamofire
            .requestJSON(.delete,
                         strUrl,
                         parameters: nil,
                         encoding: JSONEncoding.default,
                         headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: r.statusCode)!
                if errNum == .deleted
                {
                if let dict = json as? [String:Any] {
                    let statuscode:Int = r.statusCode
                    let response = Utility.checkResponse(statusCode: statuscode, responseDict: dict)
                    self.rx_deleteResponse.onNext(response)
                   // APICalls.basicParsing(data: dict, status:  r.statusCode)

                }
                }else
                {
                    APICalls.basicParsing(data:json as! [String : Any],status:r.statusCode)
                }
            }, onError: { (error) in
                print(error.localizedDescription)
            }, onCompleted: {
                print("deleteItemToCart completed")
            }, onDisposed: {
                print("deleteItemToCart Disposed")
            }).disposed(by: disposeBag)
    }

    func getCart() {
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
        }
        
        let strUrl = APIEndTails.BaseUrl + "cart/laundry"
        RxAlamofire
            .requestJSON(.get,
                         strUrl,
                         parameters: nil,
                         encoding: JSONEncoding.default,
                         headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: r.statusCode)!
                if errNum == .success
                {
                if let dict = json as? [String:Any] {
                    let statuscode:Int = r.statusCode
                    let response = Utility.checkResponse(statusCode: statuscode, responseDict: dict)
                    self.rx_getResponse.onNext(response)
 
                }
                }else
                {
                         if  let dict  = json as? [String:Any] {
                    APICalls.basicParsing(data: dict, status: r.statusCode)
                    }
                }
            }, onError: { (error) in
                print(error.localizedDescription)
            }, onCompleted: {
                print("deleteItemToCart completed")
            }, onDisposed: {
                print("deleteItemToCart Disposed")
            }).disposed(by: disposeBag)
    }
}
