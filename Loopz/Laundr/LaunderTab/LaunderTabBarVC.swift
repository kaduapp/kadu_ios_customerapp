
import UIKit
import RxCocoa
import RxSwift

class LaunderTabBarVC: UITabBarController {
    
    let disposeBag = DisposeBag()
    var tabBarIndex = 0
    override func viewDidLoad(){
        super.viewDidLoad()
        self.delegate = self
        didCartUpdate()
        
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
 
    }
}
// MARK: - UITabBarControllerDelegate
extension LaunderTabBarVC: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        tabBarIndex = tabBarController.selectedIndex
        AppConstants.SelectedIndex = AppConstants.Tabs(rawValue: tabBarIndex)!
        switch tabBarIndex {
        case 1,2:
            if !Helper.isLoggedIn(isFromHist: true){
                self.tabBarController?.selectedIndex = 0
            }
        default:
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AppDelegate.tabChange != nil {
            let temp = AppDelegate.tabChange
            AppDelegate.tabChange = nil
            Helper.changetTabTo(index: temp!)
        }
    }
    
}

extension LaunderTabBarVC {
    
    /// Observe CartDBManager
    func didCartUpdate(){
        CartDBManager.cartDBManager_response.subscribe(onNext: { [weak self]success in
            self?.updateTabBadge()
            if success == false {
                self?.tabBarController?.selectedIndex = 0
            }
        }).disposed(by: disposeBag)
    }
    
    /// update the TabBadge which holds the cartCount
    func updateTabBadge() {
        if (AppConstants.CartCount > 0) {
            let tabItem = self.tabBar.items![2]
            tabItem.badgeValue = String(AppConstants.CartCount)
        }else{
            let tabItem = self.tabBar.items![2]
            tabItem.badgeValue = nil
        }
    }
}

