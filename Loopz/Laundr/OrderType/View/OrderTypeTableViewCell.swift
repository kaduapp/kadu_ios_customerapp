//
//  OrderTypeTableViewCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 21/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class OrderTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkMarkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func updateOrderTypeCell(_ attribute: Attribute ,_ selected: Bool) {
        if let attributeName = attribute.attributeName {
            titleLabel.text = attributeName
            if (selected)
            {
            self.checkMarkImageView.image = #imageLiteral(resourceName: "LargeCheck")
            }else
            {
            self.checkMarkImageView.image = nil
            }
        }
    }
}
