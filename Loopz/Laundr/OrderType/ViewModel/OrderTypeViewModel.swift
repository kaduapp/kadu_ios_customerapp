//
//  OrderTypeViewModel.swift
//  DelivX
//
//  Created by Rahul Sharma on 21/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation

class OrderTypeViewModel {
    
    var selectedOrderType: StoreCategoryAttributeDetail?
    var selectedAttributeId : String?
    var attributes = [Attribute]()
    var selectedIndex :IndexPath? = nil
    
    func setSelectedOrderType(_ orderType: StoreCategoryAttributeDetail) {
        self.selectedOrderType = orderType
        if let attributes = orderType.attributes {
            self.attributes = attributes
        }
        if let selectedAttributeId = orderType.selectedAttributeId {
            self.selectedAttributeId = selectedAttributeId
        }
    }
    
    func getOrderTypeName() -> String {
        if let ordreType = selectedOrderType,
            let orderTypeName = ordreType.storeCategoryGroupName {
            return orderTypeName
        }
        return ""
    }
    func getAttributesCount() -> Int {
        return attributes.count
    }
    
    func getAttribute(at index: Int) -> Attribute? {
        if getAttributesCount() > index {
            return attributes[index]
        }
        return nil
    }
}
