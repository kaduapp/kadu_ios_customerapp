//
//  OrderTypeViewController.swift
//  DelivX
//
//  Created by Rahul Sharma on 21/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
protocol OrderTypeDelegate {
    func getselectedAttribute(_ attribute:Attribute? ,_ orderType: StoreCategoryAttributeDetail?)
}

class OrderTypeViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var delegate:OrderTypeDelegate? = nil
    let viewModel = OrderTypeViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
      //  titleLabel.text = viewModel.getOrderTypeName()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self as UIGestureRecognizerDelegate // This is not required
         self.view.addGestureRecognizer(tap)
    }
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
         self.cancelButtonTapped(sender)
    }
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
         viewModel.selectedAttributeId = nil
         self.dismiss(animated: true, completion: nil)
    }
    @IBAction func applyButtonTapped(_ sender: Any)
    {
        if let selectedrow =  viewModel.selectedIndex?.row,let attribute = viewModel.getAttribute(at: selectedrow)
        {
        viewModel.selectedOrderType?.selectedAttributeName = attribute.attributeName
        viewModel.selectedOrderType?.selectedAttributeId = attribute.id
        }
       
        if let selectedrow =  viewModel.selectedIndex?.row, let attribute = viewModel.getAttribute(at: selectedrow), let selectedOrderType = viewModel.selectedOrderType  {
            
             delegate?.getselectedAttribute(attribute, selectedOrderType)
             self.dismiss(animated: true, completion: nil)
        }else
        {
            if let _ = viewModel.selectedOrderType, let _ = viewModel.selectedOrderType?.selectedAttributeId
            {
                
            }else
            {
            Helper.showAlert(message: "Please select an option to Apply", head: "", type: 1)
            }
        }
       
    }
}

extension OrderTypeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getAttributesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let attribute = viewModel.getAttribute(at: indexPath.row),
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OrderTypeTableViewCell.self) , for: indexPath) as? OrderTypeTableViewCell {
           
            if let selectedAttributed = viewModel.selectedAttributeId,
                let attributtedId = attribute.id,
                selectedAttributed == attributtedId {
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                 cell.updateOrderTypeCell(attribute,true)
            } else {
                 cell.updateOrderTypeCell(attribute,false)
            }
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}

extension OrderTypeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectedIndex = indexPath
        if let cell = tableView.cellForRow(at: indexPath) as? OrderTypeTableViewCell {
            cell.checkMarkImageView.image = #imageLiteral(resourceName: "LargeCheck")
             self.applyButtonTapped(cell.checkMarkImageView)
        }
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        
        /*if let attribute = viewModel.getAttribute(at: indexPath.row),
            let attributeId = attribute.id , let attributename = attribute.attributeName{
            viewModel.selectedAttributeId = attributeId
            viewModel.selectedOrderType?.selectedAttributeId = attributeId
            viewModel.selectedOrderType?.selectedAttributeName = attributename
            
         
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            
             self.navigationController?.popViewController(animated: true)
        }*/
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as? OrderTypeTableViewCell  {
         //   cell.accessoryType = .none
            cell.checkMarkImageView.image = nil
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView:HeaderView? = nil
        var stringHead = ""
        if let GroupName = viewModel.selectedOrderType?.storeCategoryGroupName {
            stringHead = GroupName
        }
        headerView = Helper.showHeader(title: stringHead,showViewAll: false) as? HeaderView
        if let header = headerView?.headerTitle
        {
            header.textAlignment = .center
        }
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}


extension OrderTypeViewController:UIGestureRecognizerDelegate
{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                                    shouldReceive touch: UITouch) -> Bool
    {
        if let touchedView = touch.view, touchedView.isDescendant(of: tableview)  {
            return false
        }
        return true
    }
}
