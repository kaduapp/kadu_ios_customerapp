//
//  SearchItemAPI.swift
//  DelivX
//
//  Created by Rahul Sharma on 19/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire


class SearchItemAPI {
    
    let rx_APIResponse = PublishSubject<APIResponseModel>()
    let disposeBag = DisposeBag()
    
    func searchItems(_ storeCategoryId: String, _ categoryId: String, _ searchString: String) {
        let strUrl = APIEndTails.BaseUrl + "search/products/" + storeCategoryId + "/" + categoryId + "/" + searchString
        RxAlamofire
            .requestJSON(.get,
                         strUrl,
                         parameters: nil,
                         encoding: JSONEncoding.default,
                         headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
                if  let dict  = json as? [String:Any] {
                    let statuscode:Int = r.statusCode
                    let response = Utility.checkResponse(statusCode: statuscode, responseDict: dict)
                    self.rx_APIResponse.onNext(response)
                }
            }, onError: { (error) in
                self.rx_APIResponse.onError(error)
            }, onCompleted: {
                print("StoreCategoriesAndAttributes Completed")
            }, onDisposed: {
                print("StoreCategoriesAndAttributes Disposed")
            }).disposed(by: disposeBag)
    }
}
