//
//  PopoverTableViewController.swift
//  DelivX
//
//  Created by Rahul Sharma on 19/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

protocol PopoverTableVCDelegate  {
     func presentPopOver()
}

class PopoverTableViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var preferredWidth: CGFloat = 0.0
    
    var viewModel = PopoverTableViewModel()
    
    let rx_SelectedSearchItem = PublishSubject<SearchItemModel>()
    let rx_dismiss = PublishSubject<Bool>()
    var disposeBag = DisposeBag()
    var delegate:PopoverTableVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        addObserverForRxVarible()
      //  viewModel.searchItems()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        disposeBag = DisposeBag()
        viewModel.disposeBag = DisposeBag()
    }
    
    func addObserverForRxVarible() {
        self.addObserverForReloadTableView()
    }
    
    func addObserverForReloadTableView() {
        print("reloadTableView")
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
            self.preferredContentSize = CGSize(width: self.preferredWidth, height: self.tableView.contentSize.height)
            
        })
        if !viewModel.rx_ReloadTableview.hasObservers {
            viewModel.rx_ReloadTableview
                .subscribe(onNext: { isReload in
                    if isReload {
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                            self.preferredContentSize = CGSize(width: self.preferredWidth, height: self.tableView.contentSize.height)
                            if self.viewModel.getSearchItemCount() == 0 {
                                let searchItem = SearchItemModel(id: "0", productName: self.viewModel.searchString)
                                self.rx_SelectedSearchItem.onNext(searchItem)
                                self.dismiss(animated: true, completion: nil)
                            }
                        })
                    }
                }, onError: { error in
                    print("Got error on rx_ReloadTableview = \(error.localizedDescription)")
                }, onCompleted: {
                    print("rx_ReloadTableview completed")
                }, onDisposed: {
                    print("rx_ReloadTableview disposed")
                }).disposed(by: disposeBag)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tableView.layer.removeAllAnimations()
        preferredContentSize = CGSize(width: preferredWidth, height: tableView.contentSize.height)
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
    }
}


extension PopoverTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getSearchItemCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let searchItem = viewModel.getSearchItem(at: indexPath.row),
            let productName = searchItem.productName,
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SearchItemTableViewCell.self), for: indexPath) as? SearchItemTableViewCell {
            cell.titleLabel.text = "\(productName)"
            return cell
        }
        return UITableViewCell()
    }
}

extension PopoverTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedSearchItem = viewModel.getSearchItem(at: indexPath.row) {
            self.rx_SelectedSearchItem.onNext(selectedSearchItem)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
