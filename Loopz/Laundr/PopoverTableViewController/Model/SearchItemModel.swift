//
//  SearchItemModel.swift
//  DelivX
//
//  Created by Rahul Sharma on 19/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation


struct SearchItemModel: Codable {
    let id, productName: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case productName
    }
}
