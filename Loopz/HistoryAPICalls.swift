//
//  HistoryAPICalls.swift
//  UFly
//
//  Created by 3Embed on 11/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class HistoryAPICalls: NSObject {
    static let disposeBag = DisposeBag()
    static let reviewDone = PublishSubject<Bool>()
    //Get Store Data(suCategory Items)
    class func getOrderHistory(data:Int ,storeType:Int) ->Observable<[Order]> {
      //  Utility.getSelectedSuperStores().type.rawValue
        Helper.showPI(string: "")
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        var url = ""
        if storeType != -1
        {
          url = APIEndTails.Order + "/\(0)/\(data)" + "/\(storeType)"
        }else
        {
        url = APIEndTails.Order + "/\(0)/\(data)" + "/0"
        }
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var Orders = [Order]()
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [String:Any]{
                        if let titleTempArray = titleTemp[APIResponceParams.Orders] as? [Any]{
                            for data in titleTempArray {
                                let order = Order.init(data: data as! [String:Any])
                                Orders.append(order)
                            }
                        }
                        if let titleTemp = titleTemp[APIResponceParams.Profile] as? [String:Any]{
                            let user = Utility.getProfileData()
                            user.updateData(data: titleTemp, tool: true)
                        }
                    }
                    observer.onNext(Orders)
                    observer.onCompleted()
                }else if errNum == .NotFound {
                    observer.onNext([])
                    observer.onCompleted()
                }
            
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: HistoryAPICalls.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
   //get selected OrderDetail
    class func getOrderDetail(data:Order) -> Observable<Order>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Loading())
        let header = Utility.getHeader()
        let url = APIEndTails.OrderDetail + "\(data.BookingId)"
        let order:Order = data
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let data = bodyIn[APIResponceParams.Data] as! [String:Any]
                    order.updateOrderDetail(data: data)
                    observer.onNext(order)
                    observer.onCompleted()
                }
                else{
                     APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
        
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: HistoryAPICalls.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    
    }
    
    //CancelOrder
    class func cancelOrder(id:Int,reason:String) ->Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let location = Utility.currentLatAndLong()
        let header = Utility.getHeader()
        let params : [String : Any] =    [
            APIRequestParams.Latitude                :location.Latitude,
            APIRequestParams.Longitude               :location.Longitude,
            APIRequestParams.IPAddress               :Utility.getIPAddress(),
            APIRequestParams.OrderID                 :"\(id)",
            APIRequestParams.Reason                  :reason
        ]
        Helper.showPI(string: StringConstants.Canceling())
        return Observable.create { observer in
              RxAlamofire.requestJSON(.put, APIEndTails.Order, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
              let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .created {
                    observer.onNext(true)
                    observer.onCompleted()
                }
//                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                

              }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
              }).disposed(by: HistoryAPICalls.disposeBag)
            return Disposables.create()
        }.share(replay: 1)
    }
    
    
    //CancelOrder
    class func reOrder(order:Order) ->Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        let header = Utility.getHeader()
        let params : [String : Any] =    [
            APIRequestParams.OrderID                 :"\(order.BookingId)"
        ]
        Helper.showPI(string: StringConstants.Canceling())
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.Order, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .created {
                    CartAPICalls.getCartData().subscribe(onNext: {success in
                        Helper.hidePI()
                        observer.onNext(true)
                        observer.onCompleted()
                    }, onError: {error in
                        print(error)
                        Helper.hidePI()
                    }).disposed(by: HistoryAPICalls.disposeBag)
                }else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
//                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: HistoryAPICalls.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }

    
    //get selected OrderDetail
    class func getCancelReason() -> Observable<[PopupModel]>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Loading())
        let header = Utility.getHeader()
        let url = APIEndTails.CancelReasons
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, url, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var arrayOfReasons:[PopupModel] = []
                    if let data = bodyIn[APIResponceParams.Data] as? [Any] {
                        for item in data {
                            arrayOfReasons.append(PopupModel.init(data: item as! [String:Any]))
                        }
                    }
                    observer.onNext(arrayOfReasons)
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
                
            }, onError: { (Error) in
                //                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: HistoryAPICalls.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
        
    }
    
    
    //get selected OrderDetail
    class func getRating(_ storeType:Int) -> Observable<[Rating]>{
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: StringConstants.Loading())
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, "\(APIEndTails.Rating)/\(storeType)", parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    let data = bodyIn[APIResponceParams.Data] as! [Any]
                    var array:[Rating] = []
                    for item in data {
                        array.append(Rating.init(data: item as! [String:Any]))
                    }
                    observer.onNext(array)
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
                
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: HistoryAPICalls.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //Add Cart Data
    class func updateRating(rating:[Rating],order:Order,tip:Float) ->Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        var arrayRating:[Any] = []
        for item in rating {
            var options:[(String,Bool)] = []
            if item.Options.count >= item.Value {
                options = item.Options[item.Value - 1].Options
            }
            var message = ""
            for option in options {
                if option.1 {
                    message = message + option.0 + ", "
                }
            }
            let data = [
                APIRequestParams.Rating : item.Value,
                APIRequestParams.Message : message,
                APIRequestParams.Id : item.Id,
                APIRequestParams.FirstName : item.Name,
                APIResponceParams.Associated : item.Associated
                ] as [String : Any]
            arrayRating.append(data)
        }
        
        Helper.showPI(string: StringConstants.PlaceOrder())
        let header = Utility.getHeader()
        let params = [
            APIRequestParams.Review       : arrayRating,
            APIRequestParams.OrderID      : order.BookingId,
            APIRequestParams.Tip          : "\(tip)"
            ] as [String : Any]
        
        
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.Rating, parameters: params, encoding: JSONEncoding.default, headers: header).debug().subscribe(onNext: { (head, body) in
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
//                let bodyIn = body as! [String:Any]
                if errNum == .success {
                    observer.onNext(false)
                    observer.onCompleted()
//                    Helper.showAlert(message: bodyIn[APIResponceParams.Message] as! String, head: StringConstants.Success, type: 2)
                    reviewDone.onNext(true)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: Error.localizedDescription, head: StringConstants.Error(), type: 1)
                  Helper.hidePI()
            }).disposed(by: HistoryAPICalls.disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
}
