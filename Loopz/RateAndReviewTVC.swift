//
//  RateAndReviewTVC.swift
//  UFly
//
//  Created by 3 Embed on 18/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class RateAndReviewTVC: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var reviewDescription: UILabel!
    @IBOutlet weak var days: UILabel!
    @IBOutlet weak var rateAndReviewLabel: UILabel!
    @IBOutlet weak var titleHeadHight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    /// initial view setup
    func initialSetup(){
        Fonts.setPrimaryMedium(name)
        Fonts.setPrimaryRegular(reviewDescription)
        Fonts.setPrimaryRegular(days)
        Fonts.setPrimaryMedium(rateAndReviewLabel)
        
        rateAndReviewLabel.text = StringConstants.RateAndReview()
        name.textColor =  Colors.PrimaryText
        reviewDescription.textColor = Colors.SeconderyText
        days.textColor = Colors.SecoundPrimaryText
        
        }
 
}
