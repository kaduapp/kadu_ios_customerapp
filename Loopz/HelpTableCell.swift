//
//  TableViewCell.swift
//  DelivX
//
//  Created by 3 Embed on 11/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HelpTableCell: UITableViewCell {
    
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var storeAddress: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var items: UILabel!
    
    @IBOutlet weak var rightArrow: UIButton!
    @IBOutlet weak var dateTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    func initialSetup(){
        Fonts.setPrimaryMedium(storeName)
        Fonts.setPrimaryRegular(storeAddress)
        Fonts.setPrimaryMedium(amount)
        Fonts.setPrimaryRegular(items)
        Fonts.setPrimaryRegular(dateTime)
        
        storeName.textColor =  Colors.PrimaryText
        storeAddress.textColor =  Colors.SeconderyText
        amount.textColor =  Colors.SeconderyText
        items.textColor =  Colors.SeconderyText
        dateTime.textColor =  Colors.FieldHeader
     
       separator.backgroundColor =  Colors.SeparatorLight
        rightArrow.tintColor =  Colors.SeconderyText
    }
    
    
    func updateCell(){
        storeName.text = "Infinite Wellness Center"
        storeAddress.text  = "900 N College Ave Fort Collins, CO"
        amount.text = "$30.00"
        items.text = "Jetty Extracts Purple  Tangie PURE Cartridge x 1,Pom Blueberry Acai 10:1 Cannabis Quencher Sip  x 2"
        
    }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
