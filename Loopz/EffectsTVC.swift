
//
//  EffectsTVC.swift
//  UFly
//
//  Created by 3 Embed on 25/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class EffectsTVC: UITableViewCell {
    
    @IBOutlet weak var heightForProgress: NSLayoutConstraint!
    @IBOutlet weak var effectType: UILabel!
    @IBOutlet weak var effectPercentage: UILabel!
    @IBOutlet weak var progressViesBar: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    /// initial View setup
    func initialSetup(){
        Fonts.setPrimaryRegular(effectPercentage)
        
        effectType.textColor =  Colors.SeconderyText
        effectPercentage.textColor =  Colors.SeconderyText
        progressViesBar.progressTintColor =  Colors.AppBaseColor
        progressViesBar.trackTintColor =  Colors.SeparatorLight
        progressViesBar.layer.cornerRadius = 2
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    ///this method updates the effects name and percentage
    ///
    /// - Parameter data: it is of type DictObject structure
    func setData(data:DictObject) {
        effectType.text = data.Name
        var dataValue:Float = 0
        if Utility.getStore().TypeStore == .Grocery {
            heightForProgress.constant = 0
            effectPercentage.text = data.Value
        }else{
            heightForProgress.constant = 10
            progressViesBar.progress = dataValue/100
            if (data.Value.floatValue != nil) {
                dataValue = data.Value.floatValue!
                effectPercentage.text = "\(dataValue)" + UIConstants.UIElement.Percent
            }else{
                effectPercentage.text = data.Value
            }
        }
        self.updateConstraints()
        self.layoutIfNeeded()
    }
}
