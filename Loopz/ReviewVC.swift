//
//  ReviewVC.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ReviewVC: UIViewController {

    @IBOutlet weak var navWidth: NSLayoutConstraint!
    @IBOutlet weak var bottomButtonView: UIView!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var mainCollectionView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    let arrayOfTip:[Float] = [10,20,50]
    var reviewVM = ReviewVM()
    var storeType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        setVMResponse()
        if let order = reviewVM.orderRecieved{
            reviewVM.getRating(order.storeType)
            storeType = order.storeType
        }
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func reviewButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        reviewVM.submitRating()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setVMResponse() {
        Helper.networkReachabilityChange.subscribe(onNext: { [weak self]dataGot in
            self?.reviewVM.getRating(self?.storeType ?? 0)
        }).disposed(by: self.reviewVM.disposeBag)
        reviewVM.ratingVM_response.subscribe(onNext: {[weak self]result in
            if result {
                self?.mainCollectionView.reloadData()
            }else{
                self?.reviewButtonAction(UIButton())
            }
        }, onError: {error in
        }).disposed(by: self.reviewVM.disposeBag)
    }
}
