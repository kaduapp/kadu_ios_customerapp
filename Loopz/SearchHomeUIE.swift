//
//  SearchHomeUIE.swift
//  DelivX
//
//  Created by 3Embed on 24/10/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension SearchHomeVC{
    
    func setUIforNaviagtion(){
        if searchVM.loadPopular{
            self.navigationViewHeight.constant = 0
            self.dishesTableViewLeading.constant = 0
        }else{
            self.navigationViewHeight.constant = 40
            self.dishesTableViewLeading.constant = self.scrollView.frame.size.width
            
        }
    }
    
    func setBanner() {
        dummyView.addSubview(Helper.showUnVerifyNotification().0)
        dummyView.clipsToBounds = true
        if Helper.showUnVerifyNotification().1 {
            bannerHeight.constant = 20
        }else{
            bannerHeight.constant = 0
        }
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
    
    func setEmptyScreen() {
        noProductsView.isHidden = true
        restuarantEmptyView.isHidden = true
        dishesEmptyView.isHidden = true
        
        //dishesEmptyView.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
        noProductsView.setData(image: #imageLiteral(resourceName: "EmptySearch"), title: StringConstants.EmptySearchProduct(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        restuarantEmptyView.setData(image: #imageLiteral(resourceName: "EmptyHistory"), title: StringConstants.NoMatchFound() + " " + StringConstants.Restaurants(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
        //restuarantEmptyView.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
        dishesEmptyView.setData(image: #imageLiteral(resourceName: "EmptyHistory"), title: StringConstants.NoMatchFound() + " " + StringConstants.Dishes(), buttonTitle: StringConstants.StartShopping(),buttonHide: true)
    }
    
    @objc func StartShopping() {
        Helper.changetTabTo(index: AppConstants.Tabs.Home)
    }
    
    /// sets navigation
    func setNavigation(){
        
        Helper.transparentNavigation(controller: self.navigationController!)
        self.navigationController?.navigationBar.backgroundColor = Colors.SecondBaseColor
        if let navBarCount = self.navigationController?.viewControllers.count {
           if navBarCount > 1{
                NavigationWidth.constant = UIScreen.main.bounds.size.width - 44
            }else{
                NavigationWidth.constant = UIScreen.main.bounds.size.width - 20
            }
        }
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        self.searchTF.addTarget(self, action: #selector(SearchVC.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        //Helper.setShadow(sender: dispensariesProductsView)
    }
    
    func setupUI() {
        self.scrollView.showsVerticalScrollIndicator = false
        self.scrollView.showsHorizontalScrollIndicator  = false
        Helper.setButtonTitle(normal: StringConstants.Restaurants(), highlighted: StringConstants.Restaurants(), selected: StringConstants.Restaurants(), button: restuarantButton)
        Helper.setButtonTitle(normal: StringConstants.Dishes(), highlighted: StringConstants.Dishes(), selected: StringConstants.Dishes(), button: dishesButton)
        Fonts.setPrimaryBold(dishesButton)
        Fonts.setPrimaryBold(restuarantButton)
        restuarantButton.setTitleColor(Colors.PrimaryText, for: .normal)
        dishesButton.setTitleColor(Colors.Unselector, for: .normal)
        indicator.backgroundColor = Colors.PrimaryText
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        restuarantTableView.rowHeight = UITableView.automaticDimension
        dishesTableView.rowHeight = UITableView.automaticDimension
        restuarantTableView.backgroundColor = Colors.ScreenBackground
        dishesTableView.backgroundColor = Colors.ScreenBackground
        
        Helper.setUiElementBorderWithCorner(element: searchView, radius: 7, borderWidth: 0, color: UIColor.white)
        cancelButton.setTitle(StringConstants.Cancel(), for: .normal)
        Fonts.setPrimaryMedium(cancelButton)
        Helper.setUiElementBorderWithCorner(element: indicator, radius: 1, borderWidth: 0, color: UIColor.clear)
        self.navigationViewHeight.constant = 0
        self.dishesTableViewLeading.constant = 0
        self.cancelBtnTrailingConstaraint.constant = 5
        self.searchHeaderView.setNeedsUpdateConstraints()
        self.searchHeaderView.layoutIfNeeded()
    }
}

extension SearchHomeVC:UIScrollViewDelegate{
    
    /// this method Gets Called when scrollViewDidEndDragging, scrollViewDidEndDecelerating
    ///moves to next or previous screen with animation
    /// - Parameter page: is of type Integer
    func scrollToPage(page:Int){
        UIView.animate(withDuration: 0.3){
            self.scrollView.contentOffset.x = self.scrollView.frame.width*CGFloat(page)
            self.updatePage(scrolView1: self.scrollView)
        }
    }
    
    func updatePage(scrolView1: UIScrollView) {
        var scrollIn = scrollView.contentOffset.x
        if Utility.getLanguage().Code == "ar" {
            scrollIn = scrollView.contentSize.width - scrollView.frame.size.width - scrollView.contentOffset.x
        }
        let scroll = (scrollIn / 2)
        distance.constant = scroll
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        let page:CGFloat = scrollIn / scrollView.frame.size.width
        
        if page == 0 {
            restuarantButton.setTitleColor(Colors.PrimaryText, for: .normal)
            dishesButton.setTitleColor(Colors.Unselector, for: .normal)
            restuarantButton.isSelected = false
            dishesButton.isSelected = false
        }
        else if page == 1 {
            dishesButton.setTitleColor(Colors.PrimaryText, for: .normal)
            restuarantButton.setTitleColor(Colors.Unselector, for: .normal)
            restuarantButton.isSelected = true
            dishesButton.isSelected = true
        }
        //historyVM.currentPage = Int(page)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.scrollView {
            updatePage(scrolView1: scrollView)
        }
        
        if scrollView == self.dishesTableView{
            if scrollView.contentOffset.y > 0 {
                Helper.setShadow(sender: navigationView)
            }else{
                Helper.removeShadow(sender: navigationView)
            }
        }
        if scrollView == self.restuarantTableView{
            if scrollView.contentOffset.y > 0 {
                Helper.setShadow(sender: navigationView)
            }else{
                Helper.removeShadow(sender: navigationView)
            }
        }
    }
}
