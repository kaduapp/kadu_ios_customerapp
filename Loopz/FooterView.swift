//
//  FooterView.swift
//  
//
//  Created by 3Embed on 30/05/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import UIKit

class MCDfooterView:UICollectionReusableView
{
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var viewForDottedLine: UIView!
    
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var desLB: UILabel!
}
