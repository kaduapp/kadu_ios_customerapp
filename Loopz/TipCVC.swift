//
//  TipCVC.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class TipCVC: UICollectionViewCell {
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var selectButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    func setCell() {
        Helper.setButton(button: selectButton, view: container, primaryColour: Colors.SecondBaseColor, seconderyColor: Colors.AppBaseColor, shadow: true)
        selectButton.setTitleColor(Colors.SecondBaseColor, for: .selected)
        Fonts.setPrimaryMedium(selectButton)
        Helper.setUiElementBorderWithCorner(element: container, radius: 3, borderWidth: 1, color: Colors.AppBaseColor)
        Helper.setShadow(sender: self)
    }
    
    func setData(data:Float, selected:Float) {
        selectButton.setTitle(Helper.df2so(Double(data)), for: .normal)
        if data == selected {
            selectButton.isSelected = true
        }else{
            selectButton.isSelected = false
        }
    }
}
