//
//  HelpVC.swift
//  DelivX
//
//  Created by 3 Embed on 11/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HelpVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    
    let helpVM = HelpVM()
    var selectedSection = 0
    var sectionRow = 0
    var didSelectRow = false
    var navView = NavigationView().shared
    override func viewDidLoad() {
        super.viewDidLoad()
        mainTableView.rowHeight = UITableView.automaticDimension
       self.mainTableView.backgroundColor = Colors.ScreenBackground
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.FAQs())
            Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        }
       
        // Do any additional setup after loading the view.
        setData()
        helpVM.helpAndSupport()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.title = StringConstants.FAQs()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.title = " "
    }
 
    func showDetail() {
        if didSelectRow == true {
            didSelectRow = false
        }
        else {
            didSelectRow = true
        }
        self.mainTableView.reloadData()
    }
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == UIConstants.SegueIds.HelpToHelpDetail {
            let sen = sender as! Support
            let vc:HelpDetailVC = segue.destination as! HelpDetailVC
            vc.arraySupport = sen.SubCat
            vc.titleText = sen.Title
        }else if segue.identifier == UIConstants.SegueIds.HelpToWeb {
            let data:Support = sender as! Support
            let vc:WebVC = segue.destination as! WebVC
            vc.urlGiven = data.Link
            vc.htmlDesc = data.Desc
            vc.titleText = data.Title
        }
        
    }

}
extension HelpVC {
    func setData()  {
        helpVM.helpVM_response.subscribe(onNext: { [weak self]success in
            var mySet: IndexSet = IndexSet.init()
            mySet.insert(2)
            self?.mainTableView.reloadSections(mySet, with: .fade)
        }, onError: { (Error) in
            
        }).disposed(by: self.helpVM.disposeBag)
    }
}

extension HelpVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainTableView {
            if scrollView.contentOffset.y > 0 {
                if (self.navigationController != nil) {
                    Helper.addShadowToNavigationBar(controller: self.navigationController!)
                }
            }else{
                if (self.navigationController != nil) {
                    Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                }
            }
        }
    }
    
    
}

