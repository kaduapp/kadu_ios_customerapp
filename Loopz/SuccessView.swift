//
//  SuccessView.swift
//  DelivX
//
//  Created by 3Embed on 27/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class SuccessView: UIView {

    @IBOutlet weak var progressView: MBCircularProgressBarView!
    
    @IBOutlet weak var successButton: UIButton!
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    
    private static var obj: SuccessView? = nil
    static var shared: SuccessView {
        
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[4] as? SuccessView
            obj?.frame = UIScreen.main.bounds
            obj?.successLabel.text = StringConstants.PlacedSuccess()
            Fonts.setPrimaryBlack(obj?.successLabel! as Any)
            obj?.successLabel.textColor = Colors.AppBaseColor
            obj?.successButton.tintColor = Colors.AppBaseColor
            obj?.progressView.progressColor = Colors.AppBaseColor
            Helper.setUiElementBorderWithCorner(element: (obj?.holderView)!, radius: 80, borderWidth: 0, color: UIColor.clear)
        }
        obj?.progressView.value = 0
        obj?.holderView.isHidden = true
        return obj!
    }
    
    private func setup() {
        var when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            self.animate()
        }
        let window:UIWindow = UIApplication.shared.delegate!.window!!
        window.addSubview(self)
        when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.hide()
        }
    }
    
    func showPI() {
        DispatchQueue.main.async() { // or you can use
            self.setup()
        }
    }
    
    func hide() {
        DispatchQueue.main.async() { // or you can use
            if (SuccessView.obj != nil) {
                self.removeFromSuperview()
            }
        }
    }
    
    func animate() {
        DispatchQueue.main.async(){
            UIView.animate(withDuration: 2) {
                SuccessView.obj?.holderView.isHidden = false
                SuccessView.obj?.progressView.value = 100
            }
        }
    }
}
