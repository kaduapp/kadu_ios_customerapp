//
//  Wallet.swift
//  DelivX
//
//  Created by 3Embed on 14/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct Wallet {
    var Amount:Float = 0
    var HardLimit:Float = 0
    var SoftLimit:Float = 0
    init(data:[String:Any]) {
        if let temp = data[APIResponceParams.WalletBalance] as? String {
            Amount = Float(temp)!
        }
        if let temp = data[APIResponceParams.WalletBalance] as? Float {
            Amount = temp
        }
        if let temp = data[APIResponceParams.WalletBalance] as? Double {
            Amount = Float(temp)
        }
        if let temp = data[APIResponceParams.WalletSoftLimit] as? String {
            SoftLimit = Float(temp)!
        }
        if let temp = data[APIResponceParams.WalletSoftLimit] as? Float {
            SoftLimit = temp
        }
        if let temp = data[APIResponceParams.WalletSoftLimit] as? Double {
            SoftLimit = Float(temp)
        }
        if let temp = data[APIResponceParams.WalletHardLimit] as? String {
            HardLimit = Float(temp)!
        }
        if let temp = data[APIResponceParams.WalletHardLimit] as? Float {
            HardLimit = temp
        }
        if let temp = data[APIResponceParams.WalletHardLimit] as? Double {
            HardLimit = Float(temp)
        }
    }
}
