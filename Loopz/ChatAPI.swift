//
//  SignUpAPI.swift
//  LiveMPro
//
//  Created by Nabeel Gulzar on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa

class ChatAPI:NSObject {
    
    let disposebag = DisposeBag()
    let chat_Response = PublishSubject<APIResponseModel>()
    
    func getTheChatHist(bookingID: String){
        
        let strURL = APIEndTails.BaseUrl + "chatHistory/" + bookingID
        RxAlamofire
            .requestJSON(.get, strURL,headers: Utility.getHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    print(" API: \(strURL)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                    Helper.hidePI()
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.showAlert(message: error.localizedDescription, head: StringConstants.Error(), type: 1)//alertVC(errMSG: error.localizedDescription)
                self.chat_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    func postTheMessage(parameters: [String:Any]?){
        let strURL = APIEndTails.BaseUrl + "message"
        let header = Utility.getHeader()
        RxAlamofire
            .requestJSON(.post, strURL ,parameters:parameters ,headers: header)
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    print(" API: \(strURL)");
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                    
                }
            }, onError: {  (error) in
                Helper.hidePI()
                Helper.showAlert(message: error.localizedDescription, head: StringConstants.Error(), type: 1)
                self.chat_Response.onError(error)
            }).disposed(by: disposebag)
    }
    
    
    /// parse the chat api response
    ///
    /// - Parameters:
    ///   - statusCode: httpstatuscodes
    ///   - responseDict: response data
    func checkResponse(statusCode:Int,responseDict: [String:Any]){
        
        print(" Response: \(responseDict)");
        let responseCodes : APICalls.ErrorCode = APICalls.ErrorCode(rawValue: statusCode)!
        Helper.hidePI()
        switch responseCodes {
        case .success:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.chat_Response.onNext(responseModel)
            break
        default:
            APICalls.basicParsing(data: responseDict, status: statusCode)
            break
        }
    }
}


///API Response Model
class APIResponseModel {
    
    var httpStatusCode:Int = 0
    /// Data in DIctionary format
    var data: [String: Any] = [:]
    
    init(statusCode:Int, dataResponse:[String:Any]) {
        httpStatusCode = statusCode
        data = dataResponse
    }
}
