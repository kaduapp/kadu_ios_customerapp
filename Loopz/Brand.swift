//
//  Brand.swift
//  DelivX
//
//  Created by 3Embed on 25/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct Brand {
    var Name        = ""
    var ID          = ""
    var Banner      = ""
    var Logo        = ""
    var Description = ""
    
    
    init(data:[String:Any]) {
        if let temp = data[APIResponceParams.IdAddress] as? String {
            ID = temp
        }
        if let temp = data[APIResponceParams.Logo] as? String {
            Logo = temp
        }
        if let temp = data[APIResponceParams.BrandName] as? String {
            Name = temp
        }
        if let temp = data[APIResponceParams.BannerImage] as? String {
            Banner = temp
        }
        if let temp = data[APIResponceParams.Description] as? String {
            Description = temp
        }
    }
}

