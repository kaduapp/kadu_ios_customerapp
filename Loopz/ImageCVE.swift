//
//  ImageCVE.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension ImageCell:UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageInCell
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView){
        let boundsSize = scrollView.bounds.size
        var frameToCenter = self.imageInCell.frame
        let widthDiff  = boundsSize.width  - frameToCenter.size.width
        let heightDiff = boundsSize.height - frameToCenter.size.height
        frameToCenter.origin.x = (widthDiff  > 0) ? widthDiff  / 2 : 0;
        frameToCenter.origin.y = (heightDiff > 0) ? heightDiff / 2 : 0;
        self.imageInCell.frame = frameToCenter
    }
}

extension ImageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == bottomImageCollectionView{
            let cell: ImageSmallCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ImageSmallCell.self), for: indexPath) as! ImageSmallCell
            Helper.setImage(imageView: cell.SmallmageView, url: arrayImages[indexPath.row], defaultImage: UIImage())
            return cell
        }else{
            let cell: ImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.ImageCell, for: indexPath) as! ImageCell
            Helper.setImage(imageView: cell.imageInCell, url: arrayImages[indexPath.row], defaultImage: UIImage())
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == bottomImageCollectionView{
            return CGSize(width: 60, height: 60)
        }else{
            var size = CGSize.zero
            let cellWidth = UIScreen.main.bounds.width
            let cellHight = collectionView.frame.size.height
            size.height =  cellHight
            size.width  =  cellWidth
            return size
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == bottomImageCollectionView {
            let PreviousIndexPath = IndexPath(item: selected, section: 0)
            if let cell = collectionView.cellForItem(at: PreviousIndexPath) as? ImageSmallCell{
                cell.ImageOuterView.layer.borderColor = UIColor.clear.cgColor
                cell.ImageOuterView.layer.borderWidth = 0
            }
            self.selected = indexPath.row
            self.mainCollectionview.scrollToItem(at: indexPath, at: .right, animated: true)
            if let cell1 = collectionView.cellForItem(at: indexPath) as? ImageSmallCell{
                cell1.ImageOuterView.layer.borderColor = Colors.AppBaseColor.cgColor
                cell1.ImageOuterView.layer.borderWidth = 1
            }
        }
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainCollectionview{
            var visibleRect = CGRect()
            visibleRect.origin = self.mainCollectionview.contentOffset
            visibleRect.size = self.mainCollectionview.bounds.size
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            guard let indexPath = self.mainCollectionview.indexPathForItem(at: visiblePoint) else { return }
            print(indexPath,"in End")
            let PreviousIndexPath = IndexPath(item: self.selected, section: 0)
            if let cell = self.bottomImageCollectionView.cellForItem(at: PreviousIndexPath) as? ImageSmallCell{
                cell.ImageOuterView.layer.borderColor = UIColor.clear.cgColor
                cell.ImageOuterView.layer.borderWidth = 0
            }
            self.selected = indexPath.row
            let SelectedIndexPath = IndexPath(item: self.selected, section: 0)
            self.bottomImageCollectionView.scrollToItem(at: SelectedIndexPath, at: .left, animated: true)
            if let SelectedCell = self.bottomImageCollectionView.cellForItem(at: SelectedIndexPath) as? ImageSmallCell{
                SelectedCell.ImageOuterView.layer.borderColor = Colors.AppBaseColor.cgColor
                SelectedCell.ImageOuterView.layer.borderWidth = 1
            }
        }
    }
}
