//
//  AddOnTableViewCell.swift
//  DelivX
//
//  Created by 3Embed on 03/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AddOnTableViewCell: UITableViewCell {

    @IBOutlet weak var addOnName: UILabel!
    @IBOutlet weak var addOnprice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addOnName.textColor = Colors.DarkBlueBlack
        Fonts.setPrimaryRegular(addOnName, size: 12)
        addOnprice.textColor = Colors.DarkBlueBlack
        Fonts.setPrimaryRegular(addOnprice, size: 12)
        //addOnprice.text = Helper.setCurrency(displayAmount: 20, price: 20).string
    }

    func setData(data:AddOnGroup,indexPath:IndexPath){
        let dataAddOn = data.AddOns[indexPath.row]
        addOnName.text = dataAddOn.Name
        addOnprice.text = Helper.df2so(Double( dataAddOn.Price))
    }
    
    func setDataforAddon(data: AddOn){
        addOnName.text = data.Name
        addOnprice.text = Helper.df2so(Double( data.Price))
    }
}
