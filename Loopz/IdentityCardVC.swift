//
//  IdentityCardVC.swift
//  DelivX
//
//  Created by 3 Embed on 10/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class IdentityCardVC: UIViewController {
    
    @IBOutlet var idCardView: IdentityCardView!
    var editProfileVM = EditProfileVM()
    var uploadImageModel  = UploadImageModel.shared
    var navView = NavigationView().shared

    override func viewDidLoad() {
        super.viewDidLoad()
        didUpload()
        editProfileVM.pickedImage = nil
        editProfileVM.pickerObj?.delegate = self
        
        editProfileVM.userData = Utility.getProfileData()
        editProfileVM.authentication.MMJUrl = (editProfileVM.userData?.MMJCard.Url)!
        editProfileVM.authentication.IDUrl = (editProfileVM.userData?.IdCard.Url)!
        self.idCardView.updateView(data: editProfileVM.userData!)
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.IdentityCard())
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        Helper.removeNavigationSeparator(controller: self.navigationController!,image: true)
   
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // Do any additional setup after loading the view.
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func bottomButtonAction(_ sender: UIButton) {
        if idCardView.editFlag == false {
            
            if editProfileVM.userData?.MMJCard.Url.count == 0 && editProfileVM.userData?.MMJCard.Url.count == 0 {
                Helper.setButtonTitle(normal: StringConstants.AddCard(), highlighted: StringConstants.AddCard(), selected: StringConstants.AddCard(), button: idCardView.bottomButton)
                
            }
            else {
                Helper.setButtonTitle(normal: StringConstants.Save().uppercased(), highlighted: StringConstants.Save().uppercased(), selected: StringConstants.Save().uppercased(), button: idCardView.bottomButton)
                
            }
            idCardView.editFlag = true
            idCardView.idUpdateView.isHidden = false
            idCardView.mmjUpdateView.isHidden = false
        }else{
            self.editProfileVM.isValidEmail = true
            self.editProfileVM.isValidPhone = true
            idCardView.idUpdateView.isHidden = true
            idCardView.mmjUpdateView.isHidden = true
            self.editProfileVM.updateProfile()
            idCardView.editFlag = false
            Helper.setButtonTitle(normal: StringConstants.Update().uppercased(), highlighted: StringConstants.Update().uppercased(), selected: StringConstants.Update().uppercased(), button: idCardView.bottomButton)
        }
    }
    
    @IBAction func idCartdOrMMJCardAction(_ sender: UIButton) {
        if idCardView.editFlag {
            // ImageChosenTag
            var flag = 0
            editProfileVM.btnTag = EditProfileVM.ImageType(rawValue: sender.tag)!
            var imageChosen:UIImage? = nil
            if editProfileVM.btnTag == .iDCard && editProfileVM.userData?.IdCard.Verified != false {
                imageChosen = idCardView.pickedIDImage
            }else if editProfileVM.btnTag == .mmjCard && editProfileVM.userData?.IdCard.Verified != false {
                imageChosen = idCardView.pickedMMJImage
            }
            if imageChosen == nil {
                flag = 0
            }
            else {
                flag = 1
            }
            editProfileVM.pickerObj?.setProfilePic(controller: self , tag: flag)
        }
    }
    
}

// MARK: - ImagePickerModelDelegate
extension IdentityCardVC: ImagePickerModelDelegate {
    
    func didPickImage(selectedImage: UIImage , tag:Int) {
        if tag == 0 {
            
            if editProfileVM.btnTag == .mmjCard {
                editProfileVM.authentication.MMJUrl = ""
                idCardView.pickedMMJImage = nil
                idCardView.mmjCardImage.image = #imageLiteral(resourceName: "UploadDefault")
                idCardView.mmjUploadedLabel.isHidden = true
                idCardView.mmjCheckMark.isHidden = true
            }
                
            else {
                editProfileVM.authentication.IDUrl = ""
                idCardView.pickedIDImage = nil
                idCardView.idCardImage.image = #imageLiteral(resourceName: "UploadDefault")
                idCardView.idUploadedLabel.isHidden = true
                idCardView.idCheckMark.isHidden = true
            }
        }//Image Added
        else {
            
            if editProfileVM.btnTag == .mmjCard {
                
                idCardView.pickedMMJImage = selectedImage
                idCardView.mmjCardImage.image = selectedImage
                idCardView.mmjUploadedLabel.isHidden = false
                idCardView.mmjCheckMark.isHidden = false
            }
            else {
                idCardView.pickedIDImage = selectedImage
                idCardView.idCardImage.image = selectedImage
                idCardView.idUploadedLabel.isHidden = false
                idCardView.idCheckMark.isHidden = false
            }
            uploadImage()
        }
        if idCardView.pickedIDImage != nil && idCardView.pickedMMJImage != nil {
            idCardView.bottomLabelView.isHidden = true
        }
        else {
            idCardView.bottomLabelView.isHidden = true
        }
        
    }
    func uploadImage(){
        switch editProfileVM.btnTag {
        case .mmjCard?:
            editProfileVM.authentication.MMJUrl = Helper.uploadImage(data:idCardView.pickedMMJImage!,path:AmazonKeys.MMJPic, page: .IdentityCardVC)
            self.editProfileVM.userData?.MMJCard.Url = editProfileVM.authentication.MMJUrl
            self.editProfileVM.userData?.MMJCard.Verified = false
            break
        case .iDCard?:
            editProfileVM.authentication.IDUrl = Helper.uploadImage(data:idCardView.pickedIDImage!,path: AmazonKeys.IDPic, page: .IdentityCardVC)
            self.editProfileVM.userData?.IdCard.Url = editProfileVM.authentication.IDUrl
            self.editProfileVM.userData?.IdCard.Verified = false
            break
        default:
            break
        }
    }
}
extension IdentityCardVC {
    func didUpload(){
        uploadImageModel.UploadImageRespose.subscribe(onNext: { [weak self]success in
            
            if success == .IdentityCardVC {
                Helper.setButtonTitle(normal: StringConstants.Save().uppercased(), highlighted: StringConstants.Save().uppercased(), selected: StringConstants.Save().uppercased(), button: (self?.idCardView.bottomButton)!)
                self?.idCardView.updateView(data: (self?.editProfileVM.userData!)!)
            }
        }).disposed(by: self.editProfileVM.disposeBag)
        MainProfileVM.MainProfileVM_response.subscribe(onNext: { [weak self]success in
            if success {
                self?.editProfileVM.userData = Utility.getProfileData()
                self?.editProfileVM.authentication.MMJUrl = (self?.editProfileVM.userData?.MMJCard.Url)!
                self?.editProfileVM.authentication.IDUrl = (self?.editProfileVM.userData?.IdCard.Url)!
                self?.idCardView.updateView(data: (self?.editProfileVM.userData!)!)
            }
        }).disposed(by: self.editProfileVM.disposeBag)
    }
}


