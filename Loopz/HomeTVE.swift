//
//  HomeTVE.swift
//  UFly
//
//  Created by 3Embed on 08/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

// MARK: - Tableview Delegate
extension HomeVC : UITableViewDelegate, UITableViewDataSource {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if homeVM.offers.count > 0 {
                return 10
            }
        case 1:
            if homeVM.items.count > 0 {
                return 10
            }
        case 2:
            if homeVM.itemsLow.count > 0 {
                return 10
            }
        case 3:
            if homeVM.itemsFav.count > 0 {
                return 10
            }
        case 4:
            if homeVM.brands.count > 0 {
                return 10
            }
        case 5:
            if homeVM.categories.count > 0 {
                return 50
            }
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 5{
            return Helper.showHeader(title: StringConstants.Categories(), showViewAll: false, showSeperator: false)
        }
        return Helper.showHeader(title: "", showViewAll: false)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if loading {
                return 0
            }
            
            if homeVM.offers.count > 0 {
                return 1
            }
            return 0
        case 1:
            if loading {
                return 1
            }
            if homeVM.items.count > 0 {
                return 1
            }
        case 2:
            if loading {
                return 1
            }
            
            if homeVM.itemsLow.count > 0 {
                return 1
            }
        case 3:
            if loading {
                return 1
            }
            
            if homeVM.itemsFav.count > 0 {
                return 1
            }
        case 4:
            if loading {
                return 0
            }
            
            if homeVM.brands.count > 0 {
                return 1
            }
            return 0
        case 5:
            return homeVM.categories.count
        default:
            break
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section
        {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ItemImageTableCell, for: indexPath) as! ItemImageTableCell
            cell.itemImageCV.tag = 100
            cell.collectionHeight.constant = (UIScreen.main.bounds.width - 40)*417/1005
            cell.itemImageCV.hideLoader()
            if loading {
                cell.itemImageCV.showLoader()
            }
            DispatchQueue.main.async() {
                if self.loading {
                    cell.itemImageCV.showLoader()
                }else{
                    cell.itemImageCV.hideLoader()

                }
                cell.itemImageCV.reloadData()
            }
            cell.titleLabel.text = StringConstants.AvailableCoupons()
            cell.itemImageCV.isPagingEnabled = false
            cell.headHeight.constant = 0
            return cell
        case 1,2,3:
            // this cell is for offers, trending products, low price , fav
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ItemListTableCell, for: indexPath) as! ItemListTableCell
            cell.itemListCV.register(UINib(nibName: String(describing: ItemCommonCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self))
            cell.itemListCV.tag = indexPath.section
            cell.viewAllButton.tag = indexPath.section
            cell.itemListCV.hideLoader()
            cell.titleView.hideLoader()
            if loading {
                cell.itemListCV.showLoader()
                cell.titleView.showLoader()
            }else{
                switch indexPath.section {
                case 1:
                    cell.arrayAvailable = homeVM.items
                    break
                case 2:
                    cell.arrayAvailable = homeVM.itemsLow
                    break
                case 3:
                    cell.arrayAvailable = homeVM.itemsFav
                    break
                default:
                    cell.arrayAvailable = []
                    break
                }

                cell.titleLabel.text = homeVM.titles[indexPath.section]
                cell.viewAllButton.addTarget(self, action:#selector(HomeVC.viewAll) , for: UIControl.Event.touchUpInside)
                if cell.arrayAvailable.count > 2 {
                    cell.viewAllButton.isHidden = false
                }else{
                    cell.viewAllButton.isHidden = true
                }

            }
            DispatchQueue.main.async() {
                cell.itemListCV.reloadData()
            }
            return cell
        case 4:
            // this cell is topBrands
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.ItemImageTableCell, for: indexPath) as! ItemImageTableCell
            cell.itemImageCV.tag = 101
            cell.collectionHeight.constant = (UIScreen.main.bounds.width - 20)/3
            cell.itemImageCV.hideLoader()
            if loading {
                cell.itemImageCV.showLoader()
            }
            DispatchQueue.main.async() {
                cell.itemImageCV.hideLoader()
                if self.loading {
                    cell.itemImageCV.showLoader()
                }
                cell.itemImageCV.reloadData()
            }
            cell.titleLabel.text = StringConstants.topBrands()
            cell.headHeight.constant = 40
            cell.itemImageCV.isPagingEnabled = false
            return cell
        default:
            // this cell is Category_Subcategory
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.Category_SubcategoryTVC, for: indexPath) as! Category_SubcategoryTVC
            if homeVM.categories.count > indexPath.row {
                cell.setData(data: homeVM.categories[indexPath.row],cont: self)
                cell.selectButton.tag = indexPath.row
                cell.selectButton.addTarget(self, action:#selector(HomeVC.pressed) , for: UIControl.Event.touchUpInside)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    /// this method updates the Cell
    ///
    /// - Parameter sender: is of type Button each cell will be having tag as indexpath.row
    @objc func pressed(sender : UIButton!) {
        let data = homeVM.categories[sender.tag]
        if data.SubCategories.count == 0{
            let subData = SubCategory.init(data: [:], store: Store.init(data: [:]))
            let dataSend = [data as Any,subData as Any] as [Any]
            self.performSegue(withIdentifier:UIConstants.SegueIds.HomeToItemList, sender: dataSend)
        }
        var tempArr = [Category]()
        var indexPathArray = [IndexPath]()
        var ind = IndexPath()
        for dataTemp in homeVM.categories {
            let index = homeVM.categories.index(of: dataTemp)
            let indexPath = IndexPath.init(item: index!, section: 5)
            if dataTemp.Id == data.Id {
                if dataTemp.selected == false {
                    dataTemp.selected = true
                }else {
                    dataTemp.selected = false
                }
                ind = indexPath
                indexPathArray.append(indexPath)
            }else {
                if dataTemp.selected == true {
                    dataTemp.selected = false
                    indexPathArray.append(indexPath)
                }
            }
            tempArr.append(dataTemp)
        }
        homeVM.categories = tempArr
        self.mainTableView.reloadRows(at: indexPathArray, with: .fade)
        UIView.animate(withDuration: 0.5) {
            self.mainTableView.scrollToRow(at: ind, at: .middle, animated: true)
        }
    }
    
    @objc func viewAll(sender : UIButton!) {
        self.performSegue(withIdentifier: String(describing: ViewAllVC.self), sender: sender.tag)
    }
}

