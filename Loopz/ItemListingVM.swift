//
//  ItemListingVM.swift
//  UFly
//
//  Created by 3 Embed on 24/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ItemListingVM: NSObject {
    
    //Variables
    var controllerCategory:Category? = nil
    var bottomConstraint = 0
    let ItemListingVM_response = PublishSubject<Bool>()
   
    var subCategorySelected = SubCategory.init(data: [:], store: Utility.getStore())
    var SubSubCatArray:[SubSubCategory] = []
    var store:Store? = Store.init(data: [:])
    var hideStore = false
    let disposeBag = DisposeBag()
    
    var filterResult:[SubSubCategory] = []
    var arrayOfBrand:[String] = []
    var arrayOfManufacturer:[String] = []
    var selectedSort = -1
    var selectedOffer = ""
    
    
    
    /// subscription to get items
    func getItems() {
        if controllerCategory == nil {
            return
        }
        HomeAPICalls.getItems(category:(controllerCategory?.Id)!, store: (store?.Id)!, subcat: subCategorySelected.Id).subscribe(onNext: {result in
            self.SubSubCatArray = result[0] as! [SubSubCategory]
            self.store = result[1] as? Store
            self.ItemListingVM_response.onNext(true)
        
        }, onError: {error in
            print(error)
        }).disposed(by: disposeBag)
    }
}
