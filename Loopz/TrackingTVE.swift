//
//  TrackingTVE.swift
//  UFly
//
//  Created by 3 Embed on 06/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension TrackingVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0{
         let orderCell = tableView.dequeueReusableCell(withIdentifier:"OrderTrackStatusCell", for: indexPath) as! TrackingTableCell
         orderCell.setOrderStatus(order:dataOrder!)
         return orderCell
        
        }
        
        if indexPath.row == 1{
            let orderCell = tableView.dequeueReusableCell(withIdentifier:"OrderChatCell", for: indexPath) as! TrackingTableCell
            orderCell.setChatStatus(order:dataOrder!)
            return orderCell
            
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.TrackingTableCell, for: indexPath) as! TrackingTableCell

        cell.updateDriverCell(order:dataOrder!)
    
         return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1{
        
            self.performSegue(withIdentifier: String(describing: ZendeskVC.self), sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.row == 2 {
            if let status = self.dataOrder?.Status, status >= 8 && status != 40{
            return UITableView.automaticDimension
            }
            else{
             return 0
            }
        }
        return UITableView.automaticDimension
    }
    
}

