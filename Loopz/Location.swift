//
//  Location.swift
//  UFly
//
//  Created by 3Embed on 08/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class Location: NSObject {
    
    var LocationDescription                 = ""
    var Latitude:Float                      = 0
    var Longitude:Float                     = 0
    var PlaceId                             = ""
    var Name                                = ""
    var City                                = " "
    var State                               = ""
    var Zipcode                             = "0"
    var Country                             = ""
    var FullText                            = ""
    var ZoneId                              = ""
    var ZoneName                            = ""
    var MainText                            = ""
    var Tag                                 = ""
    var CityName                            = ""
    var CityId                              = ""
    var Street                              = ""
    
 
    init(data:[String:Any]) {
        Name                                = ""
        City                                = " "
        State                               = ""
        Zipcode                             = "0"
        Country                             = ""
        FullText                            = ""
        MainText                            = ""
        Tag                                 = ""
        if let titleTemp = data[GoogleKeys.PlaceId] as? String{
            PlaceId                         = titleTemp
        }
        if let titleTemp = data[GoogleKeys.Structure] as? [String:Any] {
            if let titleTemp2 = titleTemp[GoogleKeys.MainText] as? String {
                MainText = titleTemp2
            }
        }
        if let titleTemp = data[GoogleKeys.Description] as? String{
            LocationDescription             = titleTemp
        }
        if let titleTemp = data[GoogleKeys.LocationLat] as? Float{
            Latitude             = titleTemp
        }
        if let titleTemp = data[GoogleKeys.LocationLong] as? Float{
            Longitude             = titleTemp
        }
        if let titleTemp = data[GoogleKeys.LocationLat] as? Double{
            Latitude             = Float(titleTemp)
        }
        if let titleTemp = data[GoogleKeys.LocationLong] as? Double{
            Longitude             = Float(titleTemp)
        }
        if let tag = data[APIResponceParams.Tag] as? String {
            Tag = tag
        }
        
        
    }
    
    
    
    func initBangalore() -> Location {
        Name                                = ""
        City                                = " "
        State                               = ""
        Zipcode                             = "0"
        Country                             = ""
        FullText                            = "Bangalore, Karnataka, India"
        MainText                            = ""
        Tag                                 = ""
        

        PlaceId                         = "ChIJbU60yXAWrjsR4E9-UejD3_g"
        
        MainText = "Bangalore"

        LocationDescription             = "Bangalore, Karnataka, India"
        
        return self
    
    }
    
    
    
    func setFromAddress(data:Address) {
           PlaceId                         = data.PlaceId
           MainText                        = data.AddLine1
           Name = data.AddLine2
           City = data.City
           State = data.State
           Zipcode = String(data.Zipcode)
           Country = data.Country
           Latitude = data.Latitude
           Longitude = data.Longitude
           Tag = data.Tag
           FullText = AddressUtility.getSortedAdress(data)
       }
     func update(data:GMSAddress){
                 
    //             if data.name != nil {
    //                 Name = data.name!
    //             }
            MainText =  data.lines![0] ?? ""

                 // Street address
                 if data.thoroughfare != nil {
                     Street = data.thoroughfare!
                 }
                 if data.thoroughfare != nil {
                     if Street.sorted().count > 0 {
                         Street = Street + ", " + data.thoroughfare!
                     }else{
                         Street = data.thoroughfare!
                     }
                     
                     let nameString = Name.replacingOccurrences(of: ",", with: "")
                     let streetString = Street.replacingOccurrences(of: ",", with: "")
                     let nameArray = nameString.sorted()
                     let streetArray = streetString.sorted()
                     if nameArray == streetArray
                     {
                         Street = ""
                     }
                 }
                 
                 // City
                 if data.locality != nil {
                     City = data.locality!
                 }
                 // State
                 if data.administrativeArea != nil {
                     State = data.administrativeArea!
                 }
                 
                 // Zip code
                 if data.postalCode != nil {
                     Zipcode = data.postalCode!
                 }
                 
                 // Country
                 if data.country != nil {
                     Country = data.country!
                 }
                 if LocationDescription.sorted().count > 0 {
                     FullText = LocationDescription
                 }else{
                     if MainText.sorted().count > 0 && !self.sortAddress().contains(MainText){
                         let lines = data.lines! as [String]
                        FullText =  data.lines![0] ?? " "
                         
                     }else{
                         
                         FullText = self.sortAddress()
                         
                     }
                 }
            
            FullText =  data.lines![0] ?? ""
            
            if data.coordinate.latitude != 0 &&  data.coordinate.longitude != 0 {
                    Latitude = Float(data.coordinate.latitude)
                     Longitude = Float(data.coordinate.longitude)
                 }
             }
    

    func sortAddress() -> String{
           
           var address = ""
           if Name.count > 0{
               address =   Name
           }
           if Street.count > 0 && Street != Name {
               address = "\(address), " +  Street
           }
           if City.count > 0 {
               address = "\(address), " +  City
           }
           if State.count > 0 {
              // address = "\(address), " +  State
           }
           if Country.count > 0 {
               address = "\(address), " +  Country
           }
           if Zipcode.count > 0 {
           //    address = "\(address), " +  Zipcode
           }
           
           return address
       }
    
    
    
    func update(data:[String:Any], location:CLLocation){
        if let titleTemp = data[GoogleKeys.PlaceId] as? String {
            PlaceId                         = titleTemp
        }
        if let titleTemp = data[GoogleKeys.MainText] as? String{
            MainText             = titleTemp
        }
        if let titleTemp = data[GoogleKeys.Description] as? String{
            LocationDescription             = titleTemp
        }
        if let name = data[GoogleKeys.AddressName] as? String {
            Name = name
        }
        
        // City
        if let city = data[GoogleKeys.AddressCity] as? String {
            City = city
        }
        // State
        if let city = data[GoogleKeys.AddressState] as? String {
            State = city
        }
        
        // Zip code
        if let zip = data[GoogleKeys.AddressZIP] as? String {
            Zipcode = zip
        }
        
        // Country
        if let country = data[GoogleKeys.AddressCountry] as? String {
            Country = country
        }
        if LocationDescription.length > 0 {
            FullText = LocationDescription
        }else{
            if MainText.length > 0 {
                FullText = MainText + ", " + Name + ", " + City + ", " + State + ", " + Country + ", " + Zipcode
            }else{
                FullText = Name + ", " + City + ", " + State + ", " + Country + ", " + Zipcode
            }
        }
        Latitude = Float(location.coordinate.latitude)
        Longitude = Float(location.coordinate.longitude)
        // zoneid
        if let zoneid = data[GoogleKeys.ZoneId] as? String {
            ZoneId = zoneid
        }
        // zonename
        if let zonename = data[GoogleKeys.ZoneName] as? String {
            ZoneName = zonename
        }
        if let tag = data[APIResponceParams.Tag] as? String {
            Tag = tag
        }
        if let cityName = data[GoogleKeys.CityName] as? String {
            CityName = cityName
        }
        if let Cityid = data[GoogleKeys.CityID] as? String {
            CityId = Cityid
        }
    }
    
//    func setFromAddress(data:Address) {
//        PlaceId                         = data.PlaceId
//        MainText                        = data.AddLine1
//        Name = data.AddLine2
//        City = data.City
//        State = data.State
//        Zipcode = String(data.Zipcode)
//        Country = data.Country
//        Latitude = data.Latitude
//        Longitude = data.Longitude
//        Tag = data.Tag
//        FullText = AddressUtility.getSortedAdress(data)
//    }
    
    func update(data:CLPlacemark ,placeName:String){
        
        if placeName.length > 0{
            Name = placeName
        }
        else{
            if data.name != nil {
                Name = data.name!
            }
        }

        // City
        if data.locality != nil {
            if data.subLocality != nil && data.subLocality != data.locality{
                City =  "\(data.subLocality!), \(data.locality!)"
            }
            else{
                City = data.locality!
            }
        }
        // State
        if data.administrativeArea != nil {
            State = data.administrativeArea!
        }
        
        // Zip code
        if data.postalCode != nil {
            Zipcode = data.postalCode!
        }
        
        // Country
        if data.country != nil {
            Country = data.country!
        }
        if LocationDescription.sorted().count > 0 {
            FullText = LocationDescription
        }else{
            if MainText.sorted().count > 0 && MainText != Name {
                FullText = MainText + ", " + Name + ", " + City + ", " + State + ", " + Country + ", " + Zipcode
            }else{
                FullText = Name  + ", " + City + ", " + State + ", " + Country + ", " + Zipcode
            }
        }
        if data.location != nil {
            Latitude = Float(data.location!.coordinate.latitude)
            Longitude = Float(data.location!.coordinate.longitude)
        }
    }
}
