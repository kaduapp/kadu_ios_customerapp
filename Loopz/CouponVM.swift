//
//  CouponVM.swift
//  DelivX
//
//  Created by 3 Embed on 07/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CouponVM: NSObject {
    let disposeBag = DisposeBag()
    let couponVM_response = PublishSubject<Bool>()
    var couponArr:[Coupon] = []
    
    func getCoupons(){
        CheckoutAPICalls.getCoupons().subscribe(onNext:{ [weak self]success in
            self?.couponArr = success
            self?.couponVM_response.onNext(true)
        }).disposed(by: self.disposeBag)
        
    }
    
    
}
