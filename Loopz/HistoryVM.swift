//
//  HistoryVM.swift
//  UFly
//
//  Created by 3Embed on 11/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class HistoryVM: NSObject {
    
    let historyVM_response = PublishSubject<ResponceType>()
    let disposeBag = DisposeBag()
    var arrayOfActiveOrder = [Order]()
    var arrayOfPastOrder = [Order]()
    var selectedOrder = Order(data: [:])
    var currentPage = 0
    var refreshControlActive = UIRefreshControl()
    var refreshControlPast = UIRefreshControl()
    
    enum ResponceType:Int {
        case PastOrder = 2
        case ActiveOrder = 1
        
    }

    // - Parameter data: is  of type int 2 for active order 1 for past order
    func historyIn(data:Int ,storeType:Int) {
        HistoryAPICalls.getOrderHistory(data: data, storeType: storeType).subscribe(onNext: {result in
            Helper.hidePI()
            if data == 2{
                self.arrayOfActiveOrder = result 
                self.historyVM_response.onNext(.ActiveOrder)
            }else{
                self.arrayOfPastOrder = result
                self.historyVM_response.onNext(.PastOrder)
            }
        }, onError: {error in
        }).disposed(by: disposeBag)
    }
}

extension HistoryVC{
    // - Parameter data: is  of type int 2 for active order 1 for past order
    func reOrder(data:Order) {
        HistoryAPICalls.reOrder(order: data).subscribe(onNext: {result in
            if result {
                //self.navigationController?.dismiss(animated: true, completion:nil)
              
                if self.isFromProfile{
                    self.performSegue(withIdentifier: UIConstants.SegueIds.HistoryToCart, sender: self)
                }else{
                    Helper.changetTabTo(index: AppConstants.Tabs.Cart)
                }
                Helper.hidePI()
            }
        }, onError: {error in
        }).disposed(by: historyVM.disposeBag)
    }
}
