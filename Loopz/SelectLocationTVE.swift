//
//  SelectLocationTVE.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import GoogleMaps

// MARK: - UITableViewDataSource
extension SelectLocationVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if noDelivery {
            tableView.backgroundView = emptyView
            return 0
        }
        tableView.backgroundView = nil
        if selectLocVM.addressList.count > 0 {
            return 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if selectLocVM.addressList.count == 0 && selectLocVM.recentSearchList.count == 0 && selectLocVM.savedAddressList.count == 0
        {
            noContentView.isHidden = false
        }else{
            noContentView.isHidden = true
        }
        if section == 1 {
            if selectLocVM.addressList.count > 0
            {
                return selectLocVM.addressList.count
            }
            return selectLocVM.savedAddressList.count
        }
        return selectLocVM.recentSearchList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.LocationTableViewCell, for: indexPath) as! LocationTableViewCell
        if indexPath.section == 0 {
             if self.selectLocVM.didSelectRow == false {
                cell.setCurrentLocation()
            }
        }
        if indexPath.section == 1 {
            if selectLocVM.addressList.count > 0 {
                cell.setValue(data: selectLocVM.addressList[indexPath.row])
            }else{
                cell.setSavedAddress(data: selectLocVM.savedAddressList[indexPath.row])
            }
        }else if indexPath.section == 2 {
            cell.setValue(data: selectLocVM.recentSearchList[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var titleHead = ""
        switch section {
        case 0:
            titleHead = StringConstants.CurrentLocation()
            break
        case 1:
            if selectLocVM.addressList.count > 0{
                titleHead = StringConstants.ResultLocation()
            }else if selectLocVM.savedAddressList.count > 0{
                titleHead  = StringConstants.AddressListTitle()
            }
            break
        case 2:
            if selectLocVM.recentSearchList.count > 0{
                titleHead = StringConstants.RecentLocation()
            }
            break
        default:
            break
        }
        return Helper.showHeader(title: titleHead,showViewAll: false)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var titleHead = ""
        var height:CGFloat = 0
        switch section {
        case 0:
            titleHead = StringConstants.CurrentLocation()
            break
        case 1:
            if selectLocVM.addressList.count > 0{
                titleHead = StringConstants.ResultLocation()
            }else if selectLocVM.savedAddressList.count > 0{
                titleHead  = StringConstants.AddressListTitle()
            }
            break
        case 2:
            if selectLocVM.recentSearchList.count > 0{
                titleHead = StringConstants.RecentLocation()
            }
            break
        default:
            break
        }
        if titleHead.count > 0{
            height = 50
        }
        return height
    }
}

// MARK: - UITableViewDelegate
extension SelectLocationVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var data:Location! = Utility.currentLatAndLong()
        if indexPath.section == 0 && data.Latitude == 0 && data.Longitude == 0 {
            LocationManager.shared.start()
            return
        }
        Helper.showPI(string: StringConstants.ValidateLocation())
        if indexPath.section == 1 {
            if selectLocVM.addressList.count > 0 {
                data = selectLocVM.addressList[indexPath.row]
            }else if selectLocVM.savedAddressList.count > 0 {
                data = selectLocVM.savedAddressList[indexPath.row]
            }
        }else if indexPath.section == 2 {
            data = selectLocVM.recentSearchList[indexPath.row]
        }
        selectLocVM.locationManager?.delegate = self
        if data.PlaceId.length > 0 {
            selectLocVM.locationManager?.getAddressFromPlace(place: data)
        }else if data.Latitude != 0 && data.Longitude != 0 {
            let locationAddress = CLLocation(latitude: CLLocationDegrees(data.Latitude), longitude: CLLocationDegrees(data.Longitude))
            selectLocVM.locationManager?.updateLocationData(location: locationAddress, placeIn: data,flag: false)
        }
     selectLocVM.didSelectRow = true
    }
}
