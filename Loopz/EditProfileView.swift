//
//  EditProfileView.swift
//  UFly
//
//  Created by 3 Embed on 29/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class EditProfileView: UIView {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var idCardLabel: UILabel!
    @IBOutlet weak var idTitle: UILabel!
    @IBOutlet weak var mmjCardLabel: UILabel!
    @IBOutlet weak var mmjCardTitle: UILabel!
    @IBOutlet weak var dontHaveMMjLabel: UILabel!
    
    @IBOutlet weak var idUploadedLabel: UILabel!
    @IBOutlet weak var mmjUploadedLabel: UILabel!
    
    @IBOutlet weak var mmjCheckMark: UIButton!
    @IBOutlet weak var idCheckMark:  UIButton!
    
    @IBOutlet weak var mmjCardImage: UIImageView!
    @IBOutlet weak var idCardImage: UIImageView!
    
    @IBOutlet weak var bottomLabelView: UIView!
    @IBOutlet weak var bottomLabel: UILabel!
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneNum: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileSeperator: UIView!
    
    @IBOutlet weak var idCardSeperator: UIView!
    @IBOutlet weak var mmjCardSeperator: UIView!
    @IBOutlet weak var countryCode: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var appVersion: UILabel!
    
    @IBOutlet weak var nameSeparator: UIView!
    @IBOutlet weak var emailSeparator: UIView!
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var changeBtn: UIButton!
   
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var changeBtnView: UIView!
    @IBOutlet weak var profileBtnView: UIView!
    
    
    var pickedIDImage:UIImage? = nil
    var pickedMMJImage:UIImage? = nil
    var pickedProfileImage:UIImage? = nil
    
    var isDissmis = false
    var editFlag = false
    var phoneNumber:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialViewSetup()
        nameTF.Semantic()
        emailTF.Semantic()
        phoneNum.Semantic()
    }
    
    /// initial View setup
    func initialViewSetup() {
        
        Fonts.setPrimaryMedium(idCardLabel)
        Fonts.setPrimaryMedium(mmjCardLabel)
        
        Fonts.setPrimaryMedium(idUploadedLabel)
        Fonts.setPrimaryMedium(mmjUploadedLabel)
        
        Fonts.setPrimaryMedium(nameTF)
        Fonts.setPrimaryMedium(emailTF)
        Fonts.setPrimaryMedium(phoneNum)
        Fonts.setPrimaryMedium(logoutBtn)
        
        Fonts.setPrimaryRegular(idTitle)
        Fonts.setPrimaryRegular(emailTF)
        Fonts.setPrimaryRegular(bottomLabel)
        
        Helper.setButton(button: profileBtn,view:profileBtnView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        
        
        Helper.setUiElementBorderWithCorner(element: profileBtn, radius: Helper.setRadius(element: profileBtn), borderWidth: 0, color:  Colors.SeparatorLight)
        
        profileBtn.backgroundColor =  Colors.AppBaseColor
        
        dontHaveMMjLabel.textColor =  Colors.PrimaryText
        
        idCardLabel.textColor =  Colors.SeconderyText
        mmjCardLabel.textColor =  Colors.SeconderyText
        profileSeperator.backgroundColor =  Colors.SeparatorLight
        
        idCardSeperator.backgroundColor =  Colors.SeparatorLight
        mmjCardSeperator.backgroundColor =  Colors.SeparatorLight
        
        nameSeparator.backgroundColor =  Colors.SeparatorLight
        emailSeparator.backgroundColor =  Colors.SeparatorLight
        
        
        idTitle.textColor =  Colors.SecoundPrimaryText
        mmjCardTitle.textColor =  Colors.SecoundPrimaryText
        bottomLabel.textColor =  Colors.SecondBaseColor
        
        mmjUploadedLabel.textColor =  Colors.AppBaseColor
        idUploadedLabel.textColor =  Colors.AppBaseColor
        
        idCheckMark.tintColor =  Colors.AppBaseColor
        mmjCheckMark.tintColor =  Colors.AppBaseColor
        
        Helper.setUiElementBorderWithCorner(element: mmjCardImage, radius: 3.0, borderWidth: 0,color: Colors.SeparatorLight)
        Helper.setUiElementBorderWithCorner(element: idCardImage, radius: 3.0, borderWidth: 0, color:  Colors.SeparatorLight)
        Helper.setUiElementBorderWithCorner(element: profileImage, radius: Helper.setRadius(element:profileImage), borderWidth: 0.5, color:  Colors.SeparatorLight)
        Helper.setButtonTitle(normal: StringConstants.Edit(), highlighted: StringConstants.Edit(), selected: StringConstants.Edit(), button: editBtn)
        Helper.setButtonTitle(normal: StringConstants.Change(), highlighted: StringConstants.Change(), selected: StringConstants.Change(), button: changeBtn)
        Helper.setButtonTitle(normal: StringConstants.Logout(), highlighted: StringConstants.Logout(), selected: StringConstants.Logout(), button: logoutBtn)
        
    
       Helper.setUiElementBorderWithCorner(element: mmjCardImage, radius: 2.0, borderWidth: 0,color: Colors.AppBaseColor)
        
        editBtn.setTitleColor( Colors.AppBaseColor, for: .normal)
        changeBtn.setTitleColor( Colors.AppBaseColor, for: .normal)
        
        idUploadedLabel.text = StringConstants.Verified()
        mmjUploadedLabel.text = StringConstants.Verified()
        
        mmjCardLabel.text = StringConstants.MMJCard()
        idCardLabel.text  = StringConstants.IDCard()
        
        
        idTitle.text = StringConstants.UploadIDTitle()
        mmjCardTitle.text = StringConstants.UploadMMj()
        dontHaveMMjLabel.text = StringConstants.GetMMJ()
        
        Helper.updateText(text: dontHaveMMjLabel.text!, subText: StringConstants.GetOne(), dontHaveMMjLabel, color:  Colors.AppBaseColor,link: "")
        
        appVersion.text = Utility.AppVersion
      
    }
    
    
    
    

    
    
    /// updates the view
    ///
    /// - Parameter data: it is of type User model object
    func updateView(data: User){
        nameTF.text = data.FirstName
        phoneNum.text =  "\(data.CountryCode)\(data.Phone)"
        emailTF.text = data.Email
        
        Helper.setImage(imageView: profileImage, url: (data.userProfilePic), defaultImage: #imageLiteral(resourceName: "UserDefault") )
        Helper.setImage(imageView: mmjCardImage, url: data.MMJCard.Url, defaultImage: #imageLiteral(resourceName: "UploadDefault"))
        Helper.setImage(imageView: idCardImage, url: data.IdCard.Url, defaultImage: #imageLiteral(resourceName: "UploadDefault"))
        if data.IdCard.Verified {
            idUploadedLabel.isHidden = false
            idCheckMark.isHidden = false
        }else{
            idUploadedLabel.isHidden = true
            idCheckMark.isHidden = true
        }
        if data.MMJCard.Verified {
            mmjUploadedLabel.isHidden = false
            mmjCheckMark.isHidden = false
        }else{
            mmjUploadedLabel.isHidden = true
            mmjCheckMark.isHidden = true
        }
    }
    
}
