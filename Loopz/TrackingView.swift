//
//  TrackingView.swift
//  UFly
//
//  Created by 3 Embed on 06/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import GoogleMaps
import RxSwift

class TrackingView: UIView {
    var timerForTrack:Timer? = nil
    var pathDrawable = true
    let disposeBag = DisposeBag()

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var fullScreenBtn: UIButton!
    
    var startMark:GMSMarker? = nil
    var stopMark:GMSMarker? = nil
    var trackMark:GMSMarker? = nil
    var oldLat:Float = 0
    var oldLong:Float = 0
    var angleLatest:Double = 0
    
    var destMarker = ETAView().shared
    var pickMarker = ETAView().shared

    var headerView = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        mapView.settings.scrollGestures = false
         mapView.isMyLocationEnabled = true
          mapView.settings.myLocationButton = true
       // mapView.settings.zoomGestures = false
        
        fullScreenBtn.isHidden = true
        didGetResponse()

    }
    func drawPath(data:Order)
    {
        timerSet()
        
       
        startMark = Helper.setMarker(latitude: data.PickLat, longitude: data.PickLong, iconView: pickMarker)
        startMark?.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        startMark?.map = mapView
        
        
        stopMark = Helper.setMarker(latitude: data.DropLat, longitude: data.DropLong, iconView: destMarker)
        stopMark?.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        stopMark?.map = mapView
        
     // Helper.drawPath(mapView: mapView, order: data)
        self.fullScreenBtn.bringSubviewToFront(self)
    }
    
    
    func trackPath(data:Order)
    {
        if pathDrawable {
            pathDrawable = false
            mapView.clear()
            trackMark = nil
            drawPath(data: data)
        }

        if oldLat == 0 || oldLong == 0 {
            oldLat = data.DriverLat
            oldLong = data.DriverLong
        }
        var angle = GMSGeometryHeading(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(oldLat), longitude: CLLocationDegrees(oldLong)), CLLocationCoordinate2D.init(latitude: CLLocationDegrees(data.DriverLat), longitude: CLLocationDegrees(data.DriverLong)))
        if (trackMark == nil && data.Status >= 8) {
            trackMark = Helper.setMarker(latitude: data.DriverLat, longitude: data.DriverLong, icon: #imageLiteral(resourceName: "TrackCar"))
            angleLatest = angle
            trackMark?.rotation = angleLatest
            trackMark?.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            print("\n\nAngle: \(angleLatest)\n\n")
            trackMark?.map = mapView
            oldLat = data.DriverLat
            oldLong = data.DriverLong
        } else {
            angle = GMSGeometryHeading(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(oldLat), longitude: CLLocationDegrees(oldLong)), CLLocationCoordinate2D.init(latitude: CLLocationDegrees(data.DriverLat), longitude: CLLocationDegrees(data.DriverLong)))
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            trackMark?.position =  CLLocationCoordinate2D(latitude: CLLocationDegrees(data.DriverLat), longitude: CLLocationDegrees(data.DriverLong))
            angleLatest = angle
            trackMark?.rotation = angleLatest
            trackMark?.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            CATransaction.commit()
            oldLat = data.DriverLat
            oldLong = data.DriverLong
            print("\n\nAngle: \(angleLatest)\n\n")
        }
        if trackMark == nil{
         Helper.fitAllMarkers(data: [startMark!,stopMark!], mapView: mapView)
        }
        else{
         Helper.fitAllMarkers(data: [trackMark!,startMark!,stopMark!], mapView: mapView)
        }
       
    }
    
    func timerSet() {
        if timerForTrack != nil {
            timerForTrack!.invalidate()
            timerForTrack = nil
        }
        pathDrawable = false
        timerForTrack = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    @objc func update() {
        pathDrawable = true
    }
    func didGetResponse(){
        Helper.pathObserver.subscribe(onNext: { [weak self]data in
            if data.1 == 1{
                self?.destMarker.setData(title: data.0)
                self?.pickMarker.setData(title: "")
            }else{
                self?.pickMarker.setData(title: data.0)
                self?.destMarker.setData(title: "")
            }
        }).disposed(by: disposeBag)
    }
    
    

}

