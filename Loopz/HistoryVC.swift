//
//  HistoryVC.swift
//  UFly
//
//  Created by 3 Embed on 04/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController {
    
    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var dummyView: UIView!
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    @IBOutlet weak var pastOrderTableView: UITableView!
    @IBOutlet weak var activeOrderTableView: UITableView!
    
    @IBOutlet weak var topSeperatorView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContent: UIView!
    @IBOutlet weak var pastOrderButton: UIButton!
    @IBOutlet weak var activeOrderButton: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var indicator: UIView!
    @IBOutlet weak var distance: NSLayoutConstraint!
    
    var historyVM = HistoryVM()
    var emptyViewActive = EmptyView().shared
    var emptyViewPast = EmptyView().shared
    var navView = NavigationView().shared
    var isFromProfile = false
    var isPastOrder = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEmptyScreen()
        updateView()
        didconnectResponse()
        didconnectMQTTResponse()
 
        if self.navigationController != nil && !Changebles.isGrocerOnly{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.History())
        }
        else{
            self.topTitleLabel.textColor = Colors.PrimaryText
            self.topTitleLabel.text = StringConstants.History()
             Fonts.setPrimaryBold(topTitleLabel)
        }
        
    }

    override func viewDidAppear(_ animated: Bool) {
        if Utility.getLanguage().Code == "ar" {
            scrollToPage(page: 1)
        } else {
            scrollToPage(page: 0)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.refresh(sender: self.historyVM.refreshControlActive)

        if !Changebles.isGrocerOnly && (self.navigationController?.viewControllers.count)! <= 1 {
            leftBarButton()
        }
        if isFromProfile
        {
            leftBarButton()
        }
        self.setBanner()
         
    }
    
    @IBAction func didTapActiveOrder(_ sender: Any) {
        
        if Utility.getLanguage().Code == "ar" {
            scrollToPage(page: 1)
        } else {
            scrollToPage(page: 0)
        }
    }
    
    @IBAction func didTapPastOrder(_ sender: Any) {
        historyVM.historyIn(data:1, storeType: Utility.getSelectedSuperStores().type.rawValue)
        
        if Utility.getLanguage().Code != "ar" {
            scrollToPage(page: 1)
        } else {
            scrollToPage(page: 0)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backACT(_ sender: UIBarButtonItem){
        Utility.saveStore(location: [:])
        self.tabBarController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
     }
    
    //Setup Left bar button
    func leftBarButton() {
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "CloseIcon"), style: .plain, target: self, action: #selector(backACT(_:)))
        item.imageInsets = UIEdgeInsets.init(top: 0, left: -4, bottom: 0, right: -4)
        self.navigationItem.setLeftBarButtonItems([item], animated: true)
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.SegueIds.HistoryToTrackOrder {
            let nav = segue.destination as! UINavigationController
            if let viewController: TrackingVC = nav.viewControllers.first as! TrackingVC? {
                viewController.dataOrder = historyVM.selectedOrder
            }
        }
        else if segue.identifier == UIConstants.SegueIds.HistoryToOrderDetail {
            let nav = segue.destination as! UINavigationController
            if let viewController: OrderDetailVC = nav.viewControllers.first as! OrderDetailVC? {
                self.tabBarController?.tabBar.isHidden  = true
               viewController.orderDetailVM.selectedOrder = historyVM.selectedOrder
                viewController.orderDetailVM.ispastOrder = isPastOrder
               viewController.Response_Picker.subscribe(onNext: { success in
                                 print(success)
                                 if success == 16 ||  success == 20
                                 {
                                     self.refresh(sender: self.historyVM.refreshControlActive)
                                 }
                             }).disposed(by: historyVM.disposeBag)
                
                
            }
        }else if segue.identifier == UIConstants.SegueIds.HistoryToChat {
            let nav = segue.destination as! UINavigationController
            if let viewController: ChatVC = nav.viewControllers.first as! ChatVC? {
                viewController.bookingID = "\(historyVM.selectedOrder.BookingId)"
                viewController.custName = historyVM.selectedOrder.DriverName
                viewController.customerID = historyVM.selectedOrder.DriverId
                viewController.custImage = historyVM.selectedOrder.DriverImage
            }
        } else if segue.identifier == String(describing: ZendeskVC.self) {
            let nav = segue.destination as! UINavigationController
            if let nextScene: ZendeskVC = nav.viewControllers.first as! ZendeskVC?{
                nextScene.subJuctGiven = "\(historyVM.selectedOrder.BookingId)"
            }
        }else if segue.identifier == UIConstants.SegueIds.HistoryToCart {
            let nav = segue.destination as! UINavigationController
            if let nextScene: CartVC = nav.viewControllers.first as! CartVC?{
                nextScene.isCartFromProfile = self.isFromProfile
            }
        }
    }
}

//Extention for vm response
extension HistoryVC {
    /// observe for response of signinVM
    func didconnectResponse(){
        historyVM.historyVM_response.subscribe(onNext: {success in
            switch success {
            case HistoryVM.ResponceType.ActiveOrder:
                if self.historyVM.arrayOfActiveOrder.count == 0 {
                    self.activeOrderTableView.backgroundView = self.emptyViewActive
                }else{
                    self.activeOrderTableView.backgroundView = nil
                }
                self.activeOrderTableView.reloadData()
                break
            case HistoryVM.ResponceType.PastOrder:
                if self.historyVM.arrayOfPastOrder.count == 0 {
                    self.pastOrderTableView.backgroundView = self.emptyViewPast
                }else{
                    self.pastOrderTableView.backgroundView = nil
                }

                self.pastOrderTableView.reloadData()
                break
            }
            self.setBanner()
        }, onError: {error in
            print(error)
        }).disposed(by: APICalls.disposesBag)
    }
}

extension HistoryVC{
    /// observe for response of signinVM
    func didconnectMQTTResponse(){
        MQTTDelegate.message_response.subscribe(onNext: {success in
            for data in self.historyVM.arrayOfActiveOrder {
                if data.BookingId == success.Id && success.Action != 12 {
                    if success.Status == 8 || success.Status == 10 {
                        data.DriverId = success.DriverId
                        data.DriverName = success.DriverName
                        data.DriverImage = success.DriverImage
                        data.DriverEmail = success.DriverEmail
                        data.DriverMobile = success.DriverMobile
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3 ) {
                     if success.Status == 20
                     {
                        self.refresh(sender: self.historyVM.refreshControlActive)
                        self.pastOrderTableView.reloadData()
                        self.activeOrderTableView.reloadData()
                    }
                    }
                    
                    if success.Status != 0 {
                        data.Status = success.Status
                        data.StatusMessage = success.Message
                    }
                    if success.Action != 14 {
                        let index = self.historyVM.arrayOfActiveOrder.index(of: data)!
                        self.historyVM.arrayOfActiveOrder.remove(at: index)

                        var mySet: IndexSet = IndexSet.init()
                        mySet.insert(index)
                        self.activeOrderTableView.deleteSections(mySet, with: .bottom)
                        if data.Status == 15 || data.Status == 7  || data.Status == 3 || data.Status == 2  || data.Status == 16 || data.Status == 9 {
                            self.historyVM.arrayOfPastOrder.insert(data, at: 0)
                            var mySet: IndexSet = IndexSet.init()
                            mySet.insert(0)
                            self.pastOrderTableView.insertSections(mySet, with: .top)
                            self.pastOrderTableView.backgroundView = nil
                            self.scrollToPage(page: 1)
                        }else{
                            self.historyVM.arrayOfActiveOrder.insert(data, at: 0)
                            var mySet: IndexSet = IndexSet.init()
                            mySet.insert(0)
                            self.activeOrderTableView.insertSections(mySet, with: .top)
                            self.activeOrderTableView.backgroundView = nil
                            self.scrollToPage(page: 0)
                        }

                    }
                }
            }
            
        }, onError: {error in
            print(error)
        }).disposed(by: APICalls.disposesBag)
    }
}
