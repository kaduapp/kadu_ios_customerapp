//
//  UIConstants.swift
//  UFly
//
//  Created by 3Embed on 26/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class UIConstants: NSObject {
    
    struct FontSize {
        static let Heading = 20
        static let Title = 16
        static let TubTitle = 13
    }
    
    
    static let ButtonBorderWidth                           = 1
    static let ButtonCornerRadius                          = 5
    static let MainStoryBoard                              = "Main"
    static let AuthenticationStoryboard                    = "Authentication"
    static let PopupStoryboard                                      = "SelectionPopup"
    static let TabBar                                      = "TabBar"
    
    static let HistoryStoryboard                           = "HistoryUI"
    
    struct SegueIds {
        static let SuperStoreToHome                         = "SuperStoreToHome"
        static let SplashToHome                             = "SplashToHome"
        static let SplashToLocation                         = "SplashToLocation"
        static let LocationToHome                           = "LocationToHome"
        static let LoginToForgotPassword                    = "LoginToForgotPassword"
        static let ForgotPWToOtp                            = "ForgotPWToOtp"
        static let OtpToEnterNewPW                          = "OtpToEnterNewPW"
        static let SignUpToOtp                              = "SignUpToOtp"
        static let SigninToSignup                           = "SigninToSignup"
        static let OTPToIdentity                            = "OTPToIdentity"
        static let HomeToLocation                           = "homeToLocation"
        static let SigninToOTP                              = "signinToOTP"
        static let ProfileToSavedAddr                       = "profileToSavedAddr"
        static let ProfileToInviteVC                        = "profileToInviteVC"
        static let ToMap                                    = "ToMap"
        static let HomeToItemList                           = "HomeToItemList"
        static let profileToSavedCards                      = "ProfileToPayment"
        static let PaymentToAddCard                         = "paymenToAddNewCard"
        static let ItemListingToMenu                        = "ItemListingToMenu"
        static let checkoutToSavedAddress                   = "checkoutToSavedAddress"
        static let ItemListingToDispensaries                = "itemListingToDispensaries"
        static let CartToCheckout                           = "CartToConfirmOrderVC"
        static let HomeToItemDetail                         = "HomeToItemDetail"
        static let ItemListingToItemDetails                 = "itemListingToItemDetails"
        static let ProfileToEditProfile                     = "profileToEditProfile"
        static let ProfileToWishList                        = "profileToWishList"
        static let EditProfileToOTP                         = "editProfileToOTP"
        static let ItemListingToSearch                      = "itemListingToSearch"
        static let HistoryToTrackOrder                      = "historyToTrackOrder"
        static let HistoryToOrderDetail                     = "historyToOrderDetail"
        static let SearchToItemDetails                      = "SearchToItemDetails"
        static let SearchToStore                            = "SearchToStore"
        static let PaymentFromCheckout                      = "paymentFromCheckout"
        static let TrackingToDetails                        = "TrackingToDetails"
        static let TrackingVCToFullScreenMapVC              = "TrackingVCToFullScreenMapVC"
        static let CartToItemDetail                         = "CartToItemDetail"
        static let ItemdetailToAddList                      = "ItemdetailToAddList"
        static let PopupToCreateNew                         = "PopupToCreateNew"
        static let AllWishListToWishListDetails             = "AllWishListToWishListDetails"
        static let EditProfileToEditVC                      = "editProfileToEditVC"
        static let EditProfileToChangePW                    = "EditProfileToChangePW"
        static let RegisterPayment                          = "RegisterPayment"
        static let ProfileToIdentityCard                    = "profileToIdentityCard"
        static let ProfileToHelpVC                          = "profileToHelpVC"
        static let ProfileToFav                             = "ProfileToFav"
        static let PaymentToWallet                          = "PaymentToWallet"
        static let DatePickerVC                             = "DatePickerVC"
        static let HelpToWeb                                = "helpToWeb"
        static let HelpToHelpDetail                         = "HelpToHelpDetail"
        static let ConfirmVCToCouponVC                      = "ConfirmVCToCouponVC"
        static let PaymentSeutUp                             = "PaymentSeutUp"
        static let PaymentCard                              = "PaymentCard"
        static let WalletFromCheckout                       = "WalletFromCheckout"
        static let OrderDetailToWebVC                       = "OrderDetailToWebVC"
        static let AllCardsToDetails                        = "AllCardsToDetails"
        static let ListToDetails                            = "listToDetails"
        static let FavToDetails                             = "favToDetails"
        static let ItemListingHeaderCell                    = "ItemListingHeaderCell"
        static let QuickCardToPayment                       = "QuickCardToPayment"
        static let ConfirmToPaymentOp                       = "ConfirmToPaymentOp"
        static let PaymentopToAddNew                        = "PaymentopToAddNew"
        static let PaymentToQuickCard                       = "PaymentToQuickCard"
        static let ProfileToQuickCard                       = "profileToQuickCard"
        static let HelpDetailToWebVC                        = "HelpDetailToWebVC"
        static let OrderToCancel                            = "OrderToCancel"
        static let HistoryToChat                            = "HistoryToChat"
        static let profileToHistoryVC                       = "profileToHistoryVC"
        static let navDetails                               = "navDetails"
        static let HistoryToCart                            = "HistoryToCart"
        static let collectionSlot                           = "collectionSlot"
        static let seguetoConfirmOrder                      = "seguetoConfirmOrder"
     }
    
    struct CellIds {
        static let LocationTableViewCell                    = "LocationTableViewCell"
        static let Category_SubcategoryTVC                  = "Category_SubcategoryTVC"
        static let SubCategoryCVC                           = "SubCategoryCVC"
        static let MainProfileCell                          = "MainProfileCell"
        static let MainProfileMenuCell                      = "MainProfileMenuCell"
        static let SavedAddresTableCell                     = "SavedAddresTableCell"
        static let PaymentTableCell                         = "PaymentTableCell"
        static let CartCollectionCell                       = "CartCollectionCell"
        static let CartSectionHeaderCell                    = "CartSectionHeaderView"
        static let CartSectionFooterCell                    = "CartSectionFooterView"
        static let DispensariesTableCell                    = "DispensariesTableCell"
        static let ItemDetailTableCell                      = "ItemDetailTVC"
        static let RateAndReviewTableCell                   = "RateAndReviewTVC"
        static let DescriptionTableCell                     = "DescriptionTVC"
        static let EffectsTableCell                         = "EffectsTVC"
        static let ItemImageCVC                             = "ItemImageCVC"
        static let ItemUnitCVC                              = "ItemUnitCVC"
        static let ItemListTableCell                        = "ItemListTableCell"
        static let ItemImageTableCell                       = "ItemImageTableCell"
        static let ItemImageCollectionCell                  = "ItemImageCollectionCell"
        static let HistoryTableCell                         = "HistoryTableCell"
        static let WishListTableCell                        = "WishListTableCell"
        static let TrackingTableCell                        = "TrackingTableCell"
        static let MenuTableCell                            = "MenuTableCell"
        static let AdressDetailCell                         = "AdressDetailCell"
        static let TotalItemsCell                           = "TotalItemsCell"
        static let AmountDetailCell                         = "AmountDetailCell"
        static let VNHCountryPickerCell                     = "VNHCountryPickerCell"
        static let SearchHeaderCell                         = "SearchHeaderCell"
        static let SearchTVC                                = "SearchTVC"
        static let PaymentSectionHead                       = "PaymentSectionHead"
        static let PopupCell                                = "PopupCell"
        static let CreateNewListCell                        = "CreateNewListCell"
        static let AllWishListCell                          = "AllWishListCell"
        static let ConfirmAddressCell                       = "ConfirmAddressCell"
        static let ItemHeader                               = "ItemHeader"
        static let ScheduleTimeCell                         = "ScheduleTimeCell"
        static let ConfirmPayMethodCell                     = "ConfirmPayMethodCell"
        static let PromoCodeCell                            = "PromoCodeCell"
        static let ConfirmPaymentCell                       = "ConfirmPaymentCell"
        static let WalletTableCell                          = "WalletTableCell"
        static let PaymentListCell                          = "PaymentListCell"
        static let HelpTableCell                            = "HelpTableCell"
        static let HelpTitleCell                            = "HelpTitleCell"
        static let ProductsCell                             = "ProductsCell"
        static let CouponTableCell                          = "CouponTableCell"
        static let ItemListingHeaderCell                    = "ItemListingHeaderCell"
        static let ImageCell                                = "ImageCell"
        static let ItemDetailsToImages                      = "ItemDetailsToImages"
        static let RatingHeaderCell                         = "RatingHeaderCell"
        static let TipCVC                                   = "TipCVC"
        static let PickupLocationCell                       = "PickupLocationCell"
        static let DeliveryLocationCell                     = "DeliveryLocationCell"
        static let SelectLocationCell                       = "SelectLocationCell"
    }
    
    struct ControllerIds {
        
        static let SignInVC                                 = "SignInVC"
        static let CartVC                                   = "CartVC"
        //storyboard Identifiers
        static let VNHCountryPicker                         = "VNHCountryPicker"
        static let ItemDetailVC                             = "ItemDetailVC"
        static let ReviewVC                                 = "ReviewVC"
        static let OrderDetailVC                            = "OrderDetailVC"
        static let HistoryVC                                = "HistoryVC"

    }
    
    struct PageTitles {
        static let Profile                                  = StringConstants.ProfileTitle()
        static let AddressList                              = StringConstants.ManageAddress()
        static let Referrals                                = StringConstants.ReferralsTitle()
    }
    
    struct UIElement {
        static let Card                                            = "XXXX XXXX XXXX"
        static let Percent                                         = "%"
    }
    
    
}
