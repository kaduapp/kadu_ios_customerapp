//
//  WalletHistoryVM.swift
//  DelivX
//
//  Created by 3Embed on 13/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class WalletHistoryVM: NSObject {

    let walletHistoryVM_response = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    var arrayOfDebit = [Transaction]()
    var arrayOfCredit = [Transaction]()
    var currentPage = 0
    var index = 0
    
    func getHistory() {
        WalletAPICalls.getTransaction(page: currentPage).subscribe(onNext: { [weak self]data in
            self?.index = (self?.index)! + 1
            self?.arrayOfDebit = data.0
            self?.arrayOfCredit = data.1
            self?.walletHistoryVM_response.onNext(true)
        }).disposed(by: disposeBag)
    }
}
