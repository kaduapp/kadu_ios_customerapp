//
//  DispensariesVC.swift
//  UFly
//
//  Created by 3 Embed on 24/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DispensariesVC: UIViewController {

    let dispensariesVM = DispensariesVM()
    let responceBack = PublishSubject<Store>()
    
    @IBOutlet weak var mainTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDispensaries()
        dispensariesVM.getDispensaries()
        self.title =  StringConstants.Dispensaries()
        Helper.addShadowToNavigationBar(controller: self.navigationController!)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StoreToHome" {
            if let viewController: HomeVC = segue.destination as? HomeVC{
                viewController.homeVM.store = sender as! Store
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title =  StringConstants.Dispensaries()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
}

extension DispensariesVC{
    
    /// observe for ViewModel
    func getDispensaries(){
        dispensariesVM.DispensariesVM_response.subscribe(onNext: {success in
            if success {
                self.mainTableView.reloadData()
                self.mainTableView.scrollsToTop = true
            }
        }).disposed(by: self.dispensariesVM.disposeBag)
    }
    
}
