//
//  ItemUnitCVC.swift
//  UFly
//
//  Created by 3 Embed on 25/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ItemUnitCVC: UICollectionViewCell {
    
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var unitIndicator: UIView! //for paymentMethod
    @IBOutlet weak var cellBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    /// initial viewSetup
    func initialSetup(){
        Fonts.setPrimaryRegular(unitLabel)
        unitLabel.textColor = Colors.BtnBackground
    }
    
    func isSelectedUnit(){
        Helper.setUiElementBorderWithCorner(element: cellContentView, radius: 4, borderWidth: 1.0, color: Colors.Red)
        unitLabel.textColor = Colors.Red
    }
    
    func isUnselectedUnit(){
        Helper.setUiElementBorderWithCorner(element: cellContentView, radius: 4, borderWidth: 1.0, color: Colors.BtnBackground)
        unitLabel.textColor = Colors.BtnBackground
    }

    /// updates the item Unit
    func updateView(data: Unit){
   // unitLabel.text = "\(String(data.Value))(\(data.Title))"
    unitLabel.text = data.Title
    }

    //Confirm OrderVC
    func selectedPayMethod(pickup: Int){
        
        if pickup == 1 {
            unitLabel.textColor = Colors.DeliveryBtn
            unitIndicator.backgroundColor = Colors.DeliveryBtn
        }else {
            unitLabel.textColor = Colors.PickupBtn
            unitIndicator.backgroundColor = Colors.PickupBtn
        }
        unitIndicator.isHidden = false
    }
    
    func unselectedPayMethod(){
        unitLabel.textColor = Colors.FieldHeader
        unitIndicator.isHidden = true
        
    }
    
    func updatePayMethods(data: String){
      Fonts.setPrimaryMedium(unitLabel)
       unitLabel.text = data
    }
 
}
