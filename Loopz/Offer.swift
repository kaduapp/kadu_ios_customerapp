//
//  Offer.swift
//  DelivX
//
//  Created by 3Embed on 23/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation


struct Offer {
    var ImageURL = ""
    var Name = ""
    var Description = ""
    var Id = ""
    var StoreId = ""
    
    init(data:[String:Any]) {
        print(data)
        
        if let titleTemp = data["offerName"] as? String{
            
            Name = titleTemp
        }

        if let titleTemp = data[APIResponceParams.Name] as? String{
            
            Name = titleTemp
        }
        if let titleTemp = data[APIResponceParams.StoreImage] as? [String:Any]{
            ImageURL = titleTemp[APIResponceParams.Phone] as! String
        }
        if let titleTemp = data[APIResponceParams.Description] as? String {
            Description  = titleTemp
        }
        if let titleTemp = data[APIResponceParams.OfferId] as? String {
            Id  = titleTemp
        }
        if let titleTemp = data[APIResponceParams.offerStoreId] as? String {
            print("storeID")
            StoreId  = titleTemp
        }
    }
    
}
