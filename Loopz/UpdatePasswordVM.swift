//
//  UpdatePasswordVM.swift
//  UFly
//
//  Created by 3 Embed on 21/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UpdatePasswordVM: NSObject {
    
    let disposeBag = DisposeBag()
    var authentication = Authentication()
    var updatePasswordVM_responce = PublishSubject<Bool>()
    
    /// having subscription to updatepassword API defined in AuthenticationAPICalls
    func updatePassword() {
        AuthenticationAPICalls.updatePassword(user:authentication).subscribe(onNext: {[weak self]success in
            if success == true{
                self?.updatePasswordVM_responce.onNext(true)
            }
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    
}
