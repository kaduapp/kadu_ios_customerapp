//
//  MenuTableCell.swift
//  UFly
//
//  Created by 3 Embed on 20/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class MenuTableCell: UITableViewCell {
    
    @IBOutlet weak var subCatName: UILabel!
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var dotView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    /// updates the subcategory for selected category
    ///
    /// - Parameters:
    ///   - data: it is of type subcategory
    ///   - id: sucategory id..to set selected perticular sub cat
    func updateCell(data: Any,id: String){
        if let dataFrom = data as? SubSubCategory {
            subCatName.text = dataFrom.Name
            dotView.isHidden = true
            subCatName.textColor = Colors.SecoundPrimaryText
            if dataFrom.Id == id {
                
                dotView.isHidden = false
                subCatName.textColor = Colors.PrimaryText
            }
            count.text = "\(dataFrom.Products.count)"
        }else if let dataFrom = data as? Category {
            subCatName.text = dataFrom.Name
            dotView.isHidden = true
            subCatName.textColor = Colors.SecoundPrimaryText
            if dataFrom.Id == id {
                
                dotView.isHidden = false
                subCatName.textColor = Colors.PrimaryText
            }
            count.text = ""
        }
        
    }
    
    
    /// initialView setup
    func initialSetup(){
        dotView.isHidden = true
        Fonts.setPrimaryMedium(subCatName)
        Fonts.setPrimaryMedium(count)
        subCatName.textColor = Colors.SecoundPrimaryText
        dotView.layer.cornerRadius = CGFloat(Helper.setRadius(element: dotView))
        dotView.backgroundColor = Colors.PrimaryText
    }
    
    
}
