//
//  SearchHomeTVE.swift
//  DelivX
//
//  Created by 3Embed on 24/10/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension SearchHomeVC:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == dishesTableView{
            if searchVM.arrayOfItems.count == 0{
             if searchVM.recentSearchList.count > 0 {
                    tableView.backgroundView = nil
                    return 1
                }else{
                    tableView.backgroundView = noProductsView
                }
            }else if searchVM.arrayOfItems.count > 0 {
                return 1
            }
            return 0
        }else{
            if searchVM.arrayOfStores.count > 0{
                return 1
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == dishesTableView{
            if searchVM.arrayOfItems.count > 0 {
                return searchVM.arrayOfItems.count
            }
            if searchVM.recentSearchList.count > 0 {
                return searchVM.recentSearchList.count
            }
            return 0
        }else{
            if searchVM.arrayOfStores.count > 0{
                return searchVM.arrayOfStores.count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == dishesTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProductsCell.self) ) as! ProductsCell
                    if searchVM.arrayOfItems.count > 0 {
                        cell.setName(item: searchVM.arrayOfItems[indexPath.row])
                    }else{
                        cell.setName(item: searchVM.recentSearchList[indexPath.row])
                    }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier:  String(describing: DispensariesTableCell.self)) as! DispensariesTableCell
            cell.setData(data: searchVM.arrayOfStores[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        searchTF.resignFirstResponder()
        lang = Utility.getLanguage().Code
        
        if tableView == dishesTableView{
            self.selectIndex = indexPath.row
            if searchVM.arrayOfItems.count > 0 {
                self.searchTF.text = searchVM.arrayOfItems[indexPath.row]
                searchVM.getProductsWithinStore(text: searchVM.arrayOfItems[indexPath.row],language: "en")
                productName = searchVM.arrayOfItems[indexPath.row]
            }else{
                searchVM.getProductsWithinStore(text: searchVM.recentSearchList[indexPath.row],language: "en")
            }
        }else if searchVM.arrayOfStores[indexPath.row].StoreIsOpen {
            Utility.saveStore(location: searchVM.arrayOfStores[indexPath.row].dataObject)
            Utility.UtilityUpdate.onNext(Utility.TypeServe.ChangeStore)
            self.performSegue(withIdentifier: String(describing: RestHomeVC.self), sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == dishesTableView{
            return UITableView.automaticDimension
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == dishesTableView{
            if searchVM.arrayOfItems.count > 0 {
                return Helper.showHeader(title: StringConstants.Suggestions(), showViewAll: false)
            }else if searchVM.recentSearchList.count > 0 {
                return Helper.showHeader(title: StringConstants.RecentSearches(), showViewAll: false)
            }
            return UIView()
        }
        return UIView()
    }
}

