//
//  SearchHomeVC.swift
//  DelivX
//
//  Created by 3Embed on 24/10/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SearchHomeVC: UIViewController {
    @IBOutlet weak var dummyView: UIView!
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    @IBOutlet weak var dishesTableView: UITableView!
    @IBOutlet weak var restuarantTableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContent: UIView!
    @IBOutlet weak var dishesButton: UIButton!
    @IBOutlet weak var restuarantButton: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var indicator: UIView!
    @IBOutlet weak var distance: NSLayoutConstraint!
    
    @IBOutlet weak var dishesTableViewLeading: NSLayoutConstraint!
    @IBOutlet weak var navigationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelBtnTrailingConstaraint: NSLayoutConstraint!
    @IBOutlet weak var cancelBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var searchHeaderView: UIView!
    @IBOutlet weak var NavigationWidth: NSLayoutConstraint!
    
    var restuarantEmptyView = EmptyView().shared
    var dishesEmptyView = EmptyView().shared
    var noProductsView = EmptyView().shared

    var searchVM = SearchVM()
    var lang = ""
    var productName = ""
    var selectIndex = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.restuarantTableView.reloadData()
            self.dishesTableView.reloadData()
            self.setBanner()
           // self.setUIforNaviagtion()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEmptyScreen()
        self.setupUI()
        didGet()
        DispatchQueue.main.async {
            self.setNavigation()
            self.searchTF.Semantic()
            self.searchTF.placeholder = StringConstants.Search()
            self.searchVM.getPopular()
           // self.setUIforNaviagtion()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.searchTF.becomeFirstResponder()
    }
    
    @IBAction func didTapRestuarants(_ sender: Any) {
        restuarantButton.setTitleColor(Colors.PrimaryText, for: .normal)
        dishesButton.setTitleColor(Colors.Unselector, for: .normal)
        if Utility.getLanguage().Code == "ar" {
            scrollToPage(page: 1)
        }else {
            scrollToPage(page: 0)
        }
    }
    
    @IBAction func didTapDishes(_ sender: Any){
        
        dishesButton.setTitleColor(Colors.PrimaryText, for: .normal)
        restuarantButton.setTitleColor(Colors.Unselector, for: .normal)
        if Utility.getLanguage().Code != "ar" {
            scrollToPage(page: 1)
        }else{
            scrollToPage(page: 0)
        }
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        searchTF.text = ""
        self.searchVM.loadPopular = false
        self.setUIforNaviagtion()
        searchTF.resignFirstResponder()
        searchVM.arrayOfItems.removeAll()
        searchVM.arrayOfStores.removeAll()
        restuarantTableView.reloadData()
        dishesTableView.reloadData()
    }
    
    func didGet(){
        searchVM.searchVM_response.subscribe(onNext: { [weak self]success in
//            self?.noProductsView.isHidden = false
//            self?.restuarantEmptyView.isHidden = false
//            self?.dishesEmptyView.isHidden = false
            switch success {
            case .ItemName:
                self?.restuarantTableView.reloadData()
                self?.dishesTableView.reloadData()
                self?.searchVM.loadPopular = false
                if (self?.searchVM.arrayOfItems.count)! > 0 || (self?.searchVM.recentSearchList.count)! != 0 {
                    self?.dishesTableView.backgroundView = nil
                }else{
                    self?.dishesTableView.backgroundView = self?.dishesEmptyView
                    self?.dishesEmptyView.isHidden = false
                }
                
                if (self?.searchVM.arrayOfStores.count)! > 0 {
                    self?.restuarantTableView.backgroundView = nil
                }else{
                    self?.restuarantTableView.backgroundView = self?.restuarantEmptyView
                    self?.restuarantEmptyView.isHidden = false
                }
            case .Recent:
                self?.searchVM.loadPopular = false
                self?.dishesTableView.reloadData()
                break
            case .Items:
                if self?.searchVM.arrayOfExpandedStore.Products.count == 1 {
                    if self?.searchVM.arrayOfItems.count == 0{
                        self!.searchTF.text = (self?.searchVM.recentSearchList[(self?.selectIndex)!])!
                        self?.performSegue(withIdentifier: UIConstants.SegueIds.navDetails, sender: self?.searchVM.arrayOfExpandedStore.Products[0])
                        self?.productName = (self?.searchVM.recentSearchList[(self?.selectIndex)!])!
                    }else{
                        self!.searchTF.text = (self?.searchVM.arrayOfItems[(self?.selectIndex)!])!
                        self?.performSegue(withIdentifier: UIConstants.SegueIds.navDetails, sender: self?.searchVM.arrayOfExpandedStore.Products[0])
                        self?.productName = (self?.searchVM.arrayOfItems[(self?.selectIndex)!])!
                    }
                }else{
                    if self?.searchVM.arrayOfItems.count == 0{
                        self?.searchTF.text = self?.searchVM.recentSearchList[(self?.selectIndex)!]
                        self?.performSegue(withIdentifier: String(describing: SearchDetailsVC.self), sender: self?.searchVM.recentSearchList[(self?.selectIndex)!])
                        self?.productName = (self?.searchVM.recentSearchList[(self?.selectIndex)!])!
                    }else{
                    self?.searchTF.text = self?.searchVM.arrayOfItems[(self?.selectIndex)!]
                    self?.performSegue(withIdentifier: String(describing: SearchDetailsVC.self), sender: self?.searchVM.arrayOfItems[(self?.selectIndex)!])
                    self?.productName = (self?.searchVM.arrayOfItems[(self?.selectIndex)!])!
                    }
                }
                break
            default:
                break
            }
            self?.setBanner()
            self?.setUIforNaviagtion()
        }).disposed(by: self.searchVM.disposeBag)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == UIConstants.SegueIds.SearchToItemDetails {
            if let itemDetailVC:ItemDetailVC = segue.destination as? ItemDetailVC {
                itemDetailVC.itemDetailVM.item = sender as! Item                      //searchVM.arrayOfItems[button.tag]
                //                itemDetailVC.transitioningDelegate = self
            }
        }else if segue.identifier == String(describing: SearchDetailsVC.self) {
            if let viewController: SearchDetailsVC = segue.destination as? SearchDetailsVC{
                viewController.dataString = sender as! String
                viewController.searchVM.productName = productName
                productName = ""
                viewController.languageIn = lang
            }
        }else if segue.identifier == UIConstants.SegueIds.navDetails {
            if let nav = segue.destination as? UINavigationController{
                if let itemDetailVC:ItemDetailVC = nav.viewControllers.first as? ItemDetailVC {
                    itemDetailVC.itemDetailVM.item = sender as! Item
                    itemDetailVC.searchVM.productName = productName
                }
            }
        }
    }
}
