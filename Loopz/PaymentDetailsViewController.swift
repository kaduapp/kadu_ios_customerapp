//
//  PaymentDetailsViewController.swift
//  LSP
//
//  Created by Rajan Singh on 13/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class PaymentDetailsViewController: UIViewController {
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var defaultView: UIView!
    @IBOutlet weak var defaultButton: UIButton!
    @IBOutlet weak var creditCardHead: UILabel!
    @IBOutlet weak var expiryDate: UILabel!
    @IBOutlet weak var validThru: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    var navView = NavigationView().shared

    var cardData:Card? = nil
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        setUPVIewModel()
        showCardDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Class Action Methods
    @IBAction func makeDefaultButtonAction(_ sender: Any) {
        Helper.showPI(string: "")
        AddNewCardVM().defaultCard(card: cardData!)
    }
    
    @IBAction func deleteCardButtonAction(_ sender: Any) {
        AddNewCardVM().deleteCard(card: cardData!)
    }
    
    
    func setUI() {
        Fonts.setPrimaryMedium(creditCardHead)
        Fonts.setPrimaryMedium(numberLabel)
        Fonts.setPrimaryRegular(validThru)
        Fonts.setPrimaryMedium(expiryDate)
        Fonts.setPrimaryMedium(defaultButton)
        Fonts.setPrimaryMedium(deleteButton)
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.Details())
        }
        creditCardHead.text = StringConstants.CardLower()
        validThru.text = StringConstants.ValidThru()
        Helper.setButtonTitle(normal: StringConstants.MakeAsDefault(), highlighted: StringConstants.MakeAsDefault(), selected: StringConstants.MakeAsDefault(), button: defaultButton)
        Helper.setButton(button: defaultButton, view: defaultView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor, shadow: true)
        Helper.setButtonTitle(normal: StringConstants.DeleteCaps(), highlighted: StringConstants.DeleteCaps(), selected: StringConstants.DeleteCaps(), button: deleteButton)
        deleteButton.tintColor = Colors.Red
    }
    
    //MARK: - custom Methods
    
    /// show cards detail
    func showCardDetails() {
        numberLabel.text = UIConstants.UIElement.Card + " " + cardData!.Last4
        expiryDate.text = cardData!.ExpMonth + "/" + cardData!.ExpYear
        creditCardHead.text = cardData?.Brand.uppercased()
    }
}

// MARK: - VMResponse
extension PaymentDetailsViewController {
    func setUPVIewModel() {
        AddNewCardVM.addNewCard_response.subscribe(onNext: { [weak self]success in
            self?.navigationController?.popViewController(animated: true)
        }, onError: { (error) in
            
        }, onCompleted: {
            
        }).disposed(by: CardAPICalls.disposeBag)
    }
}

