//
//  PopupTVE.swift
//  DelivX
//
//  Created by 3Embed on 21/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import Foundation
// MARK: - Tableview Delegate
extension PopupVC : UITableViewDelegate, UITableViewDataSource {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if selectedFlag {
                return 1
            }
            return 0
        }
        return popupVM.arrayOfList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CreateNewListCell.self), for: indexPath) as! CreateNewListCell
            cell.textField.becomeFirstResponder()
            cell.textField.text = ""
            return cell
        }
        var cell = UITableViewCell()
        cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.PopupCell, for: indexPath) as! PopupCell
        var data = popupVM.arrayOfList[indexPath.row] as? PopupModel
        if popupVM.type == 1 {
            if let temp = popupVM.selected[0] as? Int {
                if indexPath.row == temp {
                    data?.Flag = true
                }
            }
        }else if popupVM.type == 2 {
            let array = popupVM.selected as! [String]
            if array.contains((data?.ID)!) {
                data?.Flag = true
            }
        }
        let cellIn = cell as! PopupCell
        cellIn.setData(model: data!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if popupVM.type == 1 {
            popupVM.selected.removeAll()
            popupVM.selected.append(indexPath.row)
        }else{
            let data = popupVM.arrayOfList[indexPath.row] as! PopupModel
            var array = popupVM.selected as! [String]
            if array.contains(data.ID) {
                array.remove(at: array.index(of: data.ID)!)
            }else{
                array.append(data.ID)
            }
            popupVM.selected = array
        }
        mainTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 75
        }
        return 50
    }
}
