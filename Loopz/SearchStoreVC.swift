//
//  SearchStoreVC.swift
//  DelivX
//
//  Created by 3EMBED on 14/11/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit



class SearchStoreVC: UIViewController {
    @IBOutlet weak var dummyView: UIView!
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    @IBOutlet weak var restuarantTableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelBtnTrailingConstaraint: NSLayoutConstraint!
    @IBOutlet weak var cancelBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var searchHeaderView: UIView!
    @IBOutlet weak var NavigationWidth: NSLayoutConstraint!
    
    var restuarantEmptyView = EmptyView().shared
    var searchVM = SearchVM()
    var lang = ""
    var productName = ""
    var selectIndex = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.restuarantTableView.reloadData()
            self.setBanner()
        }
        searchTF.becomeFirstResponder()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEmptyScreen()
        self.setupUI()
        DispatchQueue.main.async {
            self.setNavigation()
            self.searchTF.Semantic()
            self.searchTF.placeholder = StringConstants.Search()
        }
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        searchTF.text = ""
        self.searchVM.loadPopular = true
        searchTF.resignFirstResponder()
        searchVM.arrayOfItems.removeAll()
        searchVM.arrayOfStores.removeAll()
        restuarantTableView.reloadData()
    }
}
