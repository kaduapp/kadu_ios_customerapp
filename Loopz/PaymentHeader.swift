//
//  PaymentHeader.swift
//  UFly
//
//  Created by 3 Embed on 16/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PaymentHeader: UIView {
    
    @IBOutlet weak var uflyMoneyView: UIView!
    @IBOutlet weak var uflyMoneyLabel: UILabel!
    @IBOutlet weak var addToWalletBtn: UIButton!
    @IBOutlet weak var passbookBtn: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var moneySeparator: UIView!
   
    var tempMoney =  " "
    override func awakeFromNib() {
        super.awakeFromNib()
        initalSetup()
    }
    
    /// initial view setup
    func initalSetup(){
        
        Fonts.setPrimaryMedium(uflyMoneyLabel)
        Fonts.setPrimaryMedium(passbookBtn)
        Fonts.setPrimaryMedium(addToWalletBtn)
        uflyMoneyLabel.textColor  =  Colors.PrimaryText
        Helper.setButtonTitle(normal:StringConstants.Passbook(), highlighted: StringConstants.Passbook(), selected: StringConstants.Passbook(), button: passbookBtn)
        Helper.setButtonTitle(normal: StringConstants.AddToWallet(), highlighted: StringConstants.AddToWallet(), selected: StringConstants.AddToWallet(), button: addToWalletBtn)
        
        addToWalletBtn.tintColor =  Colors.BlueColor
        passbookBtn.tintColor =  Colors.BlueColor
        
        moneySeparator.backgroundColor =  Colors.SeparatorDark
        Helper.setShadow(sender: self)
        uflyMoneyLabel.text = StringConstants.Wallet() + " " + tempMoney
        Helper.updateText(text: uflyMoneyLabel.text!, subText: tempMoney, uflyMoneyLabel, color: Colors.AppBaseColor,link: "")
        Helper.setUiElementBorderWithCorner(element:headerView, radius:2, borderWidth:0 , color:UIColor.clear)
    }
    
}
