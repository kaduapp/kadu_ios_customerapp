//
//  ItemDetailVCE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension ItemDetailVC:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let movedOffset: CGFloat = scrollView.contentOffset.y
        if scrollView == mainTableView {
            let pageWidth: CGFloat = 350          //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            if ratio > 0 {
                if self.navigationController != nil{
                    self.navView.titleLabel.textColor = Colors.PrimaryText
                    Helper.addShadowToNavigationBar(controller: self.navigationController!)
                }
            }else {
                if self.navigationController != nil{
                    self.navView.titleLabel.textColor = UIColor.clear
                    Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                }
            }
        }
        
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let cell = mainTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? ItemDetailTVC
        if scrollView == cell?.ImageCollectionView{
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            cell?.imagePageControll.currentPage = Int(pageNumber)
        }
        
    }

    

}


