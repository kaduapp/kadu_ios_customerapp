//
//  TotalItemsCell.swift
//  UFly
//
//  Created by 3 Embed on 07/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class TotalItemsCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource{
    
    
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var itemsView: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var seperator: UIView!
    
    @IBOutlet weak var addOnTableviewHeight: NSLayoutConstraint!
    @IBOutlet weak var addOnTableview: UITableView!
    @IBOutlet weak var qtyWithPrice: UILabel!
    
    var addOnGroups:[AddOnGroup] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    /// initial view setup
    func initialSetup() {
        Fonts.setPrimaryMedium(itemName)
        Fonts.setPrimaryRegular(weight)
        Fonts.setPrimaryRegular(qtyWithPrice)
        Fonts.setPrimaryRegular(itemPrice)
        itemName.textColor = Colors.PrimaryText
        weight.textColor = Colors.SecoundPrimaryText
        qtyWithPrice.textColor = Colors.SeconderyText
        itemPrice.textColor = Colors.PrimaryText
        seperator.backgroundColor = Colors.SeparatorLight
    }
    
    /// this method updates total number of items
    ///
    /// - Parameter data: is of type CartItem
    func updateItems(data: CartItem){
        itemName.text = data.ItemName
        itemPrice.text = Helper.df2so(Double(data.FinalPrice * Float(data.QTY)))
        weight.text = ""
        Helper.setImage(imageView:itemImage,url:data.ItemImageURL,defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        weight.text = data.UnitName
        let attrText1 = "\(data.QTY) * "
        let attrText2 = Helper.df2so(Double( data.FinalPrice))
      
        qtyWithPrice.text = "\(attrText1)\(attrText2)"
       addOnGroups = data.ItemCartAddonGroups
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.addOnGroups.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addOnGroups[section].AddOns.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddOnTableViewCell.self), for: indexPath) as! AddOnTableViewCell
        
            cell.setData(data: self.addOnGroups[indexPath.section], indexPath: indexPath)
        
        return cell
    }
    
}




