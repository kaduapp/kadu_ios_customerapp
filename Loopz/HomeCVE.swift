//
//  HomeCVE.swift
//  UFly
//
//  Created by 3 Embed on 12/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import Kingfisher

///UICollectionViewDelegate
extension HomeVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 1:
            if loading {
                return 5
            }
            return homeVM.items.count
        case 2:
            if loading {
                return 5
            }
            return homeVM.itemsLow.count
        case 3:
            if loading {
                return 5
            }
            return homeVM.itemsFav.count
        case 100:
            if loading {
                return 5
            }
            return homeVM.offers.count
        case 101:
            if loading {
                return 5
            }
            return homeVM.brands.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //for section 0 and 2
        if collectionView.tag == 100 {
            let cell: ItemImageCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.ItemImageCollectionCell, for: indexPath) as! ItemImageCollectionCell
            if loading == false && homeVM.offers.count - 1 >= indexPath.row {
                
                let trimmedString = homeVM.offers[indexPath.row].ImageURL.trimmingCharacters(in: .whitespaces)
                 cell.itemImage.kf.setImage(with: URL(string: trimmedString),
                                      placeholder: #imageLiteral(resourceName: "SingleShowDefault"),
                                      options: [.transition(ImageTransition.fade(1))],
                                      progressBlock: { receivedSize, totalSize in
                },
                                      completionHandler: { image, error, cacheType, imageURL in
                                        cell.itemImage.image = Helper.image(with: image, scaledToWidth: Float(collectionView.frame.size.width - 40))
                                      
                })

            }
            
            return cell
        }
        if collectionView.tag == 101 {
            let cell: ItemImageCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.ItemImageCollectionCell, for: indexPath) as! ItemImageCollectionCell
            if loading == false {
                Helper.setImage(imageView: cell.itemImage, url: homeVM.brands[indexPath.row].Logo, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
            }
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            
            return 10
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            
        }
        
        var cell: ItemCommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self), for: indexPath) as! ItemCommonCollectionViewCell
        if loading {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self) + "2" , for: indexPath) as! ItemCommonCollectionViewCell
        }else{
            var dataArray:[Item] = []
            switch collectionView.tag {
            case 1:
                dataArray = homeVM.items
                break
            case 2:
                dataArray = homeVM.itemsLow
                break
            case 3:
                dataArray = homeVM.itemsFav
                break
            default:
                dataArray = []
                break
            }
            cell.setValue(data:dataArray[indexPath.row], isDelete: false)
            cell.addToCartBtn.tag = indexPath.row
            cell.cartVM = self.cartVM
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        var cellWidth = (UIScreen.main.bounds.width-2)/2.2
        var cellHight = (collectionView.frame.size.height) - 4
        if collectionView.tag == 100 {

        cellWidth = collectionView.frame.size.width - 40
        cellHight = cellWidth*9/16
        }else if collectionView.tag == 101 {
            cellWidth = (collectionView.frame.size.width/3)+20
            cellHight = cellWidth
        }
        size.height =  cellHight
        size.width  =  cellWidth
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 100 {
            selectedOffer = indexPath.row
            performSegue(withIdentifier: String(describing: ViewAllVC.self), sender: 100)
            return
        }else if collectionView.tag == 101 {
            selectedOffer = indexPath.row
            performSegue(withIdentifier: String(describing: ViewAllVC.self), sender: 101)
            return
        }
        var sender:Item? = nil
        var dataArray:[Item] = []
        switch collectionView.tag {
        case 1:
            dataArray = homeVM.items
            break
        case 2:
            dataArray = homeVM.itemsLow
            break
        case 3:
            dataArray = homeVM.itemsFav
            break
        default:
            dataArray = []
            break
        }
        sender = dataArray[indexPath.row]
        selected = collectionView.cellForItem(at: indexPath)
        performSegue(withIdentifier:UIConstants.SegueIds.HomeToItemDetail, sender: sender)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionView.elementKindSectionFooter:
            if collectionView.tag != 100 && collectionView.tag != 101 {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: SeeAllFooter.self), for: indexPath) as! SeeAllFooter
                headerView.seeAllButton.tag = collectionView.tag
                headerView.seeAllButton.addTarget(self, action:#selector(HomeVC.viewAll) , for: UIControl.Event.touchUpInside)
                return headerView
            }
            let footerView = UICollectionReusableView()
            return footerView
        default :
            let footerView = UICollectionReusableView()
            return footerView
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        var size = CGSize.zero
        if collectionView.tag != 100 && collectionView.tag != 101
        {
            var cellWidth:CGFloat = 0
            
            switch collectionView.tag {
            case 1:
                if homeVM.items.count > 2{
                    cellWidth = (UIScreen.main.bounds.width-2)/2.2
                }
                break
            case 2:
                if homeVM.itemsLow.count > 2{
                    cellWidth = (UIScreen.main.bounds.width-2)/2.2
                }
                break
            case 3:
                if homeVM.itemsFav.count > 2{
                    cellWidth = (UIScreen.main.bounds.width-2)/2.2
                }
                break
            default:
                break
            }
            
            let cellHight = (collectionView.frame.size.height) - 4
            size.height =  cellHight
            size.width  =  cellWidth
        }
        return size
    }
}

