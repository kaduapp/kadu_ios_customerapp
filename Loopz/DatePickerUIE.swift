//
//  DatePickerUIE.swift
//  DelivX
//
//  Created by 3Embed on 29/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension DatePickerVC {
    func setUI() {
        
        self.heightConstraint.constant = 0
        Fonts.setPrimaryMedium(headLabel)
        headLabel.text = TitleString
        headLabel.textColor = Colors.AppBaseColor
        
        Helper.setButtonTitle(normal: StringConstants.Done(), highlighted: StringConstants.Done(), selected: StringConstants.Done(), button: doneButton)
        doneButton.setTitleColor(Colors.AppBaseColor, for: .normal)
        Fonts.setPrimaryBold(doneButton)
        
        Helper.setShadow(sender: titleView)
        
    datePicker.minimumDate = Date().addingTimeInterval(TimeInterval(Utility.getAppConfig().LaterBookingBuffer))//Date()
        datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 5, to: Date())
        
        if ModeDate {
            datePicker.datePickerMode = .date
        }else{
            datePicker.date = selectedDate

            datePicker.datePickerMode = .time
        }
       
    }
}
