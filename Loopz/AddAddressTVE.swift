//
//  AddAddressTVE.swift
//  UFly
//
//  Created by 3Embed on 15/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

// MARK: - Tableview Delegate
extension AddAddressVC : UITableViewDelegate, UITableViewDataSource {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.isHidden = true
        if addressList.count > 0 {
            tableView.isHidden = false
        }
        return addressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.LocationTableViewCell, for: indexPath) as! LocationTableViewCell
        let data = addressList[indexPath.row]
        cell.headLabel.text = data.LocationDescription
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let data = addressList[indexPath.row]
        
        
        if data.PlaceId.length != 0 {
         flagMapChange = false
         locationManager?.getAddressFromPlace(place: data)
        }
    }
    
    
    
}
