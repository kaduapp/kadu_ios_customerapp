//
//  SearchDetailsVC.swift
//  DelivX
//
//  Created by 3Embed on 04/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SearchDetailsVC: UIViewController {
    
    @IBAction func test(_ sender: Any) {
        print("test")
    }
    var languageIn = "en"
    
    static let searchVM_response = PublishSubject<([String],String)>()
    @IBOutlet weak var navigationWidth: NSLayoutConstraint!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchIcon: UIButton!
    @IBOutlet weak var storeView: UIView!
    @IBOutlet weak var selectStoreTitle: UILabel!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var filterButtonView: UIView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    @IBOutlet weak var StoreViewHeight: NSLayoutConstraint!
    
    
    
 
    @IBOutlet weak var viewCartBar: UIView!
    @IBOutlet weak var extraCharges: UILabel!
    @IBOutlet weak var viewCartBtn: UIButton!
    @IBOutlet weak var viewCartBarHight: NSLayoutConstraint!
    @IBOutlet weak var viewCartlabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var itemCount: UILabel!
  
    
    
    
    var dataString = ""
    
    var searchVM = SearchVM()
    //    let transition = PopAnimator()
    var selected:UICollectionViewCell? = nil
    
    var emptyView = EmptyView().shared
    var flagLoad = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        didGet()
        setStore()
        searchTF.Semantic()
        mainCollectionView.register(UINib(nibName: String(describing: ItemCommonCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self))
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       self.showBottom()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        self.title = dataString
        searchVM.getProductsWithinStore(text: dataString,language: languageIn)
        setUI()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: DispensariesVC.self) {
            if let _: DispensariesVC =  segue.destination as? DispensariesVC {
                self.title = "    "
            }
        
        }else if segue.identifier == UIConstants.SegueIds.navDetails {
            if let nav = segue.destination as? UINavigationController{
                if let itemDetailVC:ItemDetailVC = nav.viewControllers.first as? ItemDetailVC {
                    itemDetailVC.itemDetailVM.item = sender as! Item
                    itemDetailVC.viewCartHandler = {status in
                        self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
                    }
   
                }
            }
        }else if segue.identifier == String(describing: FilterContainerVC.self) {
            if let itemDetailVC:FilterContainerVC = segue.destination as? FilterContainerVC {
                itemDetailVC.text = self.dataString
                itemDetailVC.textLanguage = self.languageIn
                
            }
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func filterAction(_ sender: Any) {
        self.performSegue(withIdentifier: String(describing: FilterContainerVC.self), sender: self)
    }
    
    @IBAction func didTapViewCartBtn(_ sender: UIButton) {
        self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
    }
    
}

extension SearchDetailsVC {
    
    /// Observe for view model
    func didGet(){
        searchVM.searchVM_response.subscribe(onNext: { [weak self]success in
            switch success {
            case .Items:
                self?.flagLoad = true
                self?.mainCollectionView.reloadData()
                break
            default:
                break
            }
        }).disposed(by: self.searchVM.disposeBag)
        
        Utility.UtilityUpdate.subscribe(onNext: { [weak self]success in
            if success == Utility.TypeServe.ChangeStore {
                self?.searchVM.getProductsWithinStore(text: (self?.dataString)!,language: (self?.languageIn)!)
                self?.setStore()
            }
        }).disposed(by: self.searchVM.disposeBag)
        
        
        CartDBManager.cartDBManager_response.subscribe(onNext: { [weak self]success in
            self?.showBottom()
        }).disposed(by: self.searchVM.disposeBag)
        
        setObserver()
    }
    
    func setObserver() {
        searchVM.filterInfo.subscribe(onNext: { [weak self]data in
            if data.0 {
                switch data.1 {
                case APIRequestParams.BrandFilter :
                    if data.0 {
                        self?.searchVM.brandArray = data.2
                    }
                    break
                case APIRequestParams.ManufacturerFilter :
                    if data.0 {
                        self?.searchVM.manufacturerArray = data.2
                    }
                    break
                case APIRequestParams.CategoryFilter :
                    if data.0 {
                        if data.2.count == 0 {
                            self?.searchVM.selectedCategory = ""
                            self?.searchVM.selectedSubCategory = ""
                            self?.searchVM.selectedSubSubCategory = ""
                        }else{
                            self?.searchVM.selectedCategory = data.2[0]
                            self?.searchVM.selectedSubCategory = data.2[1]
                            self?.searchVM.selectedSubSubCategory = data.2[2]
                        }
                    }
                    break
                case APIRequestParams.SubSubCategoryFilter :
                    if data.0 {
                        if data.2.count == 0 {
                            self?.searchVM.selectedSubSubCategory = ""
                        }else{
                            self?.searchVM.selectedSubSubCategory = data.2[0]
                        }

                    }
                    break
                case APIRequestParams.FilterPrice :
                    if data.0 {
                        if data.2.count == 0 {
                            self?.searchVM.minPrice = 0
                            self?.searchVM.maxPrice = 0
                        }else{
                            self?.searchVM.minPrice = Float(data.2[0])!
                            self?.searchVM.maxPrice = Float(data.2[1])!
                        }

                    }
                    break
                case APIRequestParams.Sort :
                    if data.0 {
                        if data.2.count == 0 {
                            return
                        }
                        self?.searchVM.selectedSort = Int(data.2[0])!

                    }
                    break
                case APIRequestParams.FIlterOffer :
                    if data.0 {
                        if data.2.count == 0 {
                            return
                        }
                        self?.searchVM.offer = data.2[0]
    
                    }
                    break
                case "":
                    self?.searchVM.brandArray = []
                    self?.searchVM.manufacturerArray = []
                    self?.searchVM.selectedCategory = ""
                    self?.searchVM.selectedSubCategory = ""
                    self?.searchVM.selectedSubSubCategory = ""
                    self?.searchVM.offer = ""
                    self?.searchVM.minPrice = 0
                    self?.searchVM.maxPrice = 0
                    self?.searchVM.selectedSort = -1
        
                    break
                default:
                    break
                }
                 self?.searchVM.getProductsWithinStore(text: (self?.dataString)!,language: (self?.languageIn)!)
            }else{
                if self?.searchVM.selectedSubCategory != nil {
                    FilterDetailVM.filterArray_responce.onNext(((self?.searchVM.selectedCategory)!,(self?.searchVM.selectedSubCategory)!,(self?.searchVM.selectedSubSubCategory)!,(self?.searchVM.brandArray)!,(self?.searchVM.minPrice)!,(self?.searchVM.maxPrice)!,(self?.searchVM.manufacturerArray)!,(self?.searchVM.offer)!,(self?.searchVM.selectedSort)!,0,"","",(self?.dataString)!))
                }
            }
        }).disposed(by: self.searchVM.disposeBag)
    }
    
    
    func setStore() {
        let store = Utility.getStore()
        Helper.setImage(imageView: storeImage, url: store.Image, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        storeNameLabel.text = store.Name
        if store.Name == StringConstants.Store() || !self.searchVM.isFromItemList{
            StoreViewHeight.constant = 0
            filterButtonView.isHidden = true
        }else{
//            StoreViewHeight.constant = 40
//            filterButtonView.isHidden = false
            StoreViewHeight.constant = 0
            filterButtonView.isHidden = true
        }
    }
    
    @IBAction func viewAllAction(sender : UIButton!){
        performSegue(withIdentifier: String(describing: DispensariesVC.self), sender: self)
    }
    

    
    /// hides the bottom bar
    ///
    /// - Parameter sender: is of UIButton type
    func hideBottom(sender: UIButton!) {
        if sender == viewCartBtn {
            UIView.animate(withDuration: 0.50) {
                self.viewCartBarHight.constant = 0
               self.view.layoutIfNeeded()
            }
        }
    }
    
    
    
    /// shows bottomcort bar if cartcount is greater than 0
    func showBottom() {
        if AppConstants.CartCount > 1 {
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Items()
        }else{
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Item()
        }
        amount.text = Helper.df2so(Double( AppConstants.TotalCartPrice))
        var length = 0
        if AppConstants.CartCount > 0 {
            length = 50
        }
        UIView.animate(withDuration: 0.50) {
            self.viewCartBarHight.constant = CGFloat(length)
        }
    }
    
}


extension SearchDetailsVC {
    func setUI() {
        Helper.removeNavigationSeparator(controller: self.navigationController!,image: true)
        
        if mainCollectionView.contentOffset.y > 0 {
            Helper.setShadow(sender: storeView)
        }else{
            Helper.removeShadow(sender: storeView)
        }
        Fonts.setPrimaryMedium(storeNameLabel)
        Fonts.setPrimaryMedium(selectStoreTitle)
        storeNameLabel.textColor = Colors.PrimaryText
        selectStoreTitle.textColor = Colors.SeconderyText
        //        Helper.setButton(button: filterButton, view: filterButtonView, primaryColour: Colors.SecondBaseColor, seconderyColor: Colors.AppBaseColor, shadow: false)
        Fonts.setPrimaryMedium(filterButton)
        selectStoreTitle.text = StringConstants.StoreSelected().uppercased()
        Helper.setUiElementBorderWithCorner(element: storeImage, radius: Helper.setRadius(element: storeImage), borderWidth: 1, color: Colors.AppBaseColor)
        
        //title
        navigationWidth.constant = UIScreen.main.bounds.size.width - 80
        searchTF.text = dataString
        Helper.setUiElementBorderWithCorner(element: searchView, radius: 4, borderWidth: 0, color: Colors.SeparatorDark)
            
            viewCartBar.backgroundColor =  Colors.AppBaseColor
            viewCartlabel.text = StringConstants.ViewCart()
            extraCharges.text = StringConstants.ExtraCharges()
        }
}

extension SearchDetailsVC:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.closeAction(UIButton())
    }
}





    

    

