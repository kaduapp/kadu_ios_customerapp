//
//  SignupUIE.swift
//  UFly
//
//  Created by 3 Embed on 09/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension SignUpVC {
    
    /// initial setup
    func setNavigationBar(){
        
        navigationBackView.isHidden = true
        Fonts.setPrimaryMedium(titleText)
        titleText.textColor = Colors.PrimaryText
        /*
         navigationBackView.isHidden = true
         Helper.removeNavigationSeparator(controller: self.navigationController!,image: false)
         Fonts.setPrimaryMedium(titleText)
         titleText.textColor = Colors.PrimaryText
         
         navigationController?.navigationBar.backgroundColor = UIColor.clear
         navigationController?.navigationBar.isTranslucent = true
         navigationController?.view?.backgroundColor = UIColor.clear*/
        //        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    func setFirstResponder(){
        if (signUpView.mobileNumberTF.text?.sorted().count)! <= 3{
            signUpView.mobileNumberTF.becomeFirstResponder()
        }
        else {
            signUpView.nameTF.becomeFirstResponder()
        }
    }
    
    
    /// initial view setup
    func setUp() {
        signUpView.mobileNumberTF.text = String(describing: signupVM.authentication.CountryCode) + String(describing: signupVM.authentication.Phone)
        if (signUpView.mobileNumberTF.text?.sorted().count)! > signupVM.authentication.CountryCode.sorted().count {
            signUpView.phNumCheckMark.isHidden = false
        }else {
            signUpView.phNumCheckMark.isHidden = true
        }
        self.signUpView.mobileNumberTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.signUpView.emailTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.signUpView.nameTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.signUpView.passwordTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.signUpView.referralTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    
    
    
    //Mark:- Keyboard ANimation
    override func keyboardWillShow(_ notification: NSNotification){
        let kbSize = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.size.height, right: 0.0)
    
        self.signupButtonViewBottomConstraint.constant = kbSize.size.height
        self.signUpView.signUpScrollView.contentInset = contentInsets;
        self.signUpView.signUpScrollView.scrollIndicatorInsets = contentInsets;
    }
    
    override func keyboardWillHide(_ notification: NSNotification){
        self.signupButtonViewBottomConstraint.constant = 0
        self.signUpView.signUpScrollView.contentInset = UIEdgeInsets.zero;
        self.signUpView.signUpScrollView.scrollIndicatorInsets = UIEdgeInsets.zero;
    }
    
  
    
  
    
}

