//
//  HelpDetailTVE.swift
//  DelivX
//
//  Created by 3 Embed on 12/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

// MARK: - UITableViewDataSource
extension HelpDetailVC: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySupport.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.HelpTitleCell ) as! HelpTitleCell
        cell.updateCell(data: arraySupport[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: String(describing: WebVC.self), sender: arraySupport[indexPath.row])
    }
    
}
