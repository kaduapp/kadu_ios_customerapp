//
//  ViewAllVC.swift
//  DelivX
//
//  Created by 3Embed on 02/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ViewAllVC: UIViewController {
    
    @IBOutlet weak var viewCartBar: UIView!
    @IBOutlet weak var extraCharges: UILabel!
    @IBOutlet weak var viewCartBtn: UIButton!
    @IBOutlet weak var viewCartBarHight: NSLayoutConstraint!
    @IBOutlet weak var viewCartlabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var itemCount: UILabel!
    
    var searchVM = SearchVM()
    var filterDetailModel = FilterDetailVM()
    var viewAllVM = ViewAllVM()
    var cartVM = CartVM()
    var offerId = ""
    var selected:UICollectionViewCell? = nil
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var sortButton: UIButton!
    
    
    @IBOutlet weak var navViewWidth: NSLayoutConstraint!
    @IBOutlet weak var searchBarButton: UIButton!
    @IBOutlet weak var navTitle: UILabel!
    
    let transition = PopAnimator()
    var loading = true
    var refreshControl: UIRefreshControl!
    var isGuestLogin = false
    
//    let transition = PopAnimator()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        cartVM.getCart()
        setObserver()
        self.title = "     "
        Helper.setButton(button: sortButton, view: sortView, primaryColour: Colors.SecondBaseColor, seconderyColor: Colors.AppBaseColor, shadow: false)
        Helper.setButton(button: filterButton, view: filterView, primaryColour: Colors.SecondBaseColor, seconderyColor: Colors.AppBaseColor, shadow: false)
        self.navTitle.text = viewAllVM.titleLabel
        getVM()
        viewCartBar.backgroundColor =  Colors.AppBaseColor
        viewCartlabel.text = StringConstants.ViewCart()
        extraCharges.text = StringConstants.ExtraCharges()
        if viewAllVM.type == 1 {
            searchVM.searchFrom = 1
        }else {
            searchVM.searchFrom = 5
        }
        if viewAllVM.type == 2 {
            searchVM.offer = "offer"
        }
        if (viewAllVM.type == 2 || viewAllVM.type == 3) && offerId.length > 0 {
            viewAllVM.index = 0
            viewAllVM.getData(id: offerId)
        }
        // Do any additional setup after loading the view.
        
        navViewWidth.constant = UIScreen.main.bounds.width - 100
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ViewAllVC.refresh), for: UIControl.Event.valueChanged)
        mainCollectionView.addSubview(refreshControl)
        
        mainCollectionView.register(UINib(nibName: String(describing: ItemCommonCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self))
        
        if !Utility.getLogin(){
            isGuestLogin = true
        }
    }
    @objc func refresh() {
        viewAllVM.index = 0
        viewAllVM.getData(id: offerId)
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            self.refreshControl.endRefreshing()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Utility.getLogin() && isGuestLogin{
            cartVM.getCart()
            isGuestLogin = false
        }
        self.showBottom()
        
    }
    
    func getVM() {
        viewAllVM.response_ViewAllVM.subscribe(onNext: { [weak self]data in
            self?.mainCollectionView.reloadData()
        }).disposed(by: self.viewAllVM.disposeBag)
        
        CartDBManager.cartDBManager_response.subscribe(onNext: { [weak self]success in
            self?.showBottom()
        }).disposed(by: self.viewAllVM.disposeBag)
    }
    
    @IBAction func didTapViewCartBtn(_ sender: UIButton) {
        self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
    }
    
    @IBAction func filterButtonAction(_ sender: Any) {
        self.performSegue(withIdentifier: String(describing: FilterContainerVC.self), sender: self)
    }
    
    
    func setTheAlphaToNavigationViewsOnScroll(_ offset:CGFloat){
        self.searchBarButton.alpha = offset
        self.navTitle.alpha = offset
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: ItemDetailVC.self) {
            let button = sender as! Item
            if let nav:UINavigationController = segue.destination as? UINavigationController {
                if let itemDetailVC: ItemDetailVC = nav.viewControllers.first as? ItemDetailVC{
//                itemDetailVC.transitioningDelegate = self
                    itemDetailVC.itemDetailVM.item = button
                    itemDetailVC.viewCartHandler = {status in
                    self.performSegue(withIdentifier: String(describing: CartVC.self), sender: self)
                    }
                }
            }
        }else if segue.identifier == String(describing: FilterContainerVC.self) {
            if let itemDetailVC: FilterContainerVC = segue.destination as? FilterContainerVC {
                if viewAllVM.type == 2 {
                    itemDetailVC.context = 2
                }else if viewAllVM.type == 1 {
                    itemDetailVC.context = 3
                }else if viewAllVM.type == 3 {
                    itemDetailVC.context = 4
                }
                itemDetailVC.searchVM = self.searchVM
                itemDetailVC.filterDetailRef = self.filterDetailModel
                itemDetailVC.textLanguage = Utility.getLanguage().Code

            }
        }
    }
}

extension ViewAllVC {
    /// shows bottomcort bar if cartcount is greater than 0
    func showBottom() {
        if AppConstants.CartCount > 1 {
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Items()
        }else{
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Item()
        }
        amount.text = Helper.df2so(Double( AppConstants.TotalCartPrice))
        var length = 0
        if AppConstants.CartCount > 0 {
            length = 50
        }
        UIView.animate(withDuration: 0.50) {
            self.viewCartBarHight.constant = CGFloat(length)
            self.view.layoutIfNeeded()
        }
    }
    
    /// hides the bottom bar
    ///
    /// - Parameter sender: is of UIButton type
    func hideBottom(sender: UIButton!) {
        UIView.animate(withDuration: 0.50) {
            self.viewCartBarHight.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    
    func setObserver() {
        searchVM.searchVM_response.subscribe(onNext: { [weak self]success in
            switch success {
            case .Items:
                self?.mainCollectionView.reloadData()
                break
            default:
                break
            }
        }).disposed(by: self.viewAllVM.disposeBag)
        
        searchVM.filterInfo.subscribe(onNext: { [weak self]data in
            if data.0 {
                switch data.1 {
                case APIRequestParams.BrandFilter :
                    if data.0 {
                        self?.searchVM.brandArray = (self?.filterDetailModel.selectedBrands)!
                        if self?.searchVM.brandArray.count == 0 && self?.searchVM.manufacturerArray.count == 0 && self?.searchVM.minPrice != 0 && self?.searchVM.maxPrice == 0 && self?.searchVM.offer == "" && self?.searchVM.selectedSort == -1 {
                            self?.searchVM.arrayOfExpandedStore  = Utility.getStore()
                            self?.searchVM.arrayOfStores = []
                            self?.mainCollectionView.reloadData()
                            return
                        }
                    
                    }
                    break
                case APIRequestParams.ManufacturerFilter :
                    if data.0 {
                        self?.searchVM.manufacturerArray = (self?.filterDetailModel.selectedManufacturer)!
                        if self?.searchVM.brandArray.count == 0 && self?.searchVM.manufacturerArray.count == 0 && self?.searchVM.minPrice != 0 && self?.searchVM.maxPrice == 0 && self?.searchVM.offer == "" && self?.searchVM.selectedSort == -1 {
                            self?.searchVM.arrayOfExpandedStore  = Utility.getStore()
                            self?.searchVM.arrayOfStores = []
                            self?.mainCollectionView.reloadData()
                            return
                        }

                    }
                    break
                case APIRequestParams.CategoryFilter :
                    if data.0 {
                        if self?.filterDetailModel.selectedCategories.count == 0 {
                            self?.searchVM.selectedCategory = ""
                            self?.searchVM.selectedSubCategory = ""
                            self?.searchVM.selectedSubSubCategory = ""
                        }else{
                            self?.searchVM.selectedCategory = (self?.filterDetailModel.selectedCategories[0])!
                            self?.searchVM.selectedSubCategory = ""
                            self?.searchVM.selectedSubSubCategory = ""
                        }
                        if self?.searchVM.brandArray.count == 0 && self?.searchVM.manufacturerArray.count == 0 && self?.searchVM.minPrice != 0 && self?.searchVM.maxPrice == 0 && self?.searchVM.offer == "" && self?.searchVM.selectedSort == -1 {
                            self?.searchVM.arrayOfExpandedStore  = Utility.getStore()
                            self?.searchVM.arrayOfStores = []
                            self?.mainCollectionView.reloadData()
                            return
                        }
                    }
                    break
                case APIRequestParams.FilterPrice :
                    if data.0 {
                        self?.searchVM.minPrice = (self?.filterDetailModel.selectedMinValue)!
                        self?.searchVM.maxPrice = (self?.filterDetailModel.selectedMaxValue)!
                        if self?.searchVM.brandArray.count == 0 && self?.searchVM.manufacturerArray.count == 0 && self?.searchVM.minPrice != 0 && self?.searchVM.maxPrice == 0 && self?.searchVM.offer == "" && self?.searchVM.selectedSort == -1 {
                            self?.searchVM.arrayOfExpandedStore  = Utility.getStore()
                            self?.searchVM.arrayOfStores = []
                            self?.mainCollectionView.reloadData()
                            return
                        }

                    }
                    break
                case APIRequestParams.Sort :
                    if data.0 {
                        
                        self?.searchVM.selectedSort = (self?.filterDetailModel.selectedSort)!
                        if self?.searchVM.brandArray.count == 0 && self?.searchVM.manufacturerArray.count == 0 && self?.searchVM.minPrice != 0 && self?.searchVM.maxPrice == 0 && self?.searchVM.offer == "" && self?.searchVM.selectedSort == -1 {
                            self?.searchVM.arrayOfExpandedStore  = Utility.getStore()
                            self?.searchVM.arrayOfStores = []
                            self?.mainCollectionView.reloadData()
                            return
                        }
                   
                    }
                    break
                case APIRequestParams.FIlterOffer :
                    if data.0 {
                        if data.2.count == 0 {
                            return
                        }
                        self?.searchVM.offer = data.2[0]
                        if self?.searchVM.brandArray.count == 0 && self?.searchVM.manufacturerArray.count == 0 && self?.searchVM.minPrice != 0 && self?.searchVM.maxPrice == 0 && self?.searchVM.offer == "" && self?.searchVM.selectedSort == -1 {
                            self?.searchVM.arrayOfExpandedStore  = Utility.getStore()
                            self?.searchVM.arrayOfStores = []
                            self?.mainCollectionView.reloadData()
                            return
                        }
   
                    }
                    break
                case "":
                    self?.searchVM.brandArray = []
                    self?.searchVM.manufacturerArray = []
                    self?.searchVM.selectedCategory = ""
                    self?.searchVM.selectedSubCategory = ""
                    self?.searchVM.selectedSubSubCategory = ""
                    self?.searchVM.offer = ""
                    self?.searchVM.minPrice = 0
                    self?.searchVM.maxPrice = 0
                    self?.searchVM.selectedSort = -1
                    if self?.searchVM.brandArray.count == 0 && self?.searchVM.manufacturerArray.count == 0 && self?.searchVM.minPrice == 0 && self?.searchVM.maxPrice == 0 && self?.searchVM.offer == "" && self?.searchVM.selectedSort == -1 {
                        self?.searchVM.arrayOfExpandedStore  = Utility.getStore()
                        self?.searchVM.arrayOfStores = []
                        self?.mainCollectionView.reloadData()
                        return
                    }
                    break
                default:
                    break
                    
                }
                
                 self?.searchVM.filterProductsWithinStore(text: "", language: Utility.getLanguage().Code)
            }else{
                if self?.searchVM.selectedSubCategory != nil {
                    FilterDetailVM.filterArray_responce.onNext(((self?.searchVM.selectedCategory)!,(self?.searchVM.selectedSubCategory)!,(self?.searchVM.selectedSubSubCategory)!,(self?.searchVM.brandArray)!,(self?.searchVM.minPrice)!,(self?.searchVM.maxPrice)!,(self?.searchVM.manufacturerArray)!,(self?.searchVM.offer)!,(self?.searchVM.selectedSort)!,0,"","",""))
                }
            }
        }).disposed(by: self.viewAllVM.disposeBag)
    }
}
