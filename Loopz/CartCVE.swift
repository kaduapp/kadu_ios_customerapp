//
//  CartCVE.swift
//  UFly
//
//  Created by 3 Embed on 20/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

///UICollectionViewDelegate
extension CartVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if cartVM.arrayOfCart.count == 0 {
            if load == true {
                collectionView.backgroundView = emptyView
            }
            return 0
        }else{
            collectionView.backgroundView = nil
            return cartVM.arrayOfCart.count + 1
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section <= cartVM.arrayOfCart.count - 1 {
            return cartVM.arrayOfCart[section].Products.count
        }
        var tax = 2
        if cartVM.arrayOfTax.count > 0 {
            tax = 3
        }
        return cartVM.arrayOfTax.count + tax
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section <= cartVM.arrayOfCart.count - 1 {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.CartCollectionCell, for: indexPath) as? CartCollectionCell
            {
                cell.setupData(data: cartVM.arrayOfCart[indexPath.section].Products[indexPath.row])
                cell.observeBack.subscribe(onNext: { data in
                    Helper.showPI(string: "")
                }).disposed(by: self.cartVM.disposeBag)
                return cell
            }
            return UICollectionViewCell()
            //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.CartCollectionCell, for: indexPath) as! CartCollectionCell
//            cell.addOnTableHeightFromcartVc.constant = CGFloat(cartVM.arrayOfCart[indexPath.section].Products[indexPath.row].Addons.count * 20)
           
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TaxCell.self), for: indexPath) as! TaxCell
            cell.leftConstraint.constant = 20
            cell.updateConstraints()
            cell.layoutIfNeeded()
            if indexPath.row == 0 {
                cell.topSeparator.isHidden = true
                var data = Tax.init(data: [:])
                data.Price = AppConstants.TotalCartPrice
                data.Name = StringConstants.SubTotal()
                cell.setData(data: data)
            } else if indexPath.row == 1 {
                cell.topSeparator.isHidden = false
                cell.setSavings()
                
            }else if indexPath.row == 2 {
                cell.topSeparator.isHidden = false
                cell.value.text = ""
                cell.title.text = StringConstants.Taxes().uppercased()
                Fonts.setPrimaryMedium(cell.title)
            }else{
                cell.topSeparator.isHidden = true
                //                cell.leftConstraint.constant = 40
                cell.updateConstraints()
                cell.layoutIfNeeded()
                cell.setData(data: cartVM.arrayOfTax[indexPath.row - 3])
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize.zero
        let cellWidth = UIScreen.main.bounds.width
        var cellHight = 0
        if indexPath.section <= cartVM.arrayOfCart.count - 1 {
         if cartVM.arrayOfCart[indexPath.section].Products[indexPath.row].Addons.count > 0{
             cellHight =   120
//            cellHight = (cartVM.arrayOfCart[indexPath.section].Products[indexPath.row].Addons.count * 25) + 112
            }
            else{
            cellHight =   90
            }
        }else{
            cellHight = 40
        }
        size.height =  CGFloat(cellHight)
        size.width  =  cellWidth
        return size
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if Utility.getSelectedSuperStores().type != .Restaurant {
            if indexPath.section <= cartVM.arrayOfCart.count - 1 {
                if self.cartVM.arrayOfCart.count > 0{
                    let cart = self.cartVM.arrayOfCart[indexPath.section]
                    if cart.Products.count > 0{
                        let item = cart.Products[indexPath.row]
                        let itemModel = Item.init(data:item)
                        self.selected = collectionView.cellForItem(at: indexPath)
                        self.performSegue(withIdentifier: UIConstants.SegueIds.CartToItemDetail, sender: itemModel)
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            if indexPath.section <= cartVM.arrayOfCart.count - 1 {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: UIConstants.CellIds.CartSectionHeaderCell, for: indexPath) as! CartSectionHeaderView
                headerView.setupData(data: cartVM.arrayOfCart[indexPath.section])
                headerView.isHidden = false
                if indexPath.section == 0 {
                    headerView.headerView.isHidden = false
                }else{
                    headerView.headerView.isHidden = true
                }
                return headerView
            }else{
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: UIConstants.CellIds.CartSectionHeaderCell, for: indexPath) as! CartSectionHeaderView
                headerView.isHidden = true
                return headerView
            }
        default :
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: CartSectionFooterView.self), for: indexPath) as! CartSectionFooterView
            footerView.addNotesTextview.tag = indexPath.section
            footerView.setExtraNotes(notes: AppConstants.extraNotes[self.cartVM.arrayOfCart[indexPath.section].StoreId] ?? "")
            return footerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        var size = CGSize.zero
        let cellWidth = UIScreen.main.bounds.width
        var cellHight = 0
        if section <= cartVM.arrayOfCart.count - 1 {
            cellHight = 80
        }else{
            cellHight = 0
        }
        if section == 0 && !Changebles.isGrocerOnly {
            cellHight = 130
        }
        size.height =  CGFloat(cellHight)
        size.width  =  cellWidth
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        var size = CGSize.zero
        let cellWidth = UIScreen.main.bounds.width
        let cellHight = 80
        size.height =  CGFloat(cellHight)
        if section == cartVM.arrayOfCart.count{
            size.height =  CGFloat(0)
        }
        size.width  =  cellWidth
        return size
    }
    
   
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        let dataItem = cartVM.arrayOfCart[indexPath.section].Products[indexPath.row]
        cartVM.arrayOfCart[indexPath.section].Products.remove(at: indexPath.row)
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred()
        collectionView.deleteItems(at: [indexPath])
       self.cartVM.deleteCartInCartVC(cartItem:dataItem)
    }
}

extension CartVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainCollectionView {
            if self.navigationController != nil {
                if scrollView.contentOffset.y > 0 {
                    Helper.addShadowToNavigationBar(controller: self.navigationController!)
                    UIView.animate(withDuration: 0.25) {
                        self.checkoutTopConstarint.constant = 0
                        self.view.layoutIfNeeded()
                    }
                }else{
                    Helper.removeShadowToNavigationBar(controller: self.navigationController!)
                    UIView.animate(withDuration: 0.25) {
                        self.showCheckoutView()
                        self.view.layoutIfNeeded()
                    }
                }
            }
            var offset = scrollView.contentOffset.y
            if offset > 30{
                offset = 1
            }else{
                offset = 0
            }
            //self.topTitleLabel.alpha = offset
             navView.titleLabel.alpha = offset
            //self.bottomConstraintsCollectionView.constant = 0
            
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {

        UIView.animate(withDuration: 0.25) {
            self.checkoutTopConstarint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // self.checkoutTopConstarint.constant = 155
        self.showCheckoutView()
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.showCheckoutView()
        if mainCollectionView.contentOffset.y >= (mainCollectionView.contentSize.height - mainCollectionView.frame.size.height) {
        }
    }
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        
    }
    
}
