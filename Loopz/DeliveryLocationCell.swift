//
//  DeliveryLocationCell.swift
//  DelivX
//
//  Created by 3EMBED on 20/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
protocol CellDelegate: class  {
    func didTapButton(_ sender: UIButton)
}
class DeliveryLocationCell: UITableViewCell {
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var workaddressLabel: UILabel!
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var addressButton: UIButton!
     var delegate: CellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        Fonts.setPrimaryRegular(workaddressLabel)
        workaddressLabel.textColor = Colors.lightBlueBlack
        iconButton.tintColor = Colors.PrimaryText
        workaddressLabel.adjustsFontSizeToFitWidth = true

    }

    @IBAction func addressButtonClicked(_ sender: Any) {
        delegate?.didTapButton(addressButton)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
     // Updating Same Button with Image
    func updateselectButtonImage(devileryAddres: Address ,PickupAddress:Address)  {
        
        if devileryAddres.Latitude == PickupAddress.Latitude && devileryAddres.Longitude == PickupAddress.Longitude {
            addressButton.setImage(UIImage(named: "CheckBoxOn") , for: .normal)
        }else
        {
            addressButton.setImage(UIImage(named: "CheckBoxOff") , for: .normal)
        }
        
        self.setAddressTagTitleIcon(data:devileryAddres)
    }
    func updateAddress(data:Address){
        
        var address = ""
        if data.FlatNo.length > 0{
            address = data.FlatNo + " "
        }
        if data.AddLine1.length > 0{
            address = address + "\(data.AddLine1 ),"
        }
        if data.AddLine2.length > 0{
            address = address + "\(data.AddLine2 ),"
        }
        if data.City.length > 1{
            address = address + "\(data.City ),"
        }
        if data.State.length > 1{
            address = address + "\(data.State )-"
        }
        if data.Zipcode.length > 0{
            address = address + "\(data.State ),"
        }
        if data.Country.length > 0{
            address = address + "\(data.Country )"
        }
        workaddressLabel.text = address
        self.setAddressTagTitleIcon(data:data)
       
    }
    
    func setAddressTagTitleIcon(data : Address){
        
        switch data.Tag {
        case "Home":
            let image = UIImage(named: "HomeAddress")?.withRenderingMode(.alwaysTemplate)
            
            iconButton.setImage(image, for: .normal)
        case "Work":
            let image = UIImage(named: "WorkAddress")?.withRenderingMode(.alwaysTemplate)
            
            iconButton.setImage(image, for: .normal)
        default:
            let image = UIImage(named: "OtherAddress")?.withRenderingMode(.alwaysTemplate)
            
            iconButton.setImage(image, for: .normal)
            
        }
        cellTitleLabel.text = data.Tag
        iconButton.tintColor = Colors.lightBlueBlack
    }

}
