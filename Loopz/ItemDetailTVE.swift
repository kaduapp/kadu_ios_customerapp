//
//  ItemDetailTVE.swift
//  UFly
//
//  Created by 3 Embed on 18/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - UITableViewDataSource
extension ItemDetailVC : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if Utility.getSelectedSuperStores().type != .Restaurant {
            return 4 + itemDetailVM.item.Effects.count
        }else{
            return 3 + itemDetailVM.item.Effects.count
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            switch section {
            case 0: return 1
            case 1: return 1
            case 2:
                if itemDetailVM.item.Ingredients.length > 0 {
                    return 1
                }else{
                    return 0
                }
            case 3:
                if Utility.getSelectedSuperStores().type == .Restaurant {
                    return 0
                }else{
                    return 1
                }
            default:
                return itemDetailVM.item.Effects[section - 4].0.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:UIConstants.CellIds.ItemDetailTableCell) as! ItemDetailTVC
            cell.instructionBtn.addTarget(self, action: #selector(ItemDetailVC.instructionBtnTapped), for: .touchUpInside)
            cell.favButtonClick.addTarget(self, action: #selector(ItemDetailVC.favBtnTapped), for: .touchUpInside)
            cell.addtoListClick.addTarget(self, action: #selector(ItemDetailVC.addToListBtnTapped), for: .touchUpInside)
            cell.isFromItemList = self.searchVM.isFromItemList
            cell.updateCell(data: itemDetailVM.item)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.DescriptionTableCell ) as! DescriptionTVC
            cell.detailArrowBtn.tag = indexPath.row
            cell.detailArrowBtn.addTarget(self, action: #selector(ItemDetailVC.detailArrowBtnTapped), for: .touchUpInside)
            cell.updateCell(data: itemDetailVM.item, flag: itemDetailVM.didTapDownArrow)
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.DescriptionTableCell ) as! DescriptionTVC
            cell.setIngredients(data: itemDetailVM.item)
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.DescriptionTableCell ) as! DescriptionTVC
            cell.setDesclimer(data: StringConstants.DesclimerData())
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.EffectsTableCell) as! EffectsTVC
            cell.setData(data: itemDetailVM.item.Effects[indexPath.section - 4].0[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var stringHead = ""
        switch section {
        case 0:
            stringHead = ""
            break
        case 1:
            stringHead = StringConstants.Discription()
            break
        case 2:
            stringHead = StringConstants.Ingredients()
            break
        case 3:
            stringHead = StringConstants.Desclimer()
            break
        default:
            stringHead = itemDetailVM.item.Effects[section - 4].1
        }
        let headView = Helper.showHeader(title: stringHead,showViewAll: false) as! HeaderView
        return headView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        if section == 2 && itemDetailVM.item.Ingredients.length == 0 {
            return 0
        }
        if Utility.getSelectedSuperStores().type == .Restaurant {
            if section == 3{
                return 0
            }
        }
            return 50
    }
}

