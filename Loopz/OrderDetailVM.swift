//
//  OrderDetailVM.swift
//  UFly
//
//  Created by 3 Embed on 12/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class OrderDetailVM {

    let orderDetailVM_response = PublishSubject<Bool>()
    var selectedOrder:Order? = nil
     var ispastOrder = false
    let dispose = DisposeBag()
    /// Observe for the OrderDetail API Defined in HistoryAPI Call
    func getOrderDetail() {
        HistoryAPICalls.getOrderDetail(data: selectedOrder!).subscribe(onNext: {[weak self] success in
         self?.selectedOrder = success
            self?.orderDetailVM_response.onNext(true)
            
        }).disposed(by: dispose)
    }
    
    
    func cancelOrder(reason:String){
        HistoryAPICalls.cancelOrder(id:(selectedOrder?.BookingId)!,reason: reason).subscribe(onNext: {[weak self] success in
            if success {
                self?.orderDetailVM_response.onNext(true)
            }
        }).disposed(by: dispose)
    }

}
