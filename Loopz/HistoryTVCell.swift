//
//  HistoryTVCell.swift
//  History
//
//  Created by 3Embed on 06/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class HistoryTVCell: UITableViewCell
{
    @IBOutlet weak var StoreTypeLBBackgroundView: UIView!
    @IBOutlet weak var storeTypeLB: UILabel!
    @IBOutlet weak var storeNameLB: UILabel!
    @IBOutlet weak var addressLB: UILabel!
    @IBOutlet weak var orderLB: UILabel!
    @IBOutlet weak var deliveryLB: UILabel!
    
    @IBOutlet weak var priceLB: UILabel!
    @IBOutlet weak var deliveryPriceLB: UILabel!
    @IBOutlet weak var heightOfFirstView: NSLayoutConstraint!
    
    var constraintReduced = false
    

    
    func configureCell(data:OrderDetails,indexPath:IndexPath,inTheSameStore:Bool)
    {
        if let storeType = data.storeType
        {
            self.storeTypeLB.text = storeType
        }
        if let storeName = data.storeName
        {
            self.storeNameLB.text = storeName
        }
        if let add = data.address
        {
            self.addressLB.text = add
        }
        if let price = data.price
        {
            self.priceLB.text = price
        }
        if let order = data.orderId
        {
            self.orderLB.text = order
        }
        if let delivery = data.deliveryFee
        {
            self.deliveryPriceLB.text = delivery
        }
        if let color = data.storeLBBackgroundColor
        {
            self.StoreTypeLBBackgroundView.backgroundColor = color
        }
        
        if indexPath.row == 0
        {
            
            if constraintReduced
            {
                updateTheReducedConstraints()
            }
        }
        
        
        
        if indexPath.row >= 1
        {
            if inTheSameStore
            {
               if !constraintReduced
               {
                    updateTheReducedConstraints()
                }
                
            }
            else
            {
                if constraintReduced,!inTheSameStore
                {
                    updateTheReducedConstraints()
                }
            }
        }
    }
    
    func updateTheReducedConstraints()
    {
        if constraintReduced
        {
            self.heightOfFirstView.constant = 45
        }
        else
        {
            self.heightOfFirstView.constant = 0
        }
        self.StoreTypeLBBackgroundView.isHidden = !constraintReduced
        self.storeTypeLB.isHidden = !constraintReduced
        self.constraintReduced = !constraintReduced
        self.updateConstraints()
        self.layoutIfNeeded()
    }
    
}
