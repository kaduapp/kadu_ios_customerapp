//
//  CreateNewListVC.swift
//  DelivX
//
//  Created by 3 Embed on 02/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CreateNewListVC: UIViewController {
    
    @IBOutlet weak var createNewListView:CreateNewListView!
   
    let createNewListVM = CreateNewListVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        didUpdateList()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
       Helper.setShadow(sender: createNewListView)
        createNewListView.layer.cornerRadius = 4
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
    }
    
    /// Dismiss Keyboard
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.dismissKeyboard()
        }) { (Bool) in
            UIView.animate(withDuration: 0.3, animations: {
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    @IBAction func doneAction(_ sender: Any) {
        
        guard createNewListView.newListNameTF.text?.length != 0 else {
            Helper.showAlert(message: StringConstants.EnterListName(), head: StringConstants.Error(), type: 1)
            return
        }
        createNewListVM.updateList(listName:createNewListView.newListNameTF.text!)
    }
 
}

extension CreateNewListVC {
    /// observe to view model
    func didUpdateList(){
        CreateNewListVM.createNewListVM_response.subscribe(onNext: {[weak self]success in
            self?.closeAction(UIButton())
            
        }, onError: {error in
            print(error)
        }).disposed(by: self.createNewListVM.disposeBag)
    }
    
}
