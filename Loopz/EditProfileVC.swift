//
//  EditProfileVC.swift
//  UFly
//
//  Created by 3 Embed on 29/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class EditProfileVC: UIViewController {
    
    @IBOutlet var editProfileVIew: EditProfileView!
    
    
    var editProfileVM = EditProfileVM()
    var uploadImageModel  = UploadImageModel.shared
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
       Helper.PullToClose(scrollView: editProfileVIew.scrollView)
        Helper.pullToDismiss?.delegate = self
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.isNavigationBarHidden = false
        addObserverForLocal()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if editProfileVM.isSaveAction {

            editProfileVM.isSaveAction = false
            editProfileVM.isValidEmail = false
            editProfileVM.isValidPhone = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserverForLocal()
    }
    
    @IBAction func idCartdOrMMJCardAction(_ sender: UIButton) {
        
           // ImageChosenTag
         var flag = 0
        editProfileVM.btnTag = EditProfileVM.ImageType(rawValue: sender.tag)!
        var imageChosen:UIImage? = nil
        if editProfileVM.btnTag == .iDCard && editProfileVM.userData?.IdCard.Verified != false {
            imageChosen = editProfileVIew.pickedIDImage
        }else if editProfileVM.btnTag == .mmjCard && editProfileVM.userData?.IdCard.Verified != false {
            imageChosen = editProfileVIew.pickedMMJImage
        }else if editProfileVM.btnTag == .profilePic{
            imageChosen = editProfileVIew.pickedProfileImage
        }
        if imageChosen == nil {
            flag = 0
        }
        else {
            flag = 1
        }
        editProfileVM.pickerObj?.setProfilePic(controller: self , tag: flag)
        
    }

    @IBAction func logoutAction(_ sender: Any) {
        dismissKeyboard()
      //  editProfileVM.logout()
        
    }
    @IBAction func closeAction(_ sender: Any) {
        dismissKeyboard()
        
        editProfileVM.authentication.Email = editProfileVIew.emailTF.text!
        let phoneNumber = editProfileVIew.phoneNum.text?.replacingOccurrences(of: editProfileVM.authentication.CountryCode, with: "")
        editProfileVM.authentication.Phone = phoneNumber!
        editProfileVM.authentication.Name = editProfileVIew.nameTF.text!
        self.dismiss(animated: true, completion: nil)
        
        if editProfileVM.authentication.Email != editProfileVM.userData?.Email || editProfileVM.authentication.Phone != editProfileVM.userData?.Phone || editProfileVM.authentication.Name != editProfileVM.userData?.FirstName || editProfileVM.authentication.ProfilePic != editProfileVM.userData?.userProfilePic || editProfileVM.authentication.MMJUrl != editProfileVM.userData?.MMJCard.Url || editProfileVM.authentication.IDUrl != editProfileVM.userData?.IdCard.Url {
            self.editProfileVM.isSaveAction = true
            self.editProfileVM.validateData()

        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }

    
    @IBAction func countryPickerAction(_ sender: Any) {
        
        let vnhStoryboard = UIStoryboard(name: UIConstants.ControllerIds.VNHCountryPicker, bundle: nil)
        if let nav: UINavigationController = vnhStoryboard.instantiateInitialViewController() as! UINavigationController? {
            let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker
            // delegate
            picker.delegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    @IBAction func editAction(_ sender: Any) {
       
      performSegue(withIdentifier: UIConstants.SegueIds.EditProfileToEditVC, sender: self)
        
    }
    @IBAction func changePasswordAction(_ sender: Any) {
        performSegue(withIdentifier: UIConstants.SegueIds.EditProfileToChangePW, sender: self)
     
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == UIConstants.SegueIds.EditProfileToOTP {
            if let otpVC:OTPVC = segue.destination as? OTPVC {
                otpVC.otpVM.authentication = editProfileVM.authentication
                otpVC.otpVM.isFromUpdateProfile = true
            }
            if segue.identifier == UIConstants.SegueIds.EditProfileToEditVC {
                if let _:EditVC = segue.destination as? EditVC {
                    
                }
                
            }
        }
       
    }
}

extension EditProfileVC {
    
    /// observe for view model
    func didValidatePhone(){
        EditProfileVM.EditProfileVM_response.subscribe(onNext: {success in
            //self.subscribe(data: success)
        }, onError: { error in
            print(error)
        }).disposed(by: self.editProfileVM.disposeBag)
        APICalls.LogoutInfoTo.subscribe(onNext: { success in
            if success {
                self.dismiss(animated: true, completion: nil)
            }
        }).disposed(by: self.editProfileVM.disposeBag)
        uploadImageModel.UploadImageRespose.subscribe(onNext: { success in
            if success == .EditVC {
                  self.editProfileVM.validateData()
            }
        }).disposed(by: self.editProfileVM.disposeBag)

    }

    ///
    ///
    /// - Parameter data: it is of type enum response type
    func subscribe(data: EditProfileVM.ResponseType){
        switch data {
        case .validPhoneNum:
            editProfileVM.validateData()
            break
        case .Logout:
            Helper.changetTabTo(index: AppConstants.Tabs.Home)
            
        case .validEmail:
            editProfileVM.validateData()
            break
        case .updateProfile:
            if self.editProfileVM.isSaveAction {
                self.dismiss(animated: true, completion: nil)
            }
            break
        case .updatePhone:
            if editProfileVM.isValidPhone && editProfileVM.isValidEmail {
                performSegue(withIdentifier:  UIConstants.SegueIds.EditProfileToOTP, sender: self)
            }
            break
        }
    }
}

extension EditProfileVC{
    func addObserverForLocal(){
        NotificationCenter.default.addObserver(self,selector: #selector(viewMoveAccKeyboardInFilter(notification:)),name: UIResponder.keyboardWillShowNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(viewMoveToOriginalInFilter),name: UIResponder.keyboardWillHideNotification,object: nil)
    }
    
    func removeObserverForLocal(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func viewMoveAccKeyboardInFilter(notification:NSNotification){
        let kbSize = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.size.height, right: 0.0)
        self.editProfileVIew.scrollView.contentInset = contentInsets;
        self.editProfileVIew.scrollView.scrollIndicatorInsets = contentInsets;
    }
    
    @objc  func viewMoveToOriginalInFilter(){
        self.editProfileVIew.scrollView.contentInset = UIEdgeInsets.zero;
        self.editProfileVIew.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero;
    }
}
