//
//  UpdatePasswordVCE.swift
//  DelivX
//
//  Created by 3 Embed on 20/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension UpdatePasswordVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
        let movedOffset: CGFloat = sender.contentOffset.y
        print(movedOffset)
        if sender == newPWView.scrollView {
            if movedOffset <= 0 {
                sender.contentOffset.y = 0
            }
            
        }
    }
    
}
