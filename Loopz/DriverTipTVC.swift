//
//  DriverTipTVC.swift
//  DelivX
//
//  Created by 3Embed on 30/05/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class DriverTipTVC: UITableViewCell {

    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var tipCV: UICollectionView!
    @IBOutlet weak var titleLable: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialViewSetup()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initialViewSetup() {
        Helper.setUiElementBorderWithCorner(element: driverImage, radius: Helper.setRadius(element: driverImage), borderWidth: 1, color: Colors.AppBaseColor)
        Fonts.setPrimaryMedium(driverName)
        driverName.textColor = Colors.PrimaryText
        Fonts.setPrimaryMedium(titleLable)
        titleLable.textColor = Colors.PrimaryText
        titleLable.text = StringConstants.leaveTip()

    }
    
    func setData(data:Order) {
        Helper.setImage(imageView: driverImage, url: data.DriverImage, defaultImage: #imageLiteral(resourceName: "UserDefault"))
        driverName.text = data.DriverName
    }

}
