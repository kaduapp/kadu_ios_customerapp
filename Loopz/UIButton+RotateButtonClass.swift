//
//  UIButton+RotateButtonClass.swift
//  DelivX
//
//  Created by 3EMBED on 21/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class UIButton_RotateButtonClass: UIButton {

    
     func rotateButton() {
     
     if(RTL.shared.isRTL){
     self.transform = CGAffineTransform(scaleX: -1.0,y: 1.0)
     }
    }
      
     
      func buttonTextAlignMent ()
     {
      if(RTL.shared.isRTL){
     self.contentHorizontalAlignment = .right
     }
     }
    
    
    
    
    func buttonImageInsets(spacing:CGFloat)
     {
     if(RTL.shared.isRTL){
     self.imageEdgeInsets =  UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -spacing)//UIEdgeInsetsMake(0, 0,0, -spacing);
     }
     else
     {
     self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -spacing, bottom: 0, right: 0)//UIEdgeInsetsMake(0, -spacing,0, 0);
     }
     }
    
}
