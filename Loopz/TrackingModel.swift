//
//  TrackingModel.swift
//  UFly
//
//  Created by 3 Embed on 06/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

struct TrackingModel {
    var orderStatusTitle = ""
    var orderStatusIcon = UIImage()
    var selectedOrder = UIImage()
}

class TrackingModelLibrary {
    
    static var trackList: [TrackingModel] {
        
        var status1 = TrackingModel()
        status1.orderStatusTitle = AppConstants.status()[0]
        status1.orderStatusIcon = #imageLiteral(resourceName: "ConfirmedOff")
        status1.selectedOrder = #imageLiteral(resourceName: "ConfirmedOn")
        
        var status2 = TrackingModel()
        status2.orderStatusTitle = AppConstants.status()[1]
        status2.orderStatusIcon = #imageLiteral(resourceName: "ReceviedOff")
        status2.selectedOrder = #imageLiteral(resourceName: "ReceviedOn")
        
        var status3 = TrackingModel()
        status3.orderStatusTitle = AppConstants.status()[2]
        status3.orderStatusIcon = #imageLiteral(resourceName: "DriverAtStoreOff")
        status3.selectedOrder = #imageLiteral(resourceName: "DriverAtStoreOn")
        
        var status4 = TrackingModel()
        status4.orderStatusTitle = AppConstants.status()[3]
        status4.orderStatusIcon = #imageLiteral(resourceName: "PickedupOff")
        status4.selectedOrder = #imageLiteral(resourceName: "pickedUpOn")
        
        var status5 = TrackingModel()
        status5.orderStatusTitle = AppConstants.status()[4]
        status5.orderStatusIcon = #imageLiteral(resourceName: "DriverAtLocOff")
        status5.selectedOrder = #imageLiteral(resourceName: "DriverAtLocOn")
        
        var status6 = TrackingModel()
        status6.orderStatusTitle = AppConstants.status()[5]
        status6.orderStatusIcon = #imageLiteral(resourceName: "DeliveredOff")
        status6.selectedOrder = #imageLiteral(resourceName: "DeliveredOn")
        
       return [status1,status2,status3,status4,status5,status6]
        
        
    }
}
