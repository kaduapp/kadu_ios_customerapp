//
//  AddAddressUIE.swift
//  DelivX
//
//  Created by 3 Embed on 28/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

extension AddAddressVC {
    
    /// initial Setup
    func initialSetup(){
        
        
        saveAsLabel.text = StringConstants.saveAsLabel()
        contentView.layer.cornerRadius = 10
        saveAsLabel.textColor =  Colors.SecoundPrimaryText
        
        
        saveDeliveryLocation.text = StringConstants.SaveDeliveryLoc()
        Fonts.setPrimaryMedium(saveDeliveryLocation)
        saveDeliveryLocation.textColor =  Colors.PrimaryText
        addressTF.textColor =  Colors.PrimaryText
        flatTextField.textColor =  Colors.PrimaryText
        landmarkTextField.textColor =  Colors.PrimaryText
        phoneNumberTextField.textColor =  Colors.PrimaryText
        
        Helper.setShadow(sender: contentView)
        Helper.setButton(button: saveButton, view: buttonView, primaryColour: Colors.AppBaseColor, seconderyColor:Colors.SecondBaseColor,shadow: true)
        Fonts.setPrimaryMedium(saveButton)
        self.searchTableView.isHidden = true
        addressTF.placeholder = StringConstants.Address()
        flatTextField.placeholder = StringConstants.DoorStep()
        landmarkTextField.placeholder = StringConstants.Landmark()
        phoneNumberTextField.placeholder =  StringConstants.PhoneNumbers()//"Phone Number"

      
        self.addressTF.addTarget(self, action: #selector(AddAddressVC.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.flatTextField.addTarget(self, action: #selector(AddAddressVC.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.landmarkTextField.addTarget(self, action: #selector(AddAddressVC.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        flagMapChange = false
        locationManager = LocationManager.shared
        if !flagEdit {
            locationManager?.delegate = self
            locationManager?.start()
        }
        
        self.mapView?.isMyLocationEnabled = true
        
        Fonts.setPrimaryMedium(homeBtn)
        Fonts.setPrimaryMedium(workBtn)
        Fonts.setPrimaryMedium(othersBtn)
        
        Helper.setButtonTitle(normal: "  "+StringConstants.Home(), highlighted: "  "+StringConstants.Home(), selected: "  "+StringConstants.Home(), button: homeBtn)
        Helper.setButtonTitle(normal: "  "+StringConstants.Work(), highlighted: "  "+StringConstants.Work(), selected: "  "+StringConstants.Work(), button: workBtn)
        Helper.setButtonTitle(normal: "  "+StringConstants.Others(), highlighted: "  "+StringConstants.Others(), selected: " "+StringConstants.Others(), button: othersBtn)
    }
    
    
    /// animate View
    func animateView(){
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.view.updateConstraints()
        }
    }
    
    
    /// initialse
    func initialise() {
        self.mapView.delegate = self
        if flagEdit {
            var tagValue = 0
            switch addressAdded?.Tag {
            case StringConstants.Home():
                tagValue = 0
                break
            case StringConstants.Work():
                tagValue = 1
                break
            default:
                tagValue = 2
                break
            }
            let btn:UIButton = self.view.viewWithTag(200+tagValue) as! UIButton
            self.tabButtonAction(btn)
            addressSelected.setFromAddress(data: addressAdded!)
            addressTF.text = AddressUtility.getSortedAddressFromLocation(addressSelected)
            
            if let data = addressAdded,  data.FlatNo.length > 0{
                flatTextField.text = data.FlatNo
                self.flatNumber = data.FlatNo
            }
            if let data = addressAdded,  data.LandMark.length > 0{
                landmarkTextField.text = data.LandMark
                self.landmark = data.LandMark
            }
            if let data = addressAdded,  data.PhoneNumber.length > 0{
                phoneNumberTextField.text = countryCode + data.PhoneNumber
                self.phonenumber = data.LandMark
            }
            self.saveButton.setTitle(StringConstants.UpdateAddress(), for: .normal)
            let locationAddress = CLLocation(latitude: CLLocationDegrees((addressAdded?.Latitude)!), longitude: CLLocationDegrees((addressAdded?.Longitude)!))
            let camera = GMSCameraPosition.camera(withLatitude: locationAddress.coordinate.latitude ,longitude: locationAddress.coordinate.longitude
                , zoom: zoomLevelofMap)
            self.mapView.animate(to: camera)
            
        }else{
            locationManager?.start()
            let btn:UIButton = self.view.viewWithTag(200) as! UIButton
            self.tabButtonAction(btn)
            self.saveButton.setTitle(StringConstants.SaveAddress(), for: .normal)
            myLocationAction(UIButton())
        }
        self.mapMarker.bringSubviewToFront(self.view)
    }
}
