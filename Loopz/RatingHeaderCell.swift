//
//  RatingHeaderCell.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class RatingHeaderCell: UITableViewCell {

    @IBOutlet weak var subseparator: UIView!
    @IBOutlet weak var reasonSeparator: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var ratestar: FloatRatingView!
    @IBOutlet weak var reasonTitle: UILabel!
    @IBOutlet weak var popUpTableView: UITableView!
    @IBOutlet weak var heightOfPopUpTableView: NSLayoutConstraint!
    @IBOutlet weak var subseperatorBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var subseparatorTopConstraints: NSLayoutConstraint!
   // @IBOutlet weak var reasonBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var reasonTopConstraints: NSLayoutConstraint!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Fonts.setPrimaryMedium(title)
        Fonts.setPrimaryRegular(reasonTitle)
        
        title.textColor = Colors.PrimaryText
        reasonTitle.textColor = Colors.ReviewBlack
        ratestar.emptyImage = #imageLiteral(resourceName: "Rating_Star_Off")
        ratestar.fullImage = #imageLiteral(resourceName: "Rating_Star_On")
        reasonTitle.text = StringConstants.Improve()
        reasonSeparator.backgroundColor = Colors.ReviewBlack
        subseparator.backgroundColor = Colors.SeparatorLight
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setData(data:Rating) {
        title.text = data.Name
        ratestar.rating = Float(data.Value)
    }
}
