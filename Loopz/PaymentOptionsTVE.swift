//
//  PaymentOptionsTVE.swift
//  DelivX
//
//  Created by 3 Embed on 27/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension PaymentOptionsVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return AppConstants.PaymentMethodsForCheckOut().count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            if Changebles.WallettAvailable{ //&& priority == true
                return 1
            }
            return 0
        case 1:
//            return 0
            return cardArray.count+1
        case 2:
            if Changebles.iDealPaymentAvailable{
                return 1
            }
            return 0
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (Changebles.WallettAvailable == false/* || priority == false*/) && section == 0 {
            return 0
        }else if (Changebles.iDealPaymentAvailable == false) && section == 2 {
            return 0
        }
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (Changebles.WallettAvailable == false /*|| priority == false*/) && section == 0 {
            return Helper.showHeader(title: "",showViewAll: false) as? HeaderView
        }else if (Changebles.iDealPaymentAvailable == false) && section == 2 {
            return Helper.showHeader(title: "",showViewAll: false) as? HeaderView
        }
        return Helper.showHeader(title: AppConstants.PaymentMethodsForCheckOut()[section],showViewAll: false) as? HeaderView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.PaymentTableCell, for: indexPath) as! PaymentTableCell
        // cell.deleteButton.addTarget(self, action:#selector(PaymentOptionsVC.linkAccountBtnPressed) , for: UIControlEvents.touchUpInside)
        cell.addMoney.addTarget(self, action:#selector(PaymentOptionsVC.addMoneyAction) , for: UIControl.Event.touchUpInside)
        cell.payUsingQC.addTarget(self, action:#selector(PaymentOptionsVC.payUsingQCAction) , for: UIControl.Event.touchUpInside)
        var flag = true
        if indexPath != selectedIndex {
            flag = false
        }
        switch indexPath.section{
        case 0:
        cell.updateWalletCell(handler:self.methodHandler,show: flag)
        cell.cellseperator.isHidden = true
            break
        case 1:

            if indexPath.row == cardArray.count {
                cell.updateAddNewCard()
            } else {
                cell.updateCreditCardCell(data: cardArray[indexPath.row], show: flag,handler:self.methodHandler,index:indexPath)

            }
            break
        case 2:
            cell.updateiDeal(show: flag)
            break
        case 3:
            cell.updateCash(show: flag,handler:self.methodHandler)
            break
        default:
            //cell.updateiDeal(show: flag)
            break
        }
        cell.cartDiscount = self.discount
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 0:
            methodHandler.isPayByWallet = !methodHandler.isPayByWallet
            break
        case 1:
            if indexPath.row == cardArray.count {
                selectedIndex = nil
                self.performSegue(withIdentifier: UIConstants.SegueIds.PaymentopToAddNew, sender: self)
            }
            else {
                
                if !methodHandler.isPayByCard || methodHandler.selectedIndexPath == indexPath{
                    methodHandler.isPayByCard = !methodHandler.isPayByCard
                }
                methodHandler.selectedIndexPath  = indexPath
                if methodHandler.isPayByCard{
                    methodHandler.isPayByCash = false
                }
            }
            break
        case 2:
           // paymentMethod = .iDeal
            break
        default:
//            paymentMethod = .Cash
            methodHandler.isPayByCash = !methodHandler.isPayByCash
            if methodHandler.isPayByCash{
                methodHandler.isPayByCard = false
            }
            break
        }
        var array = [IndexPath]()
        if selectedIndex != nil {
            array.append(selectedIndex!)
        }
        if selectedIndex != indexPath {
            array.append(indexPath)
        }
        selectedIndex = indexPath
     //  self.mainTableView.reloadRows(at: array, with: .fade)
        self.mainTableView.reloadData()
        
    }
    
    //take to quick card register screen
    func linkAccountBtnPressed(){
        self.performSegue(withIdentifier: UIConstants.SegueIds.PaymentSeutUp, sender: self)
    }
    
    //
    @objc func addMoneyAction(){
        self.performSegue(withIdentifier: UIConstants.SegueIds.PaymentToQuickCard, sender: self)
    }
    
    @objc func payUsingQCAction(){
        if methodHandler.isPayByCard {
  self.responseData.onNext((cardArray[(selectedIndex?.row)!],methodHandler,priority))
        }else {
        self.responseData.onNext((Card.init(data: [:]),methodHandler,priority))
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
}

