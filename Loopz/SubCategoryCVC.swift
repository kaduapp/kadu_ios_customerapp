//
//  SubCategoryCVC.swift
//  UFly
//
//  Created by 3Embed on 08/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SubCategoryCVC: UICollectionViewCell {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var subCatImageHeight: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    func setCell() {
        Fonts.setPrimaryRegular(mainTitle)
        mainTitle.textColor = Colors.PrimaryText
    }
    
    func setData(data:SubCategory) {
       let cellHeight = (UIScreen.main.bounds.width-80)/3
        if data.Image.count > 0{
            subCatImageHeight.constant = cellHeight - 40
        }else{
            subCatImageHeight.constant = 0
        }
        Helper.setImage(imageView: mainImage, url: data.Image, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        mainTitle.text = data.Name
    }
    
}
