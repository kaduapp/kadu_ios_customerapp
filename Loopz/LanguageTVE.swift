//
//  LanguageTVE.swift
//  DelivX
//
//  Created by 3 Embed on 23/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension LanguageVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.PaymentListCell, for: indexPath) as! PaymentListCell
        Fonts.setPrimaryRegular(cell.paymentTypeTitle)
        var flag = false
        if selectedLanguage.Code == languages[indexPath.row].Code {
            flag = true
        }
        cell.updateCell(data: languages[indexPath.row], flag: flag)
        if indexPath.row == languages.count - 1 {
            cell.separator.isHidden = true
        }else{
            cell.separator.isHidden = false
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        selectedLanguage = languages[indexPath.row]
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return Helper.showHeader(title: StringConstants.AvailableLanguages(), showViewAll: false)
    }
}
