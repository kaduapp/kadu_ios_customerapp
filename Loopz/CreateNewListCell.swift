//
//  CreateNewListCell.swift
//  DelivX
//
//  Created by 3 Embed on 02/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CreateNewListCell: UITableViewCell {

    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var createNewListBtn: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var separator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.Semantic()
        // Initialization code
        
        Fonts.setPrimaryMedium(createNewListBtn)
        Helper.setButtonTitle(normal: StringConstants.AddCard(), highlighted: StringConstants.AddCard(), selected: StringConstants.AddCard(), button: createNewListBtn)
        
         Helper.setButton(button: createNewListBtn,view:btnView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
        separator.backgroundColor = Colors.SeparatorLight
        Fonts.setPrimaryRegular(textField)
        textField.placeholder = StringConstants.Name()
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
