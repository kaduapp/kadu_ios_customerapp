//
//  EditTFE.swift
//  DelivX
//
//  Created by 3 Embed on 08/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension EditVC: UITextFieldDelegate {

        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == self.editView.phoneNumTF {
                self.editView.emailTF.becomeFirstResponder()
                self.editView.emailSeparator.backgroundColor =  Colors.AppBaseColor
            } else {
                self.editView.emailSeparator.backgroundColor =  Colors.SeparatorLight
                self.editView.emailTF.resignFirstResponder()
            }
            return true
        }
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            
            if textField == self.editView.phoneNumTF {
                
                self.editView.phSeperartor.backgroundColor =  Colors.AppBaseColor
                self.editView.emailSeparator.backgroundColor =  Colors.SeparatorLight
            } else {
                
                self.editView.phSeperartor.backgroundColor =  Colors.SeparatorLight
                self.editView.emailSeparator.backgroundColor =  Colors.AppBaseColor
            }
            
            return true
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            self.editView.phoneNum.append(string)
            if textField == editView.phoneNumTF {
                if range.location+1 == self.editProfileVM.authentication.CountryCode.length{
                    if range.location+1 == self.editProfileVM.authentication.CountryCode.length {
                        return false
                    }
                    return true
                }
            }
            return true
            
        }
        
       @objc func textFieldDidBeginEditing(_ textField: UITextField) {
            
            if textField == self.editView.phoneNumTF {
                editView.countryCodeBtn.isEnabled = true
                Helper.addDoneButtonOnTextField(tf: textField, vc: self.editView)
                self.editView.phSeperartor.backgroundColor =  Colors.AppBaseColor
                self.editView.emailSeparator.backgroundColor =  Colors.SeparatorLight
                
                if textField.text?.length == 0 {
                    textField.text = editProfileVM.authentication.CountryCode
                }
            }
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            
            switch textField {
                
            case editView.phoneNumTF:
                let phoneNumber = editView.phoneNumTF.text?.replacingOccurrences(of: editProfileVM.authentication.CountryCode, with: "")
                if (textField.text?.length)! > 0{
                    
                    guard Helper.isPhoneNumValid(phoneNumber: phoneNumber!, code: editProfileVM.authentication.CountryCode) == true else {
                        
                        if editView.phoneNumTF.text?.length == editProfileVM.authentication.CountryCode.count {
                            Helper.showAlert(message: StringConstants.PhoneNumberAvailable() , head: StringConstants.Error(), type: 1)
                        }
                        else{
                           Helper.showAlert(message: StringConstants.ValidPhoneNumber() , head: StringConstants.Error(), type: 1)
                        }
                        
                        return
                    }
                }
                
                editProfileVM.authentication.Phone = phoneNumber!
                break
                
            default:
                guard Helper.isValidEmail(text: editView.emailTF.text!) == true else {
                    Helper.showAlert(message: StringConstants.ValidEmail() , head: StringConstants.Error(), type: 1)
                    return
                }
                editProfileVM.authentication.Email = editView.emailTF.text!
                break
            }
            
    }
        
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        if textField == self.editView.phoneNumTF{
            
            let count = self.editProfileVM.authentication.CountryCode.length
            let phoneNumber = editView.phoneNumTF.text?.replacingOccurrences(of: editProfileVM.authentication.CountryCode, with: "")
            if Helper.isPhoneNumValid(phoneNumber: phoneNumber!, code: self.editProfileVM.authentication.CountryCode) && (textField.text!.length) > count {
                self.editView.phNumTickMark.isHidden = false
            }else{
                self.editView.phNumTickMark.isHidden = true
            }
        }
        else {
            if Helper.isValidEmail(text: textField.text!) {
                self.editView.emailTickMark.isHidden = false
            }
            else{
                self.editView.emailTickMark.isHidden = true
            }
            
        }
        
    }

}
