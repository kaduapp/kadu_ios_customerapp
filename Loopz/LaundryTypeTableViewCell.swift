//
//  LaundryTypeTableViewCell.swift
//  DelivX
//
//  Created by 3EMBED on 26/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
protocol LaundryTypeTableViewCellDelegate {
    func didTapselectButton(_ selectButton:UIButton ,_ indexPath:IndexPath?)
}

class LaundryTypeTableViewCell: UITableViewCell {
    @IBOutlet weak var LaundryTitleLabel: UILabel!
    @IBOutlet weak var selectImageView: UIImageView!
    @IBOutlet weak var LandryTypeDescLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var cellSeparator: UIView!
    var delegate:LaundryTypeTableViewCellDelegate? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    //rgba(193, 193, 193, 1)
    func updateCell( _ data:LaundryCellType)
    {
        LaundryTitleLabel.text = data.Head
        LandryTypeDescLabel.text = data.Body
     //   selectButton.setImage(UIImage(named: "Check_off") , for: .normal)
        self.UnSelectedCell()
    }
    func SelectedCell() {
        LaundryTitleLabel.textColor = Colors.DarkBlueBlack
        LandryTypeDescLabel.textColor = Colors.lightBlueBlack
        selectButton.setImage(UIImage(named: "CheckOn") , for: .normal)
    }
    func UnSelectedCell() {
        LaundryTitleLabel.textColor =  Colors.lightBlueBlack
        LandryTypeDescLabel.textColor = Colors.laundryUnselectCellColor
        selectButton.setImage(UIImage(named: "Check_off") , for: .normal)

    }
    @IBAction func selectButtonTapped(_ sender: Any)
    {
        
        if let tblView = self.superview as? UITableView
        {
            let indexPath = tblView.indexPath(for: self)
            delegate?.didTapselectButton(selectButton,indexPath)
        }
       // let indexPath = self.superview //[self.tableView indexPathForCell:cell];

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
