//
//  SignUpVC.swift
//  UFly
//
//  Created by Rahul Sharma on 15/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {
    
    @IBOutlet weak var navigationBackView: UIView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var signUpView: SignUpView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signupButtonViewBottomConstraint: NSLayoutConstraint!
    
    var signupVM = SignupVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signUpView.signUpScrollView.contentOffset.y = 0
        self.setUp()
        self.didSignup()
        Helper.transparentNavigation(controller: self.navigationController!)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        signUpView.signUpScrollView.contentOffset.y = 0
        addObserver()
        setNavigationBar()
        signupVM.doSignUp = false
        signupVM.signUp = false
        signUpView.activeTextField = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        removeObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
         self.setFirstResponder()
    }
    
    @IBAction func didPTapCloseButton(_ sender: Any) {
        signupVM.didTapColse = true
//        UIView.animate(withDuration: 0.2,
//                       animations: {
//                        self.dismissKeyboard()
//        }) { (Bool) in
//            UIView.animate(withDuration: 0.3, animations: {
                //self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
//
//            })
//        }
    }
    
    @IBAction func didTapCheckMark(_ sender: UIButton) {
        
        var length = 0
        if sender.isSelected == false {
            sender.isSelected = true
            length = 46
        }else {
            sender.isSelected = false
            length = 0
        }
        
        UIView.animate(withDuration: 0.50) {
            self.signUpView.refHight.constant = CGFloat(length)
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func didTapSignUpButton(_ sender: Any) {
        Helper.showPI(string: "")
        signupVM.signUp = false
        signupVM.doSignUp = true
        dismissKeyboard()
        //validateFields()
    }
    
    @IBAction func countryCodeAction(_ sender: Any) {
        
        let vnhStoryboard = UIStoryboard(name: UIConstants.ControllerIds.VNHCountryPicker, bundle: nil)
        if let nav: UINavigationController = vnhStoryboard.instantiateInitialViewController() as! UINavigationController? {
            let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker
            // delegate
            picker.delegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == UIConstants.SegueIds.SignUpToOtp {
            if let otpVC:OTPVC = segue.destination as? OTPVC {
                otpVC.otpVM.authentication = signupVM.authentication
            }
        }
    }
}

extension SignUpVC {
    
    func subscribedData(data: SignupVM.ResponseType){
        switch data {
        case .ValidateEmail:
            self.signUpView.passwordTF.becomeFirstResponder()
            break
        case .ValidatePhone:
            signUpView.nameTF.becomeFirstResponder()
            break
        case .RegisteredEmail:
            signUpView.emailTF.text = ""
            signUpView.emailCheckMark.isHidden = true
            signUpView.emailSeparator.backgroundColor =   Colors.Red
            self.dismissKeyboard()
            self.showAlertMessage(message: self.signupVM.validationMessage , head: StringConstants.Error(), type: 2)
            break
        case .RegisteredPhone:
            signUpView.phNumCheckMark.isHidden = true
            signUpView.mobileNumberTF.text = ""
            signUpView.phNumSeparator.backgroundColor =  Colors.Red
            self.dismissKeyboard()
            self.showAlertMessage(message: self.signupVM.validationMessage  , head: StringConstants.Error(), type: 1)
            break
        case .validateReferral:
            signUpView.referralTF.text = ""
            break
        case .MoveOTP:
            validateFields()
            break
        default:
            break
        }
        
    }
    
    /// observe for view model
    func didSignup(){
        
        signupVM.signupVM_response.subscribe(onNext: { [weak self]success in
            self?.subscribedData(data: success)
        }).disposed(by: signupVM.disposeBag)
        
    }
    /// Dismiss Keyboard
    func dismissKeyboard(){
        if signUpView.mobileNumberTF.text?.sorted().count == signupVM.authentication.CountryCode.sorted().count{
            signUpView.mobileNumberTF.text = ""
        }
        if signUpView.activeTextField == nil {
            if signupVM.didTapColse == false {
                validateFields()
            }
        }
        signUpView.activeTextField = nil
        view.endEditing(true)
    }
    
    
    /// this method validates all the fields before Signup
    ///
    /// - Parameter view: it is of type SignUPView
    func validateFields() {
        let phoneNumber = signUpView.mobileNumberTF.text?.replacingOccurrences(of: signupVM.authentication.CountryCode, with: "")
        guard phoneNumber?.sorted().count != 0 else {
            signUpView.phNumSeparator.backgroundColor =  Colors.Red
            signupVM.doSignUp = false
            Helper.showAlert(message: StringConstants.PhoneNumberAvailable(), head: StringConstants.Error(), type: 1)
            Helper.hidePI()
            return
        }
        
        if Helper.isPhoneNumValid(phoneNumber: phoneNumber!, code: signupVM.authentication.CountryCode)  == false {
            signUpView.mobileNumberTF.text = ""
            signUpView.phNumSeparator.backgroundColor =  Colors.Red
            Helper.showAlert(message: StringConstants.ValidPhoneNumber() , head: StringConstants.Error(), type: 1)
        }
        
        guard signUpView.nameTF.text?.sorted().count != 0 else {
            signUpView.nameSeparator.backgroundColor =  Colors.Red
            Helper.showAlert(message: StringConstants.NameAvailable(), head: StringConstants.Error(), type: 1)
            signupVM.doSignUp = false
            Helper.hidePI()
            return
        }
        signupVM.authentication.Name = signUpView.nameTF.text!
        
        guard signUpView.emailTF.text?.sorted().count != 0 else {
            signUpView.emailSeparator.backgroundColor =   Colors.Red
            signupVM.doSignUp = false
            Helper.showAlert(message: StringConstants.EmailAvailable(), head: StringConstants.Error(), type: 1)
            Helper.hidePI()
            return
        }
        guard Helper.isValidEmail(text: signUpView.emailTF.text!) == true else {
            Helper.showAlert(message: StringConstants.ValidEmail() , head: StringConstants.Error(), type: 1)
            return
        }
        
        guard signUpView.passwordTF.text?.sorted().count != 0 else {
            signUpView.pwSeparator.backgroundColor =  Colors.Red
            signupVM.doSignUp = false
            Helper.showAlert(message: StringConstants.PasswordAvailable(), head: StringConstants.Error(), type: 1)
            Helper.hidePI()
            return
        }
        signupVM.authentication.ReferralCode = signUpView.referralTF.text!
        signupVM.authentication.Password = signUpView.passwordTF.text!
        
        if signupVM.validEmail == false || signupVM.validPhone == false {
            if signupVM.validEmail == false {
                if signUpView.emailTF.text?.sorted().count != 0 {
                    signupVM.authentication.Email = signUpView.emailTF.text!
                    signupVM.validateEmail()
                    return
                }
            }
            if signupVM.validPhone == false {
                if signUpView.mobileNumberTF.text?.sorted().count != 0{
                    signupVM.authentication.Phone = signUpView.mobileNumberTF.text!
                    signupVM.validatePhone()
                    return
                }
            }
        }else if signupVM.doSignUp == true && signupVM.validEmail == true && signupVM.validPhone == true {
            let phoneNumber = signUpView.mobileNumberTF.text?.replacingOccurrences(of: signupVM.authentication.CountryCode, with: "")
            if Helper.isPhoneNumValid(phoneNumber: phoneNumber!, code: signupVM.authentication.CountryCode)  == true {
                if signupVM.validEmail && signupVM.validPhone && signupVM.doSignUp {
                    //                    signupVM.validEmail = false
                    //                    signupVM.validPhone = false
                    //                    signUpView.phNumCheckMark.isHidden = true
                    //                    signUpView.emailCheckMark.isHidden = true
                    if signupVM.signUp {
                        return
                    }
                    signupVM.signUp = true
                    self.performSegue(withIdentifier: UIConstants.SegueIds.SignUpToOtp, sender: self)
                }else{
                    validateFields()
                }
            }else{
                validateFields()
            }
        }
    }
    
    
    
     func showAlertMessage(message:String, head:String, type:Int) {
        Helper.hidePI()
        var alertPopup:UIAlertController? = nil
        alertPopup?.dismiss(animated: false, completion: nil)
        alertPopup = UIAlertController(title:head, message: message, preferredStyle: UIAlertController.Style.alert)
        alertPopup?.addAction(UIAlertAction.init(title: StringConstants.OK() , style:  UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
           
            if type == 1{
            self.signUpView.mobileNumberTF.becomeFirstResponder()
            }
            else{
             self.signUpView.emailTF.becomeFirstResponder()
            }
        }))
        if UIApplication.shared.delegate?.window != nil {
            let vc = Helper.finalController()
            vc.present(alertPopup!, animated: true, completion: nil)
        }
    }
    
}
