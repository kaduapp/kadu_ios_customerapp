//
//  LocationManager.swift
//  Trustpals
//
//  Created by NABEEL on 12/06/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import RxCocoa
import RxSwift
import RxAlamofire

protocol LocationManagerDelegate {
    //Return search address
    func didUpdateSearch(locations: [Location])
    /// When Update Location
    func didUpdateLocation(location: Location, search:Bool,update:Bool)
    
    /// When Failed to Update Location
    func didFailToUpdateLocation()
    
    func didChangeAuthorization(authorized: Bool)
}

class LocationManager: NSObject {
    let disposeBag = DisposeBag()
    
    var locationData = Location.init(data: [:])
    var latitute: Float = 0
    var longitude: Float = 0
    var name: String = ""
    var FullText = ""
    var tempName = ""
    var isCurrentLocation = false
    var city: String = ""
    var state: String = ""
    var country: String = ""
    var zipcode: String = ""
    var street: String = ""
    var countryCode: String = ""
    var CLlocationObj = CLLocation()
    
    var locationObj: CLLocationManager? = nil
    var delegate: LocationManagerDelegate? = nil
    
    static var shareObj: LocationManager? = nil
    /// Create Shared Instance
    static var shared: LocationManager {
        if shareObj == nil {
            shareObj = LocationManager()
        }
        return shareObj!
    }
    
    override init() {
        super.init()
        locationObj = CLLocationManager()
        locationObj?.delegate = self
        CommonAlertView.AlertPopupResponse.subscribe(onNext: { (success) in
            if success == .Location {
                //  UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
        }).disposed(by: self.disposeBag)
    }
    /// Start Location Update
    func start() {
        locationObj?.requestWhenInUseAuthorization()
        if CLLocationManager.authorizationStatus() == .denied {
            Helper.showAlertReturn(message: ( Utility.getLanguage().Code == "es" ? StringConstants.AllowLocationBody() : "A Kadú le gustaría usar tu ubicación para encontrar las tiendas que te rodean"), head: String(format:( Utility.getLanguage().Code == "es" ? StringConstants.AllowLocationHead(): "Permitir que Kadú acceda a tu ubicación mientras usas la aplicación"),Utility.AppName), type: ( Utility.getLanguage().Code == "es" ? StringConstants.OK(): "OK"), closeHide: false, responce: CommonAlertView.ResponceType.Location)
            Utility.saveLatAndLong(lat: 0, long: 0, address: StringConstants.EnableCurrentLocation())
            Helper.hidePI()
            return
        }
        locationObj?.startUpdatingLocation()
    }
    /// Stop Location Update
    func stop() {
        locationObj?.stopUpdatingLocation()
    }
    //MARK: - Google Auto Complete
    
    /// Call Google Auto Complete API
    ///
    /// - Parameter searchText: Words to be Searched
    func search(searchText: String, language: String) {
        
        
        let latitudeTemp = LocationManager.shared.latitute
        let longitudeTemp = LocationManager.shared.longitude
        let key = GoogleKeys.GoogleMapKey
        
        let placeAPI = String(format: GoogleKeys.PlacesAPILink ,searchText,latitudeTemp,longitudeTemp,language,key)
        
        // Call Service
        guard let url = placeAPI.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)else{
            return
        }
        RxAlamofire.requestJSON(.get, url).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            if let array = bodyIn[GoogleKeys.Predictions] as? [Any] {
                
                var arrayOfLocation = [Location]()
                if let status = bodyIn["status"] as? String, status == "OVER_QUERY_LIMIT" {
                    var data = Location.init(data: [:])
                    data = data.initBangalore()
                    arrayOfLocation.append(data)
                    self.delegate?.didUpdateSearch(locations: arrayOfLocation)
                    GoogleKeys.SelectedPositionMapKey = GoogleKeys.SelectedPositionMapKey + 1
                }
                else{
                    for dataFrom in array {
                        let data = Location.init(data: dataFrom as! [String : Any])
                        arrayOfLocation.append(data)
                    }
                    self.delegate?.didUpdateSearch(locations: arrayOfLocation)
                }
            }
        }, onError: { (Error) in
        }).disposed(by: self.disposeBag)
    }
    
    
    func updateLocationData(location : CLLocation , placeIn:Location,flag:Bool) {
            CLlocationObj = location
        let geocoder = GMSGeocoder()
            let position = CLLocationCoordinate2D.init(latitude: location.coordinate.latitude , longitude: location.coordinate.longitude)
            geocoder.reverseGeocodeCoordinate(position) { response, error in
                
                if let location = response?.firstResult() {
                    var flagSearch = true
                                // Location name
                                if self.tempName.length > 0 {
                                    self.name = self.tempName
                                    self.tempName = ""
                                    flagSearch = true
                                }else{
                                     
                                    self.name = location.lines![0]
                                        flagSearch = false
                                 }
                
                                if let country = location.country {
                                    if GoogleKeys.myCountry.length == 0 {
                                        GoogleKeys.myCountry = country
                                    }
                                }
                    placeIn.update(data: location)
                    if flag {
                                   Utility.saveLatAndLong(lat: Float(location.coordinate.latitude), long: Float(location.coordinate.longitude),address: placeIn.FullText)
                               }
                               if self.delegate != nil {
                                   self.delegate?.didUpdateLocation(location: placeIn ,search: flagSearch,update:flag)
                               }

                }
            }
           
        /*     let geoCoder = CLGeocoder()

             geoCoder.reverseGeocodeLocation(location) {
                
                (placemarks, error) -> Void in
                
                if placemarks == nil {
                    return
                }
                
                
                let arrayOfPlaces = placemarks
                
                // Place details
                let placemark: CLPlacemark = (arrayOfPlaces?[0])!
                
                // Address dictionary
                var flagSearch = true
                // Location name
                if self.tempName.length > 0 {
                    self.name = self.tempName
                    self.tempName = ""
                    flagSearch = true
                }else{
                    if let name = placemark.name {
                        self.name = name
                        flagSearch = false
                    }
                }
    //            let placeData = placemark.addressDictionary as! [String : Any]
                // Country
                if let country = placemark.country {
                    if GoogleKeys.myCountry.length == 0 {
                        GoogleKeys.myCountry = country
                    }
                }
                placeIn.update(data: placemark)
    //            placeIn.update(data: placemark,placeName:self.name)
                if flag {
                    Utility.saveLatAndLong(lat: Float(location.coordinate.latitude), long: Float(location.coordinate.longitude),address: placeIn.FullText)
                }
                if self.delegate != nil {
                    self.delegate?.didUpdateLocation(location: placeIn ,search: flagSearch,update:flag)
                }
                
            }*/
        }
    
    
    func googlePlacesInformation(placeId: String, completion: @escaping (_ place: NSDictionary) -> Void) {
        
        let urlString = String(format: GoogleKeys.PlaceEnlarge,placeId,GoogleKeys.GoogleMapKey)
        
        RxAlamofire.requestJSON(.get, urlString, parameters:nil, headers: [:]).debug().subscribe(onNext: { (head, body) in
            let bodyIn = body as! [String:Any]
            let status = bodyIn["status"] as! String
            if status != "NOT_FOUND" && status != "REQUEST_DENIED" && status != "OVER_QUERY_LIMIT" {
                if let results = bodyIn["result"] as? NSDictionary {
                    completion(results)
                }
            }else if status == "OVER_QUERY_LIMIT" || status == "REQUEST_DENIED" {
                GoogleKeys.SelectedPositionMapKey = GoogleKeys.SelectedPositionMapKey + 1
                Helper.hidePI()
            }else{
                Helper.hidePI()
            }
            
            
        }, onError: { (Error) in
        }).disposed(by: self.disposeBag)
    }
    
    
    func getAddressFromPlace(place:Location, reverseGeocodeLocation:Bool = false) {
        googlePlacesInformation(placeId: place.PlaceId,
                                completion:{ (placeFrom) -> Void in
                                    if placeFrom.count > 0
                                    {
                                        place.Name = placeFrom.value(forKey: "name") as! String
                                        let latitude =  (((placeFrom.value(forKey: "geometry") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lat") as! NSNumber)
                                        let longitude =  (((placeFrom.value(forKey: "geometry") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lng") as! NSNumber)
                                        place.Latitude = Float(truncating: latitude)
                                        place.Longitude = Float(truncating: longitude)
                                        place.FullText = placeFrom.value(forKey: "formatted_address") as! String
                                           self.delegate?.didUpdateLocation(location:place,search: true,update:false)

        
                                    }
        })
    }
    
    
}

extension LocationManager: CLLocationManagerDelegate {
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch(status) {
            
        case .authorizedAlways, .authorizedWhenInUse:
            
            print("Access")
            locationObj?.startUpdatingLocation()
            delegate?.didChangeAuthorization(authorized: true)
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.first != nil {
            manager.stopUpdatingLocation()
            let location = locations.first
            
            if self.latitute == 0.0 || self.longitude == 0.0 || self.locationData.Name == ""{
                self.isCurrentLocation = true
            }
            else{
                self.isCurrentLocation = false
            }
            
            latitute = Float((location?.coordinate.latitude)!)
            longitude = Float((location?.coordinate.longitude)!)
            
            if  self.isCurrentLocation {
            updateLocationData(location : location!, placeIn:Location.init(data: [String:Any]()),flag:true)
            }
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("\nDid fail location : %@\n",error.localizedDescription)
            delegate?.didFailToUpdateLocation()
        }
    }
}
