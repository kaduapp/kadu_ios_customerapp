//
//  ItemListTableCell.swift
//  UFly
//
//  Created by 3 Embed on 18/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ItemListTableCell: UITableViewCell {

    @IBOutlet weak var itemListCV: UICollectionView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var viewAllButton: UIButton!
    var arrayAvailable:[Item] = []

    
    override func awakeFromNib() {
        super.awakeFromNib()
        Helper.setUiElementBorderWithCorner(element: mainView, radius: 3, borderWidth: 0, color: Colors.AppBaseColor)
        separator.backgroundColor = Colors.SecondBaseColor
        titleLabel.textColor = Colors.PrimaryText
       Helper.setButtonTitle(normal: StringConstants.MoreProducts(), highlighted: StringConstants.MoreProducts(), selected: StringConstants.MoreProducts(), button: viewAllButton)
        viewAllButton.setTitleColor(Colors.AppBaseColor, for: .normal)
        Fonts.setPrimaryMedium(viewAllButton)
        Fonts.setPrimaryMedium(titleLabel)
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}


