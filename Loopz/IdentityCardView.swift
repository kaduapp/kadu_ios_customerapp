//
//  IdentityCardView.swift
//  DelivX
//
//  Created by 3 Embed on 10/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class IdentityCardView: UIView {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var idCardLabel: UILabel!
    @IBOutlet weak var idTitle: UILabel!
    @IBOutlet weak var mmjCardLabel: UILabel!
    @IBOutlet weak var mmjCardTitle: UILabel!
    @IBOutlet weak var dontHaveMMjLabel: UILabel!
    
    @IBOutlet weak var idUploadedLabel: UILabel!
    @IBOutlet weak var mmjUploadedLabel: UILabel!
    
    @IBOutlet weak var mmjCheckMark: UIButton!
    @IBOutlet weak var idCheckMark:  UIButton!
    
    @IBOutlet weak var mmjCardImage: UIImageView!
    @IBOutlet weak var idCardImage: UIImageView!
    
    @IBOutlet weak var bottomLabelView: UIView!
    @IBOutlet weak var bottomLabel: UILabel!
    
    @IBOutlet weak var idCardSeperator: UIView!
    @IBOutlet weak var mmjCardSeperator: UIView!
    
    @IBOutlet weak var idPlusBtn: UIButton!
    @IBOutlet weak var idUpdateLabel: UILabel!
    @IBOutlet weak var idPlusBtnView: UIView!
    
    @IBOutlet weak var mmjPlusBtn: UIButton!
    @IBOutlet weak var mmjUpdateLabel: UILabel!
    @IBOutlet weak var mmjPlusBtnView: UIView!
    
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var idUpdateView: UIView!
    @IBOutlet weak var mmjUpdateView: UIView!
    
    var pickedIDImage:UIImage? = nil
    var pickedMMJImage:UIImage? = nil
    
    var isDissmis = false
    var editFlag = false
    var phoneNumber:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialViewSetup()
        
    }
    
    /// initial View setup
    func initialViewSetup() {
        
        idUpdateView.isHidden = true
        mmjUpdateView.isHidden = true
        
        
        Fonts.setPrimaryMedium(idCardLabel)
        Fonts.setPrimaryMedium(mmjCardLabel)
        Fonts.setPrimaryMedium(idUploadedLabel)
        Fonts.setPrimaryMedium(mmjUploadedLabel)
        Fonts.setPrimaryMedium(idUpdateLabel)
        Fonts.setPrimaryMedium(mmjUpdateLabel)
        
        Fonts.setPrimaryRegular(idTitle)
        Fonts.setPrimaryRegular(bottomLabel)
        
        idCardLabel.textColor =  Colors.PrimaryText
        mmjCardLabel.textColor =  Colors.PrimaryText
        
        idCardSeperator.backgroundColor =  Colors.SeparatorLight
        mmjCardSeperator.backgroundColor =  Colors.SeparatorLight
        
        idTitle.textColor =  Colors.SeconderyText
        mmjCardTitle.textColor =  Colors.SeconderyText
        
        bottomLabel.textColor =  Colors.SecondBaseColor
        idUpdateLabel.textColor =  Colors.AppBaseColor
        mmjUpdateLabel.textColor =  Colors.AppBaseColor
        dontHaveMMjLabel.textColor =  Colors.PrimaryText
        
        mmjUploadedLabel.textColor =  Colors.AppBaseColor
        idUploadedLabel.textColor =  Colors.AppBaseColor
        
        idCheckMark.tintColor =  Colors.AppBaseColor
        mmjCheckMark.tintColor =  Colors.AppBaseColor
        
        scrollView.backgroundColor =  Colors.ScreenBackground
        
        Helper.setUiElementBorderWithCorner(element: mmjCardImage, radius: 3.0, borderWidth: 0,color: Colors.SeparatorLight)
        Helper.setUiElementBorderWithCorner(element: idCardImage, radius: 3.0, borderWidth: 0, color:  Colors.SeparatorLight)
        
        Helper.setUiElementBorderWithCorner(element: mmjCardImage, radius: 2.0, borderWidth: 0,color: Colors.AppBaseColor)
        
        
        idUploadedLabel.text = StringConstants.Verified()
        mmjUploadedLabel.text = StringConstants.Verified()
        
        mmjCardLabel.text = StringConstants.MMJCard()
        idCardLabel.text  = StringConstants.IDCard()
        
        
        idTitle.text = StringConstants.UploadIDTitle()
        mmjCardTitle.text = StringConstants.UploadMMj()
        
        Helper.setButton(button: idPlusBtn,view:idPlusBtnView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: false)
        Helper.setUiElementBorderWithCorner(element: idPlusBtn, radius: Helper.setRadius(element: idPlusBtn), borderWidth: 0, color:  Colors.SeparatorLight)
        
        Helper.setButton(button: mmjPlusBtn,view:mmjPlusBtnView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: false)
        Helper.setButton(button: bottomButton,view:buttonView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: false)
        Fonts.setPrimaryMedium(bottomButton)
        
        Helper.setUiElementBorderWithCorner(element: mmjPlusBtn, radius: Helper.setRadius(element: mmjPlusBtn), borderWidth: 0, color:  Colors.SeparatorLight)
        idUpdateLabel.text = StringConstants.Upload()
        mmjUpdateLabel.text = StringConstants.Upload()
        
        idUploadedLabel.isHidden = true
        idCheckMark.isHidden = true
        
        mmjUploadedLabel.isHidden = true
        mmjCheckMark.isHidden = true
        
        Helper.setButtonTitle(normal: StringConstants.Update().uppercased(), highlighted: StringConstants.Update().uppercased(), selected: StringConstants.Update().uppercased(), button: bottomButton)
        
        
    }
    
    
    
    
    
    /// updates the view
    ///
    /// - Parameter data: it is of type User model object
    func updateView(data: User){
        Helper.setImage(imageView: mmjCardImage, url: data.MMJCard.Url, defaultImage: #imageLiteral(resourceName: "UploadDefault"))
        Helper.setImage(imageView: idCardImage, url: data.IdCard.Url, defaultImage: #imageLiteral(resourceName: "UploadDefault"))
        
        // Update IdCard Label
        if data.IdCard.Verified {
            idUploadedLabel.text = StringConstants.Verified()
            
        }else{
            idUploadedLabel.text = StringConstants.Uploaded()
            
        }
        
        if data.MMJCard.Url.count == 0 && data.IdCard.Url.count == 0 {
            
            Helper.setButtonTitle(normal: StringConstants.AddCard(), highlighted: StringConstants.AddCard(), selected: StringConstants.AddCard(), button: bottomButton)
            
        }
        
        
        //Update MMJ Card Label
        if data.MMJCard.Verified {
            mmjUploadedLabel.text = StringConstants.Verified()
            
        }else{
            mmjUploadedLabel.text = StringConstants.Uploaded()
        }
        
        if data.MMJCard.Url == "" {
            mmjUploadedLabel.isHidden = true
            mmjCheckMark.isHidden = true
            
        }
        else {
            mmjUploadedLabel.isHidden = false
            mmjCheckMark.isHidden = false
            mmjUpdateLabel.text = StringConstants.UpdateLower()
        }
        if data.IdCard.Url == "" {
            idUploadedLabel.isHidden = true
            idCheckMark.isHidden = true
            idUpdateLabel.text = StringConstants.Upload()
        }
        else {
            idUploadedLabel.isHidden = false
            idCheckMark.isHidden = false
            idUpdateLabel.text = StringConstants.UpdateLower()
        }
        
    }
    
}

