//
//  Authentication.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Authentication: NSObject {
    
    var Email                               = ""
    var Name                                = ""
    var ProfilePic                          = ""
    var Id                                  = ""
    var LoginType:Int                       = 1
    var Password                            = ""
    var ZIPCode                             = ""
    var Phone                               = ""
    var CountryCode                         = Helper.setCountryCode()
    var DateOfBirth                         = Date()
    var ReferralCode                        = ""
    var ValidPhone                          = false
    var Otp                                 = ""
    var MMJUrl                              = ""
    var IDUrl                               = ""
}
