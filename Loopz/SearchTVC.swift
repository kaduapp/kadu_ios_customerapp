//
//  SearchTVC.swift
//  UFly
//
//  Created by 3 Embed on 28/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SearchTVC: UITableViewCell {

    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var searchIcon: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }

    /// initial viewsetup
    func initialSetup(){
       Fonts.setPrimaryMedium(storeName)
       Fonts.setPrimaryRegular(distance)
       searchIcon.tintColor = Colors.SeconderyText
        
       storeName.textColor = Colors.PrimaryText
       distance.textColor = Colors.PrimaryText
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    /// updates the item names
    ///
    /// - Parameter item: is of type item model object
    func setItem(item:Item) {
        storeName.text = item.Name
        distance.text = Helper.df2so(Double( item.Price))
    }
    
    /// updates the store names
    ///
    /// - Parameter store: is of type store model
    func setStore(store:Store) {
        storeName.text = store.Name
        distance.text = Helper.clipDigit(valeu: store.DistanceMiles, digits: 2) + " " + StringConstants.FarAway()
    }
}
