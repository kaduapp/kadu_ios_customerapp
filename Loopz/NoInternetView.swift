//
//  NoInternetView.swift
//  UFly
//
//  Created by 3Embed on 18/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class NoInternetView: UIView {

    @IBOutlet weak var noInternetText: UILabel!
    @IBOutlet weak var noInternetImage: UIImageView!
    
    static var obj: NoInternetView? = nil
    static var shared: NoInternetView {
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[1] as? NoInternetView
            obj?.frame = UIScreen.main.bounds
        }
        return obj!
    }

    @IBAction func closeButtonAction(_ sender: Any) {
        self.hideAlert()
    }
    
    private func setup() {
        let window:UIWindow = UIApplication.shared.delegate!.window!!
        window.addSubview(self)
        self.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.01, delay: 0.01, options: .beginFromCurrentState, animations: {() -> Void in
            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        }, completion: {(_ finished: Bool) -> Void in
        })
    }
    
    func showAlert() {
        setup()
        Helper.blurEffect(view:NoInternetView.obj!)
        noInternetText.text = StringConstants.NoInternet()
    }
    
    func hideAlert() {
        if (NoInternetView.obj != nil) {
            self.removeFromSuperview()
        }
    }
}
