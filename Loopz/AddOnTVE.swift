//
//  AddOnTVE.swift
//  DelivX
//
//  Created by 3Embed on 01/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension AddOnVC:UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return addOnVM.ArrayOfAddons.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if addOnVM.SelectedItem.AddOnGroups.count > section {
            return addOnVM.SelectedItem.AddOnGroups[section].AddOns.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddOnPopupCell.self), for: indexPath) as! AddOnPopupCell
        cell.setData(data: addOnVM.SelectedItem.AddOnGroups[indexPath.section],indexPath:indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    var title = ""
    var subTitle = ""
    let data = addOnVM.SelectedItem.AddOnGroups[section]
    title = addOnVM.SelectedItem.AddOnGroups[section].Name
    if data.Mandatory{
         title = "\(title)*"
        if data.multiple{
            if data.minimumLimit != 0 && data.maximumLimit != 0 {
             subTitle = "\(StringConstants.addonsMinLimitMessage()) \(data.minimumLimit) \(StringConstants.addonsMaxLimitMessage()) \(data.maximumLimit) \(StringConstants.addOnsOptions())"
            }
            else{
                if data.Limit == 1{
                subTitle = StringConstants.chooseOneOptionOnly()
                }
                else{
                subTitle =  "\(StringConstants.chooseUpTo()) \(data.Limit) \(StringConstants.addOnsOptions())"
                }
        }
        }
        else{
            
        subTitle = StringConstants.chooseOneOptionOnly()
        }
        
        }
    else{
         title = "\(title)"
        if data.multiple{
            if data.minimumLimit != 0 && data.maximumLimit != 0 {
                subTitle = "\(StringConstants.addonsMinLimitMessage()) \(data.minimumLimit) \(StringConstants.addonsMaxLimitMessage()) \(data.maximumLimit) \(StringConstants.addOnsOptions())"
            }
            else{
                if data.Limit == 1{
                    subTitle = StringConstants.chooseOneOptionOnly()
                }
                else{
                    subTitle =  "\(StringConstants.chooseUpTo()) \(data.Limit) \(StringConstants.addOnsOptions())"
                }
            }
        }
        else{
            subTitle = StringConstants.chooseOneOptionOnly()
        }
       }
        
    let header:HeaderView = Helper.showAddonHeader(title: title, subTitle: subTitle, showViewAll: false, showSeperator: false) as! HeaderView

//        Helper.updateText(text: data.Name + " (\(data.SelectedCount)/\(data.Limit))", subText: " (\(data.SelectedCount)/\(data.Limit))", header.headerTitle, color: Colors.SecoundPrimaryText, link: "")
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var dataGroup:AddOnGroup = addOnVM.SelectedItem.AddOnGroups[indexPath.section]
        if dataGroup.AddOns[indexPath.row].Selected {
            if !(dataGroup.Mandatory) || dataGroup.multiple{
                addOnVM.SelectedItem.AddOnGroups[indexPath.section].AddOns[indexPath.row].Selected = false
                addOnVM.SelectedItem.AddOnGroups[indexPath.section].SelectedCount = addOnVM.SelectedItem.AddOnGroups[indexPath.section].SelectedCount - 1
                var mySet: IndexSet = IndexSet.init()
                mySet.insert(indexPath.section)
                tableView.reloadSections(mySet, with: .fade)
            }else{
                if (dataGroup.Mandatory && !dataGroup.multiple){
                    var timePass = 0
                    for i in 0..<addOnVM.SelectedItem.AddOnGroups[indexPath.section].AddOns.count{
                        if addOnVM.SelectedItem.AddOnGroups[indexPath.section].AddOns[i].Selected == true{
                            timePass += 1
                        }
                    }
                    if timePass > 1 {
                        addOnVM.SelectedItem.AddOnGroups[indexPath.section].AddOns[indexPath.row].Selected = false
                        addOnVM.SelectedItem.AddOnGroups[indexPath.section].SelectedCount = addOnVM.SelectedItem.AddOnGroups[indexPath.section].SelectedCount - 1
                        var mySet: IndexSet = IndexSet.init()
                        mySet.insert(indexPath.section)
                        tableView.reloadSections(mySet, with: .fade)
                    }
                }
            }
        }else{
            
            if dataGroup.Mandatory{
               
                if dataGroup.multiple{
                    if dataGroup.maximumLimit != 0 && dataGroup.maximumLimit == addOnVM.SelectedItem.AddOnGroups[indexPath.section].SelectedCount{
                        Helper.showAlert(message: "\(StringConstants.maximumLimitMessage()) \(dataGroup.maximumLimit) \(StringConstants.optionsOnly())", head: StringConstants.Message(), type:0 )
                        return
                    }
                    
                }
                else{
                  
                    for i in 0...dataGroup.AddOns.count - 1 {
                        addOnVM.SelectedItem.AddOnGroups[indexPath.section].AddOns[i].Selected = false
                        addOnVM.SelectedItem.AddOnGroups[indexPath.section].SelectedCount = 0
                        dataGroup.SelectedCount = 0
                    }
                }
                
            }
            else{
                if dataGroup.multiple{
                    if dataGroup.maximumLimit != 0 && dataGroup.maximumLimit == addOnVM.SelectedItem.AddOnGroups[indexPath.section].SelectedCount{
                        Helper.showAlert(message: "\(StringConstants.maximumLimitMessage()) \(dataGroup.maximumLimit) \(StringConstants.optionsOnly())", head: StringConstants.Message(), type:0 )
                        return
                    }
                    
                }
                else{
                    
                    
                }
                
            }
            
            
            
//            if dataGroup.Limit <= dataGroup.SelectedCount && dataGroup.Limit > 1 {
//                Helper.showAlert(message: StringConstants.MaxLimit() + dataGroup.Name , head: StringConstants.Warning(), type: 1)
//                return
//            }
//            if dataGroup.Limit == 1 {
//                for i in 0...dataGroup.AddOns.count - 1 {
//                    addOnVM.SelectedItem.AddOnGroups[indexPath.section].AddOns[i].Selected = false
//                    addOnVM.SelectedItem.AddOnGroups[indexPath.section].SelectedCount = 0
//                    dataGroup.SelectedCount = 0
//                }
//            }
        addOnVM.SelectedItem.AddOnGroups[indexPath.section].AddOns[indexPath.row].Selected = true
            addOnVM.SelectedItem.AddOnGroups[indexPath.section].SelectedCount = dataGroup.SelectedCount + 1
            var mySet: IndexSet = IndexSet.init()
            mySet.insert(indexPath.section)
            tableView.reloadSections(mySet, with: .fade)
        }
        
        var totalAmount:Float = self.addOnVM.SelectedItem.Price
        var previewText = ""
        for group in addOnVM.SelectedItem.AddOnGroups{
            for addOn in group.AddOns{
                if addOn.Selected{
                    totalAmount = totalAmount + addOn.Price
                    previewText = previewText + addOn.Name + ", "
                }
            }
        }
        
        if previewText.length > 0{
            let endIndex = previewText.index(previewText.endIndex, offsetBy: -2)
            previewText = String(previewText[..<endIndex])
        }
        
        confirmPrice.text = StringConstants.Item() + " " + StringConstants.total().lowercased() + " " + Helper.df2so(Double(totalAmount))
        
        selectedAddOnsPreviewLabel.text = previewText
    }
    
}
extension AddOnVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        i
//        if scrollView.contentOffset.y > 5{
//            Helper.setShadow(sender: hea)
//        } else{
//            Helper.removeShadowToNavigationBar(controller: self.navigationController!)
//        }
    }
}
