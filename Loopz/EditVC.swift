//
//  EditVC.swift
//  DelivX
//
//  Created by 3 Embed on 06/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import PullToDismiss

class EditVC: UIViewController {
    
    
    @IBOutlet weak var editView: EditView!
    @IBOutlet weak var editScrollView: UIScrollView!
    var pullToDismiss:PullToDismiss? = nil
    var uploadImageModel  = UploadImageModel.shared
    var authentication = Authentication()
    var editProfileVM = EditProfileVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        setNavigationBar()
        pullToDismiss = PullToDismiss.init(scrollView: editScrollView)
        pullToDismiss?.delegate = self
//        Helper.PullToClose(scrollView:editScrollView)
//        Helper.pullToDismiss?.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Helper.transparentNavigation(controller: self.navigationController!)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.addObserver()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        Helper.nonTransparentNavigation(controller: self.navigationController!)
        self.removeObserver()
        self.view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if editProfileVM.isSaveAction {
            editProfileVM.isSaveAction = false
            editProfileVM.isValidEmail = false
            editProfileVM.isValidPhone = false
        }
        //self.editView.phoneNumTF.becomeFirstResponder()
    }
    
    @IBAction func profileImageAction(_ sender: Any) {
        // ImageChosenTag
        var flag = 0
        var imageChosen:UIImage? = nil
        imageChosen = editView.pickedProfileImage
        
        if imageChosen == nil {
            flag = 0
        }
        else {
            flag = 1
        }
        self.editProfileVM.pickerObj?.setProfilePic(controller: self , tag: flag)
        
    }
    
    @IBAction func closeAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.dismissKeyboard()
        }) { (Bool) in
            UIView.animate(withDuration: 0.3, animations: {
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    @IBAction func countryPickerAction(_ sender: Any) {
        
        let vnhStoryboard = UIStoryboard(name: UIConstants.ControllerIds.VNHCountryPicker, bundle: nil)
        if let nav: UINavigationController = vnhStoryboard.instantiateInitialViewController() as! UINavigationController? {
            let picker: VNHCountryPicker = nav.viewControllers.first as! VNHCountryPicker
            // delegate
            picker.delegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    @IBAction func updateAction(_ sender: Any) {
        dismissKeyboard()
        editProfileVM.authentication.Email = editView.emailTF.text!
        let phoneNumber = editView.phoneNumTF.text?.replacingOccurrences(of: editProfileVM.authentication.CountryCode, with: "")
        editProfileVM.authentication.Phone = phoneNumber!
        //editProfileVM.authentication.Name = editView.nameTF.text!
        //self.dismiss(animated: true, completion: nil)
        if (editProfileVM.authentication.Email != editProfileVM.userData?.Email || editProfileVM.authentication.ProfilePic != editProfileVM.userData?.userProfilePic || editProfileVM.authentication.Phone != editProfileVM.userData?.Phone  || (editView.phNumTickMark.isHidden == false && editView.emailTickMark.isHidden == false ) )
        {
            self.editProfileVM.profileUpdated = false
            self.editProfileVM.isSaveAction = true
            self.editProfileVM.validateData()
            
            
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == UIConstants.SegueIds.EditProfileToOTP {
            if let otpVC:OTPVC = segue.destination as? OTPVC {
                otpVC.otpVM.authentication = editProfileVM.authentication
                otpVC.otpVM.isFromUpdateProfile = true
            }
        }
    }
    
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
        
    }
}
extension EditVC: ImagePickerModelDelegate {
    
    
    /// validates phone number
    func didValidatePhone(){
        EditProfileVM.EditProfileVM_response.subscribe(onNext: {success in
            self.subscribe(data: success)
        }, onError: { error in
            print(error)
        }).disposed(by: self.editProfileVM.disposeBag)
        
        uploadImageModel.UploadImageRespose.subscribe(onNext: { success in
            if success == .EditVC {
               // self.editProfileVM.validateData()
            }
            
        }).disposed(by: self.editProfileVM.disposeBag)
    }
    
    
    ///
    ///
    /// - Parameter data: it is of type enum response type
    func subscribe(data: EditProfileVM.ResponseType){
        switch data {
        case .validPhoneNum:
            editProfileVM.validateData()
            break
        case .validEmail:
            editProfileVM.validateData()
            break
        case .updateProfile:
            if self.editProfileVM.isSaveAction {
                self.dismiss(animated: true, completion: nil)
            }
            break
        case .updatePhone:
            if editProfileVM.isValidPhone && editProfileVM.isValidEmail {
                performSegue(withIdentifier:  UIConstants.SegueIds.EditProfileToOTP, sender: self)
            }
            break
        case .Logout:
            break
        }
    }
    
    
    func didPickImage(selectedImage: UIImage , tag:Int) {
        if tag == 0 {
            //Image Removed
            editProfileVM.authentication.ProfilePic = ""
            editView.pickedProfileImage = nil
            editView.profileImage.image = #imageLiteral(resourceName: "UserDefault")
        }
        else {
            //Image Added
            editView.pickedProfileImage = selectedImage
            editView.profileImage.image = selectedImage
            uploadImage()
        }
        
    }
    
    func uploadImage(){
        editProfileVM.authentication.ProfilePic = Helper.uploadImage(data:editView.pickedProfileImage!,path:AmazonKeys.ProfilePic, page: .EditVC)
    }
}
// MARK: - VNHCountryPickerDelegate
extension EditVC: VNHCountryPickerDelegate, UIScrollViewDelegate {
    func didPick(country: VNHCounty) {
        editView.phoneNumTF.text = country.dialCode
        editProfileVM.authentication.CountryCode = country.dialCode
    }
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        let movedOffset: CGFloat = sender.contentOffset.y
        if sender == editScrollView {
            if movedOffset > 0 {
                sender.contentOffset.y = 0
            }
        }
    }
}

