//
//  ReviewCVE.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation


extension ReviewVC: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfTip.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TipCVC = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.TipCVC, for: indexPath) as! TipCVC
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(self.selectTip) , for: UIControl.Event.touchUpInside)
        cell.setData(data: arrayOfTip[indexPath.row], selected: reviewVM.selectTip)
        cell.setCell()
        return cell
    }
    
    @objc func selectTip(sender:UIButton) {
        if sender.isSelected {
            reviewVM.selectTip = 0
        }else{
            reviewVM.selectTip = Float(arrayOfTip[sender.tag])
        }
        let cell = mainCollectionView.cellForRow(at: IndexPath.init(item: 1, section: 0)) as? DriverTipTVC
        cell?.tipCV.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize.zero
        let cellWidth = collectionView.frame.size.height + 20
        let cellHight = collectionView.frame.size.height
        size.height =  cellHight
        size.width  =  cellWidth
        return size
    }
}
