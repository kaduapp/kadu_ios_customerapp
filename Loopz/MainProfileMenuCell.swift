//
//  CheckOutMenuListCell.swift
//  UFly
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class MainProfileMenuCell: UITableViewCell {
    
    @IBOutlet weak var menuListImage: UIButton!
    @IBOutlet weak var menuListbackgroundView: UIView!
    @IBOutlet weak var menuTitle: UILabel!
    @IBOutlet weak var menuListSeperator: UIView!
    @IBOutlet weak var cellSeparator: UIView!
    @IBOutlet weak var tableViewHight: NSLayoutConstraint!
    @IBOutlet weak var cellBtn: UIButton!
    
    @IBOutlet weak var menuTitleLeading: NSLayoutConstraint!
    @IBOutlet weak var iconWidth: NSLayoutConstraint!
    @IBOutlet weak var cellSeparatorHight: NSLayoutConstraint!
    @IBOutlet weak var logoutImage: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    
   // @IBOutlet weak var rightArrowBtn: UIButton!
    
    var show = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    /// initial view setup
    func initialSetup() {
        
        tableViewHight.constant = 0
        menuTitle.textColor =  Colors.PrimaryText
        versionLabel.textColor = Colors.PrimaryText
        //Fonts.setPrimaryRegular(menuTitle)
        Fonts.setPrimaryRegular(menuTitle, size: 14)
        Fonts.setPrimaryRegular(versionLabel, size: 14)
        cellSeparator.isHidden = true
        menuListSeperator.isHidden = true
        cellBtn.isHidden = true

        menuListSeperator.backgroundColor =  Colors.SeparatorLight
        cellSeparator.backgroundColor =  Colors.ScreenBackground
        menuListImage.tintColor = Colors.PrimaryText
        logoutImage.tintColor = Colors.PrimaryText
        Fonts.setPrimaryRegular(logoutImage)
       // rightArrowBtn.tintColor =  Colors.PrimaryText
    }
    
    /// this method update the profile menu
    ///
    /// - Parameter menu: it is of type ProfileModel Object
    func updateProfileCell(menu:ProfileModel) {
        showPaymentMethods(showPayment: false)
        menuTitle.text = menu.menuTitle
        menuListImage.setImage(menu.menuIcon, for: .normal)
         versionLabel.isHidden = true
         menuTitle.isHidden = false
    }
    
    
    func setVersionNumber(version:String) {
        showPaymentMethods(showPayment: false)
        self.menuTitleLeading.constant = 0
        self.iconWidth.constant = 0
        self.logoutImage.isHidden = true
        menuTitle.isHidden = true
        menuListImage.isHidden = true
        versionLabel.isHidden = false
        versionLabel.text = version
    }
    
    /// shows the payment methods
    ///
    /// - Parameter showPayment: true - shows dropdown false - close dropdown
    func showPaymentMethods(showPayment: Bool){
        if showPayment == true {
            tableViewHight.constant = (CGFloat((AppConstants.PaymentMethods().count-1) * 30)+10)
            //self.accessoryType = UITableViewCellAccessoryType.none
        }
        else {
            tableViewHight.constant = 0
          //self.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


