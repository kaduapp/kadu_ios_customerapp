//
//  LaunchVM.swift
//  UFly
//
//  Created by 3 Embed on 08/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LaunchVM: NSObject{
    
    let launchVMResponse = PublishSubject<Bool>()
    var locationManager:LocationManager?   = nil
    var goneHome = false
    let disposeBag = DisposeBag()
    
    /// calls the guest login API method defined in AuthenticationAPICalls
    func callGuestLogin() {
        AuthenticationAPICalls.guestLoginAPI().subscribe(onNext: {[weak self]result in
            self?.launchVMResponse.onNext(true)
        }, onError: {error in
        }).disposed(by: disposeBag)
    }
    
    
    ///this method having subscription to locationZoneAPI defined in LocationAPICalls
    /// observes to response to collect ZoneId and ZoneName
    /// - Parameter location: loctaion it is of type Location model
    func getOperationZones(location: Location){
     LocationAPICalls.operationZoneAPI(location:location,launch:true).subscribe(onNext: {[weak self]data in
            if data.keys.count == 0 {
                return
            }
            let dataAddress:[String : Any] =    [
                GoogleKeys.PlaceId             : location.PlaceId,
                GoogleKeys.Description         : location.LocationDescription,
                GoogleKeys.MainText            : location.MainText,
                GoogleKeys.AddressName         : location.Name,
                GoogleKeys.AddressCity         : location.City,
                GoogleKeys.AddressState        : location.State,
                GoogleKeys.AddressZIP          : location.Zipcode,
                GoogleKeys.AddressCountry      : location.Country,
                GoogleKeys.LocationLat         : location.Latitude,
                GoogleKeys.LocationLong        : location.Longitude,
                GoogleKeys.ZoneId              : data[APIResponceParams.Id] as! String,
                GoogleKeys.ZoneName            : data[APIResponceParams.Title] as! String,
                GoogleKeys.CityID              : data[GoogleKeys.CityID] as! String,
                GoogleKeys.CityName            : data[GoogleKeys.CityName] as! String
            ]
            Utility.saveCurrency(symbol: data[APIResponceParams.CurrencySymbol] as! String, currency: data[APIResponceParams.Currency] as! String,milageMetric: data[APIResponceParams.MilageMetric] as! String)
            Utility.saveAddress(location: dataAddress)
            Utility.subscribeFCMTopicAll()
            AddressDBManger().insertAddress(data: dataAddress)
            CartDBManager().insertCartDocument(data:[])
            self?.launchVMResponse .onNext(true)
            
        }, onError: {error in
        }).disposed(by: disposeBag)
        
    }
}
