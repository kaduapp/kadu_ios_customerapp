//
//  MCDViewControllerUIE.swift
//  DelivX
//
//  Created by 3Embed on 22/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension RestHomeVC{
    
    
    func setUI(){
        
        
        
        let navView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 130, height: 40))
        navView.backgroundColor = UIColor.clear
        navTitleLabel.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width - 220, height: 40)
        navTitleLabel.backgroundColor = UIColor.clear
        navTitleLabel.textColor = UIColor.black
        navTitleLabel.font =  UIFont.systemFont(ofSize: 17)
        
        if RTL.shared.isRTL
        {
            backButton.frame = CGRect(x: navView.frame.size.width-30, y: 0, width: 40, height: 40)
            backButton.rotateButton()
        }else
        {
            backButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        }
        
        backButton.addTarget(self, action: #selector(backButtonAction), for:    .touchUpInside)
        backButton.setImage(UIImage(named: "BackIcon"), for: UIControl.State())
        
        navView.addSubview( backButton)
        navView.addSubview(navTitleLabel)
        
        let leftBarbuttonItem = UIBarButtonItem(customView: navView)
        let searchButtonItem = UIBarButtonItem(customView: self.searchButton)
        let favButtonItem = UIBarButtonItem(customView: self.favButton)
        self.navigationItem.leftBarButtonItem = leftBarbuttonItem
        self.navigationItem.rightBarButtonItems = [searchButtonItem,favButtonItem]
        
        self.navTitleLabel.text = restuarantsStoreVM.store.Name
        navSubTitle.textColor = Colors.PrimaryText
        menuButton.setTitle(StringConstants.Menu(), for: .normal)
        Helper.setButtonTitle(normal: StringConstants.Menu(), highlighted: StringConstants.Menu(), selected: StringConstants.Menu(), button:menuButton)
        menuButton.titleLabel?.textColor =  Colors.Red
        Helper.setShadow(sender: menuButton)
        menuButton.layer.cornerRadius = CGFloat(Helper.setRadius(element: menuButton))
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0)]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
        Helper.removeShadowToNavigationBar(controller: self.navigationController!)
        viewCartBar.backgroundColor =  Colors.AppBaseColor
        viewCartlabel.text = StringConstants.ViewCart()
        extraCharges.text = StringConstants.ExtraCharges()
   
    }
    

    /// hides the bottom bar
    ///
    /// - Parameter sender: is of UIButton type
    func hideBottom(sender: UIButton!) {
        if sender == viewCartBtn {
            UIView.animate(withDuration: 0.50) {
                self.viewCartBarHight.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    /// shows bottomcort bar if cartcount is greater than 0
    func showBottom() {
        if AppConstants.CartCount > 1 {
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Items()
        }else{
            itemCount.text = "\(AppConstants.CartCount) " + StringConstants.Item()
        }
        amount.text = Helper.df2so(Double(AppConstants.TotalCartPrice))
        var length = 0
        if AppConstants.CartCount > 0 {
            length = 50
        }
        UIView.animate(withDuration: 0.50) {
            self.viewCartBarHight.constant = CGFloat(length)
            //            self.layoutIfNeeded()
        }
    }
    
    
}
