//
//  WalletHomeTVE.swift
//  DelivX
//
//  Created by 3 Embed on 22/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension WalletHomeVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == cardArray.count {
            self.performSegue(withIdentifier: String(describing: AddNewCardVC.self), sender: self)
        }else{
            let array = [indexPath,IndexPath.init(row: selectedCard, section: 0)]
            selectedCard = indexPath.row
            tableView.reloadRows(at: array, with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaymentTableCell.self)) as! PaymentTableCell
        if indexPath.row == cardArray.count {
            cell.updateAddNewCard()
            cell.deleteButton.isHidden = true
        }else{
            cell.deleteButton.isHidden = false
            cell.setSelected(data: cardArray[indexPath.row], show: true, id: cardArray[selectedCard].Id)
            cell.setSelectedCard(data: cardArray[indexPath.row], id: cardArray[selectedCard].Id)
        }
        return cell
    }
    
    func addMoney(){
        self.title = ""
        let cell = mainTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! QuickCardTableCell
        addedAmount = cell.amountTF.text!
        if addedAmount.sorted().count != 0 {
            performSegue(withIdentifier: "QuickCardToPayment", sender: paymentMethord)
            
        }
        else {
            Helper.showAlert(message: StringConstants.Pleasementiontheamount(), head: StringConstants.Error(), type: 1)
        }
        
    }
    
    
}

extension WalletHomeVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.navigationController != nil {
            if scrollView.contentOffset.y > 0 {
                Helper.addShadowToNavigationBar(controller: self.navigationController!)
            }else{
                Helper.removeShadowToNavigationBar(controller: self.navigationController!)
            }
        }
    }
}





