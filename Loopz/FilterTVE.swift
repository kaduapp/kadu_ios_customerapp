//
//  FilterTVE.swift
//  DelivX
//
//  Created by 3Embed on 09/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import SwiftRangeSlider

extension FilterVC: UITableViewDelegate, UITableViewDataSource {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView.tag == 0{
            return 1
        }
        else{

//        if filterDetailVM.type == APIRequestParams.CategoryFilter || filterDetailVM.type == APIRequestParams.SubCategoryFilter || filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
//                if filterDetailVM.arrayOfCategory.count > 0 {
//                    return filterDetailVM.arrayOfCategory.count
//                }
//            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          if tableView.tag == 0{
            return filterVM.arrayOfFilter.count
        }
            
        else{
            
         switch filterDetailVM.type {
         case APIRequestParams.Sort,StringConstants.Sort():
           return sortArray.count
         case APIRequestParams.CategoryFilter,APIRequestParams.SubCategoryFilter,APIRequestParams.SubSubCategoryFilter  :
            if filterDetailVM.arrayOfCategory.count > 0 {
               return filterDetailVM.arrayOfCategory.count
//                if filterDetailVM.selectedSection == section {
//                    return filterDetailVM.arrayOfCategory[section].SubCategories.count
//                }else{
//                    return filterDetailVM.arrayOfCategory[section].SubCategories.count
//                }
            }
            
         case APIRequestParams.FilterPrice:
             return 1

         default:
             print ("")
            }
            
        return filterDetailVM.arrayOfTypes.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FilterTVC.self), for: indexPath) as! FilterTVC
            cell.titleLabel.text = filterVM.arrayOfFilter[indexPath.row]
            switch filterVM.arrayOfFilter[indexPath.row] {
            case StringConstants.Sort():
            cell.selectedFilterImage.isHidden = true
            if filterDetailVM.selectedSort >= 0{
            cell.selectedFilterImage.isHidden = false
            }

            case StringConstants.Categories():
                cell.selectedFilterImage.isHidden = true
                if filterDetailVM.selectedCategories.count > 0{
                    cell.selectedFilterImage.isHidden = false
                }
                
            case StringConstants.Brand():
                    cell.selectedFilterImage.isHidden = true
                    if  filterDetailVM.selectedBrands.count > 0{
                        cell.selectedFilterImage.isHidden = false
                    }
            
            case StringConstants.Price():
                cell.selectedFilterImage.isHidden = true
                if searchVM.minPrice > 0 || searchVM.maxPrice > 0 {
                    cell.selectedLabel.text = Helper.df2so(Double( searchVM.minPrice)) + " - " + Helper.df2so(Double( searchVM.maxPrice))
                    cell.accessoryType = .none
                    cell.selectedFilterImage.isHidden = false
                }else{
                    cell.selectedLabel.text = ""
                }
    
            case StringConstants.Manufacturer():
                 cell.selectedFilterImage.isHidden = true
                 if filterDetailVM.selectedManufacturer.count > 0{
                         cell.selectedFilterImage.isHidden = false
                    }
            case StringConstants.Offers():
                cell.selectedLabel.text = ""
                if searchVM.offer.length > 0 {
                }else{
                }
        
            case StringConstants.Rating():
                if filterVM.selectedRating > 0 {
                    cell.selectedLabel.text = "\(filterVM.selectedRating) " + StringConstants.andAbove()
                    cell.accessoryType = .none
                }else{
                    cell.selectedLabel.text = ""
                }
    
            case StringConstants.Cousines():
                 cell.selectedFilterImage.isHidden = true
                if filterDetailVM.selectedCuisines.count > 0 {
                    cell.selectedFilterImage.isHidden = false
                }
        
            case StringConstants.FoodType():
                 cell.selectedFilterImage.isHidden = true
                if filterVM.selectedFoodType.length > 0 {
                    cell.selectedLabel.text = filterVM.selectedFoodType
                     cell.selectedFilterImage.isHidden = false
                }else{
                    cell.selectedLabel.text = ""
                }
        
                
            default:
                break
            }
            
            if indexPath.row == selectedCellIndex{
               cell.contentView.backgroundColor = UIColor.white
            }
            else{
               cell.contentView.backgroundColor = Colors.SeparatorLarge
            }
            
        return cell
            
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FilterTVC.self), for: indexPath) as! FilterTVC
            
            switch filterDetailVM.type {
            case APIRequestParams.Sort,StringConstants.Sort():
                cell.titleLabel.text = sortArray[indexPath.row]
                var selected = false
                if self.filterDetailVM.selectedSort == indexPath.row {
                   selected = true
                }
             cell.setSelectedImage(type:filterDetailVM.type,isSelected:selected )
                return cell
                
            case APIRequestParams.FilterPrice:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PriceRangeTVC.self), for: indexPath) as! PriceRangeTVC
                if filterDetailVM.maxValue > 0 {
                    cell.rangeBar.minimumValue = Double(filterDetailVM.minValue)
                    cell.rangeBar.maximumValue = Double(filterDetailVM.maxValue)
                }else{
                    cell.rangeBar.minimumValue = Double(filterDetailVM.minValue)
                    cell.rangeBar.maximumValue = Double(filterDetailVM.selectedMaxValue)
                }
                var max = filterDetailVM.selectedMaxValue
                if filterDetailVM.selectedMaxValue <= 0 {
                    max = filterDetailVM.maxValue
                }
                cell.setData(min: filterDetailVM.selectedMinValue, max: max)
                cell.rangeBar.upperValue = Double(max)
                cell.rangeBar.lowerValue = Double(filterDetailVM.selectedMinValue)
                cell.rangeBar.delegate = self
                return cell
                
            case APIRequestParams.CategoryFilter,APIRequestParams.SubCategoryFilter,APIRequestParams.SubSubCategoryFilter:
//                cell.titleLabel.text = filterDetailVM.arrayOfCategory[indexPath.section].SubCategories[indexPath.row].Name
              cell.titleLabel.text = filterDetailVM.arrayOfCategory[indexPath.row].Name

                var selected = false
              
              if filterDetailVM.selectedCategories.contains(filterDetailVM.arrayOfCategory[indexPath.row].Name){
                  selected = true
                }

            cell.setSelectedImage(type:filterDetailVM.type,isSelected:selected )
                
            case APIRequestParams.BrandFilter:
                cell.titleLabel.text = filterDetailVM.arrayOfTypes[indexPath.row]
                var selected = false
                if filterDetailVM.selectedBrands.contains(filterDetailVM.arrayOfTypes[indexPath.row]){
                    selected = true
                }
            cell.setSelectedImage(type:filterDetailVM.type,isSelected:selected )
                
            case APIRequestParams.ManufacturerFilter:
                cell.titleLabel.text = filterDetailVM.arrayOfTypes[indexPath.row]
                var selected = false
                if filterDetailVM.selectedManufacturer.contains(filterDetailVM.arrayOfTypes[indexPath.row]){
                    selected = true
                }
                cell.setSelectedImage(type:filterDetailVM.type,isSelected:selected )
                
                
            default:
                if filterDetailVM.type == StringConstants.Rating() || filterDetailVM.type == StringConstants.CostForTwo() || filterDetailVM.type == StringConstants.FoodType() || filterDetailVM.type == StringConstants.Cousines() {
                        switch filterDetailVM.type {
                        case StringConstants.Rating():
                            var selected = false
                            if filterDetailVM.selectedRating == indexPath.row + 1 {
                                selected = true
                            }
                       cell.setSelectedImage(type:filterDetailVM.type,isSelected:selected )

                        case StringConstants.Cousines():
                         cell.titleLabel.text = filterDetailVM.arrayOfTypes[indexPath.row]
                            var selected = false
                            if filterDetailVM.selectedCuisines.contains(filterDetailVM.arrayOfTypes[indexPath.row]){
                                selected = true
                            }
                    cell.setSelectedImage(type:filterDetailVM.type,isSelected:selected )
                        
                        case StringConstants.FoodType():
                            var selected = false
                            if filterDetailVM.selectedFoodType == filterDetailVM.arrayOfTypes[indexPath.row] {
                                selected = true
                            }
                     cell.setSelectedImage(type:filterDetailVM.type,isSelected:selected )
                            break
                        default:
                            break
                        }
                    }
                    
                    cell.titleLabel.text = filterDetailVM.arrayOfTypes[indexPath.row]
                break
            }
              return cell
             }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            if tableView.tag == 0{

             selectedCellIndex = indexPath.row
            let type = self.filterVM.arrayOfFilter[indexPath.row]
            switch type {
            case StringConstants.Sort():
                filterDetailVM.title = filterVM.arrayOfFilter[indexPath.row]
                filterDetailVM.type = APIRequestParams.Sort
                filterDetailVM.needle = text
                filterDetailVM.needleLanguage = textLanguage
                filterDetailVM.chosenCat = selectedCat
                filterDetailVM.chosenSubCat = selectedSubCat
                filterDetailVM.storeFilter = filterVM.storeFilters
                filterDetailVM.offer = searchVM.offer
                self.filterDetailVM.getFilter()
                
            case StringConstants.Brand():
                
                
                filterDetailVM.title = filterVM.arrayOfFilter[indexPath.row]
                filterDetailVM.type = APIRequestParams.BrandFilter
                filterDetailVM.needle = text
                filterDetailVM.needleLanguage = textLanguage
                filterDetailVM.chosenCat = selectedCat
                filterDetailVM.chosenSubCat = selectedSubCat
                filterDetailVM.storeFilter = filterVM.storeFilters
                filterDetailVM.offer = searchVM.offer
                self.filterDetailVM.getFilter()
                
            case StringConstants.Categories():
                filterDetailVM.title = filterVM.arrayOfFilter[indexPath.row]
                filterDetailVM.type = APIRequestParams.CategoryFilter
                filterDetailVM.needle = text
                filterDetailVM.needleLanguage = textLanguage
                filterDetailVM.chosenCat = selectedCat
                filterDetailVM.chosenSubCat = selectedSubCat
                filterDetailVM.storeFilter = filterVM.storeFilters
                filterDetailVM.offer = searchVM.offer
                self.filterDetailVM.getFilter()
                
            case StringConstants.Price():
                
                filterDetailVM.title = filterVM.arrayOfFilter[indexPath.row]
                filterDetailVM.type = APIRequestParams.FilterPrice
                filterDetailVM.needle = text
                filterDetailVM.needleLanguage = textLanguage
                filterDetailVM.chosenCat = selectedCat
                filterDetailVM.chosenSubCat = selectedSubCat
                filterDetailVM.storeFilter = filterVM.storeFilters
                filterDetailVM.offer = searchVM.offer
                self.filterDetailVM.getFilter()
                
            case StringConstants.Manufacturer():
                filterDetailVM.title = filterVM.arrayOfFilter[indexPath.row]
                filterDetailVM.type = APIRequestParams.ManufacturerFilter
                filterDetailVM.needle = text
                filterDetailVM.needleLanguage = textLanguage
                filterDetailVM.chosenCat = selectedCat
                filterDetailVM.chosenSubCat = selectedSubCat
                filterDetailVM.storeFilter = filterVM.storeFilters
                filterDetailVM.offer = searchVM.offer
                self.filterDetailVM.getFilter()
                
            case StringConstants.Rating(),StringConstants.FoodType(),StringConstants.CostForTwo(),StringConstants.Cousines():
                filterDetailVM.title = filterVM.arrayOfFilter[indexPath.row]
                filterDetailVM.type = filterVM.arrayOfFilter[indexPath.row]
                filterDetailVM.needle = text
                filterDetailVM.needleLanguage = textLanguage
                filterDetailVM.chosenCat = selectedCat
                filterDetailVM.chosenSubCat = selectedSubCat
                filterDetailVM.storeFilter = filterVM.storeFilters
                filterDetailVM.offer = searchVM.offer
                self.filterDetailVM.getFilter()
                
            case StringConstants.Offers():
                
                if searchVM.offer != APIRequestParams.FIlterOffer {
                    searchVM.offer = APIRequestParams.FIlterOffer
                }else{
                    searchVM.offer = ""
                }
            default:
                break
            }
           tableView.reloadData()
       }
        else{
            tableView.deselectRow(at: indexPath, animated: true)
            switch filterDetailVM.type {
                
            case APIRequestParams.FilterPrice:
                print ("Price")
                
            case APIRequestParams.Sort :
                filterDetailVM.type = APIRequestParams.Sort
                filterDetailVM.needle = text
                filterDetailVM.needleLanguage = textLanguage
                filterDetailVM.chosenCat = selectedCat
                filterDetailVM.chosenSubCat = selectedSubCat
                filterDetailVM.storeFilter = filterVM.storeFilters
                filterDetailVM.offer = searchVM.offer
                filterDetailVM.selectedSort = indexPath.row
                
           case APIRequestParams.BrandFilter :
         if filterDetailVM.selectedBrands.contains(filterDetailVM.arrayOfTypes[indexPath.row]) {
                filterDetailVM.selectedBrands.remove(at: filterDetailVM.selectedBrands.index(of: filterDetailVM.arrayOfTypes[indexPath.row])!)
                }else{
              filterDetailVM.selectedBrands.append(filterDetailVM.arrayOfTypes[indexPath.row])
                }
            
            case  APIRequestParams.ManufacturerFilter :
                
                if filterDetailVM.selectedManufacturer.contains(filterDetailVM.arrayOfTypes[indexPath.row]) {
                    filterDetailVM.selectedManufacturer.remove(at: filterDetailVM.selectedManufacturer.index(of: filterDetailVM.arrayOfTypes[indexPath.row])!)
                }else{
                    filterDetailVM.selectedManufacturer.append(filterDetailVM.arrayOfTypes[indexPath.row])
                }
                
                
            case APIRequestParams.CategoryFilter,StringConstants.Categories() :
                if filterDetailVM.selectedCategories.contains(filterDetailVM.arrayOfCategory[indexPath.row].Name) {
                    filterDetailVM.selectedCategories.remove(at: filterDetailVM.selectedCategories.index(of: filterDetailVM.arrayOfCategory[indexPath.row].Name)!)
                }else{
                filterDetailVM.selectedCategories.append(filterDetailVM.arrayOfCategory[indexPath.row].Name)
                }

            case StringConstants.Rating(),StringConstants.FoodType(),StringConstants.CostForTwo(),StringConstants.Cousines():
                
                switch filterDetailVM.type {
                case StringConstants.Rating():
                    if filterDetailVM.selectedRating == indexPath.row + 1 {
                        filterDetailVM.selectedRating = 0
                    }else{
                        filterDetailVM.selectedRating = indexPath.row + 1
                    }
                    break
                case StringConstants.Cousines():
                    
                    if filterDetailVM.selectedCuisines.contains(filterDetailVM.arrayOfTypes[indexPath.row]) {
                        filterDetailVM.selectedCuisines.remove(at: filterDetailVM.selectedCuisines.index(of: filterDetailVM.arrayOfTypes[indexPath.row])!)
                    }else{
                        filterDetailVM.selectedCuisines.append(filterDetailVM.arrayOfTypes[indexPath.row])
                    }
            
                case StringConstants.FoodType():
                    if filterDetailVM.selectedFoodType == filterDetailVM.arrayOfTypes[indexPath.row] {
                        filterDetailVM.selectedFoodType = ""
                    }else{
                        filterDetailVM.selectedFoodType = filterDetailVM.arrayOfTypes[indexPath.row]
                    }
                    break
                default:
                    break
                }
                
                
            default:
            
                break
            }
            
             self.subFiltersTableView.reloadData()
             self.mainTableView.reloadData()
            
//
//            if filterDetailVM.type == APIRequestParams.SubCategoryFilter {
//                if filterDetailVM.SubCatSelected == filterDetailVM.arrayOfCategory[indexPath.section].SubCategories[indexPath.row].Name {
//                    filterDetailVM.SubCatSelected = ""
//                }else{
//                    filterDetailVM.SubCatSelected = filterDetailVM.arrayOfCategory[indexPath.section].SubCategories[indexPath.row].Name
//                }
//                if filterDetailVM.SubCatSelected.length > 0 {
//                    if filterDetailVM.arrayOfCategory[indexPath.section].SubCategories[indexPath.row].SubSubCat.count == 0 {
//                        filterDetailVM.chosenSubCat = filterDetailVM.SubCatSelected
//                        filterDetailVM.type = APIRequestParams.SubSubCategoryFilter
//                        filterDetailVM.getFilter()
//                    }else{
//                        self.performSegue(withIdentifier: String(describing: FilterSubCatVC.self), sender: self)
//                    }
//                }else{
//                    filterDetailVM.type = APIRequestParams.SubCategoryFilter
//                    filterDetailVM.getFilter()
//                }
//                //searchVM.filterInfo.onNext((true,APIRequestParams.CategoryFilter,[filterDetailVM.selectedCategory,filterDetailVM.SubCatSelected,""]))
//                tableView.reloadData()
//            }else if filterDetailVM.type == StringConstants.Rating() || filterDetailVM.type == StringConstants.CostForTwo() || filterDetailVM.type == StringConstants.FoodType() || filterDetailVM.type == StringConstants.Cousines() {
//                switch filterDetailVM.type {
//                case StringConstants.Rating():
//                    if filterDetailVM.selectedRating == indexPath.row + 1 {
//                        filterDetailVM.selectedRating = 0
//                    }else{
//                        filterDetailVM.selectedRating = indexPath.row + 1
//                    }
//                    break
//                case StringConstants.Cousines():
//                    if filterDetailVM.selectedCosine == filterDetailVM.arrayOfTypes[indexPath.row] {
//                        filterDetailVM.selectedCosine = ""
//                    }else{
//                        filterDetailVM.selectedCosine = filterDetailVM.arrayOfTypes[indexPath.row]
//                    }
//                    break
//                case StringConstants.FoodType():
//                    if filterDetailVM.selectedFoodType == filterDetailVM.arrayOfTypes[indexPath.row] {
//                        filterDetailVM.selectedFoodType = ""
//                    }else{
//                        filterDetailVM.selectedFoodType = filterDetailVM.arrayOfTypes[indexPath.row]
//                    }
//                    break
//                default:
//                    break
//                }
//        //        searchVM.filterInfo.onNext((true,filterDetailVM.type,["\(filterDetailVM.selectedRating)",filterDetailVM.selectedFoodType,filterDetailVM.selectedCosine]))
//                tableView.reloadData()
//            }
    

        }



}

}



    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        var headerView:HeaderView? = nil
//        var stringHead = ""
//        switch section {
//        case 0:
//            stringHead = StringConstants.Filter()
//        case 1:
//            stringHead = StringConstants.Sort()
//
//        default:
//            stringHead = ""
//        }
//        headerView = Helper.showHeader(title: stringHead,showViewAll: false) as? HeaderView
//        return headerView
//
//

//        let head = Helper.showHeader(title: filterDetailVM.arrayOfCategory[section].Name, showViewAll: true) as! HeaderView
//        head.fullButton.isHidden = false
//        head.fullButton.tag = section
//
//        head.viewAllArrow.setImage(nil, for: .normal)
//        head.headerTitle.textColor = Colors.HeaderTitle
//        if filterDetailVM.selectedCategory == filterDetailVM.arrayOfCategory[section].Name {
//            head.headerTitle.textColor = Colors.AppBaseColor
//            if filterDetailVM.SubCatSelected == "" {
//                head.viewAllArrow.setImage(#imageLiteral(resourceName: "CheckIcon"), for: .normal)
//            }
//        }
//        head.viewAllText.setTitle("", for: .normal)
//        head.fullButton.addTarget(self, action:#selector(updateSubCat) , for: UIControl.Event.touchUpInside)
//        head.backgroundColor = UIColor.white
//        return head
//    }

//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 0
//    }
//}





//****************************************

    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if filterDetailVM.type == APIRequestParams.CategoryFilter || filterDetailVM.type == APIRequestParams.SubCategoryFilter || filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
//            if filterDetailVM.arrayOfCategory.count > 0 {
//                return 50
//            }
//        }
//        return 0
//    }
//    @objc func updateSubCat(sender:UIButton) {
//        if filterDetailVM.selectedSection == sender.tag {
//            filterDetailVM.selectedSection = -1
//            filterDetailVM.SubCatSelected = ""
//            filterDetailVM.selectedCategory = ""
//        }else{
//            filterDetailVM.selectedCategory = filterDetailVM.arrayOfCategory[sender.tag].Name
//            filterDetailVM.SubCatSelected = ""
//            filterDetailVM.selectedSection = sender.tag
//            if filterDetailVM.arrayOfCategory[sender.tag].SubCategories.count == 0 {
//                filterDetailVM.type = APIRequestParams.SubCategoryFilter
//                filterDetailVM.chosenCat = filterDetailVM.selectedCategory
//                filterDetailVM.getFilter()
//            }
//        }
//        searchVMference.filterInfo.onNext((true,filterDetailVM.type,[filterDetailVM.selectedCategory,filterDetailVM.SubCatSelected,filterDetailVM.SubSubCatSelected]))
//        mainTableView.reloadData()
//    }






extension FilterVC:SliderDelegate {
    
    func slider(_ rangeSlider: RangeSlider, didUpdate ratingMin: Float, ratingMax: Float) {
        let cell = subFiltersTableView.cellForRow(at: IndexPath.init(item: 0, section: 0)) as! PriceRangeTVC
        cell.setData(min: ratingMin, max: ratingMax)
        self.filterDetailVM.selectedMinValue = ratingMin
          self.filterDetailVM.selectedMaxValue = ratingMax
    }
}




