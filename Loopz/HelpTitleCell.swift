//
//  HelpTitleCell.swift
//  DelivX
//
//  Created by 3 Embed on 11/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HelpTitleCell: UITableViewCell {
    
    @IBOutlet weak var helpTitle: UILabel!
    @IBOutlet weak var rightArrow: UIButton!
    @IBOutlet weak var separator: UIView!
    //    @IBOutlet weak var termsOfUse: UIView!
    @IBOutlet weak var termsOfUseLabel: UILabel!
    @IBOutlet weak var termsOfUseHight: NSLayoutConstraint!
    @IBOutlet weak var readMoreBtnHight: NSLayoutConstraint!
    @IBOutlet weak var readMoreBtn: UIButton!
    var didSelectRow = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        Fonts.setPrimaryRegular(helpTitle)
        helpTitle.textColor = Colors.PrimaryText
        if rightArrow != nil {
            rightArrow.tintColor = Colors.PrimaryText
        }
        separator.backgroundColor =  Colors.SeparatorLight
    }
    func updateCell1(){
        helpTitle.text = StringConstants.IssueWithPrevious()
        separator.isHidden = true
    }
    
    func updateCell(data:Support){
        separator.isHidden = false
//        helpTitle.text = HelpQuery.helpQuery[index]
        helpTitle.text = data.Title
    }
    
    
    // for HelpDetailVC
    func updateHelpOrdeQ(index: Int){
        helpTitle.text = AppConstants.orderHelp()[index]
        if index == AppConstants.orderHelp().count - 1 {
            separator.isHidden = true
        }
        else {
            separator.isHidden = false
        }
    }
    
    /// updates list of the general queries
    func updateGeneralQuery(index: Int){
        separator.isHidden = false
        helpTitle.text = AppConstants.GeneralQuery()[index]
    }
    
    //updates the list of leagaQuetirs
    func updateLegalQuery(index: Int,flag: Bool){
        helpTitle.text = AppConstants.Legal()[index]
        separator.isHidden = false
        
        didSelectRow = flag
        updateTermsOfUse()
    }
    
    func updateTermsOfUse(){
        termsOfUseLabel.textColor =  Colors.SeconderyText
        separator.isHidden = false
        
        if didSelectRow == true {
            termsOfUseHight.constant = 50
            termsOfUseLabel.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamcolaboris nisi ut aliquip ex ea commodo consequat. Duis auteirure dolor in reprehenderit in voluptate velit esse cillum doloreeu fugiat nulla pariatur. Excepteur sint occaecat cupidatat nonproident, sunt in culpa qui officia deserunt mollit anim id estlaborum. "
            didSelectRow = false
            readMoreBtnHight.constant = 40
            
        }
        else {
            termsOfUseHight.constant = 0
            termsOfUseLabel.text = ""
            didSelectRow = true
            readMoreBtnHight.constant = 0
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
