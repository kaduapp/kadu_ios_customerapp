//
//  AddNewCardVC.swift
//  UFly
//
//  Created by 3 Embed on 17/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import Stripe

class AddNewCardVC: UIViewController {
    
    @IBOutlet weak var addNewCardView: AddNewCardView!
    
    var addCardVM = AddNewCardVM()
    
    let textCardLength = 0
    var maxcardLength = 19
    var maxCvvLength = 4
    var doneButtonPressed = false
    var navView = NavigationView().shared

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.addNewCardView.cardNumberTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.addNewCardView.expiresTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.addNewCardView.cvvTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        if self.navigationController != nil{
            self.navigationItem.titleView = navView
            navView.viewWidth.constant = UIScreen.main.bounds.width - 100
            navView.setData(title: StringConstants.AddNewCard())
        }
        self.setUpVM()
        textFieldDidChange(self.addNewCardView.cardNumberTF)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        doneButtonPressed = false
        addObserver()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addNewCardView.cardNumberTF.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.view.endEditing(true)
        removeObserver()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func scan(_ sender: UIButton) {
        let scanCardViewController = CardIOPaymentViewController.init(paymentDelegate: self as CardIOPaymentViewControllerDelegate)
        scanCardViewController?.hideCardIOLogo = true
        scanCardViewController?.disableManualEntryButtons = true
        scanCardViewController?.modalPresentationStyle = UIModalPresentationStyle.formSheet
        scanCardViewController?.title = StringConstants.ScanCard()
        scanCardViewController?.navigationController?.isNavigationBarHidden = false
        
        self.present(scanCardViewController!, animated: true, completion: nil)
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        
        dismissKeyboard()
        if addNewCardView.cardNumberTF.text!.length == 0 {
            Helper.showAlert(message: StringConstants.CheckCardNumber(), head: StringConstants.Error(), type: 1)
            return
        }
        let data:String = addNewCardView.expiresTF.text!
        if addNewCardView.expiresTF.text!.length == 0 {
            Helper.showAlert(message: StringConstants.CheckCardExpiry(), head: StringConstants.Error(), type: 1)
            return
        }
        var token:[String] = data.components(separatedBy: "/")
        if token.count != 2 {
            Helper.showAlert(message: StringConstants.CheckCardExpiry(), head: StringConstants.Error(), type: 1)
            return
        }
        
        if addNewCardView.cvvTF.text!.length == 0 {
            Helper.showAlert(message: StringConstants.CheckCardCVV(), head: StringConstants.Error(), type: 1)
            return
        }
        Helper.showPI(string: "")
        let stripeCard = STPCardParams.init()
        stripeCard.number = addNewCardView.cardNumberTF.text!.replacingOccurrences(of: " ", with: "")
        stripeCard.cvc = addNewCardView.cvvTF.text!.replacingOccurrences(of: " ", with: "")
        
         token = data.components(separatedBy: "/")
        stripeCard.expYear = UInt(token[1].trimmingCharacters(in: .whitespaces))!
        stripeCard.expMonth = UInt(token[0])!
        
        if STPCardValidator.validationState(forCard: stripeCard) == STPCardValidationState.valid {
            STPAPIClient.shared().createToken(withCard: stripeCard) { (Token, error) in
                if error != nil {
                    Helper.hidePI()
                    Helper.showAlert(message: error.debugDescription, head: StringConstants.Error(), type: 1)
                }else{
                    print(Token?.tokenId as Any)
                    if !self.doneButtonPressed{
                        self.doneButtonPressed = true
                        self.addCardVM.addCard(card: (Token?.tokenId)!)
                    }
                }
            }
        }else{
            Helper.hidePI()
            Helper.showAlert(message: StringConstants.CheckCardData(), head: StringConstants.Error(), type: 1)
        }
    }
    
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    //return STPCardBrandVisa, STPCardBrandMasterCard, and STPCardBrandUnknown, respectively.
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        let data = STPCardValidator.brand(forNumber: addNewCardView.cardNumberTF.text!)
        maxcardLength = STPCardValidator.maxLength(for: data)
        maxCvvLength = Int(STPCardValidator.maxCVCLength(for: data))
        addNewCardView.cardImage.image = STPImageLibrary.brandImage(for: data)
        if addNewCardView.cardNumberTF.text!.replacingOccurrences(of: " ", with: "").length == maxcardLength {
            if textField == addNewCardView.cardNumberTF {
                addNewCardView.expiresTF.becomeFirstResponder()
            }
            addNewCardView.cardNumSeparator.backgroundColor =  Colors.AppBaseColor
        }else{
            addNewCardView.cardNumSeparator.backgroundColor =  Colors.Red
        }
        if addNewCardView.expiresTF.text!.replacingOccurrences(of: " ", with: "").length == 5 {
            if textField == addNewCardView.expiresTF {
                addNewCardView.cvvTF.becomeFirstResponder()
            }
            addNewCardView.expireTFSeparator.backgroundColor =  Colors.AppBaseColor
        }else{
            addNewCardView.expireTFSeparator.backgroundColor =  Colors.Red
        }
        if addNewCardView.cvvTF.text!.replacingOccurrences(of: " ", with: "").length == maxCvvLength {
            if textField == addNewCardView.cvvTF {
                addNewCardView.cvvTF.resignFirstResponder()
            }
            addNewCardView.cvvSeparator.backgroundColor =  Colors.AppBaseColor
        }else{
            addNewCardView.cvvSeparator.backgroundColor =  Colors.Red
        }
        
        if addNewCardView.cardNumberTF.text!.replacingOccurrences(of: " ", with: "").length == maxcardLength && addNewCardView.expiresTF.text!.replacingOccurrences(of: " ", with: "").length == 5 && addNewCardView.cvvTF.text!.replacingOccurrences(of: " ", with: "").length == maxCvvLength {
            Helper.setButton(button: addNewCardView.addBtn,view:addNewCardView.addCardView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
            addNewCardView.addBtn.isUserInteractionEnabled = true
        }else {
            Helper.setButton(button: addNewCardView.addBtn,view:addNewCardView.addCardView, primaryColour: Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor, shadow: true)
            addNewCardView.addBtn.isUserInteractionEnabled = false
        }
    }
}


//Extnsion For Subscription
extension AddNewCardVC {
    func setUpVM() {
        AddNewCardVM.addNewCard_response.subscribe(onNext: { (success) in
            if success == AddNewCardVM.ResponseType.AddCard {
                _ = self.navigationController?.popViewController(animated: true)
            }
        }).disposed(by: CardAPICalls.disposeBag)
    }
}

extension AddNewCardVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.navigationController != nil {
            if scrollView.contentOffset.y > 0 {
                Helper.addShadowToNavigationBar(controller: self.navigationController!)
            }else{
                Helper.removeShadowToNavigationBar(controller: self.navigationController!)
            }
        }
    }
}


// MARK: - CardIOPaymentViewControllerDelegate delegate
extension AddNewCardVC:CardIOPaymentViewControllerDelegate {
    //MARK: - Card IO Deleagte -
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        addNewCardView.cardNumberTF.text = cardInfo.cardNumber
        addNewCardView.cardImage.image = cardInfo.cardImage
        addNewCardView.expiresTF.text = "\(cardInfo.expiryMonth)/\(cardInfo.expiryYear - 2000)"
        addNewCardView.cvvTF.text = cardInfo.cvv
        self.doneButtonAction(addNewCardView.addBtn)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

