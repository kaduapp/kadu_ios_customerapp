//
//  UserDefaultConstants.swift
//  UFly
//
//  Created by 3Embed on 26/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

struct UserDefaultConstants {
    static let SavedAddress                                 = "SavedAddress"
    static let SavedStore                                   = "SavedStore"
    static let PushToken                                    = "FCMPushToken"
    static let ProfileData                                  = "ProfileData"
    static let IsLoggedIn                                   = "IsLoggedIn"
    static let GuestLoggin                                  = "IsGuestLoggedIn"
    static let GuestProfileData                             = "GuestProfileData"
    static let ReferralCode                                 = "referralCode"
    static let Currency                                     = "Currency"
    static let MilageMetric                                 = "milageMetric"
    static let CurrencySymbol                               = "CurrencySymbol"
    static let CurrentLatitude                              = "currentLatitude"
    static let CurrentLongitude                             = "currentLongitude"
    static let CurrentAddress                               = "currentAddress"
    static let WishList                                     = "wishList"
    static let CartId                                       = "CartId"
    static let Search                                       = "Search"
    static let AppConfig                                    = "AppConfig"
    static let LanguageCode                                 = "LanguageCode"
    static let LanguageName                                 = "LanguageName"
    static let WalletBalance                                = "WalletBalance"
    static let SuperStores                                  = "SuperStores"
    static let SelectedSuperStore                           = "SelectedSuperStore"
    static let SelectedSuperStoreID                         = "SelectedSuperStoreID"
}
