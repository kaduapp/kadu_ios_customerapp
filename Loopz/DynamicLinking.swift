//
//  DynamicLinking.swift
//  DelivX
//
//  Created by 3Embed on 20/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import FirebaseDynamicLinks
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class DynamicLinking:NSObject {
    
    
    class func getDynamicLink(itemId:String) -> Observable<String> {
        var url = DynamicLinkingConstants.DynamicLink
        if itemId.length > 0 {
            url = url + "/itemId:" + itemId + "/00"
            
        }
        guard let link = URL(string: url) else {
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        
        let components = DynamicLinkComponents(link: link, domainURIPrefix: DynamicLinkingConstants.DynamicDomain)
        
        let bundleID = Utility.BundleId
        // iOS params
        let iOSParams = DynamicLinkIOSParameters(bundleID: bundleID)
        
        iOSParams.fallbackURL = URL.init(string: DynamicLinkingConstants.Appstore)
        iOSParams.appStoreID = DynamicLinkingConstants.AppStoreId
        components?.iOSParameters = iOSParams
        
        let androidParams = DynamicLinkAndroidParameters(packageName: DynamicLinkingConstants.AndroidBundleId)
        androidParams.fallbackURL = URL.init(string: DynamicLinkingConstants.AndroidURL)
        androidParams.minimumVersion = DynamicLinkingConstants.AndroidMinVersion
        components?.androidParameters = androidParams
        
        
        
        return Observable.create { observer in
            components?.shorten { (shortURL, warnings, error) in
                // Handle shortURL.
                if let error = error {
                    print(error.localizedDescription)
                }else{
                    let shortLink = shortURL
                    print(shortLink?.absoluteString ?? "")
                    let shortLinkIn = shortLink?.absoluteString ?? ""
                    observer.onNext(shortLinkIn)
                    observer.onCompleted()
                }
            }
            return Disposables.create()
            }.share(replay: 1)
    }
}
