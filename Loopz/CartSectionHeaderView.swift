
//
//  CartSectionHeaderView.swift
//  UFly
//
//  Created by 3 Embed on 22/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CartSectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dottedSeparator: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var minimumOrderLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    /// initial view setup
    func initialSetup(){
        
        Fonts.setPrimaryMedium(storeName)
        Fonts.setPrimaryRegular(addressLabel)
        Fonts.setPrimaryMedium(totalPrice)
        
        titleLabel.text = StringConstants.Cart()
        Helper.drawDottedLine(dottedSeparator)
        titleLabel.textColor = Colors.PrimaryText
        Fonts.setPrimaryBold(titleLabel, size: 25)
        storeName.textColor =  Colors.PrimaryText
        totalPrice.textColor =  Colors.PrimaryText
        addressLabel.textColor =  Colors.SecoundPrimaryText
        if minimumOrderLabel != nil {
            minimumOrderLabel.textColor = Colors.PrimaryText
            Fonts.setPrimaryRegular(minimumOrderLabel)
        }
        self.storeImage.layer.cornerRadius = 2.5
        separator.backgroundColor = Colors.SeparatorDark.withAlphaComponent(0.6)
        
        if Changebles.isGrocerOnly{
            headerViewHeight.constant = 0
        }
        else{
             headerViewHeight.constant = 50
        }
    }
    
    
    /// updates the store name, logo and ammount
    ///
    /// - Parameter data: is of type cart model object
    func setupData(data:Cart) {
        storeName.text = data.StoreName
        totalPrice.text = Helper.df2so(Double( data.TotalPrice))
        if data.StoreAreaName.length > 0{
        addressLabel.text = data.StoreAreaName
        }
        Helper.setImage(imageView: storeImage, url: data.StoreLogo, defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        minimumOrderLabel.textColor = Colors.PrimaryText
        if data.TotalPrice >= Float(data.MinimumOrder) {
            if data.TotalPrice >= Float(data.FreeDelivery) {
                minimumOrderLabel.textColor = Colors.DeliveryBtn
                minimumOrderLabel.text = StringConstants.FreeDeliveryAvailable()
            }else{
                let priceText = Helper.df2so(Double(Float(data.FreeDelivery) - data.TotalPrice))
                
                let minimumFreeString = priceText + " \(StringConstants.FreeDelivery())"
                minimumOrderLabel.attributedText  = Helper.updateRedColor(textCross:priceText, text:minimumFreeString  )
            }
        }else{
            let priceText = Helper.df2so(Double( Float(data.MinimumOrder) - data.TotalPrice))
            
            let minimumString = priceText + " \(StringConstants.MinimumOrder())"
            minimumOrderLabel.attributedText  = Helper.updateRedColor(textCross:priceText, text:minimumString  )
            
        }
    }
   
}
