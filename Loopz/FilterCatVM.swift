//
//  filterCatVM.swift
//  DelivX
//
//  Created by 3Embed on 24/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class FilterCatVM: NSObject {

    var selectedSection = -1
    var selectedCategory = ""
    var arrayOfSubCat:[SubCategory] = []
    var SubCatSelected = ""
    var SubSubCatSelected = ""
    var type = ""
    let filtercat_responce = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    func getFilterCategory() {
        FIlterAPICalls.getFilterCategory(selectedItem: selectedCategory).subscribe(onNext: { [weak self]data in
            self?.arrayOfSubCat = data
            if (self?.arrayOfSubCat.count)! > 0 {
                for i in 0...(self?.arrayOfSubCat.count)! - 1 {
                    if self?.SubCatSelected == self?.arrayOfSubCat[i].Name {
                        self?.selectedSection = i
                        break
                    }
                }
            }
            self?.filtercat_responce.onNext(true)
        }).disposed(by: disposeBag)
    }
    
}
