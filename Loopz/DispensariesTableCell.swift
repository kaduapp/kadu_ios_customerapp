//
//  DispensariesTableCell.swift
//  UFly
//
//  Created by 3 Embed on 24/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class DispensariesTableCell: UITableViewCell {
    
    @IBOutlet weak var closedLabel: UILabel!
    @IBOutlet weak var distanceIMG: UIImageView!
    @IBOutlet weak var storeOuterView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBOutlet weak var distanceLB: UILabel!
    
    @IBOutlet weak var ratingIMG: UIImageView!
    @IBOutlet weak var ratingLB: UILabel!
    //@IBOutlet weak var priceLB: UILabel!
    @IBOutlet weak var minimumLabel: UILabel!
    @IBOutlet weak var seperationView: UIView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    var constraintFlagSubtracted:Bool = false
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
       // setImageAttributes()
        setTheFontColors()
        setUpTheFontsSize()
        storeOuterView.isHidden = true
    }
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
        func setData(data:Store) {
            setDataForSearchStorez(data: data)
        }
    
    
     func setDataForSearchStorez(data:Store) {
        closedLabel.text = ""
        self.titleLabel.text = data.Name
        if data.OfferTitle == "" {
            self.subTitle.text = data.storeArea
        }else{
            self.subTitle.text = data.OfferTitle + "\n" + data.storeArea
        }
        //self.ratingLB.text = "\(data.StoreRating)"
        self.ratingLB.text = "\(Helper.clipDigit(valeu: data.StoreRating, digits: 1))"
        
        var distanceMetric = StringConstants.MileAway()
        if Int(Utility.getCurrency().2) == 1{
            distanceMetric = StringConstants.MileAway()
             self.distanceLB.text = "\(Helper.clipDigit(valeu: data.DistanceMiles, digits: 2))" + " " + "\(distanceMetric)"
        }else{
            distanceMetric = StringConstants.KMAway()
            self.distanceLB.text = "\(Helper.clipDigit(valeu: data.DistanceKm.floatValue ?? 0.0, digits: 2))" + " " + "\(distanceMetric)"
        }
        if Utility.getSelectedSuperStores().type.rawValue == 1
        {
            let minText = Helper.df2so(Double(data.CostForTwo))
             minimumLabel.text =  "\(minText) \(StringConstants.ForTwo()) "

            self.subTitle.text = data.SubcategoryName + "\n" +  data.storeArea
        }else{
         let minText = Helper.df2so(Double(data.MinimumOrder))
        if minimumLabel != nil
        {
        minimumLabel.text =  "Min \(minText)"
        }
        }

        //priceLB.isHidden = true
        if data.Image.count > 0 {
            imageWidth.constant = self.mainImageView.frame.size.height
        }else{
            imageWidth.constant = 0
        }
        Helper.setImage(imageView: self.mainImageView, url: data.Image, defaultImage: UIImage())
        self.closedLabel.text = ""
        if data.StoreIsOpen{
            self.setTheFontColors()
            self.storeOuterView.isHidden = true
        }else{
            self.setTheFonrColorForCloseStore()
            self.storeOuterView.isHidden = false
            if data.nextOpenTime != "" {
                if data.todayIsOpen == 1{
                    self.closedLabel.text = StringConstants.Opens() + " " +  StringConstants.Today() + " " + StringConstants.At() + " " + data.nextOpenTime
                }else{
                    self.closedLabel.text = StringConstants.Opens() + " " + StringConstants.Tomorrow() + " " + StringConstants.At() + " " + data.nextOpenTime
                }
            }else{
                self.closedLabel.text = StringConstants.Closed()
            }
        }
        
    }
    fileprivate func setImageAttributes()
    {
        Helper.setShadow(sender: mainImageView)
    }
    fileprivate func setTheFontColors()
    {
        titleLabel.textColor = Colors.PrimaryText
        subTitle.textColor = Colors.SeconderyText
        distanceLB.textColor = Colors.SectionHeader
        ratingLB.textColor = Colors.SectionHeader
        seperationView.backgroundColor = Colors.SeparatorLarge
     
        if self.ratingIMG != nil
        {
            self.ratingIMG.setImageColor(color: Colors.PrimaryText)

        }
        if self.distanceIMG != nil
        {
        self.distanceIMG.setImageColor(color: Colors.PrimaryText)
        }
    }
    
    fileprivate func setTheFonrColorForCloseStore(){
        titleLabel.textColor = Colors.ClosedStoreColor
        subTitle.textColor = Colors.ClosedStoreColor
        distanceLB.textColor = Colors.ClosedStoreColor
        ratingLB.textColor = Colors.ClosedStoreColor
        seperationView.backgroundColor = Colors.SeparatorLarge
        self.ratingIMG.setImageColor(color: Colors.ClosedStoreColor)
        self.distanceIMG.setImageColor(color: Colors.ClosedStoreColor)
        self.storeOuterView.backgroundColor = Colors.ClosedStoreColor
        self.storeOuterView.alpha = 0.5
        closedLabel.textColor = UIColor.red
    }
    
    func setUpTheFontsSize(){
        Fonts.setPrimaryBold(titleLabel, size: 16)
        Fonts.setPrimaryRegular(distanceLB, size: 12)
        Fonts.setPrimaryRegular(ratingLB, size: 12)
        Fonts.setPrimaryRegular(closedLabel, size: 11)
        Fonts.setPrimaryRegular(subTitle, size: 12)
        Fonts.setPrimaryRegular(distanceLB, size: 12)
          Fonts.setPrimaryRegular(minimumLabel, size: 12)
    }
    
    

}
