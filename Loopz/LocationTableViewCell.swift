//
//  LocationTableViewCell.swift
//  UFly
//
//  Created by 3Embed on 08/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {    //this cell is re-using in two Controllers SelectLocationVC and addAddressVC
    
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var locationIcon: UIButton!     //only for select locationVC table Cell
    @IBOutlet weak var tagName: UILabel!          //only for select locationVC table Cell
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /// setup view
    func initialSetup(){
        headLabel.textColor = Colors.PrimaryText
        Fonts.setPrimaryRegular(headLabel)
        
    }
    
    /// update the adress to each Address cell
    ///
    /// - Parameter data: data it is of type Location object
    func setValue(data: Location) {
        
        headLabel.text = data.LocationDescription
        locationIcon.setImage(#imageLiteral(resourceName: "OtherAddress"), for: .normal)
        tagName.text = ""
        if data.LocationDescription.length == 0 {
            headLabel.text = data.FullText
            tagName.text = ""
        }
        locationIcon.tintColor = Colors.SeconderyText
    }
    
    /// update saved Location Secion
    ///
    /// - Parameter data: Location Model Object
    func setSavedAddress(data: Location){
        tagName.textColor = Colors.AppBaseColor
        headLabel.text = data.LocationDescription
        if data.LocationDescription.length == 0 {
            headLabel.text = data.FullText
            tagName.text = data.Tag
            locationIcon.setImage(Helper.setAddressImage(tag: data.Tag), for: .normal)
        }
        locationIcon.tintColor = Colors.SeconderyText
    }
    
    /// setCurrent Location
    func setCurrentLocation(){
        let locationStored = Utility.currentLatAndLong()//Utility.getAddress()
        headLabel.text = locationStored.Name
        locationIcon.setImage(#imageLiteral(resourceName: "searchCurrentLoc"), for: .normal)
        tagName.text = ""
        locationIcon.tintColor = Colors.SeconderyText
    }
    
    
}
