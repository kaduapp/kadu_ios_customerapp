//
//  PopupTFE.swift
//  DelivX
//
//  Created by 3EMBED on 30/10/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension PopupVC: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.addAction(UIButton())
        return true
    }
}

