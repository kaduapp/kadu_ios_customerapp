//
//  WalletHistoryTVE.swift
//  UFly
//
//  Created by 3 Embed on 04/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension WalletHistoryVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.scrollView {
            updatePage(scrolView1: scrollView)
        }
        if scrollView == self.creditOrderTableView{
            if scrollView.contentOffset.y > 0 {
                Helper.setShadow(sender: navigationView)
            }else{
                Helper.removeShadow(sender: navigationView)
            }
        }
        if scrollView == self.debitTableView{
            if scrollView.contentOffset.y > 0 {
                Helper.setShadow(sender: navigationView)
            }else{
                Helper.removeShadow(sender: navigationView)
            }
        }
        
    }
    
    func updatePage(scrolView1: UIScrollView) {
        var scrollIn = scrollView.contentOffset.x
        if Utility.getLanguage().Code == "ar" {
            scrollIn = scrollView.contentSize.width - scrollView.frame.size.width - scrollView.contentOffset.x
        }
        let scroll = (scrollIn / 2)
        distance.constant = scroll
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        let page:CGFloat = scrollIn / scrollView.frame.size.width
        
        if page == 0 {
            debitOrderButton.isSelected = false
            creditOrderButton.isSelected = false
        }
        else if page == 1 {
            debitOrderButton.isSelected = true
            creditOrderButton.isSelected = true
        }
        historyVM.currentPage = Int(page)
    }
}

//Mark: UITableViewDataSource
extension WalletHistoryVC: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == debitTableView {
            if historyVM.arrayOfDebit.count == 0 {
                tableView.backgroundView = emptyViewDebit
            }else{
                tableView.backgroundView = nil
            }
            return 1
        }
        if historyVM.arrayOfCredit.count == 0 {
            tableView.backgroundView = emptyViewCredit
        }else{
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == debitTableView {
            return historyVM.arrayOfDebit.count
        }
        if tableView == creditOrderTableView {
            return historyVM.arrayOfCredit.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var array = [Transaction]()
        if tableView == debitTableView {
            array = historyVM.arrayOfDebit
        }else{
            array = historyVM.arrayOfCredit
        }
        
        let cell = debitTableView.dequeueReusableCell(withIdentifier:  String(describing: WalletHistoryCell.self), for: indexPath) as! WalletHistoryCell
        cell.setData(data: array[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if debitTableView == tableView {
//            if indexPath.row + 1 == historyVM.arrayOfDebit.count && historyVM.arrayOfDebit.count >= 10 {
//                historyVM.getHistory()
//            }
//        }else if creditOrderTableView == tableView {
//            if indexPath.row + 1 == historyVM.arrayOfCredit.count && historyVM.arrayOfCredit.count >= 10 {
//                historyVM.getHistory()
//            }
//        }
    }
}
