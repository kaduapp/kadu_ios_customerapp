//
//  FilterSubCatVC.swift
//  DelivX
//
//  Created by 3Embed on 24/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class FilterSubCatVC: UIViewController {
    
    let disposeBag = DisposeBag()
    @IBOutlet weak var mainTableView: UITableView!
    var filterDetailVM = FilterDetailVM()
    var arrayOfSubsubCat:[SubSubCategory] = []
    var searchVMReference = SearchVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = filterDetailVM.SubCatSelected
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        observerData()
        searchVMReference.filterInfo.onNext((false,APIRequestParams.SubSubCategoryFilter,[]))
    }
    
    func observerData() {
        FilterDetailVM.filterArray_responce.subscribe(onNext: { [weak self]data in
            if self?.filterDetailVM.type == APIRequestParams.SubSubCategoryFilter {
                self?.filterDetailVM.SubSubCatSelected = data.2
            }
            self?.mainTableView.reloadData()
        }).disposed(by:disposeBag)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
