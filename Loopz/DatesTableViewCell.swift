//
//  DatesTableViewCell.swift
//  DelivX
//
//  Created by 3EMBED on 25/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class DatesTableViewCell: UITableViewCell {
    @IBOutlet weak var datesLabel: UILabel!

    @IBOutlet weak var backgroundLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = .white
        self.preservesSuperviewLayoutMargins = false
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
    }
    //func to update datesLabel

    func updateSlotDateCell(_ date: String) {
         datesLabel.text = date
        Fonts.setPrimaryLight(datesLabel)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
