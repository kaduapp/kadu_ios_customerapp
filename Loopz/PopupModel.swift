//
//  PopupModel.swift
//  DelivX
//
//  Created by 3Embed on 21/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct PopupModel {
    var String      = ""
    var Flag        = false
    var ID          = ""
    var Image       = ""
    
    init(data:[String:Any]) {
        
        if let titleTemp = data[APIResponceParams.ListName] as? String {
            String         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.Reason] as? String {
            String         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.IdAddress] as? String {
            ID         = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ListId] as? String {
            ID           = titleTemp
        }
        if let titleTemp = data[APIResponceParams.ListImage] as? String {
            Image           = titleTemp
        }
    }
}

struct PopupMenu {
    
    static var Payment:[PopupModel] {
        var menu1 = PopupModel(data: [:])
        menu1.String = StringConstants.Wallet()
        
        var menu2 = PopupModel(data: [:])
        menu2.String = StringConstants.Cash()
        return [menu1,menu2]
    }
    
    
    static func getWishList() -> [PopupModel] {
        let data = Utility.getWishList()
        var array:[PopupModel] = []
        for i in data {
            let menu = PopupModel(data: i as! [String:Any])
            array.append(menu)
        }
        return array
    }
    
}




