//
//  SearchDetailsCVE.swift
//  DelivX
//
//  Created by 3Embed on 04/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension SearchDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if searchVM.arrayOfExpandedStore.Products.count == 0 && searchVM.arrayOfStores.count == 0 && flagLoad == true {
            emptyView = EmptyView().shared
            emptyView.setData(image: #imageLiteral(resourceName: "EmptySearch"), title: StringConstants.EmptySearchProduct(), buttonTitle: "", buttonHide: true)
            collectionView.backgroundView = emptyView
            return 0
        }else if searchVM.arrayOfExpandedStore.Products.count == 0 && flagLoad == true {
            emptyView.setData(image: #imageLiteral(resourceName: "EmptySearch"), title: StringConstants.EmptySearchProduct() + "\n" + StringConstants.FindInStore(), buttonTitle: "", buttonHide: true)
        }
        collectionView.backgroundView = nil
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 {
            return searchVM.arrayOfStores.count
        }
        if searchVM.arrayOfExpandedStore.Products.count > 0 {
            return searchVM.arrayOfExpandedStore.Products.count
        }
        if searchVM.arrayOfExpandedStore.Products.count == 0 && searchVM.arrayOfStores.count == 0 {
            return 0
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 1 {
            let cell: ItemListingHeaderCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemListingHeaderCell.self), for: indexPath) as! ItemListingHeaderCell
            cell.setData(data:searchVM.arrayOfStores[indexPath.row])
            cell.ViewStore.bottomHeight.constant = 0
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
            return cell
        }else {
            if searchVM.arrayOfStores.count > 0 && searchVM.arrayOfExpandedStore.Products.count == 0 {
                let cell: ItemListingHeaderCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemListingHeaderCell.self), for: indexPath) as! ItemListingHeaderCell
                cell.setEmpty(view: emptyView)
                return cell
            }
            let cell: ItemCommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemCommonCollectionViewCell.self), for: indexPath) as! ItemCommonCollectionViewCell
            cell.isFromSearchTab = !self.searchVM.isFromItemList
            let subCategory = searchVM.arrayOfExpandedStore
            cell.setValue(data: subCategory.Products[indexPath.row],isDelete: false)
            cell.addToCartBtn.tag = indexPath.row
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize.zero
        
        if indexPath.section == 1 {
            let cellWidth = UIScreen.main.bounds.width
            let cellHight = 60
            size.height =  CGFloat(cellHight)
            size.width  =  cellWidth
            return size
        }
        else {
            if searchVM.arrayOfStores.count > 0 && searchVM.arrayOfExpandedStore.Products.count == 0 {
                let cellWidth = UIScreen.main.bounds.width
                let cellHight = cellWidth/2
                size.height =  CGFloat(cellHight)
                size.width  =  cellWidth
                return size
            }
            let cellWidth = (collectionView.frame.size.width - 10)/2
            let cellHight = (collectionView.frame.size.width)/2 + 40
            size.height =  cellHight
            size.width  =  cellWidth
            return size
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            Utility.saveStore(location: searchVM.arrayOfStores[indexPath.row].dataObject)
            Utility.UtilityUpdate.onNext(Utility.TypeServe.ChangeStore)
        }else{
            if searchVM.arrayOfExpandedStore.Products.count != 0 {
            let button = searchVM.arrayOfExpandedStore.Products[indexPath.row]
            selected = collectionView.cellForItem(at: indexPath)
            performSegue(withIdentifier: UIConstants.SegueIds.navDetails, sender: button)
            }
        }
    }
    
    
}
