//
//  UpdatePasswordView.swift
//  UFly
//
//  Created by Rahul Sharma on 28/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class UpdatePasswordView: UIView {
    
    @IBOutlet weak var navigationBackView: UIView!
    @IBOutlet weak var navTitleText: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var reEnterPWTF: UITextField!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var continueBtnContainer: UIView!
    
    @IBOutlet weak var pageHeadLabel: UILabel!
    @IBOutlet weak var newPWCheckMark: UIButton!
    @IBOutlet weak var reEnterPWCheckMark: UIButton!
    
    @IBOutlet weak var newPWSeparator: UIView!
    @IBOutlet weak var reEnterPWSeparator: UIView!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
        newPasswordTF.Semantic()
        reEnterPWTF.Semantic()
    }
    
    /// view initial setup
    func initialSetup(){
        navigationBackView.isHidden = true
        Fonts.setPrimaryMedium(navTitleText)
        navTitleText.text = StringConstants.EnterNewPW()
        navTitleText.textColor =  Colors.PrimaryText
        Fonts.setPrimaryMedium(pageHeadLabel)
        Fonts.setPrimaryBlack(headLabel)
        Fonts.setPrimaryRegular(title)
        Fonts.setPrimaryMedium(newPasswordTF)
        Fonts.setPrimaryMedium(reEnterPWTF)
        
        newPWCheckMark.tintColor = Colors.AppBaseColor
        reEnterPWCheckMark.tintColor = Colors.AppBaseColor
        
        newPWCheckMark.isHidden = true
        reEnterPWCheckMark.isHidden = true
        
        pageHeadLabel.text = StringConstants.EnterNewPW()
        headLabel.text = StringConstants.EnterNewPW()
        title.text = StringConstants.MobileNumVerified()
        
        newPasswordTF.placeholder = StringConstants.YourNewPW()
        reEnterPWTF.placeholder   = StringConstants.ReEnterPW()
        
        Helper.setButton(button: continueBtn,view:continueBtnContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.Update(), highlighted: StringConstants.Update(), selected: StringConstants.Update(), button: continueBtn)
        
        continueBtn.titleLabel?.textColor = Colors.SecondBaseColor
        Fonts.setPrimaryMedium(continueBtn)
        pageHeadLabel.textColor =  Colors.PrimaryText
        headLabel.textColor =  Colors.HeadColor
        title.textColor =  Colors.SeconderyText
        newPasswordTF.textColor =  Colors.PrimaryText
        reEnterPWTF.textColor =  Colors.PrimaryText
        
    }
    
}


// MARK: - UIScrollViewDelegate
extension UpdatePasswordView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ sender: UIScrollView) {
        // Update the page when more than 50% of the previous/next page is visible
        let movedOffset: CGFloat = sender.contentOffset.y
        if sender == scrollView {
            let pageWidth: CGFloat = 238 - 64             //171 - 64
            let ratio: CGFloat = (movedOffset) / (pageWidth)
            
            print("\(movedOffset),\(ratio)")
            if ratio >= 1 {
                navigationBackView.isHidden = false
                navigationBackView.backgroundColor =  Colors.SecondBaseColor
                
                navTitleText.textColor =  Colors.PrimaryText
            }
            else {
                navigationBackView.isHidden = true
            }
        }
    }
}
