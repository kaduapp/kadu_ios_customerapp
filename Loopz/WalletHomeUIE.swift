//
//  QuickCardUIE.swift
//  DelivX
//
//  Created by 3 Embed on 22/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

import UIKit

extension WalletHomeVC {
    
    func validateUI() {
        Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor, shadow: false)
        bottomButton.setTitle(StringConstants.ADDMoney(), for: .normal)
    
        balanceHead.textColor = Colors.SeparatorDark
        balanceHead.text = StringConstants.CurrentBalance()
        Fonts.setPrimaryMedium(balanceHead)
        balance.textColor = Colors.PrimaryText
        Fonts.setPrimaryBold(balance)
        separatorBalance.backgroundColor = Colors.SeparatorLight
        moneySeparator.backgroundColor = Colors.PrimaryText
        historyButton.tintColor = Colors.AppBaseColor
        Fonts.setPrimaryMedium(historyButton)
        historyButton.setTitle(StringConstants.RecentTransactions(), for: .normal)
        hardLimit.textColor = Colors.SeparatorDark
        lightLimit.textColor = Colors.SeparatorDark
        Fonts.setPrimaryRegular(hardLimit)
        Fonts.setPrimaryRegular(lightLimit)
        headerSeparator.backgroundColor = Colors.SeparatorLight.withAlphaComponent(0.5)
        Helper.setUiElementBorderWithCorner(element: headerSeparator, radius: 0, borderWidth: 1, color: Colors.SeparatorLight)
        listHead.text = StringConstants.PayUsingCard()
        Fonts.setPrimaryBold(listHead)
        listHead.textColor = Colors.AppBaseColor
        footerHead.text = StringConstants.AddMoney()
        Fonts.setPrimaryBold(footerHead)
        footerHead.textColor = Colors.AppBaseColor
        hardColorView.backgroundColor = Colors.Red
        softColorView.backgroundColor = Colors.PickupBtn
        hardText.textColor = Colors.SeparatorDark
        softText.textColor = Colors.SeparatorDark
        Fonts.setPrimaryRegular(hardText)
        Fonts.setPrimaryRegular(softText)
        Helper.updateText(text: StringConstants.SoftLimit() + ": " + StringConstants.SoftText(), subText: StringConstants.SoftLimit() + ": ", softText, color: Colors.SeconderyText, link: "")
        Helper.updateText(text: StringConstants.HardLimit() + ": " + StringConstants.HardText(), subText: StringConstants.HardLimit() + ": ", hardText, color: Colors.SeconderyText, link: "")
        bottomView.backgroundColor = Colors.SeparatorLight.withAlphaComponent(0.3)
        Helper.setUiElementBorderWithCorner(element: bottomView, radius: 0, borderWidth: 1, color: Colors.SeparatorLight)
        

        Fonts.setPrimaryMedium(moneyTF)
        Fonts.setPrimaryMedium( currencyLabel)
         moneyTF.textColor = Colors.PrimaryText
         currencyLabel.textColor = Colors.PrimaryText

       
        moneyTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        textFieldDidChange(moneyTF)
        
        Fonts.setPrimaryRegular(voucherButton)
        voucherButton.setTitleColor(Colors.AppBaseColor, for: UIControl.State.normal)
        voucherButton.setTitle(StringConstants.HaveVoucher(), for: .normal)
        
        
        CommonAlertView.AlertPopupResponse.subscribe(onNext: { data in
            if data == CommonAlertView.ResponceType.WalletAmount {
                self.updateTheWallet()
            }
        }).disposed(by: disposeBag)
    }
    
    func updateTheWallet(){
        
//        if moneyTF.text?.floatValue != nil {
//            amountIn = (moneyTF.text?.floatValue)!
//        }
        if let amount = moneyTF.text?.floatValue{
            if amount > 0{
                amountIn = amount
                WalletAPICalls.updateBalance(data: cardArray[selectedCard], amount: amountIn).subscribe(onNext: { [weak self]data in
                    WalletAPICalls.getBalance().subscribe(onNext: { [weak self]data in
                        self?.amount = Utility.getWallet().Amount
                        self?.setBalance()
                        Helper.showAlert(message: StringConstants.WalletRecharged() + Helper.df2so(Double( (self?.amountIn)!)) + StringConstants.Balance(), head: StringConstants.Message(), type: 1)
                        self?.moneyTF.text = ""
                        self?.moneyTF.resignFirstResponder()
                        self?.textFieldDidChange((self?.moneyTF)!)
                    }).disposed(by: (self?.disposeBag)!)
                }).disposed(by: self.disposeBag)
            }
        }
    }
    
    func setBalance() {
        let wallet = Utility.getWallet()
        balance.text = Helper.df2so(Double( amount))
        hardLimit.text = StringConstants.HardLimit() + ": " + Helper.df2so(Double( wallet.HardLimit))
        lightLimit.text = StringConstants.SoftLimit() + ": " + Helper.df2so(Double(wallet.SoftLimit))
        
        Helper.updateText(text: hardLimit.text!, subText:Helper.df2so(Double(wallet.HardLimit)), hardLimit, color: Colors.Red, link: "")
        Helper.updateText(text: lightLimit.text!, subText: Helper.df2so(Double(wallet.SoftLimit)), lightLimit, color: Colors.PickupBtn, link: "")
        
        moneyTF.placeholder = StringConstants.Amount()
        currencyLabel.text = Utility.getCurrency().1
    }
    
    
}

extension WalletHomeVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        Helper.addDoneButtonOnTextField(tf: textField, vc: self.view)
        if textField.text?.sorted().count != 0 {
            Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour:  Colors.AppBaseColor, seconderyColor:  Colors.SecondBaseColor, shadow: true)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField) {
        
        if textField.text?.sorted().count != 0 {
            Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour:  Colors.AppBaseColor, seconderyColor:  Colors.SecondBaseColor, shadow: true)
        }
        else {
            Helper.setButton(button: bottomButton, view: bottomButtonView, primaryColour:  Colors.DoneBtnNormal, seconderyColor:  Colors.SecondBaseColor, shadow: true)
        }
        
    }
    
    override func keyboardWillShow(_ notification: NSNotification) {
        
        let kbSize = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.size.height, right: 0.0)
        self.mainTableView.contentInset = contentInsets;
        self.mainTableView.scrollIndicatorInsets = contentInsets;
        
    }
    
    override func keyboardWillHide(_ notification: NSNotification) {
        self.mainTableView.contentInset = UIEdgeInsets.zero;
        self.mainTableView.scrollIndicatorInsets = UIEdgeInsets.zero;
    }
}
