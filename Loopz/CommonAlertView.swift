//
//  CommonAlertView.swift
//  UFly
//
//  Created by 3Embed on 18/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CommonAlertView: UIView {

    enum ResponceType:Int {
        case Close      = 0
        case Address    = 1
        case AppVersion = 2
        case Logout     = 3
        case Delete     = 4
        case Location   = 5
        case SessionOut = 6
        case CancelOrder = 7
        case WalletAmount = 8

      }
    
    @IBOutlet weak var okButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var okButtonContainer: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var alertTypeImage: UIImageView!
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var alertBackGroundView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    
    var selected:ResponceType? = nil
    static let AlertPopupResponse = PublishSubject<ResponceType>()
    static var obj: CommonAlertView? = nil
    static var shared: CommonAlertView {
        
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as? CommonAlertView
            obj?.frame = UIScreen.main.bounds
            obj?.backgroundColor = Colors.PrimaryText.withAlphaComponent(0.2)
        }
        return obj!
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.hideAlert()
        CommonAlertView.AlertPopupResponse.onNext(.Close)
    }

    private func setup() {
        let window:UIWindow = UIApplication.shared.delegate!.window!!
        window.addSubview(self)
        alertBackGroundView.layer.cornerRadius = 4
        Helper.setButtonTitle(normal: StringConstants.OK(), highlighted: StringConstants.OK(), selected: StringConstants.OK(), button: okButton)
        Helper.setButton(button: okButton, view: okButtonContainer, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor, shadow: true)
        self.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        Helper.setShadow(sender: alertBackGroundView)
        UIView.animate(withDuration: 0.01, delay: 0.01, options: .beginFromCurrentState, animations: {() -> Void in
            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        }, completion: {(_ finished: Bool) -> Void in
        })
        
//        let when = DispatchTime.now() + 30
//        
//        DispatchQueue.main.asyncAfter(deadline: when){
//            if self.okButtonContainer.isHidden == false {
//                self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
//                UIView.animate(withDuration: 0.01, delay: 0.01, options: .beginFromCurrentState, animations: {() -> Void in
//                    self.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
//                }, completion: {(_ finished: Bool) -> Void in
//                    self.removeFromSuperview()
//                })
//            }
//        }
        
        
    }
    
    func showAlert(message: String, head:String, type:Int) {
        setup()
        headLabel.text = head
        contentLabel.text = message
        alertTypeImage.image = #imageLiteral(resourceName: "EmptyAddress")
        //        if type == 1 {
        //            alertTypeImage.image = #imageLiteral(resourceName: "ErrorIcon")
        //        }else if type == 0 {
        //            alertTypeImage.image = #imageLiteral(resourceName: "SuccessIcon")
        //        }else {
        //            alertTypeImage.image = #imageLiteral(resourceName: "Warning")
        //        }
        okButtonContainer.isHidden = true
        closeButton.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.okButtonHeight.constant = 0
            self.updateConstraints()
            self.layoutIfNeeded()
        }
    }
    
    func hideAlert() {
        if (CommonAlertView.obj != nil) {
            self.removeFromSuperview()
        }
    }
    
    func showAlert(message: String, head:String, type:Int, ok:Bool, close:Bool,responce:ResponceType) {
        setup()
        headLabel.text = head
        contentLabel.text = message
        if type == 1 {
            alertTypeImage.image = #imageLiteral(resourceName: "ErrorIcon")
        }else if type == 0 {
            alertTypeImage.image = #imageLiteral(resourceName: "SuccessIcon")
        }else {
            alertTypeImage.image = #imageLiteral(resourceName: "Warning")
        }
        closeButton.isHidden = close
        okButtonContainer.isHidden = false
        selected = responce
        UIView.animate(withDuration: 0.2) {
            self.okButtonHeight.constant = 35
            self.updateConstraints()
            self.layoutIfNeeded()
        }
    }
    
    
    @IBAction func okAction(_ sender: Any) {
        if selected != nil {
            CommonAlertView.AlertPopupResponse.onNext(selected!)
        }
        hideAlert()
    }
    
}
