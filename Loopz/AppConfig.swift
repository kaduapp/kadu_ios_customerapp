//
//  AppConfig.swift
//  DelivX
//
//  Created by 3Embed on 21/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct AppConfig {
    
    var AllCitiesPush                   = ""
    var AllPush                         = ""
    var StripeKey                       = ""
    var PrivacyPoplicy                  = ""
    var TermsAndCondition               = ""
    var LaterBookingBuffer              = 0
    var MapKeys:[String]                = []
    var PlaceKeys:[String]              = []
    var googleKey:String                = ""
    
    
    
    init(data:[String:Any]) {
        if let dataIn = data[APIResponceParams.AllCitiesPush] as? String {
            AllCitiesPush = dataIn
        }
        if let dataIn = data[APIResponceParams.AllPush] as? String {
            AllPush = dataIn
        }
        if let dataIn = data[APIResponceParams.PrivacyAndPolicy] as? String {
            PrivacyPoplicy = dataIn
        }
        if let dataIn = data[APIResponceParams.TermsAndConditions] as? String {
            TermsAndCondition = dataIn
        }
        if let dataIn = data[APIResponceParams.StripeKey] as? String {
            StripeKey = dataIn
        }
        if let dataIn = data[APIResponceParams.GoogleMapKeys] as? [String] {
            MapKeys = dataIn
        }
        if let dataIn = data[APIResponceParams.GooglePlaceKeys] as? [String] {
            PlaceKeys = dataIn
        }
        if let googleMapKey = data[APIResponceParams.googleMapKey] as? String{
            googleKey = googleMapKey
        }
        
        if let dataIn = data[APIResponceParams.LaterBookingBuffer] as? Int {
            LaterBookingBuffer = dataIn
        }
    }
    
}
