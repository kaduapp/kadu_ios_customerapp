//
//  ItemDetailCVE.swift
//  UFly
//
//  Created by 3 Embed on 25/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension ItemDetailVC:  UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == unitCollectionView{
            if itemDetailVM.item.Units.count > 0 {
                return (self.itemDetailVM.item.Units.count)
            }else{
                return 0
            }
        }
        if itemDetailVM.item.Images.count == 0{
            return 1
        }else{
            return (itemDetailVM.item.Images.count)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == unitCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.ItemUnitCVC, for: indexPath) as! ItemUnitCVC
            let unit:Unit = self.itemDetailVM.item.Units[indexPath.row]
            cell.updateView(data: unit)
            cell.isUnselectedUnit()
            if itemDetailVM.item.UnitId == unit.Id {
                cell.isSelectedUnit()
            }
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UIConstants.CellIds.ItemImageCVC, for: indexPath) as! ItemImageCVC
        if itemDetailVM.item.Images.count == 0{
            cell.itemImage.image = #imageLiteral(resourceName: "SingleShowDefault")
        }else{
            cell.updateCell(data:itemDetailVM.item, index: indexPath.row)
            cell.imageButton.addTarget(self, action: #selector(ItemDetailVC.imageBtnTapped), for: UIControl.Event.touchUpInside)
            cell.imageButton.tag = indexPath.row
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == unitCollectionView{
            
            let tempString = self.itemDetailVM.item.Units[indexPath.row].Title
            let font :UIFont = UIFont.init(name: Fonts.Primary.Regular, size: 12)!
            let size: CGSize = tempString.size(withAttributes: [NSAttributedString.Key.font: font])
            
            var cellWidth  = size.width+26
            let cellHight  = CGFloat(36)
            
            if  cellHight > cellWidth {
                cellWidth = cellHight
            }
            return CGSize(width: cellWidth, height: cellHight)
            
        }
        
        var size = CGSize.zero
        let cellWidth = collectionView.frame.size.width//UIScreen.main.bounds.width
        let cellHight = collectionView.frame.size.height
        
        size.height =  cellHight
        size.width  =  cellWidth
        return size
    }
}
extension ItemDetailVC : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == unitCollectionView{
            let unit:Unit = self.itemDetailVM.item.Units[indexPath.row]
            itemDetailVM.item.UnitId = unit.Id
            itemDetailVM.item.Price = unit.Value
            itemDetailVM.item.UnitName = unit.Title
            itemDetailVM.item.Discount = unit.Discount
            itemDetailVM.item.FinalPrice = unit.FinalPrice
            itemDetailVM.item.AddOnAvailable = unit.AddOnAvailable
            itemDetailVM.item.AddOnGroups = unit.AddOnGroups
            itemDetailVM.item.RecentAddOns = unit.RecentAddOns
            
            if unit.availableQuantity == 0{
            itemDetailVM.item.outOfStock = true
            }
            else{
            itemDetailVM.item.outOfStock = false
            }
            itemDetailVM.item.setCart()
            itemDetailVM.updatedCount = itemDetailVM.item.CartQty
            self.bottomCartView.updateAmount(data:self.itemDetailVM)
            collectionView.reloadData()
            let lastScrollOffset = self.mainTableView.contentOffset
            self.mainTableView.beginUpdates()
            var mySet: IndexSet = IndexSet.init()
            mySet.insert(0)
            self.mainTableView.reloadSections(mySet, with: .automatic)
            self.mainTableView.endUpdates()
            self.mainTableView.layer.removeAllAnimations()
            self.mainTableView.setContentOffset(lastScrollOffset, animated: false)
            
        }
    }
    
}
