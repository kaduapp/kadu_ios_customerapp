//
//  VoucherUIE.swift
//  DelivX
//
//  Created by 3Embed on 11/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
extension VoucherVC:UITextFieldDelegate {
    
    /// initial ViewSetup
    func setUI() {
        
        //Done Button
        titleLabel.textColor = Colors.PrimaryText
        titleView.backgroundColor = Colors.SecondBaseColor
        Helper.setUiElementBorderWithCorner(element: titleView, radius: 10, borderWidth: 0, color: UIColor.clear)
        Helper.setShadow(sender: self.view)
        
        //Title
        separator.backgroundColor = Colors.SeparatorLight
        Fonts.setPrimaryMedium(titleLabel)
        titleLabel.text = StringConstants.VoucherCode()
        Helper.setButton(button: confirmButton, view: confirmView, primaryColour: Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor, shadow: true)
        Helper.setButtonTitle(normal: StringConstants.Redeem(), highlighted: StringConstants.Redeem(), selected: StringConstants.Redeem(), button: confirmButton)
    }
    
    func closeVC(){
        self.voucherVM.VoucherVM_responseWallet.onNext(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func didGetResponse(){
        voucherVM.VoucherVM_response.subscribe(onNext: { [weak self]success in
            self?.view.endEditing(true)
            Helper.showAlert(message: success.1, head: StringConstants.Message(), type: 1)
            if success.0{
                Helper.finalController().dismiss(animated: true, completion: {
                  self?.closeVC()
                })
            }
        }).disposed(by: dispose)
    }
}
