//
//  PaymentDescribeCell.swift
//  DelivX
//
//  Created by 3Embed on 30/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PaymentDescribeCell: UITableViewCell {

    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Fonts.setPrimaryRegular(titleLabel)
        Fonts.setPrimaryRegular(valueLabel)
        titleLabel.textColor = Colors.PrimaryText
        valueLabel.textColor = Colors.PrimaryText
    }
    
    func setData(data:Tax) {
        titleLabel.text = data.Name
        if data.Value > 0 {
            titleLabel.text = titleLabel.text! + "(\(data.Value)%)"
        }
        valueLabel.text = Helper.df2so(Double( data.Price))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
