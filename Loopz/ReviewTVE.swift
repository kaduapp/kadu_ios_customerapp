//
//  ReviewTVE.swift
//  DelivX
//
//  Created by 3Embed on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension ReviewVC:UITableViewDataSource, UITableViewDelegate, FloatRatingViewDelegate  {
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        reviewVM.RatingArray[ratingView.tag].Value = Int(rating)
        //        var mySet: IndexSet = IndexSet.init()
        //        mySet.insert(ratingView.tag)
        //        self.mainCollectionView.beginUpdates()
        //        self.mainCollectionView.reloadSections(mySet, with: .automatic)
        //        self.mainCollectionView.layer.removeAllAnimations()
        //        self.mainCollectionView.endUpdates()
        let lastScrollOffset = self.mainCollectionView.contentOffset
        self.mainCollectionView.beginUpdates()
        var mySet: IndexSet = IndexSet.init()
        mySet.insert(1)
        self.mainCollectionView.reloadSections(mySet, with: .automatic)
        self.mainCollectionView.endUpdates()
        self.mainCollectionView.layer.removeAllAnimations()
        self.mainCollectionView.setContentOffset(lastScrollOffset, animated: false)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == mainCollectionView{
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == mainCollectionView{
            if section == 0{
                if reviewVM.orderRecieved?.PaymentType == 1 && (reviewVM.orderRecieved?.Delivery)! {
                    return 2
                }
                return 1
            }
            return reviewVM.RatingArray.count
        }
        var data = reviewVM.RatingArray[tableView.tag]
        if data.Value > 0 {
            if data.Options.count >= data.Value {
                return data.Options[data.Value - 1].Options.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if tableView == mainCollectionView{
            if indexPath.section == 0{
                if reviewVM.orderRecieved?.PaymentType == 1 && (reviewVM.orderRecieved?.Delivery)! && indexPath.row == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DriverTipTVC.self), for: indexPath) as! DriverTipTVC
                    cell.setData(data: reviewVM.orderRecieved!)
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ReviewHeaderCell.self), for: indexPath) as! ReviewHeaderCell
                cell.setData(data: reviewVM.orderRecieved!)
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.RatingHeaderCell, for: indexPath) as! RatingHeaderCell
            cell.popUpTableView.tag = indexPath.row
            var height = 0
            let data = reviewVM.RatingArray[indexPath.row]
            if data.Value > 0 {
                if data.Options.count >= data.Value {
                    height = data.Options[data.Value - 1].Options.count
                }
            }
            cell.heightOfPopUpTableView.constant = CGFloat(height * 50)
            cell.popUpTableView.reloadData()
            cell.setData(data: data)
            cell.ratestar.tag = indexPath.row
            cell.ratestar.delegate = self
            cell.reasonTitle.isHidden = false
            cell.subseparator.isHidden = false
            cell.reasonTopConstraints.constant = 30
            cell.subseparatorTopConstraints.constant = 10
            cell.subseperatorBottomConstraints.constant = 5
            cell.reasonTitle.text = StringConstants.Improve()
            if data.Options.count >= data.Value {
                if data.Options[data.Value - 1].Options.count <= 0 {
                    cell.reasonTitle.isHidden = true
                    cell.subseparator.isHidden = true
                    cell.reasonTitle.text = ""
                    cell.reasonTopConstraints.constant = 0
                    cell.subseparatorTopConstraints.constant = 0
                    cell.subseperatorBottomConstraints.constant = 0
                }
            }else{
                cell.reasonTitle.isHidden = true
                cell.subseparator.isHidden = true
                cell.reasonTitle.text = ""
                cell.reasonTopConstraints.constant = 0
                cell.subseparatorTopConstraints.constant = 0
                cell.subseperatorBottomConstraints.constant = 0
            }
            return cell
        }
        
        var data = reviewVM.RatingArray[tableView.tag]
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.PopupCell, for: indexPath) as! PopupCell
        cell.setData(data : data.Options[data.Value - 1].Options[indexPath.row])
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == mainCollectionView{
            if section == 0{
                if reviewVM.orderRecieved?.PaymentType == 1 && (reviewVM.orderRecieved?.Delivery)!{
                    return Helper.showHeader(title: "", showViewAll: false)
                }
                return Helper.showHeader(title: StringConstants.leaveTip(), showViewAll: false)
            }
            return Helper.showHeader(title: StringConstants.rateExperience(), showViewAll: false)
        }
        return Helper.showHeader(title: "", showViewAll: false)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == mainCollectionView{
            if section == 0 {
                return 0
            }
            if section == 1 {
                
                return 50
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView != mainCollectionView{
            var data = reviewVM.RatingArray[tableView.tag]
            
            let cell = tableView.cellForRow(at: indexPath) as! PopupCell
            if data.Options[data.Value - 1].Options[indexPath.row].1 {
                reviewVM.RatingArray[tableView.tag].Options[data.Value - 1].Options[indexPath.row].1 = false
                cell.selectImage.image = #imageLiteral(resourceName: "CheckBoxOff")
            }else{
                reviewVM.RatingArray[tableView.tag].Options[data.Value - 1].Options[indexPath.row].1 = true
                cell.selectImage.image = #imageLiteral(resourceName: "CheckBoxOn")
            }
        }
    }
}
