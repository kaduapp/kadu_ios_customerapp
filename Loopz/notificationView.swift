//
//  NotificationView.swift
//  DelivX
//
//  Created by 3 Embed on 23/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class NotificationView: UIView {
    
    @IBOutlet weak var backgroungView: UIView!
    @IBOutlet weak var notificationLabel: UILabel!
    
   static var obj: NotificationView? = nil
   static var shared: NotificationView {
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[3] as? NotificationView
            obj?.frame = UIScreen.main.bounds
            obj?.notificationLabel.textColor = Colors.SecondBaseColor
            Fonts.setPrimaryRegular(obj?.notificationLabel as Any)
            obj?.backgroungView.backgroundColor = Colors.Red.withAlphaComponent(0.8)
            obj?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width , height: 20)
        }
        return obj!
    }
    
    func hideNotification() {
        UIView.animate(withDuration: 2) {
            var frame = NotificationView.obj?.frame
            frame?.size.width = 0
            NotificationView.obj?.frame = frame!
        }
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            if NotificationView.obj != nil{
                self.removeFromSuperview()
            }
        }
    }
}
