//
//  BottomBarView.swift
//  UFly
//
//  Created by 3 Embed on 25/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class BottomBarView: UIView {
    
    //  @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var ViewCartLabel: UILabel!
    @IBOutlet weak var addCart: UILabel!
    @IBOutlet weak var cartImage: UIButton!
    @IBOutlet weak var leftcartImage: UIButton!
    @IBOutlet weak var inCartBtn: UIButton!
    @IBOutlet weak var inCartView: UIView!
    @IBOutlet weak var btnsView: UIView!
     @IBOutlet weak var outOfStockView: UIView!
    @IBOutlet weak var outOfStockButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    
    /// initial view setup
    func initialSetup(){
        
        // addBtn.isHidden = true
        Fonts.setPrimaryBold(minusBtn)
        Fonts.setPrimaryBold(inCartBtn)
        Fonts.setPrimaryBold(outOfStockButton)
        
        Fonts.setPrimaryBold(plusBtn)
        Fonts.setPrimaryMedium(PriceLabel)
        Fonts.setPrimaryBold(ViewCartLabel)
        Fonts.setPrimaryBold(quantity)
        PriceLabel.textColor =  Colors.SecondBaseColor
        quantity.textColor   =  Colors.PrimaryText
        ViewCartLabel.textColor   =  Colors.SecondBaseColor
        minusBtn.setTitleColor( Colors.AppBaseColor, for: .normal)
        plusBtn.setTitleColor( Colors.AppBaseColor, for: .normal)
        
        Helper.setUiElementBorderWithCorner(element: btnsView, radius: Float(UIConstants.ButtonCornerRadius), borderWidth: 1.0,color: Colors.BtnBackground)
        
        ViewCartLabel.text = StringConstants.ViewCart()
        Helper.setButton(button: inCartBtn,view:inCartView, primaryColour:  Colors.BtnBackground, seconderyColor: Colors.SecondBaseColor,shadow: true)
        Helper.setButtonTitle(normal: StringConstants.InCart(), highlighted: StringConstants.InCart(), selected: StringConstants.InCart(), button: inCartBtn)
        
        Helper.setButtonTitle(normal: StringConstants.outOfStock(), highlighted: StringConstants.outOfStock(), selected: StringConstants.outOfStock(), button: outOfStockButton)
         Helper.setButton(button: outOfStockButton,view:outOfStockView, primaryColour:  UIColor.red, seconderyColor: Colors.SecondBaseColor,shadow: true)
        
    }
    
    /// updates the total cart amount and cart count
    func updateAmount(data:ItemDetailVM){
        
        if data.item.outOfStock && Utility.getSelectedSuperStores().type != .Restaurant {
            self.outOfStockView.isHidden = false
            return
        }
        else{
            self.outOfStockView.isHidden = true
        }
        
        if data.item.CartQty != 0 {
            //addBtn.isHidden = true
            plusBtn.isHidden = false
            minusBtn.isHidden = false
            quantity.isHidden = false
            
            addCart.isHidden = true
            cartImage.isHidden = true
            
            PriceLabel.isHidden = false
            ViewCartLabel.isHidden = false
            leftcartImage.isHidden = false
            Helper.setButtonTitle(normal: StringConstants.InCart(), highlighted: StringConstants.InCart(), selected: StringConstants.InCart(), button: inCartBtn)
            
        }else{
            Helper.setButton(button: inCartBtn,view:inCartView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
            Helper.setButtonTitle(normal: StringConstants.AddToCart(), highlighted: StringConstants.AddToCart(), selected: StringConstants.AddToCart(), button: inCartBtn)
            
        }
        if data.item.CartQty == 0 {
            if data.updatedCount == 0 {
                data.updatedCount = 1
            }
            if Utility.getSelectedSuperStores().type == .Restaurant {
                data.updatedCount = 0
            }
            Helper.setButtonTitle(normal: StringConstants.AddToCart(), highlighted: StringConstants.AddToCart(), selected: StringConstants.AddToCart(), button: inCartBtn)
            inCartBtn.isUserInteractionEnabled = true
        }
        else if data.updatedCount != data.item.CartQty {
            
            if data.updatedCount == 0 {
                Helper.setButtonTitle(normal: StringConstants.Remove(), highlighted: StringConstants.Remove(), selected: StringConstants.Remove(), button: inCartBtn)
                
            }
            else {
                Helper.setButtonTitle(normal: StringConstants.ChangeQty(), highlighted: StringConstants.ChangeQty(), selected: StringConstants.ChangeQty(), button: inCartBtn)
            }
            
            Helper.setButton(button: inCartBtn,view:inCartView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
            inCartBtn.isUserInteractionEnabled = true
        }
        else if data.updatedCount == data.item.CartQty{
            Helper.setButtonTitle(normal: StringConstants.ViewCart(), highlighted: StringConstants.ViewCart(), selected: StringConstants.ViewCart(), button: inCartBtn)
            Helper.setButton(button: inCartBtn,view:inCartView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
            inCartBtn.isUserInteractionEnabled = true
        }
        quantity.text = String(describing: (data.updatedCount))
        
  
        if Utility.getSelectedSuperStores().type == .Restaurant {
            if AppConstants.Cart.count > 0 {
                Helper.setButtonTitle(normal: StringConstants.ViewCart(), highlighted: StringConstants.ViewCart(), selected: StringConstants.ViewCart(), button: inCartBtn)
                Helper.setButton(button: inCartBtn,view:inCartView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
            }else{
                Helper.setButtonTitle(normal: StringConstants.Close(), highlighted: StringConstants.Close(), selected: StringConstants.Close(), button: inCartBtn)
                Helper.setButton(button: inCartBtn,view:inCartView, primaryColour:  Colors.AppBaseColor, seconderyColor: Colors.SecondBaseColor,shadow: true)
            }
            inCartBtn.isUserInteractionEnabled = true
        }
    }
}
