//
//  UpdatePasswordVC.swift
//  UFly
//
//  Created by Rahul Sharma on 28/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class UpdatePasswordVC: UIViewController {
    
    @IBOutlet var newPWView: UpdatePasswordView!
    
    var updatePasswordVM = UpdatePasswordVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isPasswordUpdated()
        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.newPWView.newPasswordTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        self.newPWView.reEnterPWTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if self.navigationController != nil {
            Helper.transparentNavigation(controller: self.navigationController!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if self.navigationController != nil {
            
            Helper.nonTransparentNavigation(controller: self.navigationController!)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func didTapUpdate(_ sender: Any) {
        
        if newPWView.newPasswordTF.text?.sorted().count == newPWView.reEnterPWTF.text?.sorted().count && (newPWView.newPasswordTF.text?.sorted().count)! >= 6 {
            // isPasswordUpdated()
            self.updatePasswordVM.authentication.Password = newPWView.newPasswordTF.text!
            updatePasswordVM.updatePassword()
        }else if newPWView.newPasswordTF.text?.sorted().count == newPWView.reEnterPWTF.text?.sorted().count {
            Helper.showAlert(message: StringConstants.PasswordMissing(), head: StringConstants.Error() , type: 1)
        } else {
            Helper.showAlert(message: StringConstants.PWMisMatch(), head: StringConstants.Error() , type: 1)
        }
    }
}


extension UpdatePasswordVC {
    
    /// observe ViewModel
    func isPasswordUpdated(){
        updatePasswordVM.updatePasswordVM_responce.subscribe(onNext: { [weak self]success in
            if success {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }).disposed(by: updatePasswordVM.disposeBag)
    }
    
    /// Dismiss Keyboard
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
}
