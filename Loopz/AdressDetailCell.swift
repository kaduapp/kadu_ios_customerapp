//
//  adressDetailCell.swift
//  UFly
//
//  Created by 3 Embed on 07/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AdressDetailCell: UITableViewCell {
    
    @IBOutlet weak var reviewButton: UIButton!
    @IBOutlet weak var storeView: UIView!
    @IBOutlet weak var storeLogo: UIImageView!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var storeAdress: UILabel!
    @IBOutlet weak var userAdressView: UIView!
    @IBOutlet weak var adressTitle: UILabel!
    @IBOutlet weak var userAdress: UILabel!
    @IBOutlet weak var adressSeparator: UIView!
    @IBOutlet weak var orderStatusView: UIView!
    @IBOutlet weak var statusLogo: UIImageView!
    @IBOutlet weak var statusTitle: UILabel!
    @IBOutlet weak var statusSubTitle: UILabel!
    @IBOutlet weak var bottomSeperator: UIView!
    
    @IBOutlet weak var reviewBtnView: UIView!
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
    @IBOutlet weak var hightOfLoc: NSLayoutConstraint!
    @IBOutlet weak var dotedView: UIView!
    @IBOutlet weak var homeIcon: UIButton!
    ///header View
    
    @IBOutlet weak var detailsHeaderView: UIView!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var detailsIndicator: UIView!
    @IBOutlet weak var helpIndicator: UIView!
    @IBOutlet weak var headerSeparator: UIView!
    @IBOutlet weak var headerHight: NSLayoutConstraint!
    @IBOutlet weak var detailsBtn: UIButton!
    @IBOutlet weak var helpBtn: UIButton!

    
    var isFromTrackOrder = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    /// initial view setup
    func initialSetup(){
        
        reviewButton.isHidden = true
        Helper.setButton(button: reviewButton , view: reviewBtnView, primaryColour: Colors.SecondBaseColor, seconderyColor: Colors.AppBaseColor,shadow: false)
        
        Helper.DotedBorder(sender: dotedView)
        detailsHeaderView.backgroundColor = Colors.ScreenBackground
        Fonts.setPrimaryBold(detailsLabel)
        Fonts.setPrimaryBold(helpLabel)
        helpLabel.text = StringConstants.FAQs()
        detailsLabel.text = StringConstants.Details()
        detailsLabel.textColor = Colors.SectionHeader
        helpLabel.textColor = Colors.SectionHeader
        detailsIndicator.backgroundColor = Colors.SectionHeader
        helpIndicator.backgroundColor = Colors.SectionHeader
        headerSeparator.backgroundColor = Colors.SeparatorLight
        
        Fonts.setPrimaryMedium(storeName)
        Fonts.setPrimaryMedium(adressTitle)
        Fonts.setPrimaryMedium(statusTitle)
        Fonts.setPrimaryMedium(reviewButton)
        Fonts.setPrimaryRegular(storeAdress)
        Fonts.setPrimaryRegular(userAdress)
        Fonts.setPrimaryRegular(statusSubTitle)
        
        Helper.setButtonTitle(normal: StringConstants.Review(), highlighted: StringConstants.Review(), selected: StringConstants.Review(), button: reviewButton)
        storeAdress.textColor = Colors.SeconderyText
        userAdress.textColor = Colors.SeconderyText
        statusSubTitle.textColor = Colors.SeconderyText

        storeName.textColor = Colors.PrimaryText
        adressTitle.textColor = Colors.PrimaryText
        statusTitle.textColor = Colors.PrimaryText
        adressSeparator.backgroundColor = Colors.ScreenBackground
    }
    
    
    /// details is selected
    func updateHelpHeader(){
        detailsIndicator.isHidden = false
        helpIndicator.isHidden = true
        detailsLabel.textColor = Colors.SectionHeader
        helpLabel.textColor = Colors.PlaceHolder
    }
    
    ///help button selected
    func updateDetails(){
        
        detailsIndicator.isHidden = true
        helpIndicator.isHidden = false
        helpLabel.textColor = Colors.SectionHeader
        detailsLabel.textColor = Colors.PlaceHolder
    }
    
    /// updates the View Based on the OrderStatus
    ///
    /// - Parameter data: Is of type Order Model Object
    func setData(data:Order) {
        storeName.text = data.Storename.uppercased()
        storeAdress.text = data.PickAddress
        
        statusLogo.image = Helper.setImageForStatus(status: data.Status)
        statusTitle.text = data.StatusMessage
        if data.StatusMessage.length == 0 {
            statusTitle.text = Helper.setTitleForStatus(status: data.Status)
        }
        if (data.Status == 7 || data.Status == 15) && data.Reviewed == false {
            self.reviewButton.isHidden = false
            Helper.setButtonTitle(normal: StringConstants.Review(), highlighted: StringConstants.Review(), selected: StringConstants.Review(), button: reviewButton)
        }else if data.Status >= 10 && data.Status <= 14 {
            self.reviewButton.isHidden = false
            Helper.setButtonTitle(normal: StringConstants.Chat(), highlighted: StringConstants.Chat(), selected: StringConstants.Chat(), button: reviewButton)
        }else{
            if data.Status < 15 && !isFromTrackOrder{
            Helper.setButtonTitle(normal: StringConstants.TrackOrder(), highlighted: StringConstants.TrackOrder(), selected: StringConstants.TrackOrder(), button: reviewButton)
            self.reviewButton.isHidden = false
            }else{
             self.reviewButton.isHidden = true
            }
        }
        
        if data.DriverunAssigned
        {
         self.reviewButton.isHidden = true
        }

        Helper.setImage(imageView:storeLogo,url:data.StoreLogo,defaultImage: #imageLiteral(resourceName: "SingleShowDefault"))
        if data.Delivery {
            userAdress.text = data.DropAddress
            adressTitle.text = StringConstants.DeliveryLocation()
            topSpace.constant = 15
            bottomSpace.constant = 15
            hightOfLoc.constant = 15
            dotedView.isHidden = false
            homeIcon.isHidden = false
        }
        else {
            homeIcon.isHidden = true
            userAdress.text = ""
            adressTitle.text = ""
            topSpace.constant = 0
            bottomSpace.constant = 0
            hightOfLoc.constant = 0
            dotedView.isHidden = true
            
        }
    }
    
}
