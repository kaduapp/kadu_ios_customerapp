//
//  HistoryUIE.swift
//  UFly
//
//  Created by 3 Embed on 15/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension HistoryVC {
    
    
    /// initial view setup
    func updateView() {
        
        Helper.setButtonTitle(normal: StringConstants.ActiveOrder(), highlighted: StringConstants.ActiveOrder(), selected: StringConstants.ActiveOrder(), button: activeOrderButton)
        Helper.setButtonTitle(normal: StringConstants.PastOrder(), highlighted: StringConstants.PastOrder(), selected: StringConstants.PastOrder(), button: pastOrderButton)
        activeOrderButton.setTitleColor(Colors.PrimaryText, for: .normal)
        pastOrderButton.setTitleColor(Colors.Unselector, for: .normal)
        
        indicator.backgroundColor = Colors.PrimaryText
        
        Fonts.setPrimaryBold(pastOrderButton)
        Fonts.setPrimaryBold(activeOrderButton)
        
        
        self.historyVM.refreshControlActive = UIRefreshControl()
        self.historyVM.refreshControlActive.addTarget(self, action: #selector(HistoryVC.refresh(sender:)), for: UIControl.Event.valueChanged)
        activeOrderTableView.addSubview(self.historyVM.refreshControlActive)
        
        self.historyVM.refreshControlPast = UIRefreshControl()
        self.historyVM.refreshControlPast.addTarget(self, action: #selector(HistoryVC.refresh(sender:)), for: UIControl.Event.valueChanged)
        pastOrderTableView.addSubview(self.historyVM.refreshControlPast)
     
     self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
        pastOrderTableView.rowHeight = UITableView.automaticDimension
        activeOrderTableView.rowHeight = UITableView.automaticDimension
        
        pastOrderTableView.backgroundColor = Colors.ScreenBackground
        activeOrderTableView.backgroundColor = Colors.ScreenBackground
        
    }
    
    
    func setBanner() {
        dummyView.addSubview(Helper.showUnVerifyNotification().0)
        dummyView.clipsToBounds = true
        if Helper.showUnVerifyNotification().1 {
            bannerHeight.constant = 20
        }else{
            bannerHeight.constant = 0
        }
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
    
    /// this method Gets Called when scrollViewDidEndDragging, scrollViewDidEndDecelerating
    ///moves to next or previous screen with animation
    /// - Parameter page: is of type Integer
    func scrollToPage(page:Int){
        UIView.animate(withDuration: 0.3){
            self.scrollView.contentOffset.x = self.scrollView.frame.width*CGFloat(page)
            self.updatePage(scrolView1: self.scrollView)
        }
    }
    
    
    func setEmptyScreen() {
        emptyViewActive.setData(image: #imageLiteral(resourceName: "EmptyHistory"), title: StringConstants.EmptyActive(), buttonTitle: StringConstants.StartShopping(),buttonHide: false)
        emptyViewActive.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
        emptyViewPast.setData(image: #imageLiteral(resourceName: "EmptyHistory"), title: StringConstants.EmptyPast(), buttonTitle: StringConstants.StartShopping(),buttonHide: false)
        emptyViewPast.emptyButton.addTarget(self, action:#selector(StartShopping) , for: UIControl.Event.touchUpInside)
    }
    
    @objc func StartShopping() {
        if (isFromProfile)
        {
            self.dismiss(animated: true, completion: nil)
        }else
        {
        Helper.changetTabTo(index: AppConstants.Tabs.Home)
        }
    }
}
