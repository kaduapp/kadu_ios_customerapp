//
//  CouponVC.swift
//  DelivX
//
//  Created by 3 Embed on 03/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


protocol CouponVCDelegate {
    func couponRespons(success: Bool)
}

class CouponVC: UIViewController {
    
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var couponHeader: UIView!
    @IBOutlet weak var couponCodeTextField: UITextField!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var tempText = ""
    var checkoutVM = CheckoutVM()
    var couponVM = CouponVM()
    static let coupon_response = PublishSubject<Bool>()
    var delegate: CouponVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        didGetResponse()
        couponCodeTextField.Semantic()
        Helper.PullToClose(scrollView: mainTableView)
        Helper.pullToDismiss?.delegate = self
        mainTableView.backgroundColor =  Colors.ScreenBackground
        titleLabel.textColor =  Colors.PrimaryText
        Fonts.setPrimaryMedium(titleLabel)
        self.titleLabel.text = StringConstants.ApplyCoupon()
        couponHeader.backgroundColor = Colors.ScreenBackground
        couponCodeTextField.placeholder = StringConstants.PromocodeMissing()
        applyButton.setTitle(StringConstants.Apply(), for: .normal)
        applyButton.setTitleColor(Colors.AppBaseColor, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
        couponVM.getCoupons()
        // self.title = StringConstants.ApplyCoupon
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeObserver()
    }
    
    
    @IBAction func didPressApply(_ sender: Any) {
        applyPromocode()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.view.endEditing(true)
        }) { (Bool) in
            UIView.animate(withDuration: 0.3, animations: {
                self.dismiss(animated: true, completion: nil)
            })
        }
        
    }
    
    func updateCoupon(data:Coupon){
        couponCodeTextField.text = data.Code
        tempText = data.Code
    }
    
}
extension CouponVC {
    
    func didGetResponse(){
        checkoutVM.checkoutVM_response.subscribe(onNext: {success in
            if success == CheckoutVM.ResponseType.Promocode {
                self.delegate?.couponRespons(success: true)
                Helper.showAlert(message: String(format:StringConstants.AppliedPromocode(),self.checkoutVM.promocode), head: StringConstants.WooHoo(), type: 0)
                self.closeAction(self)
                
            }
        }).disposed(by: self.couponVM.disposeBag)
        couponVM.couponVM_response.subscribe(onNext: { success in
            if success == true {
                self.mainTableView.reloadData()
            }
        }).disposed(by: self.couponVM.disposeBag)
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: CouponDetailsVC.self) {
            if let controller = segue.destination as? CouponDetailsVC {
                controller.coupon = sender as! Coupon
            }
        }
    }
}
extension CouponVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == mainTableView {
            if scrollView.contentOffset.y > 0 {
                Helper.setShadow(sender: titleView)
            }else{
                Helper.removeShadow(sender: titleView)
            }
        }
    }
}
