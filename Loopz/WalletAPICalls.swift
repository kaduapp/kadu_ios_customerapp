//
//  WalletAPICalls.swift
//  DelivX
//
//  Created by 3Embed on 12/06/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire

class WalletAPICalls: NSObject {

    
    //get wallet balance
    class func getBalance() ->Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, APIEndTails.WalletDetails, parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [String:Any] {
//                        if let titleTemp2 = titleTemp[APIResponceParams.WalletBalance] as? String {
//                            Utility.setWallet(data: Float(titleTemp2)!)
//                        }
//                        if let titleTemp2 = titleTemp[APIResponceParams.WalletBalance] as? Double {
//                            Utility.setWallet(data: Float(titleTemp2))
//                        }
                        Utility.setWallet(data: titleTemp)
                    }
                    observer.onNext(true)
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: APICalls.disposesBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    //add money to wallet
    class func updateBalance(data: Card, amount: Float) ->Observable<Bool> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let params = [
            APIRequestParams.CardId     :   data.Id,
            APIRequestParams.Amount2    :   amount
            ] as [String : Any]
        
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.post, APIEndTails.RechargeWallet, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [String:Any] {
//                        if let titleTemp2 = titleTemp[APIResponceParams.WalletBalance] as? String {
//                            Utility.setWallet(data: Float(titleTemp2)!)
//                        }
//                        if let titleTemp2 = titleTemp[APIResponceParams.WalletBalance] as? Float {
//                            Utility.setWallet(data: titleTemp2)
//                        }
                        Utility.setWallet(data: titleTemp)

                    }
                    observer.onNext(true)
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: APICalls.disposesBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    //get wallet balance
    class func getTransaction(page:Int) ->Observable<([Transaction],[Transaction])> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.get, APIEndTails.WalletTransaction + "\(page)", parameters: [:], headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    var arrayDebit:[Transaction] = []
                    var arrayCredit:[Transaction] = []
                    if let titleTemp = bodyIn[APIResponceParams.Data] as? [String:Any] {
                        if let titleTemp2 = titleTemp[APIResponceParams.CreditTransctions] as? [Any] {
                            for item in titleTemp2 {
                                arrayCredit.append(Transaction.init(data: item as! [String:Any]))
                            }
                            
                        }
                        if let titleTemp2 = titleTemp[APIResponceParams.DebitTransctions] as? [Any] {
                            for item in titleTemp2 {
                                arrayDebit.append(Transaction.init(data: item as! [String:Any]))
                            }
                            
                        }
                        
                    }
                    observer.onNext((arrayDebit,arrayCredit))
                    observer.onCompleted()
                }
                else{
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: APICalls.disposesBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    //Redeem Vocuher
    class func redeemVoucher(voucher:String) ->Observable<(Bool,String)> {
        if !AppConstants.Reachable {
            Helper.showNoInternet()
            return Observable.create{ observer in
                return Disposables.create()
                }.share(replay: 1)
        }
        Helper.showPI(string: "")
        let params = [APIRequestParams.VoucherCode    :   voucher]
        let header = Utility.getHeader()
        return Observable.create { observer in
            RxAlamofire.requestJSON(.patch, APIEndTails.VoucherRedeem, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                let bodyIn = body as![String:Any]
                let message = bodyIn[APIResponceParams.Message] as! String
                Helper.hidePI()
                let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    observer.onNext((true,message))
                    observer.onCompleted()
                }else{
                    observer.onNext((false,message))
                    observer.onCompleted()
                    APICalls.basicParsing(data:body as! [String : Any],status:head.statusCode)
                }
            }, onError: { (Error) in
                Helper.showAlert(message: StringConstants.SomethingWentWrong(),head: StringConstants.Error(), type: 1)
                Helper.hidePI()
            }).disposed(by: APICalls.disposesBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
}
