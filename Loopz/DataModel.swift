//
//  DataModel.swift
//  History
//
//  Created by 3Embed on 05/06/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import UIKit

struct OrderDetails
{
    var storeType:String?
    var storeLBBackgroundColor:UIColor?
    var storeName:String?
    var address:String?
    var orderId:String?
    var deliveryFee:String?
    var price:String?
}

struct order
{
    var timeOfOrder:String?
    var orders:[OrderDetails]?
    var actualcharge:String?
    var deliveryCharge:String?
    var discount:String?
    var toPay:String?
}


struct DataModel
{
    var dataToDisplay:[order]
    {         
        let one1 = OrderDetails(storeType: "Restaurant", storeLBBackgroundColor: UIColor(red: 158/255.0, green: 208/255.0, blue: 89/255.0, alpha: 1),storeName: "Mcdonald’s", address: "125-101 West St, Powhattan,USA", orderId: "Order ID: KE123HY83", deliveryFee: nil,price:"$ 40.00")
        let two1 = OrderDetails(storeType: "Supermarket", storeLBBackgroundColor: UIColor(red: 235/255.0, green: 189/255.0, blue: 31/255.0, alpha: 1), storeName: "Amarillo We Go Shop", address: "125-101 West St, Powhattan,USA", orderId: "Order ID: AW123HY34", deliveryFee: "$ 5.00",price:"$ 20.00")
        let two2 = OrderDetails(storeType: "Supermarket", storeLBBackgroundColor: UIColor(red: 235/255.0, green: 189/255.0, blue: 31/255.0, alpha: 1), storeName: "Walmart Supercenter", address: "125-101 West St, Powhattan,USA", orderId: "Order ID: AW123HY34", deliveryFee: "$ 5.00", price: "$ 20.00")
        
        
        
        let one = order(timeOfOrder: "Placed on Mon, 27 June 2017, 10:30 AM", orders: [one1], actualcharge: "$ 60.00", deliveryCharge: "$ 5.00", discount: nil, toPay: "$ 65.00")
        let two = order(timeOfOrder: "Placed on Fri, 12 June 2017, 10:30 AM", orders: [one1,two1], actualcharge: "$ 60.00", deliveryCharge: "$ 5.00", discount: nil, toPay: "$ 60.00")
        let three = order(timeOfOrder: "Placed on Fri, 12 June 2017, 10:30 AM", orders: [two1,one1,two2], actualcharge: "$ 60.00", deliveryCharge: nil, discount: "$ 5.00", toPay: "$ 60.00")
        let four = order(timeOfOrder: "Placed on Fri, 12 June 2017, 10:30 AM", orders: [two1,two2], actualcharge: "$ 60.00", deliveryCharge: "$ 5.00", discount: "$ 5.00", toPay: "$ 60.00")
        let five = order(timeOfOrder: "Placed on Fri, 12 June 2017, 10:30 AM", orders: [two1,two2,two1], actualcharge: "$ 60.00", deliveryCharge: nil, discount: nil, toPay: "$ 60.00")
        let six = order(timeOfOrder: "Placed on Fri, 12 June 2017, 10:30 AM", orders: [one1,two1,two2], actualcharge: "$ 60.00", deliveryCharge: nil, discount: nil, toPay: "$ 60.00")
        let seven = order(timeOfOrder: "Placed on Fri, 12 June 2017, 10:30 AM", orders: [one1,one1,two1,two2], actualcharge: "$ 60.00", deliveryCharge: nil, discount: nil, toPay: "$ 60.00")
        
        
        return [one,two,three,four,five,six,seven]
    }
}
