//
//  OTPVM.swift
//  UFly
//
//  Created by 3Embed on 07/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class OTPVM: NSObject {
    
    
    /// this enum holds the ResponseType predefined constatnts it is of type Int
    ///
    /// - LoginDone: having value Int 1 for login response
    /// - NewPassword: having value Int 2 for update password login response
    /// - UpdateProfileData: having value Int 3 for updateProfile response
    enum ResponseType: Int {
        case LoginDone   =   1
        case NewPassword =   2
        case UpdateProfileData  = 3
        case Failuar = 4
    }
    
    //variables
    var authentication:Authentication? = nil
    var isFromForgotPW = false
    var isFromUpdateProfile = false
    
    let disposeBag = DisposeBag()
    let otpVM_response = PublishSubject<ResponseType>()
    
    /// this method observe get Otp API response defined in AuthenticationAPICalls having subscription to that
    func getOTP(show:Bool) {
        if isFromForgotPW {
            callNext()
            return
        }
        AuthenticationAPICalls.getOTP(user:authentication!,show:show).subscribe().disposed(by: disposeBag)
    }
    
    func callNext() {
        
        let verifyType = 1
        let processType = 2
        let emailOrMobile = authentication?.Phone
        let code = authentication?.CountryCode
        
        AuthenticationAPICalls.ForgotPassword(emailOrMobile: emailOrMobile!, code: code!, VerifyType: verifyType, ProcessType: processType).subscribe().disposed(by: self.disposeBag)
    }
    
    /// his method observe get login API response defined in AuthenticationAPICalls having subscription to that
    func signIn() {
        AuthenticationAPICalls.loginAPI(user:authentication!).subscribe(onNext: {[weak self]result in
            self?.otpVM_response.onNext(.LoginDone)
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
    
    ///  method observe get verifyOTP API response defined in AuthenticationAPICalls having subscription to that
    ///
    /// - Parameter otp: otp is of type string
    func verify(otp:String) {
        authentication?.Otp = otp
        AuthenticationAPICalls.verifyOTP(user:authentication!).subscribe(onNext: {[weak self]success in
            if success == true {
                if self?.isFromForgotPW == true {
                    self?.otpVM_response.onNext(.NewPassword)
                }
                else {//if self.isFromUpdateProfile == true {
                    self?.otpVM_response.onNext(.UpdateProfileData)
                }
                //                else{
                //                    self.signIn()
                //                }
            }
            else if success == false {
                self?.otpVM_response.onNext(.Failuar)
            }
            }, onError: {error in
                print(error)
        }).disposed(by: disposeBag)
    }
}
