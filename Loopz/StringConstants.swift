//
//  StringConstants.swift
//  UFly

//  Created by 3Embed on 27/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class StringConstants: NSObject {
    //VNHCountryPicker
    class func TypeCountry() -> String {
        return OSLocalizedString("Type Country", comment: "Type Country")
    }
    //checkout
    class func DeliverAt() -> String {
        return OSLocalizedString("Deliver At", comment: "Deliver At")
    }
    class func DeliveryTime() -> String {
        return OSLocalizedString("Delivery Time", comment: "Delivery Time")
    }
    class func Cash() -> String {
        return OSLocalizedString("Cash", comment: "Cash")
    }
    
    class func iDeal() -> String {
        return OSLocalizedString("iDeal", comment: "iDeal")
    }
    
    class func Favour() -> String {
        return OSLocalizedString("Favour", comment: "Favour")
    }
    
    class func wfdsc() -> String {
        return OSLocalizedString("QUICK CARD", comment: "QUICK CARD")
    }
    class func Now() -> String {
        return OSLocalizedString("Now", comment: "Now")
    }
    
    class func NowExtend() -> String {
        return OSLocalizedString("Now orders are meant to place orders where the courier delivers your order ASAP.", comment: "Now orders are meant to place orders where the courier delivers your order ASAP.")
    }
    
    class func NowExtendForPickUp() -> String {
        return OSLocalizedString("Now orders are meant to place orders where you can pickup your order ASAP.", comment: "Now orders are meant to place orders where you can pickup your order ASAP.")
    }
    class func Normal() -> String {
        return OSLocalizedString("Normal", comment: "Normal")
    }
    
    class func NormalExtend() -> String {
        return OSLocalizedString("There is no extra charge for delivery. turnaround time is minimum 48 hours.", comment: "There is no extra charge for delivery. turnaround time is minimum 48 hours.")
    }
    class func Express() -> String {
        return OSLocalizedString("Express", comment: "Express")
    }
    
    class func ExpressExtend() -> String {
        return OSLocalizedString("The value of the express delivery  depends on weight of the laundry.turnaround time is less than 12 hours.", comment: "The value of the express delivery  depends on weight of the laundry.turnaround time is less than 12 hours.")
    }
    class func selectLocation() -> String {
        return OSLocalizedString("Select Location", comment: "Select Location")
    }
    class func selectPayment() -> String {
        return OSLocalizedString("Select Payment", comment: "Select Payment")
    }
    class func CancelUpper() -> String {
        return OSLocalizedString("CANCEL", comment: "CANCEL")
    }
    class func Later() -> String {
        return OSLocalizedString("Schedule", comment: "Schedule")
    }
    class func LaterExtend() -> String {
        return OSLocalizedString("Scheduled orders are meant to place orders where the courier delivers your order at the selected delivery slot.", comment: "Scheduled orders are meant to place orders where the courier delivers your order at the selected delivery slot.")
    }
    
    class func LaterExtendForPickup() -> String {
        return OSLocalizedString("Scheduled orders are meant to place order where you can pickup your order at the selected slot.", comment: "Scheduled orders are meant to place order where you can pickup your order at the selected slot.")
    }
    
    class func PaymentMethod() -> String {
        return OSLocalizedString("Payment Method", comment: "Payment Method")
    }
    
    class func PaymentFailed() -> String {
        return OSLocalizedString("Payment Failed", comment: "Payment Failed")
    }

    
    class func SelectPayment() -> String {
        return OSLocalizedString("Select Payment", comment: "Select Payment")
    }
    class func AddToList() -> String {
        return OSLocalizedString("Add To List", comment: "Add To List")
    }

    //Cart
    class func DeliveryCharge() -> String {
        return OSLocalizedString("Delivery Charge", comment: "Delivery Charge")
    }
    class func Card() -> String {
        return OSLocalizedString("Card", comment: "Card")
    }
    class func AddNewItems() -> String {
        return OSLocalizedString("Add new items", comment: "Add new items")
    }
    class func Store() -> String {
        return OSLocalizedString("Store", comment: "Store")
    }
    class func Stores() -> String {
        return OSLocalizedString("Stores", comment: "Stores")
    }
    class func StoreSelected() -> String {
        return OSLocalizedString("Selected Store", comment: "Selected Store")
    }
    class func Items() -> String {
        return OSLocalizedString("Items", comment: "Items")
    }
    class func Item() -> String {
        return OSLocalizedString("Item", comment: "Item")
    }
    class func TotalAmount() -> String {
        return OSLocalizedString("Cart Total", comment: "Cart Total")
    }
    
    class func ChangeText() -> String {
        return  OSLocalizedString("Change", comment: "Change")
    }
    class func YourSavings() -> String {
        return OSLocalizedString("Your Savings", comment: "Your Savings")
    }
    class func DriverTip() -> String {
             return OSLocalizedString("Driver Tip", comment: "Driver Tip")
         }
    class func ConveinceFee() -> String {
           return OSLocalizedString("Conveince Fee", comment: "Conveince Fee")
       }
    class func Conveniencefee() -> String{
        return OSLocalizedString("Convenience fee", comment:"Convenience fee")
    }
    
    
    class func Checkout() -> String {
        return OSLocalizedString("CHECKOUT", comment: "CHECKOUT")
    }
    class func CheckoutLower() -> String {
        return OSLocalizedString("Checkout", comment: "Checkout")
    }
    class func EmptyCart() -> String {
        return OSLocalizedString("No Items In Your Cart", comment: "No items in your cart")
    }
    class func EmptyCard() -> String {
        return OSLocalizedString("No Cards Saved", comment: "No Cards Saved")
    }
    class func Recommended() -> String {
        return OSLocalizedString("Recommended", comment: "Recommended")
    }

    //Payment ViewController
    class func AddNewCard() -> String {
        return OSLocalizedString("Add New Card", comment: "Add New Card")
    }

    class func AddToWallet() -> String {
        return OSLocalizedString("Add money to wallet", comment: "Add money to wallet")
    }
    class func Passbook() -> String {
        return OSLocalizedString("Passbook", comment: "Passbook")
    }
    class func SavedCards() -> String {
        return OSLocalizedString("Saved Cards", comment: "Saved Cards")
    }
    class func ChooseCards() -> String {
        return OSLocalizedString("Choose Your Card", comment: "Choose Your Card")
    }


    //inviteVC
    class func ShareText() -> String {
        return OSLocalizedString("Join %@ today and download our app with refferal code: %@ and get access to some amazing Personalized Deals just for you!", comment: "Join %@ today and download our app with refferal code: %@ and get access to some amazing Personalized Deals just for you!")
    }
    class func YourRefCodeIs() -> String {
        return OSLocalizedString("YOUR REFFERAL CODE IS ", comment: "YOUR REFFERAL CODE IS")
    }
    class func Invite() -> String {
        return OSLocalizedString("INVITE", comment: "INVITE")
    }

    //Common
    class func Done() -> String {
        return OSLocalizedString("DONE", comment: "DONE")
    }
    class func DoneLower() -> String {
        return OSLocalizedString("Done", comment: "Done")
    }
    //LaunchVC
    class func DetectYourLocation() -> String {
        return OSLocalizedString("DETECT MY LOCATION", comment: "DETECT MY LOCATION")
    }
    class func SetLocation() -> String {
        return OSLocalizedString("Set Location", comment: "Set Location")
    }
    class func Manually() -> String {
        return OSLocalizedString("Manually", comment: "Manually")
    }
    class func RecentLocation() -> String {
        return OSLocalizedString("Recent searches", comment: "Recent searches")
    }
    class func ResultLocation() -> String {
        return OSLocalizedString("Result", comment: "Result")
    }

    //Select Location
    class func DeliveryLocation() -> String {
        return OSLocalizedString("DELIVERY LOCATION", comment: "DELIVERY LOCATION")
    }

    class func EmptyLocLabelText() -> String {
        return OSLocalizedString("\nWe need to make sure you are in %@ Turn on the location services to get started", comment: "W\nWe need to make sure you are in %@ Turn on the location services to get started")
    }

    //Login
    class func ClickHereToSignUp() -> String {
        return OSLocalizedString("Don’t have an account?", comment: "Don’t have an account?")
    }
    class func Login() -> String {
        return OSLocalizedString("Login", comment: "Login")
    }
    class func Continue() -> String {
        return OSLocalizedString("SIGN IN", comment: "SIGN IN")
    }
    
    class func LoginWithFacebook() -> String {
        return OSLocalizedString("Login With Facebook", comment: "Login With Facebook")
    }
    
    class func LiveFor() -> String {
        return OSLocalizedString("Sign in", comment: "Sign in")
    }
    class func EnterPh() -> String {
        return OSLocalizedString("Enter your phone number to proceed", comment: "Enter your phone number to proceed")
    }


    class func ForgotPassword() -> String {
        return OSLocalizedString("Forgot Password?", comment:"Forgot Password?")
    }
    class func EmailOrPhone() -> String {
        return OSLocalizedString("Phone Number", comment: "Phone Number")
    }
    class func signUp() -> String {
        return OSLocalizedString("Sign Up", comment: "Sign Up")
    }
    class func signup() -> String {
        return OSLocalizedString("Sign Up", comment: "Sign Up")
    }
    class func GotoSignUp() -> String {
        return OSLocalizedString("Dont you have an account ? Sign Up", comment: "Dont you have an account ? Sign Up")
    }
    class func haveAnAccount() -> String {
        return OSLocalizedString("Dont you have an account ?", comment: "Dont you have an account ?")
    }
    
    class func uploadImage()->String{
        
        return OSLocalizedString("Upload new image.", comment: "Upload new image.")
        
    }
    
    
    class func PhoneNumber() -> String {
        return OSLocalizedString("PHONE NUMBER", comment: "PHONE NUMBER")
    }
    class func Password() -> String {
        return OSLocalizedString("PASSWORD", comment: "PASSWORD")
    }
    class func PhoneNumMissing() -> String {
        return OSLocalizedString("Please Enter Your Phone Number", comment: "Please Enter Your Phone Number")
    }
    class func PasswordMissing() -> String {
        return OSLocalizedString("Password length should be minimum 6", comment: "Password length should be minimum 6")
    }


    //ForgotPassword
    class func Email() -> String {
        return OSLocalizedString("EMAIL ADDRESS", comment: "EMAIL ADDRESS")
    }
    class func OnlyEmail() -> String {
        return OSLocalizedString("EMAIL", comment: "EMAIL")
    }
    class func OR() -> String {
        return OSLocalizedString("OR", comment: "OR")
    }
    class func Next() -> String {
        return OSLocalizedString("NEXT", comment: "NEXT")
    }
    class func EnterYourMobNum() -> String {
        return OSLocalizedString("Enter your mobile Number / Email address and you will get verification code / link to reset password", comment: "Enter your mobile Number / Email address and you will get verification code / link to reset password")
    }
    class func ForgotPasswordHead() -> String {
        return OSLocalizedString("FORGOT PASSWORD", comment: "FORGOT PASSWORD")
    }

    //OTP ViewController
    class func VerifyCode() -> String {
        return OSLocalizedString("VERIFICATION CODE", comment: "VERIFICATION CODE")
    }
    class func Verify() -> String {
        return OSLocalizedString("VERIFY OTP", comment: "VERIFY OTP")
    }
    class func VerifyNum() -> String {
        return OSLocalizedString("VERIFY NUMBER", comment: "VERIFY NUMBER")
    }
    class func OtpTitle() -> String {
        return OSLocalizedString("Verification code sent to", comment:"Verification code sent to")
    }
    class func ResendOtp() -> String {
        return OSLocalizedString("Didn’t get code? Resend", comment: "Didn’t get code? Resend")
    }
    class func NoCode() -> String {
        return OSLocalizedString("Didn’t get code?", comment: "Didn’t get code?")
    }
    class func Resend() -> String {
        return OSLocalizedString("Resend", comment: "Resend")
    }

    //UpdatePasswordVC
    class func YourNewPW() -> String {
        return OSLocalizedString("YOUR NEW PASSWORD", comment: "YOUR NEW PASSWORD")
    }
    class func EnterNewPW() -> String {
        return OSLocalizedString("ENTER NEW PASSWORD", comment: "ENTER NEW PASSWORD")
    }
    class func ReEnterPW() -> String {
        return OSLocalizedString("RE-ENTER PASSWORD", comment: "RE-ENTER PASSWORD")
    }
    class func Update() -> String {
        return OSLocalizedString("UPDATE", comment: "UPDATE")
    }
    class func MobileNumVerified() -> String {
        return OSLocalizedString("Mobile Number Verified Enter Your New Password", comment: "Mobile Number Verified Enter Your New Password")
    }
    class func PWMisMatch() -> String {
        return  OSLocalizedString("Password mis-matched", comment: "Password mis-matched")
    }


    //Signup
    class func Refferal() -> String {
        return OSLocalizedString("REFERRAL CODE", comment: "REFERRAL CODE")
    }
    class func RefferalCode() -> String {
        return OSLocalizedString("I have a refferal code", comment: "I have a refferal code")
    }
    class func Name() -> String {
        return OSLocalizedString("NAME", comment: "NAME")
    }
    class func FullName() -> String {
        return OSLocalizedString("FULL NAME", comment: "FULL NAME")
    }

    class func Terms() -> String {
        return OSLocalizedString("By creating this account, you agree to our Terms & Conditions.", comment: "By creating this account, you agree to our Terms & Conditions.")
    }
    class func TAndC() -> String {
        return OSLocalizedString("Terms & Conditions.", comment:"Terms & Conditions.")
    }

    //Upload id
    class func Upload() -> String {
        return  OSLocalizedString("Upload", comment: "Upload")
    }
    class func Skip() -> String {
        return OSLocalizedString("Skip", comment: "Skip")
    }
    class func IDCard() -> String {
        return OSLocalizedString("IDENTITY CARDS", comment: "IDENTITY CARDS")
    }
    class func MMJCard() -> String {
        return OSLocalizedString("MEDICAL CARD", comment: "MEDICAL CARD")
    }
    class func Uploaded() -> String {
        return OSLocalizedString("Uploaded", comment: "Uploaded")
    }
    class func YourIdentity() -> String {
        return OSLocalizedString("YOUR IDENTITY", comment: "YOUR IDENTITY")
    }
    class func UploadId() -> String {
        return OSLocalizedString("UPLOAD IDENTITY", comment: "UPLOAD IDENTITY")
    }
    class func UploadIDMMJ() -> String {
        return OSLocalizedString("Upload your identity & Medical card", comment: "Upload your identity & Medical card")
    }
    class func UploadIDTitle() -> String {
        return OSLocalizedString("Upload any government issued identity Card.", comment: "Upload any government issued identity Card.")
    }
    class func UploadMMj() -> String {
        return OSLocalizedString("Upload your Medical card.", comment: "Upload your Medical card.")
    }
    class func GetMMJ() -> String {
        return OSLocalizedString("Don’t have a Medical card? Get one", comment: "Don’t have a Medical card? Get one")
    }
    class func GetOne() -> String {
        return OSLocalizedString("Get one", comment: "Get one")
    }
    class func UploadVCBottomText() -> String {
        return OSLocalizedString("Thank you for submiting the document, our account verification team will validate the document and will send notification when verification compleated", comment: "Thank you for submiting the document, our account verification team will validate the document and will send notification when verification compleated")
    }

    //Home
    class func SearchData() -> String {
        return OSLocalizedString("Search for product or dispensaries", comment: "Search for product or dispensaries")
    }
    class func TrendingProducts() -> String {
        return OSLocalizedString("Trending Products", comment: "Trending Products")
    }
    class func EveryDayLowPrice() -> String {
        return OSLocalizedString("Everyday Low Price", comment: "Everyday Low Price")
    }
    class func Price() -> String {
        return OSLocalizedString("Price", comment: "Price")
    }
    class func Categories() -> String {
        return OSLocalizedString("Categories", comment: "Categories")
    }

    class func DispensariesSearch() -> String {
        return OSLocalizedString("DISPENSARIES", comment: "DISPENSARIES")
    }

    //Profile
    class func Language() -> String {
        return OSLocalizedString("Language", comment: "Language")
    }
    class func AvailableLanguages() -> String {
        return OSLocalizedString("Available Languages", comment: "Available Languages")
    }
    class func Logout() -> String {
        return OSLocalizedString("Logout", comment: "Logout")
    }
    class func ManageAddress() -> String {
        return OSLocalizedString("Manage Addresses", comment: "Manage Addresses")
    }
    
    class func Cart()-> String{
        return OSLocalizedString("Card", comment: "Card")
    }
    
    class func Payment() -> String {
        return OSLocalizedString("Payments", comment: "Payments")
    }
    class func Favourite() -> String {
        return OSLocalizedString("Favorites", comment: "Favorites")
    }
    class func MyOrders() -> String {
        return OSLocalizedString("My Orders", comment: "My Orders")
    }
    class func Offers() -> String {
        return OSLocalizedString("Offers", comment: "Offers")
    }
    class func Help() -> String {
        return OSLocalizedString("Help", comment: "Help")
    }
    class func ReferFriends() -> String {
        return OSLocalizedString("Refer Your Friends", comment: "Refer Your Friends")
    }
    class func Hi() -> String {
        return OSLocalizedString("Hi", comment: "Hi")
    }
    class func ConfirmSave() -> String {
        return OSLocalizedString("Do you want to Save Changes", comment: "Do you want to Save Changes")
    }
    class func Warning() -> String {
        return OSLocalizedString("Warning" , comment: "Warning")
    }
    class func HelpCenter() -> String {
        return OSLocalizedString("Help Center", comment: "Help Center")
    }
    class func MyAccount() -> String {
        return OSLocalizedString("MY ACCOUNT", comment: "MY ACCOUNT")
    }
    //Address
    class func GettingAddress() -> String {
        return OSLocalizedString("Getting Address", comment: "Getting Address")
    }
    class func DeletingAddress() -> String {
        return OSLocalizedString("Deleting Address", comment: "Deleting Address")
    }
    class func AddingAddress() -> String {
        return OSLocalizedString("Adding Address", comment: "Adding Address")
    }
    class func Home() -> String {
        return OSLocalizedString("Home", comment:"Home")
    }
    class func Work() -> String {
        return OSLocalizedString("Work", comment:"Work")
    }
    class func Others() -> String {
        return OSLocalizedString("Others", comment:"Others")
    }
    class func AddNewAddress() -> String {
        return OSLocalizedString("Add New Address", comment: "Add New Address")
    }
    class func SaveAddress() -> String {
        return OSLocalizedString("Save Address", comment:"Save Address")
    }
    class func UpdateAddress() -> String {
        return OSLocalizedString("Update Address", comment:"Update Address")
    }
    class func TagMissing() -> String {
        return OSLocalizedString("Please Enter a Address Tag", comment: "Please Enter a Address Tag")
    }
    class func FlatMissing() -> String {
        return OSLocalizedString("Please Enter Flat Number", comment: "Please Enter Flat Number")
    }
    class func EmptyAddress() -> String {
        return OSLocalizedString("No saved addresses", comment: "No saved addresses")
    }
    class func SaveDeliveryLoc() -> String {
        return OSLocalizedString("Delivery Address", comment: "Delivery Address")
    }
    class func Address() -> String {
        return OSLocalizedString("ADDRESS", comment:"ADDRESS")
    }
    class func DoorStep() -> String {
        return OSLocalizedString("FLAT/HOUSE NO.", comment:"FLAT/HOUSE NO.")
    }
    class func Landmark() -> String {
        return OSLocalizedString("LANDMARK", comment:"LANDMARK")
    }

    class func ADDRESSTAG() ->String{
        return OSLocalizedString("ADDRESS TAG", comment: "ADDRESS TAG")
    }
    
    
    class func PhoneNumbers()->String{
        return OSLocalizedString("Phone Number", comment: "Phone Number")
    }
    class func saveAsLabel() -> String {
        return OSLocalizedString("SAVE AS", comment:"SAVE AS")
    }

    //Item Listing
    class func CBD() -> String {
        return OSLocalizedString("% CBD", comment: "% CBD")
    }
    class func THC() -> String {
        return OSLocalizedString("% THC", comment: "% THC")
    }
    class func ViewAll() -> String {
        return OSLocalizedString("View All", comment: "View All")
    }
    class func MoreProducts() -> String {
        return OSLocalizedString("MORE PRODUCTS", comment: "MORE PRODUCTS")
    }
    class func NearistDispensary() -> String {
        return OSLocalizedString("Nearest Dispensaries", comment: "Nearest Dispensaries")
    }
    class func MileAway() -> String {
        return OSLocalizedString("Miles", comment: "Miles")
    }
    class func KMAway() -> String {
        return OSLocalizedString("KMs", comment: "KMs")
    }
    class func Rating() -> String {
        return OSLocalizedString("Rating", comment: "Rating")
    }
    
    class func MinOrder() -> String {
        return OSLocalizedString("Min Order", comment: "Min Order")
    }
    class func Reviews() -> String {
        return OSLocalizedString("ESTIMATED TIME", comment: "ESTIMATED TIME")
    }
    class func AddCart() -> String {
        return OSLocalizedString("Add", comment: "Add")
    }
    class func AddToCart() -> String {
        return OSLocalizedString("Add To Cart", comment: "Add To Cart")
    }
    class func RateAndReview() -> String {
        return OSLocalizedString("Reviews", comment: "Reviews")
    }
    class func Review() -> String {
        return OSLocalizedString("Review", comment: "Review")
    }
    class func ExtraCharges() -> String {
        return OSLocalizedString("Extra charges may apply", comment: "Extra charges may apply")
    }

    class func Menu() -> String {
        return OSLocalizedString("MENU", comment: "MENU")
    }
    class func GotIt() -> String {
        return OSLocalizedString("GOT IT", comment: "GOT IT")
    }
    class func ViewCart() -> String {
        return OSLocalizedString("View Cart", comment: "View Cart")
    }
    class func FixedApplicable() -> String {
        return OSLocalizedString("fixed fee applicable", comment: "fixed fee applicable")
    }
    class func FreeDelivery() -> String {
        return OSLocalizedString("away from free delivery", comment: "away from free delivery")
    }
    class func FreeDeliveryAvailable() -> String {
        return OSLocalizedString("Free delivery", comment: "Free delivery")
    }
    
    class func EmptyFilterResult() -> String {
        return OSLocalizedString("No Product Found", comment: "No Product Found")
    }


    //Checkout//MapPin
    class func SelectAddress() -> String {
        return OSLocalizedString("Select Address", comment: "Select Address")
    }
    class func SelectAddressIconTitle() -> String {
        return OSLocalizedString("MapPin", comment: "MapPin")
    }
    class func SelectPaymentIconTitle() -> String {
        return OSLocalizedString("Wallet", comment: "Wallet")
    }
    class func FetchingCategories() -> String {
        return OSLocalizedString("Fetching Categories",comment: "Fetching Categories")
    }
    class func PlaceOrder() -> String {
        return OSLocalizedString("Placing Order",comment: "Placing Order")
    }
    class func DeliverThisOrder() -> String {
        return OSLocalizedString("Deliver This Order",comment: "Deliver This Order")
    }


    //Alerts
    class func Error() -> String {
        return OSLocalizedString("Error", comment: "Error")
    }
    
    class func Alert() -> String {
        return OSLocalizedString("Alert", comment: "Alert")
    }
    
    class func CheckCardData() ->String{
        return OSLocalizedString("Check Card Data", comment: "Check Card Data")
    }
    
    class func CheckCardNumber() ->String{
        return OSLocalizedString("Check Card Number", comment: "Check Card Number")
    }
    
    class func CheckCardExpiry() ->String{
        return OSLocalizedString("Check Card Expiry", comment: "Check Card Expiry")
    }
    
    class func CheckCardCVV()->String{
        OSLocalizedString("Check Card CVV", comment:"Check Card CVV")
    }
    
    class func Success() -> String {
        return OSLocalizedString("Success", comment: "Success")
    }

    class func NoCamera() -> String {
        return OSLocalizedString("No Camera", comment: "No Camera")
    }
    class func ChoosePhoto() -> String {
        return OSLocalizedString("Choose Photo", comment: "Choose Photo")
    }
    class func TakePhoto() -> String {
        return OSLocalizedString("Take Photo", comment: "Take Photo")
    }
    class func PickFromLibrary() -> String {
        return OSLocalizedString("Pick From Library", comment: "Pick From Library")
    }
    class func Remove() -> String {
        return  OSLocalizedString("Remove", comment: "Remove")
    }
    class func Cancel() -> String {
        return OSLocalizedString("Cancel", comment: "Cancel")
    }
    class func NoCameraMsg() -> String {
        return OSLocalizedString("Sorry, this device has no camera", comment: "Sorry, this device has no camera")
    }
    class func OK() -> String {
        return OSLocalizedString("OK", comment: "OK")
    }
    class func PhoneNumberAvailable() -> String {
        return OSLocalizedString("Please enter phone number", comment: "Please enter phone number")
    }
    class func NameAvailable() -> String {
        return OSLocalizedString("Please enter your name", comment: "Please enter your name")
    }
    class func PasswordAvailable() -> String {
        return OSLocalizedString("Please enter your password", comment: "Please enter your password")
    }
    class func EmailAvailable() -> String {
        return OSLocalizedString("Please enter your email", comment: "Please enter your email")
    }
    class func ValidEmail() -> String {
        return OSLocalizedString("Please enter valid email", comment: "Please enter valid email")
    }
    class func ValidPhoneNumber() -> String {
        return OSLocalizedString("Please enter valid phone number" , comment: "Please enter valid phone number")
    }
    class func EnterOtp() -> String {
        return OSLocalizedString("Please enter verification code", comment: "Please enter verification code")
    }
    class func SessionInvalid() -> String {
        return   OSLocalizedString("Your session has expired,\n please login again", comment: "Your session has expired,\n please login again")
    }
    class func Redeem() -> String {
        return   OSLocalizedString("Redeem", comment: "Redeem")
    }
    class func VoucherCode() -> String {
        return   OSLocalizedString("Voucher Code", comment: "Voucher Code")
    }
    class func VouchercodeMissing() -> String {
        return OSLocalizedString("Enter Your Vouchercode", comment: "Enter Your Vouchercode")
    }
    
    //Progress
    class func ValidateLocation() -> String {
        return OSLocalizedString("Validating Location", comment: "Validating Location")
    }
    class func Loading() -> String {
        return OSLocalizedString("Loading", comment: "Loading")
    }
    class func LoggingIn() -> String {
        return OSLocalizedString("Logging In", comment: "Logging In")
    }
    class func SigningUp() -> String {
        return OSLocalizedString("Signing Up", comment: "Signing Up")
    }
    class func GetCart() -> String {
        return OSLocalizedString("Fetching Cart", comment: "Fetching Cart")
    }
    class func UpdateCart() -> String {
        return OSLocalizedString("Updating Cart", comment: "Updating Cart")
    }
    class func Updating() -> String {
        return OSLocalizedString("Updating", comment: "Updating")
    }
    class func Canceling() -> String {
        return OSLocalizedString("Canceling", comment: "Canceling")
    }
    class func Delete() -> String {
        return OSLocalizedString("Deleting", comment: "Deleting")
    }
    class func DeleteCaps() -> String {
        return OSLocalizedString("DELETE", comment: "DELETE")
    }
    class func DeleteSmall() -> String {
        return OSLocalizedString("Delete", comment: "Delete")
    }

    //Titles
    class func ProfileTitle() -> String {
        return OSLocalizedString("Profile", comment: "Profile")
    }
    class func AddressListTitle() -> String {
        return OSLocalizedString("Saved Addresses", comment: "Saved Addresses")
    }
    class func InviteFriends() -> String {
        return OSLocalizedString("SHARE", comment: "SHARE")
    }
    class func ReferralsTitle() -> String {
        return OSLocalizedString("Referrals", comment: "Referrals")
    }
    class func PaymentOptions() -> String {
        return OSLocalizedString("Payment Options", comment: "Payment Options")
    }
    class func WishList() -> String {
        return OSLocalizedString("Wish List", comment: "Wish List")
    }
    
    class func ADDWISHLIST()->String{
        return OSLocalizedString("ADD WISH LIST", comment: "ADD WISH LIST")
    }
    
    class func Cards() -> String {
        return OSLocalizedString("Card", comment: "Card")
    }

    //ItemDetails
    class func FarAway() -> String {
        return OSLocalizedString("Distance", comment: "Distance")
    }
    class func Compare() -> String {
        return OSLocalizedString("Compare", comment: "Compare")
    }
    class func Unit() -> String {
        return OSLocalizedString("Unit", comment: "Unit")
    }
    class func Discription() -> String {
        return OSLocalizedString("Description", comment: "Description")
    }
    class func Effects() -> String {
        return OSLocalizedString("Effects", comment: "Effects")
    }

    class func Relaxed() -> String {
        return OSLocalizedString("Relaxed", comment: "Relaxed")
    }
    class func Uplifted() -> String {
        return OSLocalizedString("Uplifted", comment: "Uplifted")
    }
    class func Happy() -> String {
        return OSLocalizedString("Happy", comment: "Happy")
    }
    class func Focused() -> String {
        return OSLocalizedString("Focused", comment: "Focused")
    }
    class func Energetic() -> String {
        return OSLocalizedString("Energetic", comment: "Energetic")
    }

    //Dispensaries
    class func Dispensaries() -> String {
        return OSLocalizedString("Stores", comment: "Stores")
    }
    class func DeliveredBy() -> String {
        return OSLocalizedString("Delivered By Store", comment: "Delivered By Store")
    }
    class func DeliveryBy() -> String {
        return OSLocalizedString("Delivery by", comment: "Delivery by")
    }
    class func FreeDeliveryText() -> String {
        return OSLocalizedString("Free delivery above", comment: "Free delivery above")
    }

    //editProfile

    class func Verified() -> String {
        return OSLocalizedString("Verified", comment: "Verified")
    }

    //History
    class func TrackOrder() -> String {
        return OSLocalizedString("TRACK ORDER", comment: "TRACK ORDER")
    }
    
//    class func Cancel()->String{
//        return OSLocalizedString("Cancel", comment: "Cancel")
//    }
    
    class func Track() -> String {
        return OSLocalizedString("TRACK", comment: "TRACK")
    }
    class func HelpUpper() -> String {
        return OSLocalizedString("HELP", comment: "HELP")
    }
    class func ReOrder() -> String {
        return OSLocalizedString("REORDER", comment: "REORDER")
    }
    class func ActiveOrder() -> String {
        return OSLocalizedString("ACTIVE ORDER", comment: "ACTIVE ORDER")
    }
    
    class func ActiveOrderInSmall() -> String {
        return OSLocalizedString("Active Order", comment: "Active Order")
    }
    class func PastOrder() -> String {
        return OSLocalizedString("PAST ORDER", comment: "PAST ORDER")
    }

    //TrackingVCcomment
    class func  OrderConfirmed() -> String {
        return  OSLocalizedString("Order Confirmed", comment: "Order Confirmed")
    }
    class func  DriverAssigned() -> String {
        return  OSLocalizedString("Driver Assigned", comment: "Driver Assigned")
    }
    class func  DriverAtStore() -> String {
        return  OSLocalizedString("Driver at Store", comment: "Driver at Store")
    }
    class func  OrderPickedUp() -> String {
        return  OSLocalizedString("Order Picked Up", comment: "Order Picked Up")
    }
    class func  DriverLocation() -> String {
        return  OSLocalizedString("Driver at Delivery Location", comment: "Driver at Delivery Location")
    }
    class func  OrderDelivered() -> String {
        return  OSLocalizedString("Order Delivered", comment: "Order Delivered")
    }
    class func NewOrder() -> String {
        return OSLocalizedString("New Order", comment: "New Order")
    }
    class func AcceptedByManager() -> String {
        return OSLocalizedString("Accepted by Store", comment: "Accepted by Store")
    }
    class func RejectedByManager() -> String {
        return OSLocalizedString("Rejected By Store", comment: "Rejected By Store")
    }
    class func CancelledByManager() -> String {
        return OSLocalizedString("Cancelled By Store", comment: "Cancelled By Store")
    }
    class func OrderReady() -> String {
        return OSLocalizedString("Order Ready", comment: "Order Ready")
    }
    class func OrderPicked() -> String {
        return OSLocalizedString("Order Picked", comment: "Order Picked")
    }
    class func OrderCompleted() -> String {
        return OSLocalizedString("Order Completed", comment: "Order Completed")
    }
    class func DriverAccept() -> String {
        return OSLocalizedString("Driver Accepted", comment: "Driver Accepted")
    }
    class func DriverCancelled() -> String {
        return OSLocalizedString("Cancelled By Driver", comment: "Cancelled By Driver")
    }
    class func DriverOnTheWay() -> String {
        return OSLocalizedString("Driver On The Way", comment: "Driver On The Way")
    }
    class func DriverReached() -> String {
        return OSLocalizedString("Driver Reached", comment: "Driver Reached")
    }
    class func Delivered() -> String {
        return OSLocalizedString("Delivered", comment: "Delivered")
    }
    class func Cancelled() -> String {
        return OSLocalizedString("Cancelled", comment: "Cancelled")
    }
    class func YourOrderConfirmed() -> String {
        return  OSLocalizedString("Your order has been confirmed and will be delivered shortly.", comment: "Your order has been confirmed and will be delivered shortly.")
    }
    
    class func chatWithUs() -> String {
        return  OSLocalizedString("Have an issue? Chat with us", comment: "Have an issue? Chat with us")
    }
    
    class func waitTime() -> String {
        return  OSLocalizedString("Usual wait time is 2-3 minutes", comment: "Usual wait time is 2-3 minutes")
    }
    

    //OrderDetail
    class func ItemTotal() -> String {
        return OSLocalizedString("Sub Total", comment: "Sub Total")
    }
    class func Total() -> String {
        return OSLocalizedString("To Pay", comment: "To Pay")
    }
    class func Details() -> String {
        return OSLocalizedString("DETAILS", comment: "DETAILS")
    }
    class func GST() -> String {
        return OSLocalizedString("GST", comment: "GST")
    }

    //No internet Popup
    class func NoInternet() -> String {
        return OSLocalizedString("Seems like you are not connected to the internet.", comment: "Seems like you are not connected to the internet.")
    }

    //addNewCard
    class func CardUpper() -> String {
        return  OSLocalizedString("CARD", comment: "CARD")
    }
    class func Expires() -> String {
        return OSLocalizedString("EXPIRES", comment: "EXPIRES")
    }
    class func Expired() -> String {
        return OSLocalizedString("Expired", comment: "Expired")
    }
    class func CardNumber() -> String {
        return OSLocalizedString("CARD NUMBER", comment: "CARD NUMBER")
    }
    class func CVV() -> String {
        return OSLocalizedString("CVV", comment: "CVV")
    }


    //////for Ufly Changes
    class func NotAutherized() -> String {
        return OSLocalizedString("Please Setup Loopz Money", comment: "Please Setup Loopz Money")
    }
    class func UnableToLogin() -> String {
        return  OSLocalizedString("I am unable to log in on Loopz", comment: "I am unable to log in on Loopz")
    }
    class func TermsOfUseForLoopz() -> String {
        return OSLocalizedString("Terms of Use for Loopz ON-TIME / Assured", comment: "Terms of Use for Loopz ON-TIME / Assured")
    }
    class func CreateAnAcc() -> String {
        return OSLocalizedString("Create a new %@ account.", comment: "Create a new %@ account.")
    }
    class func CreateAcc() -> String {
        return OSLocalizedString("Enter your details to create new account", comment: "Enter your details to create new account")
    }
    class func PaidBy() -> String {
        return OSLocalizedString("Paid via", comment: "Paid via")
    }
    class func BitPay() -> String {
        return OSLocalizedString("BitPay", comment: "BitPay")
    }
 
    class func Wallet() -> String {
        return  Changebles.Wallet()
    }
    // **************************************************************************************************************************************************
    //not added localized file      //check terms and conditions in localized strings

    class func CreateNewList() -> String {
        return   OSLocalizedString("CREATE A NEW LIST", comment: "CREATE A NEW LIST")
    }
    class func EnterListName() -> String {
        return   OSLocalizedString("Please Enter The List Name", comment: "Please Enter The List Name")
    }
    class func Instructions() -> String {
        return   OSLocalizedString("Instructions", comment: "Instructions")
    }
    class func Favouritei() -> String {
        return   OSLocalizedString("Favorite", comment: "Favorite")
    }
    class func Favourited() -> String {
        return  OSLocalizedString("Favorited", comment: "Favorited")
    }
    class func ChangeQty() -> String {
        return   OSLocalizedString("Change Qty", comment: "Change Qty")
    }
    class func InCart() -> String {
        return  OSLocalizedString("In Cart", comment: "In Cart")
    }

    class func outOfStock() -> String {
        return  OSLocalizedString("Out of stock", comment: "Out of stock")
    }
    
    class func Edit() -> String {
        return  OSLocalizedString("Edit", comment: "Edit")
    }
    
    class func Change() -> String {
        return  OSLocalizedString("Change Password", comment: "Change Password")
    }
    class func ChangeLabel() -> String {
        return  OSLocalizedString("Change", comment: "Change")
    }
    class func UpdateEmailPh() -> String {
        return OSLocalizedString("Update your phone number and email address.", comment: "Update your phone number and email address.")
    }
    class func EmailAddress() -> String {
        return   OSLocalizedString("EMAIL ADDRESS", comment: "EMAIL ADDRESS")
    }
    class func OldPassword() -> String {
        return   OSLocalizedString("OLD PASSWORD", comment: "OLD PASSWORD")
    }
    class func NewPassword() -> String {
        return   OSLocalizedString("NEW PASSWORD", comment: "NEW PASSWORD")
    }
    class func ReEnterPassword() -> String {
        return   OSLocalizedString("REENTER PASSWORD", comment: "REENTER PASSWORD")
    }
    class func IdentityCard() -> String {
        return OSLocalizedString("Identity Cards", comment: "Identity Cards")
    }
    class func UpdateLower() -> String {
        return OSLocalizedString("Update", comment: "Update")
    }

    class func HelpAndSupport() -> String {
        return  OSLocalizedString("Help & Support", comment: "Help & Support")
    }
    class func HelpWithOrder() -> String {
        return OSLocalizedString("Help with this order", comment: "Help with this order")
    }
    class func HelpWithQueries() -> String {
        return OSLocalizedString("Help with other queries", comment: "Help with other queries")
    }
    class func IssueWithPrevious() -> String {
        return OSLocalizedString("Issue with previous orders", comment: "Issue with previous orders")
    }
    class func GeneralQueries() -> String {
        return OSLocalizedString("General Queries", comment: "General Queries")
    }
    class func Legal() -> String {
        return  OSLocalizedString("Legal", comment: "Legal")
    }

    class func GeneralQueriesUpper() -> String {
        return OSLocalizedString("General Queries", comment: "General Queries")
    }
    class func LegalUpper() -> String {
        return  OSLocalizedString("Legal", comment: "Legal")
    }

    // Help and Support
    //help with this order
    class func HaveNotReceived() -> String {
        return OSLocalizedString("I haven’t receved this order", comment: "I haven’t receved this order")
    }
    class func MissingOrder() -> String {
        return OSLocalizedString("Items are missing from my order", comment: "Items are missing from my order")
    }
    class func DifferentOrder() -> String {
        return OSLocalizedString("Items are different from what i orderd", comment: "Items are different from what i orderd")
    }
    class func SpillageOrder() -> String {
        return OSLocalizedString("I have packaging or spillage with this order", comment: "I have packaging or spillage with this order")
    }
    class func Received() -> String {
        return OSLocalizedString("I have received bad quality item", comment: "I have received bad quality item")
    }
    class func PaymentQuery() -> String {
        return OSLocalizedString("I have payment & refund related queries for this order", comment: "I have payment & refund related queries for this order")
    }
    //Quick Card

    class func InsufficientBalance() -> String {
        return OSLocalizedString("You have insufficient balance in wallet", comment: "You have insufficient balance in wallet")
    }

    //    // general queries
    class func PlaceOrderQuery() -> String {
        return  OSLocalizedString("I have query related to placing an order", comment: "I have query related to placing an order")
    }

    class func PaymentRelatedQ() -> String {
        return  OSLocalizedString("I have a payment or refund related query", comment: "I have a payment or refund related query")
    }
    class func CouponQuery() -> String {
        return  OSLocalizedString("I have a coupon related query", comment: "I have a coupon related query")
    }


    // legal

    class func TermsOfUse() -> String {
        return OSLocalizedString("Terms of Use", comment: "Terms of Use")
    }
    class func PrivacyPolicy() -> String {
        return OSLocalizedString("Privacy Policy", comment: "Privacy Policy")
    }
    class func Cancellations() -> String {
        return OSLocalizedString("Cancellations and Refunds", comment: "Cancellations and Refunds")
    }


    //Confirm Order
    class func PlaceTheOrder() -> String {
        return OSLocalizedString("PLACE THE ORDER", comment: "PLACE THE ORDER")
    }
    class func Schedule() -> String {
        return OSLocalizedString("Schedule", comment: "Schedule")
    }
    class func TipYourCourier() -> String {
              return OSLocalizedString("Tip Your Courier", comment: "Tip Your Courier")
          }
    class func OrderTimeTitle() -> String {
        return OSLocalizedString("When do you want to get your order?" , comment: "When do you want to get your order?")
    }
    
    class func Selectcollectionslot()->String{
        return OSLocalizedString("Please select collection slot.", comment: "Please select collection slot.")
    }
    
    
    class func CollectionAndDelivery()->String{
        return OSLocalizedString("Please select a Collection Slot and Delivery Slot", comment: "Please select a Collection Slot and Delivery Slot")
    }
    class func SelectAddresss()->String{
    return OSLocalizedString("Please select Pickup Address", comment: "Please select Pickup Address")
    }
    class func PaymentBreak() -> String {
        return OSLocalizedString("Payment Breakdown", comment: "Payment Breakdown")
    }
    class func YourPamentBreak() -> String {
        return OSLocalizedString("Your Payment Breakdown", comment: "Your Payment Breakdown")
    }
    class func DeliveryType() -> String {
        return OSLocalizedString("Order Type", comment: "Order Type")
    }
    class func SelectHow() -> String {
        return OSLocalizedString("How do you want to get your order?", comment: "How do you want to get your order?")
    }
    class func Pickup() -> String {
        return OSLocalizedString("Pickup", comment: "Pickup")
    }
    class func Delivery() -> String {
        return OSLocalizedString("Delivery", comment: "Delivery")
    }
    class func collectionandDeliveryLower() -> String {
        return OSLocalizedString("Collection & Delivery Slot", comment: "Collection & Delivery Slot")
    }
    class func laundryTypeLower() -> String {
        return OSLocalizedString("Laundry Type", comment: "Laundry Type")
    }
    class func DeliveryLocationLower() -> String {
        return OSLocalizedString("Delivery Location", comment: "Delivery Location")
    }
    class func PaymentMethodLower() -> String {
        return OSLocalizedString("Payment Method", comment: "Payment Method")
    }
    class func PickupLocationLower() -> String {
        return OSLocalizedString("Pickup Location", comment: "Pickup Location")
    }
    class func Discount() -> String {
        return OSLocalizedString("Discount", comment: "Discount")
    }
    class func PickupCharges() -> String {
        return OSLocalizedString("  - Pickup Charges", comment: "  - Pickup Charges")
    }
    class func DeliveryCharges() -> String {
        return OSLocalizedString("  - Delivery Charges", comment: "  - Delivery Charges")
    }
    class func ExpressFee() -> String {
        return OSLocalizedString("Express Fee", comment: "Express Fee")
    }
    class func Promocode() -> String {
        return  OSLocalizedString("Promocode", comment: "Promocode")
    }
    class func ConfirmOrder() -> String {
        return OSLocalizedString("Confirm Order", comment: "Confirm Order")
    }

    class func SelectDate() -> String {
        return OSLocalizedString("Select Date", comment: "Select Date")
    }
    class func SelectTime() -> String {
        return OSLocalizedString("Select Time", comment: "Select Time")
    }
    class func ChangeType() -> String {
        return OSLocalizedString("Change", comment: "Change")
    }

    //payment methods
    class func CashLower() -> String {
        return OSLocalizedString("Cash", comment: "Cash")
    }
    class func QuickCard() -> String {
        return OSLocalizedString("Quick Card", comment: "Quick Card")
    }
    class func Money() -> String {
        return OSLocalizedString("Money", comment: "Money")
    }
    class func CardLower() -> String {
        return OSLocalizedString("Card", comment: "Card")
    }
    class func Connect() -> String {
        return OSLocalizedString("Connect", comment: "Connect")
    }
    class func Amount() -> String {
        return  OSLocalizedString("Amount", comment: "Amount")
    }
    class func OrderId() -> String {
        return OSLocalizedString("Order Id:", comment: "Order Id:")
    }

    class func KeepChange() -> String {
        return OSLocalizedString("Please keep exact change handy to help us serve you better.", comment: "Please keep exact change handy to help us serve you better.")
    }

    class func AddMoney() -> String {
        return OSLocalizedString("Add Money", comment: "Add Money")
    }
    class func CurrentLocation() -> String {
        return OSLocalizedString("Current Location", comment: "Current Location")
    }

    //search


    //Promocode
    class func HaveVoucher()->String{
       return OSLocalizedString("Have a Voucher code ? ", comment: "Have a Voucher code ? ")
    }
    class func PromocodeMissing() -> String {
        return OSLocalizedString("Enter Your Promocode", comment: "Enter Your Promocode")
    }
    class func Apply() -> String {
        return OSLocalizedString("Apply", comment: "Apply")
    }
    class func AddressLower() -> String {
        return OSLocalizedString("Address", comment:"Address")
    }
    class func Products() -> String {
        return OSLocalizedString("PRODUCTS", comment: "PRODUCTS")
    }
    class func Suggestions() -> String {
        return OSLocalizedString("Suggestions", comment: "Suggestions")
    }
    class func RelatedTo() -> String {
        return OSLocalizedString("Related to", comment: "Related to")
    }
    class func RecentSearches() -> String {
        return OSLocalizedString("Popular Searches", comment: "Popular Searches")
    }
    class func NoMatchFound() -> String {
        return OSLocalizedString("No match found for", comment: "No match found for")
    }
    class func ETA() -> String {
        return OSLocalizedString("ETA", comment: "ETA")
    }

    //addAddress
    class func AddressAlreadySaved() -> String {
        return OSLocalizedString("This address is already saved", comment: "This address is already saved")
    }

    class func ApplyCoupon() -> String {
        return OSLocalizedString("Apply Coupon", comment: "Apply Coupon")
    }
    class func Coupon() -> String {
        return  OSLocalizedString("Coupons", comment: "Coupons")
    }


    //Cancel Cart
    class func RemoveCartWarning() -> String {
        return OSLocalizedString("On changing your delivery location, You will lose your cart", comment: "On changing your delivery location, You will lose your cart")
    }
    class func Message() -> String {
        return OSLocalizedString("Message", comment: "Message")
    }

    //orderDeatil
    class func NoDataAvailable() -> String {
        return OSLocalizedString("No Data Available", comment: "No Data Available")
    }
    class func Free() -> String {
        return OSLocalizedString("Free", comment: "Free")
    }
    class func FinalAmount() -> String {
        return OSLocalizedString("Final amount to pay", comment: "Final amount to pay")
    }
    class func PlacedOn() -> String {
        return OSLocalizedString("Placed on", comment: "Placed on")
    }
    class func FinalPaid() -> String {
        return OSLocalizedString("Final paid amount", comment: "Final paid amount")
    }
    class func ASAP() -> String {
        return OSLocalizedString("ASAP", comment: "ASAP")
    }
    class func Scheduled() -> String {
        return OSLocalizedString("Scheduled On", comment: "Scheduled On")
    }
    class func ValidThru() -> String {
        return OSLocalizedString("Valid Till: ", comment: "Valid Till: ")
    }
    class func MakeAsDefault() -> String {
        return OSLocalizedString("Make As Default", comment: "Make As Default")
    }
    class func WooHoo() -> String {
        return OSLocalizedString("WooHoo", comment: "WooHoo")
    }
    class func AppliedPromocode() -> String {
        return OSLocalizedString("Promocode %@ has been applied to your cart", comment: "Promocode %@ has been applied to your cart")
    }
    class func RemovePromocode() -> String {
        return OSLocalizedString("Are you sure to remove promocode?", comment: "Are you sure to remove promocode?")
    }
    class func Save() -> String {
        return OSLocalizedString("Save", comment: "Save")
    }
    class func SomethingWentWrong() -> String {
        return OSLocalizedString("Something Went Wrong\nPlease try after sometime.", comment: "Something Went Wrong\nPlease try after sometime.")
    }

    //paymentOtpions
    class func CreditAndDebit() -> String {
        return  OSLocalizedString("Credit/Debit Cards", comment: "Credit/Debit Cards")
    }
    class func StartShopping() -> String {
        return  OSLocalizedString("Start Shopping", comment: "Start Shopping")
    }
    class func StartOrdering() -> String {
        return  OSLocalizedString("Start Ordering", comment: "Start Ordering")
    }
    class func FoodCards() -> String {
        return OSLocalizedString("Food Cards", comment: "Food Cards")
    }
    class func LinkAccount() -> String {
        return  OSLocalizedString("Link Account", comment: "Link Account")
    }
    class func PayOnDelivery() -> String {
        return OSLocalizedString("Pay On Delivery", comment: "Pay On Delivery")
    }
    class func Pay() -> String {
        return  OSLocalizedString("Pay", comment: "Pay")
    }
    class func AddNewCardText() -> String {
        return OSLocalizedString("Now pay your order using Ticket Restaurants Meal Card. Add your card details\n", comment: "Now pay your order using Ticket Restaurants Meal Card. Add your card details\n")
    }

    class func ConnectToQC() -> String {
        return OSLocalizedString("Connect Your QuickCard To Experience Seamless Payments", comment: "Connect Your QuickCard To Experience Seamless Payments")
    }

    class func EmptyFav() -> String {
        return OSLocalizedString("No Favourite items", comment: "No Favourite items")
    }
    class func EmptyWish() -> String {
        return OSLocalizedString("No WishList", comment: "No WishList")
    }
    class func EmptyWishItems() -> String {
        return OSLocalizedString("No WishList Items", comment: "No WishList Items")
    }

    class func AllowLocationHead() -> String {
        return OSLocalizedString("Allow \"%@\" to access your location while you use the app?", comment: "Allow \"%@\" to access your location while you use the app?")
    }
    class func AllowLocationBody() -> String {
        return OSLocalizedString("This application would like to  use location to find the stores around you", comment: "This application would like to  use location to find the stores around you")
    }
    class func SureLogout() -> String {
        return OSLocalizedString("Are you sure you want to logout?", comment: "Are you sure you want to logout?")
    }
    class func AddCard() -> String {
        return OSLocalizedString("ADD", comment: "ADD")
    }
    class func SubTotal() -> String {
        return OSLocalizedString("Sub Total", comment: "Sub Total")
    }
    class func total() -> String {
        return OSLocalizedString("Total", comment: "Total")
    }
    class func EmptyActive() -> String {
        return OSLocalizedString("No Active Orders", comment: "No Active Orders")
    }
    
    class func NotInLocality() -> String {
        return OSLocalizedString("Sorry, we don't deliver to this locality", comment: "Sorry, we don't deliver to this locality")
    }
    
    class func EmptyPast() -> String {
        return OSLocalizedString("No Past Orders", comment: "No Past Orders")
    }
    class func EmptySearchStore() -> String {
        return OSLocalizedString("No Store Found", comment: "No Store Found")
    }
    class func EmptySearchProduct() -> String {
        return OSLocalizedString("We do not have the product you requested", comment: "We do not have the product you requested")
    }
    
    class func EmptyStore() -> String {
        return OSLocalizedString("There are no stores available under this category", comment: "There are no stores available under this category")
    }
    
    class func FindInStore() -> String {
        return OSLocalizedString("Find this products in following stores", comment: "Find this products in following stores")
    }
    class func EmptySearchRecent() -> String {
        return OSLocalizedString("No Recent Search Found", comment: "No Recent Search Found")
    }
    
    class func Found() -> String{
        return OSLocalizedString("Found", comment: "Found")
    }
    class func NotVarified() -> String {
        return OSLocalizedString("Your profile is undergoing verification", comment: "Your profile is undergoing verification")
    }
    class func NotComplete() -> String {
        return OSLocalizedString("Your profile is incomplete", comment: "Your profile is incomplete")
    }
    class func ToPay() -> String {
        return OSLocalizedString("To Pay", comment: "To Pay")
    }
    class func FAQs() -> String {
        return OSLocalizedString("FAQs", comment: "FAQs")
    }
    class func CancelOrderMsg() -> String {
        return OSLocalizedString("Are you sure you want to cancel the order?", comment: "Are you sure you want to cancel the order?")
    }
    class func Yes() -> String {
        return OSLocalizedString("Yes", comment: "Yes")
    }
    class func No() -> String {
        return OSLocalizedString("No", comment: "No")
    }
    class func CancelOrder() -> String {
        return OSLocalizedString("Cancel order", comment: "Cancel order")
    }
    class func ScanCard() -> String {
        return OSLocalizedString("Scan Card", comment: "Scan Card")
    }
    class func EnableCurrentLocation() -> String {
        return OSLocalizedString("Enable location services", comment: "Enable location services")
    }
    class func ShareProductText() -> String {
        return OSLocalizedString("Checkout this product from", comment: "Checkout this product from")
    }
    class func PlacedSuccess() -> String {
        return OSLocalizedString("Yay! You have placed your order successfully", comment: "Yay! You have placed your order successfully")
    }
    class func Confirm() -> String {
        return OSLocalizedString("Confirm", comment: "Confirm")
    }
    class func TXNID() -> String {
        return OSLocalizedString("TXN ID", comment: "TXN ID")
    }
    class func AddressCopied() -> String {
        return OSLocalizedString("Transaction address has been copied", comment: "Transaction address has been copied")
    }
    class func Copy() -> String {
        return OSLocalizedString("Copy", comment: "Copy")
    }
    class func ProcessingPayment() -> String {
        return OSLocalizedString("Processing Payment", comment: "Processing Payment")
    }
    class func Submit() -> String {
        return OSLocalizedString("Submit", comment: "Submit")
    }
    class func YourLastOrder() -> String {
        return OSLocalizedString("Your Last Order", comment: "Your Last Order")
    }
    class func Chat() -> String {
        return OSLocalizedString("CHAT", comment: "CHAT")
    }
    class func Tax() -> String {
        return OSLocalizedString("Tax", comment: "Tax")
    }
    class func Taxes() -> String {
        return OSLocalizedString("Taxes", comment: "Taxes")
    }
    class func Filter() -> String {
        return OSLocalizedString("Filter", comment: "Filter")
    }
    class func Sort() -> String {
        return OSLocalizedString("Sort", comment: "Sort")
    }
    class func Desclimer() -> String {
        return OSLocalizedString("Disclaimer", comment: "Disclaimer")
    }
    class func DesclimerData() -> String {
        return OSLocalizedString("Product information added or displayed may not be current or complete, always refer the physical package for the most accurate information and warnings, for more information contact the actual manufacturer or dealers. *Some items weights, like produce are measured by shoppers using in- aisle scales. the name of the items from deli, meat and seafood counters are determined by label applied in-store to that item", comment: "Product information added or displayed may not be current or complete, always refer the physical package for the most accurate information and warnings, for more information contact the actual manufacturer or dealers. *Some items weights, like produce are measured by shoppers using in- aisle scales. the name of the items from deli, meat and seafood counters are determined by label applied in-store to that item")
    }


    class func Send() ->String{
        return OSLocalizedString("Send", comment: "Send")
    }
    class func Brand() -> String {
        return OSLocalizedString("Brand", comment: "Brand")
    }
    class func Size() -> String {
        return OSLocalizedString("Size", comment: "Size")
    }
    class func Color() -> String {
        return OSLocalizedString("Color", comment: "Color")
    }
    class func Ingredients() -> String {
        return OSLocalizedString("Ingredients", comment: "Ingredients")
    }
    class func Manufacturer() -> String {
        return OSLocalizedString("Manufacturer", comment: "Manufacturer")
    }
    class func Clear() -> String {
        return OSLocalizedString("Clear", comment: "Clear")
    }
    class func Improve() -> String {
        return OSLocalizedString("WHAT CAN WE IMPROVE?", comment: "WHAT CAN WE IMPROVE?")
    }
    class func MinimumOrder() -> String {
        return OSLocalizedString("away from minimum order", comment: "away from minimum order")
    }
    class func Newest() -> String {
        return OSLocalizedString("Newest", comment: "Newest")
    }
    class func Popular() -> String {
        return OSLocalizedString("Popular", comment: "Popular")
    }
    class func Lowtohigh() -> String {
        return OSLocalizedString("Low to high", comment: "Low to high")
    }
    class func Hightolow() -> String {
        return OSLocalizedString("High to low", comment: "High to low")
    }
    class func MinOrderCheck() -> String {
        return OSLocalizedString("Please add more product for %@ to reach minimum order", comment: "Please add more product for %@ to reach minimum order")
    }
    class func ResultFilter() -> String {
        return OSLocalizedString("Filter Result", comment: "Filter Result")
    }
    class func AvailableCoupons() -> String {
        return OSLocalizedString("Available Coupons", comment: "Available Coupons")
    }
    class func AvailableOffers() -> String {
        return OSLocalizedString("Available Offers", comment: "Available Offers")
    }
    class func Nocouponsavailable() -> String {
        return OSLocalizedString("No coupons available", comment: "No coupons available")
    }
    class func moreDetails() -> String {
        return OSLocalizedString("More details", comment: "More details")
    }
    class func okGotIt() -> String {
        return OSLocalizedString("OK, GOT IT", comment: "OK, GOT IT")
    }
    class func howItWorks() -> String {
        return OSLocalizedString("HOW IT WORKS?", comment: "HOW IT WORKS?")
    }
    class func howToWork() -> String {
        return OSLocalizedString("To use this promocode, simply follow the below steps:\n1. Launch the \(Utility.AppName) App on your mobile and place your order.\n2. Mention the Promocode during checkout\n3. Enjoy a discount on the total cart value.", comment: "To use this promocode, simply follow the below steps:\n1. Launch the \(Utility.AppName) App on your mobile and place your order.\n2. Mention the Promocode during checkout\n3. Enjoy a discount on the total cart value.")
    }
    class func topBrands() -> String {
        return OSLocalizedString("Top brands", comment: "Top brands")
    }
    class func leaveTip() -> String {
        return OSLocalizedString("LEAVE TIP FOR DRIVER", comment: "LEAVE TIP FOR DRIVER")
    }
    class func rateExperience() -> String {
        return OSLocalizedString("RATE YOUR EXPERIENCE", comment: "RATE YOUR EXPERIENCE")
    }
    class func inviteCode() -> String {
        return OSLocalizedString("Get people to signup on the %@ App with your refferal code and earn discounts.",comment: "Get people to signup on the %@ App with your refferal code and earn discounts.")
    }
    
    class func solved()-> String{
        return OSLocalizedString("solved", comment: "solved")
        
    }
    
    class func TypeMessage() -> String {
        return OSLocalizedString("Type message here..", comment: "Type message here..")
    }
    class func AddCommendToTicket() -> String {
        return OSLocalizedString("please add the comment to this ticket", comment: "please add the comment to this ticket")
    }
    class func AddSubjectToTicket() -> String {
        return OSLocalizedString("please add the subject to this ticket", comment: "please add the subject to this ticket")
    }
    
    class func low()->String{
        return OSLocalizedString("low", comment: "low")
    }
    class func normal() ->String{
        return OSLocalizedString("normal", comment: "normal")
    }
    class func high() ->String{
        return OSLocalizedString("high", comment: "high")
    }
    class func urgent()->String{
        return OSLocalizedString("urgent", comment: "urgent")
    }
    class func open()-> String{
        return OSLocalizedString("open", comment: "open")
    }
    class func problem() -> String{
        OSLocalizedString("problem", comment: "problem");
    }
    
    class func AddPriorityToTicket() -> String {
        return OSLocalizedString("please add the priority to this ticket", comment: "please add the priority to this ticket")
    }
    class func TicketSuccessfull() -> String {
        return OSLocalizedString("you request for new ticket is successfull and ticket will be created soon", comment: "you request for new ticket is successfull and ticket will be created soon")
    }
    class func LogginginwithTouchID() -> String {
        return OSLocalizedString("Logging in with TouchID", comment: "Logging in with TouchID")
        }
    class func History() -> String {
        return OSLocalizedString("History", comment: "History")
    }
    class func Search() -> String {
        return OSLocalizedString("Search", comment: "Search")
    }
    class func SearchAddress() -> String {
        return OSLocalizedString("Search address", comment: "Search address")
    }
    class func NoCardsAvailable() -> String {
        return OSLocalizedString("No Cards Available", comment: "No Cards Available")
    }
    
    class func Credits() -> String {
        return OSLocalizedString("Credits", comment: "Credits")
    }
    class func Debits() -> String {
        return OSLocalizedString("Debits", comment: "Debits")
    }
    class func NoDebits() -> String {
        return OSLocalizedString("No Debits", comment: "No Debits")
    }
    class func NoCredits() -> String {
        return OSLocalizedString("No Credits", comment: "No Credits")
    }
    
    class func Cousines() -> String {
        return OSLocalizedString("Cuisines", comment: "Cuisines")
    }
    class func FoodType() -> String {
        return OSLocalizedString("Food Type", comment: "Food Type")
    }
    class func CostForTwo() -> String {
        return OSLocalizedString("Cost For Two", comment: "Cost For Two")
    }
    
    class func ForTwo() -> String {
        return OSLocalizedString("For Two", comment: "For Two")
    }
    
    class func Addons() -> String {
        return OSLocalizedString("Addons", comment: "Addons")
    }
    class func MaxLimit() -> String {
        return OSLocalizedString("Maximum Limit Reache for ", comment: "Maximum Limit Reache for ")
    }
    
    class func ADDMoney()->String{
        return OSLocalizedString("ADD MONEY", comment: "ADD MONEY")
    }
    
    class func CurrentBalance() -> String {
        return OSLocalizedString("Current Balance", comment: "Current Balance")
    }
    class func RecentTransactions() -> String {
        return OSLocalizedString("Recent Transactions", comment: "Recent Transactions")
    }
    class func HardLimit() -> String {
        return OSLocalizedString("Hard Limit", comment: "Hard Limit")
    }
    class func SoftLimit() -> String {
        return OSLocalizedString("Soft Limit", comment: "Soft Limit")
    }
    class func HardText() -> String {
        return OSLocalizedString("Cash limit hit and no more cash booking is allowed.", comment: "Cash limit hit and no more cash booking is allowed.")
    }
    class func SoftText() -> String {
        return OSLocalizedString("Maximum allowed cash limit is about to reached.", comment: "Maximum allowed cash limit is about to reached.")
    }
    class func PayUsingCard() -> String {
        return  OSLocalizedString("Pay Using Card", comment: "Pay Using Card")
    }
    class func WalletConfirmation() -> String {
        return OSLocalizedString("Are you sure you want to recharge your wallet with ", comment: "Are you sure you want to recharge your wallet with ")
    }
    class func QuestionMark() -> String {
        return OSLocalizedString("?", comment: "?")
    }
    class func WalletRecharged() -> String {
        return OSLocalizedString("Wohooo... You have successfully recharged your wallet with", comment: "Wohooo... You have successfully recharged your wallet with ")
    }
    class func Pleasementiontheamount() -> String {
        return OSLocalizedString("Please mention the amount", comment: "Please mention the amount")
    }
    class func Balance() -> String {
        return OSLocalizedString(" balance.!", comment: " balance.!")
    }
    class func RepeatLast() -> String {
        return OSLocalizedString("Repeat Last", comment: "Repeat Last")
    }
    class func IllChoose() -> String {
        return OSLocalizedString("I'll Choose", comment: "I'll Choose")
    }
    class func Close() -> String {
        return OSLocalizedString("Close", comment: "Close")
    }
    
    class func Closed() -> String {
        return OSLocalizedString("Closed", comment: "Closed")
    }
    
    class func Opens() -> String{
        return OSLocalizedString("Opens", comment: "Opens")
    }
    class func CollectionSlot() -> String{
        return OSLocalizedString("Collection Slot", comment: "Collection Slot")
    }
    class func DeliverySlot() -> String{
        return OSLocalizedString("Delivery Slot", comment: "Delivery Slot")
    }
    class func Today() -> String{
        return OSLocalizedString("Today", comment: "Today")
    }
    
    class func Tomorrow() -> String{
        return OSLocalizedString("Tomorrow", comment: "Tomorrow")
    }
    
    class func At() -> String{
        return OSLocalizedString("at", comment: "at")
    }
    
    class func Repeatcustomization() -> String {
        return OSLocalizedString("Repeat last used customization?", comment: "Repeat last used customization?")
    }
    
    class func customisation() -> String {
        return OSLocalizedString("CUSTOMIZE", comment: "CUSTOMIZE")
    }
    class func andAbove() -> String {
        return OSLocalizedString("and above", comment: "and above")
    }
    
    class func AwayFromStore() -> String {
        return OSLocalizedString("Away From Store", comment: "Away From Store")
    }
    class func AwayFromDelivery() -> String {
        return OSLocalizedString("Away From Delivery", comment: "Away From Delivery")
    }
    class func Away() -> String {
        return OSLocalizedString("Away", comment: "Away")
    }
    class func Ticket() -> String {
        return OSLocalizedString("Ticket", comment: "Ticket")
    }
    class func SelectCountry() -> String {
        return OSLocalizedString("Select Country", comment: "Select Country")
    }
    class func AlmostReached() -> String {
        return OSLocalizedString("Almost Reached", comment: "Almost Reached")
    }
    class func Restaurants() -> String {
        return OSLocalizedString("Restaurants", comment: "Restaurants")
    }
    class func Dishes() -> String {
        return OSLocalizedString("Dishes", comment: "Dishes")
    }
    
    
    // AddOns
    
    class func chooseAddonSingleOption() -> String {
        return OSLocalizedString("", comment: "")
    }
    
    class func chooseAddonMultipleOption() -> String {
        return OSLocalizedString("", comment: "")
    }
    
    class func Customizable() -> String {
        return OSLocalizedString("Customizable", comment: "Customizable")
    }
    
    class func maximumLimitMessage() -> String {
        return OSLocalizedString("You can select a maximum of", comment: "You can select a maximum of")
    }
    
    class func optionsOnly() -> String {
        return OSLocalizedString("optionsOnly", comment: "optionsOnly")
    }
    
    class func addonsMinLimitMessage() -> String {
        return OSLocalizedString("Select a minimum of", comment: "Select a minimum of")
    }
    
    class func addonsMaxLimitMessage() -> String {
        return OSLocalizedString("and a maximum of", comment: "and a maximum of")
    }
    class func addOnsOptions() -> String {
        return OSLocalizedString("options", comment: "options")
    }
    
    class func chooseOneOptionOnly() -> String {
        return OSLocalizedString("You can only choose 1 option", comment: "You can only choose 1 option")
    }
    
    class func chooseUpTo() -> String {
        return OSLocalizedString("You can choose up to", comment: "You can choose up to")
    }
    
    
    

    
    
    
    // Multiple cart
    
    class func itemsInCart() -> String {
        return OSLocalizedString("Items already in cart", comment: "Items already in cart")
    }
    
    class func clearCartMessage() -> String {
        return OSLocalizedString("Your cart contains items from a different store. Would you like to reset your cart before browsing this store?", comment: "Your cart contains items from a different store. Would you like to reset your cart before browsing this store?")
    }
    
    
    
    class func noStoresFound() -> String {
        return OSLocalizedString("No Stores Found", comment: "No Stores Found")
    }
    

    //Tracking Status
    class func CLOSED()->String{
        return OSLocalizedString("CLOSED", comment: "CLOSED")
    }
    class func OPEN()->String{
        return OSLocalizedString("OPEN", comment: "OPEN")
    }
    class func STATUS()->String{
       return OSLocalizedString("STATUS", comment: "STATUS")
    }
    
    
    class func OrderPlaced() -> String {
        return OSLocalizedString("Order Placed", comment: "Order Placed")
    }
    class func AcceptedbyStore() -> String {
        return OSLocalizedString("Accepted by Store", comment: "Accepted by Store")
    }
    class func OrderAcceptedByStore() -> String {
        return OSLocalizedString("Your order has been accepeted by Store. Waiting to assign the driver for your delivery..", comment: "Your order has been accepeted by Store. Waiting to assign the driver for your delivery..")
    }
    class func AcceptedByDriver() -> String {
        return OSLocalizedString("Driver assigned for your booking.", comment: "Driver assigned for your booking.")
    }
    
    class func AcceptedByDriverMessage() -> String {
        return OSLocalizedString("Driver assigned for your booking.", comment: "Driver assigned for your booking.")
    }
    
    class func EnrouteStore() -> String {
        return OSLocalizedString("On the way to Store", comment: "On the way to Store")
    }
    class func EnrouteStoreMessage() -> String {
        return OSLocalizedString("Driver is on the way to store...", comment: "Driver is on the way to store...")
    }
    class func DriverReachedStore() -> String {
        return OSLocalizedString("Driver reached at store", comment: "Driver reached at store")
    }
    class func DriverReachedStoreMessage() -> String {
        return OSLocalizedString("Driver has reached at store picking the order.. ", comment: "Driver has reached at store picking the order.. ")
    }

    class func WayToYourLocation() -> String {
        return OSLocalizedString("On the way to your location", comment: "On the way to your location")
    }
    
    class func WayToYourLocationMessage() -> String {
        return OSLocalizedString("Driver is on the way to your location with order..", comment: "Driver is on the way to your location with order..")
    }
    
    class func DriverReachedAtLocation() -> String {
        return OSLocalizedString("Driver reached at delivery location", comment: "Driver reached at delivery location")
    }
    
    class func DriverReachedLocationMessage() -> String {
        return OSLocalizedString("Driver reached to delivery location. Please receive the delivery..", comment: "Driver reached to delivery location. Please receive the delivery..")
    }
    
    class func DeliveredSuccessfully() -> String {
        return OSLocalizedString("Delivered Successfully", comment: "Delivered Successfully")
    }
    
    class func DeliveredSuccessfullyMessage() -> String {
        return OSLocalizedString("Your order has been delivered successfully..", comment: "Your order has been delivered successfully..")
    }
    class func DeliveryCompleted() -> String {
        return OSLocalizedString("Order Completed", comment: "Order Completed")
    }
    class func DeliveryCompletedMessage() -> String {
        return OSLocalizedString("Order is completed..review and rate us your feedback is valuable..", comment: "Order is completed..review and rate us your feedback is valuable..")
    }
    
    class func addExtraNotes() -> String {
        return OSLocalizedString("Add additional notes....", comment: "Add additional notes....")
    }
    
    
    
}

public func OSLocalizedString(_ key: String, comment: String) -> String {
    return OneSkyOTAPlugin.localizedString(forKey: key, value: "", table: nil)
}
