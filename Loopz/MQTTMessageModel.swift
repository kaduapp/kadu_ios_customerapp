//
//  MQTTMessageModel.swift
//  UFly
//
//  Created by 3Embed on 11/12/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class MQTTMessageModel: NSObject {
    
    var Message             = ""
    var Status              = 0
    var Reason              = ""
    var Id:Int              = 0
    var Latitude:Float      = 0
    var Longitude:Float     = 0
    var Action:Int          = 0
    var DriverName          = ""
    var DriverId            = ""
    var DriverImage         = ""
    var DriverEmail         = ""
    var DriverMobile        = "+91900355000"
    
    
    
    
    init(data:[String:Any]) {
        if let dataIn = data["status"] as? Int {
            Status = dataIn
            if dataIn == 19 || dataIn == 21 {
                Status = 8
            }
        }
        if let dataIn = data["status"] as? String {
            Status = Int(dataIn)!
            if dataIn == "19" || dataIn == "21" {
                Status = 8
            }
        }
        
        if let dataIn = data["bid"] as? Int {
            Id = dataIn
        }
        if let dataIn = data["bid"] as? String {
            Id = Int(dataIn)!
        }
        
        if let dataIn = data["longitude"] as? Float {
            Longitude = dataIn
        }
        
        if let dataIn = data["latitude"] as? Float {
            Latitude = dataIn
        }
        
        if let dataIn = data["longitude"] as? Double {
            Longitude = Float(dataIn)
        }
        
        if let dataIn = data["latitude"] as? Double {
            Latitude = Float(dataIn)
        }
        
        if let dataIn = data["action"] as? Int {
            Action = dataIn
        }
        if let titleTemp = data[APIResponceParams.StatusMessage] as? String{
            Message     = titleTemp
        }
    }
    
    func updateDriver(data:[String:Any]) {
        if let titleTemp = data[APIResponceParams.DriverName] as? String{
            DriverName             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DriverID] as? String{
            DriverId             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DriverID] as? Int{
            DriverId             = "\(titleTemp)"
        }
        if let titleTemp = data[APIResponceParams.DriverImage] as? String{
            DriverImage             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DriverEmail] as? String{
            DriverEmail             = titleTemp
        }
        if let titleTemp = data[APIResponceParams.DriverMobile] as? String{
            DriverMobile             = titleTemp
        }
        if let dataIn = data["latitude"] as? Float {
            Latitude = dataIn
        }
        if let dataIn = data["longitude"] as? Float {
            Longitude = dataIn
        }
    }
}
