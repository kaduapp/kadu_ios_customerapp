//
//  InviteVM.swift
//  DelivX
//
//  Created by 3 Embed on 09/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class InviteVM: NSObject {
    
    let invite_response = PublishSubject<Bool>()
    let disposeBag = DisposeBag()
    func getReferralCode(){
        
        AuthenticationAPICalls.getReferralCode().subscribe(onNext: { success in
            if success == true{
                self.invite_response.onNext(true)
            }
        }).disposed(by: self.disposeBag)
    }
}
