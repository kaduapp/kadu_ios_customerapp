//
//  MenuVC.swift
//  UFly
//
//  Created by 3 Embed on 20/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
protocol MenuVCDelegate{
    func didSelectSubCat(data: Any)
}

class MenuVC: UIViewController {
    //Outlets
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var tableViewHight: NSLayoutConstraint!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    
    //Variables
    var delegate:MenuVCDelegate? = nil
    var controllerSubCategoryArray:[Any] = []
    var selectedId = ""
    var bottomConstraint = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTableHight()
    }
    
   
    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension MenuVC {
    
    /// initial view setup
    func initialSetup(){
        Helper.setShadow(sender: mainTableView)
        mainTableView.layer.cornerRadius = 12
        setTableHight()
    }
    
    /// sets Table Hight as per number of cell
    func setTableHight(){
        UIView.animate(withDuration: 1) {
            self.tableViewHight.constant = CGFloat(self.controllerSubCategoryArray.count * 44)
            self.tableBottomConstraint.constant = CGFloat(self.bottomConstraint)
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
}







