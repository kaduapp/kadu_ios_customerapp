//
//  AllWishListTVE.swift
//  DelivX
//
//  Created by 3Embed on 04/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
// MARK: - Tableview Delegate,DataSource
extension AllWishListVC : UITableViewDelegate,UITableViewDataSource {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if allWishListVM.arrayOfWishList.count == 0 {
            tableView.backgroundView = emptyView
        }else{
            tableView.backgroundView = nil
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if addSelected{
                return 1
            }
            return 0
        }
       
        return allWishListVM.arrayOfWishList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CreateNewListCell.self), for: indexPath) as! CreateNewListCell
            cell.textField.becomeFirstResponder()
            cell.textField.text = ""
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.AllWishListCell, for: indexPath) as! AllWishListCell
        cell.setData(data: allWishListVM.arrayOfWishList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section != 0 && allWishListVM.arrayOfWishList.count > 0{
        tableView.deselectRow(at: indexPath, animated: true)
       allWishListVM.selected = allWishListVM.arrayOfWishList[indexPath.row]
        self.performSegue(withIdentifier: UIConstants.SegueIds.AllWishListToWishListDetails, sender: self)
      }
    }
        

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0{
          return false
      }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.section != 0 && allWishListVM.arrayOfWishList.count > 0{
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            allWishListVM.deleteFavList(index: indexPath.row)
        }
      }
    }
    
}
