//
//  LanguageModel.swift
//  DelivX
//
//  Created by 3 Embed on 23/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

struct Language {
    
    var Name = ""
    var Code = ""
    init(_ data:[String:Any]) {
        if let dataFrom = data["langCode"] as? String {
            Code = dataFrom
        }
        if let dataFrom = data["lan_name"] as? String {
            Name = dataFrom
        }
    }
}
