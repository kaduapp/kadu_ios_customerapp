//
//  SelectLocationVC.swift
//  UFly
//
//  Created by 3Embed on 28/10/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SelectLocationVC: UIViewController,UIViewControllerTransitioningDelegate {
    
    var emptyView = EmptyView().shared
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var buttonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var noContentView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var emptyScreenImage: UIImageView!
    @IBOutlet weak var emptyScreenLabel: UILabel!
    @IBOutlet weak var addressSearchBar: UISearchBar!
    var disposeBag = DisposeBag()
    let selectLocVM = SelectLocationVM()
    var noDelivery = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        didGetLocation()
        Helper.PullToClose(scrollView: mainTableView)
        Helper.pullToDismiss?.delegate = self
        buttonWidthConstraint.constant = 40
        closeButton.isHidden = false
        closeButton.isUserInteractionEnabled = true
        closeButton.addTarget(self, action: #selector(followAction), for: .touchUpInside)
        
    }
    
    
    @objc func followAction() {
        self.navigationController?.popViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismissKeyBoard()
        self.removeObserver()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2,
                       animations: {
                       self.dismissKeyBoard()
        }) { (Bool) in
            UIView.animate(withDuration: 0.3, animations: {
                if let wd = UIApplication.shared.delegate?.window {
                    if let vc = wd?.rootViewController?.children.last as? UITabBarController {
                        vc.tabBar.isHidden = false
                    }
                }
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
}

extension SelectLocationVC {
    /// observing selectLocation View Model
    func didGetLocation(){
        self.selectLocVM.SelectLocVMResponse.subscribe(onNext: { [weak self]success in
            if success.1 == false {
                self?.addressSearchBar.resignFirstResponder()
                self?.noDelivery = true
                self?.mainTableView.reloadData()
                return
            }
            Utility.unsubscribeFCMTopicAll()
            Utility.saveAddress(location: (self?.selectLocVM.dataAddress)!)
            Utility.subscribeFCMTopicAll()
            if self?.navigationController == nil {
                if success.0 == false {
                    Utility.UtilityResponse.onNext(false)
                    self?.closeAction(UIButton())
                    return
                }
            }else{
                
                if Changebles.isGrocerOnly
                {
                    Helper.setGrocerOnlyRootViewController()
                }
                else{
                self?.performSegue(withIdentifier:UIConstants.SegueIds.LocationToHome, sender: self)
                }
            }
            
        },onError : { error in
            print(error)
        }).disposed(by: disposeBag)
    }
    
}
