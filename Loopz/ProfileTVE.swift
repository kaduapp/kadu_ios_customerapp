//
//  ProfileTVE.swift
//  UFly
//
//  Created by 3Embed on 09/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit


// MARK: - UITableViewDataSource
extension ProfileVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == profileTableView && Utility.getLogin() {
            
            return mainProfileVM.profileModel.count
        }else{
            return mainProfileVM.profileModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == profileTableView {
            return mainProfileVM.profileModel[section].count
        }
        else {
            return AppConstants.PaymentMethods().count - 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == profileTableView {
            if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.MainProfileCell, for: indexPath) as! MainProfileCell
                cell.updateProfile(userData:mainProfileVM.userData)
                cell.editBtn.addTarget(self, action: #selector(ProfileVC.dismissTheView), for: .touchUpInside)
                return cell
            }else if indexPath.section == mainProfileVM.profileModel.count - 1{
                
                if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.MainProfileMenuCell, for: indexPath) as! MainProfileMenuCell
                cell.menuTitleLeading.constant = 0
                cell.iconWidth.constant = 0
                cell.updateProfileCell(menu: mainProfileVM.profileModel.last![0])
                cell.logoutImage.isHidden = false
                cell.menuListImage.isHidden = true
                cell.logoutImage.setTitle("", for: .normal)
                cell.logoutImage.setImage(#imageLiteral(resourceName: "Logout"), for: .normal)
                  return cell
                }
                else{

                    // Show Version cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.MainProfileMenuCell, for: indexPath) as! MainProfileMenuCell
                    cell.setVersionNumber(version: Changebles.version)

                  return cell
                    
                }
                
              
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.MainProfileMenuCell, for: indexPath) as! MainProfileMenuCell
            
                cell.logoutImage.isHidden = true
                cell.menuTitleLeading.constant = 20
                cell.iconWidth.constant = 22
                cell.menuListImage.isHidden = false
                cell.updateProfileCell(menu: mainProfileVM.profileModel[indexPath.section][indexPath.row])
                
                if indexPath.section == 2, indexPath.row == 0{
                    cell.showPaymentMethods(showPayment: mainProfileVM.showPayment)
                }
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.CellIds.PaymentListCell, for: indexPath) as! PaymentListCell
            cell.updateCell(string: AppConstants.PaymentMethods()[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if (section == 3){
            return 20
        }else if(section == 1) || (section == 2){
            return 50
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == profileTableView {
            if section == 1{
                return Helper.showHeader(title: StringConstants.HelpCenter().uppercased(), showViewAll: false, showSeperator: true)
            }else if section == 2{
                return Helper.showHeader(title: StringConstants.MyAccount(), showViewAll: false, showSeperator: true)
            }else if section == 3{
                return Helper.showHeader(title: "", showViewAll: false, showSeperator: true)
            }
        }
        return Helper.showHeader(title: "", showViewAll: false)
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        switch indexPath.section {
        case 2:
            if indexPath.row == 2{
                return 0
            }
        case 3:
            if indexPath.row == 0 && !Utility.getLogin(){
                return 0
            }
        default:
               return UITableView.automaticDimension
        }
     
         return UITableView.automaticDimension
    }
    
    func logout() {
        Helper.showAlertReturn(message: StringConstants.SureLogout(), head: StringConstants.Logout(), type: StringConstants.Logout(), closeHide: false, responce: .Logout)
    }
}

extension ProfileVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView != profileTableView {
            if Helper.isLoggedIn(isFromHist: false) {
                switch indexPath.row {
                case 1:
                    performSegue(withIdentifier: UIConstants.SegueIds.profileToSavedCards, sender: 1)
                    break
                default:
                    performSegue(withIdentifier: UIConstants.SegueIds.ProfileToQuickCard, sender: 2)
                    break
                }
            }
        }
        else{
            switch indexPath.section{
            case 0:
                switch indexPath.row{
                case 0:
                    //loggin
                    if Helper.isLoggedIn(isFromHist: false){
                        self.title = ""
                        performSegue(withIdentifier: UIConstants.SegueIds.ProfileToEditProfile, sender: self)
                    }
                    break
                default :
                    break
                }
            case 1:
                switch indexPath.row{
//                case 0:
//                    //offer
//                    performSegue(withIdentifier: String(describing: OfferVC.self), sender: self)
//                    break
                case 0:
                    //history(My Orders)
                    if Helper.isLoggedIn(isFromHist: false) {
                        performSegue(withIdentifier: UIConstants.SegueIds.profileToHistoryVC , sender: self)
                    }
                    break
                case 1:
                    //FAQs
                    performSegue(withIdentifier: UIConstants.SegueIds.ProfileToHelpVC , sender: self)
                    break
                case 2:
                    //help and support
                    performSegue(withIdentifier: String.init(describing: TicketsViewController.self) , sender: self)
                    break
                default :
                    break
                }
            case 2:
                switch indexPath.row{
                case 0:
                    //payment
                    if Helper.isLoggedIn(isFromHist: false) {
                        if mainProfileVM.showPayment == true {
                            mainProfileVM.showPayment = false
                        }
                        else {
                            mainProfileVM.showPayment = true
                        }
                        profileTableView.reloadRows(at: [indexPath], with: .fade)
                        
                        //self.profileTableView.reloadData()
                        UIView.animate(withDuration: 0.5) {
                            self.profileTableView.scrollToRow(at: indexPath, at: .middle, animated: true)
                        }
                    }
                    break
                case 1:
                    //Address
                    if Helper.isLoggedIn(isFromHist: false) {
                        self.title = ""
                        performSegue(withIdentifier: UIConstants.SegueIds.ProfileToSavedAddr, sender: self)
                    }
                    break
                case 2:
                    //favourite
                    if Helper.isLoggedIn(isFromHist: false) {
                        performSegue(withIdentifier: UIConstants.SegueIds.ProfileToFav, sender: self)
                    }
                    break
                case 3:
                    //wishList
                    if Helper.isLoggedIn(isFromHist: false) {
                        performSegue(withIdentifier: UIConstants.SegueIds.ProfileToWishList, sender: self)
                    }
                    break
                case 4:
                    //refer your friends
                    self.title = ""
                    if Helper.isLoggedIn(isFromHist: false) {
                        performSegue(withIdentifier: UIConstants.SegueIds.ProfileToInviteVC, sender: self)
                    }
                    break
                case 5:
                    //Language
                    self.title = ""
                    performSegue(withIdentifier: String(describing:LanguageVC.self), sender: self)
                    break
                default :
                    break
                }
            case 3:
                switch indexPath.row{
                case 0 :
                    //logOut
                    Helper.showAlertReturn(message: StringConstants.SureLogout(), head: StringConstants.Logout(), type: StringConstants.Logout(), closeHide: false, responce: .Logout)
                    break
                default :
                    break
                }
            default :
                break
            }
        }
    }
}
