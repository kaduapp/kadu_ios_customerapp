//
//  ProductsCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 1/29/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ProductsCell: UITableViewCell {

    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productProceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    func setupUI() {
        if productProceLabel != nil {
            Fonts.setPrimaryMedium(productProceLabel)
            productProceLabel.textColor = Colors.SeconderyText
        }
        
        Fonts.setPrimaryThin(productNameLabel)
        productNameLabel.textColor = Colors.SearchText
    }
    func setProducts(item:Item) {
        Fonts.setPrimaryRegular(productNameLabel, size: 15)
        productNameLabel.text = item.Name
        productProceLabel.text = Helper.df2so(Double( item.Price))
    }
    func setName(item:String) {
        Fonts.setPrimaryRegular(productNameLabel, size: 15)
        productNameLabel.text = item
    }
}
