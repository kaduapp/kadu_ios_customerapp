//
//  Fonts.swift
//  UFly
//
//  Created by 3Embed on 11/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Fonts: NSObject {

    struct Hind {
        static let Bold             = "Hind-Bold"
        static let Light            = "Hind-Light"
        static let Medium           = "Hind-Medium"
        static let Regular          = "Hind-Regular"
        static let SemiBold         = "Hind-SemiBold"
    }
    
    struct CircularAir {
        static let Light            = "CircularAirPro-Light"
        static let Book             = "CircularAirPro-Bold"
        static let Bold             = "CircularAirPro-Book"
    }
    
    struct Primary {
        static let Bold             = "Muli-Bold"
        static let Regular          = "Muli-Regular"
        static let Light            = "Muli-Light"
        static let Medium           = "Muli-SemiBold"
        static let Thin             = "Muli-ExtraLight"
        static let Black            = "Muli-Black"
        static let Italics          = "Muli-Italic"
    }
    
    //////////////////////////////////////////////////////////////////
    class func setPrimaryBold(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Bold, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Bold, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Bold, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    
    
    
    class func setPrimaryLight(_ sender: Any){
        
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Light, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Light, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Light, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryMedium(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Medium, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Medium, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Medium, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    
    class func setPrimaryItalics(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Italics, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Italics, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Italics, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    
    class func setPrimaryRegular(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Regular, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Regular, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Regular, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryBlack(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Black, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Black, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Black, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    
    class func setPrimaryThin(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Thin, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Thin, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Thin, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    /////////////////////////////////////////////////////
    class func setSeconderyBold(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: CircularAir.Bold, size: element.font.pointSize)
        }
        if let element = sender as? UITextField {
            element.font = UIFont(name: CircularAir.Bold, size: (element.font?.pointSize)!)
        }
    }
    class func setSeconderyLight(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: CircularAir.Light, size: element.font.pointSize)
        }
        if let element = sender as? UITextField {
            element.font = UIFont(name: CircularAir.Light, size: (element.font?.pointSize)!)
        }
    }
    class func setSeconderyBook(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: CircularAir.Book, size: element.font.pointSize)
        }
        if let element = sender as? UITextField {
            element.font = UIFont(name: CircularAir.Book, size: (element.font?.pointSize)!)
        }
    }
    
    // repeated functions with size parameter
    
    class func setPrimaryBold(_ sender: Any, size:CGFloat)
    {
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Bold, size: size)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Bold, size: size)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Bold, size: size)
        }
    }
    class func setPrimaryRegular(_ sender: Any, size:CGFloat){
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Regular, size: size)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Regular, size: size)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Regular, size: size)
        }
    }
    
    class func setPrimaryLight(_ sender: Any , size:CGFloat){
        
        if let element = sender as? UILabel {
            element.font = UIFont(name: Primary.Light, size: size)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: Primary.Light, size: size)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: Primary.Light, size: size)
        }
    }
}
