//
//  SendPackagesTVC.swift
//  DelivX
//
//  Created by Rahul Sharma on 05/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//


import UIKit
import RxCocoa
import RxSwift

class SendPackagesTBC: UITabBarController {
    
    let disposeBag = DisposeBag()
    var tabBarIndex = 0
    override func viewDidLoad(){
        super.viewDidLoad()
        self.delegate = self
        didCartUpdate()
        
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        
    }
}
// MARK: - UITabBarControllerDelegate
extension SendPackagesTBC: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        tabBarIndex = tabBarController.selectedIndex
        AppConstants.SelectedIndex = AppConstants.Tabs(rawValue: tabBarIndex)!
        switch tabBarIndex {
        case 1,2:
            if !Helper.isLoggedIn(isFromHist: true){
                self.tabBarController?.selectedIndex = 0
            }
        default:
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AppDelegate.tabChange != nil {
            let temp = AppDelegate.tabChange
            AppDelegate.tabChange = nil
            Helper.changetTabTo(index: temp!)
        }
    }
    
}

extension SendPackagesTBC {
    
    /// Observe CartDBManager
    func didCartUpdate(){
        CartDBManager.cartDBManager_response.subscribe(onNext: { [weak self]success in
            if success == false {
                self?.tabBarController?.selectedIndex = 0
            }
        }).disposed(by: disposeBag)
    }
    
 
}


