//
//  SendPackageVM.swift
//  DelivX
//
//  Created by Rahul Sharma on 11/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire


class SendPackageVM: NSObject
{
    let disposeBag = DisposeBag()
    var sendPackage_publish  = PublishSubject<Bool>()
    var itemAdded_publish  = PublishSubject<Bool>()
    var packageItems = [CartItem]()
    var pickupAddress : Address?
    var deliveryAddress:Address?
    var addingPackage :CartItem?
    var cartVM = CartVM()
    
    override init() {
        super.init()
        self.subscribeCart()
    }
    
    func subscribeCart(){
        CartVM.cartVM_response.subscribe(onNext: { data in
            if data.0 == CartVM.ResponceType.SuccessNormal {
                if data.1.count > 0{
                    self.packageItems = data.1[0].Products
                }
                else{
                    self.packageItems.removeAll()
                }
            self.sendPackage_publish.onNext(true)
            }
        }).disposed(by: disposeBag)
    }
    
    func addItemToCart(_ item:CartItem){
        
        if !AppConstants.Reachable {
            Helper.showNoInternet()
        }
    
        let params = [
            APIRequestParams.QTY        :  item.QTY,
            APIRequestParams.ItemName    :   item.ItemName,
            "customerId"   :   Utility.getProfileData().SId,
            "orderType" :   2,
            "storeType" : 7
            ] as [String : Any]

        RxAlamofire.requestJSON(.post, APIEndTails.sendPackageCart, parameters: params, headers: Utility.getHeader()).debug().subscribe(onNext: { (head, body) in
            Helper.hidePI()
            let errNum:APICalls.ErrorCode? = APICalls.ErrorCode(rawValue: head.statusCode)!
            if errNum == .created {
            self.cartVM.getCart()
            }else{
                
            }
        }, onError: { (Error) in
            Helper.hidePI()
        }).disposed(by: self.disposeBag)
    }
    
    func isCheckoutEnabled() -> Bool{
        
        if self.pickupAddress != nil && self.deliveryAddress != nil && self.packageItems.count > 0 {
            for item in self.packageItems{
                if item.ItemName.length > 0 && item.QTY > 0{
                   return true
                }
            }
        }
        return false
    }
}



