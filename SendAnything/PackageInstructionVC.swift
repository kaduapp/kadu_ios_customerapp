//
//  PackageInstructionVC.swift
//  DelivX
//
//  Created by Rahul Sharma on 09/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PackageInstructionVC: UIViewController {
    
    @IBOutlet var instructionTableView : UITableView!
    @IBOutlet var footerView : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func dismissButtonAction(sender : UIButton){
    self.dismiss(animated: true, completion: nil)
    }

}

extension PackageInstructionVC : UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PackageLimitsLibrary.limitsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let instructionCell =  tableView.dequeueReusableCell(withIdentifier: String(describing: PackageInstructionTVCell.self), for: indexPath) as! PackageInstructionTVCell
        instructionCell.setViews(PackageLimitsLibrary.limitsList[indexPath.row])
        return instructionCell
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?{
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 110.0
    }
}
