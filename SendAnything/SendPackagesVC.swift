//
//  SendPackagesVC.swift
//  DelivX
//
//  Created by Rahul Sharma on 05/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit


class SendPackagesVC: UIViewController {
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var navWidth : NSLayoutConstraint!
    @IBOutlet weak var navView : UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: SendPackageTableView!
    @IBOutlet weak var checkoutButton : UIButton!
    @IBOutlet weak var checkoutButtonContainer : UIView!
    
    var viewModel = SendPackageVM()
    var checkoutVM = CheckoutVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = "Send Packages"
        self.navTitle.text = "Send Packages"
        Helper.transparentNavigation(controller: self.navigationController!)
        navWidth.constant = UIScreen.main.bounds.width
        self.setSubscriberForCartUpdates()
        AppConstants.extraNotes.removeAll()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.cartVM.getCart()
        self.tableView.reloadData()
        Helper.PullToClose(scrollView:tableView)
        Helper.pullToDismiss?.delegate = self
    self.navigationController?.navigationBar.addSubview(navView)
        self.updateCheckoutState()
        self.addObserver()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeObserver()
        navView.removeFromSuperview()
    }
    
    func setSubscriberForCartUpdates(){
       viewModel.itemAdded_publish.subscribe(onNext: { data in
               self.updateCheckoutState()
            }).disposed(by: viewModel.disposeBag)
    }
    
    @IBAction func addAddressButtonAction(sender: UIButton){
        
        if Helper.isLoggedIn(isFromHist: false){
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        
        let addressVC = storyboard.instantiateViewController(withIdentifier: "SavedAddressVC") as! SavedAddressVC
        addressVC.indexPathSelected = IndexPath.init(row: sender.tag, section: 0)
        addressVC.isSendPackage = true
        addressVC.delegate = self
    self.navigationController?.pushViewController(addressVC, animated: true)
        }
        
    }
    @IBAction func closeButtonAction(sender: UIButton){
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func checkOutButtonAction(sender: UIButton){
     if Helper.isLoggedIn(isFromHist: false){
       self.performSegue(withIdentifier: "sendPackageToConfirmOrder", sender: self)
     }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier ==  "sendPackageToConfirmOrder" {
            if let itemDetailVC: ConfirmOrderVC = segue.destination as? ConfirmOrderVC {
                itemDetailVC.checkoutVM = self.checkoutVM
            }
        }
    }
    
 
    override func keyboardWillShow(_ notification: NSNotification) {
        let kbSize = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.size.height  , right: 0.0)
       
        self.tableView.contentInset = contentInsets;
        self.tableView.scrollIndicatorInsets = contentInsets;
    }
    
    override func keyboardWillHide(_ notification: NSNotification) {
        self.tableView.contentInset = UIEdgeInsets.zero;
        self.tableView.scrollIndicatorInsets = UIEdgeInsets.zero;
    }
    
    
    func updateCheckoutState(){
        
        if viewModel.isCheckoutEnabled()
        {
            Helper.setButton(button: self.checkoutButton, view: self.checkoutButtonContainer, primaryColour:  Colors.AppBaseColor, seconderyColor:  Colors.SecondBaseColor, shadow: true)
            self.checkoutButton.isUserInteractionEnabled = true
        }else
        {
            Helper.setButton(button: self.checkoutButton, view: self.checkoutButtonContainer, primaryColour:  Colors.DoneBtnNormal, seconderyColor:  Colors.SecondBaseColor, shadow: true)
            self.checkoutButton.isUserInteractionEnabled = false
        }
    
    }
}

class SendPackageTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
       register(PackageItemsTVCell.nib, forCellReuseIdentifier: PackageItemsTVCell.identifier)
    }
}

extension SendPackagesVC : UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0,1:
          let addressCell =  tableView.dequeueReusableCell(withIdentifier: String(describing: PackagesAddressTVCell.self), for: indexPath) as! PackagesAddressTVCell
          addressCell.setViews(index: indexPath.row,checkoutVM)
          return addressCell
        case 2:
            let packageCell =  tableView.dequeueReusableCell(withIdentifier: String(describing: PackageItemsTVCell.self), for: indexPath) as! PackageItemsTVCell
               packageCell.pakageVMRef = viewModel
            if !viewModel.sendPackage_publish.hasObservers{
            viewModel.sendPackage_publish.subscribe(onNext: { data in
                self.updateCheckoutState()
                UIView.setAnimationsEnabled(false)
                self.tableView?.beginUpdates()
                packageCell.itemsTableView.reloadData()
                self.tableView?.endUpdates()
                UIView.setAnimationsEnabled(true)
             
                }).disposed(by: viewModel.disposeBag)
            }
            return packageCell
        case 3:
            let estimateCell =  tableView.dequeueReusableCell(withIdentifier: String(describing: EstimatePriceTVCell.self), for: indexPath) as! EstimatePriceTVCell
            estimateCell.currencySymbol.text = Utility.getCurrency().1
            estimateCell.estimatePriceField.text = checkoutVM.packageEstimateValue
            return estimateCell
        case 4:
            let additionalNotesCell =  tableView.dequeueReusableCell(withIdentifier: String(describing: ExtraNotesTVCell.self), for: indexPath) as! ExtraNotesTVCell
            additionalNotesCell.instructions =  AppConstants.extraNotes["1"] ?? ""
            return additionalNotesCell
        default:
       
            return UITableViewCell()
        }
  
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    
}


extension SendPackagesVC:UIScrollViewDelegate
{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        var offset = scrollView.contentOffset.y / 35
        if self.navigationController != nil {
            if scrollView.contentOffset.y > 1{
                Helper.addShadowToNavigationBar(controller: self.navigationController!)
            }
            else{
                Helper.removeShadowToNavigationBar(controller: self.navigationController!)
            }
        }
        if offset > 30{
            offset = 1
            setTheAlphaToNavigationViewsOnScroll(offset)
        }
        else{
            setTheAlphaToNavigationViewsOnScroll(offset)
        }
    }
    
    func setTheAlphaToNavigationViewsOnScroll(_ offset:CGFloat){
        self.navTitle.alpha = offset
        self.titleLabel.alpha = 1 - offset
    }
}


extension SendPackagesVC:SavedAddressVCDelegate
{
    func didSelectPickupAddress(address: Address) {
        
    }
    
    func didSelectAddress(address: Address) {
        
    }
    
    func didSelectAddress(address: Address, indexPath: IndexPath) {
        if indexPath.row == 0{
         checkoutVM.pickupAddress = address
         viewModel.pickupAddress = address
        }
        else{
        checkoutVM.selectedAddress = address
        checkoutVM.deliveryAddress = address
         viewModel.deliveryAddress = address
        }
        
        self.tableView.reloadData()
        self.updateCheckoutState()
    }

}

extension SendPackagesVC : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkoutVM.packageEstimateValue = textField.text ?? ""
    }
    
}
