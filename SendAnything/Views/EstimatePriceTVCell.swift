//
//  EstimatePriceTVCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 10/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class EstimatePriceTVCell: UITableViewCell {

    @IBOutlet var estimatePriceField : UITextField!
    @IBOutlet var currencySymbol : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Helper.addDoneButtonOnTextField(tf: estimatePriceField, vc: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
