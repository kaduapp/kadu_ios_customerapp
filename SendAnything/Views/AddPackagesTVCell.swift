//
//  PackageItemTableViewCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 05/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

protocol AddPackageDelegate {
    func additemToPackage(_ item: CartItem)
}

class AddPackagesTVCell: UITableViewCell {

    @IBOutlet var itemTextField : UITextView!
    @IBOutlet var quantity : UITextView!
    @IBOutlet var removeButton : UIButton!
    var tableViewRef : UITableView!
    var isFirstTime = true
    var delegate:AddPackageDelegate? = nil
    var pakageVM = SendPackageVM()
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle:nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.itemTextField.text = "Item Name"
        self.quantity.text = "Quantity"
        self.itemTextField.textColor = Helper.getUIColor(color: "#C1C1C1")
        self.quantity.textColor = Helper.getUIColor(color: "#C1C1C1")
        self.addToolbar(itemTextField)
        self.addToolbar(quantity)
      
        // Initialization code
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /// Custom setter so we can initialise the height of the text view
    var itemName: String {
        get {
            return itemTextField.text
        }
        set {
            itemTextField.text = newValue
            
            textViewDidChange(itemTextField)
            
            if isFirstTime{
            DispatchQueue.main.async {
                self.textViewDidChange(self.itemTextField)
            }
            }
        }
    }
    
    /// Custom setter so we can initialise the height of the text view
    var itemQuantity: String {
        get {
            return quantity.text
        }
        set {
            quantity.text = newValue
            textViewDidChange(quantity)
        }
    }
    
    func setItems(_ item : CartItem){
        
        if item.ItemName.length > 0{
        self.itemTextField.text = item.ItemName
         self.itemTextField.textColor = Helper.getUIColor(color: "#242A4B")
        }
        else{
            self.itemTextField.text = "Item Name"
            self.itemTextField.textColor = Helper.getUIColor(color: "#C1C1C1")
        }
        
        if item.QTY > 0{
         self.quantity.text = "\(item.QTY)"
         self.quantity.textColor = Helper.getUIColor(color: "#242A4B")
        }
        else{
            self.quantity.text = "Quantity"
            self.quantity.textColor = Helper.getUIColor(color: "#C1C1C1")
        }

    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func addToolbar(_ textView:UITextView) {
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        
        var buttonText = ""
        
        if textView.tag == 0{
            buttonText = StringConstants.Next()
        }
        else{
           buttonText = StringConstants.Done()
        }
        let doneBarButton = UIBarButtonItem.init(title: buttonText, style: .plain, target: self, action: #selector(doneButtonAction))
        doneBarButton.tag = textView.tag
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textView.inputAccessoryView = keyboardToolbar
    }
    
    @objc func doneButtonAction(_ sender : UIBarButtonItem){
        
        if sender.tag == 0{
         quantity.becomeFirstResponder()
        }
        else{
            if delegate != nil,itemTextField.text != "Item Name",quantity.text != "Quantity",  itemTextField.text.length > 0, quantity.text.length > 0 {
                let package = CartItem.init(data: [:])
                package.ItemName = itemTextField.text
                package.QTY = Int(quantity.text)!
            delegate?.additemToPackage(package)
            }
         self.endEditing(true)
        }
        
    }
    
}

extension AddPackagesTVCell: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == Helper.getUIColor(color: "#C1C1C1") {
            textView.text = nil
            textView.textColor = Helper.getUIColor(color: "#242A4B")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            if textView.tag == 0{
            textView.text = "Item Name"
            }
            else{
            textView.text = "Quantity"
            }
            textView.textColor = Helper.getUIColor(color: "#C1C1C1")
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textView.textColor = Helper.getUIColor(color: "#242A4B")
        if pakageVM.addingPackage == nil {
            pakageVM.addingPackage = CartItem.init(data: [:])
        }
        if textView.tag == 0{
            pakageVM.addingPackage?.ItemName = textView.text
        let size = textView.bounds.size
        let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
        
        // Resize the cell only when cell's size is changed
        if size.height != newSize.height || isFirstTime {
            UIView.setAnimationsEnabled(false)
            tableView?.beginUpdates()
            tableView?.endUpdates()
            UIView.setAnimationsEnabled(true)
            
            if let thisIndexPath = tableView?.indexPath(for: self) {
                tableView?.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
            }
            if tableViewRef != nil{
            UIView.setAnimationsEnabled(false)
            isFirstTime = false
            tableViewRef.beginUpdates()
            tableViewRef.endUpdates()
            UIView.setAnimationsEnabled(true)
            }
         }
        }
        else{
            if textView.text.count > 0, let quantity = Int(textView.text){
            pakageVM.addingPackage?.QTY = quantity
            }
        }
    }
    
}





extension UITableViewCell {
    /// Search up the view hierarchy of the table view cell to find the containing table view
    var tableView: UITableView? {
        get {
            var table: UIView? = superview
            while !(table is UITableView) && table != nil {
                table = table?.superview
            }
        
            return table as? UITableView
        }
    }
}
