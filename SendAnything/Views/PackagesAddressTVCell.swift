//
//  PackagesAddressTVCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 05/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PackagesAddressTVCell: UITableViewCell {
    
    @IBOutlet var addressTitle : UILabel!
    @IBOutlet var addressField : UILabel!
    @IBOutlet var underLineView : UIView!
    @IBOutlet var addAddressButton : UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setViews(index: Int, _ viewModel : CheckoutVM ){

         addAddressButton.tag = index
        if index == 0{
        addressTitle.text = "Pickup Address*"
        addressField.text = "Add pickup address"
        addressField.textColor = Helper.getUIColor(color: "#C1C1C1")
            if let address = viewModel.pickupAddress{
                addressField.text = AddressUtility.getSortedAdress(address)
                addressField.textColor = Helper.getUIColor(color: "#242A4B")
                }
        }
        else{
        addressTitle.text = "Delivery Address*"
        addressField.text = "Add delivery address"
        addressField.textColor = Helper.getUIColor(color: "#C1C1C1")
        if let address = viewModel.deliveryAddress{
                addressField.text = AddressUtility.getSortedAdress(address)
            addressField.textColor = Helper.getUIColor(color: "#242A4B")
         }
        }
    }
    

}
