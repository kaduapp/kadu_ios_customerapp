//
//  PackageItemsTVCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 05/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire

class PackageItemsTVCell: UITableViewCell {

    @IBOutlet var itemsTableView: PackageItemsTableView!
    @IBOutlet var addNewItemButton: UIButton!
    var pakageVMRef = SendPackageVM()
    static var identifier:String {
        return String(describing: self)
    }

    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle:nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addNewItemButton.tag = 1
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func addNewItemButtonAction(_ sender : UIButton){
         if Helper.isLoggedIn(isFromHist: false){
        if let addingItem = pakageVMRef.addingPackage{
        self.pakageVMRef.addItemToCart(addingItem)
        self.pakageVMRef.packageItems.append(addingItem)
        self.pakageVMRef.addingPackage = nil
            UIView.setAnimationsEnabled(false)
            self.tableView?.beginUpdates()
            self.itemsTableView?.reloadData()
            self.tableView?.endUpdates()
            UIView.setAnimationsEnabled(true)
        }
      }
    }
    

    
    
}

class PackageItemsTableView: UITableView {
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        register(AddPackagesTVCell.nib, forCellReuseIdentifier: AddPackagesTVCell.identifier)
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        let newContentSize = CGSize.init(width:self.contentSize.width , height: self.contentSize.height )
        return newContentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}


extension PackageItemsTVCell:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pakageVMRef.packageItems.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AddPackagesTVCell.identifier) as! AddPackagesTVCell
        if indexPath.row == pakageVMRef.packageItems.count{
        cell.removeButton.isHidden = true
            cell.setItems(CartItem.init(data: [:]))
        cell.itemTextField.isUserInteractionEnabled = true
        cell.quantity.isUserInteractionEnabled = true
        }
        else{
        cell.itemTextField.isUserInteractionEnabled = false
        cell.quantity.isUserInteractionEnabled = false
     cell.setItems(pakageVMRef.packageItems[indexPath.row])
        cell.removeButton.isHidden = false
        cell.removeButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(removePackageItemAction), for: .touchUpInside)
        }
        
        cell.tableViewRef = self.tableView
        cell.pakageVM = self.pakageVMRef
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }

    @objc func removePackageItemAction(_ sender : UIButton){
    self.pakageVMRef.cartVM.deleteSendPackageItem(cartItem: self.pakageVMRef.packageItems[sender.tag])
        self.pakageVMRef.packageItems.remove(at: sender.tag)
        UIView.setAnimationsEnabled(false)
        self.tableView?.beginUpdates()
        self.itemsTableView?.reloadData()
        self.tableView?.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
}

extension PackageItemsTVCell: AddPackageDelegate{
    func additemToPackage(_ item: CartItem) {
        if Helper.isLoggedIn(isFromHist: false){
        self.pakageVMRef.addItemToCart(item)
        self.pakageVMRef.packageItems.append(item)
        self.pakageVMRef.addingPackage = nil
         UIView.setAnimationsEnabled(false)
        self.tableView?.beginUpdates()
        self.itemsTableView?.reloadData()
        self.tableView?.endUpdates()
        UIView.setAnimationsEnabled(true)
        }
    }
 
}



