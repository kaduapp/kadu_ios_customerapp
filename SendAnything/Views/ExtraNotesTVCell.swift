

//
//  ExtraNotesTVCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 13/03/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ExtraNotesTVCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    var placeholderLabel : UILabel!
    var isFirstTime = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       Helper.addDoneButtonOnTextView(descriptionTextView, vc: self)

    }
    


    /// Custom setter so we can initialise the height of the text view
    var instructions: String {
        get {
            return descriptionTextView.text
        }
        set {
            if newValue == "" || newValue == "Any Special Instructions..." {
            descriptionTextView.text = "Any Special Instructions..."
            self.descriptionTextView.textColor = Colors.laundryUnselectCellColor
            }else{
            descriptionTextView.text = newValue
            self.descriptionTextView.textColor = UIColor.black
            }
            textViewDidChange(descriptionTextView)
        }
    }
   
}

extension ExtraNotesTVCell: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == Colors.laundryUnselectCellColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Any Special Instructions..."
            textView.textColor = Colors.laundryUnselectCellColor
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
//        textView.textColor = UIColor.black
            let size = textView.bounds.size
            let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
            // Resize the cell only when cell's size is changed
            if size.height != newSize.height || isFirstTime {
                UIView.setAnimationsEnabled(false)
                tableView?.beginUpdates()
                tableView?.endUpdates()
                UIView.setAnimationsEnabled(true)
                
                if let thisIndexPath = tableView?.indexPath(for: self) {
                    tableView?.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
                }
            }
         AppConstants.extraNotes.updateValue(textView.text, forKey: "1")
    }
    
}
