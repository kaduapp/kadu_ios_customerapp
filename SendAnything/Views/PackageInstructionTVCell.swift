//
//  PackageInstructionTVCell.swift
//  DelivX
//
//  Created by Rahul Sharma on 09/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PackageInstructionTVCell: UITableViewCell {

    @IBOutlet var instructionTitle : UILabel!
    @IBOutlet var instructionMessage : UILabel!
    @IBOutlet var instructionImage : UIImageView!
    
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle:nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setViews(_ list : PackageLimits){
        
        instructionTitle.text = list.title
        instructionMessage.text = list.message
        instructionImage.image = list.icon
    
    }
    
}
