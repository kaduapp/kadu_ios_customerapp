//
//  SendPackage.swift
//  DelivX
//
//  Created by Rahul Sharma on 12/09/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation


class PackageItem: NSObject
{
    var itemName  = ""
    var itemQuantity = ""
    
    init(_ itemName : String, _ quantity : String) {
        
        self.itemName = itemName
        self.itemQuantity = quantity
    }
}



struct PackageLimits {
    var title = ""
    var message = ""
    var icon = UIImage()
}

class PackageLimitsLibrary {
    
    static var limitsList: [PackageLimits] {
        
        var limit1 = PackageLimits()
        limit1.title = "No purchases"
        limit1.message = "Delivery partner would not be able to make any purchase"
        limit1.icon = #imageLiteral(resourceName: "Group 6013")
        
        var limit2 = PackageLimits()
        limit2.title = "Package weight"
        limit2.message = "We cannot deliver items that cannot be easily carried on a vehicle"
        limit2.icon = #imageLiteral(resourceName: "Group 6014")
        
        var limit3 = PackageLimits()
        limit3.title = "No autos or cabs"
        limit3.message = "We will not be able to get something transported in an auto or a cab"
        limit3.icon = #imageLiteral(resourceName: "Group 6015")
        
        var limit4 = PackageLimits()
        limit4.title = "Restricted/ Illegal Items"
        limit4.message = "Please don't hand over any restricted or illegal items"
        limit4.icon = #imageLiteral(resourceName: "Group 6016")
        
        return [limit1,limit2,limit3,limit4]
        
        
    }
}

